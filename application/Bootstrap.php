
<?php

use Zend\Cache\Storage\Adapter\Memcached;
use Zend\Cache\StorageFactory;
use Zend\Session\SaveHandler\Cache;
use Zend\Session\SessionManager;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
class Bootstrap extends Yaf\Bootstrap_Abstract {

    protected $c;
    static $rc;


    public function _initAuth() {
        $this->c=Yaf\Application::app()->getConfig();
    }
    function _initConst(){

        $consts=$this->c['const'];
        if($consts){
            $consts=$consts->toArray();
            foreach($consts as $key=>$val){
                define($key,$val);
            }
        }
        define('DEF_MONGO_DB',$this->c['mongo']['defdb']);
        define('DEF_MYSQL_DB',$this->c['database']['defdb']);

    }
     function _initGZ(){

        if (defined('G_GZIP_COMPRESS') AND G_GZIP_COMPRESS === TRUE)
        {
            if (@ini_get('zlib.output_compression') == FALSE)
            {
                if (extension_loaded('zlib'))
                {
                    if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) AND strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== FALSE)
                    {
                        ob_start('ob_gzhandler');
                    }
                }
            }
        }
    }
    /**
     * psr2.0,travis-ci.com,drone.io
     */
    public function _initConfig(Yaf\Dispatcher $dispatcher) {
        /**
         * 把配置保存起来
         */

        error_reporting(0);
        set_include_path(get_include_path().PATH_SEPARATOR.APP_PATH."/application/library");

        /**
         * if set default_socket_timeout=-1 in php-fpm mode,will cause error  msg "mongodb  Operation now in progress"
         */
        if(strtolower(PHP_SAPI)=='cli'){
            ini_set('default_socket_timeout', 9999999999999);
        }
        mb_internal_encoding("utf-8");
        mb_http_output("UTF-8");
        $env=array();

        $env['isLocal']=Misc_Utils::isLocal();
        $env['isAjax']=$dispatcher->getRequest()->isXmlHttpRequest();
        $env['isJsonReq']=(strstr($_SERVER['CONTENT_TYPE'], 'application/json')!==false);
        $env['sockUrl']=Misc_Utils::is_https()?"https://{$_SERVER['HTTP_HOST']}:8443":"http://{$_SERVER['HTTP_HOST']}:8888";
        $env['siteUrl']=Misc_Utils::is_https()?"https://{$_SERVER['HTTP_HOST']}":"http://{$_SERVER['HTTP_HOST']}";

        Yaf\Registry::set('env', $env);
        Yaf\Registry::set('config', $this->c);




    }
    function _initFuncs(){
        include_once('Common.php');

    }
    public function _initPlugin(Yaf\Dispatcher $dispatcher) {
        /**
         * 注册一个插件
         */
//        if(class_exists("SamplePlugin")){
//            $objSamplePlugin = new SamplePlugin($dispatcher);
//            $dispatcher->registerPlugin($objSamplePlugin);
//        }


    }

    public function _initRoute(Yaf\Dispatcher $dispatcher) {
        /**
         * 在这里注册自己的路由协议,默认使用简单路由  通过派遣器获取默认的路由器
         * 动态路由在>yaf 2.3.0起作用
         */

        /**
         * dynamic resigister route
         * 为模块配置子域名的时候,并发起json/xml请求的时候
         * 使得nginx中配置的api.w.cc rewrite到 w.cc/api
         */


        $modules=explode(",",$this->c['application']['modules']);
        $router = $dispatcher->getRouter();

        array_unshift($modules,1);
   foreach($modules as $k=>$mod)
        {
            $mod2="/";
            $module="index";
            if($mod!==1)
            {
                $module=strtolower($mod);
                $mod2='/'.$module.'/';
            }

            $router->addRoute("json_{$k}_0",
                new Yaf\Route\Regex(
                    "#^$mod2([a-zA-Z_][\w-]+)/(\d+)\.(json|xml)$#", //match request uri leading "/product"
                    array(
                        'module'=>$module,
                        'controller' => ":controller",  //使用上面匹配的:name, 也就是$1作为controller
                        'action'=>"index"
                    ),
                    array(
                        1 => "controller",   // now you can call $request->getParam("name")
                        2 => "id",     // to get the first captrue in the match pattern.
                        3=>"req_type"
                    )
                )
            );
            $router->addRoute("json_{$k}_1",
                new Yaf\Route\Regex(
                    "#^$mod2([a-zA-Z_][\w-]+)\.(json|xml)$#", //match request uri leading "/product"
                    array(
                        'module'=>$module,
                        'controller' => ":controller",  //使用上面匹配的:name, 也就是$1作为controller
                        'action'=>"index"
                    ),
                    array(
                        1 => "controller"  // now you can call $request->getParam("name")
                    )
                )
            );
            $router->addRoute("json_{$k}_2",
                new Yaf\Route\Regex(
                    "#^$mod2([a-zA-Z_][\w-]+)/([a-zA-Z_][\w-]+)\.(json|xml)$#", //match request uri leading "/product"
                    array(
                        'module'=>$module,
                        'controller' => ":controller",  //使用上面匹配的:name, 也就是$1作为controller
                        'action'=>":action"
                    ),
                    array(

                        1 => "controller",   // now you can call $request->getParam("name")
                        2=>"action"
                    )
                )
            );

            $router->addRoute("json_{$k}_3",
                new Yaf\Route\Regex(
                    "#^$mod2([a-zA-Z_][\w-]+)/([a-zA-Z_][\w-]+)/(([^\/]+)/)*([\w-\.]+)\.(json|xml)$#", //match request uri leading "/product"
                    array(
                        'module'=>$module,
                        'controller' => ":controller",  //使用上面匹配的:name, 也就是$1作为controller
                        'action' => ":action",  //使用上面匹配的:name, 也就是$1作为controller
                    ),
                    array(
                        1 => "controller",
                        2 => "action",
                        3=>"param_ext",
                        4=>"param",
                        5=>"id",
                        6=>"req_type"
                    )
                )
            );

        }

        $router->addRoute("json_32",
            new Yaf\Route\Regex(
                "#^/resti/([a-zA-Z_][\w-]+)/([^\?]+)(\.json|\.xml)?$#", //match request uri leading "/product"
                array(
                    'module'=>"admin",
                    'controller' => "table",  //使用上面匹配的:name, 也就是$1作为controller
                    'action'=>"resti"
                ),
                array(
                    1=>"ztb",
                    2=>"action"
                )
            )
        );

        $router->addRoute("json_30",
            new Yaf\Route\Regex(
                "#^/rest/([a-zA-Z_][\w-]+)/([\w-]+)(\.json|\.xml)?$#", //match request uri leading "/product"
                array(
                    'module'=>"admin",
                    'controller' => "table",  //使用上面匹配的:name, 也就是$1作为controller
                    'action'=>"rest"
                ),
                array(
                    1=>"ztb",
                    2=>"ids"
                )
            )
        );

        $router->addRoute("json_31",
            new Yaf\Route\Regex(
                "#^/rest/([a-zA-Z_][\w-]+)(\.json|\.xml)?$#", //match request uri leading "/product"
                array(
                    'module'=>"admin",
                    'controller' => "table",  //使用上面匹配的:name, 也就是$1作为controller
                    'action'=>"rest"
                ),
                array(
                    1=>"ztb"
                )
            )
        );

        $router->addConfig($this->c->routes);


    }

    public function _initNamespaces(){
        /**
         * 初始化命名空间
         * 凡是以Zend,Local开头的类, 都是本地类
         */

        Yaf\Loader::getInstance()->registerLocalNameSpace(array("Zend", "Local","Pel"));
    }

    public function _initView(Yaf\Dispatcher $dispatcher){
        /**
         * 初始化view
         *
         */
        /**
         * e2 激活并初始化smarty view,否则render无法获取模板内容
         */
        $enableView=$_GET['_ev2'];
        if((!$dispatcher->getRequest()->isXmlHttpRequest() && $this->r->module!='Openapi') || $enableView==1)
        {
            $view=$this->c->view;
            $viewName=$view->get('name');
            if(!empty($view) && !empty($viewName) && method_exists($this,$viewName))
            {
                call_user_func(array($this,$viewName));
            }
        }

    }
    public function _initLayout(Yaf\Dispatcher $dispatcher){



    }
    public function _initMongo(){

//            if(class_exists('MongoClient')){
//                $defDb=$this->c['mongo']->default_db;
//
//                $mongoArr=$this->c['mongo']->toArray();
//                $read_idx=array_rand($mongoArr[$defDb]['r']);
//                $write_idx=array_rand($mongoArr[$defDb]['w']);
//
//                $mongoReadCfg=$mongoArr[$defDb]['r'][$read_idx];
//                $mongoWriteCfg=$mongoArr[$defDb]['w'][$write_idx];
//                try {
//                    $readDsn="mongodb://${mongoReadCfg['uid']}:${mongoReadCfg['pwd']}@${mongoReadCfg['host']}:${mongoReadCfg['port']}/{$mongoReadCfg['db']}";
//                    $writeDsn="mongodb://${mongoReadCfg['uid']}:${mongoReadCfg['pwd']}@${mongoReadCfg['host']}:${mongoReadCfg['port']}/{$mongoReadCfg['db']}";
//                    if(($mongoReadCfg['host']==$mongoWriteCfg['host'] && $mongoReadCfg['port']==$mongoWriteCfg['port']) || empty($mongoWriteCfg['host'])){
//                        $r_db=$w_db = new MongoClient($readDsn, $mongoReadCfg['options']);
//
//                    } else {
//                        $r_db= new MongoClient($readDsn, $mongoReadCfg['options']);
//                        $w_db = new MongoClient($writeDsn, $mongoWriteCfg['options']);
//
//                    }
//                } catch(Exception $ex){
//                    $this->log->error(print_r($ex,true));
//                }
//
//
//            }
//
//
//
//            Yaf\Registry::set("mongo_read_db", array($r_db,$mongoReadCfg['db']));
//            Yaf\Registry::set("mongo_write_db", array($w_db,$mongoReadCfg['db']));



    }
    public function _initDefaultDbAdapter(){
//        $dbAdapter = new Zend\Db\Adapter\Adapter(
//            Yaf\Application::app()->getConfig()->database->params->toArray()
//        );
//
//
//        Yaf\Registry::set("mysql_adapter_rw", $dbAdapter);
    }
    protected  function _initLogin(){


        /**
         *
         *
        define('UC_CONNECT', $this->c->snow->UC_CONNECT);
        define('UC_DBHOST', $this->c->snow->UC_DBHOST);
        define('UC_DBUSER', $this->c->snow->UC_DBUSER);
        define('UC_DBPW', $this->c->snow->UC_DBPW);
        define('UC_DBNAME', $this->c->snow->UC_DBNAME);           //  数据库名称
        define('UC_DBCHARSET', $this->c->snow->UC_DBCHARSET);     // 数据库字符集
        define('UC_DBTABLEPRE', $this->c->snow->UC_DBTABLEPRE);   // 数据库表前缀
        define('UC_KEY', $this->c->snow->UC_KEY);                 //  通信密钥,
        define('UC_API', $this->c->snow->UC_API);                 //  URL 地址, 在调用头像时依赖此常量
        define('UC_CHARSET', $this->c->snow->UC_CHARSET);         // 字符集
        define('UC_IP', $this->c->snow->UC_IP);                   // IP
        define('UC_APPID', $this->c->snow->UC_APPID);             //ID
        include './uc_client/client.php';
         *
         *      *
         */


    }

    protected function _initRules(){
        /**
         * @todo 后端生成model,前端生成backbone model和检验代码,后端REST风格代码,
         * 1. PHP端使用filter重新改造,加快速度和准确性,http://php.net/manual/en/filter.filters.validate.php
         * 2. 增加校验函数,比如校验ip每个小段可以使用大于0,小于256来判断
         *
         *
         */
        $rules=array(
            'local_ip'=>'/(^127\.0\.0\.1)|(^10\.)|(^172\.1[6-9]\.)|(^172\.2[0-9]\.)|(^172\.3[0-1]\.)|(^192\.168\.)/',
            "sm_desc"=>"/^[\s\S]{1,1000}$/",  //333 chinese chars
            "mid_desc"=>"/^[\s\S]{1,3000}$/", //1000 chinese chars
            "desc"=>"/^[\s\S]{1,10000}$/",//3333 chinese chars
            "tiny_desc"=>"/^[\s\S]{1,300}$/",
            "text"=>"/^[\s\S]{1,65535}$/",
            "lg_desc"=>"/^[\s\S]{1,30000}$/",//10000 chinese chars
            "mediumtext"=>"/^[\s\S]{1,16777215}$/",
            "longtext"=>"/^[\s\S]{1,4294967295}$/",

            "domain_comp"=>"/^([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]+$|^[a-z0-9][\w-]+$/i",
            "domain"=>"/^([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,}$/i",
            //sub domain

            "domain2"=>"/^([a-z0-9]+(-[a-z0-9]+)*\.){2}[a-z]{2,}$/i",
            "domain3"=>"/^([a-z0-9]+(-[a-z0-9]+)*\.){3}[a-z]{2,}$/i",

            "en"=>"/[a-zA-Z]+/",
            "mongoid"=>"/[0-9a-z]{24}/",
            "user"=>"/^1[35789]\d{9}$|^[\w-]+$|^\w[\w\.-]*@[0-9a-zA-Z][0-9a-zA-Z-]*\.[a-zA-Z]{1,3}$/i",
            "varchar"=>"/^[\s\S]{1,65535}$/",
            "char"=>"/^[\s\S]{1,255}$/",

            "hexcolor"=>"/^#[0-9a-f]{6}|#[0-9a-f]{3}$/",
            "rgbcolor"=>"/^rgb\(\d{1,3},\d{1,3},\d{1,3}\)$/",
            //chinese char will cost 3
            "tinytext"=>"/^[\s\S]{1,255}$/",
            "title"=>"/^[\s\S]{1,45}$/",
            "sm_title"=>"/^[\s\S]{1,65}$/",
            "mid_title"=>"/^[\s\S]{1,85}$/",
            "lg_title"=>"/^[\s\S]{1,105}$/",
            "en_name"=>"/[\w-]{0,30}/",
            "en_type"=>"/[\w-]{0,10}/",
            "http"=>"/(http|https)+:\/\/[^s]*/",
            "appid"=>"/^[a-zA-Z]{2}[0-9a-zA-Z]{10}$/",
            "ry_appid"=>"/^[0-9a-zA-Z]{12,15}$/",
            "appsecret"=>"/^[0-9a-zA-Z]{32}$/",
            "ry_appsecret"=>"/^.{32}$/",
            "uuid"=>"/^[0-9a-zA-Z]{8}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{12}$/",
            "realname"=>"/^[-\w\x{4e00}-\x{9fa5}][-\w\x{4e00}-\x{9fa5}\s]{1,24}$/u",
            "username"=>"/^[a-zA-Z][\w-]{1,17}$/",
            "password"=>"/(?=.{2,})/",
            "icon_url"=>"/^(fa\s+)?(fa-|icon-)\w{1,20}$|^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/",
            "http_url"=>"/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/",
            "email"=>"/^\w[\w\.-]*@[0-9a-zA-Z][0-9a-zA-Z-]*\.[a-zA-Z]{2,6}$/",
            "url"=>"/(http|https|ftp|blob)+:\/\/[^s]*/",
            "fileurl"=>"/(^(http|https|ftp|blob|file)+:\/\/)?.+$|^[\w\/\-]+([\w\/]|\.[\w]+)$/",
            "cdn_path"=>'/^([\w-]+\.){1,}[a-z]{2,}(\/|\/[\w-]){1,}/i',
            "file"=>"/^(http|https|ftp|blob|file)+:\/\/.+$|^[\w\/\-]+([\w\/]|\.[\w]+)$/",
            'range'=>'/^\d+$/',
            "cn_phone"=>"/^\d{7,8}$/",
            "cn_zone"=>"/^\d{4}$/",
            "hex"=>"/^#?([a-f0-9]{6}|[a-f0-9]{3})$/",
            "slug"=>"/^[a-z0-9-]+$/",
            "qq"=>"/^\d{4,}$/",
            "age"=>"/^[1-9]$|^1\d{1,2}$/",
            "cn_id"=>"/^\d{15}|\d{18}$/",
            "cn_zip"=>"/^[1-9][0-9]{5}$/",
            "cn_mobile"=>"/^1[3|5|8|7]\d{9}$/",
            'chinese'=>"/[\u4e00-\u9fa5]/",
            "cn_zone_phone"=>"/^\d{3}-\d{8}$|^\d{4}-\d{7}$/",
            "ipv4"=>"/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/",
            //attributes can't contain greater than signs;
            //does not match <img src="img.jpg" alt="My image>" />
            "html"=>"/^[\s\S]{1,65535}$/",
            "html_tag"=>"/^<([a-z]+)([^<]+)*(?:>(.*)<\/\1>|\s+\/>)$/",
            "cn_en"=>"/^[\x{4e00}-\x{9fa5}\w-][\x{4e00}-\x{9fa5}\w-\s]+$/u",//"^[\w\u4E00-\u9FFF]+$",
            'cn'=>"/^[\x{4e00}-\x{9fa5}]+$/u",
            "notempty"=>"/^[\s\S]+$/",
            "phone_400"=>"/^400[016789]\d{6}$/",
            "phone_800"=>"/^800\d7/",
            "resetpwd_token"=>"/^[a-zA-Z0-9]{8,24}$/",
            "resetpwd_code"=>"/^\d{6}$/",
            "the_captcha"=>"/^[0-9a-zA-Z]{3,5}$/",
            "res_name"=>"/^[trc]_\w+$/",
            "mfa"=>"/^\d{6}$/",
            "mime"=>"/^[\w-]+\/[\w-]+$/",
            "php_timestamp"=>"/^[1-9]{7,9}$/",
            "js_timestamp"=>"/^[1-9]{10,12}$/",
            "timespan"=>"/^[1-9]\d+$/",
            "device_name"=>"/[\S][\S ]*[\S]$/i",
            "openid"=>"/^[0-9a-z]{28}$/i",
            "json"=>'/(?(DEFINE)
         (?<number>  -? (?= [1-9]|0(?!\d) ) \d+ (\.\d+)? ([eE] [+-]? \d+)? )
         (?<boolean>   true | false | null )
         (?<string>    " ([^"\\\\]* | \\\\ ["\\\\bfnrt\/] | \\\\ u [0-9a-f]{4} )* " )
         (?<array>     \[  (?:  (?&json)  (?: , (?&json)  )*  )?  \s* \] )
         (?<pair>      \s* (?&string) \s* : (?&json)  )
         (?<object>    \{  (?:  (?&pair)  (?: , (?&pair)  )*  )?  \s* \} )
         (?<json>   \s* (?: (?&number) | (?&boolean) | (?&string) | (?&array) | (?&object) ) \s* )
      )\A (?&json) \Z /six',
            "search"=>"/^[-&\<\>\x{4e00}-\x{9fa5}\w_]+$/u",
            "ipv4"=>"/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/",
            "domain"=>"/([0-9a-z-]+\.)?[0-9a-z-]+\.[a-z]{2,7}/i",
            "mac"=>"/^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$/i",
            "mac2"=>"/^([0-9A-Fa-f]{2}){5}([0-9A-Fa-f]{2})$/"
        );


        Yaf\Registry::set("regrules",$rules);

    }
    protected function layout(){
        /**
         * 初始化layout
         *
         */
        //  $layout = new Layout(Yaf\Application::app()->getConfig()->application->layout->dir);
        // Yaf\Dispatcher::getInstance()->setView($layout);

    }
    protected function _initLogger()
    {

        $channel = $this->c['log']['channel'];
        $logger = new Monolog\Logger($this->c['log']['name']);
        $date=date("ymd");
        if ($channel == 'redis') {
            if (!file_exists('logs')) {
                mkdir('logs');
            }
            $redis=Misc_Utils::redis();
            $logger->pushHandler(new Monolog\Handler\RedisHandler($redis,  Logger::WARNING));

            Yaf\Registry::set("redis_logger", $logger);

        }
        else if ($channel == 'mongo' || empty($channel)) {


            $logger->pushHandler(new \Monolog\Handler\StreamHandler('logger_warn', Logger::WARNING));
            $logger->pushHandler(new \Monolog\Handler\StreamHandler('logger_info.txt', Logger::INFO));
            $logger->pushHandler(new \Monolog\Handler\StreamHandler('logger_err.txt', Logger::ERROR));


            Yaf\Registry::set("mongo_logger", $logger);
        }

        else if ($channel == 'file' || empty($channel)) {
            $logDir=$this->c->log->dir.'/';
            if (!file_exists($logDir)) {
                mkdir($logDir);
            }

            $logger->pushHandler(new \Monolog\Handler\StreamHandler($logDir . $date.'_debug.txt', Logger::DEBUG));
            $logger->pushHandler(new \Monolog\Handler\StreamHandler($logDir . $date.'_WARN.txt', Logger::WARNING));
            $logger->pushHandler(new \Monolog\Handler\StreamHandler($logDir . $date.'_info.txt', Logger::INFO));
            $logger->pushHandler(new \Monolog\Handler\StreamHandler($logDir . $date.'_err.txt', Logger::ERROR));


            Yaf\Registry::set("logger", $logger);


        }
        /**
         * register error handler
         */
        register_shutdown_function(function()use($logger,$date){


            $error = error_get_last();

            if( $error !== NULL && $error['type']!=8) {
                $error['server_info']=$this->getServerInfo();
                $error['user_ip']=$this->userIp();
                $content=print_r($error,true);
                $title="error:".$error['server_info'];
                $fileStats=stat($error['file']);
                $error['file']=str_replace(APP_PATH,'',$error['file']);

                // mail("ifa6@qq.com","Error:".$error['server_info'],$content);

                $logger->error("error",array('server'=>$_SERVER,'error'=>$error,'request'=>$_REQUEST));



//                $redis=Misc_Utils::redis();
//                $errorKey=$fileStats['mtime'].$fileStats['mtime'];
//                $redis->setex($errorKey,1500, 'value');
//                if(empty($_SESSION['last_error'])){
//
//                        Misc_Utils::sendWechatNotify($this->c->log->notify->wechat,"Error title",$error['file'],"error remark","http://www.fogpod.com/logs/err/date/{$date}");
//
//                }
//
//                if(!isset($_SESSION['last_error'][$error['file']])){
//                    Misc_Utils::sendWechatNotify($this->c->log->notify->wechat,"Error title",$error['file'],"error remark","http://www.fogpod.com/logs/err/date/{$date}");
//                }
//                if(count($_SESSION['last_error'])){
//                    $_SESSION['last_error'][$error['file']]='1';
//                }


                // var_dump(__DIR__,realpath('logs/'));
                //file_put_contents('logs/' . date("ymd") . '_err.txt',print_r($error,true));

            }
        });

    }
    function userIp()
    {
        $onlineip = '';
        if (getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), 'unknown')) {
            $onlineip = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), 'unknown')) {
            $onlineip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'), 'unknown')) {
            $onlineip = getenv('REMOTE_ADDR');
        } elseif (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown')) {
            $onlineip = $_SERVER['REMOTE_ADDR'];
        }
        return $onlineip;
    }
    function getServerInfo(){
        $handle = @popen('hostname --all-ip-addresses', 'r');

        return fread($handle, 4096);

    }
    protected function _initRedis(){

//        if(class_exists('Redis')){
//            $redis=new Redis();
//            $ret=$redis->pconnect($this->c['redis']['host'], $this->c['redis']['port'], $this->c['redis']['timeout']);
//            if($ret && !empty($this->c['redis']['pwd'])){
//                $authed=$redis->auth($this->c['redis']['pwd']);
//                if($authed)
//                {
//                    Yaf\Registry::set("redis", $redis);
//
//                }
//                if(strtolower(PHP_SAPI)=='cli'){
//
//                    $redis_sub=new Redis();
//
//                    $redis_sub->pconnect($this->c['redis']['host'], $this->c['redis']['port'], $this->c['redis']['timeout']);
//
//
//                    Yaf\Registry::set("redis_sub", $redis_sub);
//
//                }
//
//            }
//
//        }


    }
    protected  function _initMem()
    {
        /**
         * 初始化memcache
         */
        if(class_exists('Memcached')){
            $cache = StorageFactory::factory(array(
                /**
                 * config for twemproxy nutcracker
                 */
                'adapter' => array(
                    'name'     =>'memcached',
                    'lifetime' => 7200,
                    'options'  => array(
                        /** define key expire time **/
                        'ttl' => 3600,
                        'servers'   => array(
                            array(
                                '127.0.0.1',11211
                            )
                        ),
                        'namespace'  => $this->c->site->memcache_prefix,
//                    'liboptions' => array (
//                        'COMPRESSION' => true,
//                       /** 'binary_protocol' => true,**/
//                        'no_block' => true,
//                        'connect_timeout' => 3000
//                    )
                    )
                )
            ));
            Yaf\Registry::set("cache", $cache);
        }

//        $cache30 = StorageFactory::factory(array(
//            /**
//             * config for twemproxy nutcracker
//             */
//            'adapter' => array(
//                'name'     =>'memcached',
//                'lifetime' => 1800,
//                'options'  => array(
//                    'ttl' => 5,
//                    'servers'   => array(
//                        array(
//                            '127.0.0.1',22121
//                        )
//                    ),
//                    'namespace'  => 'my',
////                    'liboptions' => array (
////                        'COMPRESSION' => true,
////                       /** 'binary_protocol' => true,**/
////                        'no_block' => true,
////                        'connect_timeout' => 3000
////                    )
//                )
//            )
//        ));

//        Yaf\Registry::set("cache30", $cache30);
    }
    protected function  _initSession(){
        /**
         * 初始化session
         * session_write_close();
         * ini_set('session.auto_start', 0);         //关闭session自动启动
         * ini_set('session.cookie_lifetime', 0);    //设置session在浏览器关闭时失效
         * ini_set('session.gc_maxlifetime', 3600);  //session在浏览器未关闭时的持续存活时间
         * true  set httponly true to avoid cookie hijack
         */
        /**
         * session 在客户端存储的时间,如果和cookie设置一样的话,关闭浏览器就失效,对于chrome需要设置才能生效
         */

        //  ini_set('session.cookie_lifetime', 0);

        /**
         * session 在服务端存储的时间,如果和cookie设置一样的话,关闭浏览器就失效
         */
        //ini_set('session.gc_maxlifetime',1);
        /**
         * cookie httponly 存储
         */

        session_set_cookie_params(0, "/", "", FALSE, TRUE);
        //ini_set('session.gc_maxlifetime', 360);
        //expired after browser quit
        ini_set('session.cookie_lifetime', 0);
        // ini_set('session.gc_probability',1);
        // ini_set('session.gc_divisor',1);
        $cache=Yaf\Registry::get("cache");
        if($cache!==null){
            $session = new Cache($cache);
            session_set_save_handler(array(&$session,"open"),
                array(&$session,"close"),
                array(&$session,"read"),
                array(&$session,"write"),
                array(&$session,"destroy"),
                array(&$session,"gc"));
        }


    }

    protected  function smarty()
    {
        /**
         * 初始化smarty
         */
        $smartyCfg=Yaf\Registry::get("config")->get("smarty")->toArray();
        $siteTheme=Yaf\Registry::get("config")->get("site")->get("theme_dir");

        $smartyCfg['template_dir']= $smartyCfg['template_dir']."/".$siteTheme;

        $view = new Smarty_Adapter(APP_PATH.'/application/views/', $smartyCfg);
        $view->cache_lifetime = 1;
        $view->caching = 0;
        $view->minifyHtml = true;

        Yaf\Dispatcher::getInstance()->setView($view);
    }
    public function init(){
        //Yaf_Dispatcher::getInstance()->disableView();

    }
}
