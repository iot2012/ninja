

<?php
//$dom=new DOMDocument();
//$dom->loadHTMLFile("/Users/chjade/htdocs/fogpod/public/pm25.html");
//$nodes=$dom->getElementsByTagName("div");
//$nb = $nodes->length;
//foreach($dom->getElementsByTagName('div') as $link) {
//    # Show the <a href>
//    echo $link->getAttribute('class');
//    echo "<br />";
//}
//
//
//
//exit;




class AirCron extends RootCron {


    function fetchTemp($queryData,$cnt=3){
        //sample data
//        {
//            "query": {
//            "count": 1,
//  "created": "2015-03-24T08:31:17Z",
//  "lang": "en-US",
//  "diagnostics": {
//                "publiclyCallable": "true",
//   "url": {
//                    "execution-start-time": "1",
//    "execution-stop-time": "27",
//    "execution-time": "26",
//    "content": "http://weather.yahooapis.com/forecastrss?w=2502265"
//   },
//   "user-time": "28",
//   "service-time": "26",
//   "build-version": "0.2.438"
//  },
//  "results": {
//                "channel": {
//                    "atmosphere": {
//                        "humidity": "67",
//     "pressure": "30.31",
//     "rising": "2",
//     "visibility": "10"
//    },
//    "item": {
//                        "condition": {
//                            "code": "33",
//      "date": "Tue, 24 Mar 2015 12:56 am PDT",
//      "temp": "56",
//      "text": "Fair"
//     }
//    }
//   }
//  }
// }
        //https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%3D2502265&format=json&diagnostics=true&callback=

        $result=Misc_Utils::multiTry(function()use($queryData){
           // $sql=rawurlencode(sprintf("select item.condition,atmosphere from weather.forecast where woeid = %s",$weoId));

            $this->fetchYahooWeather($queryData['woeid']);

        });
        if($result[0]){
            return  $result[1];
        }
        return array('temp'=>0,'humidity'=>0);

    }

    function reserveIphone(){

        $z=new ZCurl();
        $z->addHeader(["Referer: https://reserve.cdn-apple.com/CN/zh_CN/reserve/iPhone/availability?channel=1&appleCare=N&iPP=N&partNumber=ML7K2CH/A&returnURL=http%3A%2F%2Fwww.apple.com%2Fcn%2Fshop%2Fbuy-iphone%2Fiphone6s%2F4.7-%E8%8B%B1%E5%AF%B8%E5%B1%8F%E5%B9%95-64gb-%E7%8E%AB%E7%91%B0%E9%87%91%E8%89%B2"]);
        $iphones=$z->getJson("https://reserve.cdn-apple.com/CN/zh_CN/reserve/iPhone/availability.json");

        $stores=$z->getJson("https://reserve.cdn-apple.com/CN/zh_CN/reserve/iPhone/stores.json");
        return array($iphones,$stores);
    }
    function defTbInfo(){

    }
    function fetchAll($queryData,$type='zhcn'){
        //return json_encode($this->hao123Weather($queryData['prov_cn'],$queryData['city_cn']),JSON_UNESCAPED_UNICODE);
       // Misc_Utils::rolling_curl();
        $ret=array();

        $pm25Url="http://www.pm25.in/".strtolower($queryData['city_en']);
        $pm2345Url="http://waptianqi.2345.com/air-{$queryData['c2345']}.htm";
        $airReqData=$this->genHao123ReqData($queryData['prov_cn'],$queryData['city_cn']);

        $result=Misc_Utils::rolling_curl(array('hao123.com'=>$airReqData,'2345.com'=>array("url"=>$pm2345Url)));
        if(!empty($result['hao123.com']['results']) && $result['hao123.com']['error']==""){
            $hao123=json_decode($result['hao123.com']['results'],true);
            $ret=$this->parseHao123Data($hao123,$type);
        }

        if(empty($ret['current'])) {
            Misc_Utils::log()->error("hao123 fetch Error",array($queryData['city_en']=>$result['hao123.com']['error']));

            $ret=Misc_Utils::multiTry(function() use($airReqData){

                $z=new ZCurl();
                $z->addHeader($airReqData['header']);
                $data=$z->getJson($airReqData['url']);
                file_put_contents('tianqi_log',json_encode($data)."\n",FILE_APPEND);
                $result=$this->parseHao123Data($data);

                if(!empty($result)){return $result;}
            });
            $ret=$ret[1];
        }
        if(!empty($result['2345.com']['results']) && $result['2345.com']['error']==""){
            $ret['current']['aqi']=$this->parse2345InData($result['2345.com']['results']);
        }
        if(empty($ret['current']['aqi']) || empty($ret['current']['aqi']['no2_1h']) || empty($ret['current']['aqi']['aqi']))
        {
            $log=Misc_Utils::log();
            $log->error("Pm25 fetch Error",array($queryData['city_en']=>$result['2345.com']['error']));

            $ret['current']['aqi']=Misc_Utils::multiTry(function() use($pm2345Url){
                $z=new ZCurl();
                $data=$z->get($pm2345Url,'plain');
                $result=$this->parse2345InData($data);
                if(!empty($result)){return $result;}
            });
            $ret['current']['aqi']=$ret['current']['aqi'][1];
        }

        return $ret;

    }
    function parse2345Ver2($data,$isWap=true) {



        phpQuery::newDocumentHTML($data);


        $pqAir = pq('.mainFomites .bmeta li');


        preg_match_all("/(\d+)μg/i",$pqAir->text(),$pqAir);


        $aqi=trim(pq(".power .progress .cur")->text());
        if(empty($aqi)){
            $aqi=trim(pq(".power .progress .cur")->text());
        }

        list($pm25_1h,$pm10_1h,$so2_1h,$no2_1h)=$pqAir[1];
        return compact('pm25_1h','pm10_1h','so2_1h','no2_1h','aqi');

    }
    function parse2345InData($data,$isWap=true){
        //$data=file_get_contents("http://tianqi.2345.com/air-58362.htm");



        if($isWap) {
            $data = mb_convert_encoding($data, 'utf-8', 'gb2312');
            return $this->parse2345Ver2($data);
        }

        return compact('pm25_1h','pm10_1h','so2_1h','no2_1h','aqi');



    }
    function parse2345V1($data,$isWap){

        if($isWap) {
            $data = mb_convert_encoding($data, 'utf-8', 'gb2312');
        }
        phpQuery::newDocumentHTML($data);

        if($isWap!==true){
            preg_match("/var\s+idx\s*=\s*[\"\']{1}(\d+)[\"\']{1}/i",$data,$ret);
            $aqi=$ret[1];
            $pqAir = pq('.ugm3 .value');
        } else {
            $pqAir = pq('.ugm3 .value');
            $aqi = pq('.num');
            if(!empty($aqi)) {$aqi=trim($aqi->text());}

        }


        //  $pm25 = pq('#name-pm2-5 .value');
        //   $pm10 = pq('#name-pm10 .value');

        preg_match_all("/(\d+)μg/i",$pqAir->text(),$pqAir);





        list($pm25_1h,$pm10_1h,$so2_1h,$no2_1h)=$pqAir[1];

        return compact('pm25_1h','pm10_1h','so2_1h','no2_1h','aqi');

    }
    function parsePm25InData($data){
        phpQuery::newDocumentHTML($data);

// Once the page is loaded, you can then make queries on whatever DOM is loaded.
// This example grabs the title of the currently loaded page.
        $airData = pq('.span12.data'); // in jQuery, this would return a jQuery object.  I'm guessing something similar is happening here with pq.



// You can then use any of the functionality available to that pq object.  Such as getting the innerHTML like I do here.

        $result=array_reverse(array_filter(explode("\n",strip_tags($airData->html())),'trim'));
        if(trim($result[0])=='其他城市'){
            array_shift($result);
        }
        $ret=array();
        foreach(range(0,(intval(count($result)/2)-1)) as $idx){
            if($result[$idx*2]!=''){
                $ret[str_replace(array("/",'.'),array("_",''),strtolower(trim($result[$idx*2])))]=trim($result[$idx*2+1]);

            }
        }

        return $ret;

    }

    function fetchPm25($id,$city='',$cnt=3){
        $z=new ZCurl(true,'pm');
        $cookie='Cookie:BDRCVFR[feWj1Vr5u3D]=I67x6TjHwwYf0; BDUSS=9ybVBJfkdiLWZ6WHU4V0xZeEdETUE2Tmd4REtoS05TU0d0cm9IWlJKUHQyaVJWQVFBQUFBJCQAAAAAAAAAAAEAAACGzvArYmFvZGlhb3F1AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAO1N~VTtTf1URW; BDRCVFR[FhauBQh29_R]=mbxnW11j9Dfmh7GuZR8mvqV; shifen[177028867_78264]=1426138382; BIDUPSID=0BEF699F6E76D4372898AA5FD71CCBA6; shifen[11230380870_40168]=1426656809; shifen[4759450324_58409]=1426656811; shifen[31313665_78798]=1427159639; BD_HOME=1; H_PS_PSSID=13045_1436_12669_12826_12952_12575_12692_12693_13035_12723_12797_13018_13015_12795_8498; BD_UPN=123253';

        $result=Misc_Utils::multiTry(function() use($z,$cookie,$id){
            $z->addHeader(array($cookie));
            $data=$z->getJson("https://www.baidu.com/home/xman/data/superload?id={$id}&_req_seqid=0x85dc2f500000a7c2");
            if(is_array($data) && array_key_exists('data',$data)){
                $result=$data['data']['weather']['content']['today'];
                return array('aqi'=>intval($result['pm25']),'wind'=>$result['wind'],'pollution'=>intval($result['pollution']));
            }
        });
        if($result[0]){
            return  $result[1];
        } else {
            $result2=Misc_Utils::multiTry(function() use($city){
                $pm = new Aqi(new AqiIn());
                return $pm->getPm25($city);
            });
            if($result2[0]){
                return  $result2[1];
            }
        }

        return array('pm25'=>0,'aqi'=>0);


    }
    function gen2345ReqData($code2345=''){
        return array('url'=>"http://tianqi.2345.com/air-$code2345.htm",'headers'=>array());
    }
    function genHao123ReqData($prov,$city){
        $loc2=urlencode("2|$prov|$city");
        //$hbc=urlencode('[{"province":"北京","city":"北京","town":"北京"},{"province":"上海","city":"上海","town":"上海"}]');

        $BAIDUID=strtoupper(md5(time()));
        $lpvt=strtoupper(md5(time()+1));
        $lpvtTime=time();
        $lpvtTime1=$lpvtTime+rand(0,100);
        $lpvtTime2=$lpvtTime+rand(0,100);
        $lpvtTime3=$lpvtTime+rand(0,100);
        $lpvtTime4=$lpvtTime+rand(0,100);
        $time=$lpvtTime*1000+rand(1,1000);

        $weatherCode= json_decode('{"10":"暴雨","11":"大暴雨","12":"特大暴雨","13":"阵雪","14":"小雪","15":"中雪","16":"大雪","17":"暴雪","18":"雾","19":"冻雨","20":"沙尘暴","21":"小到中雨","22":"中到大雨","23":"大到暴雨","24":"暴雨到大暴雨","25":"大暴雨到特大暴雨","26":"小到中雪","27":"中到大雪","28":"大到暴雪","29":"浮尘","30":"扬沙","31":"强沙尘暴","53":"霾","99":"无","00":"晴","001":"晴(n)","01":"多云","011":"多云(n)","02":"阴","03":"阵雨","04":"雷阵雨","05":"雷阵雨伴有冰雹","06":"雨夹雪","07":"小雨","08":"中雨","09":"大雨"}',true);

        $windDir=explode("|","|东北风|东风|东南风|南风|西南风|西风|西北风|北风|旋转风");
        $windPower=explode("|","微风|3-4|4-5|5-6|6-7|7-8|8-9|9-10|10-11|11-12");
        $headers="Cookie: BAIDUID=$BAIDUID:FG=1; _H_CS_C=412983998; bdshare_firstime=$time; Hm_lvt_$lpvt=$lpvtTime; Hm_lpvt_$lpvt=$lpvtTime; scrollflag=$time; FLASHID=F9ED38240A576F36C180DE77BE02A11F:FG=1; static=1; mtip=1; Hm_lvt_$lpvt=$lpvtTime1,$lpvtTime2,$lpvtTime3,$lpvtTime4; Hm_lpvt_$lpvt=$lpvtTime4; loc2=$loc2";

        return array('url'=>"http://tianqi.hao123.com/wData?t=$time&_=$time",'headers'=>array($headers));


    }
    function parseHao123Data($result,$type='zhcn'){
//        $weatherCode= json_decode('{"10":"暴雨","11":"大暴雨","12":"特大暴雨","13":"阵雪","14":"小雪","15":"中雪","16":"大雪","17":"暴雪","18":"雾","19":"冻雨","20":"沙尘暴","21":"小到中雨","22":"中到大雨","23":"大到暴雨","24":"暴雨到大暴雨","25":"大暴雨到特大暴雨","26":"小到中雪","27":"中到大雪","28":"大到暴雪","29":"浮尘","30":"扬沙","31":"强沙尘暴","53":"霾","99":"无","00":"晴","001":"晴","01":"多云","011":"多云","02":"阴","03":"阵雨","04":"雷阵雨","05":"雷阵雨伴有冰雹","06":"雨夹雪","07":"小雨","08":"中雨","09":"大雨"}',true);

        $weatherCode= json_decode('{"10":"暴雨","11":"大暴雨","12":"特大暴雨","13":"阵雪","14":"小雪","15":"中雪","16":"大雪","17":"暴雪","18":"雾","19":"冻雨","20":"沙尘暴","21":"小到中雨","22":"中到大雨","23":"大到暴雨","24":"大暴雨","25":"大暴雨","26":"小到中雪","27":"中到大雪","28":"大到暴雪","29":"浮尘","30":"扬沙","31":"强沙尘暴","53":"霾","99":"无","00":"晴","001":"晴","01":"多云","011":"多云","02":"阴","03":"阵雨","04":"雷阵雨","05":"雷阵雨","06":"雨夹雪","07":"小雨","08":"中雨","09":"大雨"}',true);

        $windDir=explode("|","|东北风|东风|东南风|南风|西南风|西风|西北风|北风|旋转风");
        $windPower=explode("|","微风|3-4|4-5|5-6|6-7|7-8|8-9|9-10|10-11|11-12");
        $forcastWeather=array();
        if(isset($result['forecast5d']['f']['f1'])) {
            $forcast5d = $result['forecast5d']['f']['f1'];

            foreach ($forcast5d as $key => $forcast) {
                $forcastWeather['forcast5d'][$key]['day_cond_py'] = Misc_Utils::pinyin2($weatherCode[$forcast['fa']]);
                $forcastWeather['forcast5d'][$key]['night_cond_py'] = Misc_Utils::pinyin2($weatherCode[$forcast['fb']]);
                $forcastWeather['forcast5d'][$key]['day_cond'] = $weatherCode[$forcast['fa']];
                $forcastWeather['forcast5d'][$key]['night_cond'] = $weatherCode[$forcast['fb']];
                $forcastWeather['forcast5d'][$key]['day_temp'] = $forcast['fc'];
                $forcastWeather['forcast5d'][$key]['night_temp'] = $forcast['fd'];
                $forcastWeather['forcast5d'][$key]['day_wind_dir'] = $windDir[$forcast['fe']];
                $forcastWeather['forcast5d'][$key]['night_wind_dir'] = ($forcast['ff'] == 0 ? $windDir[$forcast['fe']] : $windDir[$forcast['ff']]);
                $forcastWeather['forcast5d'][$key]['day_wind_power'] = $windPower[$forcast['fg']];
                $forcastWeather['forcast5d'][$key]['night_wind_power'] = ($forcast['fh'] == 0 ? $windPower[$forcast['fg']] : $windPower[$forcast['fh']]);
                $data = explode("|", $forcast['fi']);
                $forcastWeather['forcast5d'][$key]['sunrise'] = $data[0];
                $forcastWeather['forcast5d'][$key]['sunset'] = $data[1];
            }
            unset($result['aqi']['showurl'], $result['aqi']['url'], $result['aqi']['title']);

            $forcastWeather['current']['aqi'] = $result['aqi'];
            $forcastWeather['current']['temp'] = $result['observe']['l']['l1'];
            $forcastWeather['current']['humidity'] = $result['observe']['l']['l2'];
            $forcastWeather['current']['wind_dir'] = $windDir[$result['observe']['l']['l4']];
            $forcastWeather['current']['wind_power'] = $result['observe']['l']['l3'];
            $forcastWeather['current']['time'] = $result['observe']['l']['l7'];

            return $forcastWeather;
        }
        return array();
    }
    function hao123Weather($prov,$city){
        $z=new ZCurl();

             //$hbc=urlencode('[{"province":"北京","city":"北京","town":"北京"},{"province":"上海","city":"上海","town":"上海"}]');

        $data=$this->genHao123ReqData($prov,$city);

        $z->addHeader($data['headers']);

        $result=$z->getJson($data['url']);
        return $this->parseHao123Data($result);


    }
    function fetchYahooWeather($woeId){
        $weatherUrl="https://query.yahooapis.com/v1/public/yql?q=select%20item.condition%2Catmosphere%20from%20weather.forecast%20where%20woeid%20%3D%20#woeid#&format=json&diagnostics=true&callback=";

        $data=$this->z->getJson(str_replace("#woeid#",$woeId,$weatherUrl));
        if(is_array($data) && array_key_exists('query',$data)){
            $result=$data['query']['results']['channel'];
            $temp=(($result['item']['condition']['temp']-32)/1.8);
            return array('humidity'=>floatval($result['atmosphere']['humidity']),'temp'=>round($temp,1));

        } else {
            return false;
            echo("fetch temp error tempid $woeId\n");
        }
    }

}


