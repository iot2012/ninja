<?php
/**
 * Class PasswordException
 * 生成password的hash以及校验
 */
class PasswordException extends Exception {}

class Misc_Password
{
    static function genAppId($prefix,$length = 32, $strong = false)
    {
        if (function_exists('openssl_random_pseudo_bytes')) {
            $token = bin2hex(openssl_random_pseudo_bytes(128, $strong));
            return $prefix.substr($token, 0, $length);

        }
        /**
         * fallback to mt_rand if php < 5.3 or no openssl available
         */

        $characters = '0123456789';
        $characters .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz/+';
        $charactersLength = strlen($characters) - 1;
        $token = '';

        //select some random characters
        for ($i = 0; $i < $length; $i++) {
            $token .= $characters[mt_rand(0, $charactersLength)];
        }

        return $prefix.$token;
    }

    static function createIv($len=22){
        return mcrypt_create_iv($len, MCRYPT_DEV_URANDOM);
    }
    static function genAccessToken($appId,$secret){
        $options = [
            'cost' => 12,
            'salt' => $secret
        ];
        return password_hash($appId, PASSWORD_BCRYPT, $options);
    }
    static function verifyAToken($pwd,$salt){
        return password_verify($pwd,$salt);
    }
    static function genAppIdSecret($prefix='',$appIdLength=12,$secretLength=22){
        $secret=base64_encode(mcrypt_create_iv($secretLength, MCRYPT_DEV_URANDOM));
        $appId=self::genAppId($prefix,$appIdLength);
        return array($appId,$secret);
    }
    protected static function _generatePasswordHashValue($version, $mode, $salt, $password)
    {
        if ($version != 1) {
            throw new PasswordException("Version '$version' not supported");
        }
        switch ($mode) {
            case 'sha256':
                return hash('sha256', $salt . $password);
            default:
                throw new PasswordException("Mode '$mode' not known");
        }
    }
    static function gen_appid($len = 32, $prefix)
    {
        //$token = bin2hex(mcrypt_create_iv(128, MCRYPT_DEV_RANDOM));
        /**
         *  tend to generate predictable values
         *  $token = md5(uniqid(rand(), true));
         *
         * $token = hash_hmac(
         *           'sha512',
         *            openssl_random_pseudo_bytes(32),
         *            openssl_random_pseudo_bytes(16)
         *        );
         */

        return crc32(base64_encode(openssl_random_pseudo_bytes($len)));

    }
    public static function check($password, $hash)
    {
        $hashParts = explode('/', $hash);
        if (count($hashParts) != 4) {
            throw new PasswordException("Invalid password hash");
        }
        list($version, $mode, $salt, $hashValue) = $hashParts;
        $hashValueCheck = self::_generatePasswordHashValue($version, $mode, $salt, $password);
        return ($hashValueCheck == $hashValue);
    }

    public static function genHash($password,$isRetArr=false, $options = array())
    {
        $options = array_merge(array(
            'mode' => 'sha256',
        ));
        $version = 1; // For future use
        $mode = strtolower($options['mode']);
        $salt = self::genSalt(8);
        $hash = self::_generatePasswordHashValue($version, $mode, $salt, $password);
        if($isRetArr){
            return array($version,$mode,$salt,$hash);
        }
        return "$version/$mode/$salt/$hash";
    }

    public static function genSalt($length)
    {
        $chars = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9');
        return implode('', array_rand(array_flip($chars), $length));
    }

}
