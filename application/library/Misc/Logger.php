<?php
/**
 * Class Misc_Logger
 * 记录操作的类,用来调试和收集数据
 */
class Misc_Logger {
    protected $f;
    protected $data=array();
    protected $sec_char="=============================================================";
    protected $var_char="";
    protected $record_char=" --> ";
    protected $line_char="\n";
    protected $end_line_char="\n\n";
    function __construct($file){

          $this->f=$file;
    }

    function log_gp(){
        $this->logg();
        $this->loggp();

    }
    function logg(){
        $date=date("Y-m-d H:i:s")." - ";
        $this->data[]=$date."GET:{$this->record_char}".print_r($_GET,true);

    }
    function logc(){
        $date=date("Y-m-d H:i:s")." - ";
        $this->data[]=$date."GET:{$this->record_char}".print_r($_COOKIE,true);

    }
    function log_gpc(){
        $this->logg();
        $this->logp();
        $this->logc();


    }
    function p(){
        $date=date("Y-m-d H:i:s")." - ";
        $this->data[]=$date."POST:{$this->record_char}".print_r($_POST,true);

    }
    function r(){
        $date=date("Y-m-d H:i:s")." - ";

        $this->data[]=$date."Request{$this->record_char}\n".print_r($_REQUEST,true);

    }
    function all()
    {
        $date=date("Y-m-d H:i:s")." - ";

        $this->data[]=$date."Global{$this->record_char}".print_r($GLOBALS,true);
    }
    function raw(){

        $date=date("Y-m-d H:i:s")." - ";

        $this->data[]=$date."Raw Request{$this->record_char}".file_get_contents("php://input");
    }
    function log($type,$data,$isStore=true){

        $date=date("Y-m-d H:i:s")." - ";

        $this->data[]=$date."$type{$this->record_char}".print_r($data,true);

        if($isStore){
            $this->data[]=array();
        }
        $data=implode("\n",$this->data).$this->end_line_char;

        file_put_contents($this->f,$data,FILE_APPEND);
    }

    function __destruct() {
        $this->data[]=$this->sec_char;
        $data=implode("\n",array_values($this->data)).$this->end_line_char;
        file_put_contents($this->f,$data,FILE_APPEND);
    }
    function info($str,$ret=false){
       $this->log("INFO",$str,$ret);

    }
    function flush(){
        if(!empty($this->data)){

            $data=implode("\n",array_values($this->data));
            $this->data=array();
            file_put_contents($this->f,$data,FILE_APPEND);
        }


    }
    function err($str){
        $this->log("ERROR",$str);

        $this->data[]=$this->sec_char;
        $data=implode("\n",array_values($this->data)).$this->end_line_char;

        $this->data=array();
        file_put_contents($this->f,$data,FILE_APPEND);

    }
    function warn($str,$ret=false){
        $this->log("WARN",$str,$ret);

    }
    function debug($str,$ret=false){
        $this->log("DEBUG",$str,$ret);

    }


}

