<?php
class #modelName#Model extends SysModel {


    static $_tbName = '#tbName#';
    static $_primary = '#primaryId#';



    protected $_tbMeta=#tbMeta#;

    function __construct(){

        parent::__construct();

    }

    function exists($data){

        return  $this->findOne($data);

    }


}