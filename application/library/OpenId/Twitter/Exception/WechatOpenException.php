<?php

/**
 * TwitterOAuth - https://github.com/ricardoper/TwitterOAuth
 * PHP library to communicate with Twitter OAuth API version 1.1
 *
 * @author Dirk Luijk <dirk@luijkwebcreations.nl>
 * @copyright 2013
 */

namespace  OpenId\Twitter\Exception;

class WechatOpenException extends \Exception
{
    public function __toString()
    {
        return str_replace("Exception",'',__CLASS___)." API Response: [{$this->code}] {$this->message} (" . __CLASS__ . ") ";
    }
}
