<?php
namespace Nowcn;

use Nowcn\XMLClient;

class Sms extends XMLClient
{
	private $ConfNull="1";

    function __construct($vcpserver,$vcpuser,$vcppassword,$vcpserverport){

        $this->serverURL=($vcpserver.":".$vcpserverport);
        $this->XMLType="SMS";
        $this->VCP=$vcpuser;
        $this->VCPPassword=$vcppassword;

    }

	function sendSMS($mobile, $msg, $time="", $apitype=0)
	{

		$xml_command="<action>SMS:sendSMS</action>
						<sms:mobile>$mobile</sms:mobile>
						<sms:message>".base64_encode($msg)."</sms:message>
						<sms:datetime>$time</sms:datetime>
						<sms:apitype>$apitype</sms:apitype>";
		$this->sendSCPData($this->serverURL, $xml_command);
		$this->toPlain();

		return $this->responseXML;
	}
	function infoSMSAccount()
	{
		$xml_command="<action>SMS:infoSMSAccount</action>";
		$this->sendSCPData($this->serverURL, $xml_command);
		$this->toPlain();
		return $this->responseXML;
	}
	function readSMS()
	{
		$xml_command="<action>SMS:readSMS</action>";
		$this->sendSCPData($this->serverURL, $xml_command);
		$this->toPlain();
		return $this->responseXML;
	}

	function updateConf($username, $pass, $server){
		if(!file_exists("api/config.inc.php")) return "???smsdemo/api/config.inc.php ???????";
		if(!is_writable("api/config.inc.php")) return "???smsdemo/api/config.inc.php ???????????????????";
		$fd = fopen("api/config.inc.php","w");
		if(!$fd) return "???smsdemo/api/config.inc.php ???, ????????????";
		$line = '<? '."\n".
				'/** '."\n".
				' * ??????????? '.
				' * $'."vcpserver		SCP?????????????????????testxml.todaynic.com????????????sms.todaynic.com "."\n".
				' * $'."vcpserverport	SCP???????????????????????????????????20002 "."\n".
				' * $'."vcpsuser		??????????????????????????? "."\n".
				' * $'."vcppassword		 ?????????????????????????????????? "."\n".
				' * '."\n".
				' * www.now.cn,Inc. http://www.now.cn '."\n".
				'**/ '."\n".
				'$'.'vcpserver="'.$server.'"; '."\n".
				'$'.'vcpserverport="20002"; '."\n".
				'$'.'vcpuser="'.$username.'"; '."\n".
				'$'.'vcppassword="'.$pass.'"; '."\n".
				'?> '."\n";
		if(fwrite($fd, $line)===FALSE) return "???smsdemo/api/config.inc.php ???????????????????";
		fclose($fd);
		return "1";
	}
}
