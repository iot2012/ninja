

<?php
//$dom=new DOMDocument();
//$dom->loadHTMLFile("/Users/chjade/htdocs/fogpod/public/pm25.html");
//$nodes=$dom->getElementsByTagName("div");
//$nb = $nodes->length;
//foreach($dom->getElementsByTagName('div') as $link) {
//    # Show the <a href>
//    echo $link->getAttribute('class');
//    echo "<br />";
//}
//
//
//
//exit;


class RootCron  {
    protected $z;
    function __construct($httpObj='',$options=['en_cookie'=>false,'cookie_file'=>'cookieFile']){

        $this->z=$httpObj;

    }
    protected $_extractOpts=[];

    /**
     * @param $data
     * @return array|phpQueryObject|QueryTemplatesParse|QueryTemplatesSource|QueryTemplatesSourceQuery context
     */
    function initDom($data){

       phpQuery::newDocumentHTML($data);

    }
    function extractData($extractOpts,$source){
        $result=[];
        $this->initDom($source);
        $this->mkRecord($extractOpts,$source,$result);
        return $result;
    }
    function saveRecord($m,$record){
        $m->add($record);
    }

    function mkRecord($extractOpts,$source,&$result,$level=1){
        foreach($extractOpts as $key=>$val){

            if(isset($val['_sel'])){
                if($val['_sel'][0]=='/'){
                    preg_match($val['_sel'],$source,$match);
                    if(is_array($match) && $level>=2){
                        $result[$key]=$match;
                    }else {
                        $result[$key]=$match;
                    }



                } else {
                        $size=pq($val['jq'])->size();
                        if($level==1 && $size>1){

                                $result[$key]=array_map(function($idx) use($val){
                                    if(!empty($val['attr'])) {
                                        return pq($val['jq'])->eq($idx)->attr($val['attr']);
                                    }else {
                                        return trim(pq($val['_sel'])->text());
                                    }

                                },range(0,$size));

                        } else if($level>1 && $size>1) {
                            /**
                             *  例如获取<img src="www"> <span>text</span>
                             *  返回记录时
                             *  [[src=>"",img=>""]]
                             */
                                $result=array_map(function($idx) use($val,$key,$result){
                                    if(empty($result[$idx])){
                                        $result[$idx]=[];
                                    }
                                    if(!empty($val['attr'])) {
                                        $result[$idx][$key]=pq($val['jq'])->eq($idx)->attr($val['attr']);
                                    }else {
                                        $result[$idx][$key]=trim(pq($val['_sel'])->text());
                                    }
                                    return $result[$idx];
                                },range(0,$size));

                        } else {
                            if(!empty($val['attr'])) {
                                $result[$key]=pq($val['jq'])->attr($val['attr']);
                            }else {
                                $result[$key]=trim(pq($val['_sel'])->text());
                            }

                        }




                }
                if(!empty($val['downloadDir'])){
                    $result[$key]=$this->download($result[$key],$val['downloadDir']);
                }


            } else {
                $result[$key]=isset($result[$key])?$result[$key]:[];
                $level++;
                $this->mkRecord($val,$source,$result[$key],$level);

            }

        }

        return $result;
    }

    function download($url,$dir='.',$prefix='img_',$cnt=3,$retryMin=1e6,$retryMax=5e6){
        if("."==$dir){
            $dir="./".date("ymd");
            $dir=tempnam($dir,$prefix);
        }

        foreach(range(1,$cnt) as $item){
            try {
                $c=file_get_contents($url);

                if(!empty($c)){
                    $mime=$this->getStrMime($c);
                    $fileName=$dir.$this->getExtFromMime($mime);
                    file_put_contents($fileName,$c);
                    return $fileName;
                    break;
                }

            } catch(Exception $ex){
                $sleepSec=rand($retryMin,$retryMax);
                $exMsg=$ex->getMessage();
                usleep($sleepSec);
            }
        }
        return $url;


    }
    function getExtFromMime($mime){
        $mimes = array (
            'application/json' => 'json',
            'application/zip' => 'zip',
            'text/xml' => 'xml',
            'text/html' => 'shtml',
            'image/gif' => 'gif',
            'image/png' => 'png',
            'image/jpeg' => 'jpeg',
            'application/pdf' => 'pdf',
            'video/x-flv ' => 'flv',
            'application/msexcel' => 'xls',
            'application/vnd.ms-powerpoint' => 'ppt',
            'application/vnd.wap.wmlc' => 'wmlc',
            'audio/midi' => 'mid',
            'application/rtf' => 'rtf',
            'image/tiff' => 'tif',
            'image/x-icon' => 'ico',
            'image/x-ms-bmp' => 'bmp',
            'image/webp' => 'webp',
            'image/vnd.wap.wbmp' => 'wbmp',
            'image/x-jng' => 'jng',
            'text/comma-separated-values' => 'csv',
            'application/msword' => 'doc',
        );

        if(isset($mimes[$mime])){
          return ".".$mimes[$mime];
        } else {
            if(preg_match("/^\w+\/[a-z0-9]+$/",$mimes[$mime])){
                $exts=explode("/",$mimes[$mime]);
                return ".".$exts[1];
            }
        }
        return '';

    }
    function getStrMime($buffer) {
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        return $finfo->buffer($buffer);
    }




}


