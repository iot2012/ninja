<?php

namespace Ali\OSS;
class OSSException extends Exception {

    function __construct($lang){

        if(function_exists('get_loaded_extensions')){
            //检测curl扩展
            $extensions = get_loaded_extensions();
            if($extensions){
                if(!in_array('curl', $extensions)){
                    throw new parent(OSS_CURL_EXTENSION_MUST_BE_LOAD);
                }
            }else{
                throw new parent(OSS_NO_ANY_EXTENSIONS_LOADED);
            }
        }else{
            throw new parent('Function get_loaded_extensions has been disabled,Pls check php config.');
        }

    }


}


//检测get_loaded_extensions函数是否被禁用。由于有些版本把该函数禁用了，所以先检测该函数是否存在。

