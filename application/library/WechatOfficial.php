
<?php

include "Wechat/wxBizMsgCrypt.php";


class WechatOfficial
{
    const IOT_API_PREFIX = "https://api.weixin.qq.com";
    const DEVICEID_GET_URL = "/device/getqrcode";
    const AUTHORIZE_DEVICE_URL = "/device/authorize_device";
    const AUTH_URL = "/cgi-bin/login";
    const HOST = "mp.weixin.qq.com";
    const PROTOCOL = "https:";
    const ORIGIN_URL = "https://mp.weixin.qq.com";
    const DEVICE_UNBIND_URL = "/device/unbind";
    const LOGIN_REFERER = "https://mp.weixin.qq.com/";
    private $options;
    private $crypt;
    private $rawMessage;
    public $appId;
    private $redis;
    private $pre="wc_";
    /**
     * pre_auth_code 有效期是20分钟
     */
    private $ticketTimeout=600;
    const API_QUERY_AUTH_URL="/cgi-bin/component/api_query_auth";
    const PRE_AUTH_CODE_URL="/cgi-bin/component/api_create_preauthcode";
    const API_GET_AUTHORIZER_INFO="/cgi-bin/component/api_get_authorizer_info";
    const COMP_ACCESS_TOKEN="/cgi-bin/component/api_component_token";
    const AUORIZER_TOKEN_URL="/cgi-bin/component/api_authorizer_token";
    const POI_LIST_URL="/cgi-bin/poi/getpoilist";
    const USER_INFO_URL="/cgi-bin/user/info";
    const SHOP_LIST="/bizwifi/shop/list";

    const DEVICE_ADD="/bizwifi/device/add";
    const CONNECT_URL="/bizwifi/account/get_connecturl";
     //   https://api.weixin.qq.com/cgi-bin/poi/getpoilist?access_token=TOKEN
    private $pwd;
    private $curl;
    private $authorizer_access_token;
    //https://mp.weixin.qq.com/cgi-bin/componentloginpage?component_appid=xxxx&pre_auth_code=xxxxx&redirect_uri=xxxx
    //sample request 微信发送ticket数据
    //signature=74a040d8ed424a2b2ecdf424eacda796f21b6957&timestamp=1440574838&nonce=1536866704&encrypt_type=aes&msg_signature=4b19f99e4a14eb4e6a0adf5b31926fe77e2594e0
    function poiList($token,$begin=0,$limit=50){
        $z=new ZCurl();
        $url=self::IOT_API_PREFIX.self::POI_LIST_URL."?access_token=".$token;
        $data=$z->jsonPost($url,array(
            'begin'=>$begin,
            'limit'=>$limit
        ));
        return $data;

    }

    /**
     * 添加wifi设备
     * @param $token
     * @param $data 数组形式[
                    "shop_id"=> 429620,
                    "ssid"=> "WX123",
                    "password"=> "12345689",
                    "bssid"=> "00:1f:7a:ad:5c:a8"
                ]
     * @return mixed
     */
    function wifi_device_add($token,$shopId,$ssid,$pwd,$bssid='00:1f:7a:ad:23:89'){

        $result = $this->curl->jsonPost(self::IOT_API_PREFIX.self::DEVICE_ADD.'?access_token='.$token,[
            'shop_id'=>$shopId,'ssid'=>$ssid,'password'=>$pwd,'bssid'=>$bssid
        ]);
        return $result;

    }

    /**
     * 门店列表
     * @param $token
     * @param int $page
     * @param int $size
     * @return mixed
     */
     function shop_list($token,$page=1,$size=20){
         $result = $this->curl->jsonPost(self::IOT_API_PREFIX.self::SHOP_LIST.'?access_token='.$token,[
             'pageindex'=>$page,
             'pagesize'=>$size

         ]);
         return $result;

    }
    function wechatIp($token){
        $result = $this->curl->getJson("https://api.weixin.qq.com/cgi-bin/getcallbackip".'?access_token='.$token);
        return $result;
    }
    function getUserInfo($openId,$token){


        $result = $this->curl->getJson(self::IOT_API_PREFIX.self::USER_INFO_URL.'?access_token='.$token.'&openid='.$openId);
        if (!empty($result))
        {
            if (isset($result['errcode'])) {
                $this->errCode = $result['errcode'];
                $this->errMsg = $result['errmsg'];
                return $result;
            }
            return $result;
        }
        return false;
    }
    function authorizer_account_info($uid,$authorizer_appid,$component_access_token=''){

        $component_access_token=$this->component_access_token('', $this->redis);
        if(!empty($component_access_token)){
            $z=new ZCurl();
            $data=$z->jsonPost(self::IOT_API_PREFIX.self::API_GET_AUTHORIZER_INFO."?component_access_token=".$component_access_token,array(
                'component_appid'=>$this->options['appid'],
                    'authorizer_appid'=>$authorizer_appid

            ));
            if(!empty($data['authorizer_info'])){
                $this->updateAuthInfo($uid,$authorizer_appid,$data);
                return $data;
            }
        }


        return false;
    }
    function wifi_connect_url($token){
        $result = $this->curl->getJson(self::IOT_API_PREFIX.self::CONNECT_URL.'?access_token='.$token);
        return $result;
    }

    function accountInfo($uid,$authAppId,$appId){
        $model=new WechatOfficialInfoMModel();
        return $model->findOne(['uid'=>$uid,'authorizer_appid'=>$authAppId,'appid'=>$appId]);
    }
    function authToken($uid){
        $model=new WechatOfficialInfoMModel();
        return $model->findOne(array('appid'=>$this->options['appid'],'uid'=>$uid),array('authorizer_access_token','authorizer_refresh_token','authorizer_appid','authorization_info.authorizer_appid'));
    }
    function updateAuthToken($uid,$data){
        $model=new WechatOfficialInfoMModel();
        $query=array('appid'=>$this->options['appid'],"authorizer_appid"=>$data['authorization_info']['authorizer_appid'],'uid'=>$uid);
        $data=array('$set'=>array('authorizer_access_token'=>$data['authorization_info']['authorizer_access_token'],'authorizer_refresh_token'=>$data['authorization_info']['authorizer_refresh_token'],'appid'=>$this->options['appid'],"authorizer_appid"=>$data['authorization_info']['authorizer_appid'],'uid'=>$uid,'ctime'=>time()));

        $model->update2($data,$query);
    }
    function updateAuthInfo($uid,$authorizer_appid,$data){
        $model=new WechatOfficialInfoMModel();
        $query=array('uid'=>$uid,'appid'=>$this->options['appid'],'authorizer_appid'=>$authorizer_appid);
        $data['appid']=$this->options['appid'];
        $data['uid']=$uid;
        $data['authorizer_appid']=$authorizer_appid;
        $data['ctime']=time();
        $data=['$set'=>$data];
        $model->update2($data,$query);
    }
    function component_access_token($ticket='',$redis=''){

        $component_access_token=$this->redis->hGet($this->pre.$this->options['appid'],"component_access_token");
        if(empty($component_access_token)){
            $verifyTicket=$this->verifyTicket($this->options['appid'],$this->redis);
            if(!empty($verifyTicket)){
                $component_access_token=$this->compAccessToken($this->options['appid'],$this->options['appsecret'],$verifyTicket);
                if(empty($component_access_token)){
                    return false;
                }
            }
        }
        return $component_access_token;
    }
    function array_pluck($array, $fields)
    {

        if (is_string($fields)) {
            $fields = explode(",", $fields);

        }
        $fields = array_fill_keys($fields, 1);

        return array_intersect_key($array, $fields);

    }
    function __construct($options,$redis='')
    {

        $this->curl=new ZCurl();
        $this->options=$options;
        $this->redis=$redis;
        if(empty($this->redis)){
            $this->redis=Misc_Utils::redis();
        }
        $this->crypt=new WXBizMsgCrypt($this->options['val_token'], $this->options['enc_dec_key'], $this->options['appid']);



//        openid.wechat_open.gogo.val_token=1a2242de0e8c13467bc142f268250e4b
//openid.wechat_open.gogo.enc_dec_key=906f1a2242de08250e4be8c1346706fcc54bc142f26
//openid.wechat_open.gogo.accept_msg_url='http://www.gogoinfo.cn/callback/wechat_offical/$APPID$'
//openid.wechat_open.gogo.accept_auth_url=http://www.gogoinfo.cn/callback/wechat_offical
//
//openid.wechat_open.gogo.appid=wx692c3b311f7101d1
//openid.wechat_open.gogo.appsecret=0c79e1fa963cd80cc0be99b20a18faeb
        //$pc = new WXBizMsgCrypt($token, $encodingAesKey, $appId);


    }

    function authorization_info($uid,$authCode){

            $authorization_info=$this->authToken($uid);
            if(empty($authorization_info['authorizer_access_token'])){
                $z=new ZCurl();
                $data=$z->jsonPost($this->api_auth_url()."?component_access_token=".$this->component_access_token(),array(
                    'component_appid'=>$this->appId(),"authorization_code"=>$authCode
                ));
                if(!empty($data['authorization_info'])){

                    $this->updateAuthToken($uid,$data);
                    return ['authorizer_appid'=>$data['authorizer_appid'],'authorizer_access_token'=>$data['authorization_info']['authorizer_access_token'],'authorizer_refresh_token'=>$data['authorization_info']['authorizer_refresh_token']];
                }
                return false;

            }
            return $authorization_info;

    }
    function api_auth_url(){
        return self::IOT_API_PREFIX.self::API_QUERY_AUTH_URL;
    }
    function appId(){
        return $this->options['appid'];
    }
    function preAuthCodeUrl($token){
        return self::IOT_API_PREFIX.self::PRE_AUTH_CODE_URL."?component_access_token=".$token;
    }
    function setVerifyTicket($ticket){


        $this->redis->hSet($this->pre.$this->options['appid'],'verify_ticket',$ticket);


        $this->redis->expire($this->pre.$this->options['appid'],$this->ticketTimeout);
    }
    function compAccessTokenUrl(){

        return self::IOT_API_PREFIX.self::COMP_ACCESS_TOKEN;
    }
    function verifyTicket($appId){

        return $this->redis->hGet($this->pre.$this->options['appid'],'verify_ticket');
    }
    function authorizer_token_url($component_access_token){

        return self::IOT_API_PREFIX.self::AUORIZER_TOKEN_URL."?component_access_token".$component_access_token;
    }


    function preAuthCode($component_access_token=''){

        $ret=$this->redis->hGet($this->pre.$this->options['appid'],'pre_auth_code');
        if(empty($ret)){
            $component_access_token=$this->component_access_token('', $this->redis);

            if(!empty($component_access_token)) {
                $curl=new ZCurl();
                $data=$curl->jsonPost($this->preAuthCodeUrl($component_access_token),array('component_appid'=>$this->options['appid']));


                $this->redis->hSet($this->pre.$this->options['appid'],'pre_auth_code',$data['pre_auth_code']);
                return $data['pre_auth_code'];
            }
            return false;

        } else {
            return $ret;
        }




    }
    function compAccessToken($appId,$appSecret,$verifyTicket){
        $curl=new ZCurl();
        $data=$curl->jsonPost($this->compAccessTokenUrl(),array('component_appid'=>$appId,'component_appsecret'=>$appSecret,"component_verify_ticket"=>$verifyTicket));
        if(!empty($data['component_access_token'])){

            $this->redis->hMset($this->pre.$appId,array('component_access_token'=>$data['component_access_token']));
            return $data['component_access_token'];
        }
        return false;

    }
    function nonce(){

        return $_REQUEST['nonce'];
    }
    function timestamp(){

        return $_REQUEST['timestamp'];
    }
    function rawMsg(){
        if(empty($this->rawMsg)){
            $this->rawMsg=file_get_contents("php://input");
        }

        return $this->rawMsg;


    }
    public static function xmlSafeStr($str)
    {
        return '<![CDATA['.preg_replace("/[\\x00-\\x08\\x0b-\\x0c\\x0e-\\x1f]/",'',$str).']]>';
    }
    /**
     * 数据XML编码
     * @param mixed $data 数据
     * @return string
     */
    public static function data_to_xml($data) {
        $xml = '';
        foreach ($data as $key => $val) {
            is_numeric($key) && $key = "item id=\"$key\"";
            $xml    .=  "<$key>";
            $xml    .=  ( is_array($val) || is_object($val)) ? self::data_to_xml($val)  : self::xmlSafeStr($val);
            list($key, ) = explode(' ', $key);
            $xml    .=  "</$key>";
        }
        return $xml;
    }
    /**
     * XML编码
     * @param mixed $data 数据
     * @param string $root 根节点名
     * @param string $item 数字索引的子节点名
     * @param string $attr 根节点属性
     * @param string $id   数字索引子节点key转换的属性名
     * @param string $encoding 数据编码
     * @return string
     */
    public function xml_encode($data, $root='xml', $item='item', $attr='', $id='id', $encoding='utf-8') {
        if(is_array($attr)){
            $_attr = array();
            foreach ($attr as $key => $value) {
                $_attr[] = "{$key}=\"{$value}\"";
            }
            $attr = implode(' ', $_attr);
        }
        $attr   = trim($attr);
        $attr   = empty($attr) ? '' : " {$attr}";
        $xml   = "<{$root}{$attr}>";
        $xml   .= self::data_to_xml($data, $item, $id);
        $xml   .= "</{$root}>";
        return $xml;
    }
    function decMsg($rawMsg='',$nonce='',$timeStamp='',$msg_sign=''){
        if($rawMsg==''){
            $rawMsg=$this->rawMsg();
        }
        $xml_tree = new DOMDocument();
        $xml_tree->loadXML($rawMsg);







        if(empty($timeStamp)){$timeStamp=$_GET['timestamp'];}
        if(empty($nonce)){$nonce=$_GET['nonce'];}
        if(empty($msg_sign)){$msg_sign=$_GET['msg_signature'];}


        if(empty($timeStamp)){
            $timeStamp = $xml_tree->getElementsByTagName('TimeStamp');
            $timeStamp = $timeStamp->item(0)->nodeValue;
        }
        if(empty($nonce)){
            $nonce = $xml_tree->getElementsByTagName('Nonce');
            $nonce=$nonce->item(0)->nodeValue;

        }
        if(empty($msg_sign)){
            $msg_sign = $xml_tree->getElementsByTagName('MsgSignature');
            $msg_sign = $msg_sign->item(0)->nodeValue;
        }

        $msg = '';
        $errCode = $this->crypt->decryptMsg($msg_sign, $timeStamp, $nonce, $rawMsg, $msg);
        if ($errCode == 0) {
            //$this->_receive = (array)simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
            return (array)simplexml_load_string($msg, 'SimpleXMLElement', LIBXML_NOCDATA);
        } else {
            return false;
        }


    }
    function encMsg($msg,$nonce,$timeStamp){


        $encryptMsg = '';
        $errCode = $this->crypt->encryptMsg($msg, $timeStamp, $nonce, $encryptMsg);
        if ($errCode == 0) {
            return $encryptMsg;
        } else {
            return false;
        }


    }

}