<?php

class Esindex {

    protected $client;
    protected $index;
    protected $type;
    function __construct($db,$tb='',$opts=array()){

        $this->client=new Elastica\Client();


        $this->index=$this->client->getIndex($db);

        if(!$this->index->exists()){
            $this->index->create(array(),true);
        }
        if($tb!=''){
            $this->type=$this->index->getType($tb);


        }

    }
    function deleteType($types){
        if(is_array($types)){
            foreach($types as $key=>$val){
                $this->type=$this->index->getType($val);
                if($this->type->exists()){
                    $this->type->delete();
                }

            }
        }
    }
    function setType($type){
        $this->type=$this->index->getType($type);

    }
    function setIndex($index){
        $this->client->getIndex($index);
    }
     function updateIndex($docs,$tb='',$typeOpts=array())
    {
        return $this->opIndex($docs,$tb,$typeOpts,'update');
    }
     function createIndex($docs,$tb='',$typeOpts=array()){
        return  $this->opIndex($docs,$tb,$typeOpts,'add');


//
//        $query = array(
//            'query' => array(
//                'query_string' => array(
//                    'query' => 'win',
//                )
//            )
//        );
//
//        $path = $index->getName() . '/' . $type->getName() . '/_search';
//
//        $response = $client->request($path, Elastica\Request::GET, $query);
//        $responseArray = $response->getData();
    }

     function opIndex($docs,$tb='',$typeOpts=array(),$op='add'){

            if($tb!=''){
                $this->type = $this->index->getType($tb);
            }



            if(is_array($docs)){


                /**
                 * array("_id"=>array(key1=>val2),...)
                 */
                if(empty($docs[0])){
                    if(is_array($docs[key($docs)])){
                        $newDocs=array();
                        foreach($docs as $key=>$val){

                            $newDocs[]=new Elastica\Document($key, $val);
                        }
                        $docs=$newDocs;

                    } else {
                        /**
                         * array("key1"=>"val1,"_id"=>"k1") or array("key1"=>"val1,"_id"=>"k1")(without _id)
                         */
                        $docId=empty($docs['_id'])?(empty($docs['id'])?'':array("id",$docs['id'])):array("_id",$docs['_id']);
                        $docs=new Elastica\Document($docId[1], $docs);

                    }


                } else {
                    /**
                     * array(array("key1"=>"val1,"_id"=>"k1"),...);
                     */
                    $docs=array_map(function($doc){
                        $docId=empty($doc['_id'])?(empty($doc['id'])?'':array("id",$doc['id'])):array("_id",$doc['_id']);
                        return new Elastica\Document($docId[1], $doc);
                    },$docs);

                }



            }

             if($docs instanceof Elastica\Document){
                 $docs=array($docs);

             }
            $action=$op.'Documents';
            $this->type->$action($docs);
            $this->index->refresh();
            return true;

    }
    function search($query){
        $path = $this->index->getName() . '/' . $this->type->getName() . '/_search';
        if(is_string($query)){
            if(Misc_Utils::isPhpJson($query)){
                $result = $this->client->request($path, Elastica\Request::GET, $query);
            } else {
                $m = new Elastica\Query\QueryString();

                $m->setParam("query",$query);
                $result=$this->type->search($m);

            }

        }
        return $result;

        //$response = $client->request($path, Elastica\Request::GET, $query);

        //$m->setFields(array("username"));
        //$m->setFieldQuery("query_string","ruflin");



    }
    function searchJson($query){
         $result=$this->search($query) ;
         return (json_encode($this->toArray($result->getResults()),JSON_UNESCAPED_UNICODE));

    }
    function searchObj($query){
        $result=$this->search($query) ;
        return $this->toArray($result->getResults());

    }
    function toArray($result){
        return array_map(function($item){
            $hitData=$item->getHit();
            $data=$item->getData();
            $data['_id']=$hitData['_id'];
            return $data;

        },$result);
    }
    function deleteIndex($docs,$tb='',$typeOpts=array()){
        return self::opIndex($docs,$tb,$typeOpts,'delete');
    }
}