<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 7/23/14
 * Time: 7:25 PM
 */

class RootTable  {

    protected $R;
    protected $_ztb;
    protected $_query;
    protected $_opMenus=array('times','edit');

    protected $appContext;
    protected $_batchOpMenus=array(

    );
    protected $_isAjax;
    protected $_tbStyles=array('table','grid','list');
    protected $_tbStyle='table';
    protected $_en3dView=false;


    function __construct($appContext,$ajax=''){
        $this->appContext=$appContext;
        $this->R=new RoleResourceModel();
        $_isAjax=$ajax;

        if(!empty($_POST['resti']['action'])){

            $orgM=$this->appContext->modelName;
            $m=new $orgM();
            if(method_exists($m,$_POST['resti']['action']) && in_array($_POST['resti']['action'],$orgM::$_modelMethods,true)){

                if($m->isOwner($_REQUEST)){
                    $result=call_user_func(array($m,$_POST['resti']['action']),$_REQUEST);
                    $this->appContext->code=isset($result['code'])?$result['code']:1;
                    $this->appContext->msg=isset($result['msg'])?$result['msg']:'';
                    $this->appContext->data=isset($result['data'])?$result['data']:$result;

                    return $this->appContext->m();
                } else {

                    $this->appContext->code=-1;
                    $this->appContext->msg='access_denied';

                }

            }

        }

    }
    function opMenus(){

        return $this->_opMenus;
    }
    function batchOpMenus(){


        foreach($this->_batchOpMenus as $key=>&$item){
            $item['cfg']['name']=$this->appContext->formatMsg($this->appContext->lang,$item['cfg']['name']);
        }

        return $this->_batchOpMenus;
    }


    function en3dView(){
        return $this->_en3dView;
    }
    function tbStyles(){
        return $this->_tbStyles;
    }
    function tbStyle() {
        if($_GET['tb_style'] && in_array($_GET['tb_style'],$this->tbStyles(),true)){
            $this->appContext->_tbStyle=$_GET['tb_style'];
        }
        return $this->_tbStyle;

    }
    function ztb(){
        if(empty($this->_ztb)){
            $this->_ztb=$_REQUEST['ztb'];
        }
        return $this->_ztb;
    }


    public function getKeys($model){

        $keys=array_keys($model->tbMeta());

        $keys2=explode(",",$model->primary());


        return array_unique(array_merge($keys,$keys2));
    }

    /**
     * @param $this->appContext application's this object
     * @param $post    post data
     * @param $user    user session data
     * @param $model   CommonUserM
     * @param $resource
     * @return bool
     */
    function create($post,$user,$model){


        $level=$user['level'];

        /**
         * $this->appContext->R
         */

        if(!class_exists($model)){

            return false;
        }
        $table=$model::tbName();
        if($this->R->canDo($level,$table,'write')){


            if(class_exists($model)){
                $m=new $model;
                $columns=$this->appContext->getKeys($m);

                $ret=$m->check();

                if($ret[0]){
                    $tbMeta=$m->getTbMeta();
                    $ownerId=$m->ownerId();
                    if(!isset($ret[1][$ownerId])){
                        $ret[1][$ownerId]=intval($user[$ownerId]);
                    }
                    foreach($ret[1] as $k=>$v){
                        if($tbMeta['reg']==='fa_icon'){
                            $method="get".ucfirst($k);
                            if(method_exists($m,$method)){
                                $ret[1][$k]=$m->$method($ret[1]);
                            }
                        }
                    }

                    if(method_exists($m,'beforeAdd')) {

                        $ret[1]=$m->beforeAdd($ret[1],$user);
                        if(isset($ret[1]['_ret']['code']) && $ret[1]['_ret']['code']!=1){
                            $this->appContext->msg=$ret[1]['_ret']['msg'];
                            $this->appContext->code=$ret[1]['_ret']['code'];

                            return false;

                        }
                        else {
                            unset($ret[1]['_ret']);
                        }
                    }
                    if(!method_exists($m,'add')) {
                        $insertId=$m->insert($ret[1]);
                        $ret[1]['_insertId']=$insertId;
                    } else {
                        /**
                         * custom add function
                         */
                        $insertId=$m->add($ret[1],$user);
                        if($ret[1]['_ret']['code']==-1){
                            $this->appContext->msg=$ret[1]['_ret']['msg'];
                            $this->appContext->code=$ret[1]['_ret']['code'];
                            return false;
                        }
                        $ret[1]['_insertId']=$insertId;
                    }
                    if(method_exists($m,'afterAdd')) {

                        $ret[1]=$m->afterAdd($ret[1],$this->appContext->data);
                        if($ret[1]['_ret']['code']==-1){
                            $this->appContext->msg=$ret[1]['_ret']['msg'];
                            $this->appContext->code=$ret[1]['_ret']['code'];
                            return false;
                        }
                    }
                    if($insertId>0 ||  preg_match("/^[\w-]+$/",$insertId,$ret) || is_array($insertId) ){
                        $this->appContext->msg=$this->appContext->lang['add_record_suc'];
                        $this->appContext->data=is_array($insertId)?$insertId:[$model::$_primary=>$insertId];
                        return true;
                    }
                    else {
                        $this->appContext->msg=empty($this->appContext->msg)?$this->appContext->lang['add_record_fail']:$this->appContext->msg;
                        $this->appContext->code=$this->appContext->c['ret']['RecordAddFailed'];
                        return false;
                    }

                }
                else {
                    $this->appContext->msg="invalid data format";
                    $this->appContext->data=$ret[1];
                    $this->appContext->code=$this->appContext->c['ret']['FieldFormatError'];
                    return false;
                }

            }
            else
            {
                $this->appContext->code=$this->appContext->c['ret']['TableNotExist'];
                $this->appContext->msg='invalid request';
                return false;
            }

        }
        else{
            $this->appContext->code=$this->appContext->c['NoPermToAdd'];
            $this->appContext->msg='you have no permission to operate';
            return false;
        }

        return false;

    }
    /**
     * @resource 添加表记录
     * @desc 如果要增加添加表记录,必须添加改权限
     *
     * @return bool
     */


    function patch(){


    }
    function update($request,$user,$model,$opts){

        $level=$user['level'];
        $table=trim($model::tbName());
        $id=trim($request['id']);
        if(preg_match("/^[\w-]+$/i",$id,$pregMatchRet)===false){
            $this->appContext->code=$this->appContext->c['ret']['FieldFormatError'];
            $this->appContext->msg='invalid data '.$id;
            return $this->appContext->m();
        }

        if($this->R->canDo($level,$table,'write')){


            if(class_exists($model)){
                $m=new $model;
                $columns=$this->appContext->getKeys($m);
                $ret=$m->check();
                if($ret[0]){
                    $arr=array();
                    $arr[$m->primary()]=$id;
                    $tbMeta=$m->getTbMeta();
                    foreach($tbMeta as $k=>$v){
                        if($v['reg']==='fa_icon'){
                            $method="get".ucfirst($k);
                            if(method_exists($m,$method)){
                                $ret[1][$k]=$m->$method($ret[1]);
                            }
                        }
                    }

                    $retId=$m->update($ret[1],$arr);
                    if($retId>0){
                        $this->appContext->msg=$this->appContext->lang['up_record_suc'];
                        $this->appContext->data=array('id'=>$retId);
                    }
                    else {
                        $this->appContext->msg=$this->appContext->lang['up_record_fail'];
                        $this->appContext->code=$this->appContext->c['ret']['RecordUpdateFailed'];
                    }

                }
                else {
                    $this->appContext->msg=$this->appContext->lang['err_data_fmt'];
                    $this->appContext->data=$ret[1];
                    $this->appContext->code=$this->appContext->c['ret']['FieldFormatError'];
                }



            }
            else
            {
                $this->appContext->code=$this->appContext->c['ret']['TableNotExist'];
                $this->appContext->msg='invalid request';
            }


        }
        else{
            $this->appContext->code=$this->appContext->c['NoPermToUpdate'];
            $this->appContext->msg='you have no permission to operate';
        }
        return $this->appContext->m();


    }
    /**
     * @resource 更新表记录
     * @desc 如果要添加更新表记录,必须添加改权限
     * @return bool
     */
    function editAction(){
        /**
         * @todo check multi relation id
         */
        unset($_POST['selectAllkeyword'],$_POST['selectItemkeyword']);


    }
    function csvRecords($result,$pId){
        return Misc_Utils::arr2records($result,array($pId));
    }
    function import($file,$user,$model,$type){


        if(class_exists($model)) {
            $m = new $model;
            $pId=$m->primary();
            $cols=$this->appContext->getKeys($m);



                $result=Misc_Utils::csv2arr($file);

                $diff=array_diff($result[0],$cols);


                if(count($diff)>0){
                    $this->appContext->code=-1;
                    $this->appContext->msg="导入数据格式错误,请调整";
                } else {
                    if(!empty($result[0])){
                        $method=$type."Records";
                        if(!method_exists($this,$method)){
                            $this->appContext->code=-1;
                            $this->appContext->msg="不支持的导入的文件格式";
                        } else {
                            $records=$this->$method($result,$pId);
                            if(method_exists($model,'beforeImport')) {
                                $return = $model->beforeImport($records,$user);
                                if ($return[0] === false) {
                                    if ($return[1]['code'] == -1) {
                                        $this->appContext->msg = $return[1]['msg'];
                                        $this->appContext->code = $return[1]['code'];
                                        $this->appContext->data = $return[1]['data'];
                                        return $this->appContext->m();
                                    }
                                }

                            }
                        }

                        $m->insert($records);


                    }
                }


            return $this->appContext->m();

        }


    }

    function importAction($modelName){
        $file=$_FILES['file']['tmp_name'];
        $type=$_GET['t'];

        return $this->import($file,$_SESSION['user'],$modelName,$type);




    }
    function downloadPage($fileName,$arr,$opts=array('title'=>'','createor'=>''),$imageExtra){

        ob_end_clean();


        $xls = new PHPExcel();
        $xls->getProperties()->setCreator($opts['createor'])
            ->setLastModifiedBy($opts['createor'])
            ->setTitle($opts['title'])
            ->setSubject($opts['subject'])
            ->setDescription($opts['desc'])
            ->setKeywords($opts['keywords'])
            ->setCategory($opts['category']);
        $result=Misc_Utils::records2arr($arr);

        list($header,$data)=$result;

//// Add some data
//        $objDrawing = new PHPExcel_Worksheet_Drawing();
//        $objDrawing->setName('Logo');
//        $objDrawing->setDescription('Logo');
//
//        $objDrawing->setPath('../../../img/logo.png');
//        $objDrawing->setHeight(36);


        $sheet=$xls->setActiveSheetIndex(0);
        $cnt=count($header);
        $header[]='Qrcode';

        foreach($header as $k=>$val){

            $sheet->setCellValue(Misc_Utils::numToXlsCord(($k+1)).'1', $val);


        }
        include_once(realpath("../application/library/Qrcode/qrlib.php"));
        $fileName="./upload/qrcode/links";
        if(!is_dir($fileName)){
            mkdir($fileName,0777,true);
        }

        foreach($data as $row=>$val){
            $httpUrl="http://".$_SERVER['HTTP_HOST']."/cache/wap/qr_{$val[0]}.html";
            $fileUrl=$fileName."/".$val[0].".png";
            if(!is_file($fileUrl)){
                /*
                 * L级：约可纠错7%的数据码字
                    M级：约可纠错15%的数据码字
                    Q级：约可纠错25%的数据码字
                    H级：约可纠错30%的数据码字
                 *
                 */
                QRcode::png($httpUrl, $fileUrl, 'H',6 , 2);
            }

            foreach($val as $col=>$val2){

                $sheet->setCellValue(Misc_Utils::numToXlsCord(($col+1)).($row+2), $val2);


            }
            $sheet->getRowDimension(($row+2))->setRowHeight(200);

            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('Logo');
            $objDrawing->setDescription('Logo');
            $objDrawing->setPath($fileUrl);
            $objDrawing->setCoordinates(Misc_Utils::numToXlsCord(($cnt+1)).($row+2));
            $objDrawing->setHeight(250);

            $objDrawing->setWorksheet($sheet);

        }
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $xls->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$fileName.'"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($xls, 'Excel5');
        $objWriter->save('php://output');

    }
    function uploadAction(){

    }
    public function export($records,$modelName){

        $downloadType=$_GET['t'];

        if(!empty($records)){
            if($downloadType=='csv'){
                $data=Misc_Utils::arr2str($records);
                Misc_Utils::downloadData($downloadType,$data);

            } else if($downloadType=='html'){

                $table=Misc_Utils::arr2tb($records,"</td><td>","<tr><td>","</td></tr>");
                Misc_Utils::downloadHeader($modelName.".html",'html',true);
                echo Misc_Utils::htmlTemp($table,'5');



                exit;
            } else if($downloadType=='pdf'){

                $result=Misc_Utils::records2arr($records);


                $tpdf=new TablePdf($result[0],$result[1]);
                $tpdf->BasicTable();
                $tpdf->Output();
                exit;
            } else if($downloadType=='xls'){
                $fileName=$modelName."_".date("Ymdhis").".xls";

                if($modelName=='page'){


                    $records=array_map(function($item){
                        $item['url']="http://".$_SERVER['HTTP_HOST']."/cache/wap/qr_{$item['id']}.html";
                        return $item;
                    },$records);

                    $imgUrls=array_map(function($item){
                        return array("./upload/qrcode/links/".$item['id'].".png");
                    },$records);
                    Misc_Utils::downloadXls($fileName,$records,array(
                        'creator'=>$this->appContext->c['site']['name'].$this->appContext->c['site']['link']),array(array('Qrcode')
                    ,$imgUrls));


                } else {

                    Misc_Utils::downloadXls($fileName,$records,array(
                        'creator'=>$this->appContext->c['site']['name'].$this->appContext->c['site']['link']
                    ));
                }




                return false;
                exit;
            }



            exit;
        }
        else {

            exit("没有数据导出");
        }


    }


    /**
     * @param $request GET/POST等请求数据
     * @param $user  $user 用户的session信息,必须包含level,uid,username三个键
     * @param $modelName 类似于CommonUser的model的类名
     * @param string $whereCond
     */
    function allowCols(){

        return $this->appContext->data['columns'];
    }
    function read($request,$user,$modelName,$whereCond=[],$fields=[]){


        $m=new $modelName();



        if(count($fields)>0){
            $orgColumns=$fields;

        } else {
            $orgColumns=$this->appContext->getKeys($m);

        }

        $this->appContext->data['columns']=$m->tbMeta2($this->appContext->lang);
        if(!empty($orgColumns[0])){
            $fields=array_fill_keys($orgColumns,1);
        }

        //$this->appContext->data['columns']=array_intersect_key($this->appContext->data['columns'],$fields);
        $this->appContext->data['columns']=array_intersect_key($this->appContext->data['columns'],$fields);
        $idVal=$whereCond['_id'];
        if(isset($idVal)){
            $whereCond=array_intersect_key($whereCond,$this->allowCols());
            if(!isset($whereCond['_id'])){
                $whereCond['_id']=$idVal;
            }
        }



        if(method_exists($m,'beforeRead')) {
            $return = $m->beforeRead($request, $user,$whereCond);
            if ($return[0] === false) {
                if ($return[1]['code'] == -1) {
                    $this->appContext->msg = $return[1]['msg'];
                    $this->appContext->code = $return[1]['code'];
                    $this->appContext->data = $return[1]['data'];
                    return $this->appContext->m();
                }
            }
        }
        $this->appContext->data['queryData']=json_encode($_GET);
        $this->appContext->data['page']=intval(empty($request['p'])?1:$request['p']);
        $this->appContext->data['size']=intval(empty($request['s'])?$this->appContext->c->site->page_size:$request['s']);
        $this->appContext->data['start']=intval(empty($request['start'])?0:$request['start']);

        /**
         * q 是搜索的q,q=encodeURIComponent("type=red&link=123") 表示搜索type=red,link＝123的内容
         */





        //intval(empty($request['len'])?$this->appContext->c->page->len:$request['len']);

        $this->appContext->data['isPaging']=false;
        $this->appContext->data['media_dir']=$this->appContext->c->application->filebase;
        $ownArr=array();
        $ownerId=$m->ownerId();
        $sid=$user['uid'];
        /**
         * 是展示还是导出等import/export
         */
        $action=$_GET['zact'];
        $this->appContext->data['opAction']=$modelName::opAction();
        $this->appContext->data['opActionJson']=json_encode($modelName::opAction());
        $this->appContext->data['tbStyle']=$this->tbStyle();
        $this->appContext->data['actions']=json_encode($m->actions());
        $this->appContext->data['defUuid']=Yaf\Registry::get("config")->site->ib_uuid;
        $this->appContext->data['zact']=$action;
        $this->appContext->data['tableName']=$m->tbName();
        $this->appContext->data['en3dView']=$this->en3dView();
        $this->appContext->data['orgTbName']=$m->orgTbName();
        $orgTbName=$m::$_tbName;
        $this->appContext->data['tbNameTxt']=$this->appContext->lang["t_".$orgTbName]?$this->appContext->lang["t_".$orgTbName]:($this->appContext->lang[$orgTbName]?$this->appContext->lang[$orgTbName]:str_replace("Model","",$modelName));
        $this->appContext->data['tbName']=str_replace("Model",'',$modelName);
        $this->appContext->data['primaryKey']=$m->getPrimary();


        $this->appContext->data['styleMeta']=empty($m->styleMeta())?'{}':json_encode($m->styleMeta());



        $total=$m->total();
        $searchRet=array();

        ob_end_clean();


        if($_REQUEST['t']=='more' && ($this->appContext->data['page']-1)*$this->appContext->data['size']>=$total){
            exit('');
        }


        if($this->appContext->data['size']<$total){
            $this->appContext->data['isPaging']=true;
        }


        if(empty($action)){

            if(!empty($_POST['ids'])){
                $_POST[$this->appContext->data['primaryKey']]=$_POST['ids'];
            }


            //$whereCond=array_intersect_key($_POST,array_fill_keys($orgColumns,1));


            if(isset($whereCond['_id'])){
                if(Validator::isReg('mongoid',$whereCond['_id'])){
                    $whereCond['_id']=new MongoId($whereCond['_id']);
                } else {

                    $this->appContext->code=-1;
                    $this->appContext->msg="error mongo id";
                }
            }

            $db=$m->db();
            $tb=$m->tbName();
            if($modelName::$_chkOwn===false){
                $ownArr=array();
            }
            $ownArr=$whereCond;
            $uid=$user[$ownerId];



            if(empty($whereCond)){
                if($this->appContext->data['columns'][$ownerId]){

                    $whereCond=array($ownerId=>$uid);
                }
            }


            if($user['level']==9){
                if(!empty($_REQUEST['kw'])){

                    $record=$this->appContext->esSearch($m,$db,$tb);
                    if($record===false){
                        return $this->appContext->error("cant_search");
                    }


                } else {
                    $records=$m->find($whereCond,$this->appContext->data['size'],array(),'',$this->appContext->data['page']);
                }

            } else {


                if(method_exists($m,"beforeView")){
                    $m->beforeView($ownArr);
                }


                if(!empty($_REQUEST['kw'])){
                    $record=$this->appContext->esSearch($m,$db,$tb);
                    if($record===false){
                        return $this->appContext->error("cant_search");
                    }

                } else {

                    $records=$m->findOwn($whereCond,($this->appContext->data['page']-1),$this->appContext->data['size'],$orgColumns);

                }


            }

            $isAjax=($this->appContext->e['isAjax'] || $this->appContext->e['isJsonReq'] || $this->appContext->isAjax || $this->_isAjax);

            list($records,$mapMeta)=$this->formatRecords($records,$isAjax);


            if($isAjax){

                return $records;
            }

            if($_POST['t']=='more'){
                exit(json_encode($records,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
            }


            //$mapMeta=$this->appContext->formatMapMeta($mapMeta);


            $this->appContext->data['total']=$total;
            $this->appContext->data['records']=$records;

            $this->appContext->data['batchOpMenus']=$this->batchOpMenus();
            if($_GET['feature']){

            }
            $this->appContext->data['opMenus']=json_encode($this->opMenus());





        } else {
            if($action=='group'){
                if(method_exists($m,'doGroup')){

                    $m->doGroup($_GET,$this->appContext->data,$this->appContext->lang);

                }
            }
        }

        /**
         * colCnt 表格列数
         */
        $this->appContext->data['colCnt']=count($this->appContext->data['columns'])+2;

        /**
         * 获取行的联合查询数据以及列的额外数据(获取data-source指定的表的数据)
         */
        $this->joinRecordsCols($records,$user,$modelName);
        $this->appContext->data['jsonCols']=json_encode($this->appContext->data['columns']);
        /**
         * 必须严格比较,安全因素
         */
        /**
         * 是否是导出操作
         */
        if(array_key_exists($_GET['t'],array('xls','csv','pdf'))){
            $this->export($this->appContext->data['records'],$this->appContext->modelName());
        } else {

        }



    }
    function joinRecordsCols(&$records,$user,$modelName){

        foreach($this->appContext->data['columns'] as $key=>&$val){
            if($val['type']=='select' || $val['type']=='tree-select'){
                if($val['data_source']['tb']==''){
                    $val['data_source']['tb']=$modelName;
                }
                $tbName=$this->appContext->classFromTb($val['data_source']['tb']);
                if(class_exists($tbName)){
                    $tb=new $tbName();
                    $selKey=$pid=$tb->primary();
                    $ownId=$tb->ownerId();
                    if(empty($val['data_source']['dataset'])){

                        $dataSet='';
                        if(!empty($val['data_source']['val'])){
                            $selKey=$val['data_source']['val'];
                        }
                        /**
                         * 如果表中的一列对应的数据源是data-tree
                         */
                        if(strpos($val['data_extra'],'data-tree')!==false){
                            $dataSet=array();


                            if($tbName==$modelName){
                                foreach($records as $k=>$record){
                                    $dataSet[$record[$selKey]]=$record;
                                }
                            } else {
                                $dataSet=$tb->findOwn(array($ownId=>intval($user['uid'])),0,100,array('pid',$val['data_source']['val'],$val['data_source']['field']));

                            }

                            Misc_Utils::getSubMenus($dataSet,$val['data_source']['dataset']);

                        } else {

                            $dataSet=$tb->findOwn(array($ownId=>intval($user['uid'])),0,100,array($val['data_source']['val'],$val['data_source']['field']));

                            foreach($dataSet as $key2=>$val2){
                                $val['data_source']['dataset'][$val2[$selKey]]=$val2[$val['data_source']['field']];
                            }
                        }



                    }

                }
            }
        }
    }
    function classFromTb($tbName,$isMongo=false,$sufix="Model"){
        if($isMongo){
            $sufix='M'.$sufix;
        }
        $tbs=explode('_',$tbName);
        $tbName='';
        $tbs=array_map(function($tb){
            return ucwords($tb);

        },$tbs);

        return join('',$tbs).$sufix;

    }
    function formatMapMeta($mapMeta){
        return array_map(function($item){

            $model=$this->appContext->classFromTb($item['meta']['tb']);

            $refField=$item['meta']['ref_field'];
            $showField=$item['meta']['show_field'];

            if(class_exists($model)){
                $model=new $model;
                //    function findIn($query=array(),$cols=array(),$order="",$startPage=1,$pageSize=20){

                $data=$model->findIn([$refField=>$item['data']],array($showField,$refField),"",1,9999);
                $ret=array();
                foreach($data as $key=>$val){
                    $ret[$val[$refField]]=$val[$showField];
                }

                return $ret;
            } else {
                return $item;
            }
        },$mapMeta);
    }
    function revFmtDate($record){

        foreach(array('ctime','dateline','mtime','utime') as $key=>$val){
            if(isset($record[$val]) && ($record[$val] instanceof MongoDate) ){
                $record[$val]=$record[$val]->sec;
            }
        }
        return $record;
    }
    function formatRecords($records,$isAjax=false){
        $mapMeta=array();
        $records=array_map(function($item)use(&$mapMeta,$isAjax){
            $item=$this->revFmtDate($item);
            if(!empty($item['pic_url'])){
                $path=pathinfo($item['pic_url']);
                $item['pic_thumb100']=$path['dirname']."/".$path['filename']."_100x100.".$path['extension'];
            }
            foreach($item as $k=>$v){

                if(array_key_exists('map',$this->appContext->data['columns'][$k]))
                {
                    if(!isset($mapMeta[$k])){
                        $mapMeta[$k]['meta']=$this->appContext->data['columns'][$k]['map'];
                    }
                    $mapMeta[$k]['data'][]=$v;
                }

                if(is_array($item[$k])){
                    if($isAjax===false){
                        $item[$k]=json_encode($v,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
                    }

                } else if($this->appContext->data['columns'][$k]['reg']=='text') {
                    $item[$k] = htmlentities($v);
                }
                else if($this->appContext->data['columns'][$k]['reg']=='fa_icon'){
                    $item[$k]="<i class='fa $v'></i>";
                }
            }
            return $item;
        },$records);

        return array($records,$mapMeta);
    }
    /**
     * @param  model
     */
    function esSearch($model,$db,$tb)
    {
        if (!empty($model->indexes())) {
            $esc = new Esindex($db, $tb);
            $result = $esc->searchObj($_REQUEST['kw'] . "*");
            if (!empty($result)) {
                $records = $model->findIn($result, $model->primary(), true);
            }
            else {
                return [];
            }
            return $records;


        } else {
            return false;
        }


    }
    function array2csv($data)
    {
        $outstream = fopen("php://temp", 'r+');
        fputcsv($outstream, $data, ',', '"');
        rewind($outstream);
        $csv = fgets($outstream);
        fclose($outstream);
        return $csv;
    }
    function featureMenus($name){
        $defArr=array(
            'batchOp'=>array('add'=>array('icon'=>'fa-plus','func'=>'addRecord','order'=>1),
                'del'=>array('icon'=>'fa-trash','func'=>'rmRecord','order'=>2),
                'import'=>array('icon'=>'fa-upload','func'=>'importRecord','order'=>3),
                'export'=>array('icon'=>'fa-download','func'=>'downloadRecord','order'=>4)
            ),
            'op'=>array('del'=>array('icon'=>'fa-times','func'=>'rmRecord','order'=>1),
                'edit'=>array('icon'=>'fa-times','func'=>'editRecord','order'=>2)
            )
        );
        if($name==''){
            return $defArr;
        }
        if($name=='status_app'){
            $defArr['op'][]=array('info'=>array('icon'=>'fa-info','func'=>'router','order'=>1,'param'=>'app_status'));
        }

    }
    function featureBatchMenus(){

    }


    function delete($post,$userData,$table=''){


        $level=$userData['level'];
        if($table==''){
            $table=trim($this->ztb());
        }



        if(Validator::isEn($table)==false){
            $this->appContext->code=$this->appContext->c['ret']['FieldFormatError'];
            $this->appContext->msg='invalid data';
            return false;
        }
        $id=0;
        if($this->R->canDo($level,$table)){
            $model=$this->appContext->classFromTb($table);

            if(class_exists($model)){
                $m=new $model;

                $primary=$m->primary();
                $ownerId=$m->ownerId();
                if($userData['level']!=9){

                    $arr[$ownerId]= $userData[$ownerId];
                    if(count($post["ids"])>1){
                        $id=$m->deleteMany($post['ids'],$userData[$ownerId],$ownerId);
                    } else {
                        if(method_exists($m,'deleteOwn')){
                            $id=$m->deleteOwn($post["ids"],$userData[$ownerId],$ownerId);
                        }
                    }

                } else {
                    $id=$m->deleteMany($post['ids']);

                }


                if(!($id>0) || empty($id)){
                    $this->appContext->code=-1;
                    $this->appContext->msg='delete error';
                    return false;
                }

            }
            else
            {
                $this->appContext->code=$this->appContext->c['ret']['TableNotExist'];
                $this->appContext->msg='invalid request';
                return false;
            }


        }
        else{
            $this->appContext->code=$this->appContext->c['ret']['NoPermToRead'];
            $this->appContext->msg='you have no permission to operate';

            return false;
        }

        return true;


    }
    function setZtb($ztb){
        $this->_ztb=$ztb;
    }

}

/**
 *
 * 1. 增删改查crud操作
 * 2. readonly的列
 * 3. 针对列的属性出现不同的editor
 * 4. 几行代码增加对表的操作
 * 5. 提供元素局接口自定义构建菜单
 * 6. 权限校验归结成表+字段的操作,根据用户定义进行
 * a. 可以建立几个level的mysql用户,从而进行对应的操作,将level权限对应到数据库用户
 * b. 或者建立白名单的表,维护一个用户权限表,启动时候载入
 * 业务逻辑 level 1=>array('order'=>array(
 *                  edit=>array(id=>"1,2,3,4"),
 *                  delete=>
 *                  select=>array()
 *
 *
 *)
 * 对应的model方法
 */