<?php
/**
 * Class Lang_Lang
 * 获取语言配置
 */

class Lang {

    static function getLang($lang=''){
        if(empty($lang))
        {
            $lang=self::getLangType();
        }

        if(self::isExist($lang) )
        {
            return parse_ini_file(APP_PATH."/application/library/Lang/{$lang}.ini");
        }
        else
        {
            return  parse_ini_file(APP_PATH."/application/library/Lang/enus.ini");;
        }

    }
    static  function formatMsg($lang,$key){



        return $lang[$key]?$lang[$key]:$key;


    }
    /**
     * [name=>'hello']
     * 翻译后变成name=>'您好',要保留原来的hello值可以使用langKey:['key'=>'item','item2']
     */

    static function fmtMsgPluck($lang,$arr,$langKeys,$pluckKey){
        $newArr=[];
        foreach($arr as $key=>&$val){
            foreach($langKeys as $key2=>$item){
                if(is_string($key2)){
                    $val[$key2]=$val[$item];
                }
                $val[$item]=self::formatMsg($lang,$val[$item]);


            }
            $newArr[$val[$pluckKey]][]=$val;
        }

        return $newArr;
    }
        static function translate(){
            $args=func_get_args();
            $lang=array_shift($args);
            $result=array();
            foreach($args as $item){
                $result[]=self::formatMsg($lang,$item);
            }
            $langType=self::getLangType();
            switch($langType){
                case 'zhcn':
                    $result=implode("",$result);
                    break;
                default:{
                    $result=trim(implode(" ",$result));
                }
            }
            return $result;
    }
    /**
     * @param $lang 传入翻译的语言,array('hello'=>'您好')
     * @param $arr
     * @param $langKeys ["_a"=>"a"] 表示翻译带字段a的值同时添加一个_a保留原值,惯例是给a前面加前缀_
     * @return array  array('key1'=>[],'key2'=>[]) group by key
     */
    static function fmtMsgArr($lang,$arr,$langKeys){
        foreach($arr as $key=>&$val){
            foreach($langKeys as $key2=>$item){
                if(is_string($key2)){
                    $val[$key2]=$val[$item];
                }
                $val[$item]=self::formatMsg($lang,$val[$item]);
                /**
                 * [name=>'hello']
                 * 翻译后变成name=>'您好',要保留原来的hello值可以使用langKey:['key'=>'item','item2']
                 */


            }
        }

        return $arr;
    }
    static function isExist($lang)
    {
        /**
         * 语言是否存在于系统中
         * @param $lang 语言名字
         */

        $file=APP_PATH."/application/library/Lang/{$lang}.ini";
        return preg_match("/[a-z]{1,6}/i",$lang) && file_exists($file);
    }
    static function getLangType(){
         $lang='zhcn';


         if(!empty($_GET['lang']) && self::isExist($_GET['lang']) && preg_match("/[a-z]{1,6}/i",$_GET['lang'])){
             $_SESSION['lang']=$_GET['lang'];
             return $_GET['lang'];
         }
         else if(!empty($_SESSION['lang']) && self::isExist($_SESSION['lang']))
         {
             return $_SESSION['lang'];
         }
        else if(isset($_SESSION['user']['uid']))
        {
            if(class_exists('UserProfileModel') && $_SESSION['user']['uid']>0){
                $profile = new UserProfileModel();
                $country = $profile->getByUser($_SESSION['user']['uid'], array('country'));
                if ($country == '1') {
                    return 'zhcn';

                }
            }

        } else {
            if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && stripos($_SERVER['HTTP_ACCEPT_LANGUAGE'],"en-Us")===0){
                $lang='enus';
            }
        }
        return $lang;

    }
}


