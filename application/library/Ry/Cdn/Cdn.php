<?php

namespace Ry\Cdn;
Class Cdn {
    private $client;
    function __construct($provider,$account){
        $cdn='\Ry\Cdn'.'\\'.$provider.'Cdn';
        if(class_exists($cdn)){
            $this->client=new $cdn($account);
        } else {
            throw Exception("$cdn not exists");
        }

    }

    function exec($path,$type='File'){

        return $this->client->exec($path,$type);
    }

}







