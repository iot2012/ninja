<?php



class FogpodDomain
{
    private $d;

    private $cfg = array('domain_id' => '2929728', 'domain' => '88snow.com', 'ddns' => 'ddns');

    /**
     * @assert ("1","1") == new Dnspod("1","1")
     */
    function __construct($user, $pwd)
    {
        $this->d = new Fogpod($user, $pwd);
    }
    /**
     * @assert ("1","1") == new Dnspod("1","1")
     */
    function record_create($data)
    {

        //公共参数
        //domain_id 域名 ID
        //sub_domain - 主机记录, 如 www
        //record_type 记录类型,通过 API 记录类型获得,大写英文,比如:A
        //record_line 记录线路,通过 API 记录线路获得,中文,比如:默认
        //value - 记录值, 如 IP:200.200.200.200, CNAME: cname.dnspod.com., MX: mail.dnspod.com.
        //mx {1-20} - MX 优先级, 当记录类型是 MX 时有效,范围 1-20
        //ttl {1-604800} - TTL,范围 1-604800,不同等级域名最小值不同

        $defdata = array(
            'domain_id' => $this->cfg['domain_id'],
            'record_line' => '默认',
            'record_type' => 'A',
            'mx' => 10,
            'ttl' => 600

        );
        if (strstr(".", $data['sub_domain']) !== false) {
            exit("wrong format sub domain");
        }
        $data['sub_domain'] = $data['sub_domain'] . "." . $this->cfg['ddns'];
        $data = array_merge($defdata, $data);

        return $this->d->api_call(implode(".", array_map("ucwords", explode('_', __FUNCTION__))), $data);


    }

    function record_modify($data)
    {

        return $this->d->api_call(implode(".", array_map("ucwords", explode('_', __FUNCTION__))), $data);

    }

    function record_remove($data)
    {
        return $this->d->api_call(implode(".", array_map("ucwords", explode('_', __FUNCTION__))), $data);

    }

    function record_ddns($data)
    {
        return $this->d->api_call(implode(".", array_map("ucwords", explode('_', __FUNCTION__))), $data);

    }

    function domain_info($data)
    {
        //$data:domain_id or domain
        //2929728
        return $this->d->api_call(implode(".", array_map("ucwords", explode('_', __FUNCTION__))), $data);

    }

    function record_info($data)
    {
        //$data:domain_id or domain
        //2929728
        return $this->d->api_call(implode(".", array_map("ucwords", explode('_', __FUNCTION__))), $data);

    }

    function record_list($data)
    {
        //  domain_id 域名 ID
        //• offset 记录开始的偏移,第一条记录为 0,依次类推
        //• length 共要获取的记录的数量,比如获取 20 条,则为 20
        //• sub_domain 子域名,可选参数,如果指定则只返回此子域名的记录
        //注意事项:
        //如果域名的记录数量超过了 3000,将会强制分页并且只返回前 3000 条,这时需要通过
        $defdata = array(
            'offset' => 0,
            'length' => 20
        );
        $data = array_merge($defdata, $data);
        return $this->d->api_call(implode(".", array_map("ucwords", explode('_', __FUNCTION__))), $data);


    }
}
