<?php
namespace Domain;
/**
 * Created by JetBrains PhpStorm.
 * User: chjade
 * Date: 3/5/13
 * Time: 11:07 AM
 * To change this template use File | Settings | File Templates.
 */


class DomainFactory
{
    /**
     * @assert ("Redis",array()) === new Redis()
     */

   static function instance($cls,$config)
    {
        $domain="Domain\\".ucfirst($cls)."Domain";
        if(class_exists($domain)){

            return  new $domain($config);
        }

    }
}
