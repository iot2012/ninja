<?php

namespace Domain;

class Fogpod {
    public $grade_list = array(
        'D_Free' => '免费套餐',
        'D_Plus' => '豪华 VIP套餐',
        'D_Extra' => '企业I VIP套餐',
        'D_Expert' => '企业II VIP套餐',
        'D_Ultra' => '企业III VIP套餐',
        'DP_Free' => '新免费套餐',
        'DP_Plus' => '个人专业版',
        'DP_Extra' => '企业创业版',
        'DP_Expert' => '企业标准版',
        'DP_Ultra' => '企业旗舰版',
    );
    public $status_list = array(
        'enable' => '启用',
        'pause' => '暂停',
        'spam' => '封禁',
        'lock' => '锁定',
    );
    private $user;
    private $pwd;
    function __construct($user,$pwd){

        $this->user=$user;
        $this->pwd=$pwd;
    }
    public function api_call($api, $data) {
        if ($api == '' || !is_array($data)) {
            $this->message('danger', '内部错误：参数错误', '');
        }
        $api = 'https://api.fogpod.com/' . $api;
        $data = array_merge($data, array('login_email' => $this->user,
            'login_password' => $this->pwd, 'login_code' => $_SESSION['login_code'],
            'format' => 'json', 'lang' => 'cn', 'error_on_empty' => 'no'));
        $result = $this->post_data($api, $data, $_SESSION['cookies']);
        if (!$result) {
            $this->message('danger', '内部错误：调用失败', '');
        }
        $result = explode("\r\n\r\n", $result);
        if (preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $result[0], $cookies)) {
            $_SESSION['login_code'] = '';
            foreach ($cookies[1] as $key => $value) {
                if (substr($value, 0, 1) == 't') {
                    $_SESSION['cookies'] = $value;
                }
            }
        }
        $results = @json_decode($result[1], 1);
        if (!is_array($results)) {
            return array(false,'内部错误：返回异常');
        }

        if ($results['status']['code'] != 1 && $results['status']['code'] != 50) {
            return array(false,$results['status']['message']);
        }

        return $results;
    }
    public function get_template($template) {
        $text = file_get_contents('./template/' . $template . '.html');
        $master = file_get_contents('./template/index.html');
        $master = str_replace('{{content}}', $text, $master);
        return $master;
    }
    public function message($status, $message, $url=-1) {
        $text = $this->get_template('message');
        $text = str_replace('{{title}}', $status == 'success' ? '操作成功' : '操作失败', $text);
        $text = str_replace('{{status}}', $status, $text);
        $text = str_replace('{{message}}', $message, $text);
        $text = str_replace('{{url}}', $url, $text);
        exit($text);
    }
    private function post_data($url, $data, $cookie='') {
        if ($url == '' || !is_array($data)) {
            $this->message('danger', '内部错误：参数错误', '');
        }
        $ch = @curl_init();
        if (!$ch) {
            $this->message('danger', '内部错误：服务器不支持CURL', '');
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSLVERSION, 1);
        curl_setopt($ch, CURLOPT_COOKIE, $cookie);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_USERAGENT, 'DNSPod API PHP Web Client/1.0.0 (i@likexian.com)');
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}


class DnspodDomain
{
    private $d;

    private $cfg;

    /**
     * @assert ("1","1") == new Dnspod("1","1")
     */
    function __construct($cfg)
    {
        $this->cfg=$cfg;
        $this->d = new Dnspod($cfg['username'], $cfg['password']);
    }
    /**
     * @assert ("1","1") == new Dnspod("1","1")
     */
    function record_create($data)
    {

        //公共参数
        //domain_id 域名 ID
        //sub_domain - 主机记录, 如 www
        //record_type 记录类型,通过 API 记录类型获得,大写英文,比如:A
        //record_line 记录线路,通过 API 记录线路获得,中文,比如:默认
        //value - 记录值, 如 IP:200.200.200.200, CNAME: cname.dnspod.com., MX: mail.dnspod.com.
        //mx {1-20} - MX 优先级, 当记录类型是 MX 时有效,范围 1-20
        //ttl {1-604800} - TTL,范围 1-604800,不同等级域名最小值不同

        $defdata = array(
            'domain_id' => $this->cfg['domain_id'],
            'record_line' => '默认',
            'record_type' => 'A',
            'mx' => 10,
            'ttl' => 600

        );
        if (strstr(".", $data['sub_domain']) !== false) {
            exit("wrong format sub domain");
        }
        $data['sub_domain'] = $data['sub_domain'] . "." . $this->cfg['type'];
        $data = array_merge($defdata, $data);

        return $this->d->api_call(implode(".", array_map("ucwords", explode('_', __FUNCTION__))), $data);


    }

    function record_modify($data)
    {
        $defdata = array(
            'domain_id' => $this->cfg['domain_id'],
            'record_line' => '默认',
            'record_type' => 'A',
            'mx' => 10,
            'ttl' => 600

        );
        $data = array_merge($defdata, $data);
        return $this->d->api_call(implode(".", array_map("ucwords", explode('_', __FUNCTION__))), $data);

    }

    function record_remove($data)
    {
        return $this->d->api_call(implode(".", array_map("ucwords", explode('_', __FUNCTION__))), $data);

    }
    function record_delete($data)
    {
        return $this->record_remove($data);

    }

    function record_ddns($data)
    {
        return $this->d->api_call(implode(".", array_map("ucwords", explode('_', __FUNCTION__))), $data);

    }

    function domain_info($data)
    {
        //$data:domain_id or domain
        //2929728
        return $this->d->api_call(implode(".", array_map("ucwords", explode('_', __FUNCTION__))), $data);

    }

    function record_info($data)
    {
        //$data:domain_id or domain
        //2929728
        return $this->d->api_call(implode(".", array_map("ucwords", explode('_', __FUNCTION__))), $data);

    }

    function record_list($data)
    {
        //  domain_id 域名 ID
        //• offset 记录开始的偏移,第一条记录为 0,依次类推
        //• length 共要获取的记录的数量,比如获取 20 条,则为 20
        //• sub_domain 子域名,可选参数,如果指定则只返回此子域名的记录
        //注意事项:
        //如果域名的记录数量超过了 3000,将会强制分页并且只返回前 3000 条,这时需要通过
        $defdata = array(
            'offset' => 0,
            'length' => 20
        );
        $data = array_merge($defdata, $data);
        return $this->d->api_call(implode(".", array_map("ucwords", explode('_', __FUNCTION__))), $data);


    }
}


