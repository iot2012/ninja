<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 1/2/14
 * Time: 4:57 PM
 */

namespace Zend\Db\Sql;

/**
 * @category   Zend
 * @package    Zend_Db
 * @subpackage Sql
 */
class Replace extends Insert implements SqlInterface, PreparableSqlInterface
{
    const SPECIFICATION_INSERT = 'insert';
    const VALUES_MERGE = 'merge';
    const VALUES_SET   = 'set';
    /**#@-*/

    /**
     * @var array Specification array
     */
    protected $specifications = array(
        self::SPECIFICATION_INSERT => 'REPLACE INTO %1$s (%2$s) VALUES (%3$s)'
    );
}