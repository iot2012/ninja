<?php

/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 7/3/15
 * Time: 9:46 AM
 */
class WechatIot extends Wechat {
    const IOT_API_PREFIX="https://api.weixin.qq.com";
    const DEVICEID_GET_URL="/device/getqrcode";
    const AUTHORIZE_DEVICE_URL="/device/authorize_device";
    const DEVICE_APPLYID_URL="/shakearound/device/applyid";
    const DEVICE_UNBIND_URL="/device/unbind";
    const POI_LIST_URL="/cgi-bin/poi/getpoilist";
    private $userId;
    protected $defDevSet=array(
        "connect_protocol"=>"3|1|2",
        "auth_key"=>"",
        "close_strategy"=>"1",
        "conn_strategy"=>"1",
        "crypt_method"=>"0",
        "auth_ver"=>"0",
        "manu_mac_pos"=>"-1",
        "ser_mac_pos"=>"-2"
    );
    function poiList($begin=0,$limit=50){
        if (!$this->access_token && !$this->checkAuth()) {return false;}

        $url=self::IOT_API_PREFIX.self::POI_LIST_URL."?access_token=".$this->access_token;
        $result=$this->http_post($url,json_encode(array(
            'begin'=>$begin,
            'limit'=>$limit
        )));
        if ($result)
        {

            $json = json_decode($result,true);
            if (!$json || !empty($json['errcode'])) {
                $this->errCode = $json['errcode'];
                $this->errMsg = $json['errmsg'];
                return false;
            }
            return $json;
        }
        return false;

    }
    function __construct($options){
        parent::__construct($options);
        $this->userId=$options['userId'];

    }
    function unbindAction($device){
        /**
         *{
         *   "ticket": "TICKET",
         *   "device_id": "DEVICEID",
         *   "openid": " OPENID"
         *   }
         *
         */




        if (!$this->access_token && !$this->checkAuth()) {return false;}

        $result = $this->http_post(self::IOT_API_PREFIX.self::DEVICE_UNBIND_URL .'?access_token='.$this->access_token,json_encode($device));
        if ($result)
        {

            $json = json_decode($result,true);
            if (!$json || !empty($json['errcode'])) {
                $this->errCode = $json['errcode'];
                $this->errMsg = $json['errmsg'];
                return false;
            }
            return $json;
        }
        return false;
    }
    function deviceApplyid($arr){
        /**
         * {
                   "quantity":3,    
                   "apply_reason":"测试", 
                   "comment":"测试专用",
                   "poi_id":1234    
                }
         */
        $applys=array(
                'quantity'=>1,
                "apply_reason"=>"开发", 
                "comment"=>"测试专用",
                "poi_id"=>288100483

            );

        $applyArr=array_merge($applys,$arr);
        $applyArr=array_intersect_key($applyArr, $applys);
        if (!$this->access_token && !$this->checkAuth()) {return false;}

        $result = $this->http_post(self::IOT_API_PREFIX.self::DEVICE_APPLYID_URL .'?access_token='.$this->access_token,json_encode($applyArr));
        if ($result)
        {

            $json = json_decode($result,true);
            if (!$json || !empty($json['errcode'])) {
                $this->errCode = $json['errcode'];
                $this->errMsg = $json['errmsg'];
                return false;
            }
            return $json;
        }
        return false;

    }
   
    /**
     * @return array|bool|mixed {resp_msg:{"ret_code":0," error_info":"ok"},"deviceid":"XXX","qrticket":"XXX"}
     */
    function getQrcode(){
        if (!$this->access_token && !$this->checkAuth()) return false;

        $result = $this->http_get(self::IOT_API_PREFIX.self::DEVICEID_GET_URL .'?access_token='.$this->access_token);
        if ($result)
        {
            $json = json_decode($result,true);
            if (!$json || !empty($json['errcode'])) {
                $this->errCode = $json['errcode'];
                $this->errMsg = $json['errmsg'];
                return false;
            }
            return $json;
        }
        return false;
    }

    /**
     * "device_num":"1",
        "device_list":[
        {
        "id":"dev1",
        "mac":"123456789ABC",
        "connect_protocol":"1|2",
        "auth_key":"",
        "close_strategy":"1",
        "conn_strategy":"1",
        "crypt_method":"0",
        "auth_ver":"1",
        "manu_mac_pos":"-1",
        "ser_mac_pos":"-2"
        }
        ],
        "op_type":"1"
     * @param $data
     * @return bool
     */
    function buildDevList(){


    }
    function authorizeDevice($devList,$opType=1){

        if (!$this->access_token && !$this->checkAuth()) {
            return false;
        }

        $cnt=count($devList);
        $data=array(
            "device_num"=>$cnt,
            "device_list"=>$devList,
            "op_type"=>$opType
        );
        $result = $this->http_post(self::IOT_API_PREFIX.self::AUTHORIZE_DEVICE_URL.'?access_token='.$this->access_token,self::json_encode($data));
        // {"resp":[{"base_info":{"device_type":"gh_3caf6c97a9dc","device_id":"gh_3caf6c97a9dc_5e41fd65de97968209a62098f7e711c6"},"errcode":0,"errmsg":"ok"}]}
        if ($result)
        {

            $json = json_decode($result,true);
            if (!$json || !empty($json['errcode'])) {
                $this->errCode = $json['errcode'];
                $this->errMsg = $json['errmsg'];
                return false;
            }
            return $json;
        }

        return false;
    }
    function devGroupAdd($names){
        if (!$this->access_token && !$this->checkAuth()) {
            return false;
        }

        if(is_string($names)){
            $names=[$names];
        }
        $ret=[];
        for($i=0;$i<count($names);$i++){
            $data=array(
                "group_name"=>$names[$i],

            );
            $result = $this->http_post("https://api.weixin.qq.com/shakearound/device/group/add".'?access_token='.$this->access_token,self::json_encode($data));
            if ($result)
            {

                $json = json_decode($result,true);
                if (!$json || !empty($json['errcode'])) {
                    $this->errCode = $json['errcode'];
                    $this->errMsg = $json['errmsg'];
                    return false;
                } else {
                    $ret[]=$json;
                }
            } else {
                return false;
            }


        }
        return $ret;



    }
    function groupDevList($groupId,$begin=0,$count=300){
        if (!$this->access_token && !$this->checkAuth()) {
            return false;
        }
        /**
         * $devs 的数组结构类型
         *[
        {
        "device_id":10100,
        "uuid":"FDA50693-A4E2-4FB1-AFCF-C6EB07647825",
        "major":10001,
        "minor":10002
        }
        ]
         */
        $data=['group_id'=>$groupId,'begin'=>$begin,'count'=>$count];

        $result = $this->http_post("https://api.weixin.qq.com/shakearound/device/group/getdetail".'?access_token='.$this->access_token,self::json_encode($data));
        if ($result)
        {

            $json = json_decode($result,true);
            if (!$json || !empty($json['errcode'])) {
                $this->errCode = $json['errcode'];
                $this->errMsg = $json['errmsg'];
                return false;
            } else {
                return $json;
            }
        } else {
            return false;
        }
        /**
         * return response
         *
         {
            "data": {
            "group_id" : 123,
            "group_name" : "test",
            "total_count": 100,
            "devices" :[
            {
            "device_id" : 123456,
            "uuid" : "FDA50693-A4E2-4FB1-AFCF-C6EB07647825",
            "major" : 10001,
            "minor" : 10001,
            "comment" : "test device1",
            "poi_id" : 12345,
            },
            {
            "device_id" : 123457,
            "uuid" : "FDA50693-A4E2-4FB1-AFCF-C6EB07647825",
            "major" : 10001,
            "minor" : 10002,
            "comment" : "test device2",
            "poi_id" : 12345,
            }
            ]
            },
            "errcode": 0,
            "errmsg": "success."
         }
         */

    }
    function delDevs($groupId,$devs){
        if (!$this->access_token && !$this->checkAuth()) {
            return false;
        }
        /**
         * $devs 的数组结构类型
         *[
        {
        "device_id":10100,
        "uuid":"FDA50693-A4E2-4FB1-AFCF-C6EB07647825",
        "major":10001,
        "minor":10002
        }
        ]
         */
        $data=['group_id'=>$groupId,'device_identifiers'=>$devs];

        $result = $this->http_post("https://api.weixin.qq.com/shakearound/device/group/deletedevice".'?access_token='.$this->access_token,self::json_encode($data));
        if ($result)
        {

            $json = json_decode($result,true);
            if (!$json || !empty($json['errcode'])) {
                $this->errCode = $json['errcode'];
                $this->errMsg = $json['errmsg'];
                return false;
            } else {
                return $json;
            }
        } else {
            return false;
        }

    }
    function addDevs($groupId,$devs){
        if (!$this->access_token && !$this->checkAuth()) {
            return false;
        }
        /**
         * $devs 的数组结构类型
         *[
                {
                "device_id":10100,
                "uuid":"FDA50693-A4E2-4FB1-AFCF-C6EB07647825",
                "major":10001,
                "minor":10002
                }
         ]
         */
        $data=['group_id'=>$groupId,'device_identifiers'=>$devs];

        $result = $this->http_post("https://api.weixin.qq.com/shakearound/device/group/adddevice".'?access_token='.$this->access_token,self::json_encode($data));
        if ($result)
        {

            $json = json_decode($result,true);
            if (!$json || !empty($json['errcode'])) {
                $this->errCode = $json['errcode'];
                $this->errMsg = $json['errmsg'];
                return false;
            } else {
                return $json;
            }
        } else {
            return false;
        }

    }
    function devGroupList($begin=0,$end=50){
        if (!$this->access_token && !$this->checkAuth()) {
            return false;
        }

        $data=['begin'=>$begin,'end'=>$end];

        $result = $this->http_post("https://api.weixin.qq.com/shakearound/device/group/getlist".'?access_token='.$this->access_token,self::json_encode($data));
        if ($result)
        {

            $json = json_decode($result,true);
            if (!$json || !empty($json['errcode'])) {
                $this->errCode = $json['errcode'];
                $this->errMsg = $json['errmsg'];
                return false;
            } else {
                return $json;
            }
        } else {
            return false;
        }

    }
    function devGroupDel($ids){
        if (!$this->access_token && !$this->checkAuth()) {
            return false;
        }

        if(!is_string($ids)){
            $ids=[$ids];
        }
        $ret=[];
        for($i=0;$i<count($ids);$i++){
            $data=array(
                "group_id"=>$ids[$i],

            );
            $result = $this->http_post("https://api.weixin.qq.com/shakearound/device/group/delete".'?access_token='.$this->access_token,self::json_encode($data));
            if ($result)
            {

                $json = json_decode($result,true);
                if (!$json || !empty($json['errcode'])) {
                    $this->errCode = $json['errcode'];
                    $this->errMsg = $json['errmsg'];
                    return false;
                } else {
                    $ret[]=$json;
                }
            } else {
                return false;
            }


        }
        return $ret;
    }
    function batchAuthDevice($devList,$opType=1){
            $newArr=array();
            if(!isset($devList[0])){
                $devList=array($devList);
            }
            foreach($devList as &$dev){
                $qrData=$this->getQrcode();

                if(!empty($qrData['deviceid'])){
                    $dev['id']=$qrData['deviceid'];
                    $dev=array_merge($this->defDevSet,$dev);
                    $newArr[]=array_merge($dev,array('qrticket'=>$qrData['qrticket'],'device_type'=>$this->userId));

                }

            }
            $result=$this->authorizeDevice($devList);


            if(!empty($result)){
               return $newArr;

            } else {
                return false;
            }

    }

}
