<?php

class XlsParser {

    static function parseData($path,$lang){
        return self::xls2arr($path,true,$lang);
    }
    
    static function xls2arr($filePath,$hasHeader=false,$lang=array()) {

        $PHPExcel = PHPExcel_IOFactory::load($filePath);

        $currentSheet = $PHPExcel->getSheet(0);  /**取得一共有多少列*/
        $allColumn = $currentSheet->getHighestColumn();     /**取得一共有多少行*/
        $allRow = $currentSheet->getHighestRow();

        $all = array();
        $headers=array();
        if($hasHeader){
            $currentRow=1;


            for($currentColumn='A'; ord($currentColumn) <= ord($allColumn) ; $currentColumn++) {
                $address = $currentColumn . $currentRow;
                $string = trim($currentSheet->getCell($address)->getValue());
                if(!empty($lang)){
                    $string=array_key_exists($string,$lang)?$lang[$string]:$string;
                }
                $headers[] = $string;

            }
            for($currentRow = 2 ; $currentRow <= $allRow ; $currentRow++){
                $flag = 0;
                $col = array();
                for($currentColumn='A'; ord($currentColumn) <= ord($allColumn) ; $currentColumn++){
                    $address = $currentColumn.$currentRow;
                    $string = $currentSheet->getCell($address)->getValue();
                    $col[$headers[$flag]] = (empty($string)?'':$string);
                    $flag++;
                }
                $all[] = $col;
            }
        } else {

            for($currentRow = 1 ; $currentRow <= $allRow ; $currentRow++){
                $flag = 0;
                $col = array();
                for($currentColumn='A'; ord($currentColumn) <= ord($allColumn) ; $currentColumn++){
                    $address = $currentColumn.$currentRow;
                    $string = $currentSheet->getCell($address)->getValue();
                    $col[$flag] = $string;
                    $flag++;
                }
                $all[] = $col;
            }
        }


        return $all;
    }


}