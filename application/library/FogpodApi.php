<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 1/27/15
 * Time: 4:42 PM
 */

class FogpodApi extends \ZCurl {

    protected $api_port;
    protected $host;
    protected $token='';
    protected $redis;

    function __construct($host,$mac='',$cfg='',$redis='',$token='',$needToken=true){


        if($cfg==''){
            $cfg=Yaf\Application::app()->getConfig()->router->toArray();
        }
        $this->api_port=$cfg['api_port'];
        $this->host="http://".$host.":".$this->api_port."/1/";



        if($needToken){
            $this->redis=$redis;
            if($this->redis==''){
                $this->redis=Misc_Utils::redis();
            }
            if($token==''){
                if($mac==''){
                    $this->token=$this->token_create($cfg['ruser'],$cfg["rpwd"],24);
                } else {
                    $this->token=$redis->get("r.$mac");
                    if(!$this->token){
                        $this->token=$this->token_create($cfg['ruser'],$cfg["rpwd"],24);
                        if($this->token){
                            $redis->setex("r.$mac", 7200, $this->token);
                        }
                    }
                }
            }
        }


        parent::__construct('fogpod_cookie');
    }
    function token(){
        return $this->token;
    }
    function cache_token($mac){
        $this->redis->setex("r.$mac", 7200, $this->token);
    }
    function apipwd_set($name,$pwd,$newname,$newpwd){
        //7ca5f2c36d115d37539c7e6b5aed691b5d78229b
       // 6273362deecc78eb9ef5f3c8a31953db802d5154464d6e631aa39893e873ef66
        $data=$this->postJson($this->host."system/adminpwd",array("name"=>$name,"pwd"=>$pwd,"newpwd"=>$newpwd,"newname"=>$newname));

        return $data && ($data['status']==0);
    }

    function cloudserver_set($name,$pwd,$url,$domain_name,$domain_pwd,$hostname,$mode,$newname,$newpwd){
        $step1=$this->ddns_url_set($name,$pwd,$url,$domain_name,$domain_pwd,$hostname);
        $step2=$this->ddns_mode_set($name,$pwd,$mode);
        $data=$this->apipwd_set($name,$pwd,$newname,$newpwd);

        //$data=$this->postJson($this->host."app/cloudserver",array("name"=>$name,"pwd"=>$pwd,"url"=>$url,"domain_name"=>$domain_name,"domain_pwd"=>$domain_pwd,"hostname"=>$hostname,"mode"=>$mode,"newpwd"=>$newpwd,"newname"=>$newname));

        return ($data===true) || ($data && ($data['status']==0));
    }

    function ddns_mode_set($name,$pwd,$mode){

        $data=$this->postJson($this->host."app/ddns_mode",array("name"=>$name,"pwd"=>$pwd,"mode"=>$mode));
        return $data && ($data['status']==0);
    }
    function ddns_url_set($name,$pwd,$url,$domain_name,$domain_pwd,$hostname){

        $data=$this->postJson($this->host."app/ddns_url",array("name"=>$name,"pwd"=>$pwd,"url"=>$url,"domain_name"=>$domain_name,"domain_pwd"=>$domain_pwd,"hostname"=>$hostname));
        return $data && ($data['status']==0);
    }
    function token_create($name,$pwd,$expire=24){
        //
        $data=array();

        $data['pwd']=$pwd;
        $data['name']=$name;
        $data['expire']=$expire;
        $data=$this->postJson($this->host."token",$data);
        if($data===false){
            return $data;
        }
        return $data['token'];
    }

    function mac_get(){

        $data=$this->getJson($this->host."system/mac",array("token"=>$this->token));

        if($data['status']=='0'){

            return $data['mac'];
        } else {
            return false;
        }
    }
    function printer_set($enable,$intMode='wan'){
        $data=$this->postJson($this->host."app/printer",array("enable"=>$enable,'interface_mode'=>$intMode));
        if($data['status']=='0'){
            return $data;
        } else {
            return false;
        }

    }
    function printer_get(){
        $data=$this->getJson($this->host."app/printer_model",array("token"=>$this->token));

        if($data['status']=='0'){
            return $data['model'];
        } else {
            return false;
        }

    }
    function device_list(){

        $data=$this->getJson($this->host."lan/devlist",array("token"=>$this->token));


        if($data['status']=='0'){
            return $data['devices'];
        } else {
            return false;
        }

    }
    function install_app($name,$pwd,$app_id,$app_name,$download_url,$response_url){

        $data=$this->postJson($this->host."app/install_app",array(
            'name'=>$name,'pwd'=>$pwd,'app_id'=>$app_id,'app_name'=>$app_name,
            'download_url'=>$download_url,'response_url'=>$response_url
        ));
        if($data['status']=='0'){
            return $data;
        } else {
            return false;
        }


    }
    static function ipEnd(){
        //return "192.168.1.1:".$this->api_port;
        return $_SERVER['REMOTE_ADDR'].":".$this->api_port;
    }
    static function ip(){

        return "192.168.1.1";
        return $_SERVER['REMOTE_ADDR'];

    }


}


