<?php

class SearchController extends ApplicationController
{

    public $exFields = array('create' => array('uid' => 1));

    public function indexAction()
    {

        $type=$this->r->getParam('type');
        if(!preg_match("/([^\/]+)\.(json|xml|txt|html)$/",$type)){

            if($_SERVER['QUERY_STRING']==''){
                preg_match("/\/([^\/]+)\.(json|xml|txt|html)$/",$_SERVER['REQUEST_URI'],$match);
                $query=urldecode($match[1]);
                $queryType=$match[2];
            }
            else {
                parse_str(preg_replace('/\.(json|xml|txt|html)$/','',$_SERVER['QUERY_STRING']),$query);

            }
            $className=$this->classFromTb($type);
            $searchDb='';
            $tb='';
            $pre='';
            if(class_exists($className)){
                list($searchDb,$tb,$pre)=$this->getTbInfo($className);
                $es=new Esindex($searchDb,$pre.$tb);

                $result=$es->search($query."*");

                exit(json_encode($this->toArray($result->getResults()),JSON_UNESCAPED_UNICODE));
            } else {
                $this->msg="Error request";
                $this->code=-1;
                return $this->m();
                exit;
            }




        } else {
            $this->msg="Error request";
            $this->code=-1;
            return $this->m();

        }
        return false;

    }

    function toArray($result){
        return array_map(function($item){
            $hitData=$item->getHit();
            $data=$item->getData();
            $data['_id']=$hitData['_id'];
            return $data;

        },$result);
    }


}
