<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 12/31/14
 * Time: 5:34 PM
 */

class PodsController extends AdminController {
    function indexAction() {



        $myPod=new MyPodMModel();


        $myPods=$myPod->myPods($_SESSION['user']['uid']);

        $pod=new PodMModel();

        $allPods=$pod->all();



        $cats=new AppCategoryModel();
        $orgCats=$cats->all();
        $cats=array();
        foreach($orgCats as &$item){

            $item['name']=$this->lang[$item['name']];
            $cats[$item['id']]=$item;
        }


        /**
         * 从后端操作的对象映射到到前端的Backbone对象
         */
        $assignArr=array(
            'myPods'=>$myPods,
            'allPods'=>$allPods[1],
            'cats'=>$cats,

            'backboneJs'=>array(
                'c'=>array("allPod"=>array('data'=>json_encode($allPods[0]),
                    'init'=>"{model:Pod,url:'/rest/PodM'}"),
                    "myPod"=>array('data'=>json_encode($myPods),'init'=>"{model:MyPod,url:'/rest/MyPodM'}")),
                'm'=>array("pod"=>array('init'=>json_encode(array('idAttribute'=>PodMModel::$_primary,'urlRoot'=>'/rest/PodM'),JSON_UNESCAPED_SLASHES)),
                    "myPod"=>array('init'=>json_encode(array('idAttribute'=>MyPodMModel::$_primary,'urlRoot'=>'/rest/MyPodM'),JSON_UNESCAPED_SLASHES)))

            )

        );

        $this->v->assign($assignArr);
        return true;
    }

    function backboneInit($data){

    }


}