<?php

class DeviceController extends RootTableController
{
    public $_ztb="DeviceM";
    public  $_tbStyle = 'table';
    public $modelName="DeviceMModel";
    function viewAction(){

        return true;
    }
    function indexAction(){

        $html=<<<EOF
        {
            "type": "phone",
            "title": "Phone",
            "icon": "fa-cog",
            "brands": [
            {
                "name": "Apple",
                "devices": [
                    { "name": "iPhone",   "w": 320, "h": 480,  "inch": "3.5", "pxd": 1 },
                    { "name": "iPhone 4", "w": 640, "h": 960,  "inch": "3.5", "pxd": 2 },
                    { "name": "iPhone 5", "w": 640, "h": 1136, "inch": "4.0", "pxd": 2 }
                ]
            },
            {
                "name": "BlackBerry",
                "devices": [
                    { "name": "Bold 9930",  "w": 480,  "h": 640, "inch": "2.8", "pxd": 1 },
                    { "name": "Q10",        "w": 720,  "h": 720, "inch": "3.1", "pxd": 2 },
                    { "name": "Torch 9810", "w": 480,  "h": 640, "inch": "3.2", "pxd": 1 },
                    { "name": "Torch 9850", "w": 400,  "h": 800, "inch": "3.7", "pxd": 1 },
                    { "name": "Z10",        "w": 1280, "h": 768, "inch": "4.2", "pxd": 2 }
                ]
            },
            {
                "name": "Samsung",
                "devices": [
                    { "name": "Samsung Y",      "w": 240,  "h": 320,  "inch": "3.0", "pxd": 1 },
                    { "name": "Samsung S & S2", "w": 480,  "h": 800,  "inch": "4.5", "pxd": 1.5 },
                    { "name": "Samsung S3",     "w": 720,  "h": 1280, "inch": "4.8", "pxd": 2 },
                    { "name": "Samsung S4",     "w": 1080, "h": 1920, "inch": "5",   "pxd": 3 }
                ]
            },
            {



                "name": "HTC",
                "devices": [
                    { "name": "Desire 200", "w": 320,  "h": 480,  "inch": "3.5", "pxd": 1 },
                    { "name": "Desire X",   "w": 480,  "h": 800,  "inch": "4.0", "pxd": 1.5 },
                    { "name": "Desire SV",  "w": 480,  "h": 800,  "inch": "4.3", "pxd": 1.5 },
                    { "name": "Sensation",  "w": 540,  "h": 960,  "inch": "4.5", "pxd": 1.5 },
                    { "name": "One",        "w": 1080, "h": 1920, "inch": "4.7", "pxd": 3 }
                ]
            },
            {
                "name": "LG",
                "devices": [
                    { "name": "Optimus L5",    "w": 320,  "h": 480,  "inch": "4.0", "pxd": 1 },
                    { "name": "Optimus 3D",    "w": 480,  "h": 800,  "inch": "4.3", "pxd": 1.5 },
                    { "name": "Optimus 4X HD", "w": 720,  "h": 1280, "inch": "4.7", "pxd": 2 },
                    { "name": "Optimus G Pro", "w": 1080, "h": 1920, "inch": "5.5", "pxd": 3 },
                    { "name": "Nexus 4",       "w": 768,  "h": 1280, "inch": "4.7", "pxd": 2 }
                ]
            }
        ]
    }
EOF;
        $devices=$html;
        $this->v->assign("devices",$devices);
        $this->read($_REQUEST,$_SESSION['user'],$this->modelName());
        return true;
    }


}