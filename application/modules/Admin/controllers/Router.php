<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 3/2/15
 * Time: 8:06 PM
 */
class RouterController extends AdminController
{

    public $R;

    function init()
    {

        parent::init();
    }

    function deviceListAction(){


    }
    function sendPrintAction(){
        $mid=$_POST['mid'];

        $this->redis();
        $media=new MediaModel();
        $media=$media->findOne(array('mid'=>$mid,'uid'=>$_SESSION['user']['uid']),array('href'));
        $router=new RouterMModel();
        $routerData=$router->findOne(array('uid'=>$_SESSION['user']['uid']));
        $routerData['ruser']=$routerData['api_user'];
        $routerData['rpwd']=$routerData['password'];
        if(!empty($media) && !empty($routerData)){

            $fg=new FogpodApi($routerData['ip'],$routerData['mac'],$this->c->router->toArray());
            $fg->printer_set(1);
            $model=$fg->printer_get();

            if($model['status']=='0'){

                if(!empty(PrinterDriverMModel::indexes())){
                    $db=PrinterDriverMModel::$_db;
                    $tb=PrinterDriverMModel::$_tbName;
                    $esc=new Esindex($db,$tb);
                    // only match this model
                    $result=$esc->searchObj('"'.$model['model'].'"');
                    if(!empty($result))
                        if(count($result)>0){
                            $file=$result[0]['file'];
                        }

                    } else {
                        $this->code=-1;
                        $this->msg="cant_search";
                        return $this->m();
                    }
            }
            $this->redis->publish('cloud_print',json_encode(
                array('url'=>($this->c->application->upload->media.$media['href']),'mac'=>$_SESSION['user']['mac'],'ip'=>FogpodApi::ip(),'port'=>'9100','name'=>'p_'.$_SESSION['user']['username'],'driver_file'=>$file)

            ));
            $this->data=array('status'=>1);
        } else {
            $this->code=-1;
            $this->msg='file_not_exist';
        }

        return $this->m();
    }
}