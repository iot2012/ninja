<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 3/27/14
 * Time: 9:30 AM
 */
//总的注册数
class IftttController extends AdminController {

    public $title="ifttt";
    public $model="ifttt";
    function getTempsAction(){


        return $this->m();

    }
    function getComps(){

        $comp=new CompDataMModel();
        $comps=$comp->find();
        $comps=[
            [],
            [],
            [],

            [],

            [],


        ];


        return $comps;

    }

    function getSource($id){

    }
    public function indexAction(){

        $tempCats=array(
            array('name'=>'business','desc'=>"汽车模板",'ctime'=>1435995040,'uid'=>5,'id'=>8),
            array('name'=>'ios','desc'=>"房产模板",'ctime'=>1435995040,'uid'=>5,'id'=>7),
            array('name'=>'note_toolkit','desc'=>"汽车模板",'ctime'=>1435995040,'uid'=>5,'id'=>8),
            array('name'=>'android','desc'=>"装修模板",'ctime'=>1435995040,'uid'=>5,'id'=>6),
            array('name'=>'linked_car','desc'=>"旅游模板",'ctime'=>1435995040,'uid'=>5,'id'=>5),
            array('name'=>'linked_home','desc'=>"婚纱模板",'ctime'=>1435995040,'uid'=>5,'id'=>4),
            array('name'=>'lifestyle','desc'=>"机械模板",'ctime'=>1435995040,'uid'=>5,'id'=>5),
            array('name'=>'fit_wear','desc'=>"招商模板",'ctime'=>1435995040,'uid'=>5,'id'=>4),
            array('name'=>'music','desc'=>"法律模板",'ctime'=>1435995040,'uid'=>5,'id'=>3),
            array('name'=>'photo_video','desc'=>"金融模板",'ctime'=>1435995040,'uid'=>5,'id'=>2),
            array('name'=>'prod','desc'=>"餐饮模板",'ctime'=>1435995040,'uid'=>5,'id'=>1),
            array('name'=>'social_net','desc'=>"医疗模板",'ctime'=>1435995040,'uid'=>5,'id'=>4),
            array('name'=>'news_sports','desc'=>"美容模板",'ctime'=>1435995040,'uid'=>5,'id'=>10),

        );

        $tempCats=array_map(function($item){
            $item['en_name']=$item['name'];
            $item['name']=empty($this->lang[$item['name']])?$item['name']:$this->lang[$item['name']];
            return $item;
        },$tempCats);
        $temps=$this->getSource(1);
        $comps=$this->getComps();
        $topic=new TopicMModel();
        $allTopics=$topic->find(array(),999);
        $topics=array();
        $default=$this->lang['default'];
        foreach($allTopics as &$item){
            $catName=$this->lang[$item['cat_name']]?$this->lang[$item['cat_name']]:$item['cat_name'];

            if(empty($item['cat_name'])){
                $topics[$default][]=$item;
            } else {
                $topics[$catName][]=$item;
            }

        }
        $assignArr=array('tempCats'=>$tempCats,'temps'=>$temps,'allTopics'=>$topics);

        $this->v->assign($assignArr);
        return true;

    }


}