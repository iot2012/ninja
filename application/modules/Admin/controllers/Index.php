<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 7/9/14
 * Time: 3:47 PM
 */


class IndexController extends AdminController {

    /**
     * @resource 后台登录
     *
     * @return bool
     */

    public $site=array(
        'mall'=>array(
            'logo'=>array('front'=>"商家管理系统",'backend'=>'商家管理系统'),
            'menus'=>array('leftNav'=>array(
                'Uis'=>array('href'=>'','subs'=>array('General'=>array('href'=>'')))

            ))

        )

    );

    function indexAction($name = "Stranger") {
        /**
         * 首页登陆action
         */
        if($_SESSION['user']['level']==10){
            //header('Location: /admin/apps/index?menuid=25&type=sys');
            // exit;
        }


//        setcookie('token','',(time()-3600),'/');
//        setcookie('uid','',(time()-3600),'/');
//
//        $notify=new NotificationModel();
//        $notifyRes=$notify->getNewNotify();
//
//        $notify=json_encode($notifyRes);
//
//        $notify_count=count($notifyRes);
//
//
//        /**
//         *
//         * 获取通知
//         */


        //$tokens=$this->initToken();
        //$this->initAdmPage();
        //$tempData=array('notify'=>$notify,'notify_count'=>$notify_count,'openIdJson'=>$tokens);
       // $tempData['jsLib']=array('seajs'=>false);
        $tempData=array('data'=>array(
            'pv'=>100,
            'apple_num'=>79,
            'follows'=>121323,
            'today_visit'=>1200,
            'orders'=>19721,
            'trans'=>129,
            'repeated_visitors'=>2386

        ),
            'splash'=>array(
                'title'=>'Ninja后台管理系统',
                'content'=>'Ninja后台管理系统','img'=>'loading-bars.svg'
            )
        );
        if($_SESSION['user']['_is_new']){

            $this->forward("Admin","Page","index");
            $this->redirect("/admin/page/index/?ztb=Page&action=add&menuid=34");
            exit;
            return false;
        }

        $this->v->assign( $tempData);



        return TRUE;
    }

    public function testAction($name = "Stranger")
    {
        /**
         * 首页登陆action
         */

        $site=$this->site['mall'];
        setcookie('token','',(time()-3600),'/');
        setcookie('uid','',(time()-3600),'/');
        $info=new InfoModel();
        $news=json_encode($info->getNews());
        $notify=new NotificationModel();
        $notifyRes=$notify->getNewNotify();

        $notify=json_encode($notifyRes);

        $notify_count=count($notifyRes);

        $service=json_encode($info->getService());

        /**
         *
         * 获取通知
         */
        $openIds=new OpenidTokenModel();
        $tokens=$this->initToken();

        $this->v->assign(array(  'notify'=>$notify,'notify_count'=>$notify_count,'openIdJson'=>$tokens));
        if($_GET['code']){
            $discussGroup=new DiscussGroupModel();
            $discuss=$discussGroup->findOne(array("code"=>$_GET['code']));
            if(!empty($discuss)){

                $this->redirect('/meeting/landing');
            }


        }


        return TRUE;
    }
    public function showLoginAction(){

        if(isset($_GET['r']) && strstr($_GET['r'],$_SERVER['HTTP_HOST'])===false)
        {
            $this->v->assign("referer",$_GET['r']);

        }

        return true;


    }
    public function ifAction(){

        $this->initAdmPage();

        $ifUrl=$_GET['url'];

        $this->v->assign("ifUrl",$ifUrl);
        return true;

    }
    function deployAction(){
        $this->initAdmPage();
        return true;
    }
     function initToken(){

        $openIds=new OpenidTokenModel();
        $tokens=$openIds->getTokens($_SESSION['user']['uid']);
        $openIdCfg=$this->c['openid']->toArray();


        $tokens=array_map(function($token) use($openIdCfg){
            $_SESSION['token'][$token['provider']]=array($openIdCfg[$token['provider']]['atokenName']=>$token['atoken']);
            unset($token['atoken'],$token['rtoken']);
            return $token;
        },$tokens);

        return json_encode($tokens);
    }
     function p2pAction()
    {


        return true;
    }
    public function avAction()
    {


        return true;
    }
    public function dataAction()
    {


        return true;
    }
    public function videoAction(){


        return true;
    }
    public function mvcAction(){

        return true;

    }

     function searchAction(){
        exit('');
        return false;
    }

    public function uploadAction(){

        /**
         * 上传文件
         */
        $options = array(
            'upload_dir' =>__DIR__."/../data/upload/");
        $upload_handler = new Upload_Handler($options);

        return false;
    }
    public function friendvideoAction(){

        return true;
    }

    function vconfAction(){

        return true;
    }

    public function groupChatAction(){


        return false;
    }
    public function fileShareAction(){

        return true;
    }
    public function tAction(){


        return true;

    }
    public function vcAction(){


        return true;

    }
    public function routeAction(){

        var_dump($this->r->getParams());
        return false;

    }


}
