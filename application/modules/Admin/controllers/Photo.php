<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 4/13/15
 * Time: 10:13 AM
 */

class PhotoController extends RootTableController{

    public $modelName="PhotoMModel";
    public $_ztb="PhotoM";
    public  $_tbStyle = 'grid';
    public $_batchOpMenus=array(
        'share'=>array('cfg'=>array('icon_url'=>'glyphicon glyphicon-share','name'=>'share'))
    );
    function albumId(){
        $aid=$_GET['aid'];
        if(empty($_GET['aid'])){
            $aid=1;
        }
        return $aid;
    }
    function setLastHMenu($name){

        if(empty($_SESSION['site']['menus']['hmenus'][9999])){
            array_pop($_SESSION['site']['menus']['hmenus']);
        }
        $_SESSION['site']['menus']['hmenus'][9999]=array(
            'id'=>'999999',
            'product_name' =>'fogpod',
            'name' => $name,
            'type'=>0
        );

        $this->v->assign('site',$_SESSION['site']);

    }
    function indexAction(){

        $aid=$this->albumId();
        $albumModel=new AlbumMModel();
        $album=$albumModel->findById($aid);
        $this->setLastHMenu($album['title']);


        if(!empty($album['privacy'])){
            if($this->canView($album,$_SESSION['user']['uid'])){
                $photoModel=new PhotoMModel();
                if(!empty($album['photo_ids'])){
                    $photos=$photoModel->findIn(["_id"=>$album['photo_ids']]);
                    if(!empty($photos)){
                        $whereCond=PhotoMModel::buildIn($album['photo_ids']);
                        $this->read($_REQUEST,$_SESSION['user'],$this->modelName(),$whereCond);
                    }

                } else {
                    $this->read($_REQUEST,$_SESSION['user'],$this->modelName(),array("_id"=>-1));
                }




            }

        } else {
            $this->read($_REQUEST,$_SESSION['user'],$this->modelName());
        }

        return true;


    }

    function canView($album,$uid){
        //1:public
        if($album['privacy']==1 || $album['privacy']==2 && $album['uid']==$uid){

            return true;
        }
        return false;
    }

} 