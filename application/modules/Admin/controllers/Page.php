
<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 3/27/14
 * Time: 9:30 AM
 */


class PageController extends AdminController {

    public $model="PageM";
    public $exFields=array('create'=>array('uid'=>1));


//'http://apps.bdimg.com/store/static/kvt/81c1c2aa946bf110100968f30f2080f9.jpg',
//'http://apps.bdimg.com/store/static/kvt/10b3643c0c61ee2f934f0ba65a3a31e8.jpg',
//'http://apps.bdimg.com/store/static/kvt/258540a58c82bf5d49422a62217027ff.jpg',
//'http://apps.bdimg.com/store/static/kvt/ab26fdce281e3697a519996ec6ef7684.jpg'
//

    function userData(){


    }
    function isFileField($key){
        return preg_match("/[\w-]+_file$/",$key)!==false;

    }
    function getTempLang($name,$id,$langType='zhcn'){

        $ini=parse_ini_file("templates/$name/$id/config/locale/$langType.ini",true);

        return $ini;
    }
    function isExcel($ext){
        return in_array($ext,array(
            'xls','xlsx'
        ));
    }
    function extractPic($zipFile,$data){



        $zip = new ZipArchive;
        $res = $zip->open(ltrim($zipFile,"/"));
        $entryName=$zip->getNameIndex(1);

        $entry=preg_replace("/\/[^\/]+$|\/$/i",'',$entryName);

        if($entry==$entryName){
            $zip->extractTo($data['_imgdir']);
        } else {
            $zip->extractTo($data['_imgdir'],$entry);
        }

        if(!is_dir($data['_imgdir'])){
            mkdir($data['_imgdir'],0777,true);
        }
        $zip->extractTo($data['_imgdir']);
    }

    function page_editAction(){

    }
    function formAction(){
        /**
         * 创建页面
         */
        $data['tmpId']=$_POST['_tmpId'];
        $data['_type']='site';
        $data['tmpName']=$_POST['_tmpName'];
        $data['_id']=$_POST['_siteId'];
        $secId=preg_replace('/_$/','',$_POST['_cfg_section']);

        $action=$_POST['_action_'];
        unset($_POST['_action_'],$_POST['_cfg_section'],$_POST['_tmpId'],$_POST['_tmpName'],$_POST['_siteId']);
        $data[$secId]['fields']=$_POST;
        $data['uid']=intval($_SESSION['user']['uid']);
        foreach($_POST as $key=>$item){
            if($this->isFileField($key)){
                $ext=pathinfo($item,PATHINFO_EXTENSION);
                if($this->isExcel($ext)){
                    $xlsData=XlsParser::parseData(trim($item,"/"),array_flip($this->getTempLang($data['tmpName'],$data['tmpId'])));
                    $data['_info_'.$key]=$xlsData;
                } else if($ext=='zip') {
                    $date=date("ymd");
                    $data['_imgdir']="templates/{$data['tmpName']}/{$data['tmpId']}/data/$date/{$data['uid']}";
                    $this->extractPic($item,$data,$_SESSION['user']['id']);
                }
            }
        }
        if($action=='gen_app'){
            /**
             * 1->提交
             * 2->审核
             *
             */
            $data['app_status']=1;
        }


        /**
         * tmpId 模板Id
         */
        if(Validator::isReg("mongoid",$data['_id'])){


            $this->data=["_id"=>$this->updatePage($data)];


        } else {
            $this->code=-1;
            $this->msg="err_fmt_data";
        }

        return $this->m();


    }
    function updatePage($data){
        $site=new SiteMModel();
        $query=array('_id'=> new MongoId($data['_id']));
        unset($data['_id']);
        $set=array('$set'=>$data);

        if(isset($query['_id'])){

            return $site->update($set,$query);
        } else {
            return $site->add($data);
        }

    }
    function genQrCacheAction(){

        $content=$_POST['content'];
        $type=$_POST['type'];
        $date=date("ymdH");
        $dir="cache/temps/$date/";
        if(!is_dir($dir)){
            mkdir($dir,0777,true);
        }
        $fileName=$dir.uniqid($_SESSION['user']['uid']."_").".html";

        $assignArr=['content'=>$content,'type'=>$type];
        $this->v->assign($assignArr);
        $result=$this->v->render("page/genqrcache.phtml");

        file_put_contents($fileName,$result);
        $this->data=array('url'=>"http://".$_SERVER['HTTP_HOST']."/".$fileName);
        return $this->m();
    }
    function getTempCats($type){

    $temps= [

            'site'=>array(
                array('name'=>'general_retail','desc'=>"通用零售",'ctime'=>1435995040,'uid'=>5,'id'=>7),
                array('name'=>'institution','desc'=>"企业机构",'ctime'=>1435995040,'uid'=>5,'id'=>6),
                array('name'=>'dinning','desc'=>"餐饮",'ctime'=>1435995040,'uid'=>5,'id'=>1)
            ),
            'page'=>array(
                array('name'=>'red_pocket','desc'=>"红包",'ctime'=>1435995040,'uid'=>5,'id'=>8),
                array('name'=>'quick_buy','desc'=>"闪购",'ctime'=>1435995040,'uid'=>5,'id'=>9),
                array('name'=>'promotion','desc'=>"促销",'ctime'=>1435995040,'uid'=>5,'id'=>11),
                array('name'=>'coupon','desc'=>"优惠券",'ctime'=>1435995040,'uid'=>5,'id'=>12)
            )
    ];
        return isset($temps[$type])?$temps[$type]:$temps['site'];




//            array(
//                array('name'=>'mobile','desc'=>"汽车模板",'ctime'=>1435995040,'uid'=>5,'id'=>8),
//                array('name'=>'residential','desc'=>"房产模板",'ctime'=>1435995040,'uid'=>5,'id'=>7),
//                array('name'=>'decoration','desc'=>"装修模板",'ctime'=>1435995040,'uid'=>5,'id'=>6),
//                array('name'=>'trip','desc'=>"旅游模板",'ctime'=>1435995040,'uid'=>5,'id'=>5),
//                array('name'=>'wedding','desc'=>"婚纱模板",'ctime'=>1435995040,'uid'=>5,'id'=>4),
//                array('name'=>'merchants','desc'=>"招商模板",'ctime'=>1435995040,'uid'=>5,'id'=>4),
//                array('name'=>'law','desc'=>"法律模板",'ctime'=>1435995040,'uid'=>5,'id'=>3),
//                array('name'=>'finance','desc'=>"金融模板",'ctime'=>1435995040,'uid'=>5,'id'=>2),
//                array('name'=>'dinning','desc'=>"餐饮模板",'ctime'=>1435995040,'uid'=>5,'id'=>1),
//                array('name'=>'treatment','desc'=>"医疗模板",'ctime'=>1435995040,'uid'=>5,'id'=>4),
//                array('name'=>'beauty','desc'=>"美容模板",'ctime'=>1435995040,'uid'=>5,'id'=>10),
//                array('name'=>'art','desc'=>"文体模板",'ctime'=>1435995040,'uid'=>5,'id'=>9),
//                array('name'=>'drugstore','desc'=>"药店模板",'ctime'=>1435995040,'uid'=>5,'id'=>11),
//                array('name'=>'poster','desc'=>"海报模板",'ctime'=>1435995040,'uid'=>5,'id'=>12),
//                array('name'=>'general','desc'=>"通用模板",'ctime'=>1435995040,'uid'=>5,'id'=>13),
//                array('name'=>'machine','desc'=>"机械模板",'ctime'=>1435995040,'uid'=>5,'id'=>14),
//                array('name'=>'edu','desc'=>"教育模板",'ctime'=>1435995040,'uid'=>5,'id'=>15)
//
//            );
    }
    function createBackboneCls($pages){

        return array(
            'c'=>array(

                "page"=>array(
                    'data'=>json_encode($pages),
                    'init'=>"{model:PageModel,url:'/rest/PageM'}"
                ),


            ),

            'm'=>array(
                "comp"=>array(
                    'init'=>json_encode(array('idAttribute'=>PageCompMModel::$_primary,'urlRoot'=>'/rest/PageCompM'),JSON_UNESCAPED_SLASHES)
                ),
                "pageModel"=>array(
                    'init'=>json_encode(array(
                        'idAttribute'=>PageMModel::$_primary,
                        'urlRoot'=> '/user',
                        'url'=>'/admin/page/list.json',
                        'urls'=>[
                            'read'=> '/admin/page/get.json',
                            'create'=> '/admin/page/create.json',
                            'update'=> '/admin/page/update.json',
                            'patch'=> '/admin/page/updatecontent.json',
                            'delete'=> '/admin/page/delete'
                        ],
                        'urlRoot'=>'/rest/PageM'
                    ),JSON_UNESCAPED_SLASHES)
                )
            ),

        );
    }

    function editorAction(){



        if($_GET['type']=='page'){
            $temps=$this->getPageTemps(1);
            //var_dump($temps);
        } else {
            $temps=$this->getSiteTemps(1);
        }
        $temps=$this->fmtMsgPluck($this->lang,$temps,['_cat_name=>cat_name'],'cat_name');
        $pages=[];
        $assignArr=array('tempCats'=>$this->fmtMsgArr($this->lang,$this->getTempCats($_GET['type']),["_name"=>"name"]),'temps'=>$temps);

        $assignArr['backboneJs']=$this->createBackboneCls($pages);

//        if($_GET['type']=='page' || $_GET['type']=='p'){
//            $tmpName=$_GET['tmpName'];
//            $tmpId=$_GET['tmpId'];
//            $controller=strtolower($this->r->controller);
//            $actionJs=file_exists("{$this->assetPrefix}js/{$controller}/custom_editor_page.js")?("/{$this->assetPrefix}js/{$controller}/custom_editor_page"):"";
//            $assignArr['actionJs']=$actionJs;
//            $assignArr['tmpName']=$tmpName;
//            $assignArr['tmpId']=$tmpId;
//            $assignArr['actionJs']=$actionJs;
//            $assignArr['ui']=$_GET['ui'];
//            $this->v->assign($assignArr);
//
//            echo $this->v->render("page/custom_editor_page.phtml");
//
//            return false;
//        }

        $comps=$this->getComps();

        $assignArr['backboneJs']['c']["comp"]=array(
            'data'=>json_encode($comps),
            'init'=>"{model:Comp,url:'/rest/PageCompM'}"
        );





        $assignArr['comps']=$this->fmtMsgPluck($this->lang,$comps,['_name'=>'name','cat'],'cat');

        if(empty($_GET['pid']) && $_GET['action']!='add'){

            exit('拒绝访问');

            return false;
        }

        $pageJson='';
        if($_GET['action']=='add' && $_GET['ztb']=='Page'){
            $page=new PageModel();
            $pid=$page->add(array('title'=>'双击编辑页面名字','uid'=>$_SESSION['user']['uid'],'content'=>''));


            $page=array('id'=>$pid,'title'=>'双击编辑页面名字','utime'=>time(),'ctime'=>time(),'uid'=>$_SESSION['user']['uid'],'lang'=>'zhcn','ib_str'=>'');

            $_SESSION['user']['page']=$pid;
            $pageJson=json_encode($page);
            $assignArr['pageJson']=$pageJson;
            $assignArr['page']=$page;

            $_SESSION['user']['pid']=$pid;

        } else if($_GET['action']=='edit' && $_GET['ztb']=='Page' && $_GET['pid']){
            $page=new PageModel();
            $_SESSION['user']['pid']=intval($_GET['pid']);
            $_SESSION['user']['page']=$_GET['pid'];
            $page=$page->findOne(array('id'=>intval($_GET['pid']),'uid'=>$_SESSION['user']['uid']));
            $page['title']=trim($page["title"]);
            $page['title']=empty($page['title'])?"双击编辑页面名字":$page["title"];
            $ib=new IbeaconModel();
            $ibeacon=$ib->findOne(array('id'=>$page['ib_id']));

            $pageJson=json_encode($page);
            $page['ib_str']=implode("-",array($ibeacon['alias'],$ibeacon['major'],$ibeacon['minor']));

            if(empty($page)){
                $this->code=-1;
                $this->msg="错误请求";
                exit;
            }
            $assignArr['pageJson']=$pageJson;
            $assignArr['page']=$page;

        }
        //var_dump($assignArr['backboneJs']);exit;
        $this->v->assign($assignArr);
        return true;

    }

    public function previewAction(){


        return true;

    }
    function getPageTemps($id){

        /**
         *  ['comps'=>[
        'label'=>[
        'cfg_json'=>"",
        'app_data'=>''
        ]

        ],
        'page_name'=>"",
        'url'=>[],
        'html'=>'',
        'pic_url'=>'http://apps.bdimg.com/store/static/kvt/81c1c2aa946bf110100968f30f2080f9.jpg'

        ],
         *
         *
         */
        $temps=array(

            [
                'pages'=>array(
                    [
                        //每一页的文字

                        'name'=>"红包",
                        'pic_url'=>'/img/temps/preview/red_pocket_1_1.jpg'

                    ]

                ),
                'edit_html'=>'/page/edit?type=red_pocket&name=red_pocket',
                'name'=>'红包',
                'desc'=>'红包的描述',
                'cat_name'=>'red_pocket',
                'type'=>'page',
                'ui'=>'aui',
                '_id'=>'2',

            ]

        );
        return $temps;
    }

    function getSiteTemps($id){

        /**
         *  ['comps'=>[
        'label'=>[
        'cfg_json'=>"",
        'app_data'=>''
        ]

        ],
        'page_name'=>"",
        'url'=>[],
        'html'=>'',
        'pic_url'=>'http://apps.bdimg.com/store/static/kvt/81c1c2aa946bf110100968f30f2080f9.jpg'

        ],
         *
         *
         */
        $temps=array(
           [
            'pages'=>array(
                [
                //每一页的文字




                'name'=>"餐饮首页",
                'pic_url'=>'/img/temps/preview/dinning_1_1.jpg'

                ],
                [

                    'name'=>"购物车",
                    'pic_url'=>'/img/temps/preview/dinning_1_4.jpg'

                ],
                [

                    'name'=>"全部分店",

                    'pic_url'=>'/img/temps/preview/dinning_1_2.jpg'

                ],
                [

                    'name'=>"分店",

                    'pic_url'=>'/img/temps/preview/dinning_1_3.jpg'

                ],

            ),
            'edit_html'=>'/page/edit?type=retailer&name=dinning',
            'name'=>'餐厅模版',
            'en_name'=>'dinning',
            'desc'=>'只有一家分店的餐饮描述',
            'cat_name'=>'dinning',
               'type'=>'site',
               'ui'=>'aui',
            '_id'=>'1',

           ],
            [
                'pages'=>array(
                    [
                        //每一页的文字




                        'name'=>"企业公告通知",
                        'pic_url'=>'/img/temps/preview/workplace_1_1.jpg'

                    ],
                    [

                        'name'=>"企业餐厅",
                        'pic_url'=>'/img/temps/preview/workplace_1_4.jpg'

                    ],
                    [

                        'name'=>"考勤打卡",

                        'pic_url'=>'/img/temps/preview/workplace_1_2.jpg'

                    ],
                    [

                        'name'=>"考勤记录",

                        'pic_url'=>'/img/temps/preview/workplace_1_3.jpg'

                    ],

                ),
                'edit_html'=>'/page/edit?type=workplace&name=workplace',
                'name'=>'中小企业',
                'desc'=>'中小企业模版描述',
                'cat_name'=>'institution',
                'type'=>'site',
                'ui'=>'aui',
                '_id'=>'3',

            ]
        );
        return $temps;
    }


    function getTempsAction(){


        return $this->m();

    }
//.icon-wenben:before { content: "\e601"; }
//.icon-fenxiang:before { content: "\e612"; }
//.icon-dianhua:before { content: "\e613"; }
//.icon-ditu:before { content: "\e614"; }
//.icon-liuyanban:before { content: "\e615"; }
//.icon-tongxunlu:before { content: "\e616"; }
//.icon-uislide:before { content: "\e603"; }
//.icon-tupian:before { content: "\e604"; }
//.icon-daohang:before { content: "\e605"; }
//.icon-shipin:before { content: "\e607"; }
//.icon-fenlan:before { content: "\e617"; }
//.icon-lianjie:before { content: "\e608"; }
//.icon-choujiang:before { content: "\e619"; }
//.icon-tongji:before { content: "\e61a"; }
//.icon-html:before { content: "\e609"; }
//.icon-fengefu:before { content: "\e61c"; }
//.icon-tuji:before { content: "\e60b"; }
//.icon-liebiao:before { content: "\e60c"; }
//.icon-tables:before { content: "\e60e"; }
//.icon-shangwuhezuo:before { content: "\e61e"; }
//.icon-duanxin:before { content: "\e61f"; }
//.icon-anniu:before { content: "\e60f"; }
//.icon-biaoti:before { content: "\e60d"; }
//.icon-shangpin:before { content: "\e610"; }
//.icon-tab:before { content: "\e611"; }

      function getComps(){
        $comp=new PageCompMModel();
        $comps=$comp->find(array(),200,array(
            'cat',"_id","icon_url","name"
        ));
//        $comps=[
//            ['icon_url'=>'iconfont icon-wenben','name'=>'text','html'=>'','cfg_html'=>'','cat'=>'general','uid'=>18,'_id'=>"123"],
//            ['icon_url'=>'iconfont icon-fenxiang','name'=>'share','html'=>'','cfg_html'=>'','cat'=>'general','uid'=>18,'_id'=>"5"],
//            ['icon_url'=>'iconfont icon-uislide','name'=>'slide','html'=>'','cfg_html'=>'','cat'=>'general','uid'=>18,'_id'=>"8"],
//            ['icon_url'=>'iconfont icon-tupian','name'=>'tupian','html'=>'','cfg_html'=>'','cat'=>'general','uid'=>18,'_id'=>"9"],
//            ['icon_url'=>'iconfont icon-lianjie','name'=>'link','html'=>'','cfg_html'=>'','cat'=>'general','uid'=>18,'_id'=>"11"],
//            ['icon_url'=>'iconfont icon-tuji','name'=>'album','html'=>'','cfg_html'=>'','cat'=>'general','uid'=>18,'_id'=>"13"],
//            ['icon_url'=>'iconfont icon-liebiao','name'=>'article_list','html'=>'','cfg_html'=>'','cat'=>'general','uid'=>18,'_id'=>"45"],
//            ['icon_url'=>'iconfont icon-tables','name'=>'table','html'=>'','cfg_html'=>'','cat'=>'general','uid'=>18,'_id'=>"56"],
//            ['icon_url'=>'iconfont icon-anniu','name'=>'anniu','html'=>'','cfg_html'=>'','cat'=>'general','uid'=>18,'_id'=>"67"],
//            ['icon_url'=>'iconfont icon-biaoti','name'=>'title','html'=>'','cfg_html'=>'','cat'=>'general','uid'=>18,'_id'=>"37"],
//            ['icon_url'=>'iconfont icon-shangpin','name'=>'product','html'=>'','cfg_html'=>'','cat'=>'general','uid'=>18,'_id'=>"38"],
//            ['icon_url'=>'iconfont icon-daohang','name'=>'nav','html'=>'','cfg_html'=>'','cat'=>'general','uid'=>18,'_id'=>"39"],
//            ['icon_url'=>'iconfont icon-tongji','name'=>'statistics','html'=>'','cfg_html'=>'','cat'=>'global','uid'=>18,'_id'=>"34"],
//            ['icon_url'=>'iconfont icon-shangwuhezuo','name'=>'shangwuhezuo','html'=>'','cfg_html'=>'','cat'=>'global','uid'=>18,'_id'=>"14"],
//            ['icon_url'=>'iconfont icon-tab','name'=>'tab','html'=>'','cfg_html'=>'','cat'=>'cover','uid'=>18,'_id'=>"34"],
//            ['icon_url'=>'iconfont icon-tab','name'=>'text','html'=>'','cfg_html'=>'','cat'=>'cover','uid'=>18,'_id'=>"2"],
//            ['icon_url'=>'iconfont icon-wenben','name'=>'text','html'=>'','cfg_html'=>'','cat'=>'cover','uid'=>18,'_id'=>"1"],
//            ['icon_url'=>'iconfont icon-wenben','name'=>'haibao','html'=>'','cfg_html'=>'','cat'=>'cover','uid'=>18,'_id'=>"99"],
//
//
//
//        ];

       return $comps;

    }
    function getPageAction(){
        $uid=$_SESSION['user']['uid'];
        $tmpType=$_REQUEST['service'];


        if($tmpType=='page'){
            $sm=new SiteMModel();
            $result=array($sm->findOne(array('uid'=>$uid, "title"=> array('$exists' => true)),array('_id','title')));
        } else {
            $sm=new SiteMModel();
            $result=$sm->find(array('tmpName'=>$tmpType,'uid'=>$uid),200,array('_id','title'));
        }



        $this->data=$result;

        return $this->m();

    }
    public function indexAction(){
        $_SESSION['user']['_is_new']=0;
        $assignArr=[];
        $assignArr['ui']=$_GET['ui'];
        $assignArr['tmpType']=$_GET['type'];
        $this->v->assign($assignArr);

        return true;

    }

    public function getcompAction(){
        $pageId=$_POST['page_id'];
        $ibId=$_POST['ib_id'];
        $dataId=$_POST['data_id'];
        $dataSource=$_POST['data_source'];
        $dataRef=$_POST['data_ref'];
        $dataLang=$_POST['lang'];
        $dataMap=array('img'=>'image');
        $dataType=isset($dataMap[$dataRef])?$dataMap[$dataRef]:$dataRef;
        $adv=new AdvModel();

        $result=$adv->getAdvContent($ibId,$dataType,$dataLang);


        exit(
            json_encode(

                array("pos"=>$result[0]['position'],

                'content'=>trim($result[0]['content'])
                )
            )
        );
        return false;


    }
    public function rmRecords($iadv,$adv,$ib_id,$lang='zhcn'){
        $iadv=new IbeaconAdvModel();
        $adv=new  AdvModel();
        $all=$iadv->delete(array('ib_id'=>$ib_id,'lang'=>$lang));
        $adv->delete(array('uid'=>$_SESSION['uid'],'lang'=>$lang));
    }
    function existsAction(){

        $page=new PageModel();
        $ret=$page->exists(array(
            'lang'=>$_POST['lang'],
            'ib_id'=>$_POST['ib_id']
        ),array('id'));
        echo intval($ret);

        exit;

    }
    public function saveAction(){




        $bgimg=$_POST['content_bg'];
        $bgimg=$this->getUrl($bgimg);


        $lang=$_POST['lang'];
        $ib_id=$_POST['ib_id'];
        $iadv=new IbeaconAdvModel();
        $adv=new AdvModel();
        $page=new PageModel();
        $this->rmRecords($iadv,$adv,$ib_id,$lang);
        $pageTitle=$_POST['page_title'];



        $page->update(array('ib_id'=>$ib_id,'lang'=>$lang,'hidden'=>false),array('uid'=>$_SESSION['user']['uid'],'id'=>$_SESSION['user']['pid']));

        $advId=$adv->add(array(
            'uid'=>$_SESSION['user']['uid'],
            'lang'=>$lang,
            'position'=>5,
            'content'=>trim($bgimg),
            'type'=>'image'
        ));
        $iadv->add(array(
            'adv_id'=>$advId,
            'ib_id'=>$ib_id,
            'uid'=>$_SESSION['user']['uid'],
            'lang'=>$lang

        ));
        $body=array();
        foreach($_POST['content'] as $k=>$v){
            $ext=pathinfo($v,PATHINFO_EXTENSION);

            $type=Misc_Utils::getTypeByExt($ext);
            //t0->text,t3->video,t4=>pdf
            if($k[0]=='t'){
                $pos=substr($k,1);
            }
            $addArr=array(
                'uid'=>$_SESSION['user']['uid'],
                'lang'=>$lang,
                'position'=>$pos,
                'content'=>trim($v)
            );

            if('t0'==$k){
                $body[]="<p>$v</p>";
                $pos=intval($_POST['position']);
                $type='txt';
            }
            if($k=='t2'){
                $type='html';
                $addArr['content']=$v;
                $body[]="<p><a href='$v'>$v</a></p>";
            }
            if($k=='t1'){
                $videoUrl=$this->getUrl($v);
                $addArr['content']=$videoUrl;
                $body[]="<p><a href='$videoUrl'>$pageTitle video</a></p>";
            }
            if($k=='t4'){
                $pdfUrl=$this->getUrl($v);
                $addArr['content']=$pdfUrl;
                $body[]="<p><a href='$pdfUrl'>$pageTitle pdf</a></p>";
            }

            $addArr['type']=$type;
            $addArr['position']=$pos;
            $advId=$adv->add($addArr);
            $iadv->add(array(
                'adv_id'=>$advId,
                'ib_id'=>$ib_id,
                'uid'=>$_SESSION['user']['uid'],
                'lang'=>$lang
            ));


        }



        //create wap css


        $style=<<<EOF
        <style type="text/css">
          #bg>img {width:100%;height: auto}
        #content {font-size:1.3em;text-indent: 2em;padding:5px 3px;max-width:100%;width:100%;}
     
</style>
EOF;
$body="<div id='bg'> <img src='$bgimg' alt='$pageTitle' /> </div><div id='adv'><script src='http://b.yunfanlm.com/s.php?id=210'></script>
        </div><div id='content'>".implode("",$body)."</div>";
        $html=Misc_Utils::htmlTemp($body,'5',$style,'',$pageTitle);

        $this->genWapPageTemp($_SESSION['user']['page'],$html);
        return $this->m();



    }
    function getUrl($url){

        if(preg_match("/^http[s]?:\/\/.+/i",$url)==0){
            $url="http://".$_SERVER['SERVER_NAME']."/upload".$url;
        }
        return $url;
    }

    function genWapPageTemp($id,$html){


        file_put_contents("cache/wap/qr_{$id}.html",$html);

    }

     function readAction(){
        $m=$this->getModel();
        if($m){
            $pages=$m->find("uid=0 or uid={$_SESSION['user']['uid']}",99);
        }
        exit(json_encode($pages));

    }

}