<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 12/31/14
 * Time: 5:34 PM
 */

class AppsController extends AdminController {
     function indexAction() {



         $app=new AppMModel();


         $cats=new AppCategoryModel();
         $orgCats=$cats->all();


         $cats=array();
         foreach($orgCats as &$item){
             $item['name']=$this->lang[$item['name']];
             $cats[$item['id']]=$item;
         }

         $currency=json_encode(array('1'=>'￥','2'=>'$'));

         $assignArr=array(

             'currency'=>$currency
         );
         $isSys=false;
         if($_GET['type']=='sys'){
             $allApps=$app->allSys();
             $isSys=true;

         } else {
             $allApps=$app->all();
             $myApp=new MyAppMModel();


             $myApps=$myApp->myApps($_SESSION['user']['uid']);

             $assignArr['myApps']=$myApps;
             $assignArr['backboneJs']=array(
                 'c'=>array("allApp"=>array('data'=>json_encode($allApps[0]),
                     'init'=>"{model:App,url:'/rest/AppM'}"),
                     "myApp"=>array('data'=>json_encode($myApps),'init'=>"{model:MyApp,url:'/rest/MyAppM'}")),
                 'm'=>array("app"=>array('init'=>json_encode(array('idAttribute'=>AppMModel::$_primary,'urlRoot'=>'/rest/AppM'),JSON_UNESCAPED_SLASHES)),
                     "myApp"=>array('init'=>json_encode(array('idAttribute'=>MyAppMModel::$_primary,'urlRoot'=>'/rest/MyAppM'),JSON_UNESCAPED_SLASHES)))

             );
         }
         $assignArr['cats']=$cats;
         $assignArr['isSys']=$isSys;
         $assignArr['allApps']=$allApps[1];




        $this->v->assign($assignArr);
        return true;
    }
    function hiAction(){

        exit("app/hi");
    }
    function backboneInit($data){

    }


}