<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 2/4/15
 * Time: 4:08 PM
 */


class AirController extends OpenController {


    public $isAuth=false;

   
    function configAction(){
        ob_end_clean();
        $this->data=array(
            'sensor'=>array(
                'uuid'=>"FDA50693-A4E2-4FB1-AFCF-C6EB07647825",
                'major'=>10000,
                'minor'=>1,
                'alias'=>'Air 测试'

            ),
            'adv'=>array(
                    'img_ver'=>2,
                    'img_prefix'=>'http://d1.gogoinfo.cn/air_client/imgs/',
                    'imgs'=>['coupon.png','hot.png']

            )
        );

        return $this->m();
    }


    function sensorinfoAction(){

        /**
         *  1 ---表示get为获取一条当前记录,排序为顺序
         *  2 ---表示最后一条记录
         */
        //返回提示信息
        /**
         *
         * array(
         *      'text'=>'','type'=>'text','link'=>''
         * ),
         * array(
         *   'type'=>'img_text','img'=>'','link'=>'','text'
         * ),
         *
         */
        if(Misc_Utils::isWechat($_SERVER['HTTP_USER_AGENT'])){
                if(!$this->isBindedMac()){
                    return false;
                }

        }
        $this->doMongoRest('SensorInfoM',2);     
        
    }
    /**
     * 是否是绑定的mac地址
    */
    function isBindedMac(){
            
        return true;
    }
    function appInfoAction(){
        $this->doMongoGet('AppInfoM',2);
    }
    function envrefAction(){

        $limit=8;

        ob_end_clean();

        $city=new CityMModel();

        /**
         * 判断放回的参数是代码还是城市中文名还是城市英文名
         */

        if(empty($_REQUEST['city'])){
            $_REQUEST['city']=$this->getCurCity();

        }
        $code=filter_var($_REQUEST['city'],FILTER_VALIDATE_INT);
        if($code===false){

            $searchWord=rawurldecode($_REQUEST['city']);
            if(hexdec(bin2hex(substr($searchWord,0,1)))>128){
                $query['city_cn']=$searchWord;
            } else {
                $query['city_en']=ucfirst($searchWord);
            }
        } else {
            $query['code']=$_REQUEST['city'];
        }






        $result=$city->findOne($query,array('weoid','bd_id','prov_cn','city_cn','city_en','c2345'));



        //$pm25=$air->fetchPm25($bd_id);
        //$temp=$air->fetchTemp($woeid);
        //  $airData=json_encode(array_merge($pm25,$temp));


        //$pm25=$air->fetchPm25($bd_id);
        //$temp=$air->fetchTemp($woeid);
        //  $airData=json_encode(array_merge($pm25,$temp));



        $air=new AirCron(new ZCurl(true,'aqi'));

        //$pm25=$air->fetchPm25($bd_id);
        //$temp=$air->fetchTemp($woeid);
        //  $airData=json_encode(array_merge($pm25,$temp));




        if(empty($result)){
            $this->code=-1;
            $this->msg="invalid_city";
            return $this->m();
        }
        $bd_id=$result['bd_id'];
        $woeid=$result['weoid'];
        $limitKey="air.limit.{$result['city_en']}.".date("H");
        $dataKey="air.{$result['city_en']}.".date("H");
        if(isset($_GET['debug0911'])){
            $this->redis();
            $this->redis->del($dataKey);
            $this->redis->del($limitKey);
        }

        $redis=$this->redis();
//        if(Misc_Utils::isLocal()){
//            $redis->del($dataKey);
//        }

        $ret=$this->redis->get($dataKey);
        $jsonTemp=<<<EOF
{
    "code":1,
    "msg":"",
    "data":#data#
}
EOF;

        if(!empty($ret)){
            exit(str_replace("#data#",$ret,$jsonTemp));

        }
        else
        {

            //1000 ms
            $time=7e5;
            $cnt=5;

            foreach(range(1,$cnt) as $item){
                $curCnt=$this->redis->incr($limitKey);
                //the number of request is bigger than limit value
                if($curCnt>$limit){
                    usleep($time);

                    $ret=$this->redis->get($dataKey);
                    if(!empty($ret)){

                        exit(str_replace("#data#",$ret,$jsonTemp));
                    }

                } else {

                    $air=new AirCron(new ZCurl(true,'aqi'));

                    //$pm25=$air->fetchPm25($bd_id);
                    //$temp=$air->fetchTemp($woeid);
                    //  $airData=json_encode(array_merge($pm25,$temp));

                    $airData=$air->fetchAll($result);

                    $weatherKey=date("weather.{$result['weoid']}.Ymd");
                    $isInsForcast=$this->redis->get($weatherKey);
                    if(isset($airData['forcast5d']) && empty($isInsForcast)){
                        $winfo=new WeatherInfoMModel();
                        $winfo->insert(array('forcast5d'=>$airData['forcast5d'],'city_cn'=>$result['city_cn'],'city_en'=>$result['city_en'],'weoid'=>$result['weoid']));
                    }
                     $airData['current']=array_merge($airData['forcast5d'][0],$airData['current']);
                     if(!empty($airData['current'])){
                        $airModel=new AirInfoMModel();
                        $airModel->insert(array_merge($airData['current'],array('city_cn'=>$result['city_cn'],'city_en'=>$result['city_en'],'weoid'=>$result['weoid'])));
                    }
                    //$airData['forcast5d'];

                   
                    $this->data=$airData;
                    $airData['city']=$result['city_cn']."/".$result['city_en'];
                    //空气质量每小时更新
                    $cacheTime=15*60;
                    $this->redis->set($limitKey,0,$cacheTime);
                    $this->redis->set($dataKey,json_encode($airData),$cacheTime);
                    ob_end_clean();
                    return $this->m();

                }
            }

        }
        return $this->m();


        //$this->doMongoRest('AirInfoM',2);
    }
    function getCurCity(){
        $ip=Misc_Utils::getIp();
        if(Misc_Utils::isLocalIp($ip)){
            return '北京';
        } else {
            $city="http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=json&ip=".Misc_Utils::getIp();
            $z=new ZCurl();
            $result=$z->getJson($city);
        }

        return $result['city']?$result['city']:$result['province'];
    }

}
