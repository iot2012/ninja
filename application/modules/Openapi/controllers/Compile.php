<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 12/27/14
 * Time: 4:04 PM
 */


//http://walker0411:www0411@members.pubyun.com/dyndns/update?system=dyndns&hostname=opkcloud.f3322.org&myip=192.168.199.173


class CompileController extends OpenController {
    public $user;
    public $userModel;
        function  init(){

            parent::init();
            if(!$this->basicAuth()){

                return $this->m();
                exit;
            }


        }
        function basicAuth(){

            if (!isset($_SERVER['PHP_AUTH_USER']) && !isset($_SERVER['PHP_AUTH_PW'])) {

                header('WWW-Authenticate: Basic realm="SNOW Login"');
                header('HTTP/1.0 401 Unauthorized');
                $this->code=-1;
                $this->msg="not authorized";
                return false;

            } else {
                $this->userModel=new CommonUserModel();

                $result=$this->userModel->checkUser($_SERVER['PHP_AUTH_USER'],$_SERVER['PHP_AUTH_PW']);


                $res=$result[0];
                $this->user=$result[1];

                if($result[0]==false)
                {
                    if($result[1]!='cloud_closed'){
                        $this->code=-1;
                        $this->msg=$result[1];
                        return false;
                    } else {
                        $this->user=$result[2];
                    }

                }



            }
           return true;
        }
        function jobStatusAction(){
            $redis=$this->redis();
            $jobStatus=$redis->get("job_".$_REQUEST['id']);
            $this->data=array('status'=>0,'url'=>'');
            if(!empty($jobStatus)){
                $this->data=array('status'=>1,'url'=>$jobStatus);
            }
            return $this->m();
        }
        function androidAction(){
            /**
             * api.fogpod.com/domain/update?system=dyndns&hostname=opkcloud.f3322.org&myip=192.168.199.173
             *
             */
            if(!empty($_FILES['file'])){

                $files = $_FILES['file'];
                $dir = 'upload';

                $extractDir = realpath("./../../apks/");
                if (!is_dir($dir)) {
                    mkdir($dir, 0777);
                }
                $uploadDir = "$dir/{$files['name']}";

                move_uploaded_file($files['tmp_name'], $uploadDir);



                $pkg = $_FILES['file']['tmp_name'];

                $projName = pathinfo($_FILES['file']['name'], PATHINFO_FILENAME);
                $zip = new ZipArchive;
                $res = $zip->open("$dir/{$files['name']}");

                if ($res == TRUE) {

                    $return = $zip->extractTo($dir);
                    $projDir = realpath("$dir/$projName");
                    passthru("chmod -R 777 $projDir");
//            echo("cd $dir;android update project -p $projDir");
//            $echoProps="echo 'key.store=/Users/chjade/my/sencha1.keystore' >> $projDir/local.properties;echo 'key.alias=sencha_android1' >> $projDir/local.properties;echo 'key.alias.password=chjade' >> $projName/local.properties;echo 'key.store.password=chjade' >> $projDir/local.properties";
                    //           $data=passthru("chmod -R 777 $projDir;/Users/chjade/dev/android/tools/android update project -p $projDir;$echoProps;ant realease");
                    $id=date('ymdhis').rand(1,100000);
                    $buildDir = array(
                        'dir' => $projDir,
                        'id' => $id
                    );
                    $fileName = $_FILES['file']['name'];
                    $assignArr = array('fileName'=> $fileName,'jid'=>$id);
                    $this->v->assign($assignArr);
                    //$hd = popen("chmod -R 777 $projDir;/Users/chjade/dev/android/tools/android update project -p $projDir;$echoProps;ant realease", "r");
                    //$data=fread($hd,10240);

                    $redis = $this->redis();
                    $ret = $redis->publish('android_build', json_encode($buildDir, JSON_UNESCAPED_SLASHES));


                }
            }

            return $this->m();

        }
        function updateAction(){
            /**
             * api.fogpod.com/domain/update?system=dyndns&hostname=opkcloud.f3322.org&myip=192.168.199.173
             *
             */
            $ddns=new DdnsApiModel();
            $ret=$ddns->check();

            if($ret[0]){
                $ret[1]['value']=$ret[1]['myip'];
                $ret[1]['sub_domain']=explode(".",$ret[1]['hostname']);
                $ret[1]['sub_domain']=$ret[1]['sub_domain'][0];
                unset($ret[1]['myip']);


                $result=$ddns->update($this->user['uid'],$ret[1]);

                if(!$result){
                    $this->code=-1;
                    $this->msg="update failed,try again later";
                }

            } else {
                $this->code=-1;
                $this->msg="invalid data format";
            }
            return $this->m();

        }
        function cloudAction(){


            $uid=intval($this->user['uid']);

            $this->userModel->update(array('status'=>(intval($_GET['enable']==0?10:1))),array('uid'=>$uid));

            $this->msg="req_suc";


            return $this->m();
        }
        function recordCreateAction(){

            $ret=Validator::checkModel2(array(
                'sub_domain'=>'domain_comp',
                'value'=>'ipv4'
            ));
            if($ret[0]){
                if($this->user['level']=='9'){
                    $domain=$this->c->domain->def;
                    $cfg=$this->c->domain[$domain]->toArray();
                    $domain=\Domain\DomainFactory::instance($domain,$cfg);

                    $data['value']=$ret[1]['value'];
                    $data['sub_domain']=$ret[1]['sub_domain'];

                    $this->msg="add user successfully";
                    $ret=$domain->record_create($data);

                    if($ret['status']['code']!=1){
                        $this->code=-1;
                        $this->msg=$ret[1];
                        $this->redis();
                        $this->redis->rPush('failed_domain',"{$data['value']}@{$data['sub_domain']}");
                    }
                } else {
                    $this->code=-1;
                    $this->msg="access_denied";
                }

            } else {

                $this->code=-1;
                $this->msg="err_data_fmt";
            }

            return $this->m();

        }

}

