<?php
class ErrorController extends Yaf\Controller_Abstract {
    public function errorAction($exception) {

        Yaf\Dispatcher::getInstance()->disableView();
        header("Access-Control-Allow-Origin:*");
        if(strstr($_SERVER['REQUEST_URI'],".json")){
            ob_end_clean();
            $this->debugApi();
            return false;
        }


    }
    function getDocByUri($file,$uri,$method){
        $apiMaps=array(
            'fogpod_api'=>array('path'=>'docs/fogpod_api.json','txt'=>'fogpod api'),
            'ninja_api'=>array('path'=>'docs/ninja_api_ryatek.json','role'=>'ryatek','txt'=>'ninja api'),
            'attence'=>array('path'=>'docs/attence_api.json','role'=>'ryatek','txt'=>'attence api'),
            'niudaojia_api'=>array('path'=>'docs/niudaojia_api.json','txt'=>'niudaojia api'),
            'dinning'=>array('path'=>'docs/dinning_api.json','txt'=>'餐厅 api'),
            'lighthouse_api'=>array('path'=>'docs/lighthouse_api_ryatek.json','role'=>'ryatek','txt'=>'lighthouse api')
        );
        $doc=new RestApiDocModel();
        $docs=$doc->getDocByUrl($file);


        if(!empty($docs['docs'][$uri])){

            return $docs['docs'][$uri][$method]['samp_resp'];
        }
        return false;
    }
    function openApiReqUri(){
        $url=str_replace('/openapi','',$_SERVER['REQUEST_URI']);

        $url=parse_url($url,PHP_URL_PATH);
        if(end(explode(".",$url))!='json'){
            $url="$url.json";
        }
        return $url;

    }
    function docName(){

        return empty($_GET['d'])?"niudaojia_api":$_GET['d'];
    }
    function debugApi()
    {

        $apiMaps = array(
            'attence'=>array('path'=>'docs/attence_api.json','txt'=>'考勤 api'),
            'fogpod_api'=>array('path'=>'docs/fogpod_api.json','txt'=>'fogpod api'),
            'dinning'=>array('path'=>'docs/dinning_api.json','txt'=>'餐厅 api'),
            'ninja_api'=>array('path'=>'docs/ninja_api_ryatek.json','role'=>'ryatek','txt'=>'ninja api'),
            'niudaojia_api'=>array('path'=>'docs/niudaojia_api.json','txt'=>'牛到家Api文档'),
            'lighthouse_api'=>array('path'=>'docs/lighthouse_api_ryatek.json','role'=>'ryatek','txt'=>'lighthouse api')
        );

        $doc = new RestApiDocModel();
        $docFile = $this->docName();


        $docs = $doc->getDocByUrl($apiMaps[$docFile]['path']);


        $url = $this->openApiReqUri();

        $doc = $this->getDocByUri($apiMaps[$docFile]['path'],$url,$_SERVER['REQUEST_METHOD']);


        if (!empty($doc)) {
            exit($doc);
        }
    }
}