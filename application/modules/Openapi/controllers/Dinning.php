<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 6/26/14
 * Time: 9:52 AM
 */

/**
 * rest api for pushing ibeacon advs
 *
 */


class DinningController extends OpenController {


    public $isAuth=false;
    protected $_exMethods=array(
        'shops'=>['get']
    );
    function init(){
        parent::init();
        $this->_root=new RootTable($this,true);

    }
    function businfoAction(){
        if(!isset($_REQUEST['vid'])){
            $this->code=-1;
            $this->msg="no_perm";
            $this->m();
        }
        $this->doMongoRest("DinningBookingM",1,['name','icon','address','mobile','price']);
        $this->m();

    }
    function bookingAction(){
        if(!isset($_REQUEST['vid'])){
            $this->code=-1;
            $this->msg="no_perm";
            $this->m();
        }
        $this->doMongoRest("DinningBookingM");
        $this->m();
    }
    function contactAction(){

        if(!isset($_REQUEST['vid'])){
            $this->code=-1;
            $this->msg="no_perm";
            $this->m();
        }
        $this->doMongoRest("DinningContactM");
        $this->m();
    }
    function lineupAction(){
        $this->doMongoRest("DinningLineupM");
        $this->m();
    }
    function lineup_listAction(){
        $this->doMongoRest("DinningLineupM",1,['name','openid','mobile','number']);
        $this->m();
    }
    /**
     *
     * @todo 如何查询下面b的第一个文档
     * { "_id" : ObjectId("563085dd9a188a6a52879798"), "b" : [
     *  { 	"qq" : [ 	99 ], 	"ee" : 231 },
     * 	{ 	"qq" : [ 	33 ], 	"ee" : 99 },
     * 	{ 	"qq" : [ 	22 ], 	"ee" : 12 } ]
     * }
     */
    function dishAction(){
        if(!isset($_GET['vid']) && $_GET['shop_id']){
            $this->code=-1;
            $this->msg="no_perm";
            $this->m();
        }
        $field="shops.dishs";
        $this->processRest('DinningMModel',["_id"=>$_GET['vid']],$field);
        $this->data=$this->data['shops'][$_GET['shop_id']]['dishs'];
        $this->m();
    }
    function orderAction(){
        $this->doMongoRest("DinningBookingM");
        $this->m();
    }
    function shopAction(){

        $field='shops';
        if(!isset($_GET['vid']) && $_GET['shop_id']){
            $this->code=-1;
            $this->msg="no_perm";
            $this->m();
        }

        $shopId=intval($_GET['shop_id']);
        $this->processRest('DinningMModel',["_id"=>$_GET['vid']],['shops'=>['$slice'=>[$shopId,1]]]);
        $this->data=$this->data[$field];
        $this->m();
    }
    function shopsAction(){
        $field='shops';
        if(!isset($_GET['vid'])){
            $this->code=-1;
            $this->msg="no_perm";
            $this->m();
        }
        $this->processRest('DinningMModel',["_id"=>$_GET['vid']],$field);
        $this->data=$this->data[0][$field];
        $this->m();
    }

}

