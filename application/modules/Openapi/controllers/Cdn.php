<?php

class CdnController extends OpenController
{

    private $cdn;
    function init(){

        parent::init();
        $account=$this->accountInfo($_GET['w']);
        if($account){
            $this->cdn=new \Ry\Cdn\Cdn('Ali',$account);
        } else {

            $this->code="-1";
            $this->msg="invalid provider";
            return $this->m();
        }


    }
    function accountInfo($provider='ali'){
        $cdn=$this->c->openid->ali->$provider;
        if(!empty($cdn)){

           return  $cdn->cdn->toArray();
        }
        return false;

    }
    function refreshAction(){

        $result=Validator::checkModel2(['path'=>'cdn_path','type'=>'en_name']);
        if($result[0]){
            $this->data=$this->cdn->exec($result[1]['path'],$result[1]['type']);
        } else {
            $this->data=$result[1];
        }
        return $this->m();

    }


}