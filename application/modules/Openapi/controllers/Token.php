<?php


class TokenController extends  OpenController {



    /**
     * @todo 统一一下错误识别码
     *
     * 参考开放平台的api错误code定位识别码,
     * 1:哪类api
     *
     * 2.api的错误代码
     * 3-4.错误类型 如第二位1->请求数据格式错误
     *
     */
    public  $exActions=array("index");
    function genToken($appId,$appSecret,$siteKey){
        $arr=[
            'u'=>$appId,
            'p'=>$appSecret
        ];
        $dataStr=$siteKey."__".json_encode($arr);
        $token=Misc_Utils::genEncAtoken($dataStr,$appSecret,$siteKey,$this->c['site']['atoken']['alg']);
        return $token;
    }

    /**
     * AppId:rydd247e4ba135
     * AppSecret:qmCWAE5rwH3fHQIHG6PmB078KcMmxA==
     */
    function indexAction(){

            $token=new TokensModel();
            $ret=$token->checkRequired(['app_id','app_secret']);

            $siteKey=$this->c['site']['key'];


            if($ret[0]){
                $appId=$ret[1]['app_id'];
                $appSecret=$ret[1]['app_secret'];
                /**
                 *
                 */
                $result=$token->existAppId($appId);


                if(!empty($result['app_secret'])){

                    if(urldecode($result['app_secret'])==$ret[1]['app_secret']){

                        $accessToken=$token->accessToken($appId,$appSecret,$result['level'],$result['access_token'],$this->c->site->openid->expired);


                        $this->data=array(
                            "access_token"=>$accessToken[0],
                            "expires_in"=>$accessToken[1]
                        );

                    } else {
                        $this->code=1002;
                        $this->msg="err__app_id_app_secret";
                    }

                } else {
                    $this->code=1002;
                    $this->msg="no__app_id_app_secret";
                }

            } else {
                $this->code=1001;
                $this->msg="err_format";
                $this->data=$ret[1];
            }


            return $this->m();
        }


} 