<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 6/26/14
 * Time: 9:52 AM
 */

/**
 * rest api for pushing ibeacon advs
 *
 */

class AttenceController extends OpenController {


    function indexAction(){
        if($this->r->method=='GET'){
            $start=new MongoDate(strtotime($_GET['period']."01"));
            $end=new MongoDate(strtotime(date("Y-m-d")));
            $m=new AttenceMModel();
            $this->data=$m->findByDate($_GET['openid'],$start,$end);
            return $this->m();
        }
        $this->doMongoRest('AttenceM');

    }

}