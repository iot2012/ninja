<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 6/26/14
 * Time: 9:52 AM
 */

/**
 * rest api for pushing ibeacon advs
 *
 */
class IbeaconController extends OpenController {


    public $isAuth=false;

    function testAction(){
        $this->data=array("content"=>print_r($_SERVER,true));
        return $this->m();
    }
    public function advAction(){

        $ib=new IbeaconModel();
        $ret=$ib->check();
        $this->redis();
        if($ret[0]!==false){

            $lang=strtolower($_GET['lang']);


            $result=$ib->getCurrentAdv(strtoupper($_REQUEST['uuid']),$_REQUEST['major'],$_REQUEST['minor'],$lang);


            $like=new IbeaconLikeMModel();

            $likes=$this->redis->zScore("ib.like.{$_REQUEST['major']}",$_REQUEST['minor']);
           

            $ibUid=isset($_GET['ibuid'])?$_GET['ibuid']:'';
            $sex='';
            $major=$ret[1]['major'];
            $minor=$ret[1]['minor'];
            if($ibUid!=''){
                $user = new SnsUserMModel();
                $user=$user->findOne(array('_id'=>new MongoId()));
                $sex=$user['data']['sex'];
                if($user['snstype']=='weibo'){
                    $sex=($user['sex']=='0'?1:2);
                }




            }
            /**
             * top N排序使用
             */

         $this->redis->zIncrBy("ib.num.$major",1,$minor);
         $iblog=new IbeaconAccessLogMModel();


        $logArr=array(
              'uuid'=>$ret[1]['uuid'],
              'major'=>$ret[1]['major'],
              'minor'=>$ret[1]['minor'],
              'uid'=>$ibUid,
              'sex'=>$sex,
              'atime'=>new MongoDate()
            );
            $iblog->add($logArr);

            $this->data=array('like'=>$likes,"result"=>$result);

        }
        else{

            $this->code=100;
            $this->msg="";
            $this->data=$ret[1];
        }

        return $this->m();

    }
    function registerAction(){

        $this->doRest("Ibeacon");


    }
    public function contentAction(){

        $this->forward('ibeacon','adv');
    }
    public function iAction(){

        phpinfo();
        exit;
    }
    public function getuserAction(){

        $m=new SnsUserMModel();
        var_dump($m->pageFind(array('name'=>'zhou'),array('name'),array('name'=>1),2,4));
    }
    public function commentAction(){

        $this->doMongoRest('IbeaconCommentM',3);
    }
    public function likeAction(){
        $this->redis();
        if($this->r->method=='POST'){
            $_POST['major']=intval($_POST['major']);
            $_POST['minor']=intval($_POST['minor']);
            $this->redis->zIncrBy("ib.like.{$_POST['major']}",1,$_POST['minor']);
        }
        $this->doMongoRest('IbeaconLikeM');

    }
     function feedBackAction(){

        $this->doMongoRest('IbeaconFeedbackM');
    }

    /**
     *
     */

    function userInfoAction(){



            $user=new SnsUserMModel();
            if($this->r->method=='POST'){
                $snsUserid=$_POST['snsuserid'];
                $snstype=$_POST['snstype'];
                $vendor_major=$_POST['major'];
                $data=array('snsuserid'=>$snsUserid,'snstype'=>$snstype,'data'=>$_POST);
                $result=$user->findOne(array('snsuserid'=>$snsUserid,'snstype'=>$snstype));

                $uuid='74278BDA-1986-0501-8F0C-888888888888';



                if(!empty($result)){

                    $data['major']=$result['major'];
                    $data['minor']=$result['minor'];
                    $data['uuid']=$uuid;
                    $data['ctime']=time();

                    $user->upsert2(array('snsuserid'=>$snsUserid,'snstype'=>$snstype),$data);
                    $this->data=array('id'=>$result['_id'],'uuid'=>$uuid,'major'=>$result['major'],'minor'=>$result['minor']);
                    return $this->m();
                } else {

                    //注意:mongodb只有在保存或者插入数据的时候才会自动创建表
                    $cols=$user->r()->getCollectionNames();
                    if(!in_array("ibuser_cnt",$cols)){
                        $user->w()->ibuser_cnt->save(array('cnt'=>1));
                        $cnt=0;
                    } else {
                        $cnt=$user->w()->ibuser_cnt->findAndModify(array(),array('$inc'=>array('cnt'=>1)));
                        $cnt=$cnt['cnt'];
                    }

                    $data['major']=Misc_Utils::highBytes(($cnt+1),2);
                    $data['minor']=Misc_Utils::lowBytes(($cnt+1),2);
                    $data['ctime']=time();
                    $id=$user->upsert2(array('snsuserid'=>$snsUserid,'snstype'=>$snstype),$data);
                    $this->data=array('id'=>$id,'uuid'=>$uuid,'major'=>$data['major'],'minor'=>$data['minor']);

                    return $this->m();
                }

            } else if($this->r->method=='GET') {

                 $this->doMongoRest("SnsUserM",true);

            } else {
                return $this->m();
            }



    }
    function validateuuidAction(){

        if($this->r->method=='GET'){

            $this->doMongoRest("SnsUserM",true);
            
        }
        
    }
    public function bbsAction(){

        $ret=$this->doRest("IbeaconBbs");

        exit;

    }
    public function versionAction(){

        $this->doMongoRest('VersionM');

    }
    public function advStarAction(){
        $advStar=new advStarModel();
        $ret=$advStar->check();


    }

    public function favAction(){

    }
    public function configAction(){

        $ib=new IbeaconModel();

        $ret=$ib->check();
        $this->redis();
        if($ret[0]!==false){
            if(empty($ret[1]['uuid']) || empty($ret[1]['major'])){
                $this->code='-1';
                $this->msg="error request";

            }

            $major=$ret[1]['major'];
            $year=date('Y');
            $month=date('m');
            $key=date('d-H');

            $this->redis->hIncrBy("ib.$major.$year.$month",$key,1);

            $this->redis->hIncrBy("ib.total","$major",1);
            $result=$ib->getConfig(strtoupper($ret[1]['uuid']),$ret[1]['major']);

            $this->data=$result;

        }
        else{

            $this->code=$this->c['ret']['FieldFormatError'];
            $this->msg="";
            $this->data=$ret[1];
        }

        return $this->m();
    }


} 