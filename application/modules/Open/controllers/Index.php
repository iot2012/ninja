<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 3/13/15
 * Time: 11:18 AM
 */
/**
 * @name IndexController
 * @author chjade
 * @desc 默认控制器
 * @see http://www.php.net/manual/en/class.yaf-controller-abstract.php
 */
class IndexController extends ApplicationController {
    public $title="site_open_title";
    public $exActions=array(
        'index'
    );
    /**
     * 默认动作
     * Yaf支持直接把Yaf_Request_Abstract::getParam()得到的同名参数作为Action的形参
     * 对于如下的例子, 当访问http://yourhost/lighthouse/index/index/index/name/chjade 的时候, 你就会发现不同
     */
    protected  $apiMaps=array(
        'attence'=>array('path'=>'docs/attence_api.json','txt'=>'考勤 api'),
        'fogpod_api'=>array('path'=>'docs/fogpod_api.json','txt'=>'fogpod api'),
        'dinning'=>array('path'=>'docs/dinning_api.json','txt'=>'餐厅 api'),
        'ninja_api'=>array('path'=>'docs/ninja_api_ryatek.json','role'=>'ryatek','txt'=>'ninja api'),
        'niudaojia_api'=>array('path'=>'docs/niudaojia_api.json','txt'=>'牛到家Api文档'),
        'lighthouse_api'=>array('path'=>'docs/lighthouse_api_ryatek.json','role'=>'ryatek','txt'=>'lighthouse api')
    );

    function init(){

        $this->initHeader();
        $this->initEnv();

        $this->initCache();


        $this->loginAuth();

        $this->initUserData();

        $this->initWebview();
    }
    public function indexAction() {
        //1. fetch query

        //文档开放权限
        $doc=new RestApiDocModel();
        $docFile=empty($_GET['doc'])?"fogpod_api":$_GET['doc'];

        if(preg_match("/.+_ryatek\.json$/i",$this->apiMaps[$docFile]['path'])){

            if (!$this->e['isLocal'] && (!isset($_SERVER['PHP_AUTH_USER']) || $_SERVER['PHP_AUTH_USER'] != 'hx' || $_SERVER['PHP_AUTH_PW'] != 'pinetree')) {
                $txt="Need login for reading internal document";
                header('WWW-Authenticate: Basic realm="'.$txt.'",encoding="UTF-8"');

                //header('WWW-Authenticate: Basic realm="'.$txt.'"');
                header('HTTP/1.0 401 Unauthorized');
                exit('Unauthorized');
            }
        }

        $docs=$doc->getDocs($this->apiMaps[$docFile]['path']);

        $options1=array();
        foreach($this->apiMaps as $key1=>$doc1){
            if($key1==$docFile){
                $doc1['txt']=ucwords($doc1['txt']);
                $options1[]="<option value='doc=$key1' selected>{$doc1['txt']}</option>";
            } else {
                $doc1['txt']=ucwords($doc1['txt']);
                $options1[]="<option value='doc=$key1'>{$doc1['txt']}</option>";
            }

        }

        //
        $this->v->assign(array('allapis'=>$docs['docs'],'prefix'=>$docs['prefix'],'host'=>$docs['host'],'defSel'=>$defSel,'options'=>implode("",$options1)));

        return true;
    }
    public function docAction(){


    }


}
