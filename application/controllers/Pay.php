<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 2/9/15
 * Time: 6:07 PM
 */

use \Ali\Pay\Submit;
class PayController extends ApplicationController {

    public $aliCfg;

    public $payType='trade_create_by_buyer';

    public $validType=array(
        'app','pod','product'
    );
    public $virtualItems=array('app','pod');
    function init(){

        parent::init();

        $this->aliCfg=$this->c->pay->ali->toArray();
    }

    function wechatAction(){


    }
    function indexAction(){
        if(in_array($_GET['item_type'],$this->virtualItems,true)){
            $this->payType='create_direct_pay_by_user';
        }

        if(in_array($_GET['item_type'],$this->validType,true)){
            $payData=$this->itemData($_GET['item_type'],$_GET['item_id']);

            $orderExtraData=$payData[1];
            $payData=$payData[0];
            $submit = new Submit(Misc_Utils::array_pluck($this->aliCfg,'partner,key,sign_type,input_charset,cacert,transport'));
            $html_text = $submit->buildRequestForm($payData,"get", "确认");
            $order=new OrderMModel();

            $orderData=Misc_Utils::array_pluck($payData,"subject,price,body,show_url,receive_name,receive_address,receive_zip,receive_phone,receive_mobile,out_trade_no,quantity,total_fee");

            $orderData['trade_no']=$orderData['out_trade_no'];
            unset($orderData['out_trade_no']);
            $orderData['item_type']=$_GET['item_type'];
            $orderData['item_id']=$_GET['item_id'];
            $orderData['quantity']=($orderData['quantity']>0)?$orderData['quantity']:intval($_REQUEST['item_num']);
            $orderData=array_merge($orderData,$orderExtraData);
            $orderData['uid']=intval($_SESSION['user']['uid']);
            $orderData['username']=$_SESSION['user']['username'];
            $order->insert($orderData);
            exit($html_text);
        } else {

            exit("invalid_request");
        }




    }

    function aliPayCfg(){


    }
    function logisticData($itemType){
        //必须设置一组物流信息
        if( $this->payType=='trade_create_by_buyer'){

            return  array(
                'logistics_fee' =>"10.00",

                //$logistics_type = "EXPRESS";

                'logistics_type' => "EXPRESS",


                //必填，两个值可选：SELLER_PAY（卖家承担运费）、BUYER_PAY（买家承担运费）
                'logistics_payment' => "SELLER_PAY"
            );
        }
        return array();

    }
    function sysData(){

        $info=Misc_Utils::array_pluck($this->aliCfg,"payment_type,return_url,notify_url,partner,seller_email,_input_charset");
        //trade_create_by_buyer
        $info['return_url']=Misc_Utils::fullUrl($info['return_url']);
        $info['notify_url']='http://www.fogpod.com/callback/alipay';
            //Misc_Utils::fullUrl($info['notify_url']);

        return $info;
    }
    function receiver(){

        $receive_name = $_SESSION['user']['realname'];
        $profileModel=new UserProfileModel();
        $profile=$profileModel->getBuyerInfo($_SESSION['user']['uid']);
        $receiver=array();
        /**
         * 即时交易的话不需要下面参数
         */
        if(!empty($profile) && $this->payType!='create_direct_pay_by_user'){
            $receive_address = empty($profile['receive_address'])?$profile['address']:$profile['receive_address'];
            $receive_address = empty($receive_address)?"北京锐亚科技有限公司":$receive_address;
            $receive_zip = empty($profile['receive_zip'])?$profile['zipcode']:$profile['receive_zip'];
            //

            //收货人电话号码
            $receive_phone = $profile['telephone'];
            //收货人手机号码
            $receive_mobile = $profile['mobile'];
            $receiver=array('receive_name'=>$receive_name,'receive_mobile'=>$receive_mobile,'receive_phone'=>$receive_phone,'receive_address'=>$receive_address,'receive_zip'=>$receive_zip);
        }

        return $receiver;
    }
    function getItemInfo($itemType,$id){
         if(in_array($itemType,$this->validType,true)) {
                 $modelName=ucfirst($itemType)."MModel";
                if(class_exists($modelName)){
                    $model=new $modelName;
                    return $model->getItemInfo($id,$_REQUEST);
                } else {
                    return false;
                }


         }
    }
    function itemData($itemType,$itemId){

        $_POST=$_REQUEST;
        $buyerData=array();
        $id=$_POST['item_id'];
        $type = $_POST['item_type'];
        $itemInfo=$this->getItemInfo($type,$id);
        if(!empty($itemInfo)){

            $logisticData=$this->logisticData($_POST['item_type']);

            $compactArr=array('subject','body','show_url','out_trade_no','service');

            $subject=$itemInfo['name'];
            if($this->payType=='trade_create_by_buyer'){
                $price = $itemInfo['price'];
                //商品数量, 必填，建议默认为1，不改变值，把一次交易看成是一次下订单而非购买一件商品

                //物流费用,必填，即运费
                $quantity = "1";
                $service='trade_create_by_buyer';

                $compactArr=array_merge($compactArr,array('price','quantity'));
            } else {
                $total_fee=$itemInfo['price']*(($_REQUEST['item_num']>0)?intval($_REQUEST['item_num']):1);
                $anti_phishing_key='';
                $exter_invoke_ip='';
                $service='create_direct_pay_by_user';
                $compactArr=array_merge($compactArr,array('total_fee','anti_phishing_key','exter_invoke_ip'));
            }

            //$app['price'];
            $body = mb_substr($itemInfo['desc'],0,400);
            //商品展示地址

            $show_url = Misc_Utils::fullUrl("/product/detail/?id=$id&type=$type");

            //收货人姓名

            $out_trade_no = Misc_Utils::genOrderSn($_SESSION['user']['uid']);


            $buyerData=array_merge($logisticData,compact($compactArr),$this->sysData(),$this->receiver());
            unset($itemInfo['name'],$itemInfo['desc'],$itemInfo['_id']);

            return array($buyerData,$itemInfo);
        }

        return array($buyerData,$itemInfo);
    }


} 