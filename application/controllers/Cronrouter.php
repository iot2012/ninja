<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 2/6/15
 * Time: 3:34 PM
 */

class CronrouterController extends ApplicationController {


    /**
     * 定时扫描路由器的的worker,GWorker表示是gearman的
     */
    function init(){
        session_start();
        unset($_SESSION['user']['rights']);
        unset($_SESSION['site']);
        $this->initHeader();
        $this->initEnv();

        $this->initCache();
    }
    function routerExists($router,$mac){

        $routerData=$router->findOne(array('mac'=>$mac),array('ip','domain'));
        //应该使用domain
        if(empty($routerData)){
            return 0;
        }
        return $routerData;
    }
    function scanGAction(){



            $this->redis();
            $notify=new PodNotifierDataMModel();
            $events=$notify->find(array(),99999999);
            //$notifyId=$this->redis->blPop("router.notify",10);
        $router=new RouterMModel();


            array_map(function($event) use($notify,$router){
                if(Validator::isReg('mac2',$event['sel_router'][0]) && !empty($event['who'])){

                    $routerData=$this->routerExists($router,$event['sel_router']);

                    if(true || $this->matchFreq($event) && $routerData){


                        //$fog=new FogpodApi($routerData['ip'],$event['sel_router'],'',$this->redis);

                        //$token=$fog->token();
                        $token=true;
                        if(empty($token)){
                            /**
                             * 路由器离线的状态下
                             */
                            if($token===false){
                                if($event['notify_type'] ==  'self'){
                                    $content=$this->buildContent($event,'self');
                                    $this->sendNotify($routerData,$content);
                                    $notify->update(array('$set'=>array('last_send'=>time()),'$inc'=>array('send_times'=>1)),array('_id'=>$event['_id']));
                                }


                            }else {
                                $this->code=-1;
                                $this->msg="req_router_token_failed";
                            }


                        } else {
//                        mac:f4:f9:51:e8:d7:c2
//                        ip:192.168.1.192
//                        name:chjades-iMac
                            //@debug
                            //$devices=$fog->device_list();
                            $devices=array(
                                array(
                                    'mac'=>'f4:f9:51:e8:d7:c2',
                                    'ip'=>'192.168.1.192',
                                    'name'=>'king-iMac'
                                )
                            );
                            if($devices){
                                $devs=$this->matchDev($devices,$event);
                                if($devs){
                                    $content=$this->buildContent(array('event'=>$event,'devs'=>$devs));
                                    $this->sendNotify($event,$content);
                                    $notify->update(array('$set'=>array('last_send'=>time()),'$inc'=>array('send_times'=>1)),array('_id'=>new MongoId($event['_id'])));
                                }

                            }

                        }
                    }

                }

            },$events);



//            $worker = new GearmanWorker();
//            $worker->addServer();
//            $worker->addFunction("reverse", function ($job) {
//                return strrev($job->workload());
//            });
//
//
//            while ($worker->work()){usleep(60000);};
//         else {
//
//            $this->log->err("Missing class GearmanWorker");
//        }

    }



    function buildContent($data,$notifyType='other'){
        if($notifyType=='other'){

            $content=array();
            foreach($data['devs'] as $key=>$val) {
                $content[] = str_replace(array("#device_name#", "#event#"), array($val['name'], $this->lang['n_'.$data['event']['event']]), $this->formatMsg($this->lang, 'device_event_trigger'));
            }
              return implode(",",$content);


        } else  if($notifyType=='self'){
           return str_replace("#router_name#",$data['name'],$this->formatMsg($this->lang,'your_router_offline'));
       }


    }
    function sendNotify($event,$content=''){

        if($event['action']=='email'){
            Misc_Utils::sendMail($event['who'],$event['name_notify'],$content."\n".$event['content'],'','','queue');
        }
        else if($event['action']=='wechat'){
            Misc_Utils::sendWechat($event['who'],$event['name_notify'],$content,$event['content'],$event['_id'],'queue');

        }

    }
    function matchFreq($event){
        return ($event['last_send']?((time()-$event['freq']*60)>=$event['last_send']):true) && $event['status']==1;

    }
    /**
     * @param $arr device list
     * @param $keyword device name
     * @param $type   device type
     * @return bool
     */
    function matchDev($arr,$event)
    {
        $keyword=$event['keyword'];
        $type=$event['notify_type'];
        $phrase=$event['event'];
        if($type ==  'any_dev'){
            return true;
        }

        $res = array_filter($arr, function ($item) use ($keyword, $type,$phrase) {

            if($phrase=='enter'){

                    if ($type == 'dev_name') {
                        if ($keyword == $item['name'] ) {
                            return $item;
                        }

                    } else if ($type == 'keyword') {
                        if (stripos($item['name'], $keyword)!==false) {
                            return $item;
                        }
                    } else if ($type == 'mac') {
                        if ($keyword == strtolower(str_replace(":",'',$item['mac']))) {
                            return $item;
                        }
                    } else if ($type == 'discovery') {

                        if (in_array(strtolower(str_replace(":",'',$item['mac'])),$keyword)) {
                            return $item;
                        }
                    }


            } else if ($phrase=='exit'){

                    if ($type == 'dev_name') {
                        if ($keyword != $item['name']) {
                            return $item;
                        }

                    } else if ($type == 'keyword') {
                        if (stripos($item['name'], $keyword) === false) {
                            return $item;
                        }
                    } else if ($type == 'mac') {
                        if ($keyword != strtolower(str_replace(":",'',$item['mac']))) {
                            return $item;
                        }
                    } else if ($type == 'discovery') {
                        if (!in_array(strtolower(str_replace(":",'',$item['mac'])),$keyword)) {
                            return $item;
                        }
                    }
                }


        });

        return empty($res)?false:$res;
    }




}
