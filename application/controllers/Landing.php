<?php

class LandingController extends ApplicationController {

    public $isAuth=false;
    public $callBacks=array('reset_pwd','report_resetpwd');
    
     function indexAction(){
        /**
         *
         * 进入landing page
         */
//       if($_SESSION['user'] && $_SESSION['user']['uid']>0){
//            $this->redirect($this->c['site']['after_login_page']);
//        }


        $_SESSION['tmpId']=(uniqid("qr").'_'.mt_rand());
        /**
         *
         * assign qr tempid for qrlogin
         */
        $assignArr=array();

         $company=array(
             'hotline'=>'010-57389088',
             'email'=>'support@ryatek.com',
             'wechat'=>'ryatek',
             'qq'=>'121809321',
             'name'=>'Copyright © Rytek, Inc. All rights reserved.',
             'address'=>'北京望京望京街合生麒麟社2号楼2801'
         );
        $thumbNails=array(
            array(
                'title'=>'Title1',
                'desc'=>'description',
                'pic_url'=>'http://placehold.it/500x300'
            ),
            array(
                'title'=>'Title1',
                'desc'=>'description',
                'pic_url'=>'http://placehold.it/500x300'
            ),
            array(
                'title'=>'Title1',
                'desc'=>'description',
                'pic_url'=>'http://placehold.it/500x300'
            ),
            array(
                'title'=>'Title1',
                'desc'=>'description',
                'pic_url'=>'http://placehold.it/500x300'
            )

        );
        $assignArr['qrTmpId']=$_SESSION['tmpId'];
        $assignArr['cpRight']="Copyright © RuiYa Tech, Inc. All rights reserved.";

        if(in_array($_REQUEST['cb'],$this->callBacks)){
            $assignArr['callBack']=$_REQUEST['cb'];
        }
        if(preg_match("/^[0-9]{6}$/",trim($_REQUEST['request_code']),$ret)){
            $assignArr['requestCode']=$_REQUEST['request_code'];
        }
        if(preg_match("/^[a-zA-Z0-9]+$/",trim($_REQUEST['reset_token']),$ret)){
            $assignArr['resetToken']=$_REQUEST['reset_token'];
        }
        $assignArr=array_merge($assignArr,array('site'=>$this->c['site']->toArray(),'thumbNails'=>$thumbNails,'company'=>$company));


         /**
          * @todo landing.phtml静态化+如何处理qrcode登陆需要tmpid的情况
          */



         if($_GET['mode']=='editor' && $this->basicAuth()){
             $assignArr['editorMode']=true;
         }
         $this->v->assign($assignArr);
        return true;
    }
    function basicAuth(){
        if (!isset($_SERVER['PHP_AUTH_USER']) || $_SERVER['PHP_AUTH_USER']!='hx' || $_SERVER['PHP_AUTH_PW']!='hx20130*') {
            header('WWW-Authenticate: Basic realm="SNOW Login"');
            header('HTTP/1.0 401 Unauthorized');

            $this->code=-1;
            $this->msg="not authorized";
            exit('Unauthorized');

        }
        return true;
    }
    function productAction(){



    }
    



}