<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 1/29/15
 * Time: 6:51 PM
 */

class NotifierController extends ApplicationController {

        function routerEventNotifyAction(){


            $model=new PodNotifierDataMModel();

            $this->redis();
            $notify=$this->redis->brpoplpush("router.notify","router.notify",2);

            if(!empty($notify)){



                $event=$model->findOne(array('_id'=>new MongoId($notify)));


                $fog=new FogpodApi($event['ip'].":8888",'','',$this->redis);
                $token=$fog->token();
                if(empty($token)){
                    $this->code=-1;
                    $this->msg="req_router_token_failed";

                } else {
                    $devices=$fog->device_list();
                    if($devices){

                        if($event['notify_type']=='any_dev'){


                        } else if($event['notify_type']=='keyword') {


                        } else if($event['notify_type']=='dev_name') {

                        } else if($event['notify_type']=='mac') {

                        } else if($event['notify_type']=='self') {

                        }

                    }


                }
            }

        }

        function matchDev($arr,$keyword,$type){

            $res = array_filter($arr, function ($item) use($keyword,$type) {
                if($type=='dev_name'){
                    if($keyword==$item){
                        return $item;
                    }

                }
                else if ($type=='keyword') {
                    if(preg_match("/" . preg_quote($item) . "/", $keyword, $ret)){
                        return $item;
                    }
                }  else if ($type=='mac') {
                    if($keyword==''){
                        return $item;
                    }
                }
            });

            return !empty($res) || (strstr(strtolower($_SERVER['HTTP_USER_AGENT']),'mobile')!=false);
        }


}


