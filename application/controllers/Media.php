
<?php


 /**
  * 获取PDF文件页数的函数获取
  * 文件应当对当前用户可读（linux下）
  * @param  [string] $path [文件路径]
  * @return [array]        [数组第一位表示成功与否，第二位表示提示信息]
  */

class MediaController extends ApplicationController{

    //@todo 使用统一的upload类来处理
    public $prefix;
    public $mime;
    public $allowExts=array(
        'png','jpg','jpeg','pdf','doc','docx','ppt','pptx','xls','xlsx','gif','txt','csv','zip','gz','bz2','tgz','rar'
    );

    public function init(){

        session_start();

        parent::initEnv();
        parent::initHeader();
        header("Access-Control-Allow-Origin: *");
        $this->prefix=$this->c['application']['upload']['media'];


    }
    protected function uploadAvatarAction(){
        header("Access-Control-Allow-Origin: *");
        $data=array('mediaurl'=>'http://biz.n.cc/img/unknown_user.png');
        exit(json_encode(array(
            code=>1,
            msg=>'hello',
            data=>$data
        ),true));
        $crop = new CropAvatar(
            isset($_POST['avatar_src']) ? $_POST['avatar_src'] : null,
            isset($_POST['avatar_data']) ? $_POST['avatar_data'] : null,
            isset($_FILES['avatar_file']) ? $_FILES['avatar_file'] : null
        );
        $response = array(
            'state'  => 200,
            'message' => $crop -> getMsg(),
            'result' => $crop -> getResult()
        );
        exit(print_r(array($_POST,$_FILES,$response),true));
        return false;

    }
    protected function bytes_to_mb($bytes) {
        return round(($bytes / 1048576), 2);
    }
    public function rawUploadAction(){
        $tmpName=$_FILES['file']['tmp_name'];
        $names=explode(".",$_FILES['file']['name']);
        $mime=explode("/",$_FILES['file']['type']);
        $dir=$this->c['application']['upload']['media']."/".$mime[0]."/".date("ymd");
        if(!file_exists($dir))
        {
            @mkdir($dir,0777,true);
        }


        $file=tempnam($dir,($_SESSION['user']['uid']."_"));
        $dst=$file.".".$names[1];
        $res=move_uploaded_file($tmpName,$dst);
        if($res!==false)
        {
            $m=new MediaModel();
            $mid=$m->add(array(
                'href'=>$dst,
                'name'=>$_FILES['file']['name'],
                'uid'=>$_SESSION['user']['uid'],
                'ctime'=>time(),
                'mime'=>$_FILES['file']['type'],
            ));
            if($mid>0)
            {
                $this->data=array('mid'=>$mid);
            }
            else
            {
                $this->code="-1";
                $this->msg="add failure";
            }

        }
        else
        {

                $this->code="-1";
                $this->msg="add failure";

        }
        return $this->m();
    }
    public function getMediaAction(){

        $file=$_GET['f'];
        $info=pathinfo($file);
        Misc_Utils::z_header($info['extension']);
        readfile($this->c['application']['upload']['media'].$file);

        return false;

    }
    function printerUploadAction(){
        $allowExts=array(
            'application/excel',
            'application/mspowerpoint',
            'application/msword',
            'application/onenote',
            'application/pdf',
            'application/plain',
            'application/postscript',
            'application/powerpoint',
            'application/rtf',
            'application/vnd.ms-excel',

            'application/vnd.ms-office',

            'application/vnd.ms-powerpoint',

            'application/vnd.ms-word',

            'application/vnd.oasis.opendocument.chart',
            'application/vnd.oasis.opendocument.database',
            'application/vnd.oasis.opendocument.formula',
            'application/vnd.oasis.opendocument.graphics',
            'application/vnd.oasis.opendocument.graphics-template',
            'application/vnd.oasis.opendocument.image',
            'application/vnd.oasis.opendocument.presentation',
            'application/vnd.oasis.opendocument.presentation-template',
            'application/vnd.oasis.opendocument.spreadsheet',
            'application/vnd.oasis.opendocument.spreadsheet-template',
            'application/vnd.oasis.opendocument.text',
            'application/vnd.oasis.opendocument.text-master',
            'application/vnd.oasis.opendocument.text-template',
            'application/vnd.oasis.opendocument.text-web',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            'application/vnd.openxmlformats-officedocument.presentationml.slide',
            'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
            'application/vnd.openxmlformats-officedocument.presentationml.template',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.template',

            'application/wordperfect',

            'application/x-excel',

            'application/x-latex',

            'application/xml',
            'application/x-msexcel',

            'application/x-rtf',
            'image/*',

            'text/*'
        );

        $workFilename=$this->r->getParam('filename');

        $this->upload($workFilename,$allowExts);
        if($this->code==1){
            $mid=$this->data['mid'];
            $printer=new PodPrinterDataMModel();
            $pid=$printer->add(array(
                'mid'=>$mid,
                'url'=>$this->data['mediaurl'],
                'ctime'=>time(),
                'status'=>3,
                 'uid'=>intval($_SESSION['user']['uid'])
                )
            );
            $this->data['pid']=$pid;
        }

        ob_end_clean();
        return $this->m();

    }
    function thumbnail($client,$files){
        if(!is_array($files)){
            $files=array($files);
        }
        $files=array('img'=>array_map(function($file){
            return $this->c->application->upload->media.$file;

        },$files));

        $client->doBackground("image_thumbnail", json_encode($files),uniqid());
    }
    function photoUploadAction(){
        $allowExts=array(
            'image/*'
        );
        $workFiles=$_FILES['file'];
        $albumId=$_POST['aid'];

        if(Validator::isReg('mongoid',$albumId)){
            $albumModel=new AlbumMModel();
            $cond=array('_id'=>new MongoId($albumId),'uid'=>intval($_SESSION['user']['uid']));
            $album=$albumModel->findOne($cond);
            if(!empty($album)){
                $client= new GearmanClient();
                $client->addServer("127.0.0.1", 4730);
                $picData=$this->updateMultiple($client,$workFiles,$allowExts,2097152);
                $data=array_values($picData);
                $pids=array_keys($picData);
                $albumModel->update2(array('$push'=>array('photo_ids'=>(is_array($pids)?array('$each'=>$pids):$pids))),$cond);
                $this->data=$data;
                $this->e['isAjax']=true;


            } else {
                $this->code=-1;
                $this->msg="access_denied";
            }

        } else {
            $this->code=-1;
            $this->msg="err_req_format";
        }

        return $this->m();


    }
    function uploadAction(){

        $workFilename=$this->r->getParam('filename');
        $this->upload($workFilename,$this->allowExts);
        return $this->m();

    }

    function updateMultiple($client,$workFiles,$allowExts=array("*"),$size=5242880){
        $pids=array();

        foreach($workFiles['name'] as $key=>$name){
            $this->mime=mime_content_type($workFiles['tmp_name'][$key]);
            if($this->canUpload($this->mime,$allowExts) && $workFiles['size'][$key]<$size){
                $dst=$this->uploadFilename(array(
                    'name'=>$workFiles['name'][$key],
                    'tmp_name'=>$workFiles['tmp_name'][$key],
                    'type'=>$workFiles['type'][$key]
                ));
                $res=$this->normalUpload($workFiles['tmp_name'][$key],$dst);
                $mediaurl=str_replace($this->prefix,'',$dst);

                $photoModel=new PhotoMModel();
                $pid=$photoModel->add(array(
                        'pic_url'=>$mediaurl,
                        'ctime'=>time(),
                        'title'=>basename($workFiles['tmp_name'][$key]),
                        'uid'=>intval($_SESSION['user']['uid'])
                    )
                );

                $pids[$pid]=array(
                    'pic_url'=>$mediaurl,
                    'title'=>$workFiles['name'][$key],
                    'desc'=>$workFiles['name'][$key],
                    'pid'=>$pid

                );
                $this->thumbnail($client,$mediaurl);


            }

        }
        return $pids;
    }
    function mimeType($dst){
        return "".end(explode(".",$dst));
    }

    /**
     * @param $workFilename    传递
     * @param array $allowExts 允许的扩展名
     */
    function upload($workFilename,$allowExts=array("*")){


        $this->data=array('name'=>$workFilename);

        if(!empty($workFilename))
        {

            $retData=$this->postFilename($workFilename);
            $dst=$retData[0];
            $type=$retData[1];

            $this->postUpload($dst);

            $this->mime=$this->mimeType($dst);

            if($this->canUpload($this->mime,$allowExts)){
                $mediaurl=str_replace($this->prefix,'',$dst);
                $mid=$this->addRecord($mediaurl,$workFilename,$type);
                if($mid>0)
                {
                    $this->data=array('mid'=>$mid,'mediaurl'=>$this->c->application->upload->prefix.$mediaurl);
                }
                else
                {
                    $this->code="-1";
                    $this->msg="add_failed";
                }
            } else {
                $this->code="-1";
                $this->msg="denied_ext";
            }

        }
        else
        {
            $key=key($_FILES);

            if(is_array($_FILES[$key]['tmp_name'])){
                $seq=key($_FILES[$key]['tmp_name']);
                $_FILES[$key]['tmp_name']=$_FILES[$key]['tmp_name'][$seq];
                $_FILES[$key]['name']=$_FILES[$key]['name'][$seq];
                $_FILES[$key]['type']=$_FILES[$key]['type'][$seq];
                $_FILES[$key]['error']=$_FILES[$key]['error'][$seq];
                $_FILES[$key]['size']=$_FILES[$key]['size'][$seq];
            }
            $this->mime=$this->mimeType($_FILES[$key]['name']);
            if($this->canUpload($this->mime,$allowExts)){
                $dst=$this->uploadFilename($_FILES[$key]);
                $res=$this->normalUpload($_FILES[$key]['tmp_name'],$dst);
                $mediaurl=str_replace($this->prefix,'',$dst);

                if($res!==false)
                {
                    $mid=$this->addRecord($mediaurl,$_FILES[$key]['name']);

                    if($mid>0)
                    {
                        //Misc_Utils::setImgSession($mid,$mediaurl);
                        $this->data=array('mid'=>$mid,'mediaurl'=>$this->c->application->upload->prefix.$mediaurl);
                    }
                    else
                    {
                        $this->code="-1";
                        $this->msg="add failure";
                    }

                }
                else
                {

                    $this->code="-1";
                    $this->msg="add failure";

                }
            } else {
                $this->code="-1";
                $this->msg="denied_ext";
            }

        }

        $this->msg="upload successful";

        $this->isAjax=true;



    }
    function canUpload($curExt,$allowExts){

        return in_array($curExt,$allowExts);
    }
    function addRecord($mediaurl,$fileName,$type=''){
        $m=new MediaModel();
        return $m->add(array(
            'href'=>$mediaurl,
            'name'=>$fileName,
            'uid'=>empty($_SESSION['user']['uid'])?0:$_SESSION['user']['uid'],
            'ctime'=>time(),
            'mime'=>($type==''?$type:$_FILES['file']['type']),
        ));


    }
    function postUpload($fileName){
        $fp = fopen($fileName, "w");
        $putdata = fopen("php://input","r");
        while ($data = fread($putdata, 16384)) {
            fwrite($fp, $data);
        }

        fclose($fp);
        fclose($putdata);
    }
    function normalUpload($src,$dst){

        $res=move_uploaded_file($src,$dst);
        return $res;
    }
    function fileName($dir,$uid,$ext){

        return $dir."/".rand(0,9999).uniqid().'_'.$uid."_.".$ext;
    }
    function uploadFilename($file){

        $tmpName=$file['tmp_name'];
        $type=$file['type'];
        $mime=$this->mimeType($file['name']);

        $names=explode(".",$file['name']);
        $subdir="/".$mime."/".date("ymd");
        $dir=$this->prefix.$subdir;
        if(!file_exists($dir))
        {
            @mkdir($dir,0777,true);
        }
        $dst=$this->fileName($dir,$_SESSION['user']['uid'],$mime);


        return $dst;
    }
    function postFilename($workFilename){

        $workFilename=str_replace(".json",'',$workFilename);
        $names=pathinfo($workFilename);
        $type=Misc_Utils::getMime($names['extension']);

        $mime=explode("/",$type);

        $dir=$this->prefix."/".$mime[0]."/".date("ymd");


        if(!file_exists($dir))
        {
            @mkdir($dir,0777,true);
        }

        $dst=$this->fileName($dir,$_SESSION['user']['uid'],$names['extension']);
        return array($dst,$type);


    }


}




