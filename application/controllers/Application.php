<?php

class ApplicationController extends \Yaf\Controller_Abstract
{
    protected $layout = 'default';
    /**
     * @var $s session object
     */
    protected $s;
    /**
     *
     *
     * 是否启用二次认证
     * @var bool
     */


    public $_isDuelAuth=false;
    /**
     * $_exDuelAuthActions 存储额外的action:是否二次认证action,如果$_isDuelAuth=false,则$_exDuelAuthActions表示哪些需要认证的action,反之亦然
     *
     * $_duelAuthActions 二次认证方法 none,mobile#email,email#mobile,email,mobile
     * sms,
     * email
     * none
     */
    protected $_exDuelAuthActions=[];

    /**
     * @var $c 存储application.ini配置信息
     *
     */
    public $c;

    public $langType;
    /**
     * @var $v view object
     */
    protected $v;
    protected $redis_sub='';

    protected $redis='';
    protected $params;
    public $lang;
    //mongo log
    protected $mLog;
    //redis log
    protected $rLog;

    public $modelName;
    /**
     * @var $r request object
     */
    protected $entity;
    protected $img;
    protected $fonts;
    protected $css;
    protected $js;

    protected $lang_prefer;
    /**
     * @var $ch1 cache for 2 hours
     */
    protected $ch1;
    protected $_caches=array();
    /**
     * @var $c 存储特性
     *
     */
    protected $_feature=array();
    public $code = 1;
    public $msg = '';
    public $data = '';
    protected $needLogin = true;
    public $u = array();
    protected $hasView = true;
    protected $ver = '';
    protected $credential;
    protected $log;
    public $title;
    public $desc;
    protected $keyword;
    public $isAuth=true;
    protected $siteName='';
    protected $siteDesc='';
    protected $siteUrl='';
    protected $logintype='desk';
    protected $cToken='';
    protected $uid;
    protected $userObj;
    protected $aToken='';
    protected $sockUrl;
    protected $debug;
    protected $isJsonReq;
    protected $uri;
    protected $_logs;

    protected $_vars;
    /**
     * assigned view data
     * @var array
     */


    protected $module;
    protected $controllerAction;
    protected $assetPrefix='';


    /**
     * @var $exActions 存储额外是否登录认证例外的action,如果属性isAuth=false,则它表示哪些需要认证的action,反之亦然
     *
     */
    protected $exActions=array();
    protected $exCtokens;
    public $ret;
    public $model;
    /**
     * @var $isAjax 是否是ajax请求,有时判断程序不准确,通过它手动设定
     */
    public $isAjax;
    protected $toUrl;
    protected $env;
    protected $_isLog=false;
    protected $exFields;
    /**
     * @var $_enCsrfCheck 是否进行跨站攻击csrf检查
     */
    protected $_enCsrfCheck=true;
    /**
     * @var $_enRefCheck 是否检查是本站提交的Referer
     */
    protected $_enRefCheck=false;

    public $isApi;

    protected $_resource;

    protected $_events;

    protected function isLog(){

        return $this->_isLog;
    }
    function events(){
        return $this->_events;
    }
    function initLang(){
        $this->langType=Lang_Lang::getLangType();
        $this->lang = Lang_Lang::getLang($this->langType);

    }
    function tr(){
        $args=func_get_args();
        $locale=$this->lang['_langType'];
        $result=array();
        foreach($args as $item){
            $result[]=$this->formatMsg($this->lang,$item);
        }
        switch($locale){
            case 'zhcn':
                $result=implode("",$result);
                break;
            default:{
            $result=implode(" ",$result);
            }
        }
        return $result;

    }
    function translate(){
        $args=func_get_args();
        $locale=$this->lang['_langType'];
        $result=array();
        foreach($args as $item){
            $result[]=$this->formatMsg($this->lang,$item);
        }
        switch($locale){
            case 'zhcn':
                $result=implode("",$result);
                break;
            default:{
            $result=implode(" ",$result);
            }
        }
        return $result;
    }
    function extraEventData($openId,$uid,$notify){
        return $openId->findOne(array('uid'=>$uid,'provider'=>$notify),array('sns_id'));
    }
    function logs(){
        
        return $this->_logs;
    }

    /**
     * @param $lang
     * @param $arr
     * @param $langKeys ["_a"=>"a"] 表示翻译带字段a的值同时添加一个_a保留原值,惯例是给a前面加前缀_
     * @param $pluckKey
     * @return array  array('key1'=>[],'key2'=>[]) group by key
     */
    function fmtMsgPluck($lang,$arr,$langKeys,$pluckKey){
        $newArr=[];
        foreach($arr as $key=>&$val){
            foreach($langKeys as $key2=>$item){
                if(is_string($key2)){
                    $val[$key2]=$val[$item];
                }
                $val[$item]=$this->formatMsg($lang,$val[$item]);
                /**
                 * [name=>'hello']
                 * 翻译后变成name=>'您好',要保留原来的hello值可以使用langKey:['key'=>'item','item2']
                 */


            }
            $newArr[$val[$pluckKey]][]=$val;
        }

        return $newArr;
    }

    /**
     * @param $lang 传入翻译的语言,array('hello'=>'您好')
     * @param $arr
     * @param $langKeys ["_a"=>"a"] 表示翻译带字段a的值同时添加一个_a保留原值,惯例是给a前面加前缀_
     * @return array  array('key1'=>[],'key2'=>[]) group by key
     */
    function fmtMsgArr($lang,$arr,$langKeys){
        foreach($arr as $key=>&$val){
            foreach($langKeys as $key2=>$item){
                if(is_string($key2)){
                    $val[$key2]=$val[$item];
                }
                $val[$item]=$this->formatMsg($lang,$val[$item]);
                /**
                 * [name=>'hello']
                 * 翻译后变成name=>'您好',要保留原来的hello值可以使用langKey:['key'=>'item','item2']
                 */


            }
        }

        return $arr;
    }
    function triggerEvents($events,$model,$module,$controller,$action,$username){
        $args=get_defined_vars();

        $due=true;

        $events=$events[$action];
        $ua=new MobileDetect();

        $ua=sprintf("%s %s %s",$this->formatMsg($this->lang,$ua->getBrand()),$this->formatMsg($this->lang,$ua->getDeviceType()),$this->formatMsg($this->lang,$ua->getBrowser()));
        if(!empty($events)){
            $openId=new OpenidTokenModel();
            foreach($events as $notify=>$event){
                if(isset($event['cron'])){
                    $cron = Cron\CronExpression::factory($event['cron']);
                    $due=$cron->isDue();
                }
                if($due){

                    if(isset($event['freq'])){
                        $result=$model->last(array('module'=>$module,'controller'=>$controller,'action'=>$action,'username'=>$username),array('ctime'));
                        if((time()-$result['ctime']->sec)>$event['freq']){
                            $this->redis();

                            $extra=$this->extraEventData($openId,$_SESSION['user']['uid'],$notify);

                            $args=array_merge($args,$extra);
                            $title=$username." ".$this->formatMsg($this->lang,"a_{$controller}_{$action}").$this->formatMsg($this->lang,"on_where").$ua;
                            $content=$title;
                            $remark='';
                            /**
                             * sns_id
                             * nid=>1-10000 低五位保留作为系统内部使用
                             */
                            $data=array('title'=>$title,'content'=>$title,'to'=>array($extra['sns_id']),'remark'=>empty($event['remark'])?$remark:$event['remark'],'nid'=>1);
                            $this->redis->publish("{$notify}_log_action",json_encode($data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE));
                        }

                    }
                }
            }

        }



    }

    function formatMsg($lang,$data){


        if($lang[$data]){
            return $lang[$data];
        }
        else if(preg_match("/^([\w-]+)__([\w-]+)$/i",$data,$ret)){
            return $lang[$data]?$lang[$data]:($lang[$ret[1]]." ".$lang[$ret[2]]);
        } else {
            return $data;
        }

    }
    function getIp()
    {
        $onlineip = '';
        if (getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), 'unknown')) {
            $onlineip = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), 'unknown')) {
            $onlineip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'), 'unknown')) {
            $onlineip = getenv('REMOTE_ADDR');
        } elseif (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown')) {
            $onlineip = $_SERVER['REMOTE_ADDR'];
        }
        return $onlineip;


    }
    function doLog($mongoObj,$code,$msg,$extra=array()){


        $ret=$mongoObj->insert(array_merge(array(
            'module'=>$this->r->module,
            'controller'=>$this->r->controller,
            'action'=>$this->r->action,
            'data'=>json_encode($_POST,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES),
            'code'=>$code,
            'msg'=>$msg,
            'ip'=>$this->getIp(),
            'username'=>$_SESSION['user']['username']
        ),$extra));
        return $ret;
    }

    function error($msg=''){
        $this->code=-1;
        $this->msg=$msg;
        return $this->m();
    }
    public function m($code = '', $msg = '', $data = '')
    {
        /**
         * @todo make this support under several  conditions such as ajax request
         */
        ob_end_clean();
        if ($code=='') {
            $code=$this->code;
        }
        if ($msg=='') {
            $msg=$this->msg;
        }
        if ($data=='') {
            $data=$this->data;
        }

        //   if($this->c['application']['debug']==1)
        //        {
        //            $data=array_merge($data,array('SERVER'=>print_r($_SERVER,true),'COOKIE'=>print_r($_COOKIE,true),'POST'=>print_r($_POST,true),'GET'=>print_r($_GET,true)));
        //    }

//        var_dump(json_encode(array('code' => $code, 'msg' => $this->formatMsg($this->lang,$msg), 'data' => $data)));
//        exit;

        /**
         * 必须是POST类型的操作,并且满足声明了log的,比如定义admin.php默认是log的,声明 protected $_isLog=true;
         */
        $logM='';
        $events=$this->events();
        if($_SERVER['REQUEST_METHOD']!='GET' && $_SERVER['REQUEST_METHOD']!='OPTIONS'){
            if($logM==''){
                $logM=new OperationLogMModel();
            }
            if(($this->isLog()==true && !in_array($this->r->action,$this->logs(),true)) || ($this->isLog()==false && in_array($this->r->action,$this->logs(),true))){
                $ret=$this->doLog($logM,$this->code,$this->msg);
            }


        }
        if(!empty($events[$this->r->action])){
            if($logM==''){
                $logM=new OperationLogMModel();
            }
            $this->triggerEvents($events,$logM,$this->r->module,$this->r->controller,$this->r->action,$_SESSION['user']['username']);
        }


        if ($this->e['isAjax'] || $this->e['isJsonReq'] || $this->logintype=='hybrid' || $this->isAjax ) {
            if(isset($this->_caches[$this->r->action])){
                $this->cache($this->_caches[$this->r->action]);
            }else {
                $this->no_cache();
            }
            $msg=$this->formatMsg($this->lang,$msg);
            if(empty($data)){
                $data=1;
            } else {
                if(is_array($data)) {
                    $data=json_encode(empty($data)?1:$data,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
                }
            }


            $jsonStr=<<<EOF
{"code":$code,"msg":"$msg","data":$data}
EOF;

            exit($jsonStr);

        } else {

            if($this->code!=1){
                $toUrl=empty($this->toUrl)?'/index/error/e404':$this->toUrl;
                $this->redirect($toUrl);
                exit;

            }
            $this->v->assign(array('code' => $code, 'msg' => $msg, 'data' => $data));
            if(!empty($this->_viewTemp)){
                exit($this->v->render($this->_viewTemp));
            }
            return true;


        }


    }

    function feature(){
        return $this->_feature;
    }
    public function initHeader()
    {

        // header('X-Frame-Options: SAMEORIGIN'); header('X-Frame-Options: ALLOW-FROM uri');

        /**
         * @example security headers
         * X-Frame-Options: SAMEORIGIN
         * Strict-Transport-Security: max-age=14400
         * Facebook 使用了这些（配置了详细的CSP，关闭了XSS保护）：
         *strict-transport-security: max-age=60
         *x-content-type-options: nosniff
         *x-frame-options: DENY
         *x-xss-protection: 0
         *content-security-policy: default-src *;script-src  https://*.fbcdn.net http://*.fbcdn.net *.facebook.net  *.virtualearth.net *.google.com 127.0.0.1:* *.spotilocal.com:*  *.spotilocal.com:* https://*.akamaihd.net ws://*.facebook.com:*;
         */



        // header('Access-Control-Max-Age: 1800');    // cache for 1 day


         //header('X-Frame-Options: SAMEORIGIN');

        //header("X-Content-Security-Policy: allow 'self'; img-src *; object-src media1.com media2.com; script-src *.baidu.com *.qq.com *.sina.com *.google.com");
        //preventing browser from guessing resource type to avoid picture or other type resource as code to be executed
        // header('X-Content-Type-Options:nosniff');

            header('Access-Control-Allow-Headers: Content-Type,CToken,From-Agent,Atoken,Xuid,Cache-Control,X-Requested-With');
//        header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
//        header("Access-Control-Allow-Origin: www.fogpod.com");
         header('X-XSS-Protection:1;mode=block');


        $this->r = $this->getRequest();
        $this->uri = $this->r->getRequestUri();
        //强制hhtps包括子域名，includeSubDomains可选
        //strict-transport-security: max-age=16070400; includeSubDomains,allow cors cookie transfer
        //       header('Access-Control-Allow-Credentials: true');

        // Misc_Utils::no_cache();

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']) && strstr($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'], 'from-agent')!==false && $_SERVER['REQUEST_METHOD']=='OPTIONS') {


            exit;

        }
        $jsonUriReg = "@^/([^/]+.json|([^/]+/)+[^/]+.json)@";
        $xmlUriReg = "@^/([^/]+.xml|([^/]+/)+[^/]+.xml)@";
        $this->e=Yaf\Registry::get('env');
        if (preg_match($jsonUriReg, $this->r->getRequestUri())) {
            $this->e['isAjax']=true;
            $this->e['isJsonReq']=true;
            Misc_Utils::z_header('json');
        }
        $this->e['isWechat']=$this->isWechat();
        if (preg_match($xmlUriReg, $this->r->getRequestUri())) {
            Misc_Utils::z_header('xml');
        }
    }
    function isWechat(){
        return (!array_key_exists('wechat',$this->params) && stripos($_SERVER['HTTP_USER_AGENT'],'MicroMessenger')!==false)?"wechat":'';
    }
    function formatLog($msg,$context='',$level='info',$name='op_log'){

        return sprintf("[%s] $name.%s %s %s",date("Y-m-d H:i:s"),$name,$msg,is_array($context)?json_encode($context,JSON_UNESCAPED_UNICODE):$context);
    }
    function opFormat(){
        $data=implode(" ",array($this->r->module,$this->r->controller,$this->r->action));

        return $data;
    }

    protected function initEnv()
    {

        $this->log=Yaf\Registry::get("logger");
        $this->r=$this->getRequest();
        $this->c = Yaf\Application::app()->getConfig();
        $this->params=$this->r->getParams();
        $this->debug=$this->c['site']['debug'];
        $this->logintype=(isset($_SERVER['HTTP_FROM_AGENT']) && $_SERVER['HTTP_FROM_AGENT']=='phonegap')?'hybrid':'desk';

        $this->cToken=isset($_POST['ctoken'])?$_POST['ctoken']:(isset($_SERVER['HTTP_CTOKEN'])?$_SERVER['HTTP_CTOKEN']:false);

        if(isset($_POST['ctoken'])){unset($_POST['ctoken']);}

        $this->aToken=isset($_SERVER['HTTP_ATOKEN'])?$_SERVER['HTTP_ATOKEN']:false;


        $this->xuid=isset($_SERVER['HTTP_XUID'])?$_SERVER['HTTP_XUID']:'';

        /**
         * site info
         */
        $this->langType=Lang_Lang::getLangType();
        $this->lang = Lang_Lang::getLang($this->langType);

        $this->siteName=$this->c['application']['sitename'];
        $this->siteDesc=$this->c['application']['siteDesc'];

        if($this->r->method=='GET' && preg_match("/=[\w-@]+\.(json|xml)$/i",$_SERVER['QUERY_STRING'],$extName)){
            $pos1=(strrpos($_SERVER['QUERY_STRING'],"&"));
            $pos2=(strrpos($_SERVER['QUERY_STRING'],"="));
            if($pos1===false){
                $jsonName=key($_GET);

            } else {
                $jsonName=substr($_SERVER['QUERY_STRING'],$pos1+1,($pos2-$pos1-1));

            }
            $_GET[$jsonName]=$_REQUEST[$jsonName]=str_replace(".$extName","",$_REQUEST[$jsonName]);

        }


        $this->ver = time();

        $this->isApi=preg_match("/^api\.[^\.]+/i",$_SERVER['SERVER_NAME']);
        $this->siteUrl= $this->e['siteUrl'];
        $this->sockUrl= $this->e['sockUrl'];

        $this->debug=$this->c->site->debug;
        $this->js_test=$this->c->site->js_test;
        $this->r = $this->getRequest();
        $this->uri = $this->r->getRequestUri();
        $this->v = $this->getView();

        /**
         * dir init
         */
        $this->moduleName=strtolower($this->r->module);
        $this->img = $this->c->assets->dir->img;
        $this->css = $this->c->assets->dir->css;
        $this->js = $this->c->assets->dir->js;
        $this->fonts = $this->c->assets->dir->fonts;

        $this->controllerAction = strtolower($this->r->controller . "/" . $this->r->action);

        if ($this->r->module!='Index') {
            $this->assetPrefix=$this->moduleName."/";

            $this->v->_smarty->template_dir=$this->v->_smarty->template_dir."/".$this->moduleName;

        }

        /**
         * input/output
         */
        if($this->e['isJsonReq']) {
            $this->isAjax=true;
            if ($this->r->method=='POST' || $this->r->method=='PATCH' || $this->r->method=='PUT') {
                $data=file_get_contents("php://input");
                $_POST=json_decode($data, true);
                if(!empty($data) && is_null($_POST)){
                    $this->code=2;
                    $this->msg="error request json format";
                    $this->m();
                    exit;
                }

            } else if ($this->r->method=='GET') {
                $data=file_get_contents("php://input");
                $_GET=json_decode($data, true);

                if(!empty($data) && is_null($_GET)){
                    $this->code=2;
                    $this->msg="error request json format";
                    $this->m();
                    exit;
                }
            }

            $_REQUEST=array_map('trim', array_merge($_GET, $_POST));

        }

        //        $_PUT = array();
        //        if ('PUT' == $_SERVER['REQUEST_METHOD']) {
        //            parse_str(file_get_contents('php://input'), $_PUT);
        //        }
    }
    function cfg($defDb=''){

        $cfg=Yaf\Application::app()->getConfig();
        if($defDb==''){
            $defDb=$cfg['mongo']->defdb;
        }

        $dbArr=$cfg['mongo']->toArray();
        if(isset($dbArr[$defDb]['r'])){
            $read_idx=array_rand($dbArr[$defDb]['r']);
            $dbReadCfg=$dbArr[$defDb]['r'][$read_idx];
        }
        if(isset($dbArr[$defDb]['w'])){
            $write_idx=array_rand($dbArr[$defDb]['w']);
            $dbWriteCfg=$dbArr[$defDb]['w'][$write_idx];
        }


        return array($dbArr,$defDb,$dbReadCfg,$dbWriteCfg);
    }
//    function mlog($tbName='op_log'){
//
//        if($this->mlog==''){
//            $this->mlog=Yaf\Registry::get("mongo_log");
//        }
//
//
//        if(class_exists('MongoClient')){
//
//
//
//                $cfg=Yaf\Application::app()->getConfig();
//
//                $defDb=$cfg['mongo']->defdb."_log";
//
//                list($dbArr,$defDb,$cfg,$dbReadCfg,$dbWriteCfg)=$this->cfg($defDb);
//
//
//            $readDsn="mongodb://{$dbReadCfg['uid']}:{$dbReadCfg['pwd']}@{$dbReadCfg['host']}:{$dbReadCfg['port']}/{$dbReadCfg['db']}";
//            $writeDsn="mongodb://{$dbWriteCfg['uid']}:{$dbWriteCfg['pwd']}@{$dbWriteCfg['host']}:{$dbWriteCfg['port']}/{$dbWriteCfg['db']}";
//
//            $wdb = new MongoClient($writeDsn, $dbWriteCfg['options']);
//            $this->wdb=$wdb->$defDb;
//            $this->w=$wdb->selectCollection($defDb,'op_log');
//
//            if(($this->dbReadCfg['host']==$this->dbWriteCfg['host'] && $this->dbReadCfg['port']==$this->dbWriteCfg['port']) || empty($this->dbWriteCfg['host'])){
//                $this->rdb=$this->wdb;
//                $this->r=$this->w;
//
//            } else {
//                $rdb=new MongoClient($readDsn, $this->dbReadCfg['options']);
//                $this->rdb=$rdb->$defDb;
//                $this->r=$rdb->selectCollection($defDb,$this->tbName());
//
//            }
//            Yaf\Registry::set("mongo_read_db", array($this->rdb,$this->dbReadCfg['db']));
//            Yaf\Registry::set("mongo_write_db", array($this->wdb,$this->dbReadCfg['db']));
//
//
//
//        }
//        return $this->redis;
//    }
    function redis($db=''){

        if($this->redis==''){
            $this->redis=Yaf\Registry::get("redis");
        }
        if(class_exists('Redis')){
            if(empty($this->redis)){
                $this->redis=new Redis();
                $ret=$this->redis->pconnect($this->c['redis']['host'], $this->c['redis']['port'], $this->c['redis']['timeout']);
                if($ret && !empty($this->c['redis']['pwd'])){
                    $authed=$this->redis->auth($this->c['redis']['pwd']);
                    if($authed)
                    {

                        Yaf\Registry::set("redis", $this->redis);

                    }

                }
            }


        }
        return $this->redis;
    }
    function redis_sub(){

        if(empty($this->redis_sub)){
            $this->redis_sub=Yaf\Registry::get("redis_sub");
        }

        if(class_exists('Redis')) {
            if (true || strtolower(PHP_SAPI) == 'cli' && empty($this->redis_sub)) {
                echo("cli");
                $this->redis_sub = new Redis();

                $ret=$this->redis_sub->pconnect($this->c['redis']['host'], $this->c['redis']['port']);

                if($ret && !empty($this->c['redis']['pwd'])){
                    $authed=$this->redis_sub->auth($this->c['redis']['pwd']);
                    if($authed)
                    {

                        Yaf\Registry::set("redis_sub", $this->redis_sub);

                    }

                }


            }
        }

    }
    function mongo($db){


    }
    function mongoRead(){


    }
    function mongoWrite(){


    }
    protected function initCache()
    {

        $this->ch1=Yaf\Registry::get("cache");
        $this->ch2=Yaf\Registry::get("cache30");
    }
    protected function adminAuth()
    {

        $module=$this->moduleName;

        $controller=strtolower($this->r->controller);

        $action=strtolower($this->r->action);

        $action2='';

        $array=array(
            'GET'=>'view',
            'POST'=>'add',
            'PATCH'=>'edit',
            'PUT'=>'edit',
            'DELETE'=>'recordsRemove'
        );
        if($module=='admin' && $controller=='table' ){
            /**
             * Rest 请求时候可以隐藏Table和表名
             */
            if($action=='resti'){
                $result=$this->r->getParams();


                if(isset($result['action'])){
                    $_POST['resti']['action']=$result['action'];

                }


                $action2=$array[$this->r->method];

                $_REQUEST['ztb']=$_POST['ztb']=$result['ztb'];

                if($this->r->method=='GET'){
                    $_REQUEST['ztb']=$_GET['ztb']=$result['ztb'];
                }
            } else if($action=='rest'){
                $result=$this->r->getParams();



                if(isset($result['ids'])){
                    $_POST['ids']=$result['ids'];

                }


                $action2=$array[$this->r->method];

                $_REQUEST['ztb']=$_POST['ztb']=$result['ztb'];

                if($this->r->method=='GET'){
                    $_REQUEST['ztb']=$_GET['ztb']=$result['ztb'];
                }

            }


        }





        $tbName=$_REQUEST['ztb'];
        if(!empty($_REQUEST['ztb'])){
            if(Validator::isEn($tbName)==false){
                $this->code=$this->c['ret']['FieldFormatError'];
                $this->msg='invalid_data';
                return $this->m();
            } else {
                if(empty($this->modelName())){
                    $this->setModelName($this->classFromTb($tbName));
                }


            }

        }

        $reqResource="$module/$controller/$action";

        $rights2=array();
        if(true || empty($_SESSION['user']['rights'])){

            $roleRes=new RoleResourceModel();
            $rights=$roleRes->getByRoleId($_SESSION['user']['level']);
            foreach($rights as $key=>$val){
                $rights2[$val['type']][$val['title']]=$val;
            }
            $_SESSION['user']['rights']=$rights2;
        }


        /**
         * GET的请求验证是对controller否有读权限
         */
        $tbName=trim($_REQUEST['ztb']);
        $model=$this->modelName();
        $tableName='';
        if(!empty($model) && class_exists($model)){
            $tableName=$model::$_tbName;
        }




        if($this->r->method=='GET' && !in_array($reqResource,array("{$module}/index/index"),true) && "$module/$controller"!='admin/test'){


            $matchRes = Misc_Utils::matchedRights($rights2, $reqResource);
            $size = count($matchRes);
            if ($size == 0) {
                $this->code = $this->c['ret']['NoPermToReadRoute'];
                $this->msg = "NoPermToReadRoute";

                return $this->m();
            } else if ($size == 1) {

                if (!($rights2['c'][$matchRes[key($matchRes)]] & 4 == 4)) {
                    $this->code = $this->c['ret']['NoPermToReadRoute'];
                    $this->msg = "NoPermToReadRoute";
                } else if (false && $tableName != '' && ($rights2['t'][$tableName]['rights'] & 4) != 4) {

                    $this->code = $this->c['ret']['NoPermToReadRoute'];
                    $this->msg = "NoPermToReadRoute";

                    return $this->m();
                }
            }
        }


        /**
         * 非GET的请求验证是否对controller有写权限
         */

        if(($this->r->method=='PATCH' || $this->r->method=='DELETE' || $this->r->method=='PUT' || $this->r->method=='POST') && $reqResource!="{$module}/index/index"){
            $matchRes=Misc_Utils::matchedRights($rights2,$reqResource);
            $size=count($matchRes);
            if($size==0) {

                $this->code = $this->c['ret']['NoPermToReadRoute'];
                $this->msg = "NoPermToReadRoute";
                return $this->m();

            }else if($size>=1){
                /**
                 *  验证是否对表有操作权限,@todo 做权限验证
                 */
                if(!($rights2['c'][$matchRes[key($matchRes)]] & 2 == 2)){
                    $this->code=$this->c['ret']['NoPermToReadRoute'];
                    $this->msg="NoPermToReadRoute";
                }    else if(false && $tableName!='' && ($rights2['t'][$tableName]['rights'] & 2) != 2 ){
                    $this->code=$this->c['ret']['NoPermToReadData'];
                    $this->msg="NoPermToReadData";
                    return $this->m();
                }
            }
        }





        if($module=='admin' && $controller=='table'  && ($this->r->action=="rest" || $this->r->action=="resti")){

            $this->forward('Admin','Table',strtolower($action2));

            return FALSE;

        }
    }
    function modelName(){
        return $this->modelName;
    }
    function formatRight($action){
        if(filter_var($action,FILTER_VALIDATE_INT)){
            return intval($action);
        }
        $actions=preg_split("/[\s,;]/i",$action);
        $actions=array_unique($actions);
        $val=0;

        foreach($actions as $item){

            if($item=='read'){
                $val+=4;
                continue;
            }
            else if($item=='write'){
                $val+=2;
            }
            else if($item=='rw'){
                /**
                 * prevent when using right format like rw,read,write
                 */
                if($val & 2==2){$val=-2;}
                if($val & 4==4){$val=-4;}
                $val+=6;
            }

        }
        return $val;
    }
    function commonCheck($tbName){
        $level=$_SESSION['user']['level'];




        if(Validator::isEn($tbName)==false){
            $this->code=$this->c['ret']['FieldFormatError'];
            $this->msg='invalid data';
            return $this->m();
        }

        if($this->R->canDo($level,$tbName)) {
            $model = $this->classFromTb($tbName);
            if (class_exists($model)) {
                $model = new $model;
                return $model;
            }  else
            {
                $this->code=$this->c['ret']['TableNotExist'];
                $this->msg='invalid request';

                return $this->m();
            }
        } else{
            $this->code=$this->c['ret']['NoPermToRead'];
            $this->msg='you have no permission to operate';
            return $this->m();
        }



    }
    function cache($sec){

        $ts = gmdate("D, d M Y H:i:s", time() + $sec) . " GMT";
        header("Expires: $ts"); header("Pragma: cache");
        header("Cache-Control: max-age=$sec");
    }
    function no_cache()
    {
        header("Cache-control:no-cache,no-store,must-revalidate,post-check=0,pre-check=0");
        header("Expires: Mon, 20 Jul 1995 04:10:20 GMT");
        header("Pragma: no-cache");
    }
    function initSite(){

        if(empty($_SESSION['site'])){

            $model=new ProductModel();
            $_SESSION['site']=$model->findOne(array('name'=>$this->c['site']['name']));

        }

    }
    function initAdmPage(){

        $this->initSite();

        $this->initAdmMenus();

        $_SESSION['site']=$_SESSION['site']+$this->c['site']->toArray();
        $this->v->assign('site',$_SESSION['site']);

    }
    function setActivesMenu(&$menus,$menuId,&$menuActives){
        if(isset($menus[$menuId])){
            array_unshift($menuActives,intval($menuId));
            return $this->setActivesMenu($menus,$menus[$menuId]['pid'],$menuActives);
        }
    }
    function getByUidAction(){

        $content=$_SESSION['user']['uid'];

    }
    function initAdmMenus($menuId=''){

        if(empty($_SESSION['site']['menus'])){
            $model=new MenuModel();
            $menuId=$menuid=intval($_GET['menuid']);
            if(!empty($menuid)){
                $_SESSION['site']['menuid']=$menuid;
            }
            $_SESSION['site']['menus']=$model->find(array('product_name'=>$this->c['site']['name'],'level'=>$_SESSION['user']['level']),10000);


//
//            $arr=array();


            $_SESSION['site']['menus']=array_map(function(&$item) use (&$menus,$menuId){
                $item['name']=$this->lang[$item['name']];
                $menus[$item['id']]=$item;
                return $item;
            },$_SESSION['site']['menus']);



            $menuActives=array();

            if(!empty($menuid)){
                $this->setActivesMenu($menus,$menuId,$menuActives);
            }


            $menuActives=array_fill_keys($menuActives,1);
            $menuActives=array_intersect_key($menus,$menuActives);
//
//
//            $_SESSION['site']['vmenus']=Misc_Utils::getNavMenus($menus,$menuid);


            $vmenus=array_filter($_SESSION['site']['menus'],function($item){
                return $item['hidden']==0;
            });
            $m1=microtime(true);
            Misc_Utils::getSubMenus($vmenus, $_SESSION['site']['menus']['vmenus'],function($item) use(&$menuActives) {
                if(preg_match("/^\s*[\w-]+\s*[\w-]+\s*$/i",$item['icon'],$ret)){
                    $item['isUrl']=false;
                    if(isset($menuActives[$item['id']])){
                        $menuActives[$item['id']]['isUrl']=false;
                    }

                }

                if(isset($menuActives[$item['id']])){
                    $item['z_is_active']=1;
                }
                return $item;

            });
            $m2=microtime(true);

//            $_SESSION['site']['menus']['vmenus']=Misc_Utils::getSubMenus3($vmenus,function($item) use(&$menuActives){
//                if(preg_match("/^\s*[\w-]+\s*[\w-]+\s*$/i",$item['icon'],$ret)){
//                    $item['isUrl']=false;
//                    if(isset($menuActives[$item['id']])){
//                        $menuActives[$item['id']]['isUrl']=false;
//                    }
//
//                }
//
//                if(isset($menuActives[$item['id']])){
//                    $item['z_is_active']=1;
//                }
//                return $item;
//            });



//            <ol id='auto-radio-buttons' data-name='foo'>
//    <li class='expanded' data-value='0'>All
//        <ol>
//            <li data-value='1'>One</li>
//            <li data-value='2'>
//                Two
//                <ol>
//                    <li data-value='3'>
//                Three
//                        <ol>
//                            <li data-value='4' data-checked='1'>Four</li>
//                        </ol>
//                    </li>
//                    <li data-value='5'>Five</li>
//                </ol>
//            </li>
//        </ol>
//    </li>
//</ol>




            $_SESSION['site']['menus']['vmenus']=Misc_Utils::sort_by_inner_key($_SESSION['site']['menus']['vmenus'],'order',SORT_ASC);

            $_SESSION['site']['menus']['hmenus']=$menuActives;

            //  var_dump($menuActives, $_SESSION['site']['menus']['hmenus']);exit;

        }
    }

    protected function basicAuth()
    {

        /**
         * if request method is OPTIONS and request headers contain header from-agent,then exit
         *
         */

        // !($this->r->controller == 'Landing' && in_array($this->r->action, array('index','qrlogin','usecases')))
        if (strtolower(PHP_SAPI)!='cli' && !in_array($this->r->controller,array('Anychat','Notifypush')) && !$this->e['isLocal'] && (!isset($_SERVER['PHP_AUTH_USER']) || $_SERVER['PHP_AUTH_USER'] != 'hx' || $_SERVER['PHP_AUTH_PW'] != 'hx20130*')) {
            header("HTTP/1.0 404 Not Found");
            exit;

        }
    }
    protected function deskAuth()
    {
        if(!empty($_GET['wid']) && isset($_GET['uid']) && ($_COOKIE['uid']>0) && stripos($_SERVER["HTTP_USER_AGENT"],"MicroMessenger")){

            $profile=new UserProfileModel();
            $uid=intval($_COOKIE['uid']);
            $result=$profile->getByUser($uid,array('wechat_qlogin','qlogin_token'));
            $wid=md5($this->c->site->key.$uid.$_COOKIE['wid']);
            if($result['wechat_qlogin']==1 && $result['qlogin_token']==$wid){
                $_COOKIE['uid']=intval($_COOKIE['uid']);
                $token=md5($_COOKIE['wid']);
                $tempid=Misc_Utils::genToken(16);
                $this->ch1->setItem( $_COOKIE['uid'],$token."_".$tempid);
                ob_end_clean();
                $form=Misc_Utils::buildSubmitForm(array(
                    "tempid"=>$tempid,'uid'=>$_COOKIE['uid'],'token'=>$token
                ),'/user/login');
                $html5=<<<EOF
            <!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
$form
</body>
</html>
EOF;

                //  header('Location:http://baidu.com');exit;
                exit($html5);

            }


        }

        if (strtolower(PHP_SAPI)!='cli' && ((!in_array($this->r->action, $this->exActions) && ($this->isAuth===true || $_SESSION['user']['locked'] == true)
                    || (in_array($this->r->action, $this->exActions) && $this->isAuth===false)
                )
                && (empty($_SESSION["user"]["uid"]) || $_SESSION["user"]["uid"] <= 0 )

            )
        ) {

            $this->authError($this->lang['denied_operation']);

        }




        if ($this->_enCsrfCheck && !in_array($this->r->action, $this->exActions) && $this->r->method == 'POST' && isset($_SESSION['ctoken']) && $this->cToken != $_SESSION['ctoken'])      {

            // $this->authError('invalid post');
        }


    }
    protected function hybridAuth()
    {
        $data=$this->ch1->getItem($this->uid);

        if (((in_array($this->r->action, $this->exActions) && $this->isAuth===false) || (!in_array($this->r->action, $this->exActions) && $this->isAuth===true)  || ( $_SESSION['user']['locked'] == true)) && (empty($this->aToken) || empty($data) || ($data['atoken']!=$this->aToken))) {

            $this->authError("need login");
            exit;

        }
        if (!empty($data['atoken']) && $data['atoken']==$this->aToken) {
            $this->uid=$this->xuid;
            $this->userObj = new CommonUserModel();
            $_SESSION['user']=$this->userObj->getUserData(array('uid'=>$this->uid));

        }


    }
    protected function loginAuth()
    {
        $tbName=$_REQUEST['ztb'];


        if ($this->moduleName=='index' && $this->r->action=="getregcode" && $this->r->controller=='user'){

        }
//        if(!empty($_SESSION['user']) && $_SESSION['user']['level']==100){
//            $this->authError("not allowed to login");
//            exit;
//        }
        $_SESSION['loginType']=$this->logintype;
        if (!empty($_COOKIE['token'])) {

            $sid=$this->ch1->getItem($_COOKIE['token']);
            $user=Misc_Utils::unserializesession($sid);

            if ($user['user']['uid']==$_COOKIE['uid']) {

                session_decode($sid);
                $_SESSION['loginType']=$this->logintype;

            }

        }
        /**
         * 1为普通用户,只有大于=9的才可以登录后台
         */

        if($this->moduleName==='admin'){
            /**
             * non super admin  needs check privileges
             *
             */

            if(empty($_SESSION['user'])){
                $this->code=$this->c['ret']['NeedLogin'];
                $this->msg="NeedLogin";
                $this->toUrl=$this->c['site']['backend']['login'];

                return $this->m();

            } else if($_SESSION['user']['level']>8){

                if($_SESSION['user']['level']!=9){

                    $this->adminAuth();
                } else {
                    $modelName=$this->modelName();
                    if(empty($modelName)){
                        $this->setModelName($this->classFromTb($_REQUEST['ztb']));
                    }

                }

            }  else {
                $this->authError('access not allowed');
            }



        }
        if ($_SESSION['loginType']=='desk' || !isset($_SESSION['loginType'])) {
            $this->deskAuth();
        } else if ($_SESSION['loginType']=='hybrid') {
            $this->hybridAuth();

        }



    }
    function setModelName($modelName){

        $this->modelName=$modelName;

    }
    protected function readAction(){
        $m=$this->getModel();

        if($m) {

            $primary = $m->primary();

            $meta = $m->tbMeta();
            if (isset($meta['uid'])) {
                $res = $m->find(array('uid' => $_SESSION['user']['uid']));
            }
        }

        $this->data=$res;


        return $this->m();
    }
    protected function authError($msg = '', $data = array())
    {

        if ($this->e['isAjax']) {
            $this->m(-1, $msg, $data);
            exit;
        } else {
            $this->redirect($this->c['page']['landing']);
        }
        exit;
    }

    public function initUserData()
    {


        if (!isset($_SESSION['ctoken'])) {
            $_SESSION['ctoken'] = Misc_Utils::gen_csrf_token();


        }

        header("ctoken:{$_SESSION['ctoken']}");
        if (!empty($_SESSION['user']['uid'])) {


            // Misc_Utils::regOnlineUser($_SESSION['user']['uid'],);
            $_SESSION['user']['on']=1;
            $this->uid=$_SESSION['user']['uid'];
            $this->u['user'] = $_SESSION['user'];
            if (!empty($_SESSION['setting'])) {
                $this->u['setting']=$_SESSION['setting'];
            } else {
                $setting = new UserSettingModel();

                $_SESSION['setting']=$this->u['setting']=array_merge($setting->getByUser($this->u['user']['uid']), $this->c->setting->toArray());

            }
            if (!empty($_SESSION['friends'])) {
                $this->u['friends']=$_SESSION['friends'];
            }


        }
        $this->u['user'] = isset($this->u['user']) ? $this->u['user'] : '';




    }
    function resource(){
        return $this->_resource;
    }
    function keyword(){
        /**
         * 页面title设置可以是application.ini全局,可以是controller的title,可以是resource里指定到某个page
         */
        $res=$this->resource();
        $this->keyword=!empty($res[$this->r->action]['keyword'])?$res[$this->r->action]['keyword']:$this->keyword;
        if(empty($this->keyword)){

            $this->keyword=($this->moduleName==='admin')?$this->c->site->backend_keyword:$this->c->site->front_keyword;
        }

        return $this->lang[$this->keyword]?$this->lang[$this->keyword]:$this->keyword;
    }
    function logo(){
        /**
         * 页面title设置可以是application.ini全局,可以是controller的title,可以是resource里指定到某个page
         */
        $res=$this->resource();
        $this->logo=!empty($res[$this->r->action]['logo'])?$res[$this->r->action]['logo']:$this->logo;
        if(empty($this->logo)){

            $this->logo=($this->moduleName==='admin')?$this->c->site->backend_logo:$this->c->site->front_logo;
        }

        return $this->lang[$this->logo]?$this->lang[$this->logo]:$this->logo;
    }
    function desc(){
        /**
         * 页面title设置可以是application.ini全局,可以是controller的title,可以是resource里指定到某个page
         */
        $res=$this->resource();
        $this->desc=!empty($res[$this->r->action]['desc'])?$res[$this->r->action]['desc']:$this->desc;
        if(empty($this->desc)){

            $this->desc=($this->moduleName==='admin')?$this->c->site->backend_desc:$this->c->site->front_desc;
        }

        return $this->lang[$this->desc]?$this->lang[$this->desc]:$this->desc;
    }
    function title(){
        /**
         * 页面title设置可以是application.ini全局,可以是controller的title,可以是resource里指定到某个page
         */
        $res=$this->resource();
        $this->title=!empty($res[$this->r->action]['title'])?$res[$this->r->action]['title']:$this->title;
        if(empty($this->title)){

            $this->title=($this->moduleName==='admin')?$this->c->site->backend_title:$this->c->site->front_title;
        }

        return $this->lang[$this->title]?$this->lang[$this->title]:$this->title;
    }
    protected function initWebview()
    {

        /**
         * detect device type
         */

        $ug=$_SERVER["HTTP_USER_AGENT"];
        $win7plus=preg_match("/windows NT 6.+/i",$ug);
        $ie10plus=preg_match("/rv:1\d.+/i",$ug);
        $isMobile=preg_match("/mobile/i",$ug);
        $isWinPhone=preg_match("/WindowsPhoneOS/i",$ug);
        $isWinMobile=preg_match("/WindowsPhoneOS/i",$ug);
        $isIe=preg_match("/internet explorer/i",$ug);
        $isWinMobile=preg_match("/WindowsPhoneOS/i",$ug);
        $isIos=preg_match("/iphone|ipad|ipod|iwatch/i",$ug);
        $isAndroid=preg_match("/android/i",$ug);
        $isTablet=preg_match("/tablet/i",$ug);
        $controller=strtolower($this->r->controller);
        $viewHeader=$this->c['application']['view']['dir']."/common/header_{$controller}.".$this->c['application']['view']['ext'];

        /**
         *  Set the layout.
         * $this->getView()->setLayout($this->layout);
         *  ###use nutcracker as session
         *  - add config in php.ini
         *  - or use the following code
         */
        //        $regrules=Yaf\Registry::get("regrules");
        //        $rules=array();
        //        foreach($regrules as $key=>$val){
        //
        //            $rules[$key]=array
        //        }

        $model2=$this->modelName();

        if(!empty($model2) && class_exists($model2)){
            $model2=$model2::$_tbName;
            $modelJs="{$this->assetPrefix}js/".strtolower($this->r->controller)."/model_$model2";
        }
        $wechat=(!array_key_exists('wechat',$this->params) && stripos($_SERVER['HTTP_USER_AGENT'],'MicroMessenger')!==false )?"":'';


        $assignArr= array(
            "usernameEditable"=>empty($_SESSION['user']['can_edit_username'])?0:$_SESSION['user']['can_edit_username'],
            "isDebug"=>$this->debug,
            "isJsTest"=>$this->js_test,
            "left_logo"=>$this->c->site->left_logo,
            "wechat"=>$this->isWechat(),
            "d"=>$this->d,
            //如果要引入amd的模块，防止cmd的seajs引起冲突
            "jsLib"=>array("seajs"=>true),
            "action"=>$this->r->action,
            "feature"=>$this->feature(),
            "featureJson"=>json_encode($this->feature(),true),
            "menuId"=>isset($_GET['menuid'])?$_GET['menuid']:'',
            "siteTheme"=>$this->siteTheme,
            "ival"=>Misc_Utils::convert_reg_php2js(Yaf\Registry::get("regrules")),
            "siteCfg"=>json_encode($this->c['site']->toArray(),JSON_UNESCAPED_UNICODE),
            "tempdir"=>$controller,
            "actionHeaderJs"=>file_exists("{$this->assetPrefix}js/{$controller}/header.js")?"{$controller}/header":'',
            "debug"=>$this->debug,
            "viewHeader" => file_exists($viewHeader)?$viewHeader:'',
            "emoji_spliter_l"=>":#",
            "chats"=>"[]",
            "emoji_spliter_r"=>"#:",
            "viewdir"=>$this->r->module=='Index'?"":"/".$this->moduleName.'/'.$controller.'/',
            "title"=>$this->title(),
            "keyword"=>$this->keyword(),
            "sitedesc"=>$this->desc(),
            "logo"=>$this->logo(),
            "uploadPrefix"=>$this->c->application->upload->prefix,
            "siteName"=>$this->siteName,
            "siteDesc"=>$this->siteDesc,
            "siteUrl"=>$this->siteUrl,
            "win7plus"=>$win7plus,
            "ie10plus"=>$ie10plus,
            "isMobile"=>$isMobile,
            "isTablet"=>$isTablet,
            "winphone"=>$isWinPhone,
            "winmobile"=>$isWinMobile,
            "ie"=>$isIe,
            "loadActionJs"=>true,
            "ios"=>$isIos,
            "android"=>$isAndroid,
            "jsdir"=>$this->c['assets']['jsdir'],
            "cssdir"=>$this->c['assets']['cssdir'],
            "fontdir"=>$this->c['assets']['fontdir'],
            "imgdir"=>$this->c['assets']['imgdir'],
            "filebase"=>$this->c['application']['filebase'],
            "actionCss" => file_exists("{$this->assetPrefix}css/{$this->controllerAction}.css")?"/{$this->assetPrefix}css/{$this->controllerAction}.css":'',
            "modelJs" => (!empty($modelJs) && file_exists($modelJs.".js"))?"/{$modelJs}":'',

            "actionJs" => file_exists("{$this->assetPrefix}js/{$this->controllerAction}.js")?"/{$this->assetPrefix}js/{$this->controllerAction}":'',
            "module" => $this->controllerAction,
            "moduleName"=>$this->moduleName,
            "langJson" => json_encode($this->lang,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE),
            "langType"=>$this->langType,
            "lang" => $this->lang,
            "friendsJson"=>array(),
            "groupJson"=>array(),
            "setting"=>'{}',
            "meta"=>json_encode(array(
                'status'=>array('0'=>'offline','1'=>'online')
            )),

            "commonCss"=>file_exists("{$this->assetPrefix}css/{$controller}/common.css")?"/{$this->assetPrefix}css/{$controller}/common.css":'',
            "user" => isset($this->u['user']) ? $this->u['user'] : '',
            "userJson" => empty($this->u['user'])?'{}':json_encode($this->u['user']),
            "d"=>$this->d,
            "ctoken" => $_SESSION['ctoken']
        );

        $this->v->assign($assignArr);
    }
    function classFromTb($tbName,$isMongo=false,$sufix="Model"){
        if($isMongo){
            $sufix='M'.$sufix;
        }
        $tbs=explode('_',$tbName);
        $tbName='';
        $tbs=array_map(function($tb){
            return ucwords($tb);

        },$tbs);

        return join('',$tbs).$sufix;

    }
    public function getKeys($model){

        $keys=array_keys($model->tbMeta());

        $keys2=explode(",",$model->primary());


        return array_unique(array_merge($keys,$keys2));
    }

    protected function listFromIdAction(){
        $m=$this->getModel();
        if($m){

        }

    }
    protected function listFromUidAction(){
        $m=$this->getModel();
        if($m){

        }

    }
    protected function listAction(){
        $m=$this->getModel();
        if($m){

        }

    }
    protected function getModel(){
        $model="{$this->model}Model";
        $m='';
        if (class_exists($model)) {
            $m=new $model();
            return $m;
        }
        if (empty($m)) {
            $this->code=$this->c['ret']['TableNotExist'];
            $this->msg="Table Not Exist";
            return false;
        }

    }
    function filter($data,$action='create'){
        return $data;
    }
    function doMongoRest($model){
        $modelName=$model."Model";
        if(class_exists($modelName)) {
            $object=new $modelName();
            if($this->r->method=='POST'){
                $id=$object->insert($_POST);
                $this->data=array('id'=>$id);

            } else if($this->r->method=='GET') {

                return $this->m();
            }
        } else {
            $this->code=-1;
            $this->msg="invalid object";
        }

        return $this->m();
    }
    function doRest($model){
        $modelName=$model."Model";
        if(class_exists($modelName)){
            $object=new $modelName();
            //针对w.cc/ibeacon/comment/?ibeacon_id=1.json形式请求
            if($object->driver=='mongo'){

            }
            $ret=$object->check();


            $method="rest".$this->r->method;
            if($ret[0]){
                if(method_exists($object,$method)){
                    if($this->r->method=='GET'){
                        if(empty($ret[1])){
                            //针对主键id的请求
                            $id=$this->r->getParam("id");
                            if(!empty($id)){
                                $ret[1][$object->primaryId()]=$id;
                            } else {
                                $ret[1]='';
                            }

                        }
                    }
                    $result=call_user_func_array(array($object,$method),array($ret[1]));

                    if(empty($result) && $this->r->method!='GET'){
                        $this->code=-1;
                        $this->msg="server operate failed";
                    } else {
                        if($this->r->method=='GET'){
                            $this->data=$result;

                        } else {
                            /**
                             * 如果是非get操作就是更新数据,返回操作的id或者数目
                             */
                            $this->data=array('id'=>$result);
                        }

                    }
                } else {
                    $this->code=-1;
                    $this->msg="not exists this method";
                }

            } else {
                $this->code=-1;
                $this->msg="invalide data format";
            }
        } else {
            $this->code=-1;
            $this->msg="invalid object";
        }

        return $this->m();
    }
    protected function createAction()
    {

        $m=$this->getModel();
        if($m){
            $ret=$m->check();

            if ($ret[0]) {
                $ret[1]=$this->filter($ret[1]);

                if(isset($this->exFields[$this->r->action]['ctime'])){
                    $ret[1]['ctime']=time();
                }
                if(isset($this->exFields[$this->r->action]['uid'])){
                    $ret[1]['uid']=$_SESSION['user']['uid'];
                }
                $id=$m->add($ret[1]);
                if ($id>0) {
                    $pid=$m->primary();
                    $this->data=array($pid=>$id);

                } else {
                    $this->code=$this->c['ret']['RecordAddFailed'];
                    $this->msg="add data errors";
                }
            } else {
                $this->data=$this->c['ret']['FieldFormatError'];
                $this->code=-1;
            }

        }
        return $this->m();
    }
    protected function addAction($return='')
    {
        $m=$this->getModel();
        if($m){

            $ret=$m->check();
            $tbMeta=$m->tbMeta();
            if ($ret[0]) {
                $ret[1]=$this->filter($ret[1]);

                if(isset($tbMeta['ctime'])){
                    $ret[1]['ctime']=time();
                }
                if(isset($tbMeta['uid'])){
                    $ret[1]['uid']=intval($_SESSION['user']['uid']);
                }
                $id=$m->add($ret[1]);
                if ($id>0) {
                    $pid=$m->primary();
                    $this->data=array($pid=>$id);
                    $this->data=array_merge($return,$this->data);

                } else {
                    $this->code=$this->c['ret']['RecordAddFailed'];
                    $this->msg="add data errors";
                }
            } else {
                $this->data=$this->c['ret']['FieldFormatError'];
                $this->code=-1;
            }

        }

        return $this->m();

    }
    protected function fetchAction(){
        $m=$this->getModel();
        if($m){
            $data=$m->fetchByUid($_SESSION['user']['uid']);
            exit(json_encode($data));
        }
        return $this->m();

    }

    protected function deleteAction()
    {
        $m=$this->getModel();
        if($m){
            $primary=$m->primary();

            if(!isset($_REQUEST[$primary])){
                $_REQUEST[$primary]=$this->r->getParam($primary);
            }
            $ret=$m->check();
            if ($ret[0]===true) {
                $arr=array();
                $pid=$m->primary();
                $arr[$primary]=$ret[1][$pid];
                $arr['uid']=$_SESSION['user']['uid'];
                $id=$m->delete($arr);
                if ($id!==0) {

                } else {
                    $this->code=$this->c['ret']['RecordDeleteFailed'];
                    $this->msg="delete data errors";
                }
            } else {
                $this->data=$this->c['ret']['FieldFormatError'];
                $this->code=-1;
            }

        }

        return $this->m();
    }

    protected function updateAction()
    {
        //update by uid and id
        $m=$this->getModel();
        if($m){
            $ret=$m->check();
            $primary=$m->primary();
            if ($ret[0]===true) {

                $primaryId=$ret[1][$primary];
                unset($ret[1][$primary],$ret[1]['ctoken']);

                $id=$m->update($ret[1], array($primary=>$primaryId,'uid'=>$_SESSION['user']['uid']));
                if ($id>0) {
                    $this->data=array("id"=>$id);

                } else {
                    $this->code=$this->c['ret']['RecordUpdateFailed'];
                    $this->msg="update data errors";
                }
            } else {
                $this->data=$this->c['ret']['FieldFormatError'];
                $this->code=-1;
            }
        }
        return $this->m();
    }
    function routerUser(){
        return array(sha1($_SESSION['user']['username'].$this->c->site->key.$_SESSION['user']['rpwd']),$_SESSION['user']['rpwd']);

    }
    function getDb($model){

        if(!empty($model::$_db)){
            return $model::$_db;
        } else {
            if(strpos($model,"MModel")!==false){
                $searchDb=$this->c->mongo->defdb;
                $tb=$model::$_tbName;
                $pre=$this->c->mongo->$searchDb->r[1]->pre;
            } else {
                $searchDb=$this->c->database->defdb;
                $tb=$model::$_tbName;
                $pre=$this->c->database->$searchDb->pre;
            }
        }
        return array($searchDb,$tb,$pre);
    }
    function getTbInfo($className){

        if(strpos($className,"MModel")!==false){
            $searchDb=$this->c->mongo->defdb;
            $tb=$className::$_tbName;
            $pre=$this->c->mongo->$searchDb->pre;
        } else {
            $searchDb=$this->c->database->defdb;
            $tb=$className::$_tbName;
            $pre=$this->c->database->$searchDb->pre;
        }
        return array($searchDb,$tb,$pre);
    }
    function getDuelAuthKey($prefix,$module,$controller,$action){
        return implode(".",[$prefix,'duel',$module,$controller,$action]);
    }
    function getDuelProve($prefix,$module,$controller,$action){
        $this->redis();

        $authActionKey=$this->getDuelAuthKey($prefix,$module,$controller,$action);

        $result=$_SESSION['duel_auth'][$authActionKey];

        if($result===1){
            return true;
        }
        return false;

    }

    function actionTxt($prefix,$module,$controller,$action){
        /**
         * 考虑到多个站点公用一个redis,实际上应该分开部署
         */

        $actionTxt=$this->translate($this->langType,strtolower(implode("#",[$prefix,$module,$controller,$action])));
        return $actionTxt;
    }
    function setDuelProve($authType,$toUser,$prefix,$module,$controller,$action){
        $this->redis();

        $authActionKey=$this->getDuelAuthKey($prefix,$module,$controller,$action);
        $verifyCode=Misc_Utils::gen_appid(6);
        $_SESSION['duel_auth'][$authActionKey]=$verifyCode;
        $actionTxt=$this->actionTxt($prefix,$module,$controller,$action);
        if($authType=='email'){
            $this->sendDuelProveByEmail($verifyCode,$actionTxt,$toUser);
        } else if($authType=='mobile'){

            $this->sendDuelProveByMobile($verifyCode,$actionTxt,$toUser);
        }

    }

    function getMailMessage($email,$tempType,$tempStr,$repArr)
    {

        $siteUrl = 'https://' . $_SERVER["HTTP_HOST"];
        $requestCode = $repArr['requestCode'];
        $actionToken = $repArr['actionToken'];
        unset($repArr['requestCode']);
        $default = array(
            'siteName' => $this->translate($this->langType, $this->c['site']['name']),
            'siteLink' => $this->c['site']['link'],
            'yourEmail' => $email,
            'companyName' => $this->translate($this->langType, $this->c['site']['company']),
            'requestCode' => $requestCode,
        );
        $repArr = array_merge($default, $repArr);
        $repArr['actionLink'] = $siteUrl . "/landing/index/?actionToken={$actionToken}&cb={$tempType}&requestCode=" . $requestCode;
        $repArr['reportLink'] = $siteUrl . "/landing/index/?actionToken={$actionToken}&cb={$tempType}&requestCode=" . $requestCode;
        $mail = explode("@", $email);
        $domain = $mail[1];
        $message = "";
        $fromKeys = array_map(function ($key) {
            return "{{" . $key . "}}";
        }, array_keys($repArr));
        if (!empty($tempStr)) {
            $message = str_replace($fromKeys, array_values($repArr), $tempStr);
        }
        return $message;
    }
    function mailTitle($tempStr,$repArr){
        return $this->createTemp($tempStr,$repArr);
    }

    function createTemp($tempStr,$repArr){
        $fromKeys=array_map(function($key){
            return "{{".$key."}}";
        },array_keys($repArr));
        if(!empty($tempStr))
        {
            $message=str_replace($fromKeys,array_values($repArr),$tempStr);
        }
        return $message;
    }

    function mailTemp($tempType,$locale="enus"){
        $this->redis();
        $key="tmp.".$tempType.".".$locale;
        $temp=$this->redis->getItem($key);
        if(true || empty($temp)){
            $email=new EmailTempModel();
            $temp=$email->getOne($tempType,$locale);
            $this->redis->set($key,$temp);
        }
        return $temp;
    }
    function sendDuelProveByEmail($verifyCode,$action,$email){
        $mailTemp=$this->mailTemp('duel_auth',$this->langType);
        $mailMsg=$this->getMailMessage($email,'duel_auth',$mailTemp['content'],[
            'email'=>$email,'requestCode'=>$verifyCode
        ]);
        $mailTitle=$this->mailTitle($mailTemp['title'],array('siteName'=>$this->c['site']['name']));

        Misc_Utils::sendMail($email,$mailTitle,$mailMsg,'','',$this->c->mail->sendmethod);

    }
    function sendDuelProveByMobile($verifyCode,$action,$mobile){
        $content=str_replace(['siteName','requestCode','action'],[$this->translate($this->langType,$this->c->site->name),$verifyCode,$action],$this->lang['temp_mobile_duel_auth']);
        Misc_Utils::sendSms($mobile,$content);

    }

    function authData($authType){

        if(is_string($authType)){
            $authType=explode("#",$authType);
        }
        $authData=array_intersect_key($_SESSION['user'],array_fill_keys($authType,1));
        return array_filter($authData,function($item){
            if(!empty($item)) return $item;
        });

    }

    function duelAuth(){
        if(!empty(($this->_exDuelAuthActions))){
            $exActions=array_keys($this->_exDuelAuthActions);
            if ( (in_array($this->r->action, $exActions) && $this->_isDuelAuth===false)
                || (!in_array($this->r->action, $exActions) && $this->_isDuelAuth===true)) {

                $hasProve=$this->getDuelProve($this->redis->site->name,$this->r->module,$this->r->controller,$this->r->action);
                if($hasProve===false){
                    $authData=$this->authData($this->_exDuelAuthActions[$this->r->action]);
                    if(!empty($authData)){
                        $this->isAjax=true;
                        $this->code=$this->c->ret->DuelAuth;
                        /**
                         * 返回的二次验证类型
                         */
                        $this->data=['authType'=>$this->_exDuelAuthActions[$this->r->action],'authData'=>$authData];
                        return $this->m();
                    }

                }

            }
        }
    }
    function __destruct(){

    }
    protected function init()
    {


        /**
         * for develop testing purpose
         * @todo 关不用户自身的保存在session中，通用的保存在memcached中公用
         */
        session_start();

        //unset($_SESSION['user']['rights']);

        unset($_SESSION['site']['menus']);

        // var_dump($_SESSION['user']);exit;
        //unset($_SESSION['site']['menus']['vmenus'],$_SESSION['site']['menus']['hmenus']);
        $this->initHeader();
        $this->initEnv();

        $this->initCache();

        //  $this->basicAuth();
        $this->loginAuth();

        $this->duelAuth();
        $this->initUserData();
        $this->initWebview();
    //    var_dump($_SESSION['user']);



    }
}