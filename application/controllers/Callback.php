<?php
/**
 * Class CallbackController
 * 连接各大网站开放平台的callback
 */
use \SaeTOAuthV2;
use \Nowcn\Sms;
use \OpenId\Twitter\Twitter;
use \OpenId\Twitter\TwitterClient;
use \Facebook\FacebookSession;
use \Facebook\FacebookRequest;
use \Facebook\FacebookRedirectLoginHelper;
use \Facebook\GraphObject;
use \Facebook\GraphUser;


use \Ali\Pay\Notify;

class CallbackController extends ApplicationController
{

    protected $provider;
    public function getCfg($provider)
    {

        $openIdCfg = $this->c['openid']->toArray();
        if (!isset($openIdCfg[$provider])) {
            $this->redirect("/error/e404");
            return false;
        }


        $openId = new OpenidTokenModel();

        return array($openIdCfg[$provider], $openId);
    }

    public function connectAuth($provider)
    {

        list($openIdCfg, $openId) = $this->getCfg($provider);

        $callBackUrl = $this->siteUrl . $openIdCfg['callback'] . "/" . $provider;
        $redirect_url = "/connect/error";
        switch ($provider) {
            case 'weibo': {
                $auth = new SaeTOAuthV2($openIdCfg['akey'], $openIdCfg['skey']);

                $redirect_url = $auth->getAuthorizeURL($callBackUrl);
                break;


            }
            case 'twitter': {

                $auth = new Twitter($openIdCfg['akey'], $openIdCfg['skey']);
                $temporary_credentials = $auth->getRequestToken($callBackUrl);
                $redirect_url = $auth->getAuthorizeURL($temporary_credentials);
                break;


            }
            case 'facebook': {

                FacebookSession::setDefaultApplication($openIdCfg['akey'], $openIdCfg['skey']);

                $helper = new FacebookRedirectLoginHelper($callBackUrl);

                $scope = array('read_stream', 'publish_actions', 'user_actions.news', 'user_website');
                $redirect_url = $helper->getLoginUrl($scope);

                break;

            }
            case 'default': {
                return '/error/404';
            }


        }
        return $redirect_url;


    }

    public function errorAction()
    {

        $this->v->assign("errMsg", "error request");
        return true;
    }

    public function accountAuth($provider)
    {
        list($openIdCfg, $openId) = $this->getCfg($provider);

        $callBackUrl = $this->siteUrl . $openIdCfg['callback'] . "/" . $provider;
        $token = '';
        switch ($provider) {

            case 'weibo': {

                $oauth = new SaeTOAuthV2($openIdCfg['akey'], $openIdCfg['skey']);

                if (isset($_REQUEST['code'])) {
                    $keys = array();
                    $keys['code'] = $_REQUEST['code'];
                    $keys['redirect_uri'] = $this->siteUrl . $openIdCfg['callback'] . "/$provider";
                    try {
                        $token = $oauth->getAccessToken('code', $keys);

                    } catch (OAuthException $e) {

                    }
                }

                if ($token) {
                    $_SESSION["token"][$provider] = $token;
                    setcookie('weibojs_' . $oauth->client_id, http_build_query($token));


                    $client = new SaeTClientV2($openIdCfg['akey'], $openIdCfg['skey'], $token[$openIdCfg['atokenName']]);

                    $userinfo = $client->show_user_by_id($token[$openIdCfg['uid']]);

                    $ret = $openId->replace(array('sns_id' => $userinfo['id'], 'screen_name' => $userinfo['screen_name'], 'profile_image_url' => $userinfo['profile_image_url'], 'expires_in' => $token[$openIdCfg['expires_in']], 'uid' => $_SESSION['user']['uid'], 'atoken' => $token[$openIdCfg['atokenName']], 'atoken_secret' => 'unknown', 'provider' => $provider, 'atime' => time()));

                    $userinfo['ret_id'] = $ret;
                    return $userinfo;
                }
                break;

            }
            case 'twitter': {


                $auth = new Twitter($openIdCfg['akey'], $openIdCfg['skey']);
                if ($_GET['oauth_verifier']) {

                    $token = $auth->getAccessToken($_GET['oauth_token'], $_GET['oauth_verifier']);


                }
                if (is_array($token)) {
                    $_SESSION["token"][$provider] = $token;


                    $client = new TwitterClient(array_merge(array('output_format' => 'array', "consumer_key" => $openIdCfg['akey'], "consumer_secret" => $openIdCfg['skey']), array('oauth_token' => $token['oauth_token'], 'oauth_token_secret' => $token['oauth_token_secret'])));

                    $userinfo = $client->get("users/show", array('user_id' => $token['user_id']));
                    $ret = $openId->replace(array('sns_id' => $userinfo['id'], 'screen_name' => $userinfo['name'], 'profile_image_url' => $userinfo['profile_image_url'], 'expires_in' => 86400 * 7, 'uid' => $_SESSION['user']['uid'], 'atoken' => $token[$openIdCfg['atokenName']], 'atoken_secret' => $token[$openIdCfg['atokenSecretName']], 'provider' => $provider, 'atime' => time()));

                    $userinfo['ret_id'] = $ret;
                    $homeTimeline = $client->get("statuses/home_timeline");

                    return $userinfo;
                }

                break;
            }
            case 'facebook': {
                FacebookSession::setDefaultApplication($openIdCfg['akey'], $openIdCfg['skey']);

                $helper = new FacebookRedirectLoginHelper($callBackUrl);
                try {
                    $session = $helper->getSessionFromRedirect();
                } catch (FacebookRequestException $ex) {
                    // When Facebook returns an error
                } catch (\Exception $ex) {
                    // When validation fails or other local issues
                }
                if ($session) {
                    $userinfo = (new FacebookRequest(
                        $session, 'GET', '/me'
                    ))->execute()->getGraphObject()->asArray();

//                    $postFeeds = (new FacebookRequest(
//                        $session,
//                        'GET',
//                        '/me/home'
//                    ))->execute()->getGraphObject()->asArray();

                }

                $userinfo['profile_image_url'] = "//graph.facebook.com/{$userinfo['id']}/picture";

                $ret = $openId->replace(array('sns_id' => $userinfo['id'], 'screen_name' => $userinfo['name'],
                    'profile_image_url' => $userinfo['profile_image_url'], 'expires_in' => 86400 * 7, 'uid' => $_SESSION['user']['uid'],
                    'atoken' => $session->getToken(), 'provider' => $provider, 'atime' => time()));

                $userinfo['ret_id'] = $ret;
                return $userinfo;
            }

        }


    }

    function aliPayAction()
    {


        $config = $this->c->pay->ali->toArray();

        $return = $this->r->getParam('return');


        //is return url,redirect from user browser after user paid in alipay site
        if (isset($return)) {
            if (Misc_Utils::isLocal()) {
                $alipayNotify = new Notify($config);
                $verify_result = $alipayNotify->verifyReturn();
                if ($verify_result) {
                    $this->log->info("get verified:" . $return, $_REQUEST);
                    //参数范例 return 返回的参数
//                {
//                                    "body": "App description",
//                        "buyer_email": "ifa6@qq.com",
//                        "buyer_id": "2088102121975549",
//                        "discount": "0.00",
//                        "gmt_create": "2015-02-13 10:17:06",
//                        "gmt_logistics_modify": "2015-02-13 10:17:06",
//                        "gmt_payment": "2015-02-13 10:17:31",
//                        "is_success": "T",
//                        "is_total_fee_adjust": "N",
//                        "logistics_fee": "0.00",
//                        "logistics_payment": "SELLER_PAY",
//                        "logistics_type": "EXPRESS",
//                        "notify_id": "RqPnCoPT3K9%2Fvwbh3InTufne%2FQ0BZToFzCpj3KVgBYo6NbwCsHiM59qOWwgHLn0ilcCa",
//                        "notify_time": "2015-02-13 10:17:34",
//                        "notify_type": "trade_status_sync",
//                        "out_trade_no": "19795384051513",
//                        "payment_type": "1",
//                        "price": "0.01",
//                        "quantity": "1",
//                        "receive_address": "北京锐亚科技有限公司",
//                        "receive_name": "Roosevelt",
//                        "seller_actions": "SEND_GOODS",
//                        "seller_email": "9.18@163.com",
//                        "seller_id": "2088002074403206",
//                        "subject": "cube",
//                        "total_fee": "0.01",
//                        "trade_no": "2015021300001000540047277423",
//                        "trade_status": "WAIT_SELLER_SEND_GOODS",
//                        "use_coupon": "N",
//                        "sign": "8128cd2de2076f86de41403e7395716c",
//                        "sign_type": "MD5"
//    }
                    //验证成功
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //请在这里加上商户的业务逻辑程序代码

                    //——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
                    //获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表

                    //商户订单号

                    $out_trade_no = $_GET['out_trade_no'];

                    //支付宝交易号

                    $trade_no = $_GET['trade_no'];

                    //交易状态
                    $trade_status = $_GET['trade_status'];


                    if ($_GET['trade_status'] == 'TRADE_FINISHED' || $_GET['trade_status'] == 'TRADE_SUCCESS') {
                        $orderData = $this->updateOrder();
                        $this->log->info("order uid:", $orderData);
                        $this->addProduct($orderData);

                        $orderData['_notify_type'] = 'order_paid';
                        if ($orderData['item_type'] == 'app') {
                            $this->installApp($orderData);
                        }
                        $this->sendNotify($orderData);
                        //判断该笔订单是否在商户网站中已经做过处理
                        //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                        //如果有做过处理，不执行商户的业务程序

                    } else if ($_GET['trade_status'] == 'WAIT_SELLER_SEND_GOODS') {
                        //for debug purpose
                        if (Misc_Utils::isLocal()) {

                            $orderData = $this->updateOrder();

                            $this->addProduct($orderData);

                            $orderData['_notify_type'] = 'order_paid';
                            $this->sendNotify($orderData);
                        }

                        //判断该笔订单是否在商户网站中已经做过处理
                        //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                        //如果有做过处理，不执行商户的业务程序
                    } else {
                        echo "trade_status=" . $_GET['trade_status'];
                    }

                    echo "验证成功<br /><script>setTimeout(function(){window.close();},2000);</script>";
                    echo "trade_no=" . $trade_no;

                    //——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                } else {
                    $this->log->warn('ali sync verify failed');
                }
            }


        } else {
            //收到服务器异步通知
            $alipayNotify = new Notify($config);
            $verify_result = $alipayNotify->verifyNotify();

            if ($verify_result) {//验证成功
                $this->log->info("ali verified:", $_POST);
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //请在这里加上商户的业务逻辑程序代


                //——请根据您的业务逻辑来编写程序（以下代码仅作参考）——

                //获取支付宝的通知返回参数，可参考技术文档中服务器异步通知参数列表

                //商户订单号

                $out_trade_no = $_POST['out_trade_no'];

                //支付宝交易号

                $trade_no = $_POST['trade_no'];

                //交易状态
                $trade_status = $_POST['trade_status'];
                if ($trade_status == 'TRADE_FINISHED' || $trade_status == 'TRADE_SUCCESS') {


                    $orderData = $this->updateOrder();
                    $this->log->info("order uid:", $orderData);
                    $this->addProduct($orderData);

                    $orderData['_notify_type'] = 'order_paid';
                    if ($orderData['type'] == 'plugin') {
                        $this->installApp($orderData);
                    }
                    $this->sendNotify($orderData);
                    //判断该笔订单是否在商户网站中已经做过处理
                    //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                    //如果有做过处理，不执行商户的业务程序

                } else if ($_POST['trade_status'] == 'WAIT_BUYER_PAY') {
                    //该判断表示买家已在支付宝交易管理中产生了交易记录，但没有付款

                    //判断该笔订单是否在商户网站中已经做过处理
                    //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                    //如果有做过处理，不执行商户的业务程序

                    echo "success";        //请不要修改或删除

                    //调试用，写文本函数记录程序运行情况是否正常
                    //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
                } else if ($_POST['trade_status'] == 'WAIT_SELLER_SEND_GOODS') {
                    //该判断表示买家已在支付宝交易管理中产生了交易记录且付款成功，但卖家没有发货

                    //判断该笔订单是否在商户网站中已经做过处理
                    //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                    //如果有做过处理，不执行商户的业务程序
//
//                    $orderData=$this->updateOrder();
//                    $this->log->info("order uid:",$orderData);
//                    $this->addProduct($orderData);
//
//                    $orderData['_notify_type']='order_paid';
//                    $this->sendNotify($orderData);
                    //后台添加app信息


                    echo "success";        //请不要修改或删除

                    //调试用，写文本函数记录程序运行情况是否正常
                    //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
                } else if ($_POST['trade_status'] == 'WAIT_BUYER_CONFIRM_GOODS') {
                    //该判断表示卖家已经发了货，但买家还没有做确认收货的操作
                    $this->updateOrderStatus();
                    //判断该笔订单是否在商户网站中已经做过处理
                    //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                    //如果有做过处理，不执行商户的业务程序

                    echo "success";        //请不要修改或删除

                    //调试用，写文本函数记录程序运行情况是否正常
                    //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
                } else {
                    //其他状态判断
                    echo "success";

                    //调试用，写文本函数记录程序运行情况是否正常
                    //logResult ("这里写入想要调试的代码变量值，或其他运行的结果记录");
                }

                //——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            } else {

                $this->log->warn('alipay server verify async failed');

            }

        }


    }

    function updateOrderStatus()
    {

        $order = new OrderMModel();
        $orderData = Misc_Utils::array_pluck($_REQUEST, array("trade_no", "trade_status"));
        unset($orderData['trade_no']);
        $order->update(array('$set' => $orderData), array('trade_no' => $_REQUEST['out_trade_no']));

        //后台添加app信息

    }

    function updateOrder()
    {

        $order = new OrderMModel();
        $orderData = Misc_Utils::array_pluck($_REQUEST, array("trade_no", "buyer_email", "buyer_id", "trade_status", "use_coupon"));
        $orderData['ali_trade_no'] = $orderData['trade_no'];
        unset($orderData['trade_no']);

        $order->update(array('$set' => $orderData), array('trade_no' => $_REQUEST['out_trade_no']));


        $orderData = $order->findOne(array('trade_no' => $_REQUEST['out_trade_no']), array("item_id", "item_type", "uid", "subject"));

        return $orderData;
        //后台添加app信息

    }

    function addProduct($orderData)
    {
        if (in_array($orderData['item_type'], array('app', 'pod'), true)) {

            $modelName = 'My' . $this->classFromTb($orderData['item_type'], true);
            $model = new $modelName();
            $model->update(array('ids' => $orderData['item_id']), array('uid' => intval($orderData['uid'])));

        }

    }

    function sendNotify($data)
    {

        $this->redis->publish('notify', json_encode(
            $data));
    }

    function indexAction()
    {


        $provider = basename($_SERVER['PHP_SELF']);

        $userinfo = $this->accountAuth($provider);

        $this->v->assign("openId", json_encode($userinfo));

        /**
         * @todo post message to frontend to refresh data,use event trigger or html5 post message methods to post message between windows
         */

        return true;

    }

    public function weiboAction()
    {


        $tokenModel = new OpenidTokenModel();
        $openIdCfg = $this->c['openid']->toArray();
        $openIdCfg = $openIdCfg['weibo'];
//        $token=$tokenModel->findOne(array('uid'=>$_SESSION['user']['uid'],'provider'=>$_GET['openid']));

        $token = $_SESSION['token']['weibo'];

        $client = new SaeTClientV2($openIdCfg['akey'], $openIdCfg['skey'], $token[$openIdCfg['atokenName']]);

        $method = $_REQUEST['m'];
        $data = json_decode($_REQUEST['data']);

        if (method_exists($client, $method)) {

            $ret = call_user_func_array(array($client, $method), empty($data) ? array() : $data);
            echo json_encode($ret);
        }
        return false;

    }

    public function twitterAction()
    {

        $provider = "twitter";
        list($openIdCfg, $openId) = $this->getCfg($provider);
        $method = $_REQUEST['m'];
        $type = ($_REQUEST['t'] == 'POST') ? 'post' : 'get';

        $data = json_decode($_REQUEST['data']);
        $param = array($method, $data);
        $token = $_SESSION['token'][$provider];
        $config = array(
            'consumer_key' => $openIdCfg['akey'],
            'consumer_secret' => $openIdCfg['skey'],
            'oauth_token' => $token[$openIdCfg['atokenName']],
            'oauth_token_secret' => $token[$openIdCfg['atokenSecretName']],
            'output_format' => 'array'
        );
        $client = new TwitterClient($config);
        if (method_exists($client, $type)) {

            $ret = call_user_func_array(array($client, $type), empty($param) ? array() : $param);
            echo json_encode($ret);
        }
        return false;

    }

    public function facebookAction()
    {

        $provider = "facebook";
        list($openIdCfg, $openId) = $this->getCfg($provider);
        $method = $_REQUEST['m'];
        $type = ($_REQUEST['t'] == 'POST') ? 'POST' : 'GET';
        $data = json_decode($_REQUEST['data']);
        $token = $_SESSION['token'][$provider];
        FacebookSession::setDefaultApplication($openIdCfg['akey'], $openIdCfg['skey']);

        $session = new FacebookSession($token[$openIdCfg['atokenName']]);
        if ($session) {
            $req = new FacebookRequest($session, $type, $method, empty($data) ? array() : $data);
            $ret = $req->execute()->getGraphObject()->asArray();

            $this->data = json_encode($ret);
        } else {
            $this->code = "21000";
            $this->msg = "invalide access_token";
        }
        return $this->m();


    }

    public function connectAction()
    {

        $provider = $_GET['provider'];

        $code_url = $this->connectAuth($provider);

        $this->redirect($code_url);
        return false;
        exit;

    }

    public function init()
    {

        define("WC_HELP", str_replace("\\n", "\n", $this->lang['wc_help']));


        session_start();

        $this->ver = time();
        $this->r = $this->getRequest();

        $uri = $this->r->getRequestUri();
        $this->v = $this->getView();
        $this->c = Yaf\Application::app()->getConfig();


        $this->img = $this->c->assets->dir->img;
        $this->css = $this->c->assets->dir->css;
        $this->js = $this->c->assets->dir->js;
        $this->fonts = $this->c->assets->dir->fonts;
        $uriReg = "@^/([^/]+.json|([^/]+/)+[^/]+.json)@";
        $module = strtolower($this->r->controller . "/" . $this->r->action);

        $this->provider=empty($_GET['_n'])?'ninja':$_GET['_n'];
        $this->credential = $this->c->openid;

        $this->lang = Lang_Lang::getLang();

        $this->ch1 = Yaf\Registry::get("cache");
        $this->ver = time();

        $this->log = Yaf\Registry::get("logger");
        $this->e = Yaf\Registry::get('env');
        $this->siteUrl = $this->e['siteUrl'];
        $this->sockUrl = $this->e['sockUrl'];

        $this->redis();
        if (!isset($_SESSION['ctoken'])) {
            $_SESSION['ctoken'] = Misc_Utils::gen_csrf_token();

        }
        $this->v->assign(

            array(

                "userJson" => empty($_SESSION['user']) ? false : json_encode($_SESSION['user'])
            ));
        if (preg_match($uriReg, $uri)) {
            Misc_Utils::z_header('json');
        }

    }


    function checkSign()
    {
        $credential = $this->credential->wechat->{$this->provider};

        if(empty($credential)){
            return false;
        }
        $token=$credential->token;
        $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];


        $tmpArr = array($token, $timestamp, $nonce);
        sort($tmpArr, SORT_STRING);
        $tmpStr = implode($tmpArr);
        $tmpStr = sha1($tmpStr);

        if ($tmpStr == $signature) {

            return true;
        } else {

            return false;
        }
    }

    function fogpodAppAction()
    {
        $this->log->info("installed app", $_GET);
    }

    function installApp($orderData)
    {
        $resp_url = $this->c->router->app->resp_url;
        $download_url = $this->c->router->app->download_url . $orderData['subject'] . "/";
        $fg = new FogpodApi(FogpodApi::ip(), 'mac', $this->c->router->toArray(), '', '', false);
        $router = new RouterMModel();
        $routerData = $router->findOne(array('uid' => intval($orderData['uid'])), array('api_user', 'password'));
        $apiUser = 'admin'; //$routerData['api_user']
        $password = '40bd001563085fc35165329ea1ff5c5ecbdbbeef';//$routerData['password']
        $fg->install_app($apiUser, $password, $orderData['item_id'], $orderData['subject'], $download_url, $resp_url);
    }

    function getAdvData($data,$provider="ninja")
    {
        /**
         * get advs by user info and user pos
         */
        $newsData = ["ninja"=>array(
            array(
                array('Title' => '家居服务',
                    'Description' => '点击下方“感应器”获取当前空气指数',
                    'PicUrl' => 'http://air.gogoinfo.cn/img/air/wechat_adv_home.jpg?v=1',
                    'Url' => 'http://www.ryatek.com/'
                ),
                array('Title' => '宜家家居商品促销',
                    'Description' => '点击下方“感应器”获取当前空气指数',
                    'PicUrl' => 'http://air.gogoinfo.cn/img/air/wechat_adv_home_1.jpg',
                    'Url' => 'http://www.ryatek.com/'
                )
            ),
            array(
                array('Title' => '清新世界  惬意人生',
                    'Description' => '扫描微信二维码，关注锐亚环境卫士公众号，点击下方菜单【环境信息】获取当前空气指数，让你一手掌握各大城市空气质量，随时监测PM2.5等各大数值。',
                    'PicUrl' => 'http://air.gogoinfo.cn/img/air/wechat_adv_mall.jpg?v=3',
                    'Url' => 'http://www.ryatek.cn/'
                )


            )
        ),
        "peach"=>array(
            array(
                array('Title' => '小蜜桃餐厅官网',
                    'Description' => '点击访问小蜜桃餐厅,进行点单,参加活动等',
                    'PicUrl' => 'http://ninja.gogoinfo.cn/templates/dinning/1/res/assets/imgs/main.png',
                    'Url' => 'http://ninja.gogoinfo.cn/templates/dinning/1/res/index.html?sid=55dbc63300bb6374e81f53f2'
                )
            ),
            array(
                array('Title' => '小蜜桃餐厅官网',
                    'Description' => '点击访问小蜜桃餐厅,进行点单,参加活动等',
                    'PicUrl' => 'http://ninja.gogoinfo.cn/templates/dinning/1/res/assets/imgs/main.png',
                    'Url' => 'http://ninja.gogoinfo.cn/templates/dinning/1/res/index.html?sid=55dbc63300bb6374e81f53f2'
                )

            )
        )

        ];
        $idx = array_rand($newsData[$provider]);
        return $newsData[$provider][$idx];
    }

    function wechat_officialAction()
    {

        ob_end_clean();
        $this->log->error("post_data",$_SERVER);


        $timeStamp=$_GET['timestamp'];
        $nonce=$_GET['nonce'];

        $params = $this->r->getParams();
        $officialId = key($params);

        $options = $this->c->openid->wechat_open->gogo->toArray();
        $redis=$this->redis();



        $wo = new WechatOfficial($options,$redis);

        $user=new WechatOfficialUserMModel();



        $this->log->error("decode:raw msg", array($wo->rawMsg()));

        $msg=$wo->decMsg();

        $this->log->error("decode:msg", array($msg));

        if(isset($msg['ComponentVerifyTicket'])) {
            $redis=$this->redis();
            $wo->setVerifyTicket($msg['ComponentVerifyTicket']);
            /**
             * 微信每10分钟更新一次ticket
             */

            $this->log->error("ticket", $msg);
        }
        if(!empty($officialId)){

                if($officialId=='wx570bc396a51b8ff8'){
                    $this->wechatPublishTest();
                }

//            $wc = new Wechat(array_merge($this->credential->wechat->toArray(), array('logcallback' => array($this->log, 'info'), 'debug' => false)));
//
//            $ret = $wc->valid();
            $this->log->error("valid",[$_SERVER,$_REQUEST]);
            $token=new WechatOfficialInfoMModel();
            $token=$token->findOne(array("authorizer_appid"=>$officialId));
            $userInfo=$wo->wechatIp($token['authorization_info']['authorizer_access_token']);
            $this->log->error("user info",[$userInfo,$token['authorization_info']['authorizer_access_token']]);

        }
        exit('success');



    }
    function wechatPublishTest(){

        exit();

    }

    public function wechatAction()
    {
        /**
         * 处理微信的调用请求
         */
        // $this->log->debug("Wechat Get",$_GET);
        if (!empty($_GET['signature']) && !empty($_GET['timestamp']) && !empty($_GET['nonce']) && !empty($_GET['echostr'])) {

            if ($this->checkSign()) {
                ob_end_clean();
                exit($_GET['echostr']);
                return false;
            } else {
                $this->code = $this->c['WeChatTokenValidError'];
                $this->msg = "error wechat valid token";
                return $this->m();

            }

        }


        $wcUser = new WechatUserMModel();

        if(empty($this->credential->wechat->{$this->provider})){
            return false;
        }
        $credential=$this->credential->wechat->{$this->provider}->toArray();

        $this->log->error("credential",$credential);
        $wc = new Wechat(array_merge($credential, array('logcallback' => array($this->log, 'info'), 'debug' => false)));


        $ret = $wc->valid();



        if ($ret) {
            /**
             * must init get rev data
             */

            $wc->getRev();
            $data = $wc->getRevData();

            $type = $wc->getRevType();

            $toUsername=$wc->getRevTo();
            $from = $wc->getRevFrom();
            $this->log->error("recdata", $data);


            $userInfo = $wc->getUserInfo($from);
            $this->log->error('userinfo', $userInfo);

            switch ($type) {

                case 'text': {

                    $content = $wc->getRevContent();
                    $cmd = new WechatCmd($content, $wc, 'text');
                    $cmd->execCmd();

                    break;
                }
                case 'image': {

                    break;
                }
                case 'event': {

                    $eventArr = $wc->getRevEvent();
                    $event = strtolower($eventArr['event']);
                    if ($event == 'click') {

                        //$wc->sendText($from, print_r($eventArr, true));
                        break;
                    } else if ($event == 'view') {

                        //$wc->sendText($from, print_r($eventArr, true));
                        break;
                    } else if ($event == 'subscribe') {
                        try {


                            /*                            Array
                                                        (
                                                            [subscribe] =>
                                                                [openid] => ovgWqtwbGWsbw9vrAj8qcFr4S8QQ
                                                                [nickname] => Bing
                                                                [sex] => 1
                                                                [language] => en
                                                                [city] => 黄浦
                                                                [province] => 上海
                                                                [country] => 中国
                                                                [headimgurl] => http://wx.qlogo.cn/mmopen/Xg4Fl7h9iaqCibqf4RJvTA3g1eeoJFEf0gNGz1dwZ1ic0wSECBbmpurZTUoCcZRhQBReFg0ycU4cpTsrP0bibpNEs0BZ0QJY9BsW/0
                                                                [subscribe_time] => 1392895300
                                                                [uid] =>
                                                    )*/

                            $weChatModel = array('subscribe' => '', 'openid' => '', 'nickname' => '',
                                'language' => '', 'city' => '', 'province' => '', 'country' => '',
                                'headimgurl' => '', 'subscribe_time' => '', 'remark' => '', 'uid' => ''
                            );

                            $wm = new WechatMenuModel();

                            $menus = $wm->getWechatMenus($this->lang,$toUsername,$this->provider);
                            $this->log->error('menus', $menus);
                            $arr = array('button' => array());
                            $userInfo = array_intersect_key($userInfo, $weChatModel);
                            $result = array();
                            $openIdData = array(
                                'provider' => 'wechat'

                            );
                            if (!empty($sceneId)) {
                                $sceneId = $wc->getRevSceneId();
                                $result = $this->getScene($sceneId);
                            }

                            if (empty($result)) {
                                $result = array();
                                $userInfo['uid'] = 0;
                                $result['openid'] = $userInfo['openid'];
                                $result['realname'] = $userInfo['nickname'];


                            } else {

                                $openId = new OpenidTokenModel();
                                $userInfo['uid'] = $result['uid'];
                                $openIdData['screen_name'] = $result['nickname'];
                                $openIdData['profile_image_url'] = $result['headimgurl'];
                                $openIdData['sns_id'] = $result['openid'];

                                $openId->replace($openIdData);
                                $this->log->error('openId', $openIdData);
                            }


                            $html = $wm->buildMenu(0, $menus, $arr['button'], $userInfo);

                            $result=$wc->createMenu($arr);
                            $this->log->error('buildmenu', [$result]);
                            if ($wc->createMenu($arr)) {
                                $this->log->error('created menus', array($arr, $result));
                            }
//                            array(
//     *  	"0"=>array(
//     *  		'Title'=>'msg title',
//     *  		'Description'=>'summary text',
//     *  		'PicUrl'=>'http://www.domain.com/1.jpg',
//     *  		'Url'=>'http://www.domain.com/1.html'
//                            *  	),
//     *  	"1"=>....


                            $newsData = $this->getAdvData($data,$this->provider);
                            $wc->news($newsData)->reply();


                            $openIdData['openid'] = $result['openid'];
                            $this->triggerEvent($result, '', $openIdData);


                            $wcUser->update2(array('$set' => $userInfo), array('openid' => $userInfo['openid']));


                            //$wc->sendText($from,str_replace("\\n","\n",$this->lang['wc_help']));
                        } catch (Exception $ex) {
                            $this->log->error("subscribe exception", $ex);
                            exit;
                        }
                    } else if ($event == 'unsubscribe') {
                        try {

                            $userInfo['subscribe'] = 0;

                            $wcUser->update2(array('$set' => $userInfo), array('openid' => $userInfo['openid']));

                            $this->triggerEvent(array(''));

                        } catch (Exception $ex) {
                            $this->log->error("unsubscribe exception", $ex);
                            exit;
                        }


                    } else if ($event == 'location') {

                        $loc = new WechatLocMModel();
                        $lat = doubleval($wc->getRevLat());
                        $long = doubleval($wc->getRevLog());
                        $loc->insert(array('pos' => array($lat, $long), 'openid' => $wc->getRevFrom(), 'ctime' => time()));

                        $advData = $this->getAdvData($data);
                        $result = $loc->last();
                        /**
                         * 当与上次距离相差10米的时候推送信息
                         */
                        if (!empty($result['loc'])) {
                            $dist = Misc_Utils::measureDistance(doubleval($result['loc'][0]), doubleval($result['loc'][1]), $lat, $long);

                            $loc=new WechatUserMModel();
                            $data=[];

                            $loc->update($data,[
                                'openid'=> $userInfo['openid']

                            ]);

                            if ($dist >= 100) {
                                $wc->news($advData)->reply();
                            }


                        }


                        break;

                    } else if ($event == 'scan') {


                        try {

                            $sceneId = $wc->getRevSceneId();
                            $result = array();
                            if (!empty($sceneId)) {
                                $result = $this->getScene($sceneId);
                            } else {

                                $result['openid'] = $userInfo['openid'];
                                $result['realname'] = $userInfo['nickname'];
                                $userInfo = $wcUser->findOne(array('openid' => $result['openid']));
                                $result['uid'] = $result['uid'];
                                $this->triggerEvent($result, $wcUser);
                            }


                        } catch (Exception $ex) {
                            $this->log->error("scan exception", $ex);
                            exit;
                        }


                    }

                    break;

                }


            }

        }
        return false;

    }

    function triggerEvent($result, $wcUser = '', $formatData = '')
    {

        if ($result['type'] == 'wechat' && $result['action'] == 'adduser') {
            $this->redis->publish('notify', json_encode($result));
        }
        if ($result['type'] == 'wechat' && $result['action'] == 'link') {

            $this->redis->publish('notify', json_encode($formatData));
        }

        if ($result['type'] == 'wechat' && $result['action'] == 'login') {


            $result['token'] = Misc_Utils::genToken(24);

            if (!empty($result['uid'])) {
                $this->ch1->setItem($result['uid'], $result['token'] . "_" . $result['tempid']);
            }
            $this->log->info("publish");
            $this->redis->publish('notify', json_encode($result));
        }

    }

    function getScene($sceneId)
    {
        $scene = new WechatSceneMModel();


        return $scene->findOne(array("_id" => intval($sceneId)));


    }

    function wechatOpen_refreshToken($cfg, $refreshToken)
    {

        $url = "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid={$cfg['appid']}&grant_type=refresh_token&refresh_token=$refreshToken";
        $z = new ZCurl();
        $ret = $z->getJson($url);
        return ($ret['errcode'] == 0);

    }

    function wechatOpen_userinfo($openid, $access_token)
    {

        $url = "https://api.weixin.qq.com/sns/userinfo?access_token=$access_token&openid=$openid";
        $z = new ZCurl();
        $ret = $z->getJson($url);
        if (isset($ret['errcode'])) {

            return false;
        }
        return $ret;
    }

    function wechatOpen_verifyToken($openid, $access_token)
    {
        $url = "https://api.weixin.qq.com/sns/auth?access_token={$access_token}&openid={$openid}";
        $z = new ZCurl();
        $ret = $z->getJson($url);
        return ($ret['errcode'] == 0);
    }

    function wechatOpenAction()
    {
        $open = $this->credential['wechat_open']['def']->toArray();
        $accessUrl = "https://api.weixin.qq.com/sns/oauth2/access_token?appid={$open['appid']}&secret={$open['appsecret']}&code={$_GET['code']}&grant_type=authorization_code";
        $z = new ZCurl();
        $ret = $z->getJson($accessUrl);
        if (isset($ret['access_token'])) {
            $userinfo = $this->wechatOpen_userinfo($ret['openid'], $ret['access_token']);
            if ($userinfo) {

                $model = new WechatUserMModel();
                $result = $model->findOne(array('openid' => $ret['openid']));

                if (!empty($result)) {
                    $result['type'] = 'wechat';
                    $result['action'] = 'login';
                    $result['tempid'] = $_SESSION['tmpId'];
                    $model->update2(array('$set' => $userinfo), array('openid' => $ret['openid']));

                    $this->triggerEvent($result, $model);

                } else {
                    $result['type'] = 'wechat';
                    $result['action'] = 'login';
                    $result['tempid'] = $_SESSION['tmpId'];
                    $user = new CommonUserModel();
//                    openid	普通用户的标识，对当前开发者帐号唯一
//nickname	普通用户昵称
//sex	普通用户性别，1为男性，2为女性
//province	普通用户个人资料填写的省份
//city	普通用户个人资料填写的城市
//country	国家，如中国为CN
//headimgurl	用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空
//privilege	用户特权信息，json数组，如微信沃卡用户为（chinaunicom）
//unionid	用户统一标识。针对一个微信开放平台帐号下的应用，同一用户的unionid是唯一的。
                    $uid = $user->addWechatUser("wc_" . Misc_Utils::genToken(18), 'ryatek123', '', $userinfo['nickname'], $userinfo['headimgurl']);
                    $userinfo['uid'] = $result['uid'] = $uid;
                    $result['openid'] = $ret['openid'];
                    $profile = new UserProfileModel();
                    if ($uid > 0) {
                        $profile->updateById(array(
                            'gender' => $userinfo['sex'] == 1 ? false : true,
                            'province' => $userinfo['province'],
                            'city' => $userinfo['city'],
                            'province' => $userinfo['province'],
                            'country' => $userinfo['country'],

                        ), $uid);
                        $model->update2(array('$set' => $userinfo), array('openid' => $ret['openid']));

                        $this->triggerEvent($result, $model);
                    }


                }

            }

        }
//        access_token' => string 'OezXcEiiBSKSxW0eoylIeKaYBS9Z3XaF1eiZPzV0Cc129UGn_rD4N3sArsWup0OZnhVkEjOoH7t85mnsv9InQgLW5-jQ0-5Q9mqQWF8rjjYBl5yn7f141VmfIAwMNf1jXQ_VTBJY8Co3ih3DSPzlkw' (length=150)
//  'expires_in' => int 7200
//  'refresh_token' => string 'OezXcEiiBSKSxW0eoylIeKaYBS9Z3XaF1eiZPzV0Cc129UGn_rD4N3sArsWup0OZRXQUl_H5Pq0gOrgrjmHk4ON8TRhdttWTtdLSA8qRMWTbmTbNOimqamyn8bePZyFSOvvYamqKbz_gJovhzP1WSA' (length=150)
//  'openid' => string 'oHj90uF66DdGG-_zxsZRHsQU-u7s' (length=28)
//  'scope' => string 'snsapi_login' (length=12)
//  'unionid' => string 'ok3W4uC0WJWeFCeabMy052Tn8xj8' (l
        exit('<script>window.close();</script>');
    }

    public function wechatQRAction()
    {
        /**
         * 返回微信的二维码地址
         */
        $prefix = '';
        if(!isset($this->credential['wechat'][$this->provider])){
            return false;
        }
        $wc = new Wechat($this->credential['wechat'][$this->provider]);

        if (!empty($_SESSION['user']['uid'])) {

            $_GET['uid'] = $_SESSION['user']['uid'];
        }

        $scene = new WechatSceneMModel();

        $sceneId = $scene->insert2($_GET);

        $qrCode = $wc->getQRCode($sceneId);

        $url = $wc->getQRUrl($qrCode['ticket']);
        ob_end_clean();
        echo $url;
        return false;

    }


}

