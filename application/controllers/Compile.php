<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 3/4/15
 * Time: 11:47 AM
 */

class CompileController extends ApplicationController {

    function init(){

        session_start();
        unset($_SESSION['user']['rights']);
        unset($_SESSION['site']);
        $this->initHeader();
        $this->initEnv();

        $this->initCache();
    }

    function serviceAction()
    {
        $this->redis_sub();
        $this->redis_sub();
        $this->redis_sub->subscribe(array('android_build','ios_build'), function ($redis, $ch, $msg) {

            printf("\n$ch arrived at %s data: %s\n",date("Y-m-d H:i:is"),$msg);
            $data = json_decode($msg, true);

            $func=$ch."M";
            if(method_exists($this,$func)){

                if (!empty($data)) {
                    $this->$func($data,$redis);
                }

            }



        });
    }
    function android_buildM($data,$redis=null,$cnt=8){
        $projDir=$data['dir'];
        $fileName=basename($projDir);
        $log="$projDir/{$data['id']}_log.txt";
        $echoProps="echo 'key.store=/Users/chjade/my/sencha1.keystore' >> $projDir/local.properties;echo 'key.alias=sencha_android1' >> $projDir/local.properties;echo 'key.alias.password=chjade' >> $projDir/local.properties;echo 'key.store.password=chjade' >> $projDir/local.properties";
        $srcFile=$projDir."/src";
        if($cnt>0){
            if(file_exists($srcFile)){
                $result=passthru("/Users/chjade/dev/android/tools/android update project -p $projDir;$echoProps;cd $projDir;ant release|tee $log;mv $projDir/bin/MainActivity-release.apk $projDir/../../../../apks/$fileName.apk");
                //$result=fread($hd,1024*20);
                //fclose($hd);
                echo($result);
                if(isset($data['id'])){
                    echo("data {$data['id']}");
                    $redis2=$this->redis();
                    if($redis2){
                        echo("set key job_{$data['id']}");
                        if(strstr($result,"BUILD FAILED")!==false){
                            $redis2->set("job_{$data['id']}","/apks/$fileName.apk",3600);
                        } else {
                            $result=file_get_contents($log);
                            $redis2->set("job_{$data['id']}",$result,3600);
                        }

                    }

                }
            } else {
                sleep(1);
                $this->android_buildM($data,$redis,$cnt-1);
                echo("$projDir not exists");
            }
        }




    }
    function mail_notifyM($data){
        if(empty($data['from'])){
            $cfg = Yaf\Application::app()->getConfig();
            $data['from']=$cfg->mail->from;

        }
        Misc_Utils::sendDirectMail($data['to'], $data['title'], $data['content'],$data['from']);

    }

    function mail_log_actionM($data){
        Misc_Utils::sendDirectMail($data['to'], $data['title'], $data['content'],$data['from']);

    }
} 