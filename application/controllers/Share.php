<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 4/21/15
 * Time: 6:37 PM
 */
use Nowcn\Sms;
class ShareController extends ApplicationController{

    function indexAction(){

    }
    function smsAction(){
        if($_POST['share_type']=='PhotoM'){
            if($this->isPermitLink($_POST['link'])){
                $msg=$this->buildMsg($_POST['share_type'],$this->lang['share_'+$_POST['share_type']],array('realname'=>$_SESSION['user']['realname'],'link'=>$_POST['link']));
                Misc_Utils::sendSms($_POST['mobile'],$msg,'','',$this->c->mail->sendmethod);
            }   else {
                $this->code=-1;
            }
        }
    }
    function emailAction(){

        if($_POST['share_type']=='PhotoM'){
            if($this->isPermitLink($_POST['link'])){
                $msg=$this->buildMsg($_POST['share_type'],$this->lang['share_'+$_POST['share_type']],array('realname'=>$_SESSION['user']['realname'],'link'=>$_POST['link']));
                Misc_Utils::sendMail($_POST['email'],$this->lang['share']." ".$this->lang[$_POST['share_type']],$msg,'',$this->c->mail->sendmethod);

            } else {
                $this->code=-1;
            }

        }
        return $this->m();

    }
    function buildMsg($shareType,$temp,$vars,$lD="{{",$rD="}}"){
        $keys=array_map(function($key) use($lD,$rD){
            return $lD.$key.$rD;
        },array_keys($vars));
        if($shareType=='PhotoM'){
            return str_replace($keys,array_values($vars),$temp);
        }
        return '';

    }
    function isPermitLink($link){
        return (Validator::isReg('http',$link) && Misc_Utils::isSiteRes(explode(",",$this->site->hosts),parse_url($link,PHP_URL_HOST)));



    }

}
