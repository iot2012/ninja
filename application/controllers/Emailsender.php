<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 3/4/15
 * Time: 11:47 AM
 */

class EmailsenderController extends ApplicationController {

    function init(){

        session_start();
        unset($_SESSION['user']['rights']);
        unset($_SESSION['site']);
        $this->initHeader();
        $this->initEnv();

        $this->initCache();
    }

    function serviceAction()
    {
        $this->redis_sub();
        $this->redis_sub();
        $this->redis_sub->subscribe(array('mail_notify','mail_log_action'), function ($redis, $ch, $msg) {


            printf("\n$ch arrived at %s data: %s\n",date("Y-m-d H:i:is"),$msg);
            $data = json_decode($msg, true);
            $func=$ch."M";
            if(method_exists($this,$func)){

                if (!empty($data)) {
                    $this->$func($data);
                }

            }

        });
    }
    function mail_notifyM($data){
        if(empty($data['from'])){
            $cfg = Yaf\Application::app()->getConfig();
            $data['from']=$cfg->mail->from;

        }
        Misc_Utils::sendDirectMail($data['to'], $data['title'], $data['content'],$data['from']);

    }

    function mail_log_actionM($data){
        Misc_Utils::sendDirectMail($data['to'], $data['title'], $data['content'],$data['from']);

    }
} 