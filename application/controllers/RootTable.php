<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 7/23/14
 * Time: 7:25 PM
 */

class RootTableController extends AdminController {


     protected $_root;
     protected $_viewTemp;

     function init(){

        parent::init();

        $this->_root=new RootTable($this);


    }
    function restAction(){

        exit("windows");
        return false;
    }
    public function indexAction(){


        return true;

    }
    public function getTbMeta(){


    }
    public function simpleAction(){


        return true;

    }
    public function emptyAction(){


        return true;

    }
    public function dataAction(){


        return true;

    }


    /**
     * @resource 添加表记录
     * @desc 如果要增加添加表记录,必须添加改权限
     *
     * @return bool
     */
    public function addAction(){
        unset($_POST['selectAllkeyword'],$_POST['selectItemkeyword']);
        $this->_root->create($_POST,$_SESSION['user'],$this->modelName());
        return $this->m();
    }


    /**
     * @resource 更新表记录
     * @desc 如果要添加更新表记录,必须添加改权限
     * @return bool
     */
     function editAction(){
        /**
         * @todo check multi relation id
         */
         unset($_POST['selectAllkeyword'],$_POST['selectItemkeyword']);
        $this->update($_REQUEST,$_SESSION['user'],$this->modelName());


    }


    function importAction(){
        unset($_POST['selectAllkeyword'],$_POST['selectItemkeyword']);

        $model=$this->modelName();

        $this->_root->import($_FILES['file']['tmp_name'],$_SESSION['user'],$model,$_GET['t']);
        return $this->m();

    }

    function error($msg=''){
        $this->code=-1;
        $this->msg=$msg;
        return $this->m();
    }


     function viewAction(){
         $fields=[];
        if(!empty($_GET['_f'])){
            $fields=explode(",",$_GET['_f']);
        }

         parse_str($_GET['q'],$whereCond);

        $this->_root->read($_REQUEST,$_SESSION['user'],$this->modelName(),$whereCond,$fields);
         if($_GET['_s']=='simple'){
             $this->_viewTemp="table/simple_view.phtml";
         }
         $this->m();

    }
    function setZtb($ztb){
        $this->_ztb=$ztb;
    }
    function podAccessAction(){
        $tbName=$this->_root->ztb();
        $_GET['menuid']=18;
        $modelName=$this->_root->setZtb('ztb',$this->classFromTb("pod_".$tbName."_data",''));
        $this->redirect("/admin/table/view?ztb={$this->ztb()}&menuid=18");
        exit;
        return FALSE;
    }
    function recordsRemoveAction()
    {
        $this->_root->delete($_POST,$_SESSION['user']);
        $this->m();

    }
}

/**
 *
 * 1. 增删改查crud操作
 * 2. readonly的列
 * 3. 针对列的属性出现不同的editor
 * 4. 几行代码增加对表的操作
 * 5. 提供元素局接口自定义构建菜单
 * 6. 权限校验归结成表+字段的操作,根据用户定义进行
 * a. 可以建立几个level的mysql用户,从而进行对应的操作,将level权限对应到数据库用户
 * b. 或者建立白名单的表,维护一个用户权限表,启动时候载入
 * 业务逻辑 level 1=>array('order'=>array(
 *                  edit=>array(id=>"1,2,3,4"),
 *                  delete=>
 *                  select=>array()
 *
 *
 *)
 * 对应的model方法
 */