

<?php
/**
 * @name ErrorController
 * @desc 错误控制器, 在发生未捕获的异常时刻被调用
 * @see http://www.php.net/manual/en/yaf-dispatcher.catchexception.php
 * @author chjade
 */


class ErrorController extends Yaf\Controller_Abstract {

	/**
     * 从2.1开始, errorAction支持直接通过参数获取异常
     *
     */
    public $mcode;
    public $mdata;
    public $mmsg;
    public $isApi;
    public $isJsonXml;
    public $r;

    public function errorAction($exception) {
        //var_dump($exception instanceof Yaf\Exception\LoadFailed\Controller,$exception);exit;


    //case YAF_ERR_NOTFOUND_MODULE:
    //case YAF_ERR_NOTFOUND_CONTROLLER:
    //case YAF_ERR_NOTFOUND_ACTION:
    //case YAF_ERR_NOTFOUND_VIEW:

        $strMsg=Misc_Utils::formatException2($exception);
        $pEx=$exception->getPrevious();

        $lang=Lang_Lang::getLang();
        $env=Yaf\Registry::get('env');

        Yaf\Registry::get("logger")->error($strMsg);

        $this->r=$this->getRequest();
        $exMsg=$exception->getMessage();


        $mysqlPat=array('main'=>"/^SQLSTATE\[.+/",'dupkey'=>'/Duplicate entry (.+) for key \'([\w-]+)\'/i');

        if(!empty($pEx) && !is_int($pEx)){
            $pExMsg=$pEx->getMessage();
            if(preg_match($mysqlPat['main'],$pExMsg)){
                $this->mcode=-1;
                $this->mmsg="operation error";
                if(strstr($pExMsg,'Duplicate entry')!==false){

                    if(preg_match($mysqlPat['dupkey'],$pExMsg,$ret)){
                        $this->mmsg=trim($ret[1],'\'"').$lang['e_record_exist'];
                    }

                }

            }


        }

        $this->getView()->assign("exMsg", $exMsg);
        /**
         * disable infinate loop
         */

        if(strstr($exMsg,"No such file or directory")!==false){
            /**
             * 使用redirect,减少流量和攻击
             */
            $this->redirect('/e404.html');
            return true;
        } else {
            /**
             * @todo 做分析记录日志系统
             */
            $this->redirect('/e404.html');
        }


        return !$env['isLocal']?false:var_dump($exMsg);
    }
    protected function m($mcode = '', $mmsg = '', $mdata = '')
    {

        ob_end_clean();

        /**
         * @todo make this support under several  conditions such as ajax request
         */
        $isJSONReq=strstr($this->r->getServer('CONTENT_TYPE'), 'application/json')!==false;
        $isXmlReq=strstr($this->r->getServer('CONTENT_TYPE'), 'text/xml')!==false;
        if ($mcode=='') {
            $mcode=$this->mcode;
        }
        if ($mmsg=='') {
            $mmsg=$this->mmsg;
        }
        if ($mdata=='') {
            $mdata=$this->mdata;
        }


        if ($this->r->method=='POST' ||  $this->isApi || $isJSONReq || $isXmlReq) {
            echo json_encode(array('code' => $mcode, 'msg' => $mmsg, 'data' => $mdata));

        } else {
            if($this->mcode!=1){
                $this->forward('error','error','e404');
                exit;
            }
            $this->v->assign(array('code' => $mcode, 'msg' => $mmsg, 'data' => $mdata));

            return true;

        }
        exit;

    }
    public function e404Action(){

        $this->redirect('/e404.html');
      return true;
    }
}
