<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 12/2/14
 * Time: 7:20 PM
 */

class AdminController extends ApplicationController {

    public $_isLog=true;
    /**
     * 显示的setting内容
     * @var array
     */
    public $_feature=array(
        'setting'=>array('dev'=>false,'general'=>false,'setting'=>array('lab'=>false,'domain'=>false,'extra'=>false,'account-link'=>false)),
        'menu_searchbox'=>false
    );
    function init(){

      parent::init();

        if($this->r->method=='GET' && !($this->e['isAjax'] || $this->e['isJsonReq'])){
            $this->initAdmPage();

            /**
             * 控制前端的顶部ui显示,结合用户自定义的表实现自动配置
             */
            $feature=array(

                'top_message'=>false,
                'top_notify'=>false,
                'top_task'=>false
            );
            /**
             * get router config from data api
             */





            /**
             * @debug
             */



            $splash=array(
                'title'=>$this->title(),
                'content'=>$this->title(),'img'=>'loading-bars.svg'
            );

            $feature=$this->feature();

            if($_SESSION['user']['rights']['c']['tokens']['rights']){
                $feature['setting']['dev']=empty($_SESSION['user']['rights']['c']['tokens']['rights'])?false:intval($_SESSION['user']['rights']['c']['tokens']['rights']);
                /**
                 * 是否可以激活开发者账号
                 */
                if(CommonUserModel::canEnableOpenid(intval($_SESSION['user']['rights']['c']['tokens']['rights']))){
                    $token=new TokensModel();
                    $keys=$token->getDataByUid($_SESSION['user']['uid'],['app_id','app_secret','enc_key']);

                }
            }




            $this->v->assign(array('splash'=>$splash,'site'=>$_SESSION['site'],'featureJson'=>json_encode($feature),'feature'=>$feature));
        }


        //$router['mac']=str_replace("-","",$router['mac']);

    }





} 