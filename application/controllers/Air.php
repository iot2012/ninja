<?php

class AirController extends ApplicationController {

    public $isAuth=false;
    public $title="ry_apple";
    protected $provider;
    function wifiAction(){
        $shakeInfo=$this->getShakeUserInfo();
        return true;
    }

    function init(){
        parent::init();
        $ids=['gh_3caf6c97a9dc'=>'ninja','gh_996dce7696dc'=>'peach'];


        $this->provider=empty($_GET['_n'])?'ninja':$_GET['_n'];


    }
    /**
     * 绑定Mac地址
     */
    function bindMacAction(){
        if(Validator::isReg('mac',$_GET['mac'])){

        }

    }
    function wechatconfigAction(){


//        if(!isset($_SERVER['HTTP_REFERER'])){
//            exit('<script>alert("error");</script>');
//        }
        $ref=$_SERVER['HTTP_REFERER'];
        $wechatCfg=$this->config($ref);
        $json=json_encode($wechatCfg);
       $js=<<<EOF

    var official={};

    wx.config({
        debug: false,
        appId: "{$wechatCfg['appId']}",
        timestamp: {$wechatCfg['timestamp']},
        nonceStr: "{$wechatCfg['nonceStr']}",
        signature: "{$wechatCfg['signature']}",
        jsApiList: [
            'openWXDeviceLib','closeWXDeviceLib','getWXDeviceInfos','getWXDeviceTicket','setSendDataDirection','startScanWXDevice',
            'connectWXDevice','disconnectWXDevice','stopScanWXDevice','checkJsApi', 'onMenuShareTimeline', 'onMenuShareAppMessage', 'onMenuShareQQ', 'onMenuShareWeibo', 'hideMenuItems', 'showMenuItems', 'hideAllNonBaseMenuItem', 'showAllNonBaseMenuItem', 'translateVoice', 'startRecord', 'stopRecord', 'onRecordEnd', 'playVoice', 'pauseVoice', 'stopVoice', 'uploadVoice', 'downloadVoice', 'chooseImage', 'previewImage', 'uploadImage', 'downloadImage', 'getNetworkType', 'openLocation', 'getLocation', 'hideOptionMenu', 'showOptionMenu', 'closeWindow', 'scanQRCode', 'chooseWXPay', 'openProductSpecificView', 'addCard', 'chooseCard', 'openCard','startSearchBeacons','stopSearchBeacons','onSearchBeacons'

        ]
    });
//        window.json=JSON.stringify($json);
//        window.appId="{$wechatCfg['appId']}";
//        $("body").append($('<textarea rows="20" cols="200"></textarea>').val(window.json+"indows $ref appid:"+appId));
        var titleNode=document.getElementsByTagName("title")[0];
        var result=titleNode.getAttribute("data-edit");
        if(!result){
            titleNode.text="{$wechatCfg['title']}";

        } else{

            if(location.href.indexOf("_n=peach")!=-1){

                titleNode.text="小蜜桃餐厅";
            } else {
                $("#one_key_booking").remove();
            }

        }


EOF;



        exit($js);

    }


    /**
     *
     *
     * max:7c669d9f6468 'deviceid' => string 'gh_3caf6c97a9dc_abc1f44c10f401097376e110c1db11b7' (length=48) 'qrticket' => string 'http://we.qq.com/d/AQCb0Qq4KMiXwYaYmVftrOIRZqo4btA5kuqmIUxm' (length=59)
     * @return bool
     *
     */
    /**
     *           'group_id' => int 5258
                'group_name' => string 'R' (length=1)
     *
     */
    function addDevGroupAction(){

        $wc = new WechatIot(array_merge($this->c->openid->wechat->{$this->provider}->toArray(),array('logcallback'=>array($this->log,'info'),'debug'=>false)));
        $ret=$wc->devGroupAdd($_GET['name']);
        var_dump($ret);
        exit;
    }
    function groupDevListAction(){


        $wc = new WechatIot(array_merge($this->c->openid->wechat->{$this->provider}->toArray(),array('logcallback'=>array($this->log,'info'),'debug'=>false)));
        $ret=$wc->groupDevList(5258);
        var_dump($ret);
        exit;
    }
    /**
     * device_identifiers:["device_id":10100,
    "uuid":"FDA50693-A4E2-4FB1-AFCF-C6EB07647825",
    "major":10001,
    "minor":10002
     * ]
     */

    function addDevToGroupAction(){
        $wc = new WechatIot(array_merge($this->c->openid->wechat->{$this->provider}->toArray(),array('logcallback'=>array($this->log,'info'),'debug'=>false)));
        $ret=$wc->addDevs(5258,[
            [
                //"device_id"=>"1943186",
                "uuid"=>"FDA50693-A4E2-4FB1-AFCF-C6EB07647825","major"=>10023,"minor"=>45652
            ],
            [
                //"device_id"=>"1943185",
                "uuid"=>"FDA50693-A4E2-4FB1-AFCF-C6EB07647825","major"=>10023,"minor"=>45651
            ]

        ]);
        var_dump($ret);
        exit;
    }
    function decodeAction(){
        $code=bin2hex(base64_decode($_POST['code']));
        var_dump($code);

        return false;
    }
    function batchGenQrcodeAction(){

        $wc = new WechatIot(array_merge($this->c->openid->wechat->{$this->provider}->toArray(),array('logcallback'=>array($this->log,'info'),'debug'=>false)));


        $result=$wc->batchAuthDevice($_REQUEST);


        $m=new IbeaconWechatModel();


        $ret=$m->insert($result);

        $this->data=array('id'=>$ret,'result'=>$result);

        return $this->m();


    }
    function wcDevAction(){
        $wc = new WechatIot(array_merge($this->c->openid->wechat->{$this->provider}->toArray(),array('logcallback'=>array($this->log,'info'),'debug'=>false)));
        $arr=$_REQUEST;
        $result=$wc->deviceApplyid($arr);
        echo Misc_Utils::arr2tb($result['data']['device_identifiers']);
        exit;
    }
    function getQrcodeAction(){
        $wc = new WechatIot(array_merge($this->c->openid->wechat->peach->toArray(),array('logcallback'=>array($this->log,'info'),'debug'=>false)));
        var_dump($wc->getQrcode());

        return false;
    }
    function authorizeDeviceAction(){
        /**
         * authkey=>1234567890ABCDEF1234567890ABCDEF
         *
         *
         * [device_type=> gh_3caf6c97a9dc [device_id] => gh_3caf6c97a9dc_cfc0f5dec720ae0523f0124e620f6c15
         *
         *
         * gh_3caf6c97a9dc_cfc0f5dec720ae0523f0124e620f6c15' (length=48)
         * 'qrticket' => string 'http://we.qq.com/d/AQCb0Qq49qYkSY_xKVeFAHEpm2z313ehsBoPCEI7#param=1
         */
        /**
         * "device_num":"1",
        "device_list":[
        {
        "id":"dev1",
        "mac":"123456789ABC",
        "connect_protocol":"1|2",
        "auth_key":"",
        "close_strategy":"1",
        "conn_strategy":"1",
        "crypt_method":"0",
        "auth_ver":"1",
        "manu_mac_pos":"-1",
        "ser_mac_pos":"-2"
        }
        ],
        "op_type":"1"
         * @param $data
         * @return bool
         */
        //gh_3caf6c97a9dc [device_id] => gh_3caf6c97a9dc_abc1f44c10f401097376e110c1db11b7
        $devList=array(
             array(
                "id"=>"gh_996dce7696dc_c25ea6d76e579629ad98ceb16d003799",

                "mac"=>"EC1127570AD5",

                "connect_protocol"=>"3|1|2",
                "auth_key"=>"",
                "close_strategy"=>"1",
                "conn_strategy"=>"1",
                "crypt_method"=>"0",
                "auth_ver"=>"0",
                "manu_mac_pos"=>"-1",
                "ser_mac_pos"=>"-2"

             )


        );
        $wc = new WechatIot(array_merge($this->c->openid->wechat->peach->toArray(),array('logcallback'=>array($this->log,'info'),'debug'=>false)));
        var_dump($wc->batchAuthDevice($devList));
        return false;
    }
    function debugAction(){

        return true;
    }
    function wifiShakeAction(){
        $type=$this->r->getParam("t");
        $links=['workplace'=>['link'=>'http://www.gogoinfo.cn/templates/workplace/1/res/','txt'=>'慧阳科技','desc'=>'点击访问智慧企业','img'=>'/img/air/smart_bus.jpg'],'dinning'=>['link'=>'http://www.gogoinfo.cn/templates/dinning/1/res/?_n=peach','txt'=>'小蜜桃餐厅','desc'=>'点击访问餐厅主页','img'=>'/img/air/xiaomitao.jpg']];
        if(empty($links[$type])){
            $type="dinning";
        }
        $vars=['link'=>$links[$type]];
        $this->v->assign($vars);
        return true;
    }
    function blueAction(){
        $signPackage=$this->config();
        $assignVars=array('wechatCfg'=>$signPackage);
        $this->v->assign($assignVars);
        return true;
    }
    function bluetoothAction(){
        $signPackage=$this->config();
        $assignVars=array('wechatCfg'=>$signPackage);
        $this->v->assign($assignVars);
        return true;
    }
    function config($url=''){
        $query=[];
        if($url!=''){
            parse_str(parse_url($_SERVER['HTTP_REFERER'],PHP_URL_QUERY),$query);
            if(!empty($query['_n'])){
                $this->provider=$query['_n'];
            }
        }


        $jssdk = new \Wechat\JsSdk($this->c->openid->wechat->{$this->provider}->appid, $this->c->openid->wechat->{$this->provider}->appsecret);

        $redis=$this->redis();
        $signPackage = $jssdk->GetSignPackage($url,$this->provider);
        $signPackage['title']=$this->c->openid->wechat->{$this->provider}->title;
        return $signPackage;
    }
    function indexAction(){

        if(array_key_exists('wechat',$this->params)){

            $shake=new WechatShakeInfoMModel();
            $shakeInfo=$this->getShakeUserInfo();

            if(isset($shakeInfo['errcode']) && $shakeInfo['errcode']==0){
                $shake->insert($shakeInfo['data']);
            }
            $sensor=new SensorInfoMModel();
            $airData=$sensor->latestData($shakeInfo['data']['beacon_info']['uuid'],$shakeInfo['data']['beacon_info']['major'],$shakeInfo['data']['beacon_info']['minor']);

        } else {
            /**
             * demo data
             */
            $airData=array(
                'temp'=>'24',
                'hum'=>'34',
                'pm25'=>'95'
            );
        }

        $prov=new ProvinceMModel();
        $product=new ProductMModel();

        $prodData=$product->findByName('ry_air',array(),$this->lang);

        $product=$prodData;

        //adv data
        $adv=array(
            'pic_url'=>'/img/air/cocacola.jpg',
            'title'=>'可口可乐',
            'desc'=>'你想和谁分享新年第一瓶可口可乐'
        );

        $provinces=$prov->find(array(),100,array('name','code'));


        $assignVars=array('air'=>$airData,'noCache'=>false,'wechatAppId'=>$this->c->openid->wechat->{$this->provider}->appid,'adv'=>$adv,'provinces'=>$provinces,'productModels'=>$product['model'],'productColors'=>$product['color'],'product'=>$product);

        $this->v->assign($assignVars);

        return true;

    }
    function getAccessToken(){
        $wc = new Wechat(array_merge($this->c->openid->wechat->{$this->provider}->toArray(),array('logcallback'=>array($this->log,'info'),'debug'=>false)));
        return $wc->checkAuth();
    }
    function getShakeUserInfo(){
        $accessToken=$this->getAccessToken();
        $z=new ZCurl();

        $result=false;
        if($accessToken){
            $this->log->info("ACCESS_TOKEN.$accessToken.{$_GET['ticket']}");
            $result=$z->jsonPost("https://api.weixin.qq.com/shakearound/user/getshakeinfo?access_token=$accessToken",array(
                "ticket"=>$_GET['ticket'],
                'need_poi'=>1
            ));
        }
        return $result;

    }
    function getCurCity(){

    }

    function citiesAction(){
        $city=new CityMModel();
        $cities=$city->find(array("province_code"=>(intval($_POST['code']).'')),100,array('name','code'));

        exit(json_encode($cities));

    }


}
