<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 3/4/15
 * Time: 11:47 AM
 */

class WechatsenderController extends ApplicationController {

    function init(){

        session_start();
        unset($_SESSION['user']['rights']);
        unset($_SESSION['site']);
        $this->initHeader();
        $this->initEnv();

        $this->initCache();
    }

    function serviceAction()
    {

        $this->redis_sub();
        $this->redis_sub->subscribe(array('wechat_notify','wechat_log_action'), function ($redis, $ch, $msg) {


            printf("\n$ch arrived at %s data: %s\n",date("Y-m-d H:i:is"),$msg);
            $data = json_decode($msg, true);
            $func=$ch."M";

            if(method_exists($this,$func)){


                if (!empty($data)) {
                    $this->$func($data);
                }

            }

        });

    }
    function wechat_log_actionM($data){
        echo("wechat_log_actionM");
        array_map(function($who)use($data){
                echo("wechat_log_actionM");
                Misc_Utils::sendWechatNotify($who,$data['title'],$data['content'],$data['remark'],$data['nid']);
            },$data['to']
        );


    }

    function wechat_notifyM($data){

        array_map(function($who)use($data){

                Misc_Utils::sendWechatNotify($who,$data['title'],$data['content'],$data['remark'],$data['nid']);
            },$data['to']
        );


    }

} 