<?php

class IndexcliController extends ApplicationController
{
    public $exActions=array(

        'buildidx','company_signup','showlogin','login', 'createuser','index','qrloginimg','faceverify','forgetpwd','resetpwd','mfaauth','getregcode','newuser'
    );
    function buildIdxAction(){

        $buildArr=array(
            'fogpod'=>array('v_ibeacon')
        );
        foreach($buildArr as $db=>$tbs){
            $es=new Esindex($db);


            $type=preg_replace("/_m$/","",$tbs);
            $ret=$es->deleteType($type[0]);

            foreach($tbs as $key=>$type){
                $model=Misc_Utils::classFromTb($type);
                list($db,$tb,$pre)=$this->getTbInfo($model);
                $m=new $model();
                $cols=$m->indexes();
                $pId=$m->primary();
                if(!in_array($pId,$cols)){
                    $cols[]=$pId;
                }
                $newRecs=array();
                $recs=$m->find('',99999999,$cols);
                foreach($recs as $record){
                    $newRecs[$record[$pId]]=$record;
                }
                $es->createIndex($newRecs,$pre.$tb);
                echo("Finish indexed");
            }
        }

    }

    function indexAction(){


    }


}
