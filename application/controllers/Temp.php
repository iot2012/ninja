<?php

class TempController extends ApplicationController {

    public $isAuth;
    function indexAction(){

        $section=$_REQUEST['section'];
        if(method_exists($this,$section)){
            $this->$section($_REQUEST);
        }

        return $this->m();

    }
    //protected  $isAuth=false;
    // protected $exActions=array('dinning',"index","shop");

    function validTemp(){
        return true;
    }
    function validVendor(){
        return true;
    }
    function postAction()
    {

        if($this->validTemp($_POST['_temp_type']) && $this->validVendor($_POST['_vid'])){

            $tempData=new SiteMModel();
            $id=$tempData->insert($_POST);

            $this->data=array('id'=>$id);

        } else {

            $this->code=-1;
            $this->msg="invalid_vendor";
        }
        return $this->m();
    }
    function provinceAction(){
        $sid=$_REQUEST['sid'];
        $temp=new SiteMModel();
        if(Validator::isReg("mongoid",$sid)){
            return $temp->province($sid);
        }
        return array();

    }
    function shopAction(){




    }

    function homepage($data){
        if(Validator::isReg('mongoid',$data['sid'])) {
            $sid = $data['sid'];
            unset($data['sid']);
            $temp = new SiteMModel();
            $all_city = $temp->distinct('_info_info_file.province', array('_id' => new MongoId($sid)));

            $data = $temp->findOne(array('_id' => $sid), array($this->r->action, '_imgdir'));

            $this->data = array('comps' => $data[$this->r->action], 'temp' => array('all_city' => $all_city, '_imgdir' => $data['_imgdir']));
        } else {
            $this->code=-1;
        }
        return $this->m();
    }
    function food($data){

        if(Validator::isReg('mongoid',$data['sid'])){
            $sid=$data['sid'];
            unset($data['sid']);
            $temp=new SiteMModel();
            $dishes=$temp->findOne(array('_id'=>new MongoId($sid)),array('_info_dish_file','_info_info_file','_imgdir'));
            $shop_address=array();
            foreach($dishes['_info_info_file'] as $item){
                if($item['dinning_name']==$_POST['dinning_name']){
                    $shop_address=$item;
                    break;
                }
            }
            $dish_cats=Misc_Utils::array_pluck_arr($dishes['_info_dish_file'],array('dish_cat'));

            $this->data=array('comps'=>[],'temp'=>array('shop_address'=>$shop_address,'dish_cats'=>$dish_cats,'dishes'=>$dishes['_info_dish_file'],'_imgdir'=>$dishes['_imgdir']));
        } else {
            $this->code=-1;

        }


        return $this->m();

    }
    function shop($data){
        if(Validator::isReg('mongoid',$data['sid'])) {
            $sid = $data['sid'];
            unset($data['sid']);
            $temp = new SiteMModel();
            $shop = $temp->findOne(array('_id' => new MongoId($sid)), array('_info_info_file', '_imgdir'));

            $this->data = array('comps' => [], 'temp' => array('shops' => $shop['_info_info_file'], '_imgdir' => $shop['_imgdir']));
        } else {
            $this->code=-1;
        }
        return $this->m();
    }
}