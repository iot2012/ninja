<?php







class WechatController extends ApplicationController
{

    public $isAuth=false;
    public $aliCfg;

    public $payType = '';

    public $validType = array(
        'app', 'pod', 'product'
    );


    public $virtualItems = array('app', 'pod');

    function init()
    {

        parent::init();

        $this->wechatCfg = $this->c->pay->ali->toArray();
    }

    function preauthcodeAction(){

        $options = $this->c->openid->wechat_open->gogo->toArray();
        $wo = new WechatOfficial($options);
        $redis=$this->redis();
        $preCode=$wo->preAuthCode('',$redis);
        if($preCode){
            $this->data=array(
                'appId'=>$options['appid'],
                'preAuthCode'=>$preCode
            );
        } else {
            $this->code=-1;
        }

        return $this->m();

    }
    function shopsAction(){


    }
    function callbackAction(){
        $id=$this->r->getParam("id");
        $official=new WechatOfficialModel();

        $account=$official->findOne(["id"=>$id],['appid','appsecret']);
        $_SESSION['wechat']['auth_code']=$_GET['code'];
        $accessUrl="https://api.weixin.qq.com/sns/oauth2/access_token?appid={$account['appid']}&secret={$account['appsecret']}&code={$_GET['code']}&grant_type=authorization_code";
        $tokenInfo=json_decode(file_get_contents($accessUrl),true);


        //$wcUser->update2(array('$set' => $userInfo), array('openid' => $userInfo['openid']));
        $userUrl="https://api.weixin.qq.com/sns/userinfo?access_token={$tokenInfo['access_token']}&openid={$tokenInfo['openid']}&lang=zh_CN";

        $userInfo=json_decode(file_get_contents($userUrl),true);
        $priv=$userInfo['privilege'];
        $query=http_build_query($userInfo);
        $userInfo['appid']=$account['appid'];
        $userInfo['privilege']=$priv;
        $userInfo['access_token']=$tokenInfo['access_token'];
        $userInfo['refresh_token']=$tokenInfo['refresh_token'];
        $userInfo['scope']=$tokenInfo['scope'];
        $userInfo['expires_in']=$tokenInfo['expires_in'];
        $user=new WechatUserMModel();
        $set=array('$set' =>$userInfo);
        $user->update($set,array('openid' => $userInfo['openid'],'appid'=>$account['appid']));
        $userData=http_build_query(['_u'=>json_encode(Misc_Utils::array_pluck($userInfo,['openid','appid','city','province','sex','nickname','headimgurl']))]);
        if(!empty($_GET['_r'])){
            if(strstr($_GET['_r'],"?")!==false){
                $url=$_GET['_r']."&".$query."&".$userData;

            } else {
                $url=$_GET['_r']."?".$query."&".$userData;
            }


        } else {
            $url="/index/index";
        }

        header("Location: $url");
        exit;


    }
    function test1Action(){
      
    }
    function mobileLoginAction(){
        $official=new WechatOfficialModel();
        $query['provider']=$_GET['_n'];
        if(empty($_GET['_n'])){
            $query['provider']="ninja";
        }
        if(preg_match("/^wx_\w+$/",$_GET['_n'],$ret)){
            $query['appid']=$_GET['_n'];
        }
        if(preg_match("/^gh_\w+$/",$_GET['_n'],$ret)){
            $query['userId']=$_GET['_n'];
        }

        $account=$official->findOne($query);
        $wechat=['provider'=>$account['provider'],'appid'=>$account['appid']];
        if(!empty($account)){
            $referer=$_SERVER['HTTP_REFERER'];
            if(!empty($_GET['_r'])){
                $referer=$_GET['_r'];
            }
            $url=urlencode($account['redirect_url']."/id/{$account['id']}?_r=$referer");
            $appId=$account['appid'];
            $url="https://open.weixin.qq.com/connect/oauth2/authorize?appid=$appId&redirect_uri=$url&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect";
            header("Location: $url");

        }

        exit;


    }
    function officialLoginAction(){




        $options = $this->c->openid->wechat_open->gogo->toArray();
        $redis=$this->redis();
        $wo = new WechatOfficial($options,$redis);

        $auth_code=$_GET['auth_code'];
        $authorization_info=$wo->authorization_info($_SESSION['user']['uid'],$auth_code);
        /**
         * 微信全网发布的测试appid
         */
        if($authorization_info['authorizer_appid']=='wx570bc396a51b8ff8'){
            exit("创建成功");

        }
      //  wx570bc396a51b8ff8

        $accountInfo=$wo->accountInfo($_SESSION['user']['uid'],$authorization_info['authorizer_appid'],$options['appid']);

        if(empty($accountInfo['authorization_info']['authorizer_appid'])){
            $accountInfo=$wo->authorizer_account_info($_SESSION['user']['uid'],$authorization_info['authorizer_appid']);

        }
        $ui=$this->r->getParam('ui');

        $shopList=$wo->shop_list($authorization_info['authorizer_access_token']);
        $assignArr=[];
        if(!empty($shopList['data']['records'])){
            $assignArr['shopList']=json_encode($shopList['data']['records']);

        } else {
            $assignArr['shopList']='[]';

        }
        $this->v->assign($assignArr);
//        $result=$this->v->render("page/genqrcache.phtml");
        exit($this->v->render('wechat/officiallogin_'.trim($ui).".phtml"));

    }
    function testAction()
    {


        $prodId=$_GET['pid'];
        if(!Validator::isReg('mongoid',$prodId)){
            $this->code=-1;

        } else {
            $prod=new ProductMModel();
            $prodData=$prod->findById($prodId,array(),$this->lang);
            if(!empty($prodData)){

                $this->data=$this->getJsParams($prodData);

            } else {
                $this->code=-1;
                $this->msg="err_req";
            }
        }
        return $this->m();



    }
    function payConfig(){


       $result=$this->c->openid->wechat->ninja->pay->toArray();

       return [array_map(function($key){
           return "#".$key."#";
       },array_keys($result)),array_values($result)];

    }
    function buyAction()
    {



        $configCls = <<<'EOF'
        class WxPayConfig
        {

            const APPID = '#APPID#';
            const MCHID = '#MCHID#';
            const KEY = '#KEY#';
            const APPSECRET = '#APPSECRET#';


            const SSLCERT_PATH = '#SSLCERT_PATH#';
            const SSLKEY_PATH = '#SSLKEY_PATH#';


            const CURL_PROXY_HOST = "#CURL_PROXY_HOST#";
            const CURL_PROXY_PORT = #CURL_PROXY_PORT#;

            static $NOTIFY_URL="#NOTIFY_URL#";


            const REPORT_LEVENL = #REPORT_LEVENL#;

        }
EOF;
        $config=$this->payConfig();
        $configCls=str_replace($config[0],$config[1],$configCls);
        eval($configCls);
        require_once "Wechat/WxPay.Api.php";
        require_once "Wechat/WxPay.JsApiPay.php";
        $assignArr=array();
        $prodId=$_REQUEST['pid'];
        if(!Validator::isReg('mongoid',$prodId)){
             $this->code=-1;

        } else {
            $prod=new  ProductMModel();
            $order=new OrderMModel();


            $prodData=$prod->findById($prodId,array(),$this->lang);

            if(!empty($prodData)){

                $prodData['color']=$_REQUEST['color'];
                $prodData['model']=$_REQUEST['model'];
                $orderData=$this->mkOrder($prodData,$_REQUEST['realname'],$_REQUEST['address'],$_REQUEST['bd_gps']);

                $assignArr['jsParams']=$this->getJsParams($orderData);



            } else {
                $this->code=-1;
                $this->msg="err_req";
            }
        }
        $assignArr['title']=$prodData['name'];
        $this->v->assign($assignArr);
        return true;



    }
    function mkOrder($prodData,$realname='',$address='',$bd_gps=''){
        $price=intval($_REQUEST['number'])*$prodData['price']['price']*100;

        $orderData['body']=$prodData['desc'];
        $orderData['trade_status']='';
        $orderData['subject']=$prodData['name'];
        $orderData['total_fee']=$price;
        $orderData['address']=$address;
        $orderData['realname']=$realname;
        $orderData['bd_gps']=$bd_gps;
        $orderData['item_id']=$prodData['_id'];
        $orderData['item_type']='real';
        $uid=(!empty($_SESSION['user']['uid']))?$_SESSION['user']['uid']:'xxx';
        $orderData['trade_no']=Misc_Utils::genOrderSn($uid);
        $orderData['quantity']=intval($_REQUEST['number']);

        $order=new OrderMModel();
        $order->insert($orderData);
        return $orderData;
    }
    function getJsParams($data){
        $tools = new JsApiPay();
        $openId = $tools->GetOpenid();

//统一下单

        $log=new Misc_Logger('wepay');

        $input = new WxPayUnifiedOrder();

        $input->SetBody($data['body']);
        $input->SetAttach($data['item_type']);

        $input->SetOut_trade_no($data['trade_no']);

        $input->SetTotal_fee($data['total_fee']);

        $input->SetTime_start(date("YmdHis"));

        $input->SetTime_expire(date("YmdHis", time() + 600));


        $input->SetNotify_url(WxPayConfig::$NOTIFY_URL);
        $input->SetTrade_type("JSAPI");
        $input->SetOpenid($openId);
        $order = WxPayApi::unifiedOrder($input);
        $jsApiParameters = $tools->GetJsApiParameters($order);
        return $jsApiParameters;
    }

}