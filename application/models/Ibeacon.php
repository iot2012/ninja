
<?php

class IbeaconModel extends SysModel {


    static $_tbName  = 'ibeacon';
    protected $_primary = 'id';

    /**
     * validate each field in tables
     * @todo store get  message in i18n file
     */

    /**
     *    'gt'=>'',
     *    'get'=>'',
     *    'let'=>'',
     * @var array
     */
    protected  $_indexes=array(
        'uuid','major','minor','alias'
    );

    protected $_tbMeta=array(
        'alias'=>array('reg'=>"cn_en","let"=>"25"),
        'uuid'=>array(
            'reg'=>'uuid',
            'eq'=>'36',
            'rights'=>4
         ),
        'major'=>array('reg'=>"ui2",'rights'=>4),
        'minor'=>array('reg'=>"ui2"),
        'enter_min'=>array('reg'=>"uf4"),
        'enter_max'=>array('reg'=>"uf4"),
        'alias'=>array('reg'=>"cn_en","let"=>"25"),
        'uid'=>array('reg'=>"ui8",'rights'=>4)


    );
    protected $err=array('reg'=>array());

    function getAllIbeacons($uid){

        $m=new IbeaconModel();

        return $m->total();
    }
    function __construct(){

        parent::__construct();


    }

    function getCurrentAdv($uuid,$major,$minor,$lang="zhcn"){

        return $this->execSql("select a.* from {$this->_pre}adv as a
                                join {$this->_pre}ibeacon_adv as ai on a.id=ai.adv_id
                                join {$this->_pre}ibeacon as i on ai.ib_id=i.id

                                where (a.lang='$lang' or a.lang='all') and
                                (i.uuid=? and i.major=? and i.minor=?)
                                order by ai.ctime  desc",
            array($uuid,$major,$minor));



    }
    function getConfig($uuid,$major){

        $result=$this->find(array('uuid'=>$uuid,'major'=>$major),0,array("major","minor","enter_min","enter_max","id","trigger_time"));
        $ids=array();
        $result2=array();
        $ids=array_map(function($item)use(&$ids,&$result2){
            $result2[$item['id']]=$item;
            $result2[$item['id']]['lang']=array();
            return $item['id'];
        },$result);

        $ids="('".implode("','",$ids)."')";

        $langs=$this->execSql("select ib_id,lang from {$this->_pre}ibeacon_adv where ib_id in $ids and  lang!='all' group by ib_id,lang");

        array_map(function($item) use(&$result2){
            if(isset($result2[$item['ib_id']])){
                $result2[$item['ib_id']]['lang'][]=$item['lang'];
            }
        },$langs);
        return array_values($result2);

    }
    function getOne($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        return  $this->findOne(array($this->_primary=>$data));

    }
    function beforeAdd($arr){
        $arr['uid']=$_SESSION['user']['uid'];
        $com=new CompanyModel();
        $major=$com->majorByUid($arr['uid']);
        $arr['major']=$major['id'];
        $arr['uid']=$_SESSION['user']['uid'];
        return $arr;
    }
    function afterAdd($data,&$out){
        $out['major']=$data['major'];
        $out['uid']=$_SESSION['user']['uid'];
    }
    function add($arr){
        if(!isset($arr['uuid'])){

            $arr['uuid']=$this->uuid=Yaf\Registry::get("config")->site->ib_uuid;
        }
        return $this->insert($arr);
    }


}