<?php
class NotifyDataModel extends MongoSysModel {


    static $_tbName = 'notify_data';



    protected $_tbMeta=array (
        'from' =>
            array (
                'reg' => 'varchar',
                'lt'=>'35',
                'desc'=>"when_user"
            ),
        'to'=>array(
            'reg' => 'varchar',
            'lt'=>'35',
            'desc'=>"to_user"
        ),


        'content'=>array (
            'reg' => 'tiny_desc',
            'desc'=>'notify_content',

        ),
        'name_notify'=>array('reg' => 'varchar')
    );


}

