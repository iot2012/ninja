<?php
class PageMModel extends MongoSysModel {


    static $_tbName  = 'page';
    static $_primary  = '_id';

    static $_opAction=array(
        'crud'=>array(
            'import'=>['en'=>false,'link'=>'#'],
            'export'=>['en'=>false,'link'=>'#'],

        )

    );

    protected $_tbMeta=array (
        '_id' =>
            array (
                'reg' => 'mongoid',
                'desc' => 'mongoid',"rights"=>4
            ),
        /**
         * ['id'=>[]]
         */
        'comps'=>array(
            'reg' => 'json',
            'desc' => '',

        ),
        'title' =>
            array (
                'lt' => '30',
                'reg' => 'title'),

        'html' =>
            array (
                'lt' => '10240',
                'reg' => 'text',
                'desc' => 'temp_name',
            ),

        'ctime' =>
            array (
                'reg' => 'timestamp',
                'desc' => 'ctime',"rights"=>4
            ),
        'type'=>array(
            'reg'=>'en_name'

        ),
        'uid' =>
            array (
                'reg' => 'i8',
                'desc' => 'uid',"rights"=>4
            )


    );

    function __construct(){

        parent::__construct();

    }

    function getOne($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        $primary=$this->primary();
        return  $this->findOne(array($primary=>$data));

    }




}