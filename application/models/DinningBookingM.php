<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 11/28/14
 * Time: 10:42 AM
 */

class DinningBookingMModel extends MongoSysModel {


    static $_tbName = 'dinning_booking';



    protected $_tbMeta=array(
        'fa_icon'=>array('reg'=>"fa_icon",'rights'=>4),
        '_id'=>array(
            'reg'=>'mongoid',
            'lt'=>'1024',
            'rights'=>4
        ),
        'uid'=>array('reg'=>"ui8"),
        'name'=>array('reg'=>"device_name",'lt'=>'30'),
        'mac'=>array('reg'=>"mac"),
        'maintype'=>array('reg'=>"maintype", 'enum'=>array('nas'=>"nas",'camera'=>"camera",'printer'=>"printer",'tv'=>'tv','scanner'=>'scanner','projector'=>'projector')),
        'subtype'=>array('reg'=>"maintype"),
        'ctime'=>array('reg'=>"timestamp"),
        'fields'=>array('reg'=>"json",'lt'=>'10000')
    );

}