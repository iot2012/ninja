
<?php

class TokensModel extends RedisSysModel {



    /**
     * validate each field in tables
     * @todo store get  message in i18n file
     */

    /**
     *    'gt'=>'',
     *    'get'=>'',
     *    'let'=>'',
     * @var array
     */
    protected $_tbMeta=array(
    'app_id'=>array(
        'reg'=>'ry_appid',
        'lt'=>15
    ),
    'app_secret'=>array('reg'=>"ry_appsecret",'eq'=>32),
    'redirect_uri'=>array('reg'=>"http",'let'=>200)
);
    function uid($uid){
        return 'u'.$uid;
    }
    function appId($appId){
        return 'ap_'.$appId;
    }
    function isValidAccessToken($token){
        return $this->r->hGetAll($this->accessTokenKey($token));
    }
    function existAppId($appId){
        return $this->r->hMGet($this->appId($appId),['app_secret','level','access_token']);
    }
    function accessToken($appId,$appSecret,$level,$oldToken,$expiredSecs=7200){

        $tokenKey=$this->accessTokenKey($oldToken);
        $accessToken=$oldToken;



        $expire=$this->exists($tokenKey,['expire']);
        $expire=$expire['expire'];
        if(empty($oldToken) || !$expire){
            /**
             * 设置access_token的数据
             */
            $expire=$expiredSecs;
            $accessToken=Misc_Password::genAccessToken($appId,$appSecret);
            $tokenKey=$this->accessTokenKey($accessToken);
            $this->r->hMset($tokenKey,array(
                'app_id'=>$appId,
                'app_secret'=>$appSecret,
                'level'=>$level,
                'expire'=>(time()+$expiredSecs)
            ));
            $this->r->hMset($this->appId($appId),array(
                'access_token'=>$accessToken,
            ));
            $this->r->expire($tokenKey,$expiredSecs);
        } else {
            $expire=($expire-time()-1);
        }

        return [$accessToken,$expire];
    }
    function accessTokenKey($token){
        return md5($token);
    }
    function newEncKey($len=32){

        return Misc_Password::genSalt($len);
    }
    /**
     * @param $uid
     * @param $level
     * @param $prefix appId prefix
     */


    function newAppIdSecret($uid,$userName,$level,$prefix=''){
        /**
         * 删除原来的appId,accessToken
         */

        $result=$this->getDataByUid($uid,array("app_id","access_token"));

        if(!empty($result['app_id'])){
            $this->r->del(array($this->appId($result['app_id']),$this->accessTokenKey($result['access_token'])));
        }
        list($appId,$appSecret)=Misc_Password::genAppIdSecret($prefix);

        $this->r->hMset($this->appId($appId),array(
            'app_secret'=>$appSecret,
            'level'=>$level,
            'uid'=>$uid,
            'username'=>$userName
        ));

        $this->r->hMset($this->uid($uid),array(
            'app_id'=>$appId,
            'app_secret'=>$appSecret,
            'level'=>$level,
        ));

        return array('app_id'=>$appId,'app_secret'=>$appSecret);
    }
    function getAccountByUid($uid){

        return  $this->getDataByUid($this->uid($uid),['app_secret','app_id','enc_key']);
    }
    function getDataByAppid($appId,$cols=array()){

        return  $this->r->hMGet($this->appId($appId),['uid','username','level']);
    }
    function getDataByUid($uid,$cols=array()){

        return  $this->r->hMGet($this->uid($uid),$cols);
    }

}