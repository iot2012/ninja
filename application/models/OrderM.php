<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 2/6/15
 * Time: 10:15 AM
 */

/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 2/6/15
 * Time: 9:24 AM
 */


class OrderMModel extends MongoSysModel
{


    static $_tbName = 'order';


    protected $_tbMeta = array(
        '_id' =>
            array(
                'reg' => 'mongoid',
                'desc' => 'uid',
            ),
        'uid' =>
            array(
                'reg' => 'ui8',
            ),
        'status' =>
            array(
                'reg' => 'ui1',
                'enum'=>array('0'=>'unconfirmed','1'=>'confirmed','2'=>'canceled','3'=>'invalid','4'=>'return')

            ),
        'pay_status' =>
            array(
                'reg' => 'ui1',
                'enum'=>array('0'=>'nonpayment','1'=>'payment','2'=>'paid','3'=>'invalid','4'=>'return')

            ),
        'realname' =>
            array(
                'reg' => 'cn_en'
            ),
        'money_paid'=>array(
            'reg' => 'uf2'
        ),
        'address'=>array(
            'reg' => 'varchar',
            'lt'=>80
        ),
        'bd_gps'=>array(
            'reg' => 'ignore',
            'lt'=>50
        ),
        'address'=>array(
            'reg' => 'varchar',
            'lt'=>80
        ),
        'prod_info'=>array(
            'reg'=>'text'
        ),
        'service_id'=>array('reg'=>'mongoid'),
        'service_name'=>array('reg'=>'service_name'),
        'ctime'=>array('reg'=>'timestamp'),


    );

}