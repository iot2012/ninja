<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 11/28/14
 * Time: 10:42 AM
 */

use Ryatek\Fogpod;
class WifiAssocMModel extends MongoSysModel {


    static $_tbName = 'wifi_assoc';

    //指定可以直接通过rest方式调用的方法,返回值最好是数组
    static $_opAction=array(
        'crud'=>array(

            'edit'=>['en'=>false],
            'import'=>['en'=>false]

        )

    );
    protected $_tbMeta=array(


        '_id'=>array(
            'reg'=>'mongoid',
            'lt'=>'1024',
            'rights'=>4
        ),
        'uid'=>array('reg'=>"ui0",'rights'=>4),

        'fg_username'=>array('reg'=>"en_name",'lt'=>30),
        'fg_password'=>array('reg'=>"varchar",'lt'=>30),
        'reg_routers'=>array('reg'=>"ignore",'rights'=>4),
        'ctime'=>array('reg'=>'timestamp','rights'=>4)
    );
    function add(&$data,$user){
        $fg=new Fogpod();
        $result=$fg->bind_wifi($data['fg_username'],$data['fg_password']);
        if($result['code']==1){
            $routerData=$result['data'];
            $ret=[];
            $ret['data']['reg_routers']=$routerData;
            $ret['data']['ctime']=time();
            $ret['data']['fg_username']=$data['fg_username'];
            $ret['data']['fg_password']=$data['fg_password'];

            $setArr=['uid'=>$user['uid'],'fg_username'=>$data['fg_username'],'fg_password'=>$data['fg_password'],'ctime'=>new MongoDate($ret['data']['ctime'])];
            $op=array('$addToSet'=>array('reg_routers'=>$routerData),'$set'=>$setArr);
            $ownerId=$this->ownerId();
            $query=array($ownerId=>empty($arr[$ownerId])?intval($_SESSION['user'][$ownerId]):$arr[$ownerId]);


            $query['fg_username']=$data['fg_username'];

            $ret['data']['_id']=parent::update($op,$query);
            return $ret['data'];

        } else {
            $data['_ret']['code']=-1;
            $data['_ret']['msg']='uid_pwd_match';
        }

    }
    function afterAdd($data,&$out){
        $out['major']=$data['major'];
        $out['uid']=$_SESSION['user']['uid'];
        if(!empty($data['shop_id'])){
            $this->addWifiDevice($data);
        }

    }

} 