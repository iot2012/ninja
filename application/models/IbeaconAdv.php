<?php
class IbeaconAdvModel extends SysModel {


    static $_tbName  = 'ibeacon_adv';
    static $_primary  = 'id';


    protected $_tbMeta=array (
  'adv_id' => 
  array (
    'reg' => 'ui4',
    'desc' => 'adv_id',
      'ref_tb'=>'adv'
  ),
  'ib_id' => 
  array (
    'reg' => 'ui4',
    'desc' => 'ib_id',
      'ref_tb'=> 'ibeacon'
  ),

  'uid' => 
  array (
    'reg' => 'ui8',
    'desc' => 'uid',
      'rights'=>4
  ),
  'ctime' => 
  array (
    'reg' => 'ui4',
    'desc' => 'ctime','rights'=>4
  ),
);

    function __construct(){

        parent::__construct();

    }

    function exists($data){

        return  $this->findOne($data);

    }


}