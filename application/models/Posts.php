<?php
class PostsModel extends SysModel {


    static $_tbName  = 'posts';
    static $_primary  = 'id';



    protected $_tbMeta=array (
  'id' => 
  array (
    'reg' => 'i8',
    'desc' => 'id',
  ),
  'content' => 
  array (
    'lt' => '20000',
    'reg' => 'varchar',
    'desc' => 'content',
  ),
  'uid' => 
  array (
    'reg' => 'ui8',
    'desc' => 'uid',
      'rights'=>4
  ),
  'username' => 
  array (
    'lt' => '25',
    'reg' => 'char',
    'desc' => 'username',
      'rights'=>4
  ),
  'subject' => 
  array (
    'lt' => '500',
    'reg' => 'varchar',
    'desc' => 'subject',
  ),
  'classid' => 
  array (
    'reg' => 'ui2',
    'desc' => 'classid',
  ),
  'catid' => 
  array (
    'reg' => 'ui2',
    'desc' => 'catid',
  ),
  'viewnum' => 
  array (
    'reg' => 'ui3',
    'desc' => 'viewnum',
      'rights'=>4
  ),
  'replynum' => 
  array (
    'reg' => 'ui3',
    'desc' => 'replynum',
      'rights'=>4
  ),
  'hot' => 
  array (
    'reg' => 'ui3',
    'desc' => 'hot',
      'rights'=>4
  ),
  'ctime' =>
  array (
    'reg' => 'ui4',
    'desc' => 'dateline',
      'rights'=>4
  ),
  'picflag' => 
  array (
    'reg' => 'i1',
    'desc' => 'picflag',
      'rights'=>4
  ),
  'noreply' => 
  array (
    'reg' => 'i1',
    'desc' => 'noreply',

  ),
  'privacy' => 
  array (
    'reg' => 'i1',
    'desc' => 'privacy',
  ),
  'password' => 
  array (
    'lt' => '10',
    'reg' => 'char',
    'desc' => 'password',
  ),
  'favtimes' => 
  array (
    'reg' => 'ui3',
    'desc' => 'favtimes',
      'rights'=>4
  ),
  'sharetimes' => 
  array (
    'reg' => 'ui3',
    'desc' => 'sharetimes',
      'rights'=>4
  ),
  'status' => 
  array (
    'reg' => 'ui1',
    'desc' => 'status',
  ),
  'click1' =>
  array (
    'reg' => 'ui2',
    'desc' => 'click1',
      'rights'=>4
  ),

  'tags' => 
  array (
    'lt' => '300',
    'reg' => 'varchar',
    'desc' => 'tags'

  ),
);

    function __construct(){

        parent::__construct();

    }

    function exists($data){

        return  $this->findOne($data);

    }


}