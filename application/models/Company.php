<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 6/23/14
 * Time: 4:39 PM
 */


class CompanyModel extends SysModel {


    static $_tbName  = 'company';
    static $_primary  = 'id';
    protected $_tbMeta=array(
        'company_name'=>array(
            'reg'=>'sm_title'
        ),
        'email'=>array('reg'=>"email"),
        'contacts'=>array('reg'=>"cn_en"),
        'mobile'=>array('reg'=>"cn_mobile"),
        'pkg_type'=>array('reg'=>"cn_en",'enum'=>array('basic'=>"basic",'pro'=>'pro','com'=>'com')),
        'profession_id'=>array('reg'=>"ui1"),
        /**
         * status 1:waiting approved 2:approved 3:expired
         */
        'status'=>array('reg'=>"ui1",'enum'=>array('1'=>"normal",'2'=>'expire','3'=>'inactive','4'=>'disable')),
        'uid'=>array('reg'=>"ui8",'rights'=>4),

    );
    function majorByUid($uid){
        return $this->findOne(array('uid'=>$uid),array('id'));

    }
    function getById($id){

        return $this->findOne($id);

    }
    function getByEmail($email){

        return $this->findOne(array("email"=>$email));

    }
    function getByMobile($mobile){

        return $this->findOne(array("mobile"=>$mobile));

    }

    function beforeAdd($record){
        $cfg=Yaf\Application::app()->getConfig();
        $user=new CommonUserModel();
        $record['z_op_result']['code']=1;
        $record['z_op_result']['msg']='';
        $userName=$user->genRandomUser('lt_');
        $result=$user->isExistEmail($record['email']);
        if(!$result[0]){
            $record['uid']=$user->addUser($userName,$cfg['site']['def_pwd'],$record['email'],empty($record['contacts'])?$record['company_name']:$record['contacts'],10);
        } else {
            $record['z_op_result']['code']=-1;
            $record['z_op_result']['msg']="exist email";
        }

        return $record;

    }

    function afterAdd($record){


    }




}
