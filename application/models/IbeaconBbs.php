<?php
class IbeaconBbsModel extends SysModel {


    static $_tbName  = 'ibeacon_bbs';
    static $_primary  = 'id';



    protected $_tbMeta=array (
          'id' =>
          array (
            'reg' => 'ui4',
            'desc' => 'id',
          ),
          'content' =>
          array (
            'lt' => '1024',
            'reg' => 'varchar',
            'desc' => 'content',
          ),
          'ctime' =>
          array (
            'reg' => 'ui4',
            'desc' => 'ctime',
          ),
          'major' =>
          array (
            'reg' => 'ui2',
            'desc' => 'major id',
          ),
          'pic_url_1' =>
          array (
            'lt' => '100',
            'reg' => 'file',
            'desc' => 'pic_url_1',
          ),
          'ip' =>
          array (
            'reg' => 'ui4',
            'desc' => 'ip',
          )
    );

    function __construct(){

        parent::__construct();

    }

    function exists($data){

        return  $this->findOne($data);

    }


}
