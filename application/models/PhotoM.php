<?php
class PhotoMModel extends MongoSysModel {


    static $_tbName = 'photo';


        //@todo 增加json的类型校验
    protected $_tbMeta=array (
        '_id' =>
            array (
                'reg' => 'mongoid','rights'=>4

            ),
        'uid' =>
            array (
                'reg' => 'i8',
                'rights'=>4
            ),
        'title' =>
            array (
                'lt' => '100',
                'reg' => 'varchar',
                'rights'=>4
            ),
        'pic_url'=>array(
            'lt'=>512,
            'reg'=>'file',
            'data_extra'=>'data-role=upload',
            'desc'=>'upload_file',
        ),

        'comments'=>array(
            'reg' => 'ignore',
            'rights'=>4
        ),

        'favs'=>array(
            'reg' => 'ignore',
            'rights'=>4
        ),
        'ctime' =>
            array (
                'reg' => 'i4',
                'rights'=>4
            )


    );






}