<?php
class CompDataMModel extends MongoSysModel {


    static $_tbName  = 'comp_data';
    static $_primary  = '_id';


    protected $_tbMeta=array (
        '_id' =>
            array (
                'reg' => 'mongoid',
                'desc' => 'mongoid',"rights"=>4
            ),

        'page_id'=> array (
            'reg' => 'mongoid',
            'desc' => 'mongoid',"rights"=>4
        ),
        'app_data' =>
            array (
                'lt' => '16777215',
                'reg' => 'text',
                'desc' => 'content'),
        'ctime' =>
            array (
                'reg' => 'timestamp',
                'desc' => 'ctime',"rights"=>4
            ),
        'uid' =>
            array (
                'reg' => 'i8',
                'desc' => 'uid',"rights"=>4
            )


    );

    function __construct(){

        parent::__construct();

    }

    function getOne($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        $primary=$this->primary();
        return  $this->findOne(array($primary=>$data));

    }






}