<?php
class MyAppMModel extends MongoSysModel {


    static $_tbName = 'my_app';



    protected $_tbMeta=array (
  'uid' => 
  array (
    'reg' => 'i8',
    'desc' => 'uid',
  ),
  'ids' =>
  array (
    'reg' => 'ignore',
  ),
  'name' => 
  array (
    'lt' => '60',
    'reg' => 'varchar',
  ),
  'atime' => 
  array (
    'reg' => 'i4',
  ),
  'status' => 
  array (
    'reg' => 'i1'
  ),
);


    function myApps($uid){
        $apps=$this->findOne(array("uid"=>intval($uid)));

        $app=new AppMModel();
        if(!empty($apps['ids'])){

            return $app->find(array("_id"=>array('$in'=>MongoSysModel::toMongoIds($apps['ids']))));
        }


    }
    function deleteOwn($ids,$uid){
        $ownerId=$this->ownerId();

        $op=array('$pull'=>array('ids'=>$ids[0]));

        return parent::update($op,array($ownerId=>intval($uid)));

    }
    function deleteMany($ids,$uid){
        $ownerId=$this->ownerId();

        $op=array('$pull'=>array('ids'=>array('$in'=>$ids)));

        return parent::update($op,array($ownerId=>intval($uid)));

    }
    function exists($data){

        return  $this->findOne($data);

    }
    function update($obj,$arr){

        $op=array('$addToSet'=>array('ids'=>$obj['ids']));
        $ownerId=$this->ownerId();
        return parent::update($op,array($ownerId=>empty($arr[$ownerId])?intval($_SESSION['user'][$ownerId]):$arr[$ownerId]));

    }


}