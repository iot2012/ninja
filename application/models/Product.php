<?php
class ProductModel extends SysModel {


    static $_tbName  = 'product';
    static $_primary  = 'id';



    protected $_tbMeta=array (
  'id' => 
  array (
    'reg' => 'ui4',
    'desc' => 'id',
  ),
  'name' => 
  array (
    'lt' => '25',
    'reg' => 'varchar',
    'desc' => 'name',
  ),
  'desc' => 
  array (
    'lt' => '65535',
    'reg' => 'text',
    'desc' => 'desc',
  ),
  'packages' => 
  array (
    'lt' => '11',
    'reg' => 'varchar',
    'desc' => 'packages',
  ),
  'logo_front' => 
  array (
    'lt' => '100',
    'reg' => 'varchar',
    'desc' => 'logo_front',
  ),
  'logo_backend' => 
  array (
    'lt' => '100',
    'reg' => 'varchar',
    'desc' => 'logo_backend',
  ),
);

    function __construct(){

        parent::__construct();

    }

    function exists($data){

        return  $this->findOne($data);

    }


}