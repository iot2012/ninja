<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 11/28/14
 * Time: 10:42 AM
 */

class SubscribeMModel extends MongoSysModel {


    static $_tbName  = 'subscribe';



    protected $_tbMeta=array(
        '_id'=>array(
            'reg'=>'mongoid',
            'lt'=>'1024',
            'rights'=>4
        ),
        'title'=>array('reg'=>"title",'lt'=>'40'),
        'desc'=>array('reg'=>"desc",'lt'=>'1000'),
        /**
         * facebook=>{'send_msg':{'param1':'','param2':''}}
         *
         *
         *
         *
         */
        'topic'=>array('reg'=>"json",'lt'=>'10000'),
        'action'=>array('reg'=>"json"),
        'ctime'=>array('reg'=>"json")


    );



}