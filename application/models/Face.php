<?php
class FaceModel extends SysModel {


    static $_tbName  = 'face';
    static $_primary  = 'id';


    function getOne($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        $primary=$this->primary();
        return  $this->findOne(array($primary=>$data));

    }
    function updateSidByGroupId($groupId,$sid){

        return $this->update(array('sid'=>$sid),array('group_id'=>$groupId));

    }
    function getByUid($uid){
        return  $this->find(array('uid'=>$uid));

    }
    function updateFacePersonId($id,$faceId,$personId){
        $this->update(array('facepp_id'=>$faceId,'person_id'=>$personId),array('id'=>$id));
    }
    function addFace($uid,$username,$url){
        return $this->insert(

            array(
                "uid"=>$uid,
                "name"=>$username,
                "atime"=>time(),
                "url"=>$url

            )
        );

    }
    function updatePerson($uid,$name,$person_id){

        return $this->update(array('name'=>$name,'person_id'=>$person_id),array("uid"=>$uid));

    }
    function getPerson($uid){
        $tbName=$this->tbName();
        return  $this->execSql("select * from $tbName where uid=$uid && person_id!='' and group_id!='' union
select * from $tbName  where uid=? && person_id!='' limit 1",array($uid));
    }
    function updateGroup($id,$groupid,$groupName,$sid){
        return $this->update(array('group_id'=>$groupid,'group_name'=>$groupName,'sid'=>$sid),array("id"=>$id));
    }
    function updateSid($id,$sid,$providerName="facepp"){
        return $this->update(array('sid'=>$sid),array("id"=>$id));
    }
    function updateFaceid($id,$facepp_id,$providerName="facepp"){
        return $this->update(array(($providerName."_id")=>$facepp_id),array("id"=>$id));
    }
       function updatePersonId($id,$person_id,$providerName="facepp"){
           return $this->update(array("person_id"=>$person_id),array("id"=>$id));
       }
    function add($data){
        return $this->insert($data);

    }

}