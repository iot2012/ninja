<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 4/14/15
 * Time: 4:39 PM
 */


class FeedMModel extends MongoSysModel {


    static $_tbName = 'feed';


    protected $_tbMeta=array (
        '_id' =>
            array (
                'reg' => 'mongoid','rights'=>4

            ),
        'uid' =>
            array (
                'reg' => 'i8',
                'rights'=>4
            ),
        'title' =>
            array (
                'lt' => '100',
                'reg' => 'varchar',
            ),
        'desc' =>
            array (
                'lt' => '1024',
                'reg' => 'varchar',
            ),
        'cover'=>array(
            'lt' => '1024',
            'reg' => 'http',
            'rights'=>4

        ),
        'comments'=>array(

            'reg' => 'ignore',
            'rights'=>4

        ),
        'ctime' =>
            array (
                'reg' => 'i4',
                'rights'=>4
            ),
        'photo_ids' =>
            array (
                'reg' => 'json',
                'rights'=>4
            ),
        /**
         * 1:public  2:private 3:tags['group_a','group_b']
         */
        'privacy' =>
            array (
                'reg' => 'ignore',
                'enum'=>array('1'=>'public','2'=>'private')
            )
    );




}