<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 6/20/14
 * Time: 10:47 AM
 */

class IbeaconSensorModel extends SysModel {


    static $_tbName  = 'ibeacon_sensor';
    static $_primary  = 'id';

    protected $_tbMeta=array(
        'uid'=>array(
            'reg'=>'ui8',
            'lt'=>'1024'
        ),
        'air_sensor_mac'=>array('reg'=>"mac2"),
        'uid'=>array('reg'=>"ui8"),
        'uuid'=>array(
            'reg'=>'ui4',
        ),
        'major'=>array(
            'reg'=>'ui2'
        ),
        'minor'=>array(
            'reg'=>'ui2'
        ),
        'wechat_device_id'=>array(
            'reg'=>'ui4'
        ),
        'ctime'=>array('reg'=>"timestamp",'desc'=>'create time')
    );





}