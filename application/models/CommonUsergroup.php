<?php
class CommonUsergroupModel extends SysModel {


    static $_tbName  = 'common_usergroup';
    static $_primary  = 'groupid';



    function findGroup($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        $primary=$this->primary();
        return  $this->findOne(array($primary=>$data));

    }

}