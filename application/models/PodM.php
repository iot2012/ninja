<?php
class PodMModel extends MongoSysModel {


    static $_tbName = 'pod';
    static $_primary  = '_id';



    protected $_tbMeta=array (
  '_id' =>
  array (
    'reg' => 'mongoid',

  ),
  'uid' => 
  array (
    'reg' => 'i8',
  ),
  'name' => 
  array (
    'lt' => '100',
    'reg' => 'varchar',
  ),
  'icon_url' => 
  array (
    'lt' => '1024',
    'reg' => 'varchar',
  ),
  'url' => 
  array (
    'lt' => '1024',
    'reg' => 'varchar',
    'desc' => 'url',
  ),
  'atime' => 
  array (
    'reg' => 'i4',
  ),
  'status' => 
  array (
    'reg' => 'i1',
  ),
'cat_id' =>
    array (
        'reg' => 'json',
    ),
'tags' =>
    array (
        'reg' => 'json',
    ),
'content' =>
    array (
        'reg' => 'text',
    ),
    'pay_type'=>array('reg'=>'ui1','enum2'=>array('1'=>'per_day','2'=>'per_month','3'=>'per_quarter','4'=>'per_year'))
        

);


    function all(){
        $app=new PodMModel();

        $apps=$app->find();


       return array($apps,Misc_Utils::array_kvs($apps,'catid'));



    }
    function paramFilter($para){
        $arr=array();
        while (list ($key, $val) = each ($para)) {
            if($key == "sign" || $key == "sign_type" || substr($key,0,5) != "_item")continue;
            else {
                $arr[$key]=$val;
            }
        }
        return $arr;
    }
    function buildPriceKey($para) {

        ksort($para);
        reset($para);

        return http_build_query($para);

    }
    function buildKeys($params){

    }

    function getItemInfo($item_id,$param=array()){
        if(!empty($param)){
            $param=$this->paramFilter($param);
            if(count($param)==1){
                $priceKey=$param['_item_price'];
            }
            //findOne(array("price.month"=>232))
            $data=$this->findById($item_id,array('price','name','desc'));
            if(isset($data['price'][$priceKey]))
            {
                $data['price']=$data['price'][$priceKey];
            } else {
                return array();
            }
        } else {
            return array();
        }


        return $data;
    }
    function exists($data){

        return  $this->findOne($data);

    }


}