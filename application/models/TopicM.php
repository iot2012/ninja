<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 11/28/14
 * Time: 10:42 AM
 */

class TopicMModel extends MongoSysModel {


    static $_tbName  = 'topic';



    protected $_tbMeta=array(
        '_id'=>array(
            'reg'=>'mongoid',
            'lt'=>'1024',
            'rights'=>4
        ),
        'name'=>array('reg'=>"varchar",'lt'=>'40'),
        'icon_url'=>array('reg'=>"varchar",'lt'=>'10'),
        'keywords'=>array('reg'=>"varchar",'lt'=>'10000'),
        /**
         * [api=>{
               'method'=>'',
         *      'params'=>
         * }]
         *
         */
        'cat_name'=>array('reg'=>"varchar",'lt'=>'40'),
        'apis'=>array('reg'=>"json",'lt'=>'10000'),
        'ctime'=>array('reg'=>"timestamp")

    );
 


}