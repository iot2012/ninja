<?php
class RestApiCatModel extends SysModel {


    static $_tbName = 'rest_api_cat';
    static $_primary = 'id';



    protected $_tbMeta=array (
        'id' =>
            array (
                'reg' => 'i4',
                'desc' => '',
            ),
        'name' =>
            array (
                'lt' => '50',
                'reg' => 'varchar',
            ),
        'pid' =>
            array (
                'reg' => 'i4',
                'desc' => '',
            ),
    );

    function __construct(){

        parent::__construct();

    }
    function getCats(){
        $cats=$this->findAll();
        return Misc_Utils::array_kv($cats,'id');
    }
    function exists($data){

        return  $this->findOne($data);

    }


}