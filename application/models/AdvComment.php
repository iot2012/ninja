<?php
class AdvCommentModel extends SysModel {


    static $_tbName  = 'adv_comment';
    static $_primary  = 'id';



    protected $_tbMeta=array (
  'id' => 
  array (
    'reg' => 'ui8',
    'desc' => 'id',
  ),
  'uid' => 
  array (
    'reg' => 'ui8',
    'desc' => 'uid',
  ),
  'adv_id' => 
  array (
    'reg' => 'ui8',
    'desc' => 'adv_id',
  ),
  'authorid' => 
  array (
    'reg' => 'ui8',
    'desc' => 'authorid',
  ),
  'author' => 
  array (
    'lt' => '15',
    'reg' => 'varchar',
    'desc' => 'author',
  ),
  'ip' => 
  array (
    'reg' => 'i4',
    'desc' => 'ip',
  ),
  'dateline' => 
  array (
    'reg' => 'ui4',
    'desc' => 'dateline',
  ),
  'message' => 
  array (
    'lt' => '65535',
    'reg' => 'text',
    'desc' => 'message',
  ),
  'status' => 
  array (
    'reg' => 'i1',
    'desc' => 'status',
  ),
);

    function __construct(){

        parent::__construct();

    }

    function exists($data){

        return  $this->findOne($data);

    }


}