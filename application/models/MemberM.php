<?php


class MemberMModel extends MongoSysModel {


    static $_tbName  = 'member';
    static $_primary  = '_id';
    protected $_ownerId='uid';
    protected $_tbMeta=array(
        'username'=>array(
            'reg'=>'en_name',

        ),
        '_id'=>array('reg'=>'mongoid','rights'=>4),
        /**
         *
         */
        'level'=>array('reg'=>'ui1','type'=>'select','data_source'=>array('tb'=>'role','val'=>'id','field'=>'name','dataset'=>''),'rights'=>8),
        'age'=>array('reg'=>'ui1','type'=>'radio'),
        'gender'=>array('reg'=>'title'),
        'password'=>array('reg'=>"password",'rights'=>3),
        'email'=>array('reg'=>"email"),
        'realname'=>array('reg'=>"realname"),
        'mobile_number'=>array('reg'=>"cn_mobile"),
        'cn_id'=>array('reg'=>"cn_id"),
        'cn_zip'=>array('reg'=>"cn_zip"),
        'uid'=>array('reg'=>"ui8",'rights'=>4),
        'qq'=>array('reg'=>"qq"),
        "province"=>array('reg'=>'varchar','lt'=>'25'),
        "city"=>array('reg'=>'varchar','lt'=>'25'),
         "address"=>array('reg'=>'varchar','lt'=>'255'),
        "company"=>array('reg'=>'varchar','lt'=>'255'),
        "occupation"=>array('reg'=>'varchar','lt'=>'255'),

        /**
         * 1:白金 2:金卡 3:银 4:通卡
         */
        'level'=>array('reg'=>'varchar','lt'=>18,'rights'=>4),
        /**
         * 描述会员当前状态 status  1:正常 2:过期 ...
         */
        'bus_status'=>array('reg'=>"ui1",'rights'=>4)

    );

}
