<?php
class IbeaconCommentModel extends SysModel {


    static $_tbName  = 'ibeacon_comment';
    static $_primary  = 'id';



    protected $_tbMeta=array (
  'id' => 
  array (
    'reg' => 'ui4',
    'desc' => 'id',
  ),
  'content' => 
  array (
    'lt' => '1024',
    'reg' => 'varchar',
    'desc' => 'content',
  ),
  'ctime' => 
  array (
    'reg' => 'ui4',
    'desc' => 'ctime',
  ),
  'ibeacon_id' => 
  array (
    'reg' => 'ui8',
    'desc' => 'ibeacon_id',
  ),
  'ip' => 
  array (
    'reg' => 'ui4',
    'desc' => 'ip',
  ),
  'pic_url_1' => 
  array (
    'lt' => '100',
    'reg' => 'varchar',
    'desc' => 'pic_url_1',
  ),
);

    function __construct(){

        parent::__construct();

    }

    function exists($data){

        return  $this->findOne($data);

    }


}