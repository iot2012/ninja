<?php
use Zend\Db\TableGateway\TableGateway;
class NotificationModel extends SysModel {


    static $_tbName  = 'notification';
    static $_primary  = 'id';


    function updateMid($id,$mid,$uid){
        $this->update(array('mid'=>$mid),array('fromuid'=>$uid,'id'=>$id));
    }
    function deleteNotify($id)
    {
        return $this->delete(array("id"=>$id));
    }
    function clearNotify($uid){
        return $this->update(array('display'=>false),array("uid"=>$uid));
    }
    function addNotify($arr){


        return $this->insert(
            $arr
        );

    }
    function getAllNotify($uid='')
    {
        if($uid=='')
        {
            $uid=$_SESSION['user']['uid'];
        }
        $this->find(array('uid'=>$uid),40);

    }
    function getNewNotifyAndSetOld($uid='',$setOld=1){

        $notifies=$this->getNewNotify($uid);
        if(!empty($notifies) && $setOld=='1'){
            $ids=array_map(function($item){
                return $item['id'];
            },$notifies);
            $this->setNew($ids);
        }
        return $notifies;
    }
    function getNewNotify($uid='')
    {
        if($uid=='')
        {
            $uid=$_SESSION['user']['uid'];
        }
        if(!empty($uid))
        {
            $tbName=$this->tbName();
            $sql="select n.*,d.uids as uids,d.name as name,d.id as gid from $tbName
                            as n left join {$this->_pre}discuss_group as d on n.from_id=d.id group by n.maintype,n.subtype,n.uid,n.from_id having concat(n.maintype,'-',n.subtype)  in ('share-screen') and n.new=1 and n.uid=? and n.display=1 union select *,-1 as uids,-1 as name,-1 as gid
                            from v_notification  where display=1 and uid=? and concat(maintype,'-',subtype) not in ('share-screen') and new=1 order by `dateline` desc
          ";
            return $this->execSql($sql,array($uid,$uid));

        }
        return array();



    }
    function getUserMsg($myuid,$uid,$period=604800,$start=0,$cnt=20){

        $now=time();
        $sql="select n.roomid,n.maintype,n.subtype,n.uid,n.new,n.fromuid,n.note,n.dateline,n.id,n.from_id,n.from_idtype,n.fromuser as name,n.icon_url,n.mid,m.href as fileurl from {$this->_pre}{$this->_name} as n  left join {$this->_pre}media as m on n.mid=m.mid where n.maintype='chat'  and n.display=1 and ((n.fromuid=? and n.uid=?) or (n.fromuid=? and n.uid=?)) and (($now-n.dateline)<$period) and n.from_idtype='u' order by n.dateline desc limit $start,$cnt";
        return $this->execSql($sql,array($myuid,$uid,$uid,$myuid));
    }

    function getUserNewMsg($uid,$period=3024000){
        $now=time();
        $tbName=$this->tbName();
        $user=$this->execSql("select n.roomid,n.maintype,n.subtype,n.uid,n.new,n.fromuid,n.note,n.dateline,n.id,n.from_id,n.from_idtype,n.fromuser as name,
        n.icon_url,n.mid,m.href as fileurl,g.name  from $tbName as n  left join {$this->_pre}media as m on n.mid=m.mid
        left join {$this->_pre}discuss_group as g on n.from_id=g.id where n.maintype='chat' and n.new=1  and n.display=1
        and (($now-n.dateline)<?) and  n.from_idtype='g' and n.uid=? order by n.dateline desc",array($uid,$period));

        $group=$this->execSql("select n.roomid,n.maintype,n.subtype,n.uid,n.new,n.fromuid,n.note,n.dateline,n.id,n.from_id,n.from_idtype,n.fromuser as name
        ,n.icon_url,n.mid,m.href as fileurl from $tbName as n  left join {$this->_pre}media as m on n.mid=m.mid
        where n.maintype='chat' and n.new=1 and (($now-n.dateline)<?) and  n.from_idtype='u'  and n.display=1 and n.uid=? order by n.dateline desc",array($period,$uid));

        return array_merge($user,$group);
 //       $gids=array();
//        foreach($data as $k=>$v)
//        {
//            if($v['from_idtype']=='g')
//            {
//                $gids[]=$v['from_id'];
//            }
//        }
//        $gids="('".implode("','",$gids)."')";
//
//        $groups=$this->execSql("select *  from {$this->_pre}discuss_group where id in {$gids}");




    }
    function getGroupMsg($uid,$gid,$cnt=20,$period=604800,$start=0,$cnt=20){
        $now=time();
        /**
         * @todo get message by many times to prevent from getting too much chat data at a time
         */
        $sql="select n.roomid,n.maintype,n.subtype,n.uid,n.new,n.fromuid,n.note,n.dateline,n.id,n.from_id,
        n.from_idtype,n.fromuser,n.icon_url,n.mid,m.href as fileurl from {$this->_pre}{$this->_name} as n
          left join {$this->_pre}media as m on n.mid=m.mid where n.maintype='chat'  and n.display=1  and n.from_id=$gid
           and (($now-n.dateline)<$period) and n.from_idtype='g'  order by n.dateline desc limit $start,$cnt";

        return $this->execSql($sql);


    }
    function getNewMessage($uid,$num=1000)
    {
        return $this->find(array("uid"=>$uid,"new"=>1),$num);
    }



    function countNewNotify($uid='')
    {
        if($uid=='')
        {
            $uid=$_SESSION['user']['uid'];
        }
        //$this->execSql(array('uid'=>$uid,'new'=>1));

    }

    function getOneNotify($id=''){
        if(!empty($id))
        {
            return $this->findOne(array("id"=>$id));
        }

    }
    function setNew($id,$val=false){
        /**
         * @param $val is bit,should be assign false or true
         *
         */
        if(is_array($id)){
            $ids="id in ('".implode("','",$id)."')";
        } else {
            $ids="id in ('".str_replace("_","','",$id)."')";
        }

        return $this->update(array('new'=>$val),$ids);
    }




}