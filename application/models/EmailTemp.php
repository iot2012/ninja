
<?php


class EmailTempModel extends SysModel {


    static $_tbName  = 'email_temp';
    static $_primary  = 'id';

    /**
     * validate each field in tables
     * @todo store get  message in i18n file
     */

    /**
     *    'gt'=>'',
     *    'get'=>'',
     *    'let'=>'',
     * @var array
     */
    protected $_tbMeta=array(

    );


    function __construct(){

        parent::__construct();


    }

    function getTbName(){
        return $this->_name;
    }
    function getOne($tempName,$locale){


        return  $this->findOne(array('name'=>$tempName,'locale'=>$locale));

    }
    function updateTemp($id,$name,$conent){
        return $this->update(array("id"=>$id),array('name'=>$name,'content'=>$conent,'mtime'=>time()));
    }

    function add($data){
        return $this->insert($data);

    }

}