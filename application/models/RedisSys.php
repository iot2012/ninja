<?php


/**
 * Class SysModel
 * 定义了model的基本操作
 */
class RedisSysModel {

    /**
     * @var     redis
     */

    protected  $r;
    protected $_tbMeta=array();
    public function check($keys="",$dataSource='',$meta='',$requiredArr=''){

        if(empty($meta)){
            $meta=$this->getTbMeta();
        }
        if(empty($keys)){
            $keys=array_keys($this->getTbMeta());
        }
        if(!empty($meta)){
            return Validator::checkModel($keys,$meta,"redis",$dataSource,$requiredArr);
        }
        return true;
    }
    function getTbMeta()
    {
        return $this->_tbMeta;
    }
    public function checkRequired($requiredArr){

        return self::check("","",'',$requiredArr);
    }
     function __construct($redis='',$type='redis',$db=''){
        if(is_object($redis)){
            $this->r=$redis;

        } else {
            $redis=Yaf\Registry::get($type);
            $cfg=Yaf\Application::app()->getConfig();
            if(class_exists('Redis')){
                if(empty($redis)){
                    $redis=new Redis();
                    $ret=$redis->pconnect($cfg['redis']['host'], $cfg['redis']['port'], $cfg['redis']['timeout']);
                    if($ret && !empty($cfg['redis']['pwd'])){
                        $authed=$redis->auth($cfg['redis']['pwd']);
                        if($authed)
                        {

                            Yaf\Registry::set($type, $redis);

                        }

                    }
                }


            }
            $this->r=$redis;
        }


        return $redis;
    }

    function exists($key,$cols=array()){
       if(is_string($key)){
           if( count($cols)>0){
               return $this->r->hMGet($key,$cols);
           } else {
               return $this->r->get($key);
           }
       }

    }
    function isExist($key){
        return $this->r->exists($key);
    }


}