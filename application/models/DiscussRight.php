<?php



class DiscussRightModel extends SysModel {


    static $_tbName  = 'discuss_right';
    static $_primary  = 'id';


    function getByUidGid($uid,$gid){

        return $this->findOne(array('uid'=>$uid,'id'=>$gid));
    }
    function getById($id){
        $primary=$this->primary();
        return $this->findOne(array($primary=>$id));
    }
    function getByUid($uid){

        return $this->find(array('uid'=>$uid));
    }
    function getByGroupId($gid){

        return $this->find(array('groupid'=>$gid));
    }
    function updateById($arr,$uid)
    {
        $primary=$this->primary();
        return $this->update($arr,array($primary=>$uid));
    }
    function addSetting($arr)
    {
        return $this->insert($arr);

    }
    function updateByUidGid($data){
        return $this->replace($data);

    }


}