<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 1/24/14
 * Time: 5:00 PM
 */

class DiscussGroupModel extends SysModel {

    static $_tbName = 'discuss_group';
    protected  $_primary = 'id';


    function createGroup($uids,$name,$time,$startTime,$endTime,$place,$summary,$emails,$mobiles,$code='',$iv=''){

       $record=array(
            'uid'=>$_SESSION['user']['uid'],
            'uids'=>$uids,
            'name'=>$name,
            'dateline'=>$time,
            'starttime'=>$startTime,
           'endtime'=>$endTime,
           'place'=>$place,
           'added_emails'=>$emails,
           'added_mobiles'=>$mobiles,
           'summary'=>empty($summary)?'':$summary,
           'istemp'=>$_POST['istemp']==0?false:true

        );
        if(!empty($code)){
            $record['istemp']=true;
            $record['attend_code']=$code;
            $record['iv']=$iv;
        }
      return $this->insert($record);

    }
    function updateGroup($id,$uid,$data){

       return $this->update(
            $data,array('id'=>$id,'uid'=>$uid)
        );
    }
    function existsMeeting($data){
        $time=time();
        $ret=$this->findOne(array("id"=>$data['meeting']));
        if(!empty($ret)){
            $uid=$data['uid']+0;
            $match="/^{$uid}[_][^_]+|[^_]+_{$uid}_[^_]+|[^_]+_{$uid}$/";
            if(preg_match($match,$ret['uids'],$result) && $time<$ret['endtime'] && $data['mcode']==$ret['attend_code']) {
                return $ret;
            } else {
                return false;
            }
        }
    }
    function getGroupByUid($uid){

        $uid=intval($uid);
        $uid2="^{$uid}[_][^_]+|[^_]+_{$uid}_[^_]+|[^_]+_{$uid}$";
        $tbName=$this->tbName();
        $result=$this->execSql("select * from $tbName where uid=? or uids regexp '$uid2'",array($uid));
        $uidsArr=array();
        $gidArr=array();

        foreach($result as $key=>$val){

            $uidsArr[]=$val['uids'];
        }
        foreach($result as $key=>$val){

            $gidArr[]=$val['id'];
        }
        $uidsArr=array_unique(explode("_",implode("_",$uidsArr)));

        $whUidsArr="uid in ('".implode("','",$uidsArr)."')";

        $whGidsArr="groupid in ('".implode("','",$gidArr)."')";

        $user=new CommonUserModel();
        $right=new DiscussRightModel();
        /**
         * merge group right
         */
        $rights=$right->find($whGidsArr,500);
        if(empty($rights)){
            foreach($result as $key=>&$group)
            {
                $uids=explode('_',$group['uids']);
                array_map(function($item) use(&$group){

                    if($item==$group['uid'])
                    {
                        $group['rights'][$item]=array('en_group'=>1,'en_screen'=>1,'en_desk'=>1,'en_vid'=>1,'en_voice'=>1);
                    }
                    else
                    {
                        $group['rights'][$item]=array('en_group'=>1,'en_screen'=>0,'en_desk'=>0,'en_vid'=>0,'en_voice'=>0);
                    }




                },$uids);


            }

        }
        else
        {
            foreach($result as $key=>&$group)
            {
                $uids=explode('_',$group['uids']);
                $uids=array_unique($uids);
                foreach($rights as $key=>$right)
                {
                    if($right['groupid']==$group['id'])
                    {
                        $idx=array_search($right['uid'],$uids);
                        unset($uids[$idx]);
                        $group['rights'][$right['uid']]=$right;
                    }
                }
                foreach($uids as $key=>$uid)
                {
                    if($uid==$group['uid']){
                        $group['rights'][$uid]=array('en_group'=>1,'en_screen'=>1,'en_desk'=>1,'en_vid'=>1,'en_voice'=>1);
                    }
                    else
                    {
                        $group['rights'][$uid]=array('en_group'=>1,'en_screen'=>0,'en_desk'=>0,'en_vid'=>0,'en_voice'=>0);
                    }

                }
                if(!isset($group['rights'][$group['uid']]))
                {
                    $group['rights'][$group['uid']]=array('en_group'=>1,'en_screen'=>1,'en_desk'=>1,'en_vid'=>1,'en_voice'=>1);
                }
            }


        }

//        $result=array_map(function($item) use(&$right) {
//
//            $item['rights']=$right->find($item['id']);
//            $uids=explode("_",$item['uids']);
//            foreach($uids as $key=>$v){
//
//
//            }
//
//            return $item;
//
//        },$result);


        $userArr=Misc_Utils::getStatus($user->find($whUidsArr,1000,array('uid','username','realname','icon_url')));



        $userArr2=array();
        $arr=array_map(function($item) use(&$userArr2){$userArr2[$item['uid']]=$item;return '';},$userArr);
        /**
         * 数据格式
         * $uids=array(0=>1)
         * $userArr2=array(1=>'a')
         *
         */
        $result=array_map(function($item) use($userArr2){
            $uids=explode("_",$item['uids']);
            $item['users']=array_intersect_key($userArr2,array_flip($uids));
            return $item;
        },$result);
        /**
         *  result数据格式
         * [{"id":"1","uid":"4","uids":"3","dateline":"1390723843","name":"Cameron,Andy Liu","status":"0","users":{"3":{'username':'ab1'}}]
         */
        return $result;

    }
    function updateGroupName($uid,$id,$groupName){
        return $this->update(array('name'=>$groupName),array('uid'=>$uid,'id'=>$id));

    }
    function getGroupByGid($gid){

        return $this->findOne(array('id'=>$gid));
    }
    function getGroupByUidGid($uid,$gid){

        return $this->findOne(array('id'=>$gid,'uid'=>$uid));
    }

    function deleteGroup($id){

        return $this->delete(array('id'=>$id));
    }

}
