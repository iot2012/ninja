
<?php

class CalendarModel extends SysModel {


    static $_tbName  = 'calendar';
    static $_primary  = 'id';

    /**
     * validate each field in tables
     * @todo store get  message in i18n file
     */

    /**
     *    'gt'=>'',
     *    'get'=>'',
     *    'let'=>'',
     * @var array
     */
    protected $_tbMeta=array(
        'name'=>array(
            'reg'=>'file',
            'lt'=>'1024',
            'desc'=>'media url'
        ),
        'desc'=>array('reg'=>"ui8",'desc'=>'user id'),
        'ctime'=>array('reg'=>"timestamp",'desc'=>'create time'),
        'location'=>array('reg'=>"mime",'desc'=>'mime type'),
        'tz'=>array('reg'=>"ui1",'desc'=>"privacy")
    );
    protected $err=array('reg'=>array(


    ));


    function __construct(){

        parent::__construct();


    }
    function getTbName(){
        return $this->_name;
    }
    function getOne($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        $primary=$this->primary();
        return  $this->findOne(array($primary=>$data));

    }
    function getAll($uid){

        return $this->find(array('uid'=>$uid),20,'','id desc');
    }



}