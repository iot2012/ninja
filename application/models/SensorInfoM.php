<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 2/3/15
 * Time: 3:25 PM
 */

class SensorInfoMModel extends MongoSysModel {

    static $_tbName ="sensor_info";
    static $_db='ninja';
    protected  $_mongoChk=true;
    /**
     * 查询返回的列和排序,以及权限
     * @var array
     */
    protected $_modelRights=array('ryatek'=>array('4'=>
                                    array('cols'=>array('ctime','aqi','temp','humidity'),
                                    'order'=>array('ctime'=>-1))
    ));
    protected $_tbMeta=array(
       'major'=>array(
           'reg'=>'ui2'
       ),
       'sensor_type'=>array('reg'=>"ui1"),
       'sensor_mac'=>array('reg'=>"ignore"),
       'device_mac'=>array('reg'=>"ignore"),
       'guid'=>array('reg'=>"ignore"),
       'osinfo'=>array('reg'=>"varchar",'lt'=>"200"),
       'sensor_ver'=>array('reg'=>"ui1"),
       'minor'=>array('reg'=>"ui2"),
        /**
         * location 是保留字段?
         */
       'loc'=>array('reg'=>'ignore'),
       'aqi'=>array('reg'=>"ui2"),
       'temp'=>array('reg'=>"ui1"),
       'humidity'=>array('reg'=>"ui1"),
       'uuid'=>array('reg'=>"uuid"), 'ctime'=>array('reg'=>"timestamp",'rights'=>'4')

    );
    function beforePost($post,&$out){
      
    }
    function afterPost($post,&$out){
        $adv=new ContentServiceModel();
        $out['content']=$adv->fetch($post);
    }
    function beforeGet(&$inData,&$checkedResult){
        $ibeaconSensor=new IbeaconSensorModel();
        if(!empty($checkedResult['uuid']) && !empty($checkedResult['major']) && !empty($checkedResult['minor'])){
            $data=$ibeaconSensor->findOne(['uuid'=>strtoupper($checkedResult['uuid']),'major'=>$checkedResult['major'],'minor'=>$checkedResult['minor']]);
            if(!empty($data)){
                /*
                 * 环境卫士绑定的一个ibeacon
                 */
                unset($checkedResult['uuid'],$checkedResult['major'],$checkedResult['minor']);
                $checkedResult['sensor_mac']=$data['air_sensor_mac'];
            }
        }
    }
    function afterGet($inData,$checkedData,&$outData){



        $outData['ctime']=$outData['ctime']->sec;
        $outData['actionType']=0;
        $outData['message']='';
        unset($outData['_id']);
    }


} 