<?php
class CurlApiModel
{

    protected $_url;
    /**
     * @var     mapping table name without prefix
     */

    protected $_tbMeta;

    public function check($keys="",$dataSource='',$meta='',$requiredArr=''){

        if(empty($meta)){
            $meta=$this->getTbMeta();
        }
        if(empty($keys)){
            $keys=array_keys($this->getTbMeta());
        }
        if(!empty($meta)){
            return Validator::checkModel($keys,$meta,str_replace('Model','',__CLASS__),$dataSource,$requiredArr);
        }
        return true;
    }

    public function checkRequired($requiredArr){
        $meta=$this->getTbMeta();
        $keys=array_keys($this->getTbMeta());
        return Validator::checkModel($keys,$meta,str_replace('Model','',__CLASS__),'',$requiredArr);
    }

    function tbMeta(){
        return $this->_tbMeta;
    }
    function getTbMeta(){
        return $this->_tbMeta;
    }
}