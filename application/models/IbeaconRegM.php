
<?php

class IbeaconRegMModel extends MongoSysModel {


    static $_tbName  = 'ibeacon_reg';
    static $_primary  = '_id';

    /**
     * validate each field in tables
     * @todo store get  message in i18n file
     */

    /**
     *    'gt'=>'',
     *    'get'=>'',
     *    'let'=>'',
     * @var array
     */

    protected $_tbMeta=array(
        'alias'=>array('reg'=>"cn_en","let"=>"25"),
        'desc'=>array(
            'reg'=>'sm_desc'
        ),

        'bd_gps'=>array(
            'reg'=>'ignore','rights'=>8
        ),
        'address'=>array(
            'reg'=>'title'
        ),
        'uuid'=>array(
            'reg'=>'uuid',
            'eq'=>'36',
            'desc'=>'uuid',

        ),

        'air_sensor_mac'=>array('reg'=>"mac2",'rights'=>8),
        'shop_id'=>array('reg'=>"ui4",'rights'=>8),
        'wifi_ssid'=>array('reg'=>"en_name",'rights'=>8),
        'wifi_pwd'=>array('reg'=>"password",'rights'=>8),
        'service'=>array('reg'=>"en_name",'rights'=>8),
        'service_id'=>array('reg'=>"mongoid",'rights'=>8),

        'major'=>array('reg'=>"ui2",'desc'=>'major'),
        'minor'=>array('reg'=>"ui2",'desc'=>'minor'),
        'enter_min'=>array('reg'=>"uf4",'rights'=>8),
        'enter_max'=>array('reg'=>"uf4",'rights'=>8),
        'alias'=>array('reg'=>"cn_en","let"=>"25"),
        'uid'=>array('reg'=>"ui8",'rights'=>8),


    );

    function afterAdd($data,&$out){
        $out['major']=$data['major'];
        $out['uid']=$_SESSION['user']['uid'];
        if(!empty($data['shop_id'])){
            $this->addWifiDevice($data);
        }

    }
    function addWifiDevice($data){
        $cfg=$this->config();
        $options = $cfg->openid->wechat_open->gogo->toArray();
        $wc=new WechatOfficial($cfg);
        $info=new WechatOfficialInfoMModel();
        $info=$info->findOne(['uid'=>self::uid()],['authorizer_access_token']);
        if(!empty($info['authorizer_access_token'])){
            $wc->wifi_device_add($info['authorizer_access_token'],$data['shop_id'],$data['wifi_ssid'],$data['wifi_pwd']);
        }
    }
    function add($arr){
        if(!isset($arr['uuid'])){

            $arr['uuid']=$this->uuid=Yaf\Registry::get("config")->site->ib_uuid;
        }
        return $this->insert($arr);
    }




}