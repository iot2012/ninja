<?php
class UserProfileModel extends SysModel {


    static $_tbName  = 'user_profile';
    static $_primary  = 'uid';

    function getByUser($uid,$arr=array()){

        return $this->findOne(array('uid'=>$uid),$arr);
    }

    function getById($id){
        $primary=$this->primary();
        return $this->findOne(array($primary=>$id));
    }
    function getBuyerInfo($uid){
        return $this->findById($uid,array('country','receive_address','address','receive_zip','zipcode','mobile','telephone'));

    }
    function setBackendTour($uid,$data){
        $primary=$this->primary();
        return $this->update(array('backend_tour'=>$data),array($primary=>$uid));
    }
    function getBackendTour($uid){
        $primary=$this->primary();
        $data=$this->findOne(array($primary=>$uid),['backend_tour']);
        return $data['backend_tour'];
    }
    function getCommonProfile($uid){
        return $this->findById($uid,array('mfa','mfa_secret','wechat_qlogin','country','receive_address','address','receive_zip','zipcode','mobile','telephone'));
    }
    function updateById($arr,$uid,$isUpdate=true)
    {
        if($isUpdate==true)
        {
            $primary=$this->primary();
            return $this->update($arr,array($primary=>$uid));
        }
        else
        {
            $arr['uid']=$uid;
            return $this->replace($arr);
        }

    }

}