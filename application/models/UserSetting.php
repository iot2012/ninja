<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 1/9/14
 * Time: 11:03 AM
 */

class UserSettingModel extends SysModel {


    static $_tbName  = 'user_setting';
    static $_primary  = 'uid';

    protected $_tbMeta=array(
        'convstyle'=>array(
            'reg'=>'ui1'
        ),
        'enablesound'=>array('reg'=>"ui0"),
        'addmeflag'=>array('reg'=>"ui1"),
        'blockmsg'=>array('reg'=>"ui1"),
        'convhistory'=>array('reg'=>"ui0"),
        'mtime'=>array('reg'=>"ui4"),
        'en_desknotify'=>array('reg'=>"ui4"),
        'lang_prefer'=>array('reg'=>"/[a-zA-Z]{4}/"),
        'en_camlogin'=>array('reg'=>"ui0"),
        'send_key'=>array('reg'=>"ui0"),
        'levelid'=>array('reg'=>"ui1"),
        'is_viewed_guide'=>array('reg'=>"ui0")
    );


    function getByUser($uid){

        $tbName=$this->tbName();
        $ret=$this->execSql("select u.*,l.* from {$tbName}  u join {$this->_pre}user_auth  l on u.levelid=l.level
        where  u.uid=?",array($uid));
        if(!empty($ret) && !empty($ret[0])){
            return $this->filterOut($ret[0]);
        }
        return array();

    }
    function filterOut($arr){
        $arr['convhistory']= ($arr['convhistory']==true)?1:0;
        $arr['en_desknotify']= ($arr['en_desknotify']==true)?1:0;
        $arr['enablesound']= ($arr['enablesound']==true)?1:0;
        $arr['send_key']= ($arr['send_key']==true)?1:0;
        return $arr;
    }
    function filterIn($arr){

        $arr=array(
            'convhistory'=>($arr['convhistory']=='1')?true:false,'en_desknotify'=>($arr['en_desknotify']=='1')?true:false,
            'enablesound'=>($arr['enablesound']=='1')?true:false, 'send_key'=>($arr['send_key']=='1')?true:false

        );
        return $arr;

    }

    function getById($id){
        $primary=$this->primary();
        return $this->findOne(array($primary=>$id));
    }

    function updateById($arr,$uid)
    {
        $primary=$this->primary();
        $_SESSION['setting']=array_merge($_SESSION['setting'],$this->filterOut($arr));
        return $this->update($arr,array($primary=>$uid));
    }

    /**
     * add setting
     * @param $uid
     * @param $arr insert data
     * @return mixed inserted id
     */
    function addSetting($uid,$arr)
    {
        $primary=$this->primary();
        $_SESSION['setting']=array_merge($_SESSION['setting'],$this->filterOut($arr));
        return $this->insert(array_merge($this->filterIn($arr),array($primary=>$uid)));

    }





} 