<?php
class ResourceModel extends SysModel {


    static $_tbName = 'resource';



    protected $_tbMeta=array (
        'id' =>
            array (
                'reg' => 'ui2',
                'rights'=>4
            ),
  'title' => 
  array (
    'lt' => '25',
    'reg' => 'varchar',
  ),
  'uid' => 
  array (
    'reg' => 'ui8',
    'desc' => '',
  ),
  'type' => 
  array (
    'lt' => '15',
    'reg' => 'varchar','enum'=>array('t'=>'表','c'=>'访问路径')
  ),
  'name' => 
  array (
    'lt' => '35',
    'reg' => 'varchar',
  ),
  'desc' => 
  array (
    'lt' => '150',
    'reg' => 'varchar',
  ),
  'extra' => 
  array (
    'lt' => '11',
    'reg' => 'varchar',
  ),
);

    function __construct(){

        parent::__construct();

    }

    function exists($data){

        return  $this->findOne($data);

    }


}