<?php
class WechatUserCfgModel extends SysModel {


    static $_tbName  = 'wechat_user_cfg';
    static $_primary  = 'openid';

    function saveCfg($data){

        return $this->replace($data);

    }
    function findCfg($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        $primary=$this->primary();
        return  $this->findOne(array($primary=>$data));

    }
    function findRoom($openid)
    {
        $primary=$this->primary();
        return $this->findOne(array($primary>$openid));

    }
    function getCurRoom($openid){
        $primary=$this->primary();
        return $this->findOne(array($primary=>$openid),array('current_room'));

    }
    function appendRoom($openid,$newRoom){
        $rooms=$this->findRoom($openid);
        if(!empty($rooms))
        {
            $rooms=explode(",",$rooms['rooms']);
            $newRoom=implode(",",array_unique(array_merge($rooms,$newRoom)));
        }
       return  $this->update(array('rooms'=>$newRoom),array('openid'=>$openid));
    }
    function setCurentRoom($openid,$room){
        return  $this->update(array('rooms'=>$room),array('openid'=>$openid));


    }
    function appendBlockUsers($openid,$newBlockedUser){

        $rooms=$this->findRoom($openid);
        if(!empty($rooms))
        {
            $blockedUsers=explode(",",$rooms['blocked_users']);
            $newBlockedUser=implode(",",array_unique(array_merge($blockedUsers,$newBlockedUser)));
        }
        return  $this->update(array('blocked_users'=>$newBlockedUser),array('openid'=>$openid));

    }


}
