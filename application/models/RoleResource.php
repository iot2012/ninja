<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 8/11/14
 * Time: 10:11 AM
 */


class RoleResourceModel  extends SysModel {

    /**
     * use linux right model
     * 0: cant do any operation
     * 4: read
     * 2: write
     * 6:read/write
     * @var string
     *
     * res_name reg expression r is releation t is table  c is controller
     */
    protected $_actionModel=array(
        'read'=>4,
        'write'=>2,
        'none'=>0,
        'rw'=>6,
        'readwrite'=>6,
        'r'=>4,
        'w'=>2,
        'update'=>8,
        'u'=>8

    );

    protected $_rights = array();

    static $_tbName  = 'role_resource';
    static $_primary  = 'id';

        protected $_tbMeta=array(
            'id' =>
                array (
                    'reg' => 'ui2','rights'=>4
                ),
            'role_name'=>array(
                'reg'=>'cn_en',
                'lt'=>'25',

            ),

            'res_name'=>array('reg'=>"res_name"),
            'rights'=>array('reg'=>"ui1"),
            'extra'=>array('reg'=>"phpjson")
        );

    /**
     * @param $level   user level
     * @param $resName [t_tableName|r_relationName|c_controller/action]
     * @param $action
     * @return bool
     */
    public function canDo($level,$resName,$action='read'){
        return true;
        $action=$this->formatRight($action);
        if($level==9){
            return true;
        }
        else {

            $this->rights=$this->getByRoleRes($level,$resName);

            if(!empty($this->rights)){
                    if($this->rights['rights'] & $action  == $action){
                        return true;
                    }


            }
            return false;
        }

    }

    /**
     * 格式化action成标准的权限
     * @param $action
     * @return int
     */
    public function formatRight($action){
        if(filter_var($action,FILTER_VALIDATE_INT)){
            return intval($action);
        }
        $actions=preg_split("/[\s,;]/i",$action);
        $actions=array_unique($actions);
        $val=0;

        foreach($actions as $item){

                if($item=='read'){
                    $val+=4;
                    continue;
                }
                else if($item=='write'){
                    $val+=2;
                }
                else if($item=='rw'){
                    /**
                     * prevent when using right format like rw,read,write
                     */
                    if($val & 2==2){$val=-2;}
                    if($val & 4==4){$val=-4;}
                    $val+=6;
                }

        }
        return $val;
    }
    function getByRoleId($roleId){
        $tbName=$this->tbName();

        return $this->execSql("select r.type,r.title,rr.* from $tbName rr join {$this->_pre}resource r on r.name=rr.res_name where rr.role_id=?",array($roleId));
    }
    function getById($id){
        $primary=$this->primary();
        return $this->findOne(array($primary=>$id));
    }
    function getByRoleRes($role,$res){
       return $this->findAll(array('role_id'=>$role,'resource'=>$res));
    }

    function updateById($arr,$uid)
    {

        $primary=$this->primary();
        return $this->update($arr,array($primary=>$uid));
    }
    function add($arr)
    {
        return $this->insert($arr);

    }




}