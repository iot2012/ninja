<?php
class MyPodMModel extends MongoSysModel {


    static $_tbName = 'my_pod';



    protected $_tbMeta=array (
          'uid' =>
          array (
            'reg' => 'i8',
            'desc' => 'uid',
          ),
          'ids' =>
          array (
            'reg' => 'ignore',
          ),
          'name' =>
          array (
            'lt' => '60',
            'reg' => 'varchar',
          ),
          'atime' =>
          array (
            'reg' => 'i4',
          ),
          'status' =>
          array (
            'reg' => 'i1'
          ),
);


    function myPods($uid){
        $apps=$this->findOne(array("uid"=>intval($uid)));

        $app=new PodMModel();
        if(!empty($apps['ids'])){

            return $app->find(array("_id"=>array('$in'=>MongoSysModel::toMongoIds($apps['ids']))));
        }


    }
    function deleteOwn($ids,$uid){
        $ownerId=$this->ownerId();

        $op=array('$pull'=>array('ids'=>$ids));

        return parent::update($op,array($ownerId=>intval($uid)));

    }
    function deleteMany($ids,$uid){
        $ownerId=$this->ownerId();

        $op=array('$pull'=>array('ids'=>array('$in'=>$ids)));

        return parent::update($op,array($ownerId=>intval($uid)));

    }
    function exists($data){

        return  $this->findOne($data);

    }
    function update($obj,$query){

        $op=array('$addToSet'=>array('ids'=>$obj['ids']),'$set'=>array('uid'=>intval($query['uid'])));
        $ownerId=$this->ownerId();
        $result=parent::update($op,array($ownerId=>intval($query['uid'])));

        return $result;



    }


}