<?php
class ShopClsModel extends SysModel {


    static $_tbName = 'shop_cls';
    static $_primary = 'id';



    protected $_tbMeta=array (
  'id' => 
  array (
    'reg' => 'ui3','rights'=>4
  ),
        'pid' =>
            array (
                'reg' => 'i4','type'=>'tree-select','data_source'=>array('tb'=>'ShopCls','val'=>'id','field'=>'name'),'data_extra'=>'data-tree=radio'
            ),
  'name' => 
  array (
    'lt' => '30',
    'reg' => 'cn_en',
  ),
  'ctime' => 
  array (
    'reg' => 'timestamp','rights'=>4
  ),
  'uid' => 
  array (
    'reg' => 'i8','rights'=>4
  ),
);

    function __construct(){

        parent::__construct();

    }

    function exists($data){

        return  $this->findOne($data);

    }


}