<?php
class RoleModel extends SysModel {


    static $_tbName = 'role';
    static $_primary  = 'id';



    protected $_tbMeta=array (

        'id' =>
            array (
                'reg' => 'ui2','rights'=>4
            ),
  'name' => 
  array (
    'lt' => '20',
    'reg' => 'varchar',
  ),
  'desc' => 
  array (
    'lt' => '50',
    'reg' => 'varchar',
  ),
);

    function __construct(){

        parent::__construct();

    }

    function exists($data){

        return  $this->findOne($data);

    }


}