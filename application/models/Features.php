<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 6/20/14
 * Time: 10:46 AM
 */



class FeaturesModel extends SysModel {


    static $_tbName  = 'features';
    static $_primary  = 'id';


    function getById($id){
        $primary=$this->primary();
        return $this->findOne(array($primary=>$id));
    }

    function updateById($arr,$uid)
    {
        $primary=$this->primary();
        return $this->update($arr,array($primary=>$uid));
    }
    function add($arr)
    {
       $array=<<<EOF
        1#用户能否创建视频人数,创建桌面分享人数,无法组视频###1
        2
EOF;

        return $this->insert($arr);

    }


}