
<?php


class ApnTokenModel extends SysModel {


    static $_tbName  = 'apn_token';
    static $_primary  = 'device_token';

    /**
     * validate each field in tables
     * @todo store get  message in i18n file
     */

    /**
     *    'gt'=>'',
     *    'get'=>'',
     *    'let'=>'',
     * @var array
     */
    protected $_tbMeta=array(

    );


    function __construct(){

        parent::__construct();


    }
    function getTbName(){
        return $this->_name;
    }
    function getOneByToken($token){


        return  $this->findOne(array('device_token'=>$token));

    }




}