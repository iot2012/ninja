<?php
class AirInfoMModel extends MongoSysModel {

    static $_tbName ="air_info";
    static $_db='ninja';
    protected  $_mongoChk=true;
    /**
     * 查询返回的列和排序,以及权限
     * @var array
     */
    protected $_modelRights=array('ryatek'=>array('4'=>
        array('cols'=>array('pm25','air'),
            'order'=>array('ctime'=>-1))
    ));
    protected $_tbMeta=array(
        'city_cn'=>array(
            'reg'=>'varchar','lt'=>'21'
        ),
        'city_en'=>array('reg'=>"varchar",'lt'=>'21'),
        'weoid'=>array('reg'=>"\d{7}"),
        'air'=>array('reg'=>"ignore"),
        'pm25'=>array('reg'=>"ignore"),
        'ctime'=>array('reg'=>'timestamp')
    );
    function beforeGet(&$inData){
        if(array_key_exists('city_cn',$inData)){
            $inData['city_cn']=urldecode($inData['city_cn']);

        }
    }

    function afterGet($inData,$checkedData,&$outData){
        parent::afterGet($outData);
        $outData['ctime']=$outData['ctime']->sec;

    }


}