<?php

/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 11/28/14
 * Time: 10:42 AM
 */

class WechatUserMModel extends MongoSysModel
{


    static $_tbName = 'wechat_user';

    protected $_tbMeta=array (
        'uid' =>
            array (
                'reg' => 'i8',
                'desc' => 'uid',
            ),
        'openid' =>
            array (
                'lt' => '50',
                'reg' => 'varchar',
                'desc' => 'openid',
            ),
        'sex' =>
            array (
                'reg' => 'i1',
                'desc' => 'sex',
            ),
        'city' =>
            array (
                'lt' => '30',
                'reg' => 'varchar',
                'desc' => 'city',
            ),
        'country' =>
            array (
                'lt' => '30',
                'reg' => 'varchar',
                'desc' => 'country',
            ),
        'nickname' =>
            array (
                'lt' => '40',
                'reg' => 'varchar',
                'desc' => 'nickname',
            ),
        'language' =>
            array (
                'lt' => '20',
                'reg' => 'varchar',
                'desc' => 'language',
            ),
        'headimgurl' =>
            array (
                'lt' => '1000',
                'reg' => 'varchar',
                'desc' => 'headimgurl',
            ),
        'subscribe_time' =>
            array (
                'reg' => 'i4',
                'desc' => 'subscribe_time',
            ),
        'province' =>
            array (
                'lt' => '30',
                'reg' => 'varchar',
                'desc' => 'province',
            ),
        'subscribe' =>
            array (
                'reg' => 'i0',
                'desc' => 'subscribe',
            ),
        'remark' =>
            array (
                'lt' => '1024',
                'reg' => 'varchar',
                'desc' => 'remark',
            ),
        'privilege' =>
            array (
                'lt' => '11',
                'reg' => 'varchar',
                'desc' => 'privilege',
            ),
    );

}