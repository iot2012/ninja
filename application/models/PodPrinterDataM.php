<?php
class PodPrinterDataMModel extends MongoSysModel {


    static $_tbName = 'pod_printer_data';

    //remove import action
    //配置操作的选项,这里配置了print打印
    static $_opAction=array(
        'crud'=>array('import'=>['en'=>false],'edit'=>['en'=>false]),
        'extra'=>array('print'=>
            array(
            'icon'=>'fa-print',
            'title'=>'start_print'
            ))
    );

    protected $_tbMeta=array (
        '_id'=>array('reg'=>'mongoid','rights'=>8),
        'uid'=>array('reg'=>'ui8','rights'=>8),
        'router'=>array('reg'=>'mac2','type'=>'select','desc'=>'router_mac','data_source'=>array('tb'=>'routerM','val'=>'mac','field'=>'name','dataset'=>''),'rights'=>8),

        'mid'=>array (
            'reg' => 'file',
            'desc'=>'upload_file',
            'data_extra'=>'data-role=upload',
            'map'=>array('tb'=>'media','show_field'=>'href','ref_field'=>'mid')

        ),
        'status'=>array('desc'=>'printer_status',"reg"=>'ui1','enum'=>array('1'=>'printing','2'=>'waiting','3'=>'canceled','4'=>'uploaded'),'rights'=>4),
        'print_time'=>array("reg"=>'timestamp','rights'=>4)
    );
    function beforeAdd(&$indata){


        return $indata;
    }
    function afterAdd($inData,&$outData){
        $inData['code']=1;
        $inData['msg']="";
        $outData['uid']=$_SESSION['user']['uid'];




        return $inData;
    }


}