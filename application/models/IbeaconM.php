
<?php

class IbeaconMModel extends MongoSysModel {


    static $_tbName  = 'ibeacon';
    static $_primary  = '_id';

    /**
     * validate each field in tables
     * @todo store get  message in i18n file
     */

    /**
     *    'gt'=>'',
     *    'get'=>'',
     *    'let'=>'',
     * @var array
     */

    protected $_tbMeta=array(
        'alias'=>array('reg'=>"cn_en","let"=>"25"),
        'desc'=>array(
            'reg'=>'sm_desc'
        ),

        'bd_gps'=>array(
            'reg'=>'ignore','rights'=>8
        ),
        'address'=>array(
            'reg'=>'title'
        ),
        'uuid'=>array(
            'reg'=>'uuid',
            'eq'=>'36',
            'desc'=>'uuid',

        ),
        'air_sensor_mac'=>array('reg'=>"mac2",'rights'=>8),
        'shop_id'=>array('reg'=>"ui4",'rights'=>8),
        'wifi_ssid'=>array('reg'=>"en_name",'rights'=>8),
        'wifi_pwd'=>array('reg'=>"password",'rights'=>8),
        'service'=>array('reg'=>"en_name",'rights'=>8),
        'service_id'=>array('reg'=>"mongoid",'rights'=>8),

        'major'=>array('reg'=>"ui2",'desc'=>'major'),
        'minor'=>array('reg'=>"ui2",'desc'=>'minor'),
        'enter_min'=>array('reg'=>"uf4",'rights'=>8),
        'enter_max'=>array('reg'=>"uf4",'rights'=>8),
        'alias'=>array('reg'=>"cn_en","let"=>"25"),
        'uid'=>array('reg'=>"ui8",'rights'=>8),


    );
    protected $err=array('reg'=>array());

    //getAllIbeacons
    function ibeaconCount($uid){

        return $this->total(['uid'=>$uid]);
    }
    function in_wechat_times($uid){

        $m=new WechatLocMModel();

        return $m->total(['uid'=>$uid]);
    }



    function __construct(){

        parent::__construct();


    }


    function getCurrentAdv($uuid,$major,$minor,$lang="zhcn"){

        return $this->execSql("select a.* from {$this->_pre}adv as a
                                join {$this->_pre}ibeacon_adv as ai on a.id=ai.adv_id
                                join {$this->_pre}ibeacon as i on ai.ib_id=i.id

                                where (a.lang='$lang' or a.lang='all') and
                                (i.uuid=? and i.major=? and i.minor=?)
                                order by ai.ctime  desc",
            array($uuid,$major,$minor));



    }
    function getConfig($uuid,$major){

        $result=$this->find(array('uuid'=>$uuid,'major'=>$major),0,array("major","minor","enter_min","enter_max","exit",$this->primary(),"trigger_time"));
        $ids=array();
        $result2=array();
        $ids=array_map(function($item)use(&$ids,&$result2){
            $result2[$item[$this->primary()]]=$item;
            $result2[$item[$this->primary()]]['lang']=array();
            return $item[$this->primary()];
        },$result);

        $ids="('".implode("','",$ids)."')";

        $langs=$this->execSql("select ib_id,lang from {$this->_pre}ibeacon_adv where ib_id in $ids and  lang!='all' group by ib_id,lang");

        array_map(function($item) use(&$result2){
            if(isset($result2[$item['ib_id']])){
                $result2[$item['ib_id']]['lang'][]=$item['lang'];
            }
        },$langs);
        return array_values($result2);

    }
    function getOne($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        $primary=$this->primary();
        return  $this->findOne(array($primary=>$data));

    }
    function beforeAdd($arr,$user){

        if(!($arr['uid']>0)) {

            $arr['uid']=$user['uid'];
        }
        if(!empty($arr['bd_gps'])){

            $arr['bd_gps']=array_filter(explode(",",$arr['bd_gps']),"doubleval");

        }
        if($arr['uuid']!='88888888-4444-4444-4444-CCCCCCCCCCCC'){
            $model=new IbeaconAdmModel();
            $result=$model->findOne(array(
                'uuid'=>$arr['uuid'],'major'=>$arr['major'],'minor'=>$arr['minor'],'username'=>$user['username']
            ));
            if(empty($result)){
                return array('z_op_result'=>array('code'=>-1,'msg'=>'no_perm_add'));
            }
        }

        return $arr;
    }
    function afterAdd($data,&$out){
        $out['major']=$data['major'];
        $out['uid']=$_SESSION['user']['uid'];
        if(!empty($data['shop_id'])){
            $this->addWifiDevice($data);
        }

    }
    function addWifiDevice($data){
        $cfg=$this->config();
        $options = $cfg->openid->wechat_open->gogo->toArray();
        $wc=new WechatOfficial($cfg);
        $info=new WechatOfficialInfoMModel();
        $info=$info->findOne(['uid'=>self::uid()],['authorizer_access_token']);
        if(!empty($info['authorizer_access_token'])){
            $wc->wifi_device_add($info['authorizer_access_token'],$data['shop_id'],$data['wifi_ssid'],$data['wifi_pwd']);
        }
    }
    function add($arr){
        if(!isset($arr['uuid'])){

            $arr['uuid']=$this->uuid=Yaf\Registry::get("config")->site->ib_uuid;
        }
        return $this->insert($arr);
    }




}