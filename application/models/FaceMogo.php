<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 6/10/14
 * Time: 11:02 AM
 */

class FaceModel extends MongoSysModel {



    static $_tbName  = 'face';
    static $_primary  = 'id';


    function getOne($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        $primary=$this->primary();
        return  $this->findOne(array($primary=>$data));

    }

    function getByUid($uid){
        return  $this->find(array('uid'=>$uid));

    }
    function addFace($uid,$username,$data){
      return $this->insert($uid,$username,$data);

    }
    function getPersonId($uid){
        return  $this->findOne("uid=$uid and person_id!=''");
    }
    function updateGroupId($id,$groupid){
        return $this->update(array('group_id'=>$groupid),array("id"=>$id));
    }
    function updateSid($id,$sid,$providerName="facepp"){
        return $this->update(array('sid'=>$sid),array("id"=>$id));
    }
    function updateFaceid($id,$facepp_id,$providerName="facepp"){
        return $this->update(array(($providerName."_id")=>$facepp_id),array("id"=>$id));
    }
    function updatePersonId($id,$person_id,$providerName="facepp"){
        return $this->update(array("person_id"=>$person_id),array("id"=>$id));
    }
    function add($data){
        return $this->insert($data);

    }


} 