<?php
class PageCompMModel extends MongoSysModel {


    static $_tbName  = 'page_comp';
    static $_primary  = '_id';
    static $_chkOwn=false;


    protected $_tbMeta=array (
        '_id' =>
            array (
                'reg' => 'mongoid',
                'desc' => 'mongoid',"rights"=>4
            ),


        'icon_url' =>
        array (
            'lt' => '512',
            'reg' => 'icon_url',
            'desc' => 'icon_url',
        ),
        'name' =>
            array (
                'lt' => '30',
                'reg' => 'en_name',
                'desc' => 'temp_name',
            ),
        'html' =>
            array (
                'lt' => '16777215',
                'reg' => 'html',
                'desc' => 'content'),
        'cfg_html' =>
            array (
                'lt' => '16777215',
                'reg' => 'html',
                'desc' => 'content'
            ),
        'cat'=>array (
                'lt' => '512',
                'reg' => 'en_name',
                'desc' => 'category',
            ),    
        'ctime' =>
            array (
                'reg' => 'timestamp',
                'desc' => 'ctime',"rights"=>4
            ),

        'uid' =>
            array (
                'reg' => 'i8',
                'desc' => 'uid',"rights"=>4
            )


    );

    function __construct(){

        parent::__construct();

    }



    function getOne($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        $primary=$this->primary();
        return  $this->findOne(array($primary=>$data));

    }






}