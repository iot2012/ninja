
<?php

class IbeaconAdmModel extends SysModel {


    static $_tbName  = 'ibeacon_adm';
    static $_primary  = 'id';

    /**
     * validate each field in tables
     * @todo store get  message in i18n file
     */

    /**
     *    'gt'=>'',
     *    'get'=>'',
     *    'let'=>'',
     * @var array
     */
    static  $_indexes=array(
        'uuid','major','minor','username'
    );
    protected $_tbMeta=array(
        'uuid'=>array(
            'reg'=>'uuid',
            'eq'=>'36',
   
        ),
        'major'=>array('reg'=>"ui2"),
        'minor'=>array('reg'=>"ui2"),
        'username'=>array('reg'=>"username",'desc'=>'assigned_to'),
        'uid'=>array('reg'=>"ui8",'rights'=>8)


    );
    protected $err=array('reg'=>array());


    function __construct(){

        parent::__construct();


    }



}