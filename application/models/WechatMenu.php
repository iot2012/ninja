<?php


class WechatMenuModel extends SysModel {


    static $_tbName = 'wechat_menu';
    static $_primary = 'id';


    protected $_tbMeta=array (
  'id' => 
  array (
    'reg' => 'ui4',
      'rights'=>4

  ),
  'name' => 
  array (
    'lt' => '40',
    'reg' => 'mid_title',
      'desc'=>'menu_name'
  ),
  'type' => 
  array (
    'reg' => 'ui1',
      'desc'=>'menu_type',
      'enum'=>array(1=>'click',2=>'url',0=>'default')
  ),
  'url' => 
  array (
    'lt' => '256',
    'reg' => 'http',

  ),
  'parent' => 
  array (
    'reg' => 'ui1',
    'desc' => 'parent_menu_id',
  ),
  'key' => 
  array (
    'lt' => '128',
    'reg' => 'varchar',
  ),
  'seq' => 
  array (
    'reg' => 'ui1',
    'desc' => 'pos_seq',
  ),
  'hassub' => 
  array (
    'reg' => 'i0',
      'enum'=>array(1=>'has_submenu',0=>'no_submenu')

  ),
);

    function __construct(){

        parent::__construct();

    }

    protected $menus=array(
        '2'=>'view',
        '1'=>'click'
    );



    function getMenus($cols='',$officialName="gh_3caf6c97a9dc"){

        return $this->find(array('official_name'=>$officialName),100,$cols);

    }
    function getWechatMenus($lang='',$toUsername="gh_3caf6c97a9dc",$_n="ninja"){

        $menus=$this->getMenus(array('name','key','url','type','parent','id'),$toUsername);

        $menuData=array();
        foreach($menus as $key=>$menuItem){
            if($lang!==''){
                $menuItem['name']=$this->formatMsg($lang,$menuItem['name']);
            }
            $queryArr=['_n'=>$_n,'_to'=>$toUsername];
            if(!empty($_n)){
                $queryArr=['_n'=>$_n];
            }
            $query=http_build_query($queryArr);
            $menuItem['url']=(strpos($menuItem['url'],"?")===false)?($menuItem['url']."?".$query):($menuItem['url']."&".$query);
            $menuData['items'][$menuItem['id']]=$menuItem;
            $menuData['parents'][$menuItem['parent']][] = $menuItem['id'];
        }
        return $menuData;
    }
    function buildMenu($parentId, $menuData,&$html,$userInfo)
    {

        if (isset($menuData['parents'][$parentId]))
        {
            foreach ($menuData['parents'][$parentId] as $itemId)
            {
                if(strtolower($menuData['items'][$itemId]['name'])=='fogpod'){
                    $menuData['items'][$itemId]['url']=$menuData['items'][$itemId]['url']."?wid={$userInfo['openid']}&uid={$userInfo['uid']}";
                }
                if($menuData['items'][$itemId]['type']!=0){
                    unset($menuData['items'][$itemId]['hassub'],$menuData['items'][$itemId]['seq'],$menuData['items'][$itemId]['id'],$menuData['items'][$itemId]['parent']);

                    $menuData['items'][$itemId]['type']=$this->menus[$menuData['items'][$itemId]['type']];
                    $html[]=$menuData['items'][$itemId];
                }
                else {
                    $cnt=array_push($html,array('sub_button'=>array(),'name'=>$menuData['items'][$itemId]['name']));
                    $this->buildMenu($itemId, $menuData,$html[$cnt-1]['sub_button'],$userInfo);

                }

            }

        }

        return $html;
    }



}