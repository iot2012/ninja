<?php
class MenuModel extends SysModel {


    static $_tbName  = 'menu';
    static $_primary  = 'id';



    protected $_tbMeta=array (
  'id' => 
  array (
    'reg' => 'ui4',
    'desc' => 'id',
  ),
  'product_id' => 
  array (
    'reg' => 'ui2',
    'desc' => 'product_id',
  ),
  'icon' => 
  array (
    'lt' => '100',
    'reg' => 'varchar',
    'desc' => 'icon',
  ),
  'name' => 
  array (
    'lt' => '30',
    'reg' => 'varchar',
    'desc' => 'name',
  ),
  'type' => 
  array (
    'reg' => 'ui1',
    'desc' => 'type',
  ),
  'href' => 
  array (
    'lt' => '100',
    'reg' => 'varchar',
    'desc' => 'href',
  ),
);

    function __construct(){

        parent::__construct();

    }

    function exists($data){

        return  $this->findOne($data);

    }


}