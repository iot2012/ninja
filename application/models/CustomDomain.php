<?php
class CustomDomainModel extends SysModel {


    static $_tbName = 'custom_domain';
    static $_primary = 'id';
    protected $_ownerId="username";
    protected $_tbMeta=array (
        'username'=>array('reg'=>'username'),
        'custom_domain'=>array('reg'=>'domain')
);

    function __construct(){

        parent::__construct();

    }
    function beforeAdd(&$data){
        $data['username']=$_SESSION['user']['username'];
        unset($data['uid']);
        return $data;
    }
    function exists($data){

        return  $this->findOne($data);

    }


}