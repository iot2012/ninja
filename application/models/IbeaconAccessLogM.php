<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 1/4/15
 * Time: 7:08 PM
 */




class IbeaconAccessLogMModel extends MongoSysModel {

    static $_tbName ="ibeacon_accesslog";

    protected $_tbMeta=array(
        '_id'=>array(
            'reg'=>'mongoid',
            'eq'=>'24',
            'rights'=>4
        ),
        'uuid'=>array(
            'reg'=>'uuid',
            'eq'=>'36',
            'desc'=>'uuid',
            'rights'=>4
        ),
        'uid'=>array('reg'=>'mongoid','rights'=>4),
        'major'=>array('reg'=>"ui2",'rights'=>4),
        'minor'=>array('reg'=>"ui2"),
        'atime'=>array('reg'=>"timestamp",'rights'=>4),

    );
    protected $uuid;
    function __construct($uuid=''){
        parent::__construct();
        $this->uuid=$uuid;
        if($this->uuid==''){
            $this->uuid=Yaf\Registry::get("config")->site->ib_uuid;
        }


    }
    function ibeaconNum($majors){
        $ib=new IbeaconModel();
        $majors="('".implode("','",$majors)."')";
        return $ib->total("major in $majors");

    }
    function vistorNum($majors){
        $redis=Yaf\Registry::get("redis");
        return $redis->hget("ib.total","$majors");

    }

    function vistorNum2($majors){

        $uids=$this->r->aggregate(
            array('$match' => array('major'=>$majors)),
            array('$group' => array(
                '_id'=>array('year'=>array('$year'=>'$ctime'),'month'=>array('$month'=>'$ctime'))
            )));

        return $uids;

    }
    function regUsers($majors,$start=0,$limit=100,$cols=array()){

        $uids=$this->r->aggregate(
            array('$match' => array('uid'=>array('$exists'=>true),'major'=>$majors)),
            array('$group' => array(
                '_id'=>array("uid"=>'$uid')
            )));
        if($uids['ok']==1 && !empty($uids['result'])){
           $uids2=array_map(function($uid){
                return preg_match("/[0-9a-z]{24}/",$uid['_id']['uid'])?new MongoId($uid['_id']['uid']):'';
            },$uids['result']);
            $uids2=array_filter($uids2,'trim');

            $users=new SnsUserMModel();
            return $users->pageFind(array('_id'=>array('$in'=>array_values($uids2))),array(),'',$start,$limit);
        }
        return array();

    }
    function regUserNum($majors){

        $total=$this->total2(
            array('_id'=>'$uid','count'=>array('$sum'=>1)),
            array(
            //"uuid"=>$this->uuid,
            'major'=>array(
                '$in'=>is_array($majors)?$majors:array($majors)
            ),
            'uid'=>array('$exists'=>true,'$ne'=>'')
        )

        );
        if($total['ok']==1){
            return count($total['result']);
        }
        return false;
    }
    function beforeView(&$data){

        $m=new CompanyModel();
        $ownerId=$this->ownerId();
        $id=$m->findOne(array(
            $ownerId=>$data[$ownerId]
        ),array('id','status'));
        /**
         * 有效的状态
         */
        if($id['status']>=4){
            $data=array(
                'major'=>($id['id'].'')
            );
        } else {
            $data='';
        }

    }
    function findOwner(){
        
    }
    function todayRegUserNum($majors){
        $today  = new MongoDate(mktime(0, 0, 0, date("m")  , date("d"), date("Y")));
        $tomorrow  = new MongoDate(mktime(0, 0, 0, date("m")  , date("d")+1, date("Y")));
        $total=$this->total2(

            array('_id'=>'$uid','count'=>array('$sum'=>1)),
            array(
                //"uuid"=>$this->uuid,
                'major'=>array(
                    '$in'=>is_array($majors)?$majors:array($majors)
                ),
                'uid'=>array('$exists'=>true),
                "atime"=>array('$gte'=>$today,'$lt'=>$tomorrow)
            )

        );
        if($total['ok']==1){
            return count($total['result']);
        }
        return false;
    }
    function todayVisitorNum($majors){
        $today  = new MongoDate(mktime(0, 0, 0, date("m")  , date("d"), date("Y")));
        $tomorrow  = new MongoDate(mktime(0, 0, 0, date("m")  , date("d")+1, date("Y")));
        return $this->total(array(
            //"uuid"=>$this->uuid,
            'major'=>array(
                '$in'=>is_array($majors)?$majors:array($majors)
            ),
            "atime"=>array('$gte'=>$today,'$lt'=>$tomorrow)
        ));
        return array_sum(array_values($this->vistorNumBySex($majors[0])));
    }
    function vistorNumBySex($majors){

//      return $this->total2(
//            array('$match' => '')
//        );
        $redis=Yaf\Registry::get("redis");
        $keys=array_map(function($item){
            return date("d-".str_pad($item,2,0,STR_PAD_LEFT));

        },range(0,23));
        $year=date('Y');
        $month=date('m');
        return array_map(function($item) use($month){
            return intval($item);
        },$redis->hmGet("ib.{$majors}.{$year}.{$month}",$keys));


    }
    function vistorMonthNumBySex($majors){

//      return $this->total2(
//            array('$match' => '')
//        );
        $redis=Yaf\Registry::get("redis");
        $keys=array_map(function($item){
            return date("m-06-".str_pad($item,2,0,STR_PAD_LEFT));

        },range(0,23));



        return array_map(function($item){
            return intval($item);
        },$redis->hmGet("ib.11000.2015",$keys));





    }
    function vistorYearNumBySex($major){

//      return $this->total2(
//            array('$match' => '')
//        );
        $redis=Yaf\Registry::get("redis");
        $keys=array_map(function($item){
            return date("m-d-".str_pad($item,2,0,STR_PAD_LEFT));

        },range(0,23));
        $result=$redis->hmGet("ib.num.$major.".date("y"),$keys);

        return array_map(function($item){
            return intval($item);
        },$result);


    }
    function ibVistorNumBySex($major){

        $redis=Yaf\Registry::get("redis");

        $ibeacons=$redis->zRevRange("ib.num.$major", 0, 10, true);

        $qStr=$this->arrToInStr(array_keys($ibeacons),'minor');
        $ib=new IbeaconModel();

        $result=$ib->find("major='$major' and $qStr",9999,array('alias','id','minor'));
        $result2=array();
        array_map(
            function($item)use(&$result2,&$ibeacons){
                $result2[$item['alias']]=$ibeacons[$item['minor']];
            },
            $result
        );

        return $result2;


    }
}
