<?php
class PodNotifierDataMModel extends MongoSysModel {


    static $_tbName = 'pod_notifier_data';

    static $_indexes=array(
        'name_notify','keyword','who','action','notify_type','uid'
    );
    static $_tbMap=array(

    );
    protected $_tbMeta=array (
        'name_notify'=>array('reg' => 'cn_en'),
        '_id'=>array('reg'=>'mongoid','rights'=>8),
        'uid'=>array('reg'=>'ui8','rights'=>8),
        'sel_router'=>array('reg'=>'mac2','type'=>'select','data_source'=>array('tb'=>'routerM','val'=>'mac','field'=>'name','dataset'=>''),'rights'=>8),
        'notify_type'=>array(
            'reg'=>'en_name',
            'desc'=>'sel_device',
            'enum'=>array('keyword'=>array('txt'=>'关键词','reg'=>array('keyword'=>'cn_en')),'dev_name'=>array('txt'=>'设备名','reg'=>array('keyword'=>'cn_en')),'discovery'=>array('txt'=>'选择路由器上设备','reg'=>array('keyword'=>'mac2'),'hide'=>'keyword'),
                'mac'=>array('txt'=>'MAC地址','reg'=>array('keyword'=>'mac2')),'self'=>array('txt'=>'该路由器','show'=>'event.die','disable'=>'keyword'))
        ),
        'keyword'=>array(
            'reg'=>'ignore',
            'desc'=>'notify_devs'

        ),
        'event' =>
            array (
                'reg' => 'varchar',
                'lt'=>'15',
                'desc'=>"when_user",
                'enum'=>array('enter'=>'user_enter','exit'=>'user_exit','die'=>array('txt'=>'offline','only'=>'notify_type.self'))
            ),
        'action'=>array(
            'reg' => 'varchar',
            'lt'=>'15',
            'enum'=>array('wechat'=>array('txt'=>'do_wechat','sel'=>'chkWechatBind'),'email'=>'do_email')
        ),

        'who' =>
            array (
                'data_extra'=>'data-role=tagsinput readonly data-plus=1',
                'reg' => 'varchar',
                'plugin'=>'wechat_plus',
                'desc'=>'notify_who'
            ),
        'content'=>array (
            'reg' => 'tiny_desc',
            'desc'=>'notify_content',

        ),

        'freq'=>array("reg"=>'ui2','desc'=>'notify_freq','rights'=>8,'enum'=>array('10'=>'call_per_10m','15'=>'call_per_20m','30'=>'call_per_30m','60'=>'call_per_1h','720'=>'call_per_12h','1440'=>'call_per_1d')),

        'status'=>array('desc'=>'notify_status',"reg"=>'ui1','enum'=>array('1'=>'enable','2'=>'disable')),
        'last_send'=>array("reg"=>'timestamp','rights'=>4,'desc'=>'last_notify')
    );
    function beforeAdd(&$indata){
        $indata['sel_router']=$_SESSION['user']['router'];
        $indata['who']=explode(",",$indata['who']);
        if($indata['notify_type']=='discovery'){
            $indata['keyword']=array_map(function($item){
                return strtolower($item);
            },$indata['keyword']);
        }
        else if($indata['notify_type']=='mac'){
            $indata['keyword']=strtolower($indata['keyword']);
        }
        return $indata;
    }
     function afterAdd($inData,&$outData){
        $inData['code']=1;
        $inData['msg']="";
         $outData['uid']=$_SESSION['user']['uid'];


         $redis=Misc_Utils::redis();
         $redis->rPush('router.notify',$inData['_insertId']);


        return $inData;
    }


}