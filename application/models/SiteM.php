<?php



class SiteMModel extends MongoSysModel {


    static $_tbName  = 'site';
    static $_primary  = '_id';

    static $_opAction=array(
        'crud'=>array(
            'add'=>['en'=>true,'link'=>'/admin/page/index/?ztb=Page&action=add'],
            'import'=>['en'=>false],
            'export'=>['en'=>false],

        ),
        'extra'=>[
                [
                'class'=>'opt-qrcode',
                'icon'=>'fa fa-qrcode',
                'href'=>'',
                'title'=>'qrcode'
                ]

        ]

    );


    protected $_tbMeta=array (
        '_id' =>
            array (
                'reg' => 'mongoid',
                'desc' => 'mongoid',"rights"=>8
            ),
        /**
         * ['id'=>[]]
         */
        'title' =>
            array (
                'lt' => '30',
                'reg' => 'title',
                'desc' => 'title',
            ),
        'desc' =>
            array (
                'lt' => '1024',
                'reg' => 'desc',
                'desc' => 'desc',
            ),
        'logo' =>
            array (
                'lt' => '1024',
                'reg' => 'fileurl',
                'desc' => 'logo',
            ),
        '_type' =>
            array (
                'lt' => '6',
                'reg' => 'en_name',
                'desc' => 'type',
            ),
        'theme' =>
            array (
                'lt' => '1024',
                'reg' => 'en_name',
                'desc' => 'theme',
            ),
        'ctime' =>
            array (
                'reg' => 'timestamp',
                'desc' => 'ctime',"rights"=>4
            ),

        'uid' =>
            array (
                'reg' => 'i8',
                'desc' => 'uid',"rights"=>4
            )


    );

    function __construct(){

        parent::__construct();


    }

    function getOne($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        $primary=$this->primary();
        return  $this->findOne(array($primary=>$data));

    }




}