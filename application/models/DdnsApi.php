<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 12/27/14
 * Time: 4:10 PM
 */


class DdnsApiModel  extends CurlApiModel {


       private $c;
       private $obj;
        private $cfg;
       protected $_tbMeta=array(
           'system'=>array(
               'reg'=>'/^dyndns$/i',
           ),
           'hostname'=>array('reg'=>"domain"),
           'myip'=>array('reg'=>"ipv4",'type'=>'datetime')

       );
    function __construct(){

         $this->c= Yaf\Application::app()->getConfig();


//openid.dnspod.domain_id=2929728
//openid.dnspod.domain=88snow.com
//openid.dnspod.ddns=ddns
//openid.dnspod.username=ifa6@qq.com
//openid.dnspod.password=h666666

         $provider=$this->c['domain']['def'];
         $this->cfg=$this->c['domain'][$provider];
         if(!empty($this->cfg)){

             $this->cfg=$this->cfg->toArray();
             $obj="\\Domain\\".ucfirst($provider)."Domain";

             $this->obj=new $obj($this->cfg);
         }


    }
    function create($username,$data){

        $result=$this->obj->record_create($data);
        if($result && $result[0]!==false){
            $domain=new DomainMModel();
            $data['record_id']=$result['record']['id'];
            $data['username']=$username;
            $data['domain_id']=$this->cfg['domain_id'];

            return $domain->upsert2(array('username'=>$username),$data);
        }

        return false;


    }
    function update($uid,$data){
        $router=new RouterMModel();
        $result=$router->findOne(array('uid'=>intval($uid),'domain'=>$data['hostname']),array("_id","record_id"));
        if(!empty($result['_id'])){
            $data['record_id']=$result['record_id'];
            $ret=$this->obj->record_modify($data);
            if($ret && $ret[0]!==false){
                return $router->update2(array('ip'=>$data['value']),array('_id'=>$result['_id']));
            }
        }

        return false;
    }


} 