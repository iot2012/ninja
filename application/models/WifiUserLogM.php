<?php

/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 11/28/14
 * Time: 10:42 AM
 */

class WifiUserLogMModel extends MongoSysModel
{


    static $_tbName = 'wifi_user_log';

    protected $_tbMeta=array (
        'dev_mac' =>
            array (
                'reg' => 'mac',
            ),

        'ctime' =>
            array (
                'reg' => 'timestamp'
            ),
        'router_mac' =>
            array (
                'reg' => 'mac'
            ),
        'status'=>[
            'reg'=>'ui1'
        ]


    );

    /**
     * 权限存取的JSON格式
     *
     * {uid:'','tb':'',right:{read:[field1,field2],write:[field1,field2]}}
     * @param $req
     * @param $user
     * @param $whereCond
     * @return array
     *
     */
    function beforeRead(&$req,&$user,&$whereCond){
        $whereCond['uid']=12;
        return 0;
        return [false,['code'=>-1]];

    }

}