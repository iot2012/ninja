<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 7/21/14
 * Time: 4:34 PM
 */

class EventModel  extends SysModel {


    static $_tbName  = 'event';
    static $_primary  = 'id';

    /**
     * validate each field in tables
     * @todo store get  message in i18n file`
     */

    /**
     *    'gt'=>'',
     *    'get'=>'',
     *    'let'=>'',
     * @var array
     */

    protected $_tbMeta=array(
        'cal_id'=>array(
            'reg'=>'ui8',
            'lt'=>'1024',
            'desc'=>'calendar id'
        ),
        'id'=>array(
            'reg'=>'ui8',
            'lt'=>'1024',
            'desc'=>'event id'
            ),
        'color'=>array('reg'=>"hexcolor",'desc'=>'hex color'),
        'cal_name'=>array('reg'=>"mid_title",'desc'=>'calendar name'),
        'title'=>array('reg'=>"mid_title",'desc'=>'event title'),
        'start'=>array('reg'=>"datetime",'desc'=>'event start date'),
        'end'=>array('reg'=>"datetime",'desc'=>'event end date'),
        'allday'=>array('reg'=>"ui0",'desc'=>'all day'),
        'uid'=>array('reg'=>"ui8",'desc'=>"user uid")
    );

    protected $err=array('reg'=>array());


    function __construct(){

        parent::__construct();


    }
    function getAll($uid,$cid){
        return $this->find(array('uid'=>$uid,'cal_id'=>$cid));
    }
    function getTbName(){
        return $this->_name;
    }
    function getOne($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        $primary=$this->primary();
        return  $this->findOne(array($primary=>$data));

    }




} 