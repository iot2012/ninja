<?php
class WeatherInfoMModel extends MongoSysModel {

    static $_tbName ="weather_info";
    static $_db='ninja';
    protected  $_mongoChk=true;
    /**
     * 查询返回的列和排序,以及权限
     * @var array
     */
    protected $_modelRights=array('ryatek'=>array('4'=>
        array('cols'=>array('forcast_5d','ctime'),
            'order'=>array('ctime'=>-1))
    ));
    protected $_tbMeta=array(
        'forcast_5d'=>array(
            'reg'=>'ignore'
        )
    );
    function beforeGet(&$inData){
        if(array_key_exists('city_cn',$inData)){
            $inData['city_cn']=urldecode($inData['city_cn']);

        }
    }

    function afterGet($inData,$checkedData,&$outData){
        parent::afterGet($outData);
        $outData['ctime']=$outData['ctime']->sec;

    }


}