
<?php

class AdvModel extends SysModel {


    static $_tbName  = 'adv';
    static $_primary  = 'id';
    static $_indexes=array(
        'content','type'

    );
    /**
     * validate each field in tables
     * @todo store get  message in i18n file
     */

    /**
     *    'gt'=>'',
     *    'get'=>'',
     *    'let'=>'',
     *     'type'=>'front ui widget'
     * @var array
     *  rich_content: article
        text_content:
        txt_img_content:
        video_content:
        img_content:
        txt_video_content:
     */
    /**
     *
     *  rich_content: article
        text_content:
        txt_img_content:
        video_content:
        img_content:
        txt_video_content:
     *
     * @var array
     */
    protected $_tbMeta=array(
    'content'=>array(
        'reg'=>'desc',
        'desc'=>'content',
        'kind'=>''
    ),
        'type'=>array('reg'=>"en_type",'let'=>10,'ref'=>'content',
            'enum'=>array('image'=>"image",'html'=>"html",'txt'=>"text",'pdf'=>'pdf','video'=>'video')),
    'uid'=>array('reg'=>"ui8",'rights'=>4),
    'ctime'=>array('reg'=>"ui4",'type'=>'datetime','rights'=>4),
        'lang'=>array('reg'=>"en",'eq'=>4,'enum'=>array('enus'=>"english",'zhcn'=>"chinese")),
        'position'=>array('reg'=>"ui1",'enum'=>array(1=>"enter",2=>'exit',3=>'item',4=>'meeting',5=>'background'))
    );
    protected $err=array('reg'=>array());




    function __construct(){

        parent::__construct();


    }
    function getAdvContent($ib_id,$type,$lang){
        $sql="select a.* from {$this->_pre}adv as a
                                join {$this->_pre}ibeacon_adv as ai on a.id=ai.adv_id
                                join {$this->_pre}page as p on ai.ib_id=p.ib_id
                                where a.lang='$lang' and a.type='$type' and p.ib_id='$ib_id'
                                order by ai.ctime  desc";
        //array($uuid,$major,$minor)

       return $this->execSql($sql);

    }
    function getOne($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        $primary=$this->primary();
        return  $this->findOne(array($primary=>$data));

    }



}