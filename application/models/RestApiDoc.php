<?php


class RestApiDocModel extends SysModel {


    static $_tbName = 'rest_api_doc';
    static $_primary = 'id';



    protected $_tbMeta=array (
        'id' =>
            array (
                'reg' => 'i4',
                'desc' => '',
            ),
        'cid' =>
            array (
                'reg' => 'i4',
                'desc' => '',
            ),
        'url' =>
            array (
                'lt' => '160',
                'reg' => 'varchar',
            ),
        'version' =>
            array (
                'lt' => '10',
                'reg' => 'varchar',
            ),
        'method' =>
            array (
                'lt' => '4',
                'reg' => 'varchar',
            ),
        'desc' =>
            array (
                'lt' => '2560',
                'reg' => 'varchar',
            ),
        'params' =>
            array (
                'lt' => '1024',
                'reg' => 'varchar',
            ),
        'return' =>
            array (
                'lt' => '1500',
                'reg' => 'varchar',
            ),
        'atime' => array(

        ),
        'mtime' => NULL,
        'uid' =>
            array (
                'reg' => 'i4',
                'desc' => '',
            ),
        'notes' =>
            array (
                'lt' => '1024',
                'reg' => 'varchar',
            ),
        'error' =>
            array (
                'lt' => '1024',
                'reg' => 'varchar',
            ),
        'samp_req' =>
            array (
                'lt' => '2048',
                'reg' => 'varchar',
            ),
        'samp_resp' =>
            array (
                'lt' => '2048',
                'reg' => 'varchar',

            ),
    );
    private   $catArr=array(
        'Token'=>12,
        'System'=>13,
        'Wifi'=>14,
        'Wan'=>15,
        'Lan'=>16,
        'App'=>17,
        'Security'=>18,
        'File'=>19

    );
    function __construct(){

        parent::__construct();

    }
    function getDocByUrl($file){
        $cat=new RestApiCatModel();
        $cats=$cat->getCats();
        $cnt=100000;
        $entries   = array();
        if(file_exists($file)) {
            $apiDoc = json_decode(file_get_contents($file), true);

            $time = "2015-12-12 12:12:22";
            $spliter = "###";
            $errors = "";
            $entries=array();
            $version=$apiDoc['version'];

            array_map(function($row) use($cats,$time,$spliter,$apiDoc,&$cnt,&$entries,$version){
                $row['time']=$time;


                $cid=empty($this->catArr[ucfirst($row['cat'])])?11:$this->catArr[ucfirst($row['cat'])];

                $row['cid']=$cid;
                $row['version']=$version;
                $row['desc']=(empty($row['desc'])?"empty description":$row['desc']);
                $row['error']=(empty($row['error'])?"empty errors":$row['error']);
                $parameters=$row['parameters'];
                $return=$row['return'];
                $sampReq=array();
                $sampResp=array();


                list($sampReq,$parameters)=$this->formatData($parameters);
                list($sampResp,$return)=$this->formatData($return);


                $return=json_encode($return,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
                $sampResp=trim(json_encode($sampResp,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT));
                if($row['method']=='GET'){
                    //http_build_query 默认是1738 空格是+
                    //PHP_QUERY_RFC3986 空格是%20 === rawurlencode
                    $sampReq=http_build_query($sampReq,null,'&',PHP_QUERY_RFC3986);
                    $parameters=Misc_Utils::http_build_raw($parameters);

                } else {
                    $sampReq=json_encode($sampReq,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
                    $parameters=json_encode($parameters,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);

                }


                $row['return']=$return;
                $row['samp_resp']=$sampResp;
                $row['samp_req']=$sampReq;
                $row['return']=$return;
                $row['params']=$parameters;
                $row['url']=$row['url'];
                $row['url_id']=$row['method']."-".str_replace(array("/","."),array("-","-"),$row['url']);
                unset($row['parameters']);
                $cnt=$cnt+1;
                $entries[$row['url']][$row['method']] = $row;

            },$apiDoc['api']);

        }

        return array('prefix'=>$apiDoc['prefix'],'host'=>$apiDoc['host'],'docs'=>$entries);
    }

    function formatData($data,$spliter="###"){
        $sampResp=array();
        foreach($data as $key=>$val){
            if(!is_array($val)){
                $val2=explode($spliter,$val);
                $data[$key]=$val2[0];
                if(count($val2)>2){
                    $temp=array_shift($val2);
                    $sampResp[$key]=implode("",$val2);
                    $data[$key]=$temp;

                } else {
                    $sampResp[$key]=($val2[1]==null)?$val2[0]:$val2[1];
                }

                $json=json_decode($sampResp[$key],true);
                $sampResp[$key]=empty($json)?$sampResp[$key]:$json;

            } else {
                $return=$this->formatData($val);
                $data[$key]=$return[1];
                $sampResp[$key]=$return[0];
            }

        }

        return array($sampResp,$data);

    }
    function getDocs($file){

        $cat=new RestApiCatModel();
        $cats=$cat->getCats();
        $cnt=100000;
        $entries= array();
        if(file_exists($file)) {
            $apiDoc = json_decode(file_get_contents($file), true);

            $time = "2015-12-12 12:12:22";
            $spliter = "###";
            $errors = "";
            $entries=array();
            $version=$apiDoc['version'];
            $catArr=array(
                'Token'=>12,
                'System'=>13,
                'Wifi'=>14,
                'Wan'=>15,
                'Lan'=>16,
                'App'=>17,
                'Security'=>18,
                'File'=>19,
                'Statistics'=>20

            );
            array_map(function($row) use($cats,$time,$spliter,$apiDoc,&$cnt,&$entries,$version){
                $row['time']=$time;


                $cid=empty($this->catArr[ucfirst($row['cat'])])?ucfirst($row['cat']):$this->catArr[ucfirst($row['cat'])];

                $row['cid']=$cid;
                $row['version']=$version;
                $row['desc']=$this->desc($row['desc'],$cid,$row['method']);
                $row['error']=(empty($row['error'])?"empty errors":$row['error']);
                $parameters=$row['parameters'];
                $return=$row['return'];
                $sampReq=array();
                $sampResp=array();


                list($sampReq,$parameters)=$this->formatData($parameters);


                list($sampResp,$return)=$this->formatData($return);

                $return=json_encode($return,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
                $sampResp=trim(json_encode($sampResp,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT));
                if($row['method']=='GET'){
                    //http_build_query 默认是1738 空格是+
                    //PHP_QUERY_RFC3986 空格是%20 === rawurlencode
                    $sampReq=http_build_query($sampReq,null,'&',PHP_QUERY_RFC3986);
                    $parameters=Misc_Utils::http_build_raw($parameters);

                } else {
                    $sampReq=json_encode($sampReq,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
                    $parameters=json_encode($parameters,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);

                }


                $row['return']=$return;
                $row['samp_resp']=$sampResp;
                $row['samp_req']=$sampReq;
                $row['return']=$return;
                $row['params']=$parameters;
                $row['url']=$row['url'];
                $row['url_id']=$row['method']."-".str_replace(array("/","."),array("-","-"),$row['url']);
                unset($row['parameters']);
                $cnt=$cnt+1;
                $catName=$this->catName($cats,$cid);


                $entries[$catName][] = $row;

            },$apiDoc['api']);

        }

        return array('prefix'=>$apiDoc['prefix'],'host'=>$apiDoc['host'],'docs'=>$entries);
    }
    function catName($cats,$cid){
        $catName=strtolower(isset($cats[$cid]['name'])?$cats[$cid]['name']:$cid);
        $lang=$this->lang();
        return isset($lang[$catName])?$lang[$catName]:$catName;
    }
    function desc($desc,$catName,$method){
        $lang=$this->lang();
        $method=strtolower($method);
        $catName=strtolower($catName);
        $method2=strtolower("${catName}_$method");
        if(!empty($desc)){
            return isset($lang[$desc])?$lang[$desc]:$desc;
        }

        $desc=isset($lang[$method2])?$lang[$method2]:'';

        if($desc==''){
            if($lang['_langType']=='zhcn'){
                $desc=$lang[$method].(isset($lang[$catName])?$lang[$catName]:$catName)."信息";
            } else if($lang['_langType']=='enus') {
                $desc=$lang[$method]." ".(isset($lang[$catName])?$lang[$catName]:$catName)." info.";
            }
        }
        return $desc;
    }
    function tr($lang,$word){

        return isset($lang[$word])?$lang[$word]:$word;

    }
    function lang(){
        $lang=Lang::getLangType();
        $langFile="docs/locale/$lang.ini";
        if(file_exists($langFile)){
            return parse_ini_file($langFile);
        }
        return [];
    }

    function exists($data){

        return  $this->findOne($data);

    }


}