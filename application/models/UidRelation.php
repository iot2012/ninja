<?php
class UidRelationModel extends SysModel {


    static $_tbName  = 'uid_relation';
    static $_primary  = 'id';



    protected $_tbMeta=array (
  'id' => 
  array (
    'reg' => 'ui4',
    'desc' => 'id',
  ),
  'mainuid' => 
  array (
    'reg' => 'ui8',
    'desc' => 'mainuid',
  ),
  'uid' => 
  array (
    'reg' => 'ui8',
    'desc' => 'uid',
  ),
);

    function __construct(){

        parent::__construct();

    }

    function exists($data){

        return  $this->findOne($data);

    }
    function getServices($uid){
        $tbName=$this->tbName();
        return $this->execSql("select regdate,note,uid,icon_url,email,realname,username from {$this->_pre}common_user where
            uid=any(select subuid from {$tbName} where mainuid=?)",array($uid));
    }

}