<?php
class  TempFormMModel extends MongoSysModel {


    static $_tbName  = 'temp_form';
    static $_primary  = '_id';

    static $_opAction=array(
        'crud'=>array(
            'add'=>['en'=>true,'link'=>'/admin/page/index/?ztb=Page&action=add']

        )

    );


    protected $_tbMeta=array (
        'name' =>
            array (
                'reg' => 'en_name',
                'desc' => 'mongoid',"rights"=>4
            ),
        /**
         * ['id'=>[]]
         */
        'uid'=> array (
            'reg' => 'ui8',

        ),
        'id' =>
            array (
                'lt' => 'i2',
                'reg' => 'title',
                'desc' => 'title',
            ),
         'section'=>array (
             'lt' => 'i2',
             'reg' => 'en_name',
             'desc' => 'section',
         ),

         'fields'=>array (
             'lt' => 'i2',
             'reg' => 'en_name',
             'desc' => 'section',
         ),
        'ctime'=>array (
            'lt' => 'i4',
            'reg' => 'timestamp'
        )



    );

    function __construct(){

        parent::__construct();

    }

    function province($sid){

        return  $this->distinct("_info_info_file.province",array('_id'=>new MongoId($sid)));
    }
    function getOne($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        $primary=$this->primary();
        return  $this->findOne(array($primary=>$data));

    }




}