<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 6/20/14
 * Time: 10:47 AM
 */

class ChatModel extends SysModel {


    static $_tbName  = 'chat';
    static $_primary  = 'id';

    protected $_tbMeta=array(
        'from_id'=>array(
            'reg'=>'ui8',
            'lt'=>'1024',
            'desc'=>'from id'
        ),
        'from_idtype'=>array('reg'=>"en_name",'desc'=>'from id type','let'=>10),
        'uid'=>array('reg'=>"ui8",'desc'=>'user id'),
        'id'=>array(
            'reg'=>'ui4',
            'desc'=>'chat url'
        ),
        'ctime'=>array('reg'=>"timestamp",'desc'=>'create time')
    );

    function fetchByUid($uid){
        $tbName=$this->tbName();
        return $this->execSql("select c1.*,d.name as realname,d.icon_url from {$tbName} as c1 inner join {$this->_pre}discuss_group as d on c1.from_id=d.id where c1.uid=?
        union select c2.*,u.realname,u.icon_url from {$tbName} as c2 inner join {$this->_pre}common_user as u  on c2.from_id=u.uid where c2.uid=?",array($uid,$uid));

    }


    protected function getById($id)
    {
        $primary=$this->primary();
        return $this->findOne(array($primary=>$id));
    }

    protected function updateById($arr, $uid)
    {
        $primary=$this->primary();
        return $this->update($arr, array($primary=>$uid));
    }

}