<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 11/28/14
 * Time: 10:42 AM
 */


class RouterMModel extends MongoSysModel {


    static $_tbName = 'router';

    //指定可以直接通过rest方式调用的方法,返回值最好是数组
    static $_modelMethods=array(
        'deviceList'
    );
    protected $_tbMeta=array(


        '_id'=>array(
            'reg'=>'mongoid',
            'lt'=>'1024',
            'rights'=>4
        ),
        'cloud_status'=>array('reg'=>"ui0"),
        'uid'=>array('reg'=>"ui8"),
        'name'=>array('reg'=>"device_name",'lt'=>'30'),
        'mac'=>array('reg'=>"mac"),
        'ctime'=>array('reg'=>"timestamp"),
        'ip'=>array('reg'=>"ipv4"),
        'domain'=>array('reg'=>"domain"),
        'password'=>array('reg'=>'password')
    );
    static function encRouterPwd($pwd){

        return sha1(sha1($pwd));
    }
    function isOwner($arr){

        return isset($arr['mac'])?parent::isOwner(array('mac'=>$arr['mac'])):false;
    }
    function deviceList($param){


        $fg=new FogpodApi(FogpodApi::ip(),$param['mac']);

        return $fg->device_list();

    }

} 