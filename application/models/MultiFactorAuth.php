<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 8/11/14
 * Tim
*/

class MultiFactorAuthModel extends SysModel {


    static $_tbName  = 'multifactor_auth';
    static $_primary  = 'uid';

    /**
     * validate each field in tables
     * @todo store get  message in i18n file
     */

    /**
     *    'gt'=>'',
     *    'get'=>'',
     *    'let'=>'',
     * @var array
     */
    protected $_tbMeta=array(
        'secret'=>array('reg'=>'/^\w+{16}$/')
    );


    function __construct(){

        parent::__construct();


    }
    function updateMfa($uid,$secret){
        return $this->replace(array('uid'=>$uid,'secret'=>$secret));
    }
    function rmMfa($uid){
        return $this->delete(array('uid'=>$uid));


    }
    function getTbName(){
        return $this->_name;
    }

    function getOne($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        $primary=$this->primary();
        return  $this->findOne(array($primary=>$data));

    }


    function add($data){
        return $this->insert($data);

    }

}