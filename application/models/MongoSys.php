<?php

/**
 * Class SysModel
 * 定义了model的基本操作
 */


class MongoSysModel {

    static $_modelMethods;
    protected   $gw;
    static $_chkOwn=true;

    /**
     * @var     mapping table name without prefix
     */
    protected $_name;


    static $_db=DEF_MONGO_DB;

    protected   $r;

    protected   $rdb;
    protected   $wdb;
    protected   $w;
    protected $_actions=array();
    static $_indexes=array();
    /**
     * $_primary不是auto_increment类型要预设为multi,表示多个关联,one,other
     *
     * @var bool
     */
    protected $dbReadCfg='';
    protected $dbWriteCfg='';

    protected $logger;
    protected $_primaryType='auto';
    static $_primary='_id';
    protected $_pre;
    static $_tbName ;
    static $_mapMeta;
    protected $_tbMeta=array(
        '_id'=>array('reg'=>'mongoid')
    );
    protected $regs;
    protected $esc;
    protected  $_mongoChk;
    protected $_modelRights;
    static $_orgTbName;
    static $_opAction=array(
        'crud'=>array(
            'import'=>['en'=>true,'link'=>'#'],
            'export'=>['en'=>true,'link'=>'#'],
            'del'=>['en'=>true,'link'=>'#'],
            'add'=>['en'=>true,'link'=>'#'],
            'edit'=>['en'=>true,'link'=>'#'],

        )

    );
    static $_styleMeta;



     function styleMeta(){
        return $this::$_styleMeta;
    }
     static function tbInfo(){

        $db=static::$_db;
        $tb=static::$_tbName;
        $index=static::$_indexes;
        if(is_null($db)){
            $cfg=Yaf\Application::app()->getConfig();

            $db=$cfg['mongo']->defdb;

        }
        return compact("db","tb","index");
    }
    function config(){
        return Yaf\Registry::get("config");
    }
    function db(){
        return $this::$_db;
    }
    function mongoChk(){
        return $this->_mongoChk;

    }
    function __construct($cfg=''){
        $this->logger=Yaf\Registry::get("logger");


        if($this::$_db!=''){
            $defDb=$this::$_db;
        }
        if($cfg==''){
            $cfg=$this->cfg($defDb);
        }
        $this->dbReadCfg=$cfg[2];
        $this->dbWriteCfg=$cfg[3];
        $defDb=self::$_db=$cfg[1];
        self::$_orgTbName=(!empty($this::$_tbName)?($this::$_tbName):str_replace("Model",'',get_called_class()));
        if(class_exists('MongoClient')){

            $this->scheme=$defDb;
            $this->_pre=$cfg[0][$defDb]['pre'];
            if($cfg[2]->application->en_elasticsearch && !empty($this->_indexes)){
                try {
                    $this->esc=new Esindex($this->scheme,self::tbName());
                } catch(Exception $ex){
                    $this->logger->error($ex->getMessage());
                }

            }

            $readDsn="mongodb://{$this->dbReadCfg['uid']}:{$this->dbReadCfg['pwd']}@{$this->dbReadCfg['host']}:{$this->dbReadCfg['port']}/{$this->dbReadCfg['db']}";
            $writeDsn="mongodb://{$this->dbWriteCfg['uid']}:{$this->dbWriteCfg['pwd']}@{$this->dbWriteCfg['host']}:{$this->dbWriteCfg['port']}/{$this->dbWriteCfg['db']}";

            $wdb = new MongoClient($writeDsn, $this->dbWriteCfg['options']);
            $this->wdb=$wdb->$defDb;
            $this->w=$wdb->selectCollection($defDb,$this->tbName());

            if(($this->dbReadCfg['host']==$this->dbWriteCfg['host'] && $this->dbReadCfg['port']==$this->dbWriteCfg['port']) || empty($this->dbReadCfg['host'])){
                $this->rdb=$this->wdb;
                $this->r=$this->w;

            } else {
                if(!empty($this->dbReadCfg)){
                    $rdb=new MongoClient($readDsn, $this->dbReadCfg['options']);
                    $this->rdb=$rdb->$defDb;
                    $this->r=$rdb->selectCollection($defDb,$this->tbName());
                }

            }
            Yaf\Registry::set("mongo_read_db", array($this->rdb,$this->dbReadCfg['db']));
            Yaf\Registry::set("mongo_write_db", array($this->wdb,$this->dbReadCfg['db']));

        } else {
            $this->logger->error('Mongo Class not exists');
        }

    }

    function formatMsg($lang,$data){


        if($lang[$data]){
            return $lang[$data];
        }
        else if(preg_match("/^([\w-]+)__([\w-]+)$/i",$data,$ret)){
            return $lang[$data]?$lang[$data]:($lang[$ret[1]]." ".$lang[$ret[2]]);
        } else {
            return $data;
        }

    }
    function batchInsert(&$arr,$opt=array()){
        $ret=$this->w->batchInsert($arr,$opt);
        if($ret || $ret['ok']==1 && empty($ret['err']) ){
            $arr=array_map(function($item){
                $item['_id']=$item['_id']->__toString();
                return $item;
            },$arr);
            $this->buildEsIdx($arr);
        }

    }
    function pre(){
        return $this->_pre;
    }
    //Late Static Bindings 延迟绑定
    static function opAction(){
        return static::$_opAction;
    }
    function modelRights(){

        return $this->_modelRights;
    }
    function indexes(){
        return $this::$_indexes;
    }


    function group(){



    }


    function findById($id,$cols=array()){

        $primary=$this->primary();
        $result=$this->findOne(array($primary=>new MongoId($id)),$cols);


        return $result;
    }
    function last($query = array() , $fields = array()){
        $result=$this->pageFind($query,$fields,array('$natural'=>-1),1,1);
        return $result[0];
    }
    function exists($arr){
        $ret=$this->findOne($arr,array('_id'));
        return !empty($ret);
    }
    function actions(){
        return $this->_actions;
    }
    function rc(){
        return $this->r;
    }
    function findOne($query = array() , $fields = array(),$order=''){
        // return $this->r->findOne(array('$query'=>array(),'$orderby'=>array('$natural'=>-1)));
        if($order==''){
            if(isset($query['_id'])){
                $query['_id']=new MongoId($query['_id']);
            }
            $result=$this->r->findOne($query,$fields);
            if(!empty($result)){
                $result['_id']=$result['_id']->__toString();
                return $result;
            }
        } else {
            $result=$this->pageFind($query,$fields,$order,1,1);
            return $result[0];
        }
//        $new_object['utime']=$_SERVER['REQUEST_TIME'];
//        $result=$this->wdb->command(array(
//            'findOne' => $this->tbName(),
//            'query' => array('$query'=>'','$orderby'=>array('$natural'=>-1)),
//
//            'fields' => array('_id' => 1)   # Only return _id field
//        ));
//        return $result;

    }

    /**
     * 以ID作为组件排除记录
     */
    function records_kv(){


    }
     function cfg($defDb=''){

        $cfg=Yaf\Application::app()->getConfig();
        if($defDb==''){
            $defDb=$cfg['mongo']->defdb;
        }

        $dbArr=$cfg['mongo']->toArray();
        if(isset($dbArr[$defDb]['r'])){
            $read_idx=array_rand($dbArr[$defDb]['r']);
            $dbReadCfg=$dbArr[$defDb]['r'][$read_idx];
        }
        if(isset($dbArr[$defDb]['w'])){
            $write_idx=array_rand($dbArr[$defDb]['w']);
           $dbWriteCfg=$dbArr[$defDb]['w'][$write_idx];
        }


        return array($dbArr,$defDb,$dbReadCfg,$dbWriteCfg);
    }
     function orgTbName(){
        return static::$_orgTbName;
    }

    function w($tb=''){
        return $this->wdb;
    }
    static function arrToInStr($arr,$key){

        return $key." in ('".implode("','",$arr)."')";

    }
    function r($tb=''){
        return $this->rdb;
    }
//    function execute(){
//      $func =
//"function(greeting, name) { ".
//"return greeting+', '+name+', says '+greeter;".
//"}";
//$this->r->execute($code,args);
//    }
    function isOwner($arr){
        $ownerId=$this->ownerId();
        if(!isset($arr[$ownerId])){
            $arr[$ownerId]=intval($_SESSION['user'][$ownerId]);
        }
        return $this->exists($arr);

    }
    function primary(){

        return  $this::$_primary;
    }
    function primaryId(){

        return  $this::$_primary;
    }
    static function redis($type='redis',$db=''){


        $redis=Yaf\Registry::get($type);
        $cfg=Yaf\Application::app()->getConfig();
        if(class_exists('Redis')){
            if(empty($redis)){
                $redis=new Redis();
                $ret=$redis->pconnect($cfg['redis']['host'], $cfg['redis']['port'], $cfg['redis']['timeout']);
                if($ret && !empty($cfg['redis']['pwd'])){
                    $authed=$redis->auth($cfg['redis']['pwd']);
                    if($authed)
                    {

                        Yaf\Registry::set($type, $redis);

                    }

                }
            }


        }
        return $redis;
    }
    static function uid(){

        return $_SESSION['user']['uid'];
    }
    static function buildIn($ids,$key='_id',$isAssoc=false){
        $ids=self::toMongoIds($ids,$isAssoc);
        return array($key=>array('$in'=>$ids));
    }
    function buildQuery($query,$conj='$in'){
        $newQuery=[];
        foreach($query as $key=>&$val){
            if($key=='_id'){
                foreach($val as $key2=>$item){
                    $val[$key2]=new MongoId($item);
                }
                $newQuery[$key]=array($conj=>$val);
            }
        }
        return $newQuery;
    }
    function findIn($query=array(),$cols=array(),$order="",$startPage=1,$pageSize=20){


        return $this->pageFind($this->buildQuery($query),$cols,$order,$startPage,$pageSize);

    }
    function findAllIn($query=array(),$cols=array(),$order="",$startPage=1,$pageSize=20){

        return $this->pageFind($this->buildQuery($query,'$all'),$cols,$order,$startPage,$pageSize);

    }



    /**
     * find not in array()
     * @param array $ids
     * @param string $key
     * @param array $arr
     * @param bool $isAssoc
     * @param int $limit
     * @param array $cols
     * @param string $order
     * @param int $page
     * @return array|null
     */
    function findNIn($query=array(),$cols=array(),$order="",$startPage=1,$pageSize=20){
        return $this->pageFind($this->buildQuery($query,'$nin'),$cols,$order,$startPage,$pageSize);
    }

    function findInOwn($query=array(),$own,$cols=array(),$order="",$startPage=1,$pageSize=20){
        $query=$this->buildQuery($query,'$in');
        $query[$this->ownerId()]=$own;
        return $this->pageFind($query,$cols,$order,$startPage,$pageSize);
    }
    static function tbName(){

        return static::$_tbName;
    }

    /**
     * @param $key array|string
     * @param $dataSource associate array
     * the a,b,c is from $_POST key
     * string:a,b,c
     * array:array(a,b,c)
     */

    public function primaryType(){
        return $this->_primaryType;
    }


    function getTbName(){
        return $this::$_tbName;
    }
    public function checkRequired($requiredArr){
        $meta=$this->getTbMeta();
        $keys=array_keys($this->getTbMeta());
        return Validator::checkModel($keys,$meta,$this->getTbName(),'',$requiredArr);
    }
    public function check($keys="",$dataSource='',$meta='',$requiredArr=''){

        if(empty($meta)){
            $meta=$this->getTbMeta();
        }
        if(empty($keys)){
            $keys=array_keys($this->getTbMeta());
        }
        if(!empty($meta)){
            return Validator::checkModel($keys,$meta,$this->getTbName(),$dataSource,$requiredArr);
        }
        return true;
    }


    /**
     * @param $arr  if $arr dont contains _id,it will fill by _id after data inserted
     * @param array $options
     * @return mixed
     */
    function buildEsIdx($arr){
        if(!empty($this->_indexes) && $this->esc){
            $arr=Misc_Utils::array_pluck_arr($arr,array_merge($this->_indexes,array($this->primary())));
            $this->esc->createIndex($arr);

        }

    }
    function insert($arr , array $options = array() ) {
        $arr['ctime']=new MongoDate($_SERVER['REQUEST_TIME']);
        $this->w->insert($arr,$options);
        if(is_object($arr)){
            return $arr->_id;
        }
        $arr["_id"]=$arr['_id']->__toString();
        $this->buildEsIdx($arr);
        return $arr["_id"];
    }


    function replace(array $criteria , array $new_object , array $options = array() ){

        return $this->update($new_object,$criteria,$options);
    }
    function fmtDate($record){

        foreach(array('ctime','dateline','mtime','utime') as $key=>$val){
            if(isset($record[$val])){
                $record[$val]=new MongoDate($record[$val]);
            }
        }

        return $record;
    }
    function revFmtDate($record){

        foreach(array('ctime','dateline','mtime','utime') as $key=>$val){
            if(isset($record[$val]) && ($record[$val] instanceof MongoDate) ){
                $record[$val]=$record[$val]->sec;
            }
        }
        return $record;
    }
    function add($data,$options=array()){
        $time=time();
        if(isset($data[0])){
            foreach($data as &$record){
                $record=$this->fmtDate($record);
                if(!isset($record['ctime'])){
                    $record['ctime']=new MongoDate($time);
                }

            }
            $this->w->batchInsert($data,$options);

            return array_map(function($item){
                return $item->__toString();

            }, array_column($data, '_id'));
        } else {
            $data=$this->fmtDate($data);
            if(!isset($data['ctime'])){
                $data['ctime']=new MongoDate($time);
            }
            $this->w->insert($data,$options);
            return $data["_id"]->__toString();
        }




    }
    function insert2($doc) {

           $cursor = $this->w->find(array())->sort(array("_id"=>-1))->limit(1);


            $seq=$cursor->getNext();
            if($seq["_id"]>0){
                $seq=$seq['_id']+1;
            }
            else {
                $seq=1;

            }

            $doc['_id'] = $seq;


            $results = $this->w->insert($doc);
            return $seq;


    }
    /**
     * 注意update会更改原有的记录为新的,如果更改部分字段则为array('$set'=>array())
     * @param array $criteria
     * @param array $new_object
     * @param array $options
     * @return mixed
     */
    function upsert(array $criteria , array $new_object , array $options = array() ){
        if(isset($new_object['$addToSet'])){
            $new_object['$set']['utime']=new MongoDate($_SERVER['REQUEST_TIME']);
        } else {
            if(isset($new_object['$set'])){
                $new_object['$set']['utime']=new MongoDate($_SERVER['REQUEST_TIME']);
            } else {
                $new_object['utime']=new MongoDate($_SERVER['REQUEST_TIME']);
            }
        }
        $options['upsert']=true;
        return $this->w->update(
            $criteria,
            $new_object,
            $options
        );
    }
    function upset(array $setArr,array $criteria, array $options = array('multiple'=>true,'upsert'=>false)){
        foreach(array('ctime','dateline','mtime','utime') as $key=>$val){
            if(isset($new_object[$val])){
                $setArr[$val]=new MongoDate($setArr[$val]);
            }
        }

    }
    function update2(array $setArr,array $criteria, array $options = array('multiple'=>true,'upsert'=>false)){

        return $this->update(
            $setArr,
            $criteria,
            $options
        );
    }

    /**
     * {a:[1,2,4],c:[4,5,6]}
     * @param $query
     * @param $eleArr ["b.1","c.2"]
     * @return null
     */
    function unsetByIdx($query,$eleArr){

        $obj=['$unset'=>array_fill_keys($eleArr,1)];

        return $this->update(
            $query,$obj

        );

    }
    /**
     * {a:[1,2,4],c:[4,5,6]}
     * @param $query
     * @param $eleArr ["b.1","c.2"]
     * @return null
     */
    function pullByIdx($query,$eleArr){

        $objs=['$unset'=>array_fill_keys($eleArr,1)];


        $ret=$this->update(
            $query,$objs

        );
        $eleArr=array_map(function($item){
            return substr($item,0,strrpos($item,"."));
        },$eleArr);
        $this->w->update(
            $query,['$pull'=>array_fill_keys($eleArr,null)]

        );
        return $ret;


    }
    function update(array &$new_object ,array $criteria, array $options = array('upsert'=>true,'new'=>true) ,$retArr= array('_id' => 1)){
        foreach(array('ctime','dateline','mtime','utime') as $key=>$val){
            if(isset($new_object[$val])){
                $new_object[$val]=new MongoDate($new_object[$val]);
            }
        }
        $retval = $this->w->findAndModify(
            $criteria,
            $new_object,
            $retArr,
            $options
        );
        if(!empty($retval['_id']))  {
            return $retval['_id']->__toString();
        }
        return null;

    }

    /**
     * 注意update会更改原有的记录为新的,如果更改部分字段则为array('$set'=>array())
     * @param array $query
     * @param array $new_object
     * @param array $options
     * @return mixed
     */
    function upsert2(array $query , array $new_object , array $options = array() )
    {
        //$new_object['utime']=$_SERVER['REQUEST_TIME'];
//        $result=$this->rdb->command(array(
//            'findAndModify' => $this->tbName(),
//            'query' => $query,
//            'update' => $new_object,
//            'new' => true,        # To get back the document after the upsert
//            'upsert' => true,
//            'fields' => array('_id' => 1)   # Only return _id field
//        ));
        $retval = $this->w->findAndModify(
            $query,
            $new_object,
            $options,
            array('_id' => 1)

        );
        return $retval;
    }


    function tbMeta()
    {
        return $this->_tbMeta;
    }

    function getTbMeta(){
        return $this->_tbMeta;
    }
    function Id($id){
        return new MongoId($id);
    }
    function getPrimary(){
        return empty($this->_primary)?"_id":$this->_primary;

    }
    static  function toMongoIds($ids,$isAssoc=false){
        if($isAssoc==false){
            return array_map(function($item){
                return new MongoId($item);
            },$ids);
        } else {
            return array_map(function($item){
                return new MongoId($item['_id']);
            },$ids);
        }


    }


    function andQuery($arr){

    }

    function orQuery($arr){

    }
    /**
     *
     * @example $t->findOr([["_id"=>new MongoId("55f901899fe61f7917204ad5"),"uid"=>5]])
     * @param array $whArr
     * @param array $cols
     * @param string $order
     * @param int $startPage
     * @param int $pageSize
     * @return array
     */

    function findOr(array $whArr=array(), $cols=array(),$order='',$startPage=1,$pageSize=20){
        return $this->pageFind(array('$or'=>$whArr),$cols,$order,$startPage,$pageSize);
    }



    /*
     *
     *
     * @param array $whArr 查找条件  {abc:/www/,cde:/^abc/,efg:null}
     * @param int $limit  查找数目
     * @param array $cols 查找的列
     * @param array $order array('name'=>1) sort asc,array('name'=>-1) =>des
     * @return mixed
     */

    function find($whArr=array(), $limit=20,$cols=array(),$order='',$page=1)
    {
        if($whArr===''){
            $whArr=array();
        }

        if($page<=1){
            if($order==''){
                $cursor=$this->r->find($whArr,$cols)->limit($limit);
            }
            else {
                $cursor=$this->r->find($whArr,$cols)->limit($limit)->sort($order);
            }
        } else if($page>1){
            $start=($page-1)*20;
            if($order==''){
                $cursor=$this->r->find($whArr,$cols)->skip($start)->limit($limit);
            }
            else {
                $cursor=$this->r->find($whArr,$cols)->skip($start)->limit($limit)->sort($order);
            }
        }


        $cursor->rewind();
        $documents=array();
        $record=$cursor->current();
        while($record){
            if($record['_id']){
                $record['_id']=$record['_id']->__toString();
            }
            $documents[]=$record;
            $record=$cursor->getNext();
        }
        if(isset($whArr['_id'])){
            return $documents[0];
        }
        return $documents;



    }

    /**
     * array('abc'=>1,"cde"=>0)表示abc存在而cde不存在
     * @param $keys
     * @return array|null
     */
    function findExistKeys($keys){


        $query=array();
        foreach($keys as $key=>$val){
            $query[$key]=array('$exists'=>(bool)$val);
        }
        return $this->findOne($query,array("_id"));


    }
    /**
     * @param $keys  abc
     * @param bool $isExist 当keys是string的时候
     * @return array|null
     */
    function findExistKey($keys,$isExist=true){



            return $this->findOne(array($keys=>array('$exists'=>$isExist)),array("_id"));

    }
    function findLast($query = array() , $fields = array()){
        $result=$this->pageFind($query,$fields,array('$natural'=>-1),1,1);

        return $result[0];
    }
    /**
     *
     * updateInner(['ids.id'=>'55ee47b8ba713db079c9ca11'],array(
                'ids.status'=>22
                ););
     * @param $query
     * @param $innerData
     */
    function updateInner($query,$innerData){
        $innerData=$this->innerData($innerData);
        $data=array('$set'=>$innerData);

        $this->update2($data,$query);
    }


    function innerData($arrs){


        $result=array();
        foreach($arrs as $key=>$val){

            $ret=explode(".",$key);
            if(count($ret)>1){
                $lastField=array_pop($ret);
                array_push($ret,'$');
                $queryKey=implode(".",$ret).".".$lastField;
                $result[$queryKey]=$val;
            } else {
                $result[$key]=$val;
            }

        }

        return $result;

    }
    /**
     *
     * { _id: 1, results: [ { product: "abc", score: 10 }, { product: "xyz", score: 5 } ] }
     * @param $arrs
     * @return array
     *
     * db.survey.find(
        { results: { $elemMatch: { product: "xyz", score: { $gte: 8 } } } }
        )
     */
    function innerQuery($arrs){


        $cols=array();
        foreach($arrs as $key=>$val){

            $ret=explode(".",$key);
            if(count($ret)>1){
                $lastField=array_pop($ret);
                $queryKey=implode(".",$ret);
                $cols[$queryKey]=array('$elemMatch'=>array($lastField=>$val));
            }

        }

        return $cols;

    }

    /**
     * only return embed document
     * @param array $whArr
     * @param array $cols
     */
    function findInner(array $whArr=array(),  $cols=''){
          $innerCols=$this->innerQuery($whArr);
          if($cols!=''){
              $cols=array_merge($innerCols,$cols);
          }
          return $this->pageFind($whArr,$cols);

    }

    function tbMeta2($lang){
        $tbMeta=$this->tbMeta();
        $tbName=$this->tbName();
        foreach($tbMeta as $k=>&$v){
            if(!empty($v['enum'])){
                $v['enum']=array_map(function($item)use($lang,$k){
                    /**
                     * $k对应value
                     */
                    if(is_array($item)){
                        $item2=$item['txt'];
                        $kItem=$k.'_'.$item2;
                        $item['txt']=$lang[$kItem]?$lang[$kItem]:($lang[$item2]?$lang[$item2]:$item2);

                    } else {

                        $kItem=$k.'_'.$item;
                        $item=$lang[$kItem]?$lang[$kItem]:($lang[$item]?$lang[$item]:$item);

                    }
                    return $item;



                },$v['enum']);
            }
            if(empty($v['desc'])){
                $v['desc']=Misc_Utils::getTbLangVal($k,$tbName,$lang);

            } else {
                $v['desc']=$lang[$v['desc']]?$lang[$v['desc']]:$v['desc'];
            }



        }
        return $tbMeta;
    }
    function countByDate($start,$end='',$field='ctime'){
        if($end==''){
            $end=time();
        }
        $arr=[$field=>[
            '$lt'=>new MongoDate($end),
            '$gt'=>new MongoDate($start)
        ]];
        return  $this->total($arr);
    }


    /**
     * @param $month 201502
     * @param string $field
     * @return int
     */
    function monthCount($month,$field='ctime'){
        list($start,$end)=month_timestamp($month);
        return $this->countByDate($start,$end,$field);
    }
    function weekCount($field='ctime'){
        $start=strtotime("-7 day");
        return  $this->countByDate($start,time(),$field);
    }
    function dayCount($field='ctime'){
        $start=strtotime(date("Y-m-d"));
        return  $this->countByDate($start,time(),$field);
    }

    function total($arr=array()){
        $cursor=$this->r->find($arr);

        return $cursor->count();
    }
    function total2($groupArr,$query=array()){
        return $this->r->aggregate(
            array('$match' => $query),
            // array('_id'=>'$uid','count'=>array('$sum'=>1)
            array('$group' =>$groupArr


                //   array('$sort' => array( '_id' => self::SORT_DESC)),

//                        'type' => array('$push'=> array('type' => '$type',
//                        'title'=>'$title',
//                        'link'=>'$link',
//                        'dt'=>'$dt',
//                        'own'=>'$own',
//                        'usr'=>'$usr'))

            ));
//        db.articles.aggregate( [
//                        { $match : { score : { $gt : 70, $lte : 90 } } },
//                        { $group: { _id: null, count: { $sum: 1 } } }
//                       ] );
    }

    /**
    @example query
     * pageFind( array(), array('comments' => array( '$slice' => [2,5] ) ) ,1); 查找元素是2开始的5个元素
     *
     */
    function pageFind(array $whArr=array(), array $cols=array(),$order='',$startPage=1,$pageSize=20){


       return $this->find($whArr,$pageSize,$cols,$order,$startPage);


    }

    function ownerId(){

        return empty($this->_ownerId)?'uid':$this->_ownerId;
    }
    function ownerArr(){
        return array();
    }
    function findOwn($ownerArr,$page=1,$size=20,$cols=""){
        if(is_string($cols)){
            $cols=explode(",",$cols);
        }
        return $this->pageFind($ownerArr,$cols,'',(($page<=1)?1:$page),$size);

    }
    function delete(array $criteria, array $options=array())
    {
        return $this->w->remove($criteria,$options);
    }
    function fmtPrimaryId($id){

        return new MongoId($id);
    }

    function  findAndModify ( array $query , array $update , array $fields , array $options ){

        return $this->w()->findAndModify($query,$update,$fields,$options);
    }
    function execSql($sql,$data=array())
    {
        $adapter=$this->gw->getAdapter();
        /**
         * Adapter::QUERY_MODE_EXECUTE exec directly not use prepare sql
         * $adapter->query($sql,Adapter::QUERY_MODE_EXECUTE)
         *
         */
        $result=$adapter->query($sql,$data);
        $result->buffer();
        if(method_exists($result,"toArray")){
            return $result->toArray();
        } else if(method_exists($result,"count")) {
            return $result->count();
        }
        return $result;


    }
    /**
     * @param $ids
     * @param string $uid 当level为9的时候,可以不提供uid进行删除
     * @param string $ownerId
     * @return mixed
     */
    function deleteOwn($ids,$uid,$ownerId='uid'){

        $primary=$this->primary();
        if(method_exists($this,'fmtPrimaryId')){
            if(is_array($ids)){
                $ids=$ids[0];
            }
        }

        $ids=$this->fmtPrimaryId($ids);
        $ids=array(
            $primary => $ids
        );
        if($uid!=''){
            if(method_exists($this,'ownerIdData'))
            {
                $ids[$ownerId]=$this->ownerIdData($uid);

            } else {
                $ids[$ownerId]=intval($uid);
            }
        }



        return $this->delete($ids);
    }
    function distinct($key,$query=array()){

       $result=$this->wdb->command(array("distinct" => $this->tbName(), "key" => $key,'query'=>$query));
        if($result['ok']==1){

            return $result['values'];
        }
        return array();

    }
    /**
     * @param $ids
     * @param string $uid 当level为9的时候,可以不提供uid进行删除
     * @param string $ownerId
     * @return mixed
     */
    function deleteMany($ids,$uid='',$ownerId='uid'){
        $primary=$this->primary();
        if(method_exists($this,'fmtPrimaryId')){
            if(is_array($ids)){

                $ids=array_map(function($item){
                    return  $this->fmtPrimaryId($item);

                },$ids);

            }
        }

        $ids=array(
            $primary => array('$in' => $ids)
        );
        if($uid!=''){
            if(method_exists($this,'ownerIdData'))
            {
                $ids[$ownerId]=$this->ownerIdData($uid);

            } else {
                $ids[$ownerId]=intval($uid);
            }
        }

        return $this->delete($ids);
    }


}