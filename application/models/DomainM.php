<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 12/27/14
 * Time: 5:20 PM
 */

class DomainMModel extends MongoSysModel {

    static $_tbName="domain";
    protected $_tbMeta=array(

        'domain'=>array('reg'=>"domain"),
        'record_id'=>array('reg'=>"ui8"),
        'username'=>array('reg'=>"username"),
        'ip4'=>array('reg'=>"ipv4",'type'=>'datetime')

    );

} 