<?php
class AppCategoryModel extends SysModel {


    static $_tbName = 'app_category';



    protected $_tbMeta=array (
);

    function __construct(){

        parent::__construct();

    }
    function all(){
      return $this->find('',9999,array("id","name","icon_url"));


    }
    function exists($data){

        return  $this->findOne($data);

    }


}