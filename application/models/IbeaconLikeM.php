<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 11/28/14
 * Time: 6:14 PM
 */

class IbeaconLikeMModel extends MongoSysModel {

    static $_tbName ="ibeacon_like";
    protected $_ownerId='major';
    protected $_tbMeta=array(
        'uuid'=>array(
            'reg'=>'uuid',
            'rights'=>4
        ),
        'major'=>array('reg'=>"ui2"),
        'minor'=>array('reg'=>"ui2"),
        'like'=>array('reg'=>"ui1"),
        'ibuid'=>array('reg'=>"mongoid")
    );

    function add($data){

       if(empty($data['ibuid'])){
           $data['like']=intval($data['like']);
           $data['major']=intval($data['major']);
           $data['minor']=intval($data['minor']);
           $data['utime']=$_SERVER['REQUEST_TIME'];
           return $this->insert(Misc_Utils::array_pluck($data,'uuid,major,minor,ibuid,utime,like'));


       } else {
        return $this->update($data,Misc_Utils::array_pluck($data,'uuid,major,minor,ibuid'));
       }
       
    }
    function doGroup($data,&$outData,$lang){

     //  $this->find
        $start=intval($data['start']?$data['start']:0);
        $len=intval($data['len']?$data['len']:20);
        $redis=Yaf\Registry::get("redis");


        $major=$this->ownerIdData($_SESSION['user']['uid']);

        $total=$redis->zCard("ib.like.{$major}");


        if($major!==false){
            $data=$redis->zRange("ib.like.{$major}",$start,$len,true);
            $result=array();
            $cnt=0;
            $minors=array();
            $ibeacon=new IbeaconModel();

            foreach($data as $k=>$v){

                $result[$cnt]['major']=$major;
                $result[$cnt]['minor']=$k;
                $minors[]=$k;
                $result[$cnt]['like']=$v;
                $cnt++;


            }
            $minorSql=SysModel::arrToInStr($minors,"minor");

            $ibeaconArr=$ibeacon->find("major='$major' and ".$minorSql,$len,"alias");

            foreach($result as $k=>$v){

                $result[$k]['alias']=$ibeaconArr[$k]['alias'];
            }

        }
        $outData['columns']=array('alias'=>array(
            'reg'=>"alias",'desc'=>Misc_Utils::getTbLangVal('alias',IbeaconModel::orgTbName(),$lang)
        ))+$outData['columns'];
        unset($outData['columns']['uuid'],$outData['columns']['ibuid']);
        $outData=($outData+array('total'=>$total,'records'=>$result));


    }
    function beforePost(&$data){

            $this->redis->zIncrBy('ib.likes',1,"{$data['major']}.{$data['minor']}");


    }

    function beforeView(&$data){


        $id=$this->ownerIdData($data['uid']);

        if($id){
            $data=array(
                'major'=>(intval($id)),
                'like'=>array('$exists'=>true,'$gt'=>0)
            );
        } else {
            $data='';
        }
    }
    function ownerIdData($uid,$cols=array('id','status')){

        $m=new CompanyModel();

        $id=$m->findOne(
            array('uid'=>$uid),$cols);
        /**
         * 有效的状态
         */
        if($id['status']>=4){
            return intval($id['id']);
        }
        return false;
    }

}
