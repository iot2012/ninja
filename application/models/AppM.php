<?php
class AppMModel extends MongoSysModel {


    static $_tbName = 'app';
    static $_primary  = '_id';



    protected $_tbMeta=array (
      '_id' =>
      array (
        'reg' => 'mongoid',

      ),
      'uid' =>
      array (
        'reg' => 'i8',
      ),
      'name' =>
      array (
        'lt' => '100',
        'reg' => 'varchar',
      ),
      'icon_url' =>
      array (
        'lt' => '1024',
        'reg' => 'varchar',
      ),
      'url' =>
      array (
        'lt' => '1024',
        'reg' => 'varchar',
        'desc' => 'url',
      ),
      'atime' =>
      array (
        'reg' => 'i4',
      ),
      'status' =>
      array (
        'reg' => 'i1',
      ),
    'cat_id' =>
        array (
            'reg' => 'json',
        ),
    'tags' =>
        array (
            'reg' => 'json',
        ),
    'content' =>
        array (
            'reg' => 'text',
        ),
            'price'=>array('reg'=>'uf2'),
        'currency'=>array('reg'=>'ui1',array('1'=>'dollar','2'=>'cny'))
    );


    function all($arr=''){

        if($arr==''){
            $arr=array();
        }

        $apps=$this->find($arr,1000);

        return array($apps,Misc_Utils::array_kvs($apps,'catid'));


    }
    function getItemInfo($item_id){
        $data=$this->findById($item_id);

        return array('price'=>$data['price'],'name'=>$data['name'],'desc'=>$data['desc']);
    }
    function allSys(){
        $apps=$this->find(array('from'=>'sys'),1000);
        return array($apps,Misc_Utils::array_kvs($apps,'catid'));

    }
    function exists($data){

        return  $this->findOne($data);

    }


}