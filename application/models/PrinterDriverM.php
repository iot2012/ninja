<?php
class PrinterDriverMModel extends MongoSysModel {


    static $_tbName = 'printer_driver';
    static $_indexes=array(
        'brand','file','model','driver_name'
    );
    protected $_tbMeta=array (
      '_id' =>
      array (
        'reg' => 'mongoid',

      ),
      'brand' =>
      array (
        'reg' => 'en_name',
      ),
      'file' =>
      array (
        'lt' => '1024',
        'reg' => 'varchar', 'desc' => 'driver_file',
      ),
      'model' =>
      array (
        'lt' => '30',
        'reg' => 'varchar'
      ),
      'driver_name' =>
      array (
        'reg' => 'varchar',
          'lt' => '30'


      ),
        'ctime' =>
            array (
                'reg' => 'i4'

        )
    );



}