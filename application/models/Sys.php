<?php

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
/**
 * Class SysModel
 * 定义了model的基本操作
 */
class SysModel {
    static $_modelMethods;
    protected   $gw_r='';
    protected   $gw='';
    static $_chkOwn=true;
    /**
     * @var     mapping table name without prefix
     */

    static $_orgTbName;
    static $_primary ="id";
    protected $_ownerId;
    protected $_actions;
    protected $dbReadCfg='';
    protected $dbWriteCfg='';
    protected $readAdapter='';
    protected $writeAdapter='';
    protected $logger;
    static $_mapMeta;
    /**
     * $_primary不是auto_increment类型要预设为multi,表示多个关联,one,other
     *
     * @var bool
     */
    static $_db=DEF_MYSQL_DB;
    protected $_primaryType='auto';
    static $_indexes;
    protected $esc;
    protected $_pre;
    static    $_tbName ;
    protected $_tbMeta=array();
    protected $regs;
    protected $_modelRights;
    static $_opAction=array(
        'crud'=>array( 'import'=>['en'=>true,'link'=>'#'],
            'export'=>['en'=>true,'link'=>'#'],
            'del'=>['en'=>true,'link'=>'#'],
            'add'=>['en'=>true,'link'=>'#'],
            'edit'=>['en'=>true,'link'=>'#'],

        )

    );
    static $_styleMeta;
    function styleMeta(){
        return $this::$_styleMeta;
    }
    //Late Static Bindings 延迟绑定
    static function opAction(){
        return static::$_opAction;

    }

    function modelRights(){

        return $this->_modelRights;
    }
    static function uid(){

        return $_SESSION['user']['uid'];
    }
    function primary(){

        return  $this::$_primary;
    }
    static function redis($type='redis',$db=''){


        $redis=Yaf\Registry::get($type);
        $cfg=Yaf\Application::app()->getConfig();
        if(class_exists('Redis')){
            if(empty($redis)){
                $redis=new Redis();
                $ret=$redis->pconnect($cfg['redis']['host'], $cfg['redis']['port'], $cfg['redis']['timeout']);
                if($ret && !empty($cfg['redis']['pwd'])){
                    $authed=$redis->auth($cfg['redis']['pwd']);
                    if($authed)
                    {

                        Yaf\Registry::set($type, $redis);

                    }

                }
            }


        }
        return $redis;
    }
    function config(){
        return Yaf\Registry::get("config");
    }
    function primaryId(){

        return  $this::$_primary;
    }
    function getPrimary(){

        return $this::$_primary;
    }
    function ownerArr(){
        return array();
    }
    function getTbName(){

        return self::$_tbName;
    }
     function tbName(){

        return self::$_tbName;
    }
    function indexes(){
        return $this::$_indexes;
    }
    function isOwner($arr){
        $ownerId=$this->ownerId();
        if(!isset($arr[$ownerId])){
            $arr=intval($_SESSION['user'][$ownerId]);

        }
        return $this->exists($arr);

    }
    function actions(){
        return $this->_actions;
    }
    /**
     * @param $key array|string
     * @param $dataSource associate array
     * the a,b,c is from $_POST key
     * string:a,b,c
     * array:array(a,b,c)
     */
     function db(){
        return $this::$_db;
    }
    function ownerId(){
        return empty($this->_ownerId)?'uid':$this->_ownerId;
    }
    public function primaryType(){
        return $this->_primaryType;
    }

    public function total($query=''){
        $key=$this->primary();
        if($pos=strpos($key,',')){
            $key=substr($key,0,$pos);
        }
        if(empty($key)){
            $key="*";
        }
        $where='';
        if($query!=''){
            $query="where $query";
        }
        $tbName=$this->tbName();
        $result=$this->execSql("SELECT COUNT({$key}) as cnt FROM {$tbName} USE INDEX (PRIMARY) $query ");
        return $result[0]['cnt'];
    }
    function restGet($data){

        return $this->find($data,50);

    }
    public function checkRequired($requiredArr){

        return self::check("","",'',$requiredArr);
    }
    public function check($keys="",$dataSource='',$meta='',$requiredArr=''){

        if(empty($meta)){
            $meta=$this->getTbMeta();
        }
        if(empty($keys)){
            $keys=array_keys($this->getTbMeta());
        }
        if(!empty($meta)){
            return Validator::checkModel($keys,$meta,$this->tbName(),$dataSource,$requiredArr);
        }
        return true;
    }

    public function fetchByUid($uid){
        return $this->find(array("uid"=>$uid));

    }
     function orgTbName(){

        return self::$_orgTbName;
    }
    function cfg($db=''){

        $cfg=Yaf\Application::app()->getConfig();
        if($db==''){
            $defDb=$cfg['database']->defdb;
        }

        $dbArr=$cfg['database']->toArray();
        if(isset($dbArr[$defDb]['r'])){
            $read_idx=array_rand($dbArr[$defDb]['r']);
        }
        if(isset($dbArr[$defDb]['w'])){
            $write_idx=array_rand($dbArr[$defDb]['w']);
        }

        return array($dbArr,$defDb,$dbArr[$defDb]['r'][$read_idx],$dbArr[$defDb]['w'][$write_idx]);
    }
    function __construct($cfg=''){

        $this->logger=Yaf\Registry::get("logger");

        if($cfg==''){
            $cfg=$this->cfg();
        }

        list($dbArr,$defDb,$this->dbReadCfg,$this->dbWriteCfg)=$cfg;
        if($this::$_db!=''){
            $defDb=$this::$_db;
        } else {

        }
        $this->_pre=$dbArr[$defDb]['pre'];
        self::$_db=$this->dbReadCfg['database'];
        self::$_orgTbName=(!empty($this::$_tbName)?($this::$_tbName):str_replace("Model",'',get_called_class()));
        self::$_tbName=$this->_pre.self::$_orgTbName;


        $tbName=$this->tbName();
        if($cfg->application->en_elasticsearch && !empty($this->indexes())){
            try {
                $this->esc=new Esindex(self::$_db,self::$_tbName);
            } catch(Exception $ex){
                $this->logger->error($ex->getMessage());
            }

        }

        $this->gw = new TableGateway(self::$_tbName,$this->writeAdapter());

        if($this->dbReadCfg['port']==$this->dbWriteCfg['port'] && $this->dbReadCfg['host']==$this->dbWriteCfg['host']){

            $this->readAdapter=$this->writeAdapter;
            Yaf\Registry::set("mysql_adapter_r", $this->readAdapter);
            Yaf\Registry::set("mysql_adapter_rw", $this->writeAdapter);
            $this->gw_r=$this->gw = new TableGateway(self::$_tbName,$this->writeAdapter);
        } else {

            $this->gw_r = new TableGateway(self::$_tbName,$this->readAdapter());
        }

    }

    function readAdapter(){
        if($this->readAdapter=='') {
            $this->readAdapter = new Zend\Db\Adapter\Adapter(
                $this->dbReadCfg
            );
        }
        return $this->readAdapter;
    }
    function writeAdapter(){
        if($this->writeAdapter==''){
            $this->writeAdapter = new Zend\Db\Adapter\Adapter(
                $this->dbWriteCfg
            );
        }

        return $this->writeAdapter;
    }

    function gw(){

        if($this->gw==''){
            $this->gw = new TableGateway(self::$_tbName,$this->writeAdapter());
        }
        return $this->gw;

    }
    function gw_r(){
        if($this->gw_r==''){
            $this->gw_r = new TableGateway(self::$_tbName,$this->readAdapter());
        }
        return $this->gw_r;
    }
    function formatMsg($lang,$data){


        if($lang[$data]){
            return $lang[$data];
        }
        else if(preg_match("/^([\w-]+)__([\w-]+)$/i",$data,$ret)){
            return $lang[$data]?$lang[$data]:($lang[$ret[1]]." ".$lang[$ret[2]]);
        } else {
            return $data;
        }

    }
    function findOwn($ownerArr,$start=0,$len=30,$cols=""){
        if(empty($cols)){
            $cols="*";
        } else {
            if(is_array($cols)){
                $cols='`'.implode('`,`',$cols).'`';
            }
        }

        foreach($ownerArr as $key=>$val){
            $whereStr[]="$key='$val'";
        }
        $primary=$this->primary();
        $whereStr=empty($whereStr)?'':("where ".implode(" and ",$whereStr));
        $tbName=$this->tbName();
        $sql="select $cols from {$tbName} $whereStr order by $primary limit $start,$len";
        return $this->execSql($sql);
    }
     static function tbInfo(){

        $db=static::$_db;
        $tb=static::$_tbName;
        $index=static::$_indexes;
        if(is_null($db)){
            $cfg=Yaf\Application::app()->getConfig();
            $db=$cfg['database']->defdb;

        }
        return compact("db","tb","index");
    }
    function tbMeta()
    {
        return $this->_tbMeta;
    }
    function getTbMeta()
    {
        return $this->_tbMeta;
    }
    function find($whArrOrStr="", $limit=100, $cols='', $order='')
    {
        /**
         * $needCols : return the nessasary columns
         *    ex1:array('aliasName'=>'name','age') == 'select name as aliasName'
         *    ex2:array('name','age')
         *    ex3:name=userName&d
         */
        $selObj = $this->gw->getSql()->select();

        $needCols=$tempNeedCols=array();

        if(is_string($cols)){
            $cols=str_replace(',',"&",$cols);
            parse_str($cols,$tempNeedCols);
            foreach($tempNeedCols as $key=>$val){
                if(empty($val)){
                    $needCols[]=$key;
                    continue;
                }
                $needCols[$key]=$val;
            }
        }
        else {
            $needCols=$cols;
        }


        if(!empty($needCols) && is_array($needCols)){

            $selObj->columns($needCols);
        }
        if(!empty($whArrOrStr)){$selObj->where($whArrOrStr);}
        if(!empty($order)){
            $selObj->order($order);
        }
        if(!empty($limit)){
            $selObj->limit($limit);
        }

        return $this->gw->selectWith($selObj)->toArray();

    }
    function findOne($whArrOrStr='',$needCols=array())
    {
        /**
         *
         */

        $selObj = $this->gw->getSql()->select();

        if(is_string($needCols)){
            parse_str($needCols,$cols);
            $needCols=array();
            foreach($cols as $k=>$v)
            {
                if(empty($v)){$needCols[]=$k;}
                else {
                    $needCols[$k]=$v;

                }
            }


        }

        if(!empty($needCols) && is_array($needCols)){

            $selObj->columns($needCols);
        }

        if(!empty($whArrOrStr)){$selObj->where($whArrOrStr);}
        $selObj->limit(1);
        $res=$this->gw->selectWith($selObj)->toArray();
        return empty($res)?false:$res[0];
    }
    protected function findById($id,$cols=array())
    {

        return $this->findOne(array(
            $this->primary()=>$id
        ),$cols);
    }
    protected function findByUser($uid)
    {
        if(empty($uid)){
            $uid=$_SESSION['user']['uid'];
        }
        return $this->gw->select()->where(array(
            'uid'=>$uid
        ))->toArray();
    }
    function findAll()
    {
        return $this->find(array(),99999);
    }
    function execSql($sql,$data=array())
    {
        $adapter=$this->gw->getAdapter();
        /**
         * Adapter::QUERY_MODE_EXECUTE exec directly not use prepare sql
         * $adapter->query($sql,Adapter::QUERY_MODE_EXECUTE)
         *
         */
        $result=$adapter->query($sql,$data);
        $result->buffer();
        if(method_exists($result,"toArray")){
            return $result->toArray();
        } else if(method_exists($result,"count")) {
            return $result->count();
        }
        return $result;


    }
    function filter($data){
        return $data;
    }

    function restDelete($id){

    }
    function restPut($data){

    }
    function deleteOwn($ids,$ownIdData,$ownIdName='uid'){

        $ids="('".implode("','",$ids)."')";
        $primary=$this->primary();
        return $this->delete("$primary in $ids and $ownIdName='$ownIdData'");
    }
    function deleteThis($ids){

        $ids="('".implode("','",$ids)."')";
        $primary=$this->primary();
        return $this->delete("$primary in $ids");
    }
    function deleteMany($ids){

        $ids="('".implode("','",$ids)."')";
        $primary=$this->primary();

        return $this->delete("$primary in $ids");
    }
    function deleteOwnMany($ids,$ownIdData,$ownIdName='uid'){

        $ids="('".implode("','",$ids)."')";
        $primary=$this->primary();

        return $this->delete("$primary in $ids  and $ownIdName='$ownIdData'");
    }
    function findBy($idArr)
    {
        $selObj = $this->gw->getSql()->select();

        $primary=$this->_primary;
        $selObj->where(array(
            $primary => $idArr
        ));
        $order = sprintf('FIELD('.$primary.', %s)', implode(',', array_fill(0, count($idArr), Zend\Db\Sql\Expression::PLACEHOLDER)));
        $selObj->order(array(new Zend\Db\Sql\Expression($order, $idArr)));

        return $this->gw->selectWith($selObj)->toArray();
    }

    function tbMeta2($lang){
        $tbMeta=$this->tbMeta();
        $tbName=$this->tbName();
        foreach($tbMeta as $k=>&$v){
            if(!empty($v['enum'])){
                $v['enum']=array_map(function($item)use($lang,$k){
                    /**
                     * $k对应value
                     */
                    if(is_array($item)){
                        $item2=$item['txt'];
                        $kItem=$k.'_'.$item2;
                        $item['txt']=$lang[$kItem]?$lang[$kItem]:($lang[$item2]?$lang[$item2]:$item2);

                    } else {

                        $kItem=$k.'_'.$item;
                        $item=$lang[$kItem]?$lang[$kItem]:($lang[$item]?$lang[$item]:$item);

                    }
                    return $item;



                },$v['enum']);
            }
            if(empty($v['desc'])){
                $v['desc']=Misc_Utils::getTbLangVal($k,$tbName,$lang);

            } else {
                $v['desc']=$lang[$v['desc']]?$lang[$v['desc']]:$v['desc'];
            }



        }
        return $tbMeta;
    }

    public function add($arr)
    {
        foreach(array('ctime','dateline','mtime') as $key=>$val){
            if(isset($this->_tbMeta[$val]) && !isset($arr[$val])){
                $arr[$val]=time();
            }
        }

        return $this->insert($arr);


    }
    function updateOwn($set,$whArrOrStr)
    {

        return $this->gw->update($set, $whArrOrStr);

    }



    function execInsert($sql)
    {
        $adapter=$this->gw->getAdapter();

        return $adapter->query($sql, Adapter::QUERY_MODE_EXECUTE);


    }
    static function buildQuery($query,$type='in',$conj=' and '){
        $newQuery=[];
        foreach($query as $key=>&$val){
            if($type=='in'){
                $newQuery[]=self::arrToStr($val,$key,$type);
            } 
            
        }
        return implode(" $conj ",$newQuery);
    }
    static function arrToStr($arr,$key,$type='in'){

           return $key." $type ('".implode("','",$arr)."')";         

    }
    static function arrToInStr($arr,$key){

        return $key." in ('".implode("','",$arr)."')";

    }
      static function arrToNInStr($arr,$key){

        return $key." not in ('".implode("','",$arr)."')";

    }
    static function buildIn($ids,$key='_id',$isAssoc=false){
        return self::arrToInStr($ids,$isAssoc);

    }

    //$query=array(),$cols=array(),$order="",$startPage=1,$pageSize=20)
    function findNIn($query=array(),$cols=array(),$order="",$startPage=1,$pageSize=20){
        return $this->pageFind($this->buildQuery($query),$cols,$order,$startPage,$pageSize);
    }
 
    function pageFind($query,$cols,$order,$startPage,$pageSize){

        return $this->find($query,$pageSize,$cols,$order,$startPage);
    }
    function findInOwn($query=array(),$own,$cols=array(),$order="",$startPage=1,$pageSize=20){
        $query=$this->buildQuery($query,'in');
        $query[$this->ownerId()]=$own;
        return $this->pageFind($query,$cols,$order,$startPage,$pageSize);
    }
    function replace($set){

        return $this->gw->replace($set);

    }

    function update($set,$whArrOrStr,$oldArr='')
    {
        $update=$this->gw->update($set, $whArrOrStr);
        if(!empty($this->indexes()) && is_array($whArrOrStr)){
            $pId=$this->primary();
            $pIdVal=$whArrOrStr[$pId];

            if(isset($pIdVal)){
                if($oldArr==''){
                    $oldArr=$this->findOne(array($pId=>$pIdVal));

                }
                $oldArr=array_merge($oldArr,$set);
                if($this->esc && !empty($this->indexes())){
                    $indexes=Misc_Utils::array_pluck($oldArr,$this->indexes());
                    $array[$pIdVal]=$indexes;
                    $this->esc->updateIndex($array);
                }

            }

        }
        return $update;

    }
//$insertIds  = array();
//for ($x = 0; $x < sizeof($filtername); $x++) {
//$orders = array(
//'poid'              => null,
//'order_id'          => $poid,
//'item_desc'         => $filtername[$x],
//'item_qty'          => $filterquantity[$x],
//'item_price'        => $filterprice[$x],
//'total'             => $filtertotal[$x],
//'cash_on_delivery'  => $val_delivery,
//'is_check'          => $val_check,
//'bank_transfer'     => $val_transfer,
//'transaction_date'  => $dateorder
//);
//$this->db->insert('po_order', $orders);
//$insertIds[$x]  = $this->db->insert_id(); //will return the first insert array

    function delete($whArrOrStr,$oldArr='')
    {
        if(!empty($this->indexes())){

            $pId=$this->primary();

            if(is_array($whArrOrStr) && isset($whArrOrStr[$pId])){
//                if($oldArr==''){
//
//                    $oldArr=$this->find($whArrOrStr,9999999,$pId);
//                    $newArr=array();
//                    foreach($oldArr as $k=>$v){
//                        $newArr[$v[$pId]]=array();
//                    }
//                    $oldArr=$newArr;
//                } else {
//                    $arr=array();
//                    $arr[$pId]=1;
//                    $oldArr=array_fill_keys($oldArr,$arr);
//                }
                if($this->esc && !empty($this->indexes())) {
                    $this->esc->deleteIndex(array($whArrOrStr[$pId]=>array($pId=>$whArrOrStr[$pId])));
                }

            }

        }

        return $this->gw->delete($whArrOrStr);
    }

    /**
     * @param $tb
     * @param $docs Elastica\Document's array or Elastica\Document or array
     * @param array $typeOpts
     * array(
    'number_of_shards' => 4,
    'number_of_replicas' => 1,
    'analysis' => array(
    'analyzer' => array(
    'indexAnalyzer' => array(
    'type' => 'custom',
    'tokenizer' => 'standard',
    'filter' => array('lowercase', 'mySnowball')
    ),
    'searchAnalyzer' => array(
    'type' => 'custom',
    'tokenizer' => 'standard',
    'filter' => array('standard', 'lowercase', 'mySnowball')
    )
    ),
    'filter' => array(
    'mySnowball' => array(
    'type' => 'snowball',
    'language' => 'German'
    )
    )
    )
    )
     */
    function exists($data,$cols=array()){

        $result=$this->findOne($data,$cols);

        return !empty($result);

    }
    function batchInsert($set,$option=''){

        return $this->insert($set,true);
    }
    function insert($set,$isBatch=false)
    {


        if($isBatch || is_array($set[0]))
        {
            $cols=array_keys($set[0]);
            $cols='(`'.implode('`,`',$cols).'`)';



            $vals=implode(",",array_map(function($item){return "('".implode("','",array_values($item))."')";},$set));
            $tbName=$this->tbName();
            $sql="insert into {$tbName}$cols values{$vals}";


            $ret=$this->execInsert($sql);
            $retId=$this->gw->adapter->getDriver()->getLastGeneratedValue();
        }
        else
        {


            $this->gw->insert($set);
            $retId=$this->gw->adapter->getDriver()->getLastGeneratedValue();
            if(!empty($this->indexes()) && $this->esc){
                $indexes=Misc_Utils::array_pluck($set,$this->indexes());
                $array=array();
                $array[$retId]=$indexes;
                $this->esc->createIndex($array);

            }

        }
        return  $retId;

        /**
         *
         * this can not return null when insert multiple value
         * return $this->gw->lastInsertValue;
         */


    }

    function distinct($key,$whArrOrStr,$cols=array()){

        $selObj = $this->gw->getSql()->select();

        $needCols=$tempNeedCols=array();

        if(is_string($cols)){
            $cols=str_replace(',',"&",$cols);
            parse_str($cols,$tempNeedCols);
            foreach($tempNeedCols as $key=>$val){
                if(empty($val)){
                    $needCols[]=$key;
                    continue;
                }
                $needCols[$key]=$val;
            }
        }
        else {
            $needCols=$cols;
        }


        if(!empty($needCols) && is_array($needCols)){
            $needCols[]=new Expression("DISTINCT($key) as $key");
            $selObj->columns($needCols);
        }
        if(!empty($whArrOrStr)){$selObj->where($whArrOrStr);}
        return $this->gw->selectWith($selObj)->toArray();
    }
    function restPost($data){
        $tbMeta=$this->tbMeta();

        return $this->add($data);
    }


}