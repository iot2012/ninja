<?php
class UserShopModel extends SysModel {


    static $_tbName = 'user_shop';
    static $_primary = 'id';



    protected $_tbMeta=array (
  'id' => 
  array (
    'reg' => 'ui4','rights'=>4
  ),
  'uid' => 
  array (
    'reg' => 'i8','rights'=>4
  ),
  'name' => 
  array (
    'lt' => '42',
    'reg' => 'cn_en',
  ),

        'cls_id' =>
            array (
                'reg' => 'i4','type'=>'tree-select','data_source'=>array('tb'=>'ShopCls','val'=>'id','field'=>'name'),'data_extra'=>'data-tree=radio'
            ),

  'logo' => 
  array (
      'thumbnail'=>false,
      'acceptExts'=>array('image/*'),
      'fileSize'=>524288,
      'maxFiles'=>1,
        'lt' => '1024',
        'reg' => 'file',
      'data_extra'=>'data-role=upload',
  ),
  'ctime' => 
      array (
        'reg' => 'timestamp',
      ),
);

    function __construct(){

        parent::__construct();

    }

    function exists($data){

        return  $this->findOne($data);

    }


}