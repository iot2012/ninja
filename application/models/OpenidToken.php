
<?php
class OpenidTokenModel extends SysModel {


    static $_tbName  = 'openid_token';
    static $_primary  = 'tid';


    function getOne($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        $primary=$this->primary();
        return  $this->findOne(array($primary=>$data));

    }

    function deleteToken($uid,$tid){
        return $this->delete(array('id'=>$tid,'uid'=>$uid));
    }

    function getTokens($uid){
        $tbName=$this->tbName();
        return $this->execSql("select sns_id,screen_name,profile_image_url,atoken,atoken_secret,rtoken,provider,ctime,uid,defshow,isshow,expires_in,id
        from $tbName where uid=? group by provider",array($uid));

    }

}