<?php

class IbeaconWechatMModel extends MongoSysModel {


    static $_tbName  = 'ibeacon_wechat';
    static $_primary  = '_id';

    /**
     * validate each field in tables
     * @todo store get  message in i18n file
     */

    /**
     *    'gt'=>'',
     *    'get'=>'',
     *    'let'=>'',
     * @var array
     */


    protected $_tbMeta=array(
        'uuid'=>array(
            'reg'=>'uuid',
            'eq'=>'36','rights'=>4

        ),
        'major'=>array('reg'=>"ui2",'rights'=>4),
        'minor'=>array('reg'=>"ui2",'rights'=>4),
        'mac'=>array('reg'=>"mac2"),
        'qrticket'=>array('reg'=>"desc",'rights'=>4,"lt"=>512),
        'connect_protocol'=>array('reg'=>"ui1",'rights'=>8,'def'=>'3|2|1'), //1:android classic bluetooth 2:ios classic bluetooth 3:ble 4:wifi
        'device_type'=>array('reg'=>"en_name","eq"=>"15"),
        'auth_key'=>array('reg'=>"varchar","eq"=>"32",'rights'=>8),
        'close_strategy'=>array('reg'=>"ui1","let"=>"25",'rights'=>8),
        /**
         * 连接策略，32位整型，按bit位置位，目前仅第1bit和第3bit位有效（bit置0为无效，1为有效；第2bit已被废弃），且bit位可以按或置位（如1|4=5），各bit置位含义说明如下：
        1：（第1bit置位）在公众号对话页面，不停的尝试连接设备
        4：（第3bit置位）处于非公众号页面（如主界面等），微信自动连接。当用户切换微信到前台时，可能尝试去连接设备，连上后一定时间会断开
        8：（第4bit置位），进入微信后即刻开始连接。只要微信进程在运行就不会主动断开
         */
        'conn_strategy'=>array('reg'=>'ui1','check'=>array('1'=>'in_offwc_conn','4'=>'out_offwc_conn','8'=>'in_wechat_conn'),'rights'=>8),
        /**
         * auth version，设备和微信进行auth时，会根据该版本号来确认auth buf和auth key的格式（各version对应的auth buf及key的具体格式可以参看“客户端蓝牙外设协议”），该字段目前支持取值： 0：不加密的version 1：version 1
         */
        'auth_ver'=>array('reg'=>'ui0','rights'=>8),
        /**
         * auth加密方法，目前支持两种取值： 0：不加密 1：AES加密（CBC模式，PKCS7填充方式）
         */
        'crypt_method'=>array('reg'=>'ui0','rights'=>8),
        'auth_ver'=>array('reg'=>'ui0','rights'=>8),
        /**
         * 表示mac地址在厂商广播manufature data里含有mac地址的偏移，取值如下： -1：在尾部、 -2：表示不包含mac地址 其他：非法偏移
         */
        'manu_mac_pos'=>array('reg'=>'i1','rights'=>8),
        /**
         * 表示mac地址在厂商serial number里含有mac地址的偏移，取值如下： -1：表示在尾部 -2：表示不包含mac地址 其他：非法偏移
         */
        'ser_mac_pos'=>array('reg'=>'i1','rights'=>8),
        /**
         * 1：设备更新（更新已授权设备的各属性值）
         */
        'op_type'=>array('reg'=>"ui0",'rights'=>8),
        'ctime'=>array('reg'=>'timestamp','rights'=>4)

    );
    protected $err=array('reg'=>array());

    function __construct(){

        parent::__construct();


    }
    function apiAccount($userId,$user){
        $account=new WechatOfficialModel();
        $query=['userId'=>$userId];
        if($user['level']!=9){
            $query['uid']=$user['uid'];

        }
        $account=new WechatOfficialModel();
        return $account->findOne($query);





    }
    function add(&$data,$user){
        $account=$this->apiAccount($data['device_type'],$user);
        if(empty($account)){
            $data['_ret']['code']=-1;
            $data['_ret']['msg']='err_fmt';
        }
        $wc = new WechatIot($account);
        $newArr=$wc->batchAuthDevice($data);
        if($newArr==false){
            $data['_ret']['code']=-1;
            $data['_ret']['msg']='err_fmt';
            return false;

        }
        return parent::add($newArr);


    }



}