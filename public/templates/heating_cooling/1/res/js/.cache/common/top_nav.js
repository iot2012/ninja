/*TMODJS:{"version":21,"md5":"0227c3dede8df31e1d44c9d801eadb8c"}*/
template('common/top_nav',function($data,$filename
/**/) {
'use strict';var $utils=this,$helpers=$utils.$helpers,brand=$data.brand,$escape=$utils.$escape,signIn=$data.signIn,_l=$data._l,$each=$utils.$each,menus=$data.menus,val=$data.val,i=$data.i,subItem=$data.subItem,j=$data.j,nav_search=$data.nav_search,$out='';$out+='<div class="navbar navbar-default navbar-fixed-top" role="navigation"> <div class="container"> <div class="navbar-header"> <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button> ';
if(brand){
$out+=' <a class="navbar-brand" href="index.html"> ';
if(brand.img){
$out+=' <img src="';
$out+=$escape(brand.img);
$out+='" alt="..."> ';
}else{
$out+=' <span>';
$out+=$escape(brand.title);
$out+='</span> ';
}
$out+=' </a> ';
}
$out+=' </div> <div class="collapse navbar-collapse"> ';
if(signIn){
$out+=' <a class="btn btn-theme-primary navbar-btn navbar-right hidden-sm hidden-xs">';
$out+=$escape(_l.signIn);
$out+='</a> ';
}
$out+=' <ul class="nav navbar-nav navbar-right"> ';
$each(menus,function(val,i){
$out+=' <li class="dropdown';
if(i==0){
$out+='active';
}
$out+='"> <a href="';
$out+=$escape(val.href);
$out+='" ';
if(val.subs){
$out+='class="dropdown-toggle" data-toggle="dropdown"';
}
$out+='>';
$out+=$escape(val.name);
$out+=' ';
if(val.subs){
$out+='<b class="caret"></b>';
}
$out+='</a> ';
if(val.subs){
$out+=' <ul class="dropdown-menu"> ';
$each(val.subs,function(subItem,j){
$out+=' <li><a href="';
$out+=$escape(subItem.href);
$out+='">';
$out+=$escape(subItem.name);
$out+='</a></li> ';
});
$out+=' </ul> ';
}
$out+=' </li> ';
});
$out+='  ';
if(nav_search){
$out+=' <li class="hidden-xs hidden-sm" id="navbar-search"> <a href="index.html#"> <i class="fa fa-search"></i> </a> <div class="hidden" id="navbar-search-box"> <div class="input-group"> <input type="text" class="form-control" placeholder="Search"> <span class="input-group-btn"> <button class="btn btn-default" type="button">Go!</button> </span> </div> </div> </li> ';
}
$out+=' </ul> ';
if(nav_search){
$out+='  <form class="navbar-form navbar-right visible-xs visible-sm" role="search"> <div class="input-group"> <input type="text" class="form-control" placeholder="Search"> <span class="input-group-btn"> <button class="btn btn-theme-primary" type="button">Search!</button> </span> </div> </form> ';
}
$out+=' </div> </div> </div>';
return new String($out);
});