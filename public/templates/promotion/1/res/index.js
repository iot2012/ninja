var dataReadyCb;
if(typeof seajs=='function'){
    seajs.config(
        {base: '/templates/bbs/assets/js'}
    );

}



var isImgKey=function(key){
    var fileKeys=['img','icon','avatar','avatars','imgs','icons','logo','logos']
    var result=fileKeys.map(function(item){
        var regExp=new RegExp(item+'$|\[[\w-]+'+item+'\]','i');
        return regExp.test(key);

    });
    return result.indexOf(true)>=0;

}
/**
 * 每个页面对应哪个section数据
 */

var config={
    t_section:{
        'index':'index'
    },
    imgdir:'/'
};
var tmpData={
    'content':{
        'content':'东方财富网研报中心提供沪深两市最全面的上市公司公告信息,第一时间提供各上市公司最新公告,深入解析上市公司最新变化、重大事项',
        'logo':'https://www.baidu.com/img/baidu_jgylogo3.gif',
        'title':'标题',
        'link':'http://baidu.com'
    }

}
$(function(){
    $("script[type='text/html']").each(function(idx,item)   {
        console.log('tmpData',tmpData);
        var id=$(item).attr("id").replace(/_t_/,'');
        $(item).replaceWith(template(id,$.extend({_c:config},tmpData[id])));
    });



});
if((typeof sendAuth=='function')){
    $.ajaxSetup({
        beforeSend:sendAuth,
        timeout:10000

    });
    $("body").on('submit',"form.ajax-form",ajaxSubmitHandler);

}

/**
 *  prevent submitting multiple time in short period
 */


$("body").on("click","a[href='#back']",function(){
    history.back();
    return false;
});