var gulp = require('gulp'),
    minifyHtml = require("gulp-minify-html");
    less = require("gulp-less");

var test=function(a,b,c){
  console.log("test",a,b,c);

}
var  jshint = require("gulp-jshint");    
//var imagemin = require('gulp-imagemin');
//var pngquant = require('imagemin-pngquant'); //png图片压缩插件
var livereload = require('gulp-livereload');
// gulp.task('img-min', function () {
//     return gulp.src('assets/img/*.(jpg|jpeg|png|gif)')
//         .pipe(imagemin({
//             progressive: true,
//             use: [pngquant()] //使用pngquant来压缩png图片
//         }))
//         .pipe(gulp.dest('dist/img'));
// });
gulp.task('compile-less', function () {
    gulp.src('assets/less/*.less')
        .pipe(less())
        .pipe(gulp.dest('assets/css'));
});
gulp.task('compile-less', function () {
    gulp.src('assets/less/*.less')
        .pipe(less())
        .pipe(gulp.dest('assets/css')).pipe(livereload());
});
gulp.task('reload',function(){
    //do something
    // gulp.src('assets/**/*.css')
    //     .pipe(livereload());
});
gulp.task('minify-js', function () {
    gulp.src('assets/**/*.js') // 要压缩的html文件
        .pipe(minifyHtml()) //压缩
        .pipe(gulp.dest('dist/html'));
});
gulp.task('minify-html', function () {
    gulp.src('assets/**/*.html') // 要压缩的html文件
        .pipe(minifyHtml()) //压缩
        .pipe(gulp.dest('dist/html'));
});
//gulp.watch(['assets/**/*.(js|html|htm)'], ['reload']);

gulp.task('lintHTML', function() {
  return gulp.src('assets/**/*.html')
    // if flag is not defined default value is 'auto' 
    .pipe(jshint.extract('auto|always|never'))
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});
gulp.task('lint', function() {
  return gulp.src('assets/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default', { verbose: true }));
});

gulp.task('watch', function() {
       livereload.listen();
        //livereload.listen(9000);
      gulp.watch(['assets/**/*.html','assets/**/*.htm','*.html','*.htm']).on('change', livereload.reload
    //      gulp.src(file.path)
    // // if flag is not defined default value is 'auto' 
    //  //reporter('default') 继续执行如果发现错误 reporter('fail')停止执行 reporter('jshint-stylish')格式化
    //     .pipe(jshint.extract('auto|always|never'))
    //     .pipe(jshint())
    //     .pipe(jshint.reporter('jshint-stylish'))
        //.pipe(jshint.reporter('fail'));
          );
        gulp.watch([ 'assets/**/*.js','widget/**/*.js']).on('change', livereload.reload
        //  gulp.src(file.path)
        // .pipe(jshint())
        // .pipe(jshint.reporter('jshint-stylish'));
        //.pipe(jshint.reporter('fail'));
        //.pipe(jshint.reporter('default', { verbose: true }));
         );

        gulp.watch(['assets/**/*.css']).on('change',livereload.changed);

        gulp.watch(['assets/**/*.less'],['reload-less']);
       // gulp.watch(['assets/**/*.css']).on('change', livereload.changed);
       
  //要在这里调用listen()方法
  // 看守所有.scss档
  // gulp.watch('src/styles/**/*.scss', ['styles']);

  // // 看守所有.js档
  // gulp.watch('src/scripts/**/*.js', ['scripts']);

    //gulp.watch(['less/*.less','assets/**/*.(js|html|htm|css)'], ['reload']);
});



// var gulp = require('gulp'),  
//     sass = require('gulp-ruby-sass'),
//     autoprefixer = require('gulp-autoprefixer'),
//     minifycss = require('gulp-minify-css'),
//     jshint = require('gulp-jshint'),
//     uglify = require('gulp-uglify'),
//     imagemin = require('gulp-imagemin'),
//     rename = require('gulp-rename'),
//     clean = require('gulp-clean'),
//     concat = require('gulp-concat'),
//     notify = require('gulp-notify'),
//     cache = require('gulp-cache'),
//     livereload = require('gulp-livereload');

// 样式
// gulp.task('styles', function() {  
//   return gulp.src('src/styles/main.scss')
//     .pipe(sass({ style: 'expanded', }))
//     .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
//     .pipe(gulp.dest('dist/styles'))
//     .pipe(rename({ suffix: '.min' }))
//     .pipe(minifycss())
//     .pipe(gulp.dest('dist/styles'))
//     .pipe(notify({ message: 'Styles task complete' }));
// });

// // 脚本
// gulp.task('scripts', function() {  
//   return gulp.src('src/scripts/**/*.js')
//     .pipe(jshint('.jshintrc'))
//     .pipe(jshint.reporter('default'))
//     .pipe(concat('main.js'))
//     .pipe(gulp.dest('dist/scripts'))
//     .pipe(rename({ suffix: '.min' }))
//     .pipe(uglify())
//     .pipe(gulp.dest('dist/scripts'))
//     .pipe(notify({ message: 'Scripts task complete' }));
// });

// // 图片
// gulp.task('images', function() {  
//   return gulp.src('src/images/**/*')
//     .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
//     .pipe(gulp.dest('dist/images'))
//     .pipe(notify({ message: 'Images task complete' }));
// });

// // 清理
// gulp.task('clean', function() {  
//   return gulp.src(['dist/styles', 'dist/scripts', 'dist/images'], {read: false})
//     .pipe(clean());
// });

// // 预设任务
// gulp.task('default', ['clean'], function() {  
//     gulp.start('styles', 'scripts', 'images');
// });