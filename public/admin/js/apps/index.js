$.subscribe('order_paid',function(e,data){
    var app=allApps.findWhere({'_id':data['item_id']});
    app.set('_is_local',1);
    myApps.add(app);
});

var autoSearch= _.debounce(function () {

    var idName=$("#apps_li>li.active>a").attr("href");


    var filterLis = $(idName).find(".search-item");

    var rex = new RegExp($(this).val(), 'i');
    filterLis.hide();
    filterLis.filter(function () {
        return rex.test($(this).find('.search_item_txt').text());
    }).show();
},300);
var doGlobalSearch2= _.debounce(autoSearch,300);
$(".search_keyword").on('keyup',doGlobalSearch2);

(typeof myApps!='undefined') &&  myApps.on('add',function(addedModel,curCols){

    var appLi='<li class="search-item"> <div class="app_mask"><span class="fa fa-remove app-remove" data="<%=_id%>"></span></div> <%if(icon_url && icon_url.indexOf(" ")!==-1){%> <span class="<%=icon_url%>"></span> <% } else {%> <img src="<%=icon_url%>"> <% }%> <span class="glyphicon-class search_item_txt"><%=name%></span> </li>';
    var liHtml=template.compile(appLi)(addedModel.toJSON());
    $("#added_apps>ul").append(liHtml);
    var appModal=$("#pod_modal_"+addedModel.get("_id"));
    if(appModal.size()>0){
        appModal.find(".app-install").text(config.lang['installed']).prop('disabled',true);
    }
    if(addedModel.get('_is_local')!=1){
        addedModel.save({ids:addedModel.get("_id")},{patch:true,url:myApps.url});
    }

});

var Z=new z();





$("#all_apps .app-item").on('click',function(e){
    var self=this;

    var appId=$(this).attr("id").replace(/m_/,'');
    Z.getHtmlFrag("app_detail.html",function(html){
        var app=allApps.findWhere({_id:appId});
        var appData=app.toJSON();
        var price=config.lang.get;
        if(appData.price>(1e-6)){
            price=currency[(appData.currency==""?1:appData.currency)]+appData['price'];

        }
        var isBought=false && myApps.findWhere({_id:appId});
        var data=$.extend(appData,{isBought:isBought,title:capStr($(self).find('.search_item_txt').text())},{l_price:price},langPluck(config.lang,['installed','from','desc','capimg','app','install','comment']));
        var modal=template.compile(html)(data);
        var modal=$(modal);

        modal.modal({backdrop:true,keyboard:false}).on('shown.bs.modal',function(e){

            $(this).next().remove();
        });


    });
    e.preventDefault();
    return false;

});

$("body").on("click",".app-install",function(){
    var self=this;
    var appId=$(this).attr('data');
    var price=$(this).attr('data-price');
    var appText=$(this).text();
    if(price>1e-6){
        $(this).text(config.lang.buy);
    } else {
        $(this).text(config.lang.install);
    }
    if(appText==config.lang.buy){

        openWindow("/pay/index?item_type=app&item_id="+appId);


    } else if(appText==config.lang.install){
        var app=allApps.findWhere({'_id':appId});
        $(this).html('<span class="fa fa-check"></span>'+config.lang.insted_app).prop('disabled',true);

        myApps.add(app);
    }

});

if(screen.availWidth<=500){

    $("#search_word").on('click',function(){
        $(this).parent().parent().parent().find("li:not(.active,.pull-right)").hide();
        $(this).animate({width:200});
        $(this).focus();
    }).on('blur',function(){
        $(this).animate({width:60},400,'swing',function(){
            $(this).parent().parent().parent().find("li:not(.active,.pull-right)").show();
        });


    });
}
$("#added_apps").on("click",".app-remove",function(){
    var self=this;
    var obj= $(self).parent().parent();
    setUpRmPopover(obj,config.lang.un_app,{'trigger':'manual','placement':'bottom'},{ok:function(){
        obj.popover('destroy');
        obj.remove();
        var app=myApps.findWhere({_id:$(self).attr('data')});

        if(app){app.destroy();}
    }});
    obj.popover('show');


});