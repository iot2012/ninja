//var left_side_width = 220; //Sidebar width in pixels
//

var setFlotLegend=function(obj){
    var canvasDiv=$(obj.chart.canvas).parent();
    var legend=canvasDiv.find(".legend");
    if(legend.size()==0){
    var temp='<div class="legend"> <div style="position: absolute; width: 25px; height: 30px; top: 5px; right: 5px; opacity: 0.85; background-color: rgb(255, 255, 255);"> </div> <table style="position:absolute;top:5px;right:5px;;font-size:smaller;color:#545454"> <tbody> <%for(var i=0;i<legend.length;i++){%> <tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid <%=legend[i].pointColor%>;overflow:hidden"></div></div></td><td class="legendLabel"><%=legend[i].label%></td></tr> <%}%> </tbody></table></div>';

    var legendHtml=template.compile(temp)({legend:obj.datasets});
    canvasDiv.prepend(legendHtml);

    }


}

$(function() {
    seajs.use(['jquery.flot'],function(){
        seajs.use(['jquery-ui.min','jquery.slimscroll','jquery.flot.resize','jquery.flot.pie','flot.tooltip','curvedLines','flot.spline.index','metisMenu','icheck','jquery.peity','sparkline.index','Chart.min.js'],function(){



            var pieChartData = [
                { label: config.lang.female, data: 16.8, color: "#62cb31"},
                { label: config.lang.male, data: 6.3, color: "#A4E585"}

            ];

            /**
             * Pie Chart Options
             */
            var pieChartOptions = {
                series: {
                    pie: {
                        show: true
                    }
                },
                grid: {
                    hoverable: true
                },
                tooltip: true,
                tooltipOpts: {

                    content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
                    shifts: {
                        x: 20,
                        y: 0
                    },
                    defaultTheme: false
                }
            };

            $.plot($("#flot-pie-chart"), pieChartData, pieChartOptions);

            // Initialize animate panel function


            // Sparkline bar chart data and options used under Profile image on left navigation panel
            $("#sparkline1").sparkline([5, 6, 7, 2, 0, 4, 2, 4, 5, 7, 2, 4, 12, 11, 4], {
                type: 'bar',
                barWidth: 7,
                height: '30px',
                barColor: '#62cb31',
                negBarColor: '#53ac2a'
            });



            $("span.pie").peity("pie", {
                fill: ["#62cb31", "#edf0f5"]
            })

            $(".line").peity("line",{
                fill: '#62cb31',
                stroke:'#edf0f5',
            })

            $(".bar").peity("bar", {
                fill: ["#62cb31", "#edf0f5"]
            })

            $(".bar_dashboard").peity("bar", {
                fill: ["#62cb31", "#edf0f5"]
            });
            /**
             * 单店访客详情
             * @type {string[]}
             */
            var appleDays=['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30'];
            var appleDaysVals1=[83, 93, 60, 85, 82, 74, 61, 178, 89, 101, 152, 140, 160,97, 65, 86, , 79, 54, 53, 58, 122, 111, 79, 101, 98, 106, 52,79, 137, 128];

            var sharpLineData = {
                labels: appleDays,
                datasets: [
                    {
                        label:  config.lang.new_official_account,
                        fillColor: "rgba(98,203,49,0.5)",
                        strokeColor: "rgba(98,203,49,0.7)",
                        pointColor: "rgba(98,203,49,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(98,203,49,1)",
                        data: appleDaysVals1
                    }
                ]
            };

            var sharpLineOptions = {

                scaleShowGridLines : true,
                scaleGridLineColor : "rgba(0,0,0,.05)",
                scaleGridLineWidth : 1,
                bezierCurve : false,
                pointDot : true,
                pointDotRadius : 4,
                pointDotStrokeWidth : 1,
                pointHitDetectionRadius : 20,
                datasetStroke : true,
                datasetStrokeWidth : 1,
                datasetFill : true,
                responsive: true,

                onAnimationComplete: function(){
                    setFlotLegend(this);


                }
            };

            var ctx = document.getElementById("visitor_stat").getContext("2d");
            var myNewChart = new Chart(ctx).Line(sharpLineData, sharpLineOptions);



            //var lineData = {
            //    labels: appleDays,
            //    datasets: [
            //        {
            //            label: config.lang.new_official_account,
            //            fillColor: "rgba(220,220,220,0.5)",
            //            strokeColor: "rgba(220,220,220,1)",
            //            pointColor: "rgba(220,220,220,1)",
            //            pointStrokeColor: "#fff",
            //            pointHighlightFill: "#fff",
            //            pointHighlightStroke: "rgba(220,220,220,1)",
            //            data: appleDaysVals1
            //        }
            //
            //    ]
            //};
            //
            //var lineOptions = {
            //    scaleShowGridLines : true,
            //    scaleGridLineColor : "rgba(0,0,0,.05)",
            //    scaleGridLineWidth : 1,
            //    bezierCurve : true,
            //    bezierCurveTension : 0.4,
            //    pointDot : true,
            //    pointDotRadius : 4,
            //    pointDotStrokeWidth : 1,
            //    pointHitDetectionRadius : 20,
            //    datasetStroke : true,
            //    datasetStrokeWidth : 1,
            //    datasetFill : true,
            //    responsive: true
            //};
            //

            //var ctx = document.getElementById("visitor_stat").getContext("2d");
            //var myNewChart = new Chart(ctx).Line(lineData, lineOptions);



            /**
             * 单个苹果月/周曲线
             */
            var appleDays=['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30'];
            var appleDaysVals1=[77, 65, 86, , 79, 54, 53, 58, 122, 111, 79, 101, 98, 106, 52,79, 83, 93, 60, 85, 82, 74, 61, 178, 89, 101, 152, 140, 160, 137, 128];
            var appleDaysVals2=[54, 53, 58, 122, 111, 79, 101, 98, 106,  152, 140, 160, 137, 128,52, 178, 89, 101,77, 65, 86, 79, 83, 93, 60, 85, 82, 74, 61, 79 ];
            var lineData = {

                labels: appleDays,
                datasets: [
                    {
                        label: config.lang.total_follows,
                        fillColor: "rgba(220,220,220,0.5)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: appleDaysVals1
                    },
                    {
                        label: config.lang.total_enter,
                        fillColor: "rgba(98,203,49,0.5)",
                        strokeColor: "rgba(98,203,49,0.7)",
                        pointColor: "rgba(98,203,49,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(26,179,148,1)",
                        data: appleDaysVals2
                    }
                ]
            };

            var lineOptions = {
                tooltip: true,
                legend: {
                    show: true,
                    labelBoxBorderColor: "#ffffff",
                    position: "ne",
                    margin: 100,
                    backgroundColor: "red"

                },
                scaleShowGridLines : true,
                scaleGridLineColor : "rgba(0,0,0,.05)",
                scaleGridLineWidth : 1,
                bezierCurve : true,
                bezierCurveTension : 0.4,
                pointDot : true,
                pointDotRadius : 4,
                pointDotStrokeWidth : 1,
                pointHitDetectionRadius : 20,
                datasetStroke : true,
                datasetStrokeWidth : 1,
                datasetFill : true,
                responsive: true,
                onAnimationComplete: function(){
                    setFlotLegend(this);


                }
            };


            var ctx = document.getElementById("lineOptions").getContext("2d");
            var myNewChart = new Chart(ctx).Line(lineData, lineOptions);
            /**
             * 环境卫士
             * @type {{scaleBeginAtZero: boolean, scaleShowGridLines: boolean, scaleGridLineColor: string, scaleGridLineWidth: number, barShowStroke: boolean, barStrokeWidth: number, barValueSpacing: number, barDatasetSpacing: number, responsive: boolean}}
             */
            var singleBarOptions = {
                scaleBeginAtZero : true,
                scaleShowGridLines : true,
                scaleGridLineColor : "rgba(0,0,0,.05)",
                scaleGridLineWidth : 1,
                barShowStroke : true,
                barStrokeWidth : 1,
                barValueSpacing : 5,
                barDatasetSpacing : 1,
                responsive:true
            };

            /**
             * 红包发送与打开统计
             */
            var appleHot10=[config.lang.red_locket_sent ,config.lang.red_locket_open];
            var appleHot10Vals=[40, 20];
            var singleBarData = {
                labels: appleHot10,
                datasets: [
                    {
                        label: config.lang.red_locket_stat,
                        fillColor: "rgba(98,203,49,0.5)",
                        strokeColor: "rgba(98,203,49,0.8)",
                        highlightFill: "rgba(98,203,49,0.75)",
                        highlightStroke: "rgba(98,203,49,1)",
                        data: appleHot10Vals
                    }
                ]
            };

            var ctx = document.getElementById("red_locket_stat").getContext("2d");
            var myNewChart = new Chart(ctx).Bar(singleBarData, singleBarOptions);




            /**
             * 环境卫士访问统计
             */
            var appleHot10=["朝阳区A区" ,"朝阳区C区", "朝阳区D区", "朝阳区E区", "朝阳区F区", "朝阳区G区", "朝阳区H区","朝阳区I区","朝阳区J区","朝阳区K区"];
            var appleHot10Vals=[40, 20, 30, 100, 30, 50, 60,30,90,80];
            var singleBarData = {
                labels: appleHot10,
                datasets: [
                    {
                        label: "My Second dataset",
                        fillColor: "rgba(98,203,49,0.5)",
                        strokeColor: "rgba(98,203,49,0.8)",
                        highlightFill: "rgba(98,203,49,0.75)",
                        highlightStroke: "rgba(98,203,49,1)",
                        data: appleHot10Vals
                    }
                ]
            };

            var ctx = document.getElementById("singleBarOptions").getContext("2d");
            var myNewChart = new Chart(ctx).Bar(singleBarData, singleBarOptions);





            /**
             * Flot charts data and options
             */
            var data1 = [[0, 55], [1, 48], [2, 40], [3, 36], [4, 40], [5, 60], [6, 50], [7, 51]];
            var data2 = [[0, 56], [1, 49], [2, 41], [3, 38], [4, 46], [5, 67], [6, 57], [7, 59]];

            var chartUsersOptions = {
                series: {
                    splines: {
                        show: true,
                        tension: 0.4,
                        lineWidth: 1,
                        fill: 0.4
                    }
                },
                grid: {
                    tickColor: "#f0f0f0",
                    borderWidth: 1,
                    borderColor: 'f0f0f0',
                    color: '#6a6c6f'
                },
                colors: ["#62cb31", "#efefef"]
            };

            $.plot($("#flot-line-chart"), [data1, data2], chartUsersOptions);

            /**
             * Flot charts 2 data and options
             */
            var chartIncomeData = [
                {
                    label: "line",
                    data: [[1, 10], [2, 26], [3, 16], [4, 36], [5, 32], [6, 51]]
                }
            ];

            var chartIncomeOptions = {
                series: {
                    lines: {
                        show: true,
                        lineWidth: 0,
                        fill: true,
                        fillColor: "#64cc34"

                    }
                },
                colors: ["#62cb31"],
                grid: {
                    show: false
                },
                legend: {
                    show: false
                }
            };

            $.plot($("#flot-income-chart"), chartIncomeData, chartIncomeOptions);



        });

    });


        // Add special class to minimalize page elements when screen is less than 768px



});
