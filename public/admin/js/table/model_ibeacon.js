
bdMapCbs={};


seajs.use(["http://api.map.baidu.com/components?ak=BD0wOR97fUQM0pAvKG9LkU6z&v=1.0"],function(){

    tbOpCbs['always']=function(data,formName,modalId){



        var tbName=modalId;
        var formObj=$("#"+tbName+" form");
        var gpsName=formObj.find("input[name=bd_gps]");

        if(gpsName.size()==0){
            formObj.append($("<input>").attr({'name':'bd_gps','type':'hidden'}));
        }
        var address=$("#"+tbName).find("input[name=address]");
        var gpsName=$("#"+tbName).find("input[name=bd_gps]");
        gpsName.prop('readonly',true);
        address.prop('type','hidden');
        console.log(seajs.resolve("common/ibeacon_bind"));
        initBindUi(formObj);
        console.log(tbName,formObj);
        if($("#"+tbName).find("#_bd_gps").size()==0){
            bdMapCbs['changeLoc']=function(){

                $("#"+tbName).modal('hide');
            };
            bdMapCbs['return']=function(){

                $("#"+tbName).modal('show');
                return true;
            };
            bdMapCbs['placeList']=function(){

                $("#"+tbName).modal('show');
                return true;
            };
            address.after('<lbs-geo id="_bd_gps" city="北京" enable-modified="true"></lbs-geo>');
            attachBdMap($("#_bd_gps")[0],gpsName,address);
            address.parent().css({'padding-left':'15px'});
            address.parent().parent().after("<hr>");
        }

    }

//监听定位失败事件 geofail

});


$("body").on('click',".change-loc",function(evt){
    if(navigator.userAgent.indexOf("Window")!=-1 && navigator.userAgent.indexOf("Chrome")!=-1){
        $(".title.-ft-large").css({"margin-top":"-70px"});

    }

    $("body .back-btn").on('click',function(){
        bdMapCbs['return'] &&  bdMapCbs['return']();
        return true;
    });

    console.log("event",$._data( $('.back-btn')[0], 'events' ));
    bdMapCbs['changeLoc'] &&  bdMapCbs['changeLoc']();

    (function(){
        var interval=setInterval(function() {
                if($(".geo-selectlist").size()>0){
                    $(".geo-selectlist").on('click',function(){
                      
                        bdMapCbs['return'] &&  bdMapCbs['return']();
                        return true;
                    });
                    clearInterval(interval);
                }},500
        )})();
});
function wifiUserConfig(jqForm,data){

    if($("#wifi-config-win").size()==0){
        var options=[];
        var data=data.data;
        if($.isArray(data) && data.length>0){
            options=data.map(function(shop){
                return "<option value='"+shop['shop_id']+"'>"+shop['shop_name']+"</option>";
            });

            options=options.join("");


        } else {

            openPlainModal(jqForm,"没有找到门店","请在先配置门店,<a target='_blank' class='text-danger btn btn-primary' href='https://mp.weixin.qq.com/merchant/entityshop?action=list&token=18771280&lang=zh_CN'> 前往配置地址</a>");
        }


        var modalHtml='<form class="form-horizontal"> <div class="form-group"> <label  class="col-sm-3 control-label">WIFI的SSID</label> <div class="col-sm-9"> <input type="text" name="wifi_ssid" class="form-control" placeholder="WIFI的SSID(wifi名字)"> </div> </div> <div class="form-group"> <label class="col-sm-3 control-label">WIFI密码</label> <div class="col-sm-9"> <input type="password" class="form-control"  name="wifi_pwd" placeholder="WIFI密码"> </div> </div> <div class="form-group"> <label class="col-sm-3 control-label">设置门店</label> <div class="col-sm-9"> <select  class="form-control"  name="shop_id">'+options+' </select></div> </div> <div class="form-group"> <div class="col-sm-9"> <button type="submit" class="btn btn-default">保存</button> </div> </div> </form>';
        console.log("shopList",data);
        var modal=openModal(jqForm,"配置wifi连接",modalHtml,{},{},{'id':'wifi-config-win'});

        modal.find("[type=submit]").on('click',function(){
            jqForm.append($("<input>").attr({'name':'wifi_ssid','type':'hidden'}).val(modal.find('[name=wifi_ssid]').val()));
            jqForm.append($("<input>").attr({'name':'shop_id','type':'hidden'}).val(modal.find('[name=shop_id]').val()));
            jqForm.append($("<input>").attr({'name':'wifi_pwd','type':'hidden'}).val(modal.find('[name=wifi_pwd]').val()));
            modal.modal('hide');
            return false;

        });
    } else {

    }


}
//https://mp.weixin.qq.com/cgi-bin/componentloginpage?component_appid=wx692c3b311f7101d1&pre_auth_code=preauthcode@@@lySaEq9ffzhsZ5wnOxFEmbazhHw7FSJSO09InhWJXNkoB2lDNJIFjDzwk9CcSqSo&redirect_uri//=http:://www.gogoinfo.cn/wechat/officialLogin
function absUrl(path){

    return location.protocol+"//"+"www.gogoinfo.cn"+path;
}
function initBindUi(jqForm){
    var options={
        configHtml:' <div class="form-group"> <div class="col-sm-offset-4 col-sm-10 input-group"> <div class="checkbox"> <label> <input type="checkbox" class="page-cfg" name="page-cfg">配置页面 </label> </div> </div> </div>'

    };
    if(jqForm.find(".page-cfg").size()==0){

    }


    if(jqForm.find(".page-cfg").size()==0){

        console.log("jqForm",jqForm);

        var optionsObj=$(options.configHtml);


        optionsObj.find(".page-cfg").on('click',function(evt){
            if($(this).prop("checked")==true){
                var id='m_'+uniqueId();
                var body="<iframe style='min-height:600px;width:100%;border: 0px' src='/admin/table/view/?ztb=SiteM&menuid=34&q=_type%3Dsite&_s=simple&_f=title,desc,ctime,_type' data='"+id+"'></iframe>";

                var modal=openModal("body",config.lang.choose_item,body,{},{},{'id':id,'data-choose-model':1});
                $.subscribe("page-cfg",function(evt,data){
                    modal.modal('hide');
                    if(jqForm.find("[name=service_id]").size()==0){
                        jqForm.append($("<input name=\"service_id\" type='hidden'>").val(data.id));
                    }
                    console.log(evt,"evt",data);
                });
            }


        });
        //optionsObj.on('click','input[name=page-cfg]',function(e){
        //    setuploadingEffect(jqForm);
        //    if($(this).prop('checked')){
        //        //wifiUserConfig(jqForm);
        //        $.get('/wechat/preauthcode').done(function(data){
        //            try {
        //                console.log(data);
        //                var data=$.parseJSON(data);
        //                var url="https://mp.weixin.qq.com/cgi-bin/componentloginpage?component_appid="+data.data.appId+"&pre_auth_code="+data.data.preAuthCode+"&redirect_uri="+absUrl("/wechat/officialLogin/ui/wifi");
        //                    $(document).on('wifi_cfg',function(event,data){
        //
        //                        wifiUserConfig(jqForm,data);
        //                        $(document).off('wifi_cfg');
        //                    });
        //                /**
        //                 * 绑定到button尝试一下点击是否会被屏蔽
        //                 */
        //                openWindow(url,{width:1080});
        //            } catch(ex) {
        //                console.log(ex);
        //
        //            }
        //
        //
        //        });
        //    }
        //
        //});
        //optionsObj.on('click','input[name=service]',function(e){
        //    if($(this).prop('checked')) {
        //        var service=$(this).val();
        //        var label=$.trim($(this).parent().text());
        //        var createTxt=config.lang.create+label;
        //        $.post("/admin/page/getpage",{service:service},function(data){
        //                var data=$.parseJSON(data);
        //                if(data.code==1){
        //                    label='选择'+label;
        //                    var select='<select style="min-width:150px" data-placeholder="'+label+'" name="service_id" class="service-open" data="'+service+'">';
        //                    var options=[];
        //                    if($.isArray(data.data)){
        //                        var options=data.data.map(function(item){
        //                            return '<option value="'+item._id+'">'+(('title' in item)?item.title:'')+'</option>';
        //                        });
        //                        console.log("wfwe",options[options.length-1]);
        //
        //                    } else {
        //                        options.push('<option value="-2">'+label+'</option>');
        //                    }
        //                    options.push('<option value="-1">'+createTxt+'</option>');
        //                    if(options.length>10){
        //                        var tmp=options[9];
        //                        options[9]=options[options.length-1];
        //                        options[options.length-1]=tmp;
        //
        //                    }
        //
        //                    console.log("after options",options[9]);
        //                    var select='<div class="form-group"><label  class="col-sm-4 col-xs-12 control-label">'+label+'</label><div class="input-group  col-sm-8 col-xs-12"> '+select+options.join("")+"</select></div></div>";
        //
        //                    var selNode=$('[data="'+service+'"]');
        //                    $(".service-open").parent().parent().hide();
        //
        //                    if(selNode.size()==0){
        //                        var selObj=$(select);
        //                        selObj.find("select.service-open").on('change',function(e){
        //                            console.log(this);
        //                            var openUrl="/admin/page/editor/?ztb=Page&action=add&type=p&tmpName=" + service + "&tmpId=1";
        //                            if($(this).val()==-1){
        //                                if(service=='page'){
        //                                    openUrl="/admin/page/index/?ztb=Page&action=add&ui=hide_side";
        //                                }
        //                                openWindow(openUrl,{width:1080});
        //                            }
        //                        });
        //                        $(".wifi-config").eq(1).append($(selObj));
        //
        //                        seajs.use(['chosen.jquery.js','/css/chosen.css'],function(){
        //                            $(".service-open[data="+service+"]").chosen();
        //                            $.unsubscribe('service_id');
        //                            $.subscribe('service_id',function(evt,data){
        //
        //                                    $("[name=service_id]").prepend(
        //                                       "<option value='"+data._id+"'>"+data.title+"</option>"
        //                                    );
        //                                    $("[name=service_id]").find("option[value="+data._id+"]").prop('selected',true);
        //                                    $(".service-open[data="+service+"]").trigger("chosen:updated");
        //
        //
        //                            });
        //                            $(".chosen-select").bind("chosen:maxselected", function (a) { console.log(a);alert(a); });
        //
        //                        });
        //
        //                    } else {
        //                        selNode.parent().parent().show();
        //                    }
        //
        //
        //
        //                }
        //
        //        });
        //
        //
        //    }
        //
        //});
        jqForm.find("[type=submit]").parent().parent().before(optionsObj);


    }




}

var attachBdMap=function(lbsGeoDom,jqGps,jqAddress){



    lbsGeoDom.addEventListener("geofail",function(evt){
        console.log("fail");
    });
//监听定位成功事件 geosuccess
    lbsGeoDom.addEventListener("geosuccess",function(evt){
        console.log(evt.detail);
        var address = evt.detail.address;
        var coords = evt.detail.coords;
        var x = coords.lng;
        var y = coords.lat;

        jqGps.size()>0 && jqGps.val([x,y].join(","));
        jqAddress.size()>0 &&  jqAddress.val(address);
        console.log("地址："+address);
        console.log("地理坐标："+x+','+y);
    });
}

