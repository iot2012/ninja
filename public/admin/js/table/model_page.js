var qrCodeUrl="http://"+ location.host + "/cache/wap/#id#.html";
seajs.use(['jq.qr'],function(){
    $("table").on('click','.opt-qrcode',function(e) {
        var qrId = 'qr_' + $(this).attr("data-id");
        var text=$("#r_"+$(this).attr("data-id")).find("td[data=c_title]").text();
        var div = "<div id='" + qrId + "'></div><h4>"+text+"</h4>";
        var modalId="m_"+qrId;
        openPlainModal($("body"), config.lang.qr_wap, div,modalId, function () {
            console.log("view");
            if( $('#' + qrId).find('table').size()==0){

                $('#' + qrId).qrcode({
                    render: "table",
                    text:  qrCodeUrl.replace(/#id#/,qrId)
                });
            }

        });


    });

});
