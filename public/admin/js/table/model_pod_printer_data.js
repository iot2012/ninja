


$("[data-tran-title]").each(function(idx,item) {
    var title=$(item).attr('data-tran-title');
    $(item).attr('title',config.lang[title]);
});
var printableMime="image/*,application/pdf,text/*,application/pdf";
var uploadUrl="/media/printerUpload.json";
var tbOpCbs={
    'always':function(data,formName){
        console.log("always ..");
        seajs.use(['/js/dropzone','/css/dropzone.css'],function(){
            var fileNodeName="#"+formName+" button[data-role=upload]";
            var fileNode=$(fileNodeName);

            console.log(fileNode,fileNodeName);
            if(!fileNode.hasClass('dz-clickable')){
                fileNode.on('click',function(e){
                    return false;
                });
                var objForm=$("#"+formName);
                objForm.find("[type=submit],[type=reset]").hide();
                fileNode.addClass('dropzone').css({"min-width":"300px"});
                attachDropzone(fileNodeName,{url: uploadUrl,acceptedFiles:printableMime},false,{
                    success:function(e,data){

                        console.log("uploaded",data);
                        if(data.code==1){

                            var formJson={'ztb':tbModel,'_id':data.data.pid,'status':config.lang.uploaded,print_time:curDateTime(),file_id:data.data.mid};
                            var opCol2=opCol(tbModel,data.data.pid,['times','print']);
                            console.log("opCol",opCol2);
                            appendRow(formJson,opCol2);
                            showOverlay(objForm,'check',combLang([config.lang.upload,config.lang.suc]));
                        } else {
                            showOverlay(objForm,'times',data.msg);
                        }

                        console.log("upload suc",e,"msg",data);
                    },
                    error:function(e,data){
                        showOverlay($(formName),'times',combLang([config.lang.upload,config.lang.fail]));
                    }

                });


            }
        });


    }

}
$("#tb_"+tbModel).on('click',".opt-print",function(evt){
    var faNode=$(this).find(".fa");
    if(faNode.hasClass('fa-print')){
        faNode.removeClass("fa-print").addClass("fa-stop");
        var tbId=$(this).attr('data-id');
        var mid=$.trim($('#r_'+tbId).find('[data=c_mid]').text());
        console.log("print mid",tbId);
        $.post('/admin/router/sendprint',{mid: mid},function(data){

        });

    } else {

        faNode.removeClass("fa-stop").addClass("fa-print");
    }

});

