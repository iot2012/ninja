/**
 * Created by chjade on 3/2/15.
 */
var wechatAdduser=function(data){
    console.log(data);
    $("[data-role=tagsinput][name=who]").tagsinput('add',{value:data.openid,text:data.realname});

}
var createObjTag=function(input){


    var nextEle=input.next();
    if(!nextEle.hasClass('bootstrap-tagsinput')){
        input.on('beforeItemAdd', function(event) {
            console.log(event,"");
            // event.item: contains the item
            // event.cancel: set to true to prevent the item getting added
        });
        input.tagsinput({itemValue: 'value',
            itemText: 'text',
            confirmKeys: [13, 188]

        });

        // if()
        var nextEle=input.next();
        var nextInput= nextEle.find("input");
        if(input.prop('readonly')){

            nextInput.prop('readonly',true);

        }

        if(input.attr("data-plus")==1 && nextInput.find("fa-plus").size()==0){
            var span=$("<span class='fa fa-plus fa-2x plus-action'></span>");
            span.on('click',function(evt){
                setQrUrl(function(data){

                    if (/^http[s]?:\/\/.+/.test($.trim(data))) {

                        createPopover(nextInput,'扫描二维码添加微信用户','<img width="140px" height="auto" src="/img/ajax-loading.gif">',{placement:'bottom',trigger:'manual'},{'shown':function(obj){
                            console.log(obj);
                            $(obj).find("img").attr('src',data);

                        }});
                        nextInput.popover('show');
                    }


                },'?action=adduser&type=wechat');
            });
            nextEle.append(span);
        }

    }
}

var whoAction=function(value,closestForm){
    var closestForm=closestForm || $("#"+tbModel+"-form-edit");
    switch(value){
        case 'wechat':{
            var who1=closestForm.find("input[name=who]");
            who1.off('beforeItemAdd');
            who1.tagsinput('removeAll');
            who1.tagsinput('destroy');
            createObjTag(who1);
            break;
        }
        case 'email':{
            var who1=closestForm.find("input[name=who]");

            who1.tagsinput('removeAll');
            who1.tagsinput('destroy');

            var result=isJson(who1.val());
            if(result){
                who1.val(result.join(","));
            }
            who1.tagsinput();

            who1.on('beforeItemAdd',function(event){
                event.cancel=(ival['email'].test(event.item)?false:true);

            });

            break;
        }
        default:{


        }

    }
}
seajs.use(['jquery.multiple.select','/css/multiple-select.css'],function(){

    tbOpCbs['add']=function(data,formName){
        createObjTag($("#"+formName).find("input[name=who]"));

    },


    tbOpCbs['edit']=function(data){
        console.log("row data",data);
        if(data['notify_type']=='discovery'){

            //模拟change事件
            var kws=$.parseJSON(data['keyword']);
            $("select[name=notify_type]").find('option[value=discovery]').prop('selected',true).trigger('change');

            (function(){
                var cnt=0;
                var kwInt=setInterval(function(){
                    var isExists=$("select[name='keyword[]']").next().hasClass('ms-parent');
                    console.log("isExists",isExists,kws);
                    //如果发生异常导致clearinterval不会执行
                    if(isExists){
                        $("select[name='keyword[]']").multipleSelect('setSelects', kws);
                        clearInterval(kwInt);
                    }
                    cnt++;
                    if(cnt>100){
                        clearInterval(kwInt);
                    }

                },100);

            }());

            whoAction(data['action']);


        }

    }
    $("body").delegate('select', "change", function() {
        var self=$(this);
        var option=$("option:selected", this);
        var value=$(this).val();
        var name=$(this).attr("name");

        console.log(name,this);

        var result=($.isPlainObject(jsonCols[name]) && ('enum' in jsonCols[name]) && (typeof jsonCols[name].enum[$(this).val()]!='undefined'))?jsonCols[name].enum[$(this).val()]:{};
        var closestForm=$(this).closest("form");
        var enumActions={};
        var newResult={};



        if($.isPlainObject(result)){
            if('reg' in result){
                var fields=result['reg'];
                for(var key in fields){
                    var reg=fields[key];
                    closestForm.find("[name="+key+"]").attr('ref',('{"reg":"'+reg+'"}'));
                }

            }
            if('sel' in result){

                console.log(result['sel'],this);

                var fn = window[result['sel']];

                if (typeof fn === "function") {fn.apply(null, [this]);}

            }

        }
        if(name=='notify_type'){
            var inputName='keyword';
            var options=$("select[name=event]").find("option");
            var dieOpt=$("select[name=event]").find("option[value=die]");

            var ele=closestForm.find("select[name='"+inputName+"[]']");
            var input=closestForm.find("input[name="+inputName+"]");
            switch(value){
                case 'self':{
                    options.not("[value=die]").hide();
                    dieOpt.prop('selected',true);

                    break;
                }
                case 'discovery':{
                    options.not("[value=die]").show();
                    $(options[0]).prop('selected',true);
                    dieOpt.hide();
                    $("select[name=event]").find("option[value=die]").hide();


                    var closestForm=$(this).closest("form");


                    if(ele.size()>0){
                        input.attr('name',inputName).prop('disabled',true).hide();
                        ele.prop('disabled',false);
                        ele.next().show();
                    } else {
                        $.get('/resti/RouterM/deviceList',{mac:$("#data-mac").text().replace(/-/g,'').toLowerCase()},function(data){
                            console.log("dev list",data);
                            var data=$.parseJSON(data);

                            if($.isArray(data.data)){
                                var multiSel=$('<select multiple>'+data.data.map(function(item){
                                   return "<option value='"+item.mac.replace(/:/g,'')+"'>"+item.name+'</option>';
                                })+'</select>');
                                multiSel.attr('name',inputName+"[]");
                                input.prop('disabled',true);

                                input.before(multiSel).hide();
                                multiSel.multipleSelect({
                                    width: '100%',
                                    placeholder:config.lang.choose_device,
                                    selectAllText:config.lang.selectall,
                                    onOpen:function(){
                                        var parentDom=$("input[name=keyword]").parent();
                                        var refresh=parentDom.find("span.fa-refresh");

                                        if(refresh.size()==0){
                                            var refreshObj=$('<span class="fa fa-refresh fa-15x multi-select"></span>').on('click',function(evt){
                                                $.get('/resti/RouterM/deviceList',{mac:$("#data-mac").text().replace(/-/g,'').toLowerCase()}).done(function(data){
                                                    var data=$.parseJSON(data);
                                                    if($.isArray(data.data)){
                                                        multiSel.html(data.data.map(function(item){
                                                            return "<option value='"+item.mac.replace(/:/g,'')+"'>"+item.name+'</option>';
                                                        }));
                                                        multiSel.multipleSelect('refresh');
                                                    }

                                                })

                                            });
                                            parentDom.append(refreshObj);
                                        }


                                    }
                                });
                            }

                        });
                    }
                    break;
                }

                default:{
                    options.not("[value=die]").show();
                    $(options[0]).prop('selected',true);
                    dieOpt.hide();

                    var closestForm=$(this).closest("form");


                    input.attr('name',inputName).prop('disabled',false).show();
                    ele.prop('disabled',true);
                    ele.next().hide();
                }

            }

        }
        if(name=='action'){
            whoAction(value,closestForm);

        }
    });


});
