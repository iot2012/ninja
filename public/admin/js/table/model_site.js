(function(){
    var addRowHref=$("#add-row").attr('href');
    var type=(location.href.indexOf("type=page")!=-1)?'page':'site';
    if(addRowHref){
        $("#add-row").attr('href',((addRowHref.indexOf("?")!=-1)?(addRowHref+"&type="+type):(addRowHref+"?type="+type)));
    }


}());


var qrCodeUrl="http://"+ location.host + "/cache/wap/#id#.html";
seajs.use(['jq.qr'],function(){
    $("table").on('click','.opt-qrcode',function(e) {
        var qrId = 'qr_' + $(this).attr("data-id");
        var text=$("#r_"+$(this).attr("data-id")).find("td[data=c_title]").text();
        qrCodeUrl=qrCodeUrl.replace(/#id#/,qrId);
        var div = "<div id='" + qrId + "'></div><h4>"+text+"</h4><br><div class='alert alert-info'>链接地址:"+qrCodeUrl+"</div>";
        var modalId="m_"+qrId;
        createIframe({'src':'http://biz.n.cc/admin/table/view/?ztb=SiteM&menuid=34&q=_type%3Dsite&_s=simple&_f=title,desc,ctime,_type'});
        openPlainModal($("body"), config.lang.qr_wap, div,modalId, function () {

            if( $('#' + qrId).find('table').size()==0){

                $('#' + qrId).qrcode({
                    render: "table",
                    width:120,
                    height:120,
                    text:  qrCodeUrl
                });
            }

        });


    });

});
