
var opCol=function(tbName,primaryId,cfg,tbStyle){
    var cfg=cfg || ['edit','times'];
    var tbStyle=tbStyle || 'table';
    cfg=cfg.map(function(item){
        return '<li> <a href="#"  data="'+tbName+'" class="opt-'+item+'" > <i class="fa fa-'+item+' fa-15x"></i> </a> </li>';
    });
    var colStr='';

    if(tbStyle=='table'){
        colStr='<td class="operation"> <ul class="nav navbar-nav" data="r_'+primaryId+'" data-table="'+tbName+'">'+cfg.join('')+"</ul> </td>";
    } else if(tbStyle=='list'){
        colStr='<td class="operation"> <ul class="nav navbar-nav" data="r_'+primaryId+'" data-table="'+tbName+'">'+cfg.join('')+"</ul> </td>";
    } else if(tbStyle=='grid'){
        colStr='<ul class="nav navbar-nav" data="r_'+primaryId+'" data-table="'+tbName+'">'+cfg.join('')+"</ul>";
    }

    return colStr;
}
var appendRow=function(formJson,opCol2){


    var newRow = buildRow($.isArray(formJson)?formJson:[formJson],colName(formJson['ztb']),opCol2);
    console.log("new row",newRow);

    var tbId="tb_"+formJson[0]['ztb'];

    delete formJson['ztb'];
    // tb_common_user tbName
    //add it
    //  footable.appendRow(newRow);
    // $("div[id=" + pid + "]").modal('toggle');
    // setTimeout(function(){modal.modal('hide');modal.toggle();console.log("tt",modal);},1500);

    if(tbStyle=='table'){
        $("#"+tbId).append(newRow.join("")).trigger('footable_redraw');
    } else if(tbStyle=='list'){
    }  else if(tbStyle=='grid'){
        $("#"+tbId).append(newRow.join(""));
    }

}
/**
 *
 * refCols 引用的列字典,如果是文件,给出地址
 * opCol2  操作(edit,remove等)的icons
 * cols    列元数据
 */

var makeTableRow=function(row,refCols,opCol2,cols){
    var primaryId=row[primaryKey];
    var tr='<tr id="r_'+primaryId+'">';
    var tbName=row['ztb'];
    // add more dropdown menu
    //<li class="dropdown pull-right"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-list fa-15x"></i> </a> <ul class="dropdown-menu" role="menu"> <li> <a href="#"> <span class="fa fa-file-code-o"></span> CSV </a> </li> <li> <a href="#"> <span class="fa fa-file-pdf-o"></span>Excel </a> </li> <li class="divider"></li> </ul> </li>
    //使用opCol2变量会导致opCol2为undefined,导致始终执行后面的函数
    var opCol3=opCol2 || opCol(tbName,primaryId,null,'table');

    var tds=refCols.map(function(key){
        if(cols[key].reg=='file'){
            row[key]=ajaxMedia[row[key]];
        }
        var checked=$(".filter_cols a[data=c_"+key+"] :checkbox").prop('checked');
        if(checked){
            return '<td data="c_' + key + '">' + ((typeof row[key]!='undefined')?row[key]:'') + '</td>';
        } else {
            return '<td style="display:none" data="c_' + key + '">' + ((typeof row[key]!='undefined')?row[key]:'') + '</td>';
        }

    });
    return tr+'<th  data-ignore="true" class="edit-row"><input type="checkbox"/></th>'+tds.join('')+opCol3+'</tr>';

}



var makeListRow=function(row,refCols,opCol2,cols){


}


var makeGridRow=function(row,refCols,opCol2,cols){

    var primaryId=row[primaryKey];
    var tbName=row['ztb'];
    // add more dropdown menu
    //<li class="dropdown pull-right"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-list fa-15x"></i> </a> <ul class="dropdown-menu" role="menu"> <li> <a href="#"> <span class="fa fa-file-code-o"></span> CSV </a> </li> <li> <a href="#"> <span class="fa fa-file-pdf-o"></span>Excel </a> </li> <li class="divider"></li> </ul> </li>
    //使用opCol2变量会导致opCol2为undefined,导致始终执行后面的函数
    var opCol3=opCol2 || opCol(tbName,primaryId,null,'grid');
    var gridMeta=styleMeta['grid'];


    var liTemp='<li><a href="" title="" class="data-cover"> <img src="<%=pic_url%>"> <img src="<%=pic_url%>"> <img src="<%=pic_url%>"> <img src="<%=pic_url%>"> <img src="<%=pic_url%>"> </a> <h5 class="z-item z-item-title"><%=title%></h5> <h5 class="z-item z-item-desc"><%=desc%></h5> <%==opCols%> </li>';
    return template.compile(liTemp)({
        opCols:opCol3,
        title:((typeof row['title']!='undefined')?row['title']:((typeof row[gridMeta['title']]!='undefined')?row[gridMeta['title']]:'title')),
        desc:((typeof row['desc']!='undefined')?row['desc']:((typeof row[gridMeta['desc']]!='undefined')?row[gridMeta['desc']]:'desc')),
        pic_url:((typeof row['pic_url']!='undefined')?row['pic_url']:((typeof row[gridMeta['pic_url']]!='undefined')?row[gridMeta['pic_url']]:'/img/unknown_cover.png'))
    });

}


var buildRow=function(rowData,refCols,opCol2,cols){

    var cols=cols || jsonCols;
    var rows=rowData.map(function(row){
        return !!window[('make'+capStr(tbStyle)+'Row')] && window[('make'+capStr(tbStyle)+'Row')](row,refCols,opCol2,cols);

    });
    return rows;

}

var colName=function(tbName){
   // var tds=$("#tb_"+tbName+">thead>tr.col-meta").eq(0).find("th[data^=c_]");
   // return Array.prototype.slice.call(tds.map(function(idx,item){return $(item).attr('data').replace(/c_/,'');}),0);
    return _.keys(jsonCols);
}
var loadData=function(loadingObj,param,table){
    var table = table || $(loadingObj).closest('table');
    setuploadingEffect($(loadingObj));
    console.log("load more");
    var table = table || $(loadingObj).closest('table');
    var footable=table.data('footable');
    $.post("/admin/table/view?ztb="+tbModel,param).done(function(data){
        if($.trim(data)!=''){
            var tbModel=table.attr("id").replace(/tb_/,'');
            var newRow=buildRow($.parseJSON(data),colName(tbModel));
            table.find("tbody").append(newRow).trigger('footable_redraw');
        }

    });
}
var beforeOp=function(action,obj){
    if(jsonActions && (action in jsonActions)){
        if(!jsonActions[action].match(/^[\w-]+$/)){
            location.href=addQuery(jsonActions[action],obj);
        } else {
            (typeof jsonActions[action]=='function') && jsonActions[action]();
        }
        return true;
    }
    return false
}
var addRow=function(evt){

    var tbName=$(this).attr("data");

    if(beforeOp('add',{ztb:tbName,action:'add'})){
        evt.preventDefault();
        return;
    }

    var formName=tbName+"-form-add";
    var pid=tbName+"_add_win";
    ajaxCbs[formName]=[function(data){
        var formJson=$.unparam(ajaxCbs[formName+'-data']);

        formJson['uuid']=formJson['uuid']?formJson['uuid']:defUuid;

        //build up the row we are wanting to add
        formJson[primaryKey]=data.id;

        formJson= $.extend(formJson,data);
        console.log("ajax append row");
        appendRow(formJson);
        setTimeout(function(){$("#"+pid).modal('hide');},3000);
    }];


    $(evt).prop("disabled",true);
    var modal = $("#" + pid);
    var lTbName=config.lang['t_'+tbName.toLowerCase()]?config.lang['t_'+tbName.toLowerCase()]:tbName;
    var rowData={};
    if (modal.size() < 1) {
        $.get('/admin/js/table/temp/record_add.html', function (html) {
            var profileModal = $(template.compile(html)($.extend({},{l_record:config.lang.record,l_add:config.lang.add,l_tbName:tbNameTxt,tbName:tbName,'write_all':canWrite(),lang: config.lang,cols:jsonCols,inputTypes:inputTypes})));
            // profileModal.drags({handle: ".modal-header"});
            // profileModal.find(".modal-dialog").resizable();
            // jqObj,title,html,popOverOpts,css,cbDict


            profileModal.modal().on("hidden.bs.modal",function(){
                $(evt).prop("disabled",false);

            }).on('shown.bs.modal',function(){
                var input=profileModal.find("input[data-role]");

                if($.isFunction(tbOpCbs['add'])){
                    tbOpCbs['add'](rowData,formName);
                }
                if($.isFunction(tbOpCbs['always'])){
                    tbOpCbs['always'](rowData,formName);
                }

                console.log("xx streaming 2xx");

            });

        });
    }  else {
        modal.modal();
    }


    evt.preventDefault();

}
$("#add-row").on('click',addRow);
var setUpRmRow=function(popUpEle,opts,title){
    var opts=opts || {};
    var many=false;
    var title=title || config.lang.rm_row_prompt;

    setUpRmPopover(popUpEle,title,opts,{
        'shown':function(){
            console.log("rm shownn");
        },
        'show':function(){
            console.log("rm show");
        },
        'ok':function(){
            var rmData={};

            rmData['ztb']=popUpEle.attr("data");
            rmData["ids"]=[popUpEle.attr("data-id")];

            doRm(rmData);
            console.log("ok show");

        },
        'hidden':function(){
            console.log("ok hidden");
            console.log("hidden");
        }
    });
}

var setUpRmManyRow=function(popUpEle,opts,title){
    var opts=opts || {};
    var many=false;
    var title=title || config.lang.rm_row_prompt;

    setUpRmPopover(popUpEle,title,opts,{
        'shown':function(){
            console.log("rm shownn");
        },
        'show':function(){
            console.log("rm show");
        },
        'ok':function(){
            var rmData={};

            rmData['ztb']=popUpEle.attr("data");

            var ids=Array.prototype.slice.call($("tbody tr").has(".edit-row>input:checked:visible").map(function(a,item){return $(item).attr("id").replace(/r_/,'');}),0);

            console.log("many ids",ids);
            rmData["ids"]=ids;


            doRm(rmData);
            console.log("ok show");

        },
        'hidden':function(){
            console.log("ok hidden");

        }
    });
}
$('.operation a.opt-times').each(function(idx,item){

    setUpRmRow($(item));
});
setUpRmManyRow($("a.rm-sel-row"),{placement:'bottom'},config.lang.rm_sel_row_prompt);

var doRm=function(rmData){
    console.log("do remove");
    $.post("/admin/table/recordsremove",rmData,function(data){
        var data= $.parseJSON(data);
        if(data.code==1){
            if(tbStyle=='table'){
                if(rmData['ids'].length>1){
                    $("input[name=sel_all]").prop("checked",false);
                }

            }

            rmData['ids'].map(function(item){
                $("#r_"+item).remove();
            });
            console.log('remove suc');
        } else {
            console.log('remove failure');
        }
    });
}


$("a[data-child]").on('click',function(){
     location.href="/admin/photo";

});
var z=new z();
$("body").on('click','[data-action-type=share]',function(evt){
    var shareLink=getAbsUrl("/share/visit/ztb/"+tbModel+"/id/"+queryData['aid']);
    var shareTxt=config.lang['share_'+tbModel].replace(/\{\{realname\}\}/ig,config.user.realname).replace(/\{\{link\}\}/gi,shareLink);
    var shareWin=$("#shareWin");
    if(shareWin.size()==0) {
        $.get('/admin/js/share/temp/share_win.html', function (html) {

            var profileModal = $(template.compile(html)($.extend({}, {
                link: shareLink,
                shareTxt: shareTxt,
                l_content: config.lang.content,
                l_share: config.lang.share,
                l_general: config.lang.general,
                l_sms: config.lang.sms,
                l_mail: config.lang.mail,
                l_qrcode: config.lang.qrcode,
                l_link: config.lang.link,
                l_apply: config.lang.apply,
                l_mobile_num: config.lang.mobile_num,
                l_email: config.lang.email,
                l_copy: config.lang.copy,
                l_scan_share:config.lang.scan_share,
                share_type:tbModel
            })));


            // profileModal.drags({handle: ".modal-header"});
            // profileModal.find(".modal-dialog").resizable();
            // jqObj,title,html,popOverOpts,css,cbDict

            profileModal.on("hidden.bs.modal", function () {
                $(evt).prop("disabled", false);
            }).on('shown.bs.modal', function () {

                $("a[href='#pro-share-link']").on('click',function(){
                    var self=this;
                        seajs.use(['zc'],function(){
                            var copyBtn=$($(self).attr("href")).find(".copy-btn")[0];

                            window.zcClient = new ZeroClipboard(copyBtn);

                            zcClient.on('ready', function(event) {
                                doCopy(copyBtn);
                                console.log( 'movie is loaded',$("#"+$(self).attr("data-jq")).val());


                            });
                            zcClient.on('aftercopy',function(evt){

                                showOverlay(profileModal,'',config.lang.copy_suc,2);
                                return false;
                            });

                        });

                    });


                $("a[href='#pro-share-qrcode']").on('click',function(){

                   seajs.use(['qrcode','jq.qr'],function(){

                       if($.trim($("#qrcode_link").html())==''){

                           $("#qrcode_link").qrcode({text:shareLink,'width':128,'height':128});
                       }

                   });

                });
                window._bd_share_config = {
                    "common": {
                        "bdSnsKey": {},
                        "bdPopTitle": "fogpod",
                        "bdText": shareTxt,
                        "bdMini": "2",
                        "bdMiniList": false,
                        "bdPic": "",
                        "bdStyle": "1",
                        "bdSize": "24"
                    },
                    "share": {},
                    "image": {
                        "viewList": ["qzone", "tsina", "tqq", "renren", "weixin"],
                        "viewText": "分享到：",
                        "viewSize": "16"
                    },
                    "selectShare": {
                        "bdContainerClass": null,
                        "bdSelectMiniList": ["qzone", "tsina", "tqq", "renren", "weixin"]
                    }
                };
                seajs.use(['http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js']);

            });
            profileModal.modal('toggle');

        });
    }
    else {
        shareWin.modal();
    }

});
$("body").on('click','.copy-btn',function(evt){
    doCopy(this);
});
function doCopy(self) {
    window.zcClient.setText( $("#"+$(self).attr("data-jq")).val());
}
var printableMime="image/*";
var uploadUrl="/media/photoUpload.json";
var tbOpCbs={
    'always':function(data,formName){
        console.log("always ..");
        seajs.use(['/js/dropzone','/css/dropzone.css'],function(){
            var fileNodeName="#"+formName+" button[data-role=upload]";
            var fileNode=$(fileNodeName);

            console.log(fileNode,fileNodeName);
            if(!fileNode.hasClass('dz-clickable')){
                fileNode.on('click',function(e){
                    return false;
                });
                var objForm=$("#"+formName);
                objForm.find("[type=submit],[type=reset]").hide();
                fileNode.addClass('dropzone').css({"min-width":"300px"});
                attachDropzone(fileNodeName,{url:uploadUrl ,acceptedFiles:printableMime,uploadMultiple:true,extraData:{'aid':queryData['aid']}},false,{
                    success:function(e,data){

                        console.log("uploaded",data);
                        if(data.code==1){



                            var formJson=data.data.map(function(record){
                                record['ztb']=tbModel;
                                record['_id']=record.pid;
                                return record;

                            });
                            appendRow(formJson);
                            showOverlay(objForm,'check',combLang([config.lang.upload,config.lang.suc]),2);
                        } else {
                            showOverlay(objForm,'times',data.msg,2);
                        }

                        console.log("upload suc",e,"msg",data);
                    },
                    error:function(e,data){
                        showOverlay($(formName),'times',combLang([config.lang.upload,config.lang.fail]));
                    }

                });


            }
        });


    }
}


var imageTemp='<div class="modal fade"> <div class="modal-dialog"> <div class="modal-content"> ' +
    '<div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><%if(!!download){%><a href="" target="_blank" class="external" style="float:right;margin-right:0.8em;margin-top:2px">' +
    '<span class="fa fa-external-link"></span></a><%} else {%>' +
    '<a class="cap-download" style="float:right;margin-right:0.8em;margin-top:2px" href="#" download=""><span class="fa fa-download"></span></a><%}%><h4 class="modal-title"><%=title%></h4>' +
    ' </div> <div class="modal-body preview-img" style="overflow: auto"><img src="<%=src%>"></div> <div class="modal-footer"> </div> </div></div></div>';
$("a.data-cover[data-child]").off('click');
$("a.data-cover[data-child]>img").on('click',function(evt){

    evt.preventDefault();
    evt.stopPropagation();
    var title=$(this).parent().next().text();
    var imageModel=template.compile(imageTemp)({title:title,src:$(this).parent().find("img").attr("src").replace(/_100x100/,'')});
    $(imageModel).modal();
    return false;
});