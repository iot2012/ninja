
var substringMatcher = function(strs) {
    return function findMatches(q, cb) {
        var matches, substrRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function(i, str) {
            if (substrRegex.test(str.tokens)) {
                // the typeahead jQuery plugin expects suggestions to a
                // JavaScript object, refer to typeahead docs for more info
                matches.push(str);
            }
        });

        cb(matches);
    };
};

seajs.use(['typeahead.js'],function(){
    console.log("typeahead");



    $.get('/ibeacon/getbyuid.json').done(function(data){


    });


    var bestPictures = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: '/tsearch/ibeacon/%QUERY.json'
    });

    bestPictures.initialize();

    $('input[name=ibeacon]').typeahead(null, {
        name: 'best-pictures',
        displayKey: 'alias',
        source: bestPictures.ttAdapter(),
        templates: {
            empty: [
                '<div class="empty-message">',
                '没有符合的ibeacon',
                '</div>'
            ].join('\n'),
            header: '<div><span class="league-name" style="padding:1px 50px;width:100px;font-weight:bold">别名</span><span class="league-name" style="padding:5px;font-weight:bold">ID</span><span class="league-name" style="padding:5px;font-weight:bold">Major</span><span class="league-name" style="padding:5px;font-weight:bold">minor</span></div>',
            suggestion: template.compile('<p><strong style="padding:1px 30px;padding-right:40px;width:100px;"><%=alias%></strong><span style="padding-right:40px"><%=id%> </span><span style="padding-right:40px"><%=major%> </span><%=minor%></p>')
        }
    });


});





var alertTemp='<div class="alert alert-<%=alertType%> alert-dismissible" role="alert" style="display:none;position: fixed; top: 0px; left: 0px; z-index: 9999; width: 100%; "> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button> <strong><%=title%></strong><%=content%></div>';

var showSidebar=function(){
    $("body").addClass("edit");
    $("body").removeClass("devpreview sourcepreview");

}
var toogleDragPanel=function(){
    console.log("toggle");
    if($("body").hasClass("edit"))
    {
        hideSidebar();
    }
    else {
        showSidebar();
    }



}
var hideSidebar=function(){
    $("body").removeClass("edit");$("body").addClass("devpreview sourcepreview");
}





$.ajaxPrefilter(function( options ) {
    //http://w.cc/adminhttp://w.cc/page/template".
    //var reg=new RegExp(location.protocol+"//"+location.host,"i");
    //options.url =  options.url.replace(reg,'');
    //options.crossDomain = false;
//    options.dataType="json";
//    options.data=data;

});
var RightPageView = Backbone.View.extend({

    el: $("#right-temp"),
    initialize: function () {

    }


});
var PageModel=Backbone.Model.extend({

    idAttribute:'id',
    //不要增加默认idAttribute的如id:""否则导致无法触发create.json
    urlRoot: '/user',
    initialize:function(){
        // this.listenTo(this,'remove',this.onRemove);

    },
    url:'/page/list.json',
    urls: {
        'read': '/page/get.json',
        'create': '/page/create.json',
        'update': '/page/update.json',
        'patch': '/page/updatecontent.json',
        'delete': '/page/delete'
    },
    onRemove:function(e){
        console.log(e,"remove page");
    },
    onChange:function(model){

    }



});

var Pages=Backbone.Collection.extend({

    model: PageModel,


    initialize: function () {
        // this.listenTo(this,'change:isChanged',this.renderControl);
        //this.listenTo(this,'change:uids',this.changeUids);

    },
    url: '/page/read.json',
    comparator: 'mtime'

});
var pages=new Pages;
$("body").on('click','.remove[data-ref][data-source]',function(e){
    var id=$(this).attr('data-id');
    if(id){

        $("#com_"+id).remove();
    }
});
var PageView=Backbone.View.extend({
    model:PageModel,
    tagName:'li',
    initialize: function () {
        this.listenTo(this.model,'change:title',this.onChange);
    },
    events: {
        "click": "loadPage",
        "click .fa-trash":"rmPage",
        "click .fa-edit":"editPage"
    },
    loadPage:function(e){
        this.$el.parent().find("li").removeClass('list-item-active');
        this.$el.addClass('list-item-active');
        var temp=(this.model.get('content')?this.model.get('content'):'');
        var html=template.compile(temp)({id:this.model.get('id')});
        var htmlObj=$(html);
        var pcId='pc_'+this.model.id;
        $(".config-section .page-config:not(#"+pcId+")").hide();
        var self=this;
        //htmlObj.attr({'id':pcId}).addClass('page-config');
        if($("#"+pcId).size()>0){
            $("#"+pcId).show();
        } else {
            htmlObj.find("[data-ref][data-source]").each(function(idx,item){

                var action='add';
                var uniId=$(this).attr('data-id');
                var rmDom=$(this).parent().parent();
                if(!$(this).attr('data-id')){

                    uniId=randomNumber();
                    $(this).attr({"data-id":uniId});
                }
                if(!rmDom.attr("data-source")){
                    rmDom.attr({"data-id":$(this).attr('data-id'),"data-ref":$(this).attr('data-ref'),"data-source":$(this).attr('data-source')});
                }

                ibAdvPub($(item).attr('data-ref'),self.model.get('id'),action,uniId);
            });
        }

        $(".view-container").html(htmlObj);
    },
    onChange:function(model){
        this.$el.find(".search-item-txt").text(model.get('title'));
    },
    editPage:function(e){
        var pageTemp='<form role="form form-inline" action="/page/updatecontent" id="page-edit-form"> <div class="form-group"> <input type="text" class="form-control" value="<%=value%>" name="title" placeholder="页面标题"> </div> </form>';
        var html=template.compile(pageTemp)({value:this.model.get("title")});
        var self=this;
        openModal(this.$el,'重命名页面',html,{ok:function(modalId){
            self.model.set({title:$("#"+modalId).find("input[name=title]").val()});

        }});

    },

    rmPage:function(e){
        var self=this;
        openModal($(this),(config.lang.ok_rm_page+" "+self.model.get('title')),"确认删除该页面吗,删除该页面后将无法恢复",{ok:function(){

            $(".view-container").empty();
            $(".config-section").empty();
            self.$el.remove();
            self.model.destroy();
        }},{},{id:("rm-page-"+self.model.get(self.model.idAttribute))});
        e.preventDefault();
        e.stopPropagation();
        return false;

    },
    template:function(){
        var pageTemp=' <span class="fa fa-angle-right expand-item"></span> <span class="fa fa-edit expand-item"></span> <span class="fa fa-trash expand-item"></span> <span class="fa fa-file-text-o"></span> <span class="search-item-txt" title="<%=title%>"><%=title%></span>';
        return template.compile(pageTemp);
    },

    render: function () {
        console.log(this.model);
        var page=this.model.toJSON();

        this.$el.attr({"class":"list-group-item  search-item","id":("page-"+page['id'])});

        this.$el.html(this.template()(page));

        return this;
    }


});


var PageListView = Backbone.View.extend({

    el: $("#app-pages"),
    initialize: function () {
        this.listenTo(pages, "add", this.addPageView);
        var self=this;
        this.filterPage=_.debounce(function () {
            var text=self.$el.find(".search_txt").val();
            var filterLis = self.$el.find(">.search-item");

            var rex = new RegExp(text, 'i');
            filterLis.hide();
            filterLis.filter(function () {
                return rex.test($(this).find('.search-item-txt').text());
            }).show();
        },300);
        pages.fetch();
    },
    addPageView: function (page, at) {

        var view = new PageView({model:page});

        this.$el.append(view.render().el);
    },
    events: {
        "click .panel-heading .fa-plus":"addPage",
        "keyup .search_txt":"filterPage"
    },


    addPage:function(e){
        var pageTemp='<form role="form form-inline" action="/page/updatecontent" id="page-add-form"> <div class="form-group"> <input type="text" class="form-control" name="title" placeholder="页面标题"> </div> </form>';
        var html=template.compile(pageTemp)({});

        openModal(this.$el,'增加页面',html,{ok:function(modalId){
            var page=new PageModel({title:$("#"+modalId).find("input[name=title]").val()});
            pages.create(page,{
                success: function (cols, resp, opt) {
                    console.log("chengong", resp);

                    page.set("id", resp.data.id);
                    pages.add(page);
                }
            });
            //page.save(page.toJSON(), {
            //    success: function (cols, resp, opt) {
            //        console.log(typeof resp, resp);
            //
            //        page.set("id", resp.data.id);
            //        pages.add(page);
            //    }
            //});
        }});


    },
    selRightView:function(e){
        var data=e.currentTarget.getAttribute("data");

        if(data!='left'){

            $(e.currentTarget.parentNode).toggleClass('active');
        }
        else
        {
            e.preventDefault();
            return false;
        }
        var leftPanel=$("div.leftPanel");
        var curCol=parseInt(/col-sm-(\d+).+/.exec(leftPanel.attr("class"))[1]);
        var classPre=["col-sm","col-md","col-lg"];

        switch(data){

            case 'mid':
            {
                var midPanel=$("div.midPanel");
                if(midPanel.css("display")=='none'){
                    var leftClass=classPre.map(function(item){return item+'-'+(curCol-3)});
                    $("div.leftPanel").attr("class",(leftClass.join(" ")+" leftPanel"));
                }
                else {
                    var leftClass=classPre.map(function(item){return item+'-'+(curCol+3)});
                    $("div.leftPanel").attr("class",(leftClass.join(" ")+" leftPanel"));


                }
                $("div.midPanel").toggle();
                break;
            }
            case 'right':
            {
                var rightPanel=$("div.rightPanel");
                if(rightPanel.css("display")=='none'){

                    var leftClass=classPre.map(function(item){return item+'-'+(curCol-3)});
                    $("div.leftPanel").attr("class",(leftClass.join(" ")+" leftPanel"));
                }
                else {
                    var leftClass=classPre.map(function(item){return item+'-'+(curCol+3)});
                    $("div.leftPanel").attr("class",(leftClass.join(" ")+" leftPanel"));

                }
                $("div.rightPanel").toggle();
                break;
            }

        }

    }

});
var pageListView=new PageListView;


var localSavePage=function(content){
    var pageId=$("#page-editor").attr("data");
    localStorage.setItem("page"+pageId,content);

}

var showAlert=function(title,content,alertType){
    var alertHtml=template(alertTemp)({alertType:(alertType || "info"),title:title,content:content});
    var alertObj=$(alertHtml);
    $("body").append(alertObj);
    alertObj.slideDown(400,function(){
        var that=this;
        setTimeout(function(){$(that).slideUp();},2000);
    });

}
var saveLayout=function(){
    setuploadingEffect($("#page-save>a"));

    $.post(
        "/page/updatecontent",
        {'content':$('.demo').html()},
        function(data) {
            showAlert("Page"," save successfully");
        }
    );
}


function randomNumber() {
    return randomFromInterval(1, 1e8)
}
function randomFromInterval(e, t) {
    return Math.floor(Math.random() * (t - e + 1) + e)
}

var currentDocument = null;
var timerSave = 1000e3;


seajs.use([(config.jsdir+'bswiz')],function(){

    seajs.use([(config.jsdir+'common/header')]);

    $('#rootwizard').bootstrapWizard({'tabClass': 'bwizard-steps',

        checkTab:function(obj){
            // $("ul.bwizard-steps").hide('fast');
            // $("#page-editor>nav").slideDown('fast');
            // return true;

            if(obj.find("a").size()>0)
            {
                console.log(obj.find("a").attr("href"));


                var tag=obj.find("a").attr("href");
                if(tag=="#tab1"){
                    return true;
                    if(true || typeof $("#pageinfo-form").attr('data')==='undefined')
                    {
                        showTips(obj,config.lang.choose_temp,{place:"right"});
                        return false;
                    }

                }
                else if(tag=="#tab2"){
                    ajaxCbs['pageinfo-form']=[function(data) {
                    }]
                }
                else if(tag=="#tab3"){
                    ajaxCbs['pageinfo-form']=[function(data) {
                    }]
                }
                else if(tag=="#tab4"){
                    ajaxCbs['pageinfo-form']=[function(data) {
                    }]
                }
                else if(tag=="#tab5"){
                    ajaxCbs['pageinfo-form']=[function(data){

                        $("ul.bwizard-steps").hide('fast');
                        $("#page-editor>nav").slideDown('fast');

                        var form=$("#pageinfo-form");
                        /**
                         * @todo add share password function
                         */
                        pages.add(new Page({id:data.id,title:form.find("input[name=title]").val(),
                            desc:form.find("textarea[name=desc]").val(),
                            privacy:form.find("input[name=privacy]").val(),
                            pwd:(form.find("input[name=pwd]").size()>0)?form.find("input[name=pwd]").val():''
                        }));
                        $("#page-editor").attr("data",data.id);
                        $(".pager.wizard").hide();



                    },''];
                    if(ajaxSubmit($("#pageinfo-form"),$("#pageinfo-form"))===false)
                    {
                        showTips(obj,config.lang.fill_form,{place:"right"});
                        return false;
                    }


                }
                return true;
            }



        },
        onTabClick:function(obj){

            return this.checkTab(obj);


        },
        onNext:function(obj){

            return this.checkTab(obj);

        }


    });



 //   $("[href=#tab3]").trigger('click');

    $(".display-type li i").tooltip({placement:'right'});

    $(window).resize(function() {
        $("body").css("min-height", $(window).height() - 90);
        $(".demo").css("min-height", $(window).height() - 160)
    });
    $(document).ready(function() {

        $("body").css("min-height", $(window).height() - 90);


        $("#page-edit").click(toogleDragPanel);



    });



    $("#create-div").on('click','a.choose-btn',function(){
        console.log("select a template");
        var selTemp=$(this).closest(".site-temp");
        $("#create-div .site-temp").removeClass("temp-select");

        $("#create-div a.choose-btn").find("i").remove();

        $(this).closest(".site-temp").addClass("temp-select");
        $(this).append("<i class='fa fa-check text-danger'></i>");

        $("#pageinfo-form").data($("#pageinfo-form").attr('data'),$(this).attr("data"));
    });


    randomNumber();
    $("#pageinfo-form").attr('data',"page"+randomNumber());





});
childWindow="";

