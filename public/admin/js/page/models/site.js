define(function(require,exports,module){
    module.exports=Backbone.Model.extend({
        defaults: {
            "title":  config.lang.unnamed,
            "desc":  ""
        },
        idAttribute:'_id',
        //不要增加默认idAttribute的如id:""否则导致无法触发create.json
        urlRoot: '/user',
        initialize:function(){
            // this.listenTo(this,'remove',this.onRemove);

        },
        url:'/site/list.json',
        urls: {
            'read': '/site/get.json',
            'create': '/site/create.json',
            'update': '/site/update.json',
            'patch': '/site/updatecontent.json',
            'delete': '/site/delete'
        },
        onRemove:function(e){
            console.log(e,"remove page");
        },
        onChange:function(model){

        }

    });

});

var site=new SiteModel();
site.on('change:title',function(model,a,b){

    console.log(model,'a',a,'b',b);
    $(".app-title").text(model.get("title"));

});

