define(function(require, exports, module) {
    var Backbone=require('backbone');
    var template=require('art');
    Backbone.View.extend({
        model:PageModel,
        tagName:'li',
        initialize: function () {
            this.listenTo(this.model,'change:title',this.onChange);
        },
        events: {
            "click": "loadPage",
            "click .fa-trash":"rmPage",
            "click .fa-edit":"editPage"
        },
        loadPage:function(e){
            this.$el.parent().find("li").removeClass('list-item-active');
            this.$el.addClass('list-item-active');
            var temp=(this.model.get('content')?this.model.get('content'):'');
            var html=template.compile(temp)({id:this.model.get('id')});
            var htmlObj=$(html);
            var pcId='pc_'+this.model.id;
            $(".config-section .page-config:not(#"+pcId+")").hide();
            var self=this;
            //htmlObj.attr({'id':pcId}).addClass('page-config');
            if($("#"+pcId).size()>0){
                $("#"+pcId).show();
            } else {
                htmlObj.find("[data-ref][data-source]").each(function(idx,item){

                    var action='add';
                    var uniId=$(this).attr('data-id');
                    var rmDom=$(this).parent().parent();
                    if(!$(this).attr('data-id')){

                        uniId=randomNumber();
                        $(this).attr({"data-id":uniId});
                    }
                    if(!rmDom.attr("data-source")){
                        rmDom.attr({"data-id":$(this).attr('data-id'),"data-ref":$(this).attr('data-ref'),"data-source":$(this).attr('data-source')});
                    }

                    ibAdvPub($(item).attr('data-ref'),self.model.get('id'),action,uniId);
                });
            }

            $(".view-container").html(htmlObj);
        },
        onChange:function(model){
            this.$el.find(".search-item-txt").text(model.get('title'));
        },
        editPage:function(e){
            var pageTemp='<form role="form form-inline" action="/page/updatecontent" id="page-edit-form"> <div class="form-group"> <input type="text" class="form-control" value="<%=value%>" name="title" placeholder="页面标题"> </div> </form>';
            var html=template.compile(pageTemp)({value:this.model.get("title")});
            var self=this;
            openModal(this.$el,'重命名页面',html,{ok:function(modalId){
                self.model.set({title:$("#"+modalId).find("input[name=title]").val()});

            }});

        },

        rmPage:function(e){
            var self=this;
            openModal($(this),(config.lang.ok_rm_page+" "+self.model.get('title')),"确认删除该页面吗,删除该页面后将无法恢复",{ok:function(){

                $(".view-container").empty();
                $(".config-section").empty();
                self.$el.remove();
                self.model.destroy();
            }},{},{id:("rm-page-"+self.model.get(self.model.idAttribute))});
            e.preventDefault();
            e.stopPropagation();
            return false;

        },
        template:function(){
            var pageTemp=' <span class="fa fa-angle-right expand-item"></span> <span class="fa fa-edit expand-item"></span> <span class="fa fa-trash expand-item"></span> <span class="fa fa-file-text-o"></span> <span class="search-item-txt" title="<%=title%>"><%=title%></span>';
            return template.compile(pageTemp);
        },

        render: function () {
            console.log(this.model);
            var page=this.model.toJSON();

            this.$el.attr({"class":"list-group-item  search-item","id":("page-"+page['id'])});

            this.$el.html(this.template()(page));

            return this;
        }


    });

    module.exports={};
});

