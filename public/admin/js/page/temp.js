function jq( myid ) {
 
    return "#" + myid.replace( /(:|\.|\[|\]|,)/g, "\\$1" );
 
}
(function($) {

    var o = $({});

    $.sub= function() {
        o.on.apply(o, arguments);
    };

    $.unsub= function() {
        o.off.apply(o, arguments);
    };

    $.pub = function() {
        o.trigger.apply(o, arguments);
    };

}(jQuery));
function randomNumber(prefix) {
    var prefix=prefix || '';
    return prefix+randomFromInterval(1, 1e8);
}
function randomFromInterval(e, t) {
    return Math.floor(Math.random() * (t - e + 1) + e)
}
$(function(){

    $("html,body,html *").off();

    $("body").on('click','a',function(e){

        e.preventDefault();
        return false;
    });
    $.sub('input',function(evt,data){
        console.log(data);
        var editDom=$("[data-edit='"+data.name+"']");
        var attrDom=$("[data-attr='"+data.name+"']");
        if(attrDom.size()>0){
            if(attrDom[0].tagName.toLowerCase()=='a'){
                attrDom.attr('href',data.value);
            }

        }
        if(data.type!='file'){

            if(editDom.size()==0){
                var displayDoms=[];
                if(!$.isArray(data.value)){
                    data.value=[data.value];
                }
                displayDoms=data.value.map(function(val){
                    return "[data-display='"+data.name+"="+val+"']";
                });

                $("[data-display^='"+data.name+"=']").hide();
                $(displayDoms.join(",")).show();

            } else {

                editDom.html(data.value);
            }

        } else {
            if(editDom.size()>0 && editDom[0].tagName.toLowerCase()=='img'){
                 editDom.attr('src',data.value);
             } 
           
        }
    });
    $("[data-am-widget]").on('click',function(){
        $(this).parent().find('[data-am-widget]').removeClass('comp-select');
        $(this).addClass('comp-select');
        var dataCid=$(this).attr('data-cid');
        if(!dataCid){
            dataCid=randomNumber("c_");
            $(this).attr('data-cid',dataCid);
        }
        
        parent.$('body').trigger("load_comp_cfg",{comp:$(this).attr("data-am-widget"),'cid':dataCid});
    });
    //when loading finish
    $("#_ry_temp_css").size()>0 && $("#_ry_temp_css").remove();
    //$("[data-edit],[data-display]").css({'display':'block !important'});

});