$("body").on('click','.file-upload',popImgUploadWin);
$("body").on('load_comp_cfg',compCfg);
$("body").on('click','#vc a.remove',function(e){
    var cid=$(this).parent().find(".view [data-cid]").attr("data-cid");
    $("[data-ccid="+cid+"]").remove();
});

$("body").on('click','.device .device__content a[contenteditable=true]:not([data-toggle=collapse])',function(evt){
    var self=this;
    var aTag=$(this);
    var aTagLi=$(this).parent();
    var dataCid=$(this).attr("data-cid");
    var opts={};
    for(var i= 0,len=pages.models.length;i<len;i++){
        opts[pages.models[i].get('_id')]=pages.models[i].get('title');
    }
    var cfgPanel=$(".config-section .panel-body");
    cfgPanel.children().hide();
    if(!dataCid){

        dataCid=randomNumber("a_");
        $(this).attr("data-cid",dataCid);

        var aObj=$(template.compile(aTemp)({txt:$(this).text(),target:$(this).attr("href"),'opts':opts}));

        aObj.attr("data-ccid",dataCid);
        aObj.on("change","input",function(evt){
            if($(this).hasClass('ext-link')){
                $("a[data-cid="+dataCid+"]").attr('href',this.value);
            } else {
                $("a[data-cid="+dataCid+"]").text(this.value);
            }

        });
        aObj.on("click",".add-link",function(evt){

            $('[data-cid='+dataCid+']').after($('[data-cid='+dataCid+']').clone().removeAttr("data-cid"));

            evt.preventDefault();
            return false;

        });
        aObj.on("click",".rm-link",function(evt){

            $('[data-cid='+dataCid+']').remove();
            $(".config-section .panel-body").empty();
            evt.preventDefault();
            return false;

        });
        aObj.on('change','select',function(evt){
            console.log($(this).val());
            if($(this).val()=='_ex'){
                aObj.find(".ext-link").show();
            } else {
                aObj.find(".ext-link").hide();
            }
        });

        $(cfgPanel).append(aObj);
    } else {
        cfgPanel.children('form'+'[data-ccid='+dataCid+']').show();
    }







});

$("body").on('click','.device .device__content .carousel-inner img',function(evt) {
    console.log("clicked img");

    var self=this;
    var aTag=$(this);
    var aTagLi=$(this).parent();
    var carousel=$(this).parent().parent().parent();
    var carInner=carousel.find(".carousel-inner");
    var carInd=carousel.find(".carousel-indicators");

    var cfgPanel=$(".config-section .panel-body");

    cfgPanel.children().hide();
    var dataCid=$(this).attr("data-cid");
    if(!dataCid){
        dataCid=randomNumber("c_");
        $(this).attr("data-cid",dataCid);
        var opts={};
        var slideObj=$(template.compile(slideTemp)({txt:$(this).text(),'opts':opts})).attr("data-ccid",dataCid);

        slideObj.on("click",".add-link",function(evt){
            carInner.append(carInner.find(">.item").eq(0).clone().removeClass("active"));
            carInd.append(carInd.find(">li").eq(0).clone().removeClass("active").attr("data-slide-to",carInd.find(">li").size()));
            evt.preventDefault();
            return false;

        });
        slideObj.on("click",".rm-link",function(evt){

            //
            $(self).parent().removeClass("item").hide();//remove();
            carInd.find(">li").eq($(self).parent().prevAll().size()).remove();
            carInd.find(">li").each(function(idx,item){
                console.log("indicate",item,idx);
                $(item).attr("data-slide-to",idx);
                if(idx==0){
                    $(item).addClass("active");
                }

            });
            carInd.parent().carousel('next');
            $(".config-section .panel-body").empty();
            evt.preventDefault();
            return false;

        });
        cfgPanel.append(slideObj);
        initUploadPanel(slideObj.find("div[data-upload='yes']")[0],function(data){
            $(self).attr("src",config.uploadPrefix+data.data.mediaurl);
            console.log("uploaded",data);
        });
    } else {

        cfgPanel.find(">form").show();
    }





});
$('body').on('click', "#vc .mask",function (e) {

    var orgThis=this;
    var self=$(this).next();
    var textAreaTag=self;
    var dataCid=self.attr("data-cid");
    var cfgPanel=$(".config-section .panel-body");
    cfgPanel.children().hide();
    if(!dataCid){
        dataCid=randomNumber("if_");
        $(this).attr("data-cid",dataCid);
        var exampleData='';
        var prevHtml='<style>ul[data-comp=video] img {max-width: 350px}</style> <br><ul class="nav nav-tabs" role="tablist" data-comp="video"> <li role="presentation" class="active"><a href="#youku_share" aria-controls="home" role="tab" data-toggle="tab">优酷</a></li> <li role="presentation"><a href="#iqiyi_share" aria-controls="profile" role="tab" data-toggle="tab">爱奇艺</a></li> <li role="presentation"><a href="#qq_share" aria-controls="messages" role="tab" data-toggle="tab">腾讯</a></li>  </ul> <div class="tab-content"> <div role="tabpanel" class="tab-pane active" id="youku_share"><img src="<%=imgdir%>/page_temp/youku_share.png" alt=""></div> <div role="tabpanel" class="tab-pane" id="iqiyi_share"><img src="<%=imgdir%>/page_temp/iqiyi_share.png" alt=""></div> <div role="tabpanel" class="tab-pane" id="qq_share"><img src="<%=imgdir%>/page_temp/qq_share.png" alt=""></div>  </div>';

        var textArea=$('<textarea class="form-control" rows="5" placeholder="'+config.lang.iframeData+'"></textarea>').attr("data-ccid",dataCid);


        textArea.on('change',function(evt){
            $(orgThis).next().replaceWith($(this).val());

        });
        var bdEditor=[textArea,template.compile(prevHtml)({imgdir:config.imgdir})];

        cfgPanel.append(bdEditor);



    } else {
        $("#"+dataCid).show();

    }


});
$('body').on('click', "#vc .dropdown-menu>li>a",function (e) {
    return false;
    console.log("logg");

});

$('body').on('click','.tab-pane p[contenteditable],.panel-body[contenteditable]',function(e){
    console.log("p.contenteditable");
    var self=this;
    var textAreaTag=$(this);
    var dataCid=$(this).attr("data-cid");
    var cfgPanel=$(".config-section .panel-body");
    cfgPanel.children().hide();
    if(!dataCid){
        dataCid=randomNumber("t_");
        $(this).attr("data-cid",dataCid);

        var bdEditor=$("<div id='"+bdEditorId+"'></div>").attr("data-ccid",dataCid);
        var bdEditorId='ed_'+dataCid;
        var bdEditor=$("<div id='"+bdEditorId+"'></div>");
        cfgPanel.append(bdEditor);
        seajs.use(['assets/ueditor/ueditor.config.textarea.js'],function(){
            seajs.use(['assets/ueditor/ueditor.all.js'],function(){
                seajs.use("assets/ueditor/lang/zh-cn/"+config.langType+".js",function(){
                    var ue = UE.getEditor(bdEditorId);

                    ue.addListener( 'contentChange', function( editor ) {
                        $(self).html(ue.getContent());
                    });
                });

            });

        });


    } else {
        var bdEditorId='ed_'+dataCid;
        $("#"+bdEditorId).show();
    }



});


$("#page-download").on('click',function(e){

    var html='<div class="modal fade"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> <h4 class="modal-title">Page Source Code</h4> </div> <div class="modal-body"> <textarea style="width:100%;height: 100%;min-height: 500px">modalcontent</textarea> </div> </div> </div> </div>';

    var newHtml=html.replace(/modalcontent/i,getSource());
    console.log(newHtml);
    $(newHtml).modal();

});



