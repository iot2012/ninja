
var customEditor=function(isPage){
    var isPage=isPage || false;
    if($("#config_wizard").size()==0){

        var wizard='<div id="config_wizard"> <ul class="wiz-ul"> </ul> <div class="tab-content wiz-item" style="margin-top: 7px"> </div> <ul class="pager wizard"> <li class="previous first" style="display:none;"><a href="#"><%=l_n_first%></a></li> <li class="previous"><a href="#"><%=l_n_prev%></a></li> <li class="next last" style="display:none;"><a href="#"><%=l_n_last%></a></li> <li class="next"><a href="#"><%=l_n_next%></a></li> <li class="app-gen hidden"><a href="#"><%=l_gen_app%></a></li> </ul> </div>';
        var lang=langPluck(config.lang,['gen_app','n_last','n_first','n_prev','n_next']);
        $(".config-panel").html(template.compile(wizard)(lang));
    }


    $("body").on('change',"form:not(#pageinfo-form) input,textarea",function(evt){

        var inputType=$(this).attr('type');
        if(inputType!='file'){
            var vals=$(this).val();

            if(inputType=='checkbox'){
                vals=[];
                $("[name='"+$(this).attr('name')+"']:checked").each(function(idx,item){
                    vals.push($(item).val());
                });
            }

            var iframeId="vc_"+site.get("tmpId");
            console.log("iframeid",iframeId);



            $("#"+iframeId)[0].contentWindow.$.pub('input',{name:$(this).attr('name').replace(/\[\]/,''),type:$(this).attr('type'),'value':vals});

        } else {
            var iframeId="vc_"+site.get("tmpId");
            $("#"+iframeId)[0].contentWindow.$.pub('input',{name:$(this).attr('name').replace(/\[\]/,''),type:$(this).attr('type'),'value':$(this).val()});
        }

    });
    var iframeId="vc_"+site.get("tmpId");

    var tempDir=getTempDir(site.get("tmpName"),site.get("tmpId"));
    var tempUrl=getTempUrl(site.get("tmpName"),site.get("tmpId"));

    if($("#"+iframeId).size()==0){
        $("#vc .device__content").append(createIframe({'onLoad':"iframeOnLoad(this)",'id':iframeId,'src':tempUrl})).css({'overflow':'hidden'});
    } else {
        $("#"+iframeId).attr({'onLoad':"iframeOnLoad(this)",'id':iframeId,'src':tempUrl}).css({'overflow':'hidden'});
    }



    var cfgUrl=getConfigUrl(site.get("tmpName"),site.get("tmpId"));

    seajs.use(["form_builder"],function(builder){

        $.when($.getJSON((cfgUrl+"config_all.json")) , $.getJSON((cfgUrl+"locale/"+config.langType+".json"))).then(function(config,lang) {



            var wizLis=[];
            var wizItems=[];
            // <li><a href="#ztab1" data-toggle="tab"><span class="label">1</span>{{$lang.choose_temp}}</a></li>
            var cnt=1;
            console.log("config",config[0].wizard);

                //
                //var pageInfoLi='<li><a href="#_page_info" data-toggle="tab"><span class="label">1</span>设置页面信息</a></li>';
                //var pageInfoItem='<div class="tab-pane panel panel-default" id="_page_info" style="border-top:0px"><form role="form" class="ajax-form form-horizontal" action="/admin/site/add" method="post" id="pageinfo-form"> <div class="form-group"> <label class="col-sm-6 col-md-3 col-lg-3 control-label _extra-cls">页面名字</label> <div class="col-sm-6 col-md-9 col-lg-9"> <input type="text" class="form-control required"  name="title" id="title" placeholder=""> </div> </div> <div class="form-group"> <label class="col-sm-6 col-md-3 col-lg-3 control-label _extra-cls">页面描述</label> <div class="col-sm-6 col-md-9 col-lg-9"> <textarea type="text" class="form-control required" name="desc" id="desc" placeholder="" rows="6"></textarea><input type="hidden" name="_type" value="page"/> </div> </div> </form></div>';
                //
                //
                //
                //wizLis.push(pageInfoLi);
                //wizItems.push(pageInfoItem);
                //


            for(var section in config[0].wizard){
                console.log("config",config,config[0]['wizard'][section]);
                var wizLi='<li><a href="#w_'+section+'" data-toggle="tab"><span class="label">'+cnt+'</span>'+translate(lang[0],section)+'</a></li>'
                wizLis.push(wizLi);
                var buildData=builder.process(config[0]['wizard'][section],lang[0],(section+'_'),{'_tmpId':site.get('tmpId'),'_tmpName':site.get('tmpName'),'_siteId':site.get('_id')});
                var item='<div class="tab-pane panel panel-default" id="w_'+section+'" style="border-top:0px">' +buildData+'</div>';
                wizItems.push(item);
                cnt++;
            }
            console.log(wizItems.join(""));
            var logo="[name$=logo]";
            var imgs="[name$='imgs[]']";
            var img="[name$=img]";
            var imgDoms=[logo,imgs,img];



            $("#config_wizard .wiz-ul").prepend(wizLis.join(""));
            $("#config_wizard .wiz-item").prepend(wizItems.join(""));

            var lastLi='<li><a href="#w_last_" data-toggle="tab"><span class="label">'+wizLis.length+'</span>完成</a></li>';


            var lastPane='<div class="tab-pane panel panel-default" id="w_last_" style="border-top:0px">'+'<div class="alert alert-info alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> <h4>店面装修成功</h4> <p>恭喜您的店面装修成功,店面地址是:<span class="shop-link"></span></p> <p> <div class="btn-group btn-group-justified" role="group" aria-label="Justified button group"> <a href="#" class="btn btn btn-primary btn-block wiz-finish" role="button">继续创建</a> <a href="#" class="btn btn-default btn-block wiz-close" role="button" onclick="window.close()">关闭窗口</a> </div> </p> </div>';
            if(isPage){
                var lastPane='<div class="tab-pane panel panel-default" id="w_last_" style="border-top:0px">'+'<div class="alert alert-info alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> <h4>页面创建成功</h4> <p>恭喜您的页面创建成功,地址是:<span class="shop-link"></span></p> <p><div class="btn-group btn-group-justified" role="group" aria-label="Justified button group"> <a href="#" class="btn btn btn-primary btn-block wiz-finish" role="button">继续创建</a> <a href="#" class="btn btn-default btn-block wiz-close" role="button" onclick="window.close()">关闭窗口</a> </div></p> </div>';
            }


            $('a[download]').each(function(idx,a){
                $(a).attr("href",tempDir+"/config/data/"+$(a).attr("href"));
            });

            $("#config_wizard").bootstrapWizard({'tabClass': 'bwizard-steps', onTabClick:function(tab, navigation, index){
                console.log('click',tab, navigation, index);
            },onNext:function(tab,nav,idx){
                var form=$(tab.find(">a").attr("href")).find("form");

                console.log("tabya",tab,nav,idx,form);
                /**
                 * 填写页面信息验证
                 */

                ajaxCbs[form.attr("id")]=[function(data) {
                    //如果成功触发下一步
                    console.log("wiz submit return", data);
                    if (form.attr("id") == 'pageinfo-form') {
                        $("input[name=_siteId]").val(data._id);
                        site.set('_id',data._id);
                        site.set('title',form.find("[name=title]").val());
                        site.set('desc',form.find("[name=desc]").val());

                    }
                    if(form.find(".app-gen").size()==0){
                        $(tab.next().find("a")).trigger('click');

                    }


                }];


                if(ajaxCheck(form)){
                    ajaxSubmit(form);

                }

                return false;

            },onTabShow: function(tab, navigation, index) {
                //var prevSel="_"+navigation.parent().attr('id')+"_prev";
                //var prevDom=$(document).data(prevSel)?$(document).data(prevSel):navigation.find("li").eq(0).attr();
                //console.log(this,tab,navigation,index);
                if(index==1){

                } else {

                }
                $("#config_wizard .app-gen").addClass('hidden');
                var activeLi = navigation.find('li.active');
                var section=activeLi.find(">a").attr("href");
                var size=$(navigation).find("li").size();
                if((size-1)==index){
                    $("#config_wizard .app-gen>a").off('click');
                    $("#config_wizard .app-gen").removeClass('hidden');
                    var lastForm=$(section).find("form");
                    ajaxCbs[lastForm.attr("id")]=[function(data){
                        //如果成功触发下一步
                        console.log("callback");

                        if(typeof opener.$=='object'){
                            opener.$.publish('service_id',{
                                title:site.get('title'), _id:site.get('_id')
                            });
                        }

                        $("#config_wizard").replaceWith(lastPane);
                    },function(){
                    }];

                    $("#config_wizard .app-gen>a").on('click',function(e){



                        ajaxSubmit(lastForm);

                        e.preventDefault();
                        return false;
                    });

                }

                var sectionName=section.replace(/#w_/,'');

                $(section).find(".upload-file").each(function(idx,item){
                    if($(item).find(".webuploader-pick").size()==0 && $(item).size()>0  && $(item).attr('id')){
                        var name=$(item).attr('name');
                        var input=$(item).find("input[type=hidden][name='"+name+"']");
                        createUploader({accept:{extensions:(($(item).attr("data-ext")=='')?"":$(item).attr("data-ext"))},id:"#"+$(item).attr('id'),name:$(item).attr('name'),complete:function(resp){
                            if(input.size()==0 && resp.code==1){
                                input=$('<input>').attr({name:name,value:resp.data.mediaurl,type:'hidden'});
                                $(item).prepend(input);
                            }
                            console.log("site",site.toJSON());
                            var iframeId="vc_"+site.get("tmpId");

                            $("#"+iframeId)[0].contentWindow.$.pub('input',{name:$(item).attr('name').replace(/\[\]/,''),type:$(item).attr('type'),'value':$(item).find(".upload-img>img").attr("src")});
                        }});
                    }

                });
                var frameUrl=$("#"+iframeId).attr('src').split("/");
                var lastUrl=frameUrl.pop();
                var frameDir=(frameUrl.length==0)?'':frameUrl.join("/");
                if(index!=0){
                    $("#"+iframeId).attr('src',(frameDir+'/'+(('target' in config[0]['wizard'][sectionName])?config[0]['wizard'][sectionName]['target']:'index.html')));

                }

            }
            });


        });


    });


};
var SiteModel=Backbone.Model.extend({
    defaults: {
        "title":  config.lang.unnamed,
        "desc":  ""
    },
    idAttribute:'_id',
    //不要增加默认idAttribute的如id:""否则导致无法触发create.json
    urlRoot: '/user',
    initialize:function(){
        // this.listenTo(this,'remove',this.onRemove);

    },
    url:'/site/list.json',
    urls: {
        'read': '/site/get.json',
        'create': '/site/create.json',
        'update': '/site/update.json',
        'patch': '/site/updatecontent.json',
        'delete': '/site/delete'
    },
    onRemove:function(e){
        console.log(e,"remove page");
    },
    onChange:function(model){

    }

});
var Pages=Backbone.Collection.extend({

    model: PageModel,


    initialize: function () {
        // this.listenTo(this,'change:isChanged',this.renderControl);
        //this.listenTo(this,'change:uids',this.changeUids);

    },
    url: '/page/read.json',
    comparator: 'mtime'

});

var PageView=Backbone.View.extend({
    model:PageModel,
    tagName:'li',
    initialize: function () {
        this.listenTo(this.model,'change:title',this.onChange);
    },
    events: {
        "click": "loadPage",
        "click .fa-trash":"rmPage",
        "click .fa-edit":"editPage"
    },
    loadPage:function(e){

        if(this.$el.parent().find("li.list-item-active").attr("id")==this.$el.attr("id")){

            return false;
        }
        this.$el.parent().find("li").removeClass('list-item-active');
        this.$el.addClass('list-item-active');
        var temp=(this.model.get('content')?this.model.get('content'):'');
        var html=template.compile(temp)({id:this.model.get(this.model.idAttribute)});
        var htmlObj=$(html);
        var pcId='pc_'+this.model.get(this.model.idAttribute);
        $(".config-section .page-config:not(#"+pcId+")").hide();
        var self=this;
        //htmlObj.attr({'id':pcId}).addClass('page-config');
        if($("#"+pcId).size()>0){
            $("#"+pcId).show();
        } else {
            htmlObj.find("[data-ref][data-source]").each(function(idx,item){

                var action='add';
                var uniId=$(this).attr('data-id');
                var rmDom=$(this).parent().parent();
                if(!$(this).attr('data-id')){

                    uniId=randomNumber();
                    $(this).attr({"data-id":uniId});
                }
                if(!rmDom.attr("data-source")){
                    rmDom.attr({"data-id":$(this).attr('data-id'),"data-ref":$(this).attr('data-ref'),"data-source":$(this).attr('data-source')});
                }

                ibAdvPub($(item).attr('data-ref'),self.model.get('id'),action,uniId);
            });
        }
        $("#vc .device__content").html(htmlObj);
    },
    onChange:function(model){
        this.$el.find(".search-item-txt").text(model.get('title'));
    },
    editPage:function(e){
        var pageTemp='<form role="form form-inline" action="/page/updatecontent" id="page-edit-form"> <div class="form-group"> <input type="text" class="form-control" value="<%=value%>" name="title" placeholder="页面标题"> </div> </form>';
        var html=template.compile(pageTemp)({value:this.model.get("title")});
        var self=this;
        openModal(this.$el,'重命名页面',html,{ok:function(modalId){
            self.model.set({title:$("#"+modalId).find("input[name=title]").val()});

        }},null,null,null,{'show_footer':true});

    },

    rmPage:function(e){
        var self=this;
        openModal($(this),(config.lang.ok_rm_page+" "+self.model.get('title')),"确认删除该页面吗,删除该页面后将无法恢复",{ok:function(){

            $(".view-container").empty();
            $(".config-section").empty();
            self.$el.remove();
            self.model.destroy();
        }},{},{id:("rm-page-"+self.model.get(self.model.idAttribute))});
        e.preventDefault();
        e.stopPropagation();
        return false;

    },
    template:function(){
        var pageTemp=' <span class="fa fa-angle-right expand-item"></span> <span class="fa fa-edit expand-item"></span> <span class="fa fa-trash expand-item"></span> <span class="fa fa-file-text-o"></span> <span class="search-item-txt" title="<%=title%>"><%=title%></span>';
        return template.compile(pageTemp);
    },

    render: function () {
        console.log(this.model);
        var page=this.model.toJSON();

        this.$el.attr({"class":"list-group-item  search-item","id":("page-"+page[this.model.idAttribute])});

        this.$el.html(this.template()(page));

        return this;
    }


});


var PageListView = Backbone.View.extend({

    el: $("#app-pages"),
    initialize: function () {
        this.listenTo(pages, "add", this.addPageView);
        var self=this;
        this.filterPage=_.debounce(function () {
            var text=self.$el.find(".search_txt").val();
            var filterLis = self.$el.find(">.search-item");

            var rex = new RegExp(text, 'i');
            filterLis.hide();
            filterLis.filter(function () {
                return rex.test($(this).find('.search-item-txt').text());
            }).show();
        },300);
        pages.fetch();
    },
    addPageView: function (page, at) {

        var view = new PageView({model:page});

        this.$el.append(view.render().el);
    },
    events: {
        "click .panel-heading .fa-plus":"addPage",
        "keyup .search_txt":"filterPage"
    },


    addPage:function(e){
        var pageTemp='<form role="form form-inline" action="/page/updatecontent" id="page-add-form"> <div class="form-group"> <input type="text" class="form-control" name="title" placeholder="页面标题"> </div> </form>';
        var html=template.compile(pageTemp)({});

        openModal(this.$el,'增加页面',html,{ok:function(modalId){
            var page=new PageModel({title:$("#"+modalId).find("input[name=title]").val()});

            pages.create(page,{
                success: function (cols, resp, opt) {
                    page.set(page.idAttribute, resp.data[page.idAttribute]);
                    pages.add(page);
                }
            });

        }},null,null,null,{'show_footer':true});


    },
    selRightView:function(e){
        var data=e.currentTarget.getAttribute("data");

        if(data!='left'){

            $(e.currentTarget.parentNode).toggleClass('active');
        }
        else
        {
            e.preventDefault();
            return false;
        }
        var leftPanel=$("div.leftPanel");
        var curCol=parseInt(/col-sm-(\d+).+/.exec(leftPanel.attr("class"))[1]);
        var classPre=["col-sm","col-md","col-lg"];

        switch(data){

            case 'mid':
            {
                var midPanel=$("div.midPanel");
                if(midPanel.css("display")=='none'){
                    var leftClass=classPre.map(function(item){return item+'-'+(curCol-3)});
                    $("div.leftPanel").attr("class",(leftClass.join(" ")+" leftPanel"));
                }
                else {
                    var leftClass=classPre.map(function(item){return item+'-'+(curCol+3)});
                    $("div.leftPanel").attr("class",(leftClass.join(" ")+" leftPanel"));


                }
                $("div.midPanel").toggle();
                break;
            }
            case 'right':
            {
                var rightPanel=$("div.rightPanel");
                if(rightPanel.css("display")=='none'){

                    var leftClass=classPre.map(function(item){return item+'-'+(curCol-3)});
                    $("div.leftPanel").attr("class",(leftClass.join(" ")+" leftPanel"));
                }
                else {
                    var leftClass=classPre.map(function(item){return item+'-'+(curCol+3)});
                    $("div.leftPanel").attr("class",(leftClass.join(" ")+" leftPanel"));

                }
                $("div.rightPanel").toggle();
                break;
            }

        }

    }

});

var __=function(key){

}

var pages=new Pages;
var pageListView=new PageListView;
var site=new SiteModel();
var initData=function(){
    ajaxCbs['pageinfo-form']=[function(data){


        $("ul.bwizard-steps").hide('fast');


        var form=$("#pageinfo-form");
        /**
         * @todo add share password function
         */


        site.set({_id:data[site.idAttribute],pwd:(form.find("input[name=pwd]").size()>0)?form.find("input[name=pwd]").val():'',privacy:form.find("input[name=privacy]").val(),'title':form.find("input[name=title]").val(),'desc':form.find("textarea[name=desc]").val()});

        //console.log(data._id,data);


        $("#page-editor").attr("data",data._id);
        $(".pager.wizard").hide();

        if(site.get('tmpId') && site.get('tmpId')!=-1){
            //alert("tmpnam");
            customEditor();
        } else {
            pages.add(new PageModel({_id:data.pageId,title:"unknown", desc:form.find("textarea[name=desc]").val()}));
            $("#top_toolbar").slideDown();
            $("#page-editor").addClass("custom");
            $(".left-pane,.right-pane,.mid-pane").addClass("col-lg-4 col-md-4 col-sm-12 col-xs-12");
            initCompEvent();


        }


        $("[href=#ztab3]").trigger('click');




    },''];
    site.on('change:title',function(model,a,b){

        console.log(model,'a',a,'b',b);
        $(".app-title").text(model.get("title"));

    });

}
initData();
var makeCRCTable = function(){
    var c;
    var crcTable = [];
    for(var n =0; n < 256; n++){
        c = n;
        for(var k =0; k < 8; k++){
            c = ((c&1) ? (0xEDB88320 ^ (c >>> 1)) : (c >>> 1));
        }
        crcTable[n] = c;
    }
    return crcTable;
}

var crc32 = function(str) {
    var crcTable = window.crcTable || (window.crcTable = makeCRCTable());
    var crc = 0 ^ (-1);

    for (var i = 0; i < str.length; i++ ) {
        crc = (crc >>> 8) ^ crcTable[(crc ^ str.charCodeAt(i)) & 0xFF];
    }

    return (crc ^ (-1)) >>> 0;
};

var aTemp='<form class="form-inline"> <div class="form-group"> <label class="sr-only"><%=txt%></label> <input type="text" class="form-control"  placeholder="<%=txt%>" value="<%=txt%>"> </div> <div class="form-group"> <label class="sr-only" ><%=target%></label> <select class="form-control"><%for(var key in opts){%> <option value="<%=key%>"><%=opts[key]%></option> <%}%><option value="_ex">外部链接</option> </select> </div><div class="form-group"> <label class="sr-only">外部链接地址</label> <input type="text" class="form-control ext-link" placeholder="外部链接地址"  style="display:none"> </div><div class="form-group"><button type="button" class="btn btn-primary  rm-link">删除链接</button> <button type="button" class="btn btn-primary  add-link">添加链接</button></div></form>';


var slideTemp='<form class="form-inline"><div class="form-group btn btn-primary btn-lg" data-upload="yes">上传图片</div><button type="button" class="btn btn-primary btn-lg  rm-link">删除图片</button> <button type="button" class="btn btn-primary btn-lg  add-link">添加图片</button></form>';
var getTempUrl=function(name,id){
    //目录分层假设分成一万个子目录(parseInt(id.substr(id.length-5,5),16)%10000)].join("/")
    return getTempDir(name,id)+"/res/index.html";

}
var getTempDir=function(name,id){
    //目录分层假设分成一万个子目录(parseInt(id.substr(id.length-5,5),16)%10000)].join("/")
    return "/"+["templates",name.replace(/\s+/,_)].join("/")+"/"+id;

}
var translate=function(){
    var langs=[];
    var limiter=' ';
    var lang=arguments[0];
    for(i=1;i<arguments.length;i++){
        langs.push(lang[arguments[i]]?lang[arguments[i]]:arguments[i]);
    }
    if(config.lang.langType!='zhcn'){
        limiter='';

    }
    return langs.join(limiter);

}
var getConfigUrl=function(name,id){
    //目录分层假设分成一万个子目录(parseInt(id.substr(id.length-5,5),16)%10000)].join("/")
    return "/"+["templates",name.replace(/\s+/,_)].join("/")+"/"+id+"/config/";

}
var initWizard=function(){


        $('#rootwizard').bootstrapWizard({'tabClass': 'bwizard-steps',

            checkTab:function(obj){
                // $("ul.bwizard-steps").hide('fast');
                // $("#page-editor>nav").slideDown('fast');
                // return true;che
                if(obj.find("a").size()>0)
                {
                    console.log(obj.find("a").attr("href"));

                    var tag=obj.find("a").attr("href");
                    if(tag=="#ztab1"){

                        if(typeof site.get('tmpId')==='undefined')
                        {
                            showTips(obj,config.lang.choose_temp,{place:"right"});
                            return false;
                        }

                    }
                    else if(tag=="#ztab2"){

                        if(!site.get('_id')){
                            showTips(obj,config.lang.fill_form,{place:"right"});
                            return false;
                        }
                        $(".bwizard-steps").hide();
                        $("#app-pages").find(">.list-group-item").eq(0).trigger('click');



                        $.get('/user/doneBackendTour').done(function(data){

                            var data= $.parseJSON(data);
                            if(data.data!='1'){
                                seajs.use(["bootstro","/css/bootstro.css"],function(){

                                    bootstro.start($(".bootstro"), {finishButton:'<button class="btn btn-mini btn-success bootstro-finish-btn"><i class="icon-ok"></i>'+config.lang.finish_txt+'</button>',prevButtonText:config.lang.n_prev,nextButtonText:config.lang.n_next,onComplete:function(idx){
                                        console.log("complete",data);

                                    },onExit:function(data){
                                        $.post('/user/doneBackendTour',{set:1});
                                    }

                                    });
                                });


                            }
                        });

                    }
                    else if(tag=="#ztab3"){

                    }
                    return true;
                }



            },
            onTabClick:function(obj){

                return this.checkTab(obj);


            },
            onNext:function(obj){

                return this.checkTab(obj);

            }


        });


};
var initUploader=function(formId,picName){
    seajs.use('z_uploader',function(uploadDialog){
        $(".upload-icon").on('click',function(evt){
            uploadDialog.show($(this).attr("id"),{
                cbs:{
                    afterSendFile:function(file,data){
                        var picDom=$("#"+formId).find("input[name="+picName+"]");
                        if(picDom.size()==0){
                            var input=$('<input>').attr({name:picName,value:data.data.mediaurl,type:'hidden'});
                            $("#"+formId).prepend(input);
                        } else {
                            picDom.val(data.data.mediaurl);
                        }

                        uploadDialog.hide($(this).attr("id"),$("#img-upload"));
                        console.log(arguments);
                    }

                }
            });
        });
    });

}

var popImgUploadWin=function(e){
    var self=$(this);
    var preview=$(self.attr('prev_target'));
    preview=preview.size()>0?preview:false;
    console.log("preview",preview);
    $.get('/admin/js/table/temp/image_setting.html').done(function(data){
        var imgHtml=template.compile(data)({lang:config.lang});
        seajs.use(['jquery.jcrop', 'canvas2blob', 'load_image', '/css/jquery.jcrop.css'], function () {
            var streaming = false;
        });
        console.log("preview admin",preview);
        console.log("streaming");
        console.log(imgHtml);
        createPopover(self,"上传文件",imgHtml,{delay:{hide:1e6},placement:'bottom',container:'body',trigger:'manual'},{'shown':function(obj){
            /**
             *  当弹出popover的时候执行的callback
             */
            console.log("preview showupload",preview,obj);
            $('.popover-content .ava-tabs a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                shownUploadTabs(e,{
                    'top':obj,
                    'upload_url':(location.protocol+"//"+location.host+'/index/media/upload'),
                    'inputName':'content',
                    'preview_jq':'',
                    'success':function(){}

                });
            });

        }},{'min-width':'400px'});
        self.popover('toggle');

    });


    return false;
}
var prevPhoneInit=function(){

    function setDevice(device) {
        deviceNs = device ? "device--" + device : "";
        containerNs = device ? "container--" + device : "";
        $(".device").removeClass().addClass("device " + deviceNs);
        $(".container").removeClass().addClass("container " + containerNs);
    };

    $(".nav").on("click", "a", function(e) {
        console.log("fdas");
        e.preventDefault();
        setDevice($(e.target).data("device"));
    });

    // $(document).on("keyup", function(e) {
    //     switch(e.keyCode) {
    //         case 49 :
    //             setDevice();
    //             break;
    //         case 50 :
    //             setDevice("ipad-mini");
    //             break;
    //         case 51 :
    //             setDevice("ipad");
    //             break;
    //         case 52 :
    //             setDevice("browser");
    //             break;
    //     }
    // });
}
var initCompEvent=function(){
    $(".demo, .demo .column").sortable({connectWith: ".column",opacity: .35,handle: ".preview"});
    $(".demo .column").sortable({opacity: .35,connectWith: ".column",revert: true,
        stop: function(event, ui) {
            var compDom=$(ui.item);
            $(compDom).parent().find(".box.box-element").removeClass('active');
            $(compDom).addClass('active');


            var compModel=comps.findWhere({'_id':compDom.attr('data-comp-id')});
            var compName=compDom.attr('data-comp-name');
            var compData=$.extend(langPluck(config.lang,compLang),{comp_name:'<span class="glyphicon glyphicon-move"></span>',img_dir:(config.imgdir+'page_temp')});

            if(typeof compModel!='undefined' && compModel.get('html')=='' ||typeof compModel.get('html')=='undefined' ){

                /**
                 * 避免默认的获取到多条数据,从而错误的设置了model
                 */
                compModel.sync('read',compModel,{
                    success:function(resp,code,xhr){
                        console.log("model",resp,'r',resp,'oot',xhr);
                        compModel.set(resp[0]);
                        var html=template.compile(resp[0]['html'])(compData);
                        compDom.html(html);
                    }

                });


            } else {
                var html=template.compile(compModel.get('html'))(compData);
                compDom.html(html);
            }
            seajs.use('/admin/page/comps/'+compName+'.js',function(compName){


            });



        }});

    $(".sidebar-nav .lyrow").draggable({connectToSortable: ".demo",helper: "clone",handle: ".drag",drag: function(e, t) {
        t.helper.width(400);
        console.log("drag");
    },stop: function(e, t) {

        console.log("lyrow sstop");

    }});
    /**
     *
     */
    $(".sidebar-nav .box").draggable({connectToSortable: ".column",helper: "clone",handle: ".drag",drag: function(e, t) {
        t.helper.width(400);

    },stop: function(e,ui) {


        handleJsIds()
    },start:function(e,t){
        console.log("startttt",e,t,this);

    }});
    $("[data-target=#downloadModal]").click(function(e) {
        e.preventDefault();
        downloadLayoutSrc()
    });
    $("#download").click(function() {
        downloadLayout();
        return false
    });
    $("#downloadhtml").click(function() {
        downloadHtmlLayout();
        return false
    });

    $("#clear").click(function(e) {
        e.preventDefault();
        clearDemo()
    });

    $("#page-edit").click(toogleDragPanel);
    seajs.use(["jquery.toolbar","/css/jquery.toolbars.css"],initPrevToolbar);

    $(".nav-header").click(function() {
        $(".sidebar-nav .boxes, .sidebar-nav .rows").hide();
        $(this).next().slideDown()
    });
    $('body').on('click', '#continue-share-non-logged', function () {
        $('#share-not-logged').hide();
        $('#share-logged').removeClass('hide');
        $('#share-logged').show();
    });

    $('body').on('click', '#continue-download-non-logged', function () {
        $('#download-not-logged').hide();
        $('#download').removeClass('hide');
        $('#download').show();
        $('#downloadhtml').removeClass('hide');
        $('#downloadhtml').show();
        $('#download-logged').removeClass('hide');
        $('#download-logged').show();
    });
    $("#page-save a").on('click',function(){saveLayout();});
    $("#external-preview").on('click',function(evt){

        externalPrev();
        evt.preventDefault();
        return false;
    });
    $("#rotate-btn").on('click',function(){
        var pre_width=$("#prev_width").val();
        var pre_height=$("#prev_height").val();
        $("#prev_width").val(pre_height);
        $("#prev_height").val(pre_width);

        devicePrev(pre_height+"px",pre_width+"px");
    });
    setUpRmPopover($("#page-remove"),"Do you want to remove this page?",{placement:"bottom"},{ok:function(){
        $(".demo").empty();
    }});
    removeElm();
    configurationElm();
    gridSystemGenerator();
    setInterval(function() {
        handleSaveLayout()
    }, timerSave);


}

var initBaseEvent=function() {

    $("[href=#all-temps]").on('click',function(){
        $(".site-temp>.tab-pane").addClass('active');
        $(this).addClass('btn-primary');
        $(this).parent().find(".cat-name").removeClass('btn-primary').addClass("btn-default");

    });

    $(".display-type li i").tooltip({placement: 'right'});

    $(window).resize(function () {
        $("body").css("min-height", $(window).height() - 90);
        $(".demo").css("min-height", $(window).height() - 160)
    });
    seajs.use(['cropper', '/css/cropper.css'], function () {
        seajs.use(['cropavatar'], function (CropAvatar) {
            var CropAvatar = new CropAvatar($('#ztab2'));
        });

    });

    $(".search_temp").on('keyup', doGlobalSearch2);

    $("body").on('click', ".z-btn-link", function () {

        $(this).parent().find("a.btn-primary").addClass("btn-default").removeClass("btn-primary");
        $(this).removeClass("btn-default").addClass("btn-primary");
        $($(this).attr("href")).parent().find(".tab-pane").removeClass('active');

        $($(this).attr("href")).addClass('active');

    });

    $("#vc .device .fa-qrcode").on('click', function (evt) {
        genQrCache(getSource, site.get('title'));
    });


    //$(document).on('hidden.bs.modal', function (e) {
    //    $(e.target).removeData('bs.modal');
    //});
    $("#create-div").on('click', 'a.choose-btn', function () {
        site.set('title', $("#" + $(this).attr("data")).find(".temp-title").text());
        site.set('desc', $("#" + $(this).attr("data")).find(".temp-desc").text());
        site.set('tmpId', $(this).attr("data").replace(/site-/, ''));
        site.set('tmpName', $(this).attr("data-temp-name"));
        site.set('ui', $(this).attr("data-ui"));
        $("[href=#ztab2]").trigger('click');


    });

}
var prevDeviceCfg=function(devices){
    var columns=3;

    console.log(devices);
    var columnData=devices.map(function(item,k){

        var len=item.brands.length;
        var count=Math.floor(len/columns);
        var reminder=len-count*columns;
        var columnData=[];

        for(var i=0;i<columns;i++)
        {


            if(i<reminder)
            {
                columnData.push(item.brands.slice(i*(count+1),((i+1)*(count+1))));

            }
            else {
                if(i===reminder)
                {
                    columnData.push(item.brands.slice(i*(count+1),((i+1)*(count+1)-1)));
                }
                else
                {
                    columnData.push(item.brands.slice((i*(count+1)-1),((i+1)*(count+1)-1)));
                }

            }


        }
        return columnData;
    });










    $("#pageinfo-form").attr('data',"page"+randomNumber());
    var temp='<%for(var i=0,len=devs.length;i<len;i++){%> <li class="dropdown dropdown-large"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-15x <%=devs[i].icon%>"></i><b class="caret"></b></a> <ul class="dropdown-menu dropdown-menu-large row"> <%for(var j=0,len=columnData[i].length;j<len;j++){%> <li class="col-sm-4"> <ul> <%for(var k=0,len1=columnData[i][j].length;k<len1;k++){%> <li class="dropdown-header"><%=columnData[i][j][k].name%></li> <%for(var m=0,len2=columnData[i][j][k].devices.length;m<len2;m++){%> <li><a href="#" data-w="<%=columnData[i][j][k].devices[m].w%>" data-h="<%=columnData[i][j][k].devices[m].h%>" data-pxd="<%=columnData[i][j][k].devices[m].pxd%>"><%=columnData[i][j][k].devices[m].name%><span class="badge pull-right"><%=columnData[i][j][k].devices[m].inch%></span></a></li> <%}%> <%}%> <li class="divider"></li> </ul> </li> <%}%> </ul></li> <%}%>';
    var navs=template.compile(temp)({columnData:columnData,columns:columns,devs:devices});
    var navObj=$(navs);
    navObj.on('click','.dropdown-menu-large>li>ul>li>a',function(evt){
        var w=$(this).attr('data-w');
        var h=$(this).attr('data-h');
        var pxd=$(this).attr('data-pxd');
        $("#prev_width").val(w);
        $("#prev_height").val(h);

        devicePrev(w+"px",h+"px");

        console.log(evt,w,h,pxd);
        evt.preventDefault();

        return false;

    });

    $("#prev-li").on('click',function(){
        navObj.slideToggle();
        $("#pre-input").slideToggle();
        $("#external-preview").slideToggle();
    });

    navObj.css({"display":"none"});
    $("#prev-toolbar>.navbar-right").prepend(navObj);


}

function iframeOnLoad(obj){
    var script=$("<script></script>").attr("id","_ry_temp_js");
    var link=$("<link>").attr("id","_ry_temp_css");

    $(obj).contents().find("body").append(script);
    $(obj).contents().find("head").append(link);
    $(obj).contents().find("#_ry_temp_js").attr('src',"/admin/js/page/temp.js?"+(~new Date()));

    //$(obj).contents().find("#_ry_temp_css").attr({'href':("/admin/css/page/temp.css?"+(~new Date())),'rel':'stylesheet'});
}
var RightPageView = Backbone.View.extend({

    el: $("#right-temp"),
    initialize: function () {

    }


});



var searchSuggest=function(){
    console.log("typeahead");



    $.get('/ibeacon/getbyuid.json').done(function(data){

    });


    var bestPictures = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: '/tsearch/ibeacon/%QUERY.json'
    });

    bestPictures.initialize();

    $('input[name=ibeacon]').typeahead(null, {
        name: 'best-pictures',
        displayKey: 'alias',
        source: bestPictures.ttAdapter(),
        templates: {
            empty: [
                '<div class="empty-message">',
                '没有符合的ibeacon',
                '</div>'
            ].join('\n'),
            header: '<div><span class="league-name" style="padding:1px 50px;width:100px;font-weight:bold">别名</span><span class="league-name" style="padding:5px;font-weight:bold">ID</span><span class="league-name" style="padding:5px;font-weight:bold">Major</span><span class="league-name" style="padding:5px;font-weight:bold">minor</span></div>',
            suggestion: template.compile('<p><strong style="padding:1px 30px;padding-right:40px;width:100px;"><%=alias%></strong><span style="padding-right:40px"><%=id%> </span><span style="padding-right:40px"><%=major%> </span><%=minor%></p>')
        }
    });

    //$('input[name=ibeacon]').typeahead({
    //    hint: true,
    //    highlight: true,
    //    minLength: 1
    //}, {
    //    name: 'ibeacon',
    //    displayKey: 'value',
    //    source: ibeacon.ttAdapter(),
    //    templates: {
    //        empty: [
    //            '<div class="empty-message">',
    //            '没有符合结果',
    //            '</div>'
    //        ].join('\n'),
    //        suggestion: template.compile('<p><strong><%=value%></strong> – <%=major%> – <%=minor%></p>')
    //    },
    //    engine: template
    //});
    //$('input[name=ibeacon]').typeahead(
    //    {
    //        name: 'states',
    //        displayKey: 'value',
    //        source: substringMatcher(states)
    //    });

}
var createIframe = function (attr, css) {

    var localAttr = {name: '', class: 'iframe-doc', frameborder: 'no', allowtransparency: true, scrolling: 'auto', hidefocus: ""};
    var localCss = {width: "100%", height: "100%", top: 0, 'background-color': 'transparent', left: 0};
    var props = {"webkitallowfullscreen": true, "allowfullscreen": true};
    var attr = $.extend(localAttr, attr);
    var css = $.extend(localCss, css);

    return $("<iframe></iframe>").attr(attr).css(css).prop(props);

}
var autoSearch= _.debounce(function () {
    /**
     * search-item 表示呈现一个项目的UI
     * search_txt 表示过滤项目里面的text
     * @type {*|jQuery}
     */
    var filterLis = $(".site-temp").find(".search-item");

    var rex = new RegExp($(this).val(), 'i');
    filterLis.hide();
    filterLis.filter(function () {
        return rex.test($(this).find('.search_txt').text());
    }).show();
},300);
var doGlobalSearch2= _.debounce(autoSearch,300);
var compCfg=function(evt,data){
    var jsonFile="/admin/js/page/json/"+site.get('ui')+"/"+data.comp+'.json';
    var dataCid=data.cid;
    $.get(jsonFile,function(json){
        if($.trim(json)!=''){
            seajs.use(["comp_config.js?v="+~new Date()],function(compConfig){


                var cfgPanel=$(".config-section .panel-body");
                var cfgDom=cfgPanel.find('[data-ccid='+dataCid+']');
                cfgPanel.find("[data-ccid]").hide();
                if(cfgDom.size()==0) {
                    console.log("data-comp",data.comp);
                    var cfgHtml='<form class="form-horizontal" method="post">'+compConfig.processContent(json,config.lang[data.comp],config.lang,data.comp)+'</form>';
                    console.log("config html",cfgHtml);
                    var cfgObj=$(cfgHtml).attr('data-ccid',dataCid);
                    cfgObj.append("<button type='submit' class='btn btn-primary btn-block'>"+config.lang.submit+"</button>");
                    cfgObj.find(".add-block").prev().find(">.panel-heading").append(
                        ' <button type="button" class="btn btn-default pull-right  block-add" style="margin-top:-7px"><span class="glyphicon glyphicon-plus"></span></button> <button type="button" class="btn btn-default pull-right  block-remove" style="margin-top:-7px;margin-right:10px"><span class="glyphicon  glyphicon-trash"></span></button> '

                    );

                    cfgObj.find("[input]")
                    cfgObj.attr('action','/test/sub');
                    cfgPanel.append(cfgObj);


                    cfgPanel.on('click',".block-add",function(evt){
                        $(this).parent().parent().parent().after($(this).parent().parent().parent().clone(true));

                    });
                    cfgPanel.on('click',".block-remove",function(evt){
                        $(this).parent().parent().parent().remove();

                    });
                } else {
                    cfgDom.show();
                }


            });
        }




        //('click',function(evt){
        //
        //    $(this).before($(this).prev().clone(true));
        //    evt.preventDefault();
        //    return false;
        //});

    });
};




var initDropzone=function(){
    return false;
    Dropzone.autoDiscover = false;
    Dropzone.dictDefaultMessage="点击或者拖放文件上传";
    var dropzone=new Dropzone("form.dropzone",{ url: "/media/upload.json" ,dictDefaultMessage:"点击或者拖放文件上传",previewsContainer:'',uploadMultiple:false,maxFiles:1});
    dropzone.options.uploadMultiple=false;
    dropzone.options.maxFiles=1;
    dropzone.options.dictDefaultMessage="点击或者拖放文件上传";
    dropzone.on("complete", function(file){
        var input=$(this.element);
        var type=input.attr("data-prev-type");

        dropzone.removeFile(file);
        console.log("complete",file,this);
    });
    dropzone.on("success",function(e,data){
        var input=$(this.element);
        var type=input.attr("data-prev-type");
        if(type=='bg'){
            $(input.attr('data-prev')).css({"background":"url("+('/upload'+data.data.mediaurl)+") no-repeat"});
        }
        console.log("success","fd",e,data);
    });
    dropzone.on("error", function(e){

        console.log("error",e,this);
    });
    dropzone.on("addedfile", function(e){

        console.log("addedfile",e,this);
    });

}
var initPrevToolbar=function(){

    $('#sourcepreview').toolbar({
        content: '#preview-toolbar',
        position: 'bottom'

    }).on('toolbarItemClick',
        function( event,target) {
            var icon=$(target).find("i");
            $(".tool-items>a").removeClass("item-gray");
            $(target).addClass("item-gray");

            if(icon.hasClass("fa-desktop"))
            {
                if($("#prev-iframe").size()==0)
                {
                    var demoDiv=$("div.demo.ui-sortable");
                    var previewIframe = createIframe({src: "/page/preview",id:"prev-iframe"}, {'width': '100%', 'height': "800px"});
                    $("div.demo.ui-sortable").hide();
                    demoDiv.before(previewIframe);
                    previewIframe.load(function() {
                        $(this).contents().find("body").html(getSource());
                    });

                }



            }
            else if(icon.hasClass("fa-mobile-phone"))
            {
                if(icon.hasClass("fa-rotate-270"))
                {

                }
                else {

                }
                //console.log(evt.target);
            }
            else if(icon.hasClass("fa-tablet"))
            {
                if(icon.hasClass("fa-rotate-270"))
                {

                }
                else {

                }
                //console.log(evt.target);
            }
            else if(icon.hasClass("fa-gear"))
            {

                var arrowCnt=$(document).data('toolbar-arrow');
                $(document).data('toolbar-arrow',arrowCnt?(arrowCnt+1):1);
                arrowCnt=$(document).data('toolbar-arrow');
                if(arrowCnt%2===1){
                    $("div.tool-container>div.arrow").css({left:"30%",right:"30%"});
                }
                else {
                    $("div.tool-container>div.arrow").css({left:"50%",right:"50%"});
                }

                $("a.tool-item[data=hidden]").toggle();



            }
            return false;


        }
    );

}

var externalPrev=function(){
    var win=openWindow();
    $(document).unbind('prev_win_event');

    window.childWindow=openWindow("/admin/page/preview/?external=1");

    $(document).on('prev_win_event', function(e, message){

        $(window.childWindow.document).find("body #preview").append(getSource());
        console.log($(childWindow.document).find("body>#preview").html());

    });

}
var devicePrev=function(width,height){
    var demoDiv=$("div.demo.ui-sortable");
    var previewIframe=$("#prev-iframe");
    if(previewIframe.size()==0)
    {
        previewIframe = createIframe({src: "/page/preview",id:"prev-iframe"}, {'width': width, 'height': height});
        $("div.demo.ui-sortable").hide();
        demoDiv.before(previewIframe);
        previewIframe.load(function() {
            $(this).contents().find("body").html(getSource());
        });
    }
    else {

        previewIframe.animate({width:width,height:height},1000);
    }

}

var localSavePage=function(content){
    var pageId=$("#page-editor").attr("data");
    localStorage.setItem("page"+pageId,content);

}
function handleSaveLayout() {
    var e = $(".demo").html();
    if (e != window.demoHtml) {
        localSavePage(e);
        window.demoHtml = e
    }
}
var showAlert=function(title,content,alertType){
    var alertHtml=template(alertTemp)({alertType:(alertType || "info"),title:title,content:content});
    var alertObj=$(alertHtml);
    $("body").append(alertObj);
    alertObj.slideDown(400,function(){
        var that=this;
        setTimeout(function(){$(that).slideUp();},2000);
    });

}
var saveLayout=function(){
    setuploadingEffect($("#page-save>a"));

    $.post(
        "/admin/page/updatecontent",
        {'content':$('.demo').html()},
        function(data) {
            showAlert("Page"," save successfully");
        }
    );
}

var genQrCache=function(content,siteName){
    var siteName=siteName || config.lang.uname_title;
    $.post(
        "/admin/page/genQrCache?_ev2=1",
        {'content':content},
        function(data) {
            var data= $.parseJSON(data);
            if(data.code==1){
                seajs.use(["jq.qr.js"],function(){
                    // img
                    var qrId="qr_"+site.cid;
                    var div=$("<div></div>").append($("<div></div>").attr("id",qrId)).prepend("<h4>扫描二维码预览</h4>");
                    var modalId="m_"+qrId;

                    openPlainModal($("body"),siteName, div.html(), modalId,function (domObj,id) {
                        if($('#' + qrId).find("table,canvas").size()==0){
                            $('#' + qrId).qrcode({
                                text:  data.data.url
                            });
                        }


                    });




                });

            }

        }
    );
}
function downloadLayout(){
    $.ajax({
        type: "POST",
        url: "/build_v3/downloadLayout",
        data: { 'layout-v3': $('#download-layout').html() },
        success: function(data) { window.location.href = '/build_v3/download'; }
    });
}

function downloadHtmlLayout(){
    $.ajax({
        type: "POST",
        url: "/build_v3/downloadLayout",
        data: { 'layout-v3': $('#download-layout').html() },
        success: function(data) { window.location.href = '/build_v3/downloadHtml'; }
    });
}

function undoLayout() {

    $.ajax({
        type: "POST",
        url: "/build_v3/getPreviousLayout",
        data: { },
        success: function(data) {
            undoOperation(data);
        }
    });
}

function redoLayout() {

    $.ajax({
        type: "POST",
        url: "/build_v3/getPreviousLayout",
        data: { },
        success: function(data) {
            redoOperation(data);
        }
    });
}
function handleJsIds() {
    handleModalIds();
    handleAccordionIds();
    handleCarouselIds();
    handleTabsIds()
}
function handleAccordionIds() {
    var e = $(".demo #myAccordion");
    var t = randomNumber();
    var n = "panel-" + t;
    var r;
    e.attr("id", n);
    e.find(".panel").each(function(e, t) {
        r = "panel-element-" + randomNumber();
        $(t).find(".panel-title").each(function(e, t) {
            $(t).attr("data-parent", "#" + n);
            $(t).attr("href", "#" + r)
        });
        $(t).find(".panel-collapse").each(function(e, t) {
            $(t).attr("id", r)
        })
    })
}
function handleCarouselIds() {
    var e = $(".demo #myCarousel");
    var t = randomNumber();
    var n = "carousel-" + t;
    e.attr("id", n);
    e.find(".carousel-indicators li").each(function(e, t) {
        $(t).attr("data-target", "#" + n)
    });
    e.find(".left").attr("href", "#" + n);
    e.find(".right").attr("href", "#" + n)
}
function handleModalIds() {
    var e = $(".demo #myModalLink");
    var t = randomNumber();
    var n = "modal-container-" + t;
    var r = "modal-" + t;
    e.attr("id", r);
    e.attr("href", "#" + n);
    e.next().attr("id", n)
}
function handleTabsIds() {
    var e = $(".demo #myTabs");
    var t = randomNumber();
    var n = "tabs-" + t;
    e.attr("id", n);
    e.find(".tab-pane").each(function(e, t) {
        var n = $(t).attr("id");
        var r = "panel-" + randomNumber();
        $(t).attr("id", r);
        $(t).parent().parent().find("a[href=#" + n + "]").attr("href", "#" + r)
    })
}
function randomNumber(prefix) {
    var prefix=prefix || '';
    return prefix+randomFromInterval(1, 1e8);
}
function randomFromInterval(e, t) {
    return Math.floor(Math.random() * (t - e + 1) + e)
}
function gridSystemGenerator() {
    $(".lyrow .preview input").bind("keyup", function() {
        var e = 0;
        var t = "";
        var n = false;
        var r = $(this).val().split(" ", 12);
        $.each(r, function(r, i) {
            if (!n) {
                if (parseInt(i) <= 0)
                    n = true;
                e = e + parseInt(i);
                t += '<div class="col-md-' + i + ' column"></div>'
            }
        });
        if (e == 12 && !n) {
            $(this).parent().next().children().html(t);b
            $(this).parent().prev().show()
        } else {
            $(this).parent().prev().hide()
        }
    })
}
function configurationElm(e, t) {
    $(".demo").delegate(".configuration > a", "click", function(e) {
        e.preventDefault();
        var t = $(this).parent().next().next().children();
        $(this).toggleClass("active");
        var cls=$(this).attr("rel");
        if(cls=="pos-bottom"){
            $(this).parent().parent().toggleClass("pos-bottom");
            return;
        }
        t.toggleClass($(this).attr("rel"));
    });
    $(".demo").delegate(".configuration .dropdown-menu a", "click", function(e) {
        e.preventDefault();
        var t = $(this).parent().parent();
        var n = t.parent().parent().next().next().children();
        t.find("li").removeClass("active");
        $(this).parent().addClass("active");
        var r = "";
        t.find("a").each(function() {
            r += $(this).attr("rel") + " "
        });
        t.parent().removeClass("open");
        n.removeClass(r);
        n.addClass($(this).attr("rel"))
    })
}
function removeElm() {
    $(".demo").delegate(".remove", "click", function(e) {
        e.preventDefault();
        $(this).parent().remove();
        if (!$(".demo .lyrow").length > 0) {
            clearDemo();
        }

    })
}
function clearDemo() {
    $(".demo").empty();
    localSavePage("");
}
function removeMenuClasses() {
    $("#ry-menu li button").removeClass("active")
}
function changeStructure(e, t) {
    $("#download-layout ." + e).removeClass(e).addClass(t)
}
function cleanHtml(e) {
    $(e).parent().append($(e).children().html())
}
function downloadLayoutSrc() {
    var e = "";
    $("#download-layout").children().html($(".demo").html());
    var t = $("#download-layout").children();
    t.find(".preview, .configuration, .drag, .remove").remove();
    t.find(".lyrow").addClass("removeClean");
    t.find(".box-element").addClass("removeClean");
    t.find(".lyrow .lyrow .lyrow .lyrow .lyrow .removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".lyrow .lyrow .lyrow .lyrow .removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".lyrow .lyrow .lyrow .removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".lyrow .lyrow .removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".lyrow .removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".removeClean").remove();
    $("#download-layout .column").removeClass("ui-sortable");
    $("#download-layout .row-fluid").removeClass("clearfix").children().removeClass("column");
    if ($("#download-layout .container").length > 0) {
        changeStructure("row-fluid", "row")
    }
    var formatSrc =$.htmlClean($("#download-layout").html(), {format: true,allowedAttributes: [["id"], ["class"], ["data-toggle"], ["data-target"], ["data-parent"], ["role"], ["data-dismiss"], ["aria-labelledby"], ["aria-hidden"], ["style"],["data-slide-to"], ["data-slide"]]});
    $("#download-layout").html(formatSrc);
    $("#downloadModal textarea").empty();
    $("#downloadModal textarea").val(formatSrc);
    return  formatSrc;
}
function getSource() {
    var e = "";
    console.log();
    $("#download-layout").children().html($(".device__content.column").html());
    var t = $("#download-layout").children();
    t.find(".preview, .configuration, .drag, .remove",'.mask').remove();
    t.find(".lyrow").addClass("removeClean");
    t.find(".box-element").addClass("removeClean");
    t.find(".lyrow .lyrow .lyrow .lyrow .lyrow .removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".lyrow .lyrow .lyrow .lyrow .removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".lyrow .lyrow .lyrow .removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".lyrow .lyrow .removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".lyrow .removeClean").each(function() {
        cleanHtml(this)
    });
    //t.find(".removeClean").each(function() {
    //    cleanHtml(this)
    //});

    $("#download-layout .column").removeClass("ui-sortable");
    $("#download-layout .row-fluid").removeClass("clearfix").children().removeClass("column");
    if ($("#download-layout .container").length > 0) {
        changeStructure("row-fluid", "row")
    }
    var formatSrc = $.htmlClean($("#download-layout").html(), {format: true,allowedAttributes: [["id"], ["class"], ["data-toggle"], ["data-target"], ["data-parent"], ["role"], ["data-dismiss"], ["aria-labelledby"], ["aria-hidden"],['onclick'],['onmouseout'],['onmouseover'],["data-slide-to"], ["data-slide"]]});

    return  formatSrc;
}



var substringMatcher = function(strs) {
    return function findMatches(q, cb) {
        var matches, substrRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function(i, str) {
            if (substrRegex.test(str.tokens)) {
                // the typeahead jQuery plugin expects suggestions to a
                // JavaScript object, refer to typeahead docs for more info
                matches.push(str);
            }
        });

        cb(matches);
    };
};



var showSidebar=function(){
    $("body").addClass("edit");
    $("body").removeClass("devpreview sourcepreview");

}
var toogleDragPanel=function(){
    console.log("toggle");
    if($("body").hasClass("edit"))
    {
        hideSidebar();
    }
    else {
        showSidebar();
    }



}
var hideSidebar=function(){
    $("body").removeClass("edit");$("body").addClass("devpreview sourcepreview");
}

var initUploadPanel=function(nodes,callback,acceptExts){

    console.log("initUploadPanel",node);
    var maxFiles=10000;
    var fileSize=1024;
    var acceptExts=acceptExts || "image/*";
    var callback=callback || function(){};
    if(!$.isArray(nodes)){
        nodes=[nodes];
    }

    nodes.map(function(node){
        //var name=$(node).attr("name");
        attachDropzone(node,{acceptedFiles:acceptExts,paramName: "file",maxFilesize:fileSize,maxFiles:maxFiles},false,{
            success:function(e,data){


            if(data.code==1){
                callback(data);
                showOverlay($(".config-section"),'check',config.lang.upload_suc,2);
            } else {
                showOverlay($(".config-section"),'times',data.msg,2);
            }
            console.log("upload suc",e,"msg",data);
        },
        error:function(e,data){
            showOverlay($(".config-section"),'times',config.lang.upload_failed,2);
        }

    });
});


}
