$.subscribe('order_paid',function(e,data){
    var pod=allPods.findWhere({'_id':data['item_id']});
    pod.set('_is_local',1);
    myPods.add(pod);
});


var autoSearch= _.debounce(function () {

    var idName=$("#pods_li>li.active>a").attr("href");


    var filterLis = $(idName).find(".search-item");

    var rex = new RegExp($(this).val(), 'i');
    filterLis.hide();
    filterLis.filter(function () {
        return rex.test($(this).find('.search_item_txt').text());
    }).show();
},300);
myPods.on('add',function(addedModel,curCols){

    var podLi='<li class="search-item"> <div class="app_mask" data="<%=name%>"><span class="fa fa-plus item-add" data="<%=_id%>" title="添加<%=display%>服务" data-toggle="tooltip"></span></div> <%if(icon_url && icon_url.indexOf(" ")!==-1){%> <span class="item-dist <%=icon_url%>"></span> <% } else {%> <img src="<%=icon_url%>"> <% }%> <span class="glyphicon-class search_item_txt"><%=name%></span> </li>';
    var liHtml=template.compile(podLi)(addedModel.toJSON());
    var podModal=$("#pod_modal_"+addedModel.get("_id"));
    if(podModal.size()>0){
        podModal.find(".pod-install").text(config.lang['activated_pod']).prop('disabled',true);
    }
    $("#added_pods>ul").append(liHtml);
    addedModel.save({ids:addedModel.get("_id")},{patch:true,url:myPods.url});


});

var Z=new z();


var doGlobalSearch2= _.debounce(autoSearch,300);

$(".search_keyword").on('keyup',doGlobalSearch2);
var capStr = function (string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

$("#all_pods .pod-item").on('click',function(e){
    var self=this;

    var podId=$(this).attr("id").replace(/m_/,'');
    Z.getHtmlFrag("pod_detail.html",function(html){
        var pod=allPods.findWhere({_id:podId});
        var myPod=myPods.findWhere({_id:podId});
        var data=$.extend(pod.toJSON(),{isBought:myPod,title:capStr($(self).find('.search_item_txt').text())},langPluck(config.lang,['activated_pod','from','desc','capimg','pod','activate','comment']));
        var modal=template.compile(html)(data);
        var modal=$(modal);

        modal.modal({backdrop:true,keyboard:false}).on('shown.bs.modal',function(e){


            $(this).next().remove();

        });


    });
    e.preventDefault();
    return false;

});
$("body").on("click",'.app_mask',function(e){

    console.log("app mask");
    var ztb=capStr("pod")+capStr($(this).attr("data"))+capStr("data")+"M";


    var dataLink=$(this).attr("data-link") || "/admin/table/view?ztb="+ztb+"&menuid=18";


    location.replace(dataLink);

    e.preventDefault();
    return false;
});
$("body").on("click",".pod-install",function(){
    var self=this;
    var podId=$(this).attr('data');
    var pod=allPods.findWhere({'_id':podId});
    if($.isPlainObject(pod.get('price')) || pod.price>le-6){

        Z.getHtmlFrag("shopping_cart.html",function(html){
            var prodData=pod.toJSON();
            var isObjPrice=$.isPlainObject(prodData['price']);
            var dateLang={m:'month',d:'day',y:'year',q:'quarter'};
            var selectHtml='<select name="price">';
            var defSel='';
            var options=[];
            if(isObjPrice){
                var cnt=0;
                for(var x in prodData['price']){

                    var len=x.length;
                    var dateName=dateLang[x[0]];
                    console.log("dateName",dateName);
                    var priceTxt=[prodData['price'][x], (x.substr(1)+config.lang[dateName])].join("/");
                    if(cnt==0){
                        defSel=prodData['price'][x];
                    }
                    options.push("<option value="+x+" data='"+prodData['price'][x]+"'>"+priceTxt+"</option>");
                    cnt++;
                }
                selectHtml=selectHtml+options.join("")+"</select>";
            } else {
                defSel=prodData['price'];
                selectHtml=prodData['price'];
            }
            console.log(selectHtml);
            prodData['price']=selectHtml;

            var data=$.extend(prodData,{defSel:defSel,title:capStr($(self).find('.search_item_txt').text())},langPluck(config.lang,['shopping_cart','total','price','checkout','product','quantity']));
            var htmlObj=$(template.compile(html)(data));
            var computePrice=function(num,price){
                $(".total-price").text((num*price));
            }
            htmlObj.find("input[name=quantity]").on('change',function(){
                console.log("change la");
                computePrice($(this).val(),parseFloat(htmlObj.find(':input[name=price]').find("option:selected").attr("data")));

            });
            htmlObj.find(".btn-checkout").on('click',function(evt){
                htmlObj.modal('hide');
                openWindow("/pay/index?"+ $.param({item_type:'pod',item_id:$(this).attr("data"),item_num:htmlObj.find("input[name=quantity]").val(),_item_price:htmlObj.find(":input[name=price]").val()}));
                evt.preventDefault();
                return false;
            });
            htmlObj.find("select[name=price]").on('change',function(){
                console.log("change la");
                computePrice(htmlObj.find('input[name=quantity]').val(),parseFloat($("option:selected", this).attr('data')));

            });
            htmlObj.modal();

        });
    } else {
        $(this).html('<span class="fa fa-check"></span>'+config.lang.activated_pod).prop('disabled',true);
        myPods.add(pod);

    }


});
var getPrice=function(){

}
$("#added_pods").on("click",".pod-remove",function(){
    var self=this;
    var obj= $(self).parent().parent();
    setUpRmPopover(obj,config.lang.un_pod,{'trigger':'manual','placement':'bottom'},{ok:function(){
        obj.popover('destroy');
        obj.remove();
        var pod=myPods.findWhere({_id:$(self).attr('data')});

        if(pod){pod.destroy();}
    }});
    obj.popover('show');


});