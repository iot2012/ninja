<?php



class MedooSysModel {

    /**
     * @var     mapping table name without prefix
     */
    static $_db;
    static $_orgTbName;
    protected $_primary="id";
    protected $_ownerId;
    protected $_actions;
    protected $dbReadCfg='';
    protected $dbWriteCfg='';
    protected $readAdapter='';
    protected $writeAdapter='';
    protected $logger;
    protected $w;
    protected $r;

    /**
     * $_primary不是auto_increment类型要预设为multi,表示多个关联,one,other
     *
     * @var bool
     */
    protected $_databaseType="mysql";

    protected $_primaryType='auto';
    protected $_indexes;
    protected $esc;
    protected $_pre;
    static    $_tbName ;
    protected $_tbMeta=array();
    protected $regs;
    protected $scheme;
    protected $_modelRights;


    function cfg(){
        if(class_exists('Yaf\Application')){
            $cfg=Yaf\Application::app()->getConfig();
             $defDb=$cfg['database']->defdb;

            $dbArr=$cfg['database']->toArray();
            $read_idx=array_rand($dbArr[$defDb]['r']);
            $write_idx=array_rand($dbArr[$defDb]['w']);

            $this->dbReadCfg=$dbArr[$defDb]['r'][$read_idx];
            $this->dbWriteCfg=$dbArr[$defDb]['w'][$write_idx];

            $this->dbReadCfg['database_type']=empty($this->dbReadCfg['database_type'])?$this->_databaseType:$this->dbReadCfg['database_type'];
            $this->dbReadCfg['database_name']=$this->dbReadCfg['database'];
            $this->dbWriteCfg['database_name']=$this->dbReadCfg['database'];
            $this->dbReadCfg['server']=$this->dbReadCfg['hostname'];
            $this->dbWriteCfg['server']=$this->dbReadCfg['hostname'];
            return array($dbArr,$defDb,$cfg);
        }    
        
       return false;

      
    }
    function __construct($cfg=''){





        if($cfg==''){
            $cfg=$this->cfg();
        }
        if($cfg==''){
            throw Exception("no database config");
        }    
        list($dbArr,$defDb,$orgCfg)=$cfg;


        if($this::$_db!=''){
            $defDb=$this::$_db;
        }
        $this->_pre=$dbArr[$defDb]['pre'];
        $this->scheme=$this->dbReadCfg['database'];
        self::$_orgTbName=(!empty($this::$_tbName)?($this::$_tbName):str_replace("Model",'',get_called_class()));
        self::$_tbName=$this->_pre.self::$_orgTbName;



        if($orgCfg->application->en_elasticsearch && !empty($this->_indexes)){
            try {
                $this->esc=new Esindex($this->scheme,self::$_tbName);
            } catch(Exception $ex){

            }

        }

        if($this->dbReadCfg['port']==$this->dbWriteCfg['port'] && $this->dbReadCfg['host']==$this->dbWriteCfg['host']){

            $this->w=$this->r=new Medoo($this->dbReadCfg);
        } else {
            $this->w=new Medoo($this->dbWriteCfg);
            $this->r=new Medoo($this->dbReadCfg);
        }

    }

    function modelRights(){

        return $this->_modelRights;
    }
    function primary(){

        return  $this->_primary;
    }
    function primaryId(){

        return  $this->_primary;
    }
    function getPrimary(){

        return  $this->_primary;
    }
    function getTbName(){

        return self::$_tbName;
    }
    public function tbName(){

        return self::$_tbName;
    }
    function indexes(){
        return $this->_indexes;
    }
    function actions(){
        return $this->_actions;
    }
    /**
     * @param $key array|string
     * @param $dataSource associate array
     * the a,b,c is from $_POST key
     * string:a,b,c
     * array:array(a,b,c)
     */
    function ownerId(){
        return empty($this->_ownerId)?'uid':$this->_ownerId;
    }
    public function primaryType(){
        return $this->_primaryType;
    }
    static function arrToInStr($arr,$key){

        return $key." in ('".implode("','",$arr)."')";

    }
    public function total($query=''){
        if(is_string($query)){
            $key=$this->_primary;
            if($pos=strpos($key,',')){
                $key=substr($key,0,$pos);
            }
            if(empty($key)){
                $key="*";
            }
            $where='';
            if($query!=''){
                $query="where $query";
            }
            $tbName=$this->tbName();
            $result=$this->r->query("SELECT COUNT({$key}) as cnt FROM {$tbName} USE INDEX (PRIMARY) $query ")->fetchAll();
            return $result[0]['cnt'];
        } else {
           return  $this->r->count($this->tbName(),$query);
        }


    }
    function restGet($data){

        return $this->find($data,50);

    }
    public function checkRequired($requiredArr){
        $meta=$this->getTbMeta();
        $keys=array_keys($this->getTbMeta());
        return Validator::checkModel($keys,$meta,$this->tbName(),'',$requiredArr);
    }
    public function check($keys="",$dataSource='',$meta='',$requiredArr=''){

        if(empty($meta)){
            $meta=$this->getTbMeta();
        }
        if(empty($keys)){
            $keys=array_keys($this->getTbMeta());
        }
        if(!empty($meta)){
            return Validator::checkModel($keys,$meta,$this->tbName(),$dataSource,$requiredArr);
        }
        return true;
    }

    public function fetchByUid($uid){
        return $this->find(array("uid"=>$uid));

    }
    static function orgTbName(){

        return self::$_orgTbName;
    }


    function r(){
        if($this->r=='') {
            $this->r = new Medoo(
                $this->dbReadCfg
            );
        }
        return $this->r;
    }
    function w(){
        if($this->w==''){
            $this->w = new Medoo(
                $this->dbWriteCfg
            );
        }

        return $this->w;
    }


    function findOwn($ownerArr,$start=0,$len=30,$cols=""){
        if(empty($cols)){
            $cols="*";
        } else {
            if(is_array($cols)){
                $cols='`'.implode('`,`',$cols).'`';
            }
        }
        $ownerIdKey=$this->ownerId();
        $str=http_build_query($ownerArr,'',' and ');
        $tbName=$this->tbName();
        $sql="select $cols from {$tbName} where $str order by $ownerIdKey limit $start,$len";
        return $this->execSql($sql);
    }
    function tbMeta()
    {
        return $this->_tbMeta;
    }
    function getTbMeta()
    {
        return $this->_tbMeta;
    }
    function find($whArrOrStr=array(), $limit=100, $cols='*', $order='')
    {
        /**
         * $needCols : return the nessasary columns
         *    ex1:array('aliasName'=>'name','age') == 'select name as aliasName'
         *    ex2:array('name','age')
         *    ex3:name=userName&d
         */

        if(!empty($limit)){
            if(is_array($whArrOrStr)){
                $whArrOrStr['LIMIT']=$limit;
                if($order!=''){
                    $whArrOrStr['ORDER']=$order;
                }
            } else if(is_string($whArrOrStr)){
                $whArrOrStr.=" limit 0,100".($order=='')?'':" order by $order";
            }
        }

        return $this->r->select($this->tbName(),$cols,$whArrOrStr);

    }
    function findOne($whArrOrStr=array(),$needCols="*")
    {
        /**
         *
         */
        return $this->r->get($this->tbName(),$needCols,$whArrOrStr);

    }
    protected function findById($id,$needCols="*")
    {
        return $this->find(array(
            $this->primary()=>$id
        ),1,$needCols);
    }
    protected function findByUser($uid,$needCols="*")
    {
        if(empty($uid)){
            $uid=$_SESSION['user']['uid'];
        }
        return $this->find(array(
            "uid"=>$uid
        ),1,$needCols);
    }
    function findAll($needCols="*")
    {
        return $this->find(array(),99999,$needCols);
    }
    function execSql($sql,$data=array())
    {

        $this->r->pdo->pdo->prepare($sql);
        $this->r->pdo->execute($data);
        return $this->r->pdo->fetchAll();


    }
    function filter($data){
        return $data;
    }

    function restDelete($id){

    }
    function restPut($data){

    }
    function deleteOwn($ids,$ownId){

        $primary=$this->primary();
        if(!empty($this->_indexes)){

            if($this->esc && !empty($this->_indexes)) {
                foreach($ids as $key=>$val){
                    $this->esc->deleteIndex(array($val=>array($primary=>$val)));
                }

            }


        }
        return $this->w->delete($this->tbName(),array(
            'AND'=>array(
                $primary=>$ids,
                "uid"=>$ownId
            )
          )
        );
    }
    function deleteMany($ids){

        if(is_string($ids)){
            $ids=explode(",",$ids);
        }
        $pId=$this->primary();

        $this->w->delete($this->tbName(),array($pId=>$ids));
        if(!empty($this->_indexes)){


            if($this->esc && !empty($this->_indexes)) {
                foreach($ids as $key=>$val){
                    $this->esc->deleteIndex(array($val=>array($pId=>$val)));
                }

            }


        }

    }

    function findBy($idArr)
    {
        $selObj = $this->gw->getSql()->select();


        $selObj->where(array(
            $this->_primary => $idArr
        ));
        $order = sprintf('FIELD('.$this->_primary.', %s)', implode(',', array_fill(0, count($idArr), '?')));
        $selObj->order(array(new Zend\Db\Sql\Expression($order, $idArr)));

        return $this->gw->selectWith($selObj)->toArray();
    }

    function tbMeta2($lang){
        $tbMeta=$this->tbMeta();
        $tbName=$this->tbName();
        foreach($tbMeta as $k=>&$v){
            if(!empty($v['enum'])){
                $v['enum']=array_map(function($item)use($lang,$k){
                    /**
                     * $k对应value
                     */
                    if(is_array($item)){
                        $item2=$item['txt'];
                        $kItem=$k.'_'.$item2;
                        $item['txt']=$lang[$kItem]?$lang[$kItem]:($lang[$item2]?$lang[$item2]:$item2);

                    } else {

                        $kItem=$k.'_'.$item;
                        $item=$lang[$kItem]?$lang[$kItem]:($lang[$item]?$lang[$item]:$item);

                    }
                    return $item;



                },$v['enum']);
            }
            if(empty($v['desc'])){
                $v['desc']=Misc_Utils::getTbLangVal($k,$tbName,$lang);

            } else {
                $v['desc']=$lang[$v['desc']]?$lang[$v['desc']]:$v['desc'];
            }



        }
        return $tbMeta;
    }
    public function add($arr)
    {
        foreach(array('ctime','dateline','mtime') as $key=>$val){
            if(isset($this->_tbMeta[$val]) && !isset($arr[$val])){
                $arr[$val]=time();
            }
        }

        return $this->insert($arr);


    }
    function updateOwn($set,$whArrOrStr=array())
    {

        return $this->w->update($set, $whArrOrStr);

    }



    function execInsert($sql)
    {

      return $this->w->query($sql)->fetchAll();


    }

    function replace($set='',$where=array()){
        /**
         *
         * REPLACE DELAYED INTO `online_users` SET `session_id`='3580cc4e61117c0785372c426eddd11c', `user_id` = 'XXX', `page` = '/', `lastview` = NOW();
         */
        $tb=$this->tbName();
        $query=$set;

        if(is_array($set)){
            $query=Misc_Utils::build_str($set);
           // "replace into $tb set ".;1
        }
        return $this->w->query($query);

    }
    function update($set,$whArrOrStr,$oldArr='')
    {
        $update=$this->w->update($this->tbName(),$set, $whArrOrStr=array());
        if(!empty($this->_indexes)){
            $pId=$this->primary();
            $pIdVal=$whArrOrStr[$pId];

            if(isset($pIdVal)){
                if($oldArr==''){
                    $oldArr=$this->findOne(array($pId=>$pIdVal));

                }
                $oldArr=array_merge($oldArr,$set);
                if($this->esc && !empty($this->_indexes)){
                    $indexes=Misc_Utils::array_pluck($oldArr,$this->_indexes);
                    $array[$pIdVal]=$indexes;
                    $this->esc->updateIndex($array);
                }

            }

        }
        return $update;

    }
//$insertIds  = array();
//for ($x = 0; $x < sizeof($filtername); $x++) {
//$orders = array(
//'poid'              => null,
//'order_id'          => $poid,
//'item_desc'         => $filtername[$x],
//'item_qty'          => $filterquantity[$x],
//'item_price'        => $filterprice[$x],
//'total'             => $filtertotal[$x],
//'cash_on_delivery'  => $val_delivery,
//'is_check'          => $val_check,
//'bank_transfer'     => $val_transfer,
//'transaction_date'  => $dateorder
//);
//$this->db->insert('po_order', $orders);
//$insertIds[$x]  = $this->db->insert_id(); //will return the first insert array

    function delete($whArrOrStr,$oldArr='')
    {
        if(!empty($this->_indexes)){

            $pId=$this->primary();


            if($this->esc && !empty($this->_indexes)) {
                $this->esc->deleteIndex(array($whArrOrStr[$pId]=>array($pId=>$whArrOrStr[$pId])));
            }


        }

        $this->w->delete($this->tbName(),$whArrOrStr);
    }

    /**
     * @param $tb
     * @param $docs Elastica\Document's array or Elastica\Document or array
     * @param array $typeOpts
     * array(
    'number_of_shards' => 4,
    'number_of_replicas' => 1,
    'analysis' => array(
    'analyzer' => array(
    'indexAnalyzer' => array(
    'type' => 'custom',
    'tokenizer' => 'standard',
    'filter' => array('lowercase', 'mySnowball')
    ),
    'searchAnalyzer' => array(
    'type' => 'custom',
    'tokenizer' => 'standard',
    'filter' => array('standard', 'lowercase', 'mySnowball')
    )
    ),
    'filter' => array(
    'mySnowball' => array(
    'type' => 'snowball',
    'language' => 'German'
    )
    )
    )
    )
     */
        function exists($data,$cols){

            $result=$this->findOne($data,$cols);

            return !empty($result);

        }

    function insert($set)
    {

        $retId=$this->w->insert($this->tbName(),$set);
        if(!empty($this->_indexes) && $this->esc){
            $indexes=Misc_Utils::array_pluck($set,$this->_indexes);
            $array=array();
            $indexes[$this->primary()]=$retId;
            $array[$retId]=$indexes;
            $this->esc->createIndex($array);

        }
        return  $retId;

        /**
         *
         * this can not return null when insert multiple value
         * return $this->gw->lastInsertValue;
         */


    }


    function restPost($data){
        $tbMeta=$this->tbMeta();

        return $this->add($data);
    }


}