<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 7/24/15
 * Time: 2:37 PM
 */
class Config {

    static $items=array();

    static function set($key,$val){
        self::$items[$key]=$val;
    }
    static function get($key){

        return self::$items[$key];
    }
    static function getAll(){
        return self::$items;
    }

    static function getAllKeys(){
        return array_keys(self::$items);
    }

    static function getAllVals(){
        return array_values(self::$items);
    }


}
