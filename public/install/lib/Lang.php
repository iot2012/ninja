<?php
/**
 * Class Lang_Lang
 * 获取语言配置
 */

class Lang {

    protected $iniPath;
    protected $defLang='enus';
    function __construct($iniPath){
        $this->iniPath=$iniPath;

    }
    function getLang($lang=''){
        if(empty($lang))
        {
            $lang=$this->getLangType();
        }

        if($this->isExist($lang) )
        {
            return parse_ini_file($this->iniPath."/{$lang}.ini");
        }
        else
        {
            return  parse_ini_file($this->iniPath."/{$this->defLang}.ini");
        }

    }
        function formatMsg($lang,$key){


        return $lang[$key]?$lang[$key]:$key;


    }
    /**
     * [name=>'hello']
     * 翻译后变成name=>'您好',要保留原来的hello值可以使用langKey:['key'=>'item','item2']
     */

 function fmtMsgPluck($lang,$arr,$langKeys,$pluckKey){
        $newArr=[];
        foreach($arr as $key=>&$val){
            foreach($langKeys as $key2=>$item){
                if(is_string($key2)){
                    $val[$key2]=$val[$item];
                }
                $val[$item]=$this->formatMsg($lang,$val[$item]);


            }
            $newArr[$val[$pluckKey]][]=$val;
        }

        return $newArr;
    }
     function t(){
            $args=func_get_args();
            $lang=array_shift($args);
            $result=array();
            foreach($args as $item){
                $result[]=$this->formatMsg($lang,$item);
            }
            $langType=$this->getLangType();
            switch($langType){
                case 'zhcn':
                    $result=implode("",$result);
                    break;
                default:{
                    $result=trim(implode(" ",$result));
                }
            }
            return $result;
    }
    /**
     * @param $lang 传入翻译的语言,array('hello'=>'您好')
     * @param $arr
     * @param $langKeys ["_a"=>"a"] 表示翻译带字段a的值同时添加一个_a保留原值,惯例是给a前面加前缀_
     * @return array  array('key1'=>[],'key2'=>[]) group by key
     */
     function fmtMsgArr($lang,$arr,$langKeys){
        foreach($arr as $key=>&$val){
            foreach($langKeys as $key2=>$item){
                if(is_string($key2)){
                    $val[$key2]=$val[$item];
                }
                $val[$item]=$this->formatMsg($lang,$val[$item]);
                /**
                 * [name=>'hello']
                 * 翻译后变成name=>'您好',要保留原来的hello值可以使用langKey:['key'=>'item','item2']
                 */

            }
        }

        return $arr;
    }
     function isExist($lang)
    {
        /**
         * 语言是否存在于系统中
         * @param $lang 语言名字
         */


        return preg_match("/[a-z]{1,6}/i",$lang) && file_exists($this->iniPath."/$lang.ini");
    }
     function getLangType(){
         $lang='zhcn';


         if(!empty($_GET['lang']) && $this->isExist($_GET['lang']) && preg_match("/[a-z]{1,6}/i",$_GET['lang'])){
             $_SESSION['lang']=$_GET['lang'];
             return $_GET['lang'];
         }
         else if(!empty($_SESSION['lang']) && $this->isExist($_SESSION['lang']))
         {
             return $_SESSION['lang'];
         }
        else {
            if(stripos($_SERVER['HTTP_ACCEPT_LANGUAGE'],"en-Us")===0){
                $lang='enus';
            }
        }
        return $lang;

    }
}


