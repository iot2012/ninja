<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 6/17/15
 * Time: 10:32 AM
 */

class InstIni {
    /**
     * 默认的值分隔符号
     * option abc="@nihao"
     * @var string
     */
    private $defValSign='@';
    /**
     * config cmd api
     * 要调用api时小节的名字
     * @var string
     */
    private $apiSecName='api';
    /**
     * 包解压的目录
     * @var string
     */
    private $lang;
    private $lng;
    /**
     * 配置文件名字
     * @var string
    private  $inis;
    /**
     * 存放到数据库中的记录的默认值
     * @var array
     */

    function setLang($lng){
        $this->lng=$lng;
    }
    function getLang(){

    }
    function __construct($iniFile,$lng){
        $this->lng=$lng;
        $this->lang=$lng->getLang();
        $this->inis=parse_ini_file($iniFile,true);

    }
    function parseIni($inis=''){
        if($inis!=''){
            $this->inis=$inis;
        }
        $result=[];
        foreach($this->inis as $cfgStep=>$cfgs) {
            $res=[];
            foreach($cfgs as $key=>$item){
                $items=explode(".","$key");
                $inputType='text';
                $sign=substr($item,0,2);
                if( $sign=="@@"){
                    $item=substr($item,2);
                    $inputType='hidden';
                } else if($sign=="##") {
                    $item=substr($item,2);
                    $inputType='file';
                }
                if($item=='true'){
                    $item=true;
                }
                $langKey=preg_replace("/([\w-]+)\.([\w-]+\.)+([\w-]+)$/i","\\1.\\3",$key);
                $langMainKey=$this->lng->t($this->lang,"cfg",$items[0]);
                $res[$langMainKey][]=[$this->lng->t($this->lang,$langKey),$key,$item,$inputType];
            }
            $result[$cfgStep]['data']=$res;
            $result[$cfgStep]['_name']=$this->lng->t($this->lang,'cfg',$cfgStep);
        }
        return $result;

    }


}
