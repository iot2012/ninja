<?php

spl_autoload_register(function($class) {
    $len=strlen($class);
    if(substr($class,-5)=='Model'){
        $class=substr($class,0,$len-5);
        @include(MODELS_PATH."/$class.php");
    } else {
        @include(LIB_PATH."/$class.php");
    }
});
