<?php


include("config.php");


include(LIB_PATH."/Common.php");
include(LIB_PATH."/ZLoader.php");
include("Bootstrap.php");




// Add your class dir to include path

$lng=new Lang("data/locale");
$lang=$lng->getLang();

$tpls=[
    'agreement'=>$lng->t($lang,'read_agreement'),
    'chk_required'=>$lng->t($lang,'chk_required')

];



if(is_file(INST_LOCK_PATH)){
    exit($lng->t($lang,'app__installed'));
}

$chkList=file_get_contents("data/check_list.ini");

$curStep=empty($_GET['step'])?'agreement':$_GET['step'];
$errors=array();
$steps=array();

switch($curStep){
    case 'agreement':{

        if(!verCheck(REQUIRE_PHP_VERSION)){
            $errors['php']=$lng->t($lang,'php_ver_err','require',' ',REQUIRE_PHP_VERSION,'cur_ver',' ',phpversion());
        } else {
            $errors['php']=1;
        }
        $chkExts=parse_ini_string($chkList,true);


        $ini=new InstIni(INST_APP_INI,$lng);
        $result=$ini->parseIni();

        $errResult=extCheck($chkExts,$lang,$lng);

        $errors=array_merge($errResult,$errors);

        foreach($errors as $key=>$val){
            if($val!=1){
                $steps[]=$lng->t($lang,$val);
            }
        }
        z_inc('index',array('lng'=>$lng,'logo'=>"/img/logo.png",'ini'=>$result,'errs'=>$errors,'tpls'=>$tpls,'lang'=>$lang,'steps'=>json_encode($steps),'forms'));
        break;
    }
    case 'install':{
        include LIB_PATH."/Medoo.php";

        break;
    }


}

function z_inc($tpl,$D=array(),$dir='views'){
    include("$dir/$tpl.tpl");
}