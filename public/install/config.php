<?php
/**
 * Install path configuration
 */
define(INST_APP_PATH,'../../');
define(INST_CONF_PATH,APP_PATH . 'conf/');
define(REQUIRE_PHP_VERSION,5.5);
define(INST_APP_INI,"data/application.ini");
define('INST_LOCK_PATH',APP_PATH."application/data/install.lock");



/**
 * framework configuration
 */

define(APP_PATH,  '.');
define(CONF_PATH,  APP_PATH.'/conf');
define(LIB_PATH,  'lib');
define(MODELS_PATH,  'models');
define(VIEWS_PATH,  'views');
define(CSS_PATH,"assets/css");
define(JS_PATH,"assets/js");



