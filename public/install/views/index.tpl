<?php z_inc('header',$D);?>

<div id="rootwizard" class=" panel panel-default">
    <div class="navbar">
        <div class="navbar-inner">
                <ul>
                    <?php
                        $cnt=1;

                        foreach($D['ini'] as $key=>$val)
                        {


                            echo<<<EOF
<li><a href="#t_$key" data-toggle="tab">$cnt.{$val['_name']}</a></li>
EOF;
                            $cnt++;
                        }

                    ?>


                </ul>
            </div>
       
    </div>
    <div class="progress">
        <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuemin="0" aria-valuemax="100">
            <span class="sr-only">40% Complete (success)</span>
        </div>
    </div>

    <div class="tab-content">
        <?php

         foreach($D['ini'] as $key=>$val)
        {

        echo<<<EOF
        <div class="tab-pane" id="t_{$key}">
EOF;
            z_inc("$key",$D);
            echo "</div>";
        }


        ?>

        <ul class="pager wizard">
            <li class="previous first" style="display:none;"><a href="#"><?php echo $D['lang']['n_first']?></a></li>
            <li class="previous"><a href="#"><?php echo $D['lang']['n_prev']?></a></li>
            <li class="next last" style="display:none;"><a href="#"><?php echo $D['lang']['n_last']?></a></li>
            <li class="next"><a href="#"><?php echo $D['lang']['n_next']?></a></li>
        </ul>
    </div>
</div>

<div id="lockScreenMask"></div>
<img src="/img/ajax-loading.gif" alt="" style="display:none" class="loading-icon"/>


<?php z_inc('footer',$D);?>

<script src="<?php echo JS_PATH;?>/bswiz.js"></script>
<script src="<?php echo JS_PATH;?>/index.js"></script>
</body></html>