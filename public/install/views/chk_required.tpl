
<ul class="list-group">
<?php

foreach($D['errs'] as $key=>$val){

        if($val==1){
            echo '<li class="list-group-item "><span class="glyphicon glyphicon-ok pull-right text-success"></span>'.($D['lng']->t($D['lang'],$key,'cond_match'));
        } else {
            echo '    <li class="list-group-item list-group-item-danger"><span class="glyphicon glyphicon-remove  pull-right text-danger"></span>'.($D['lng']->t($D['lang'],$key,$val));
        }
        echo '</li>';


    }
    ?>
</ul>