<script src="<?php echo JS_PATH;?>/jqm.js"></script>
<script src="<?php echo JS_PATH;?>/bootstrap.js"></script>
<script src="<?php echo JS_PATH;?>/seajs.js"></script>
<script src="<?php echo JS_PATH;?>/art.js"></script>

<script>

    seajs.config({
        base: "/js/",
        paths: {
            echarts : 'echarts/src',
            zrender : 'zrender/src',
            vendor:'vendor',
            admin:'/admin/js'
        },
        alias : {
            amazeui: 'vendor/js/amazeui',
            echarts : 'echarts/echarts',
            zrender : 'zrender/zrender'
        }
    });


</script>



