


//var wxDev=openWXDeviceLib();
//
//alert(JSON.stringify(wxDev));

var centerObj=function(container,div){


    var oW=container.outerWidth(true);
    var oH=container.outerHeight(true);


    div.css({position:"absolute","z-index":999});
    container.before(div);
    container.prop("disabled",true);
    div.show();


    var containerOffset=container.offset();
    div.offset({left:(containerOffset.left+(oW-div.outerWidth())/2),top:(containerOffset.top+(oH-div.outerHeight())/2)});


}

$(function(){

    $(".expand-desc").on('click',function(e){
        if($(this).find("span").hasClass("glyphicon-chevron-right")){
            $(this).find("span").removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down");
        } else {
            $(this).find("span").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-right");
        }
        $(".desc-item").toggle();
        e.preventDefault();
        return false;
    });

    var airUrl="http://api.gogoinfo.cn/air/envref";



    $("#wifi-tab").on('click',function(evt){
        location.href="/air/wifi";
        evt.preventDefault();
        return false;
    });



    $(".z-nav-bottom>ul>li>a").on('click',function(evt){
        console.log("windows",$(this).attr('href'));
        if($(this).attr('href')!="#sensor-sec"){
            $('#arrow-down').hide();
        } else {
            $('#arrow-down').show();
        }

    });
    $("body").on('click',".product-select>li>a",function(evt){
        $(this).parent().parent().find("li>a").not(this).removeClass('active');
        $(this).addClass('active');
        evt.preventDefault();
        return false;
    });


    var attachBdMap=function(lbsGeoDom,jqGps,jqAddress){



        lbsGeoDom.addEventListener("geofail",function(evt){
            console.log("fail");
        });
//监听定位成功事件 geosuccess
        lbsGeoDom.addEventListener("geosuccess",function(evt){
            console.log(evt.detail);
            var address = evt.detail.address;
            var coords = evt.detail.coords;
            var x = coords.lng;
            var y = coords.lat;

            jqGps.size()>0 && jqGps.val([x,y].join(","));
            jqAddress.size()>0 &&  jqAddress.val(address);
            console.log("地址："+address);
            console.log("地理坐标："+x+','+y);
        });
    }

    var startPay=function(color,number,model,pid){

        if(isRequiredWechat()) {


            location.href = '/wechat/buy';

            evt.preventDefault();
            evt.stopPropagation();
            return false;

        }
        return false;

    }
    var showTips=function(jqObj,text){
        jqObj.tooltip({
            'title':text
        });
       jqObj.tooltip('show');

    }
    var fillAddress=function(evt){
        $(this).prop('disabled',true);
        var number=$("input[name=number]").val();
        var model= $.trim($(".model-sel>a.active").text());
        var color=$.trim($(".color-sel>a.active").text());
        var pid=$(this).attr("data");
        $("input[name=number]").parent().removeClass("input-error");
        $(".model-data,.color-data").removeClass("input-error");

        if(number==''){
            $("input[name=number]").parent().addClass("input-error");

            showTips( $("input[name=number]").parent(),'数量格式错误');
            return false;
        }

        if(color==''){
            $(".color-data").addClass("input-error");
            showTips( $(".color-data"),'请选择颜色');
            return false;
        }
        $(".buy-widget").hide();
        $(".z-spinner").show();
        seajs.use(["http://api.map.baidu.com/components?ak=BD0wOR97fUQM0pAvKG9LkU6z&v=1.0"],function(){
            var gps=$("input[name=bd_gps]");
            var address=$("input[name=address]");

            $("#address_add_win").show();
            //var number=$.trim($(this).find("input[name=number]").val());
            //var model= $.trim($(this).find(".model-sel>a.active").text());
            //var color=$.trim($(this).find(".color-sel>a.active").text());
            if($("#address-form").find("input[name=number]").size()==0){

                $("#address-form").append($("<input name='pid' type='hidden'>").val(pid));
                $("#address-form").append($("<input name='number' type='hidden'>").val(number));
                $("#address-form").append($("<input name='model' type='hidden'>").val(model));
                $("#address-form").append($("<input name='color' type='hidden'>").val(color));
            } else {

            }

            $(".z-spinner").hide();
            attachBdMap($("#_bd_gps")[0],gps,address);

        });
        evt.preventDefault();

        return false;

    }
    function checkInputs(inputRegs,inputs){

        var errInputs=[];
        var emptyInputs=[];
        for(var name in inputRegs){
            if(inputRegs[name]['r']==1){
                if(inputs[name]=='' || !inputs[name]){
                    emptyInputs.push(name);
                    continue;
                }

            }
            if(!inputRegs[name]['reg'].test(inputs[name])){
                errInputs.push(name);
                continue;
            }

        }
        if(errInputs.length!=0 || emptyInputs.length!=0){
            return [false,errInputs,emptyInputs];
        }
        return [true];
    }
    var err={
        'realname':'名字格式错误',
        'address':'地址格式错误',
        'mobile':'手机格式错误'
    };
    $("body").on('submit',".ajax-form",function(evt){
        $(this).find('[name=submit]').prop('disabled',true);
        var data= $.unparam($(this).serialize());
        var inputRegs={'realname':{reg:/^[-\w\u4E00-\u9FFF]{1,25}$/,'r':1},'address':{reg:/^.{10,}$/,'r':1},'mobile':{reg:/^1[6|9|3|5|8|7]\d{9}$/,'r':1}};
        var ret=checkInputs(inputRegs,data);
        if(ret[0]===false){

            var self=this;
            ret[1].concat(ret[2]).forEach(function(name){
                   $(self).find(":input").parent().removeClass("input-error");
                   showTips($(self).find("[name="+name+"]").parent(),err[name]);
                   $(self).find("[name="+name+"]").parent().addClass("input-error");
            });
            return false;
        }

        return true;
    });
    $("#buy-btn").on('touch click',fillAddress);
    function isRequiredWechat(){
        var result=navigator.userAgent.match(/MicroMessenger\/(\d\.\d+)/);
        if(result){
            var version=parseFloat(result[1]);
            return version>=5;
        }
        return false;

    }

    function invokeWxPay(json){
        console.log("invoke Json api");
        WeixinJSBridge.invoke(
            'getBrandWCPayRequest', json,
            function(res){
                console.log(res);
                if(res.err_msg == "get_brand_wcpay_request:ok" ) {


                }     // 使用以上方式判断前端返回,微信团队郑重提示：res.err_msg将在用户支付成功后返回    ok，但并不保证它绝对可靠。
            }
        );
    }


});

