module.exports = {
    entry: "./src/main.js",
    output: {
        path: "./dist",
        publicPath: "dist/",
        filename: "index.js"
    },
    module: {
        loaders: [
            { test: /\.styl$/, loader: "style!css!stylus" },
            { test: /\.html$/, loader: "html" }
        ]
    }
}