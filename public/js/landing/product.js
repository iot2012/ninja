


seajs.use(['jquery.easings.min','jquery.fullpage'],function(){
    $(function() {
        function i(i) {
            c = i;
            var n = r[i].name;
            for (var t in l) l.hasOwnProperty(t) && $(t).attr("data-status", n + "_start")
        }

        function n(i) {
            var n = r[i];
            o(r[i].selecter_leave);
            for (var t in n.selecter) n.selecter.hasOwnProperty(t) && $(t).attr("data-status", "")
        }

        function t(i) {
            var n = r[i];
            o(r[i].selecter_start);
            for (var t in n.selecter) n.selecter.hasOwnProperty(t) && $(t).attr("data-status", n.name + "_start")
        }

        function o(i) {
            for (var n in i)
                if (i.hasOwnProperty(n)) {
                    var t = {};
                    for (var o in i[n]) i[n].hasOwnProperty(o) && (t[a[o]] = i[n][o]);
                    $(n).css(t)
                }
        }
        var s = {
                time: 800
            },
            a = {},
            e = document.getElementsByTagName("body")[0];
        "animation" in e.style ? a = {
            animation: "animation",
            animationDelay: "animationDelay",
            animationDirection: "animationDirection",
            animationDuration: "animationDuration",
            animationFillMode: "animationFillMode",
            animationIterationCount: "animationIterationCount",
            animationName: "animationName",
            animationPlayState: "animationPlayState",
            animationTimingFunction: "animationTimingFunction",
            transition: "transition",
            transitionDelay: "transitionDelay",
            transitionDuration: "transitionDuration",
            transitionProperty: "transitionProperty",
            transitionTimingFunction: "transitionTimingFunction",
            transform: "transform",
            transformOrigin: "transformOrigin",
            transformStyle: "transformStyle",
            perspective: "perspective",
            perspectiveOrigin: "perspectiveOrigin"
        } : "WebkitAnimation" in e.style ? a = {
            animation: "WebkitAnimation",
            animationDelay: "WebkitAnimationDelay",
            animationDirection: "WebkitAnimationDirection",
            animationDuration: "WebkitAnimationDuration",
            animationFillMode: "WebkitAnimationFillMode",
            animationIterationCount: "WebkitAnimationIterationCount",
            animationName: "WebkitAnimationName",
            animationPlayState: "WebkitAnimationPlayState",
            animationTimingFunction: "WebkitAnimationTimingFunction",
            transition: "WebkitTransition",
            transitionDelay: "WebkitTransitionDelay",
            transitionDuration: "WebkitTransitionDuration",
            transitionProperty: "WebkitTransitionProperty",
            transitionTimingFunction: "WebkitTransitionTimingFunction",
            transform: "WebkitTransform",
            transformOrigin: "WebkitTransformOrigin",
            transformStyle: "WebkitTransformStyle"
        } : location.href = -1 !== location.href.indexOf("?") ? location.href + "&type=normal" : location.href + "?type=normal";
        var r = [{
                name: "status1",
                selecter: {
                    ".section1 .ball_base": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-in-out"
                    },
                    ".section1 .ball_light": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-in-out"
                    },
                    ".section1 .num_help": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section1 .num_shadow": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section1 .five": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section1 .five_num": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    }
                },
                selecter_leave: {
                    ".section1 .ball_base": {
                        transitionDelay: "0ms",
                        transitionDuration: "200ms"
                    },
                    ".section1 .ball_light": {
                        transitionDelay: "500ms",
                        transitionDuration: "100ms"
                    },
                    ".section1 .num_help": {
                        transitionDelay: "0ms",
                        transitionDuration: "500ms"
                    },
                    ".section1 .num_shadow": {
                        transitionDelay: "100ms",
                        transitionDuration: "100ms"
                    },
                    ".section1 .five_1": {
                        transitionDelay: "100ms",
                        transitionDuration: "100ms"
                    },
                    ".section1 .five_2": {
                        transitionDelay: "100ms",
                        transitionDuration: "100ms"
                    },
                    ".section1 .five_3": {
                        transitionDelay: "100ms",
                        transitionDuration: "100ms"
                    },
                    ".section1 .five_num": {
                        transitionDelay: "100ms",
                        transitionDuration: "100ms"
                    }
                },
                selecter_start: {
                    ".section1 .ball_base": {
                        transitionDelay: "1600ms",
                        transitionDuration: "1500ms"
                    },
                    ".section1 .ball_light": {
                        transitionDelay: "1600ms",
                        transitionDuration: "1500ms"
                    },
                    ".section1 .num_help": {
                        transitionDelay: "300ms",
                        transitionDuration: "500ms"
                    },
                    ".section1 .num_shadow": {
                        transitionDelay: "1500ms",
                        transitionDuration: "500ms"
                    },
                    ".section1 .five_1": {
                        transitionDelay: "800ms",
                        transitionDuration: "800ms"
                    },
                    ".section1 .five_2": {
                        transitionDelay: "800ms",
                        transitionDuration: "800ms"
                    },
                    ".section1 .five_3": {
                        transitionDelay: "1100ms",
                        transitionDuration: "600ms"
                    },
                    ".section1 .five_num": {
                        transitionDelay: "1400ms",
                        transitionDuration: "1500ms"
                    }
                }
            }, {
                name: "status2",
                selecter: {
                    ".section2 .bigbox": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section2 .bigbox_right": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section2 .bigbox_bottom": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section2 .bigbox_deco": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section2 .smallbox": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section2 .scissors.vi": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-in"
                    },
                    ".section2 .scissors.ho": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-in"
                    },
                    ".section2 .scissors.vi_d": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section2 .scissors.ho_d": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section2 .line_vi": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-in"
                    },
                    ".section2 .line_ho": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-in"
                    }
                },
                selecter_leave: {
                    ".section2 .bigbox": {
                        transitionDelay: "0ms",
                        transitionDuration: "200ms"
                    },
                    ".section2 .bigbox_right": {
                        transitionDelay: "0ms",
                        transitionDuration: "200ms"
                    },
                    ".section2 .bigbox_bottom": {
                        transitionDelay: "0ms",
                        transitionDuration: "200ms"
                    },
                    ".section2 .bigbox_deco": {
                        transitionDelay: "0ms",
                        transitionDuration: "200ms"
                    },
                    ".section2 .smallbox": {
                        transitionDelay: "0ms",
                        transitionDuration: "200ms"
                    },
                    ".section2 .scissors.vi": {
                        transitionDelay: "0ms",
                        transitionDuration: "200ms"
                    },
                    ".section2 .scissors.ho": {
                        transitionDelay: "0ms",
                        transitionDuration: "200ms"
                    },
                    ".section2 .scissors.vi_d": {
                        transitionDelay: "0ms",
                        transitionDuration: "200ms"
                    },
                    ".section2 .scissors.ho_d": {
                        transitionDelay: "0ms",
                        transitionDuration: "200ms"
                    },
                    ".section2 .line_vi": {
                        transitionDelay: "0ms",
                        transitionDuration: "200ms"
                    },
                    ".section2 .line_ho": {
                        transitionDelay: "0ms",
                        transitionDuration: "200ms"
                    }
                },
                selecter_start: {
                    ".section2 .bigbox": {
                        transitionDelay: "200ms",
                        transitionDuration: "600ms"
                    },
                    ".section2 .bigbox_right": {
                        transitionDelay: "1700ms",
                        transitionDuration: "600ms"
                    },
                    ".section2 .bigbox_bottom": {
                        transitionDelay: "1700ms",
                        transitionDuration: "600ms"
                    },
                    ".section2 .bigbox_deco": {
                        transitionDelay: "1700ms",
                        transitionDuration: "600ms"
                    },
                    ".section2 .smallbox": {
                        transitionDelay: "2000ms",
                        transitionDuration: "600ms"
                    },
                    ".section2 .scissors.vi": {
                        transitionDelay: "800ms",
                        transitionDuration: "800ms"
                    },
                    ".section2 .scissors.ho": {
                        transitionDelay: "1200ms",
                        transitionDuration: "600ms"
                    },
                    ".section2 .scissors.vi_d": {
                        transitionDelay: "1600ms",
                        transitionDuration: "600ms"
                    },
                    ".section2 .scissors.ho_d": {
                        transitionDelay: "1800ms",
                        transitionDuration: "600ms"
                    },
                    ".section2 .line_vi": {
                        transitionDelay: "800ms",
                        transitionDuration: "800ms"
                    },
                    ".section2 .line_ho": {
                        transitionDelay: "1200ms",
                        transitionDuration: "600ms"
                    }
                }
            }, {
                name: "status3",
                selecter: {
                    ".section3 .rocket": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section3 .back": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section3 .dust_pic": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section3 .line": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section3 .light": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    }
                },
                selecter_leave: {
                    ".section3 .rocket": {
                        transitionDelay: "1000ms",
                        transitionDuration: "500ms"
                    },
                    ".section3 .back": {
                        transitionDelay: "0ms",
                        transitionDuration: "300ms"
                    },
                    ".section3 .dust_pic": {
                        transitionDelay: "0ms",
                        transitionDuration: "300ms"
                    },
                    ".section3 .line": {
                        transitionDelay: "0ms",
                        transitionDuration: "300ms"
                    },
                    ".section3 .light": {
                        transitionDelay: "0ms",
                        transitionDuration: "300ms"
                    }
                },
                selecter_start: {
                    ".section3 .rocket": {
                        transitionDelay: "200ms",
                        transitionDuration: "800ms"
                    },
                    ".section3 .back": {
                        transitionDelay: "1000ms",
                        transitionDuration: "500ms"
                    },
                    ".section3 .dust_pic": {
                        transitionDelay: "1000ms",
                        transitionDuration: "500ms"
                    },
                    ".section3 .line": {
                        transitionDelay: "1000ms",
                        transitionDuration: "500ms"
                    },
                    ".section3 .light": {
                        transitionDelay: "1000ms",
                        transitionDuration: "500ms"
                    }
                }
            }, {
                name: "status4",
                selecter: {
                    ".section4 .pc": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section4 .mobile": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section4 .android": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section4 .icon1": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section4 .icon2": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section4 .icon3": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section4 .icon4": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section4 .border": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    }
                },
                selecter_leave: {
                    ".section4 .pc": {
                        transitionDelay: "600ms",
                        transitionDuration: "500ms"
                    },
                    ".section4 .mobile": {
                        transitionDelay: "100ms",
                        transitionDuration: "500ms"
                    },
                    ".section4 .border": {
                        transitionDelay: "0ms",
                        transitionDuration: "500ms"
                    }
                },
                selecter_start: {
                    ".section4 .pc": {
                        transitionDelay: "200ms",
                        transitionDuration: "800ms"
                    },
                    ".section4 .mobile": {
                        transitionDelay: "1000ms",
                        transitionDuration: "600ms"
                    },
                    ".section4 .border": {
                        transitionDelay: "2800ms",
                        transitionDuration: "500ms"
                    }
                }
            }, {
                name: "status5",
                selecter: {
                    ".section5 .pc": {
                        transitionProperty: "all",
                        transitionTimingFunction: "linear"
                    },
                    ".section5 .mobile": {
                        transitionProperty: "all",
                        transitionTimingFunction: "linear"
                    },
                    ".section5 .wifi": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section5 .wifi_continues": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section5 .wifi_uncontinues": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section5 .icon": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    }
                },
                selecter_leave: {
                    ".section5 .pc": {
                        transitionDelay: "300ms",
                        transitionDuration: "200ms"
                    },
                    ".section5 .mobile": {
                        transitionDelay: "200ms",
                        transitionDuration: "200ms"
                    },
                    ".section5 .wifi": {
                        transitionDelay: "100ms",
                        transitionDuration: "200ms"
                    },
                    ".section5 .wifi_continues": {
                        transitionDelay: "0ms",
                        transitionDuration: "0ms"
                    },
                    ".section5 .wifi_uncontinues": {
                        transitionDelay: "0ms",
                        transitionDuration: "0ms"
                    },
                    ".section5 .icon": {
                        transitionDelay: "0ms",
                        transitionDuration: "200ms"
                    }
                },
                selecter_start: {
                    ".section5 .pc": {
                        transitionDelay: "0ms",
                        transitionDuration: "500ms"
                    },
                    ".section5 .mobile": {
                        transitionDelay: "300ms",
                        transitionDuration: "500ms"
                    },
                    ".section5 .wifi": {
                        transitionDelay: "700ms",
                        transitionDuration: "500ms"
                    },
                    ".section5 .wifi_continues": {
                        transitionDelay: "2100ms",
                        transitionDuration: "400ms"
                    },
                    ".section5 .wifi_uncontinues": {
                        transitionDelay: "2100ms",
                        transitionDuration: "400ms"
                    },
                    ".section5 .icon1": {
                        transitionDelay: "1200ms",
                        transitionDuration: "500ms"
                    },
                    ".section5 .icon2": {
                        transitionDelay: "1600ms",
                        transitionDuration: "500ms"
                    },
                    ".section5 .icon3": {
                        transitionDelay: "1400ms",
                        transitionDuration: "500ms"
                    },
                    ".section5 .icon4": {
                        transitionDelay: "1800ms",
                        transitionDuration: "500ms"
                    }
                }
            }, {
                name: "status6",
                selecter: {
                    ".section6 .box": {
                        transitionProperty: "all",
                        transitionTimingFunction: "linear"
                    },
                    ".section6 .object": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section6 .base_inner": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section6 .light_inner div": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section6 .base_inner_frame": {}
                },
                selecter_leave: {
                    ".section6 .box": {
                        transitionDelay: "500ms",
                        transitionDuration: "200ms"
                    },
                    ".section6 .object": {
                        transitionDelay: "0ms",
                        transitionDuration: "500ms"
                    },
                    ".section6 .base_inner": {
                        transitionDelay: "0ms",
                        transitionDuration: "500ms"
                    },
                    ".section6 .light_inner div": {
                        transitionDelay: "0ms",
                        transitionDuration: "200ms"
                    }
                },
                selecter_start: {
                    ".section6 .box": {
                        transitionDelay: "0ms",
                        transitionDuration: "200ms"
                    },
                    ".section6 .object": {
                        transitionDelay: "500ms",
                        transitionDuration: "500ms"
                    },
                    ".section6 .base_inner": {
                        transitionDelay: "500ms",
                        transitionDuration: "500ms"
                    },
                    ".section6 .light_inner div": {
                        transitionDelay: "500ms",
                        transitionDuration: "200ms"
                    }
                }
            }, {
                name: "status7",
                selecter: {
                    ".section7 .icon1": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section7 .icon2": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section7 .icon3": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section7 .icon4": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section7 .icon5": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section7 .btn_down": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section8 .btn_down": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    }
                },
                selecter_leave: {
                    ".section7 .icon1": {
                        transitionDelay: "350ms",
                        transitionDuration: "200ms"
                    },
                    ".section7 .icon2": {
                        transitionDelay: "150ms",
                        transitionDuration: "200ms"
                    },
                    ".section7 .icon3": {
                        transitionDelay: "250ms",
                        transitionDuration: "200ms"
                    },
                    ".section7 .icon4": {
                        transitionDelay: "200ms",
                        transitionDuration: "200ms"
                    },
                    ".section7 .icon5": {
                        transitionDelay: "100ms",
                        transitionDuration: "200ms"
                    },
                    ".section7 .btn_down": {
                        transitionDelay: "100ms",
                        transitionDuration: "300ms"
                    }
                },
                selecter_start: {
                    ".section7 .icon1": {
                        transitionDelay: "300ms",
                        transitionDuration: "500ms"
                    },
                    ".section7 .icon2": {
                        transitionDelay: "900ms",
                        transitionDuration: "500ms"
                    },
                    ".section7 .icon3": {
                        transitionDelay: "600ms",
                        transitionDuration: "500ms"
                    },
                    ".section7 .icon4": {
                        transitionDelay: "750ms",
                        transitionDuration: "500ms"
                    },
                    ".section7 .icon5": {
                        transitionDelay: "1050ms",
                        transitionDuration: "500ms"
                    },
                    ".section7 .btn_down": {
                        transitionDelay: "800ms",
                        transitionDuration: "500ms"
                    }
                }
            },{
                name: "status8",
                selecter: {
                    ".section8 .icon1": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section8 .icon2": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section7 .icon3": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section7 .icon4": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section7 .icon5": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section7 .btn_down": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    },
                    ".section8 .btn_down": {
                        transitionProperty: "all",
                        transitionTimingFunction: "ease-out"
                    }
                },
                selecter_leave: {
                    ".section7 .icon1": {
                        transitionDelay: "350ms",
                        transitionDuration: "200ms"
                    },
                    ".section7 .icon2": {
                        transitionDelay: "150ms",
                        transitionDuration: "200ms"
                    },
                    ".section7 .icon3": {
                        transitionDelay: "250ms",
                        transitionDuration: "200ms"
                    },
                    ".section7 .icon4": {
                        transitionDelay: "200ms",
                        transitionDuration: "200ms"
                    },
                    ".section7 .icon5": {
                        transitionDelay: "100ms",
                        transitionDuration: "200ms"
                    },
                    ".section7 .btn_down": {
                        transitionDelay: "100ms",
                        transitionDuration: "300ms"
                    }
                },
                selecter_start: {
                    ".section7 .icon1": {
                        transitionDelay: "300ms",
                        transitionDuration: "500ms"
                    },
                    ".section7 .icon2": {
                        transitionDelay: "900ms",
                        transitionDuration: "500ms"
                    },
                    ".section7 .icon3": {
                        transitionDelay: "600ms",
                        transitionDuration: "500ms"
                    },
                    ".section7 .icon4": {
                        transitionDelay: "750ms",
                        transitionDuration: "500ms"
                    },
                    ".section7 .icon5": {
                        transitionDelay: "1050ms",
                        transitionDuration: "500ms"
                    },
                    ".section7 .btn_down": {
                        transitionDelay: "800ms",
                        transitionDuration: "500ms"
                    }
                }
            }],
            l = {
                html: {
                    transitionProperty: "all",
                    transitionDuration: s.time + "ms",
                    transitionTimingFunction: "linear"
                }
            },
            c = 0,
            m = $(window).width(),
            u = $(window).height(),
            D = {
                onLeave: function(t, o) {
                    i(o - 1), n(t - 1)
                },
                afterLoad: function(i, n) {
                    t(n - 1), $(".menu li").removeClass("active").eq(n - 1).addClass("active"), 1 === n ? $(".head .btn").fadeOut(100, function() {
                        $(".head .head_link").fadeIn(300)
                    }) : 7 === n ? ($(".head .btn").fadeOut(100), $(".head .head_link").fadeOut(100)) : $(".head .head_link").fadeOut(100, function() {
                        $(".head .btn").fadeIn(300)
                    })
                },
                afterRender: function() {},
                afterResize: function() {
                    var i = $(window).height(),
                        n = $(window).width();
                    if (i === u && n !== m) return void(m = n);
                    if (m = n, u = i, i > 650) $(".main_container").css("zoom", 1), $(".text_container").css("zoom", 1), $(".btn_down").css("zoom", 1);
                    else {
                        var t = (i - 100) / 600;
                        $(".main_container").css("zoom", t), $(".text_container").css("zoom", t), $(".btn_down").css("zoom", t), $.fn.fullpage.moveTo && $.fn.fullpage.moveTo(c + 1)
                    }
                },
                afterSlideLoad: function() {},
                onSlideLeave: function() {}
            };
        D.afterResize(), o(l), o(r[0].selecter), o(r[1].selecter), o(r[2].selecter), o(r[3].selecter), o(r[4].selecter), o(r[5].selecter), o(r[6].selecter), i(0), setTimeout(function() {
            t(0)
        }, 200), $("#view").fullpage({
            verticalCentered: !0,
            resize: !0,
            scrollingSpeed: s.time,
            easing: "easeInQuart",
            navigation: !1,
            navigationPosition: "right",
            slidesNavigation: !1,
            slidesNavPosition: "bottom",
            loopBottom: !0,
            loopTop: !1,
            loopHorizontal: !0,
            autoScrolling: !0,
            scrollOverflow: !1,
            css3: !1,
            paddingTop: "100px",
            paddingBottom: "100px",
            normalScrollElementTouchThreshold: 5,
            keyboardScrolling: !0,
            touchSensitivity: 15,
            continuousVertical: !1,
            animateAnchor: !1,
            sectionSelector: ".section",
            slideSelector: ".slide",
            onLeave: D.onLeave,
            afterLoad: D.afterLoad,
            afterRender: D.afterRender,
            afterResize: D.afterResize,
            afterSlideLoad: D.afterSlideLoad,
            onSlideLeave: D.onSlideLeave
        }), $(".menu ul li").bind("click", function() {
            var i = $(this);
            i.hasClass("active") || $.fn.fullpage.moveTo(parseInt(i.data("sectionid"), 10))
        }), $(".section1 .arrow").bind("click", function() {
            $.fn.fullpage.moveTo(2)
        }), $(".section7 .arrow").bind("click", function() {
            $.fn.fullpage.moveTo(1)
        })
    });

});