
$(".dock-contact .dock-icon>li>a").on('click',function(evt){
    var href=$(this).attr("href");
    var data=$(this).attr('data');
    if(href=='#qq'){
        openWindow("http://wpa.qq.com/msgrd?v=3&uin="+data+"&site=qq&menu=yes");
    } else if(href=="#scroll-up"){
        window.scroll(0,0);
    }else if(href=="#collaspe-in"){
        $(".dock-contact .dock-icon>li.active").removeClass('active');
        $(".dock-contact .tab-content .tab-pane.active").removeClass('active');
        return false;
    }

});
//http://wpa.qq.com/msgrd?v=3&uin={{$company.qq}}&site=qq&menu=yes"
var wechatLogin=function(qrData){
    console.log("wechat login",qrData);

    if(/\d+/.test(qrData.uid))
    {
        //var now = new Date();
        //var time = now.getTime();
        ///**
        // * for chrome,you must set small expireTime because of chrome does not delete cookies
        // * @type {number}
        // */
        //var expireTime = time + 1000*1;
        //now.setTime(expireTime);
        //document.cookie = 'uid='+data.uid+';expires='+now.toGMTString()+';path=/';
        //document.cookie = 'token='+data.token+';expires='+now.toGMTString()+';path=/';
        //
        //
        //location.href="/admin/index/index";
        if(!$(document).data("can_post")){
            $(document).data("can_post",true);
            $.post("/user/login",qrData).done(function(data){
                console.log("login data",data);
                var data=$.parseJSON(data);
                if(data.code==1){
                  location.href="/admin/index/index";

                }

            }).always(function(){
                $(document).data("can_post",true);
            }).fail(function(){
                $(document).data("can_post",false);
            });
        }

        //$("#qrlogin-form").submit();

    }
    else {
        showAlert(config.lang.unlinked,config.lang.link_help,"danger",8000);
        $("#qr-sign-in").popover('hide');
    }
}

var setQrUrl=function(url) {

    $.get(url,function (data) {
        if (/^http[s]?:\/\/.+/.test(data)) {
            $("#pb_qrcode").attr({'src': data}).load(function () {
                $(this).css({width: '200px', height: 'auto'});
            });
        }
    });
}
var setWizStep=function(step){

    $("#signup-wizard .stepwizard-step>button").prop('disabled',true);
    $("#signup-wizard .stepwizard-step>button[data='#step-"+step+"']").prop('disabled',false);
}
var meetingLogin=function(meeting){
    var $z=z();
    var self = this;
    var signUp = $("#meeting_login");
    if(meeting.user.level=='110'){
        if (signUp.size() == 0) {

            $z.getHtmlFrag("/js/landing/temp/meeting_login.html", function (data) {

                var html = (template.compile(data)($.extend(meeting, {lang: config.lang})));

                signUp = $(html);

                signUp.modal({backdrop: true});

            });
        }
        else {
            signUp.modal('toggle');
        }
    } else {

        $("#sign-in").trigger('click');
    }

}
$("#sign-up").on('click',function(evt){

   if($("input[name=aggree]").val()!='1'){

       return false;
   }
    return true;
});
seajs.use(['art','bootstrap','jq.cookie','socket.io'],function(template){


    window.CH = io.connect(config.chUrl);
    CH.emit('qr.room',{room:("qr_"+config.tmpId)});

    var reg_username='';
    var reg_checked='';
    if($.cookie('remember'))
    {
        reg_username=$.cookie('remember');
        reg_checked="checked";
    }
    var login='<form action="/user/login" method="POST" name="login-form" id="login-form" class="ajax-form">' +
        '<div class="return-error"></div><div class="return-suc"></div>' +
        '<div class="input-group user-login"> <input type="text"  value="<%=reg_username%>" name="username" class="form-control"  placeholder="<%=username%>" ref=\'{"reg":"user","r":1}\'> </div> ' +
        '<div class="input-group user-login "> ' +
        '<input type="password" class="form-control" placeholder="<%=pwd%>" ref=\'{"reg":"password","r":1}\' name="password"> </div><label class="checkbox"> ' +
        '<input type="checkbox" <%=reg_checked%> value="1" name="remember"> <%=remember%> <span> <a data-toggle="modal" href="#" id="forget-pwd"> ' +
        '<%=forgetpwd%>?</a> </span> </label> ' +
        '<input class="btn btn-primary btn-lg btn-login btn-block" type="submit" id="loginBtn" value="<%=signin%>"> </div></form>'+
    '<form action="/user/forgetpwd" method="POST" name="forgetpwd-form" id="forgetpwd-form" class="ajax-form" style="display: none"><div class="return-error"></div>' +
        '<div class="return-suc"></div><div class="input-group user-login"> ' +
        '<input type="text" value="" name="email" class="form-control" placeholder="<%=your_regemail%>" ref=\'{"reg":"email","r":"1"}\'> ' +
    '</div><input class="btn btn-lg btn-login btn-block" type="submit" id="findPwdBtn" value="<%=reset_pwd%>"> </form>'+
        '<div style="display: none" id="check-mail"><h4><%=check_mail_expire%></h4><a class="btn btn-lg btn-login btn-block" target="_blank" id="go-check-email" style="color:white"><%=check_mail%></a></div>'
+ '<form action="/user/resetpwd" method="POST" name="reset-form" id="reset-form" class="ajax-form" style="display: none"><div class="return-error"></div><div class="return-suc"></div> <div class="input-group user-lo' +
        'gin"><input type="hidden" name="actionToken" id="actionToken" value="<%=actionToken%>"/> <input type="text" value="" id="requestCode" name="requestCode" class="form-control" placeholder="<%=reqcode%>" ref=\'{"reg":"resetpwd_code","r":1}\'> ' +
        '</div> <div class="input-group user-login "> <img id="captcha-img" class="form-control" src="/landing/gencaptcha/" data="/landing/gencaptcha/"> ' +
        '<input type="text" class="form-control" name="magicword" id="magicword" placeholder="<%=capimg_txt%>" ref=\'{"reg":"the_captcha","r":1}\'> </div><div class="input-group user-login"> <input type="password" value="" name="reset_password" class="form-control" placeholder="<%=newpwd%>" ref=\'{"reg":"password","r":1}\'> </div> <div class="input-group user-login "> <input type="password" class="form-control" placeholder="<%=pwdagain%>" ref=\'{"reg":"password","r":1,"ref":"renewpassword"}\' name="reset_repassword"> </div>   </span> </label><input class="btn btn-lg btn-login btn-block" type="submit" id="resetBtn" value="<%=reset_pwd%>"></form>'
   + '<div style="display: none" id="reset-result"><span id="reset-result-txt"></span><input class="btn btn-lg btn-login btn-block" type="button" id="backToLogin" value="Back to login"></div>'
    + '<form action="/user/mfaauth" method="POST" name="mfa-form" id="mfa-form" class="ajax-form" style="display: none"><div class="return-error"></div>' +
        '<div class="return-suc"></div><input type="text" value="" name="mfa" class="form-control" placeholder="<%=authcode_phone%>" ref=\'{"reg":"mfa","r":"1"}\'><input class="btn btn-lg btn-login btn-block" type="submit" id="mfaBtn" value="<%=multiauth%>"></form>';


    var usernameValue=meeting.user?(meeting.user.email || meeting.user.username): '';
    if(meeting.user && meeting.user.mobile!='0' && usernameValue!=''){
        usernameValue=meeting.user.mobile;
    }
    var reg_username=usernameValue || reg_username;
    var loginHtml=template.compile(login)($.extend({reg_username:reg_username,reg_checked:reg_checked},config.lang,meeting,{usernameValue:usernameValue}));
    var loginParam=meeting.meeting?("/?mnum="+meeting.meeting):"";
    ajaxCbs['meeting-form']=[function(data){

        location.href = "/index/index"+loginParam;

    }];
    ajaxCbs['router-form']=[function(data){
        location.href="/admin/index/index";
        return false;
    }];
    ajaxCbs['regcode-form']=[
        function(data){
            console.log("regcode",data,"data");

            if(regType){
                if(regType==1){
                    setWizStep("3");
                    location.href="/index/index";

                    $("#regcode-form").hide();
                } else if(regType==2){

                    $("#sign-up-form").hide();
                    companySign();
                }
                //fogpod
                else if(regType==3){
                    console.log("forpod");
                    setWizStep("3");
                    $("#router-form").parent().find('form').hide();
                    $("#router-form").show();
                    //$("#router-form #en_fog_cloud").on('click',function(){
                    //        if($("#router_pwd").prop('checked')){
                    //                $(this).show();
                    //        } else {
                    //            $(this).hide();
                    //        }
                    //});



                }
            } else {
                location.href="/index/index";
                $("#regcode-form").hide();
            }






            return 0;
        },function(data){
            console.log("reg router user failed");
        }
    ];
    var getRegcode=function(e){

        $("#regcode-get").prop("disabled",true);

        var errDiv=$("#regcode-form").find("return-error");
        var sucDiv=$("#regcode-form").find("return-suc");
        console.log("regcode get");

        setuploadingEffect($(this));
        $.post("/user/getregcode",{userName:$(document).data('regUserName')},function(data){

            var ret= $.parseJSON(data);
            $("body").prepend(ret.data.requestCode);
            processRet(ret,sucDiv,errDiv,function(data){
                $('.countdown').show();
                $('.countdown').countdown({
                    tmpl : '%{s}'+config.lang.sec_left,
                    afterEnd:function(){

                        $("#regcode-get").text(config.lang.re_val_code).show();
                        $('.countdown').hide();
                    }
                });
                $("#regcode-form input[name=actionToken]").val(ret.data.actionToken);
                $("#regcode-chk").prop('disabled',false).addClass('active').show();

            },function(data){

            });


        }).complete(function(data){
            $("#regcode-get").hide();
        });

    };
    $("#signUp").on('click',function(e){
        $(".signup-section").addClass("signup");
        $("#thumbnail-content,.hero").hide();
        $(".sign-up-container").slideLeftShow();
        e.preventDefault();
        return false;
    });

    $("#sign-in").popover({html:true,trigger:'click',placement:'bottom',container:'body'}).on('shown.bs.popover',function(e){

        var popupWinId=$(this).attr('aria-describedby');
        var self=$(this).next();
        //$("#wechat-sign-in").attr({'data-content': qrLoginGuide, 'title': config.lang.scan_login}).popover(
        //    {html: true, trigger: 'click', placement: 'bottom',container:'body'}
        //).on('shown.bs.popover',function(e) {
        //    var popupWinId=$(this).attr('aria-describedby');
        //    var self=$(this).next();
        //    var wechatQrUri="https://open.weixin.qq.com/connect/qrconnect?appid=wxdfb5bc44d3e66e8f&redirect_uri="+encodeURIComponent('http://www.fogpod.com')+"&response_type=code&scope=snsapi_login&state="+config.tmpId+"#wechat_redirect";
        //
        //
        //    if(!$(document).data('isWechatListening')) {
        //        $(document).data('isWechatListening', true);
        //
        //        $("#pb_qrcode").attr({'src':wechatQrUri }).load(function () {
        //            $(this).css({width: '200px', height: 'auto'});
        //        });
        //    }
        //
        //        }).on('hidden.bs.popover', function () {
        //
        //            $(this).next().remove();
        //
        //            // setTimeout("$('#service-tab .tab-content').css({'overflow':'scroll'});",13500);
        //
        //
        //
        //});
        $("#qr-sign-in").attr({'data-content': qrLoginGuide, 'title': config.lang.scan_login}).popover(
            {html: true, trigger: 'click', placement: 'bottom',container:'body'}
        ).on('shown.bs.popover',function () {

                //  var tw=$(this).next().attr({});

                if(!$(document).data('isListening')){
                    $(document).data('isListening',true);
                    setQrUrl(("/callback/wechatqr/?action=login&type=wechat&tempid="+config.tmpId));

                    CH.on('notify',function(data){
                        console.log("notify data",data);
                        if('_notify_type' in data){
                            $.publish(data['_notify_type'],data);
                        }


                        else if (typeof window[data['type']+capStr(data['action'])] == 'function'){
                            window[data['type']+capStr(data['action'])](data);
                        }


                    });

                }
                var nextEle = $(this).next();

                // tw.width((tw. width()+30));
                bsFist = 2;

            }).on('hidden.bs.popover', function () {

                $(this).next().remove();

                // setTimeout("$('#service-tab .tab-content').css({'overflow':'scroll'});",13500);


            });

        $("#wechat-sign-in").on('click',function(){
            var wechatQrUri="https://open.weixin.qq.com/connect/qrconnect?appid=wxdfb5bc44d3e66e8f&redirect_uri="+encodeURIComponent('http://www.fogpod.com/callback/wechatopen')+"&response_type=code&scope=snsapi_login&state="+config.tmpId+"#wechat_redirect";
            CH.on('notify',function(data){
                console.log("notify data",data);
                if('_notify_type' in data){
                    $.publish(data['_notify_type'],data);
                }
                else if (typeof window[data['type']+capStr(data['action'])] == 'function'){
                    window[data['type']+capStr(data['action'])](data);
                }

            });
            var win=openWindow(wechatQrUri);

        });
       ajaxCbs['login-form']=[
        function(data){
            if(data.data && data.data.mfa==1){
                $("#login-form").hide();
                $("#mfa-form").show();
            }

            else {
                console.log(data);
                if(data.action && data.action=='quit'){

                   CH.emit('quit_session',{uid:data.uid});
                }

                location.href=config.siteCfg['after_login_page']+loginParam;
            }

            return 0;
        }
       ];
        ajaxCbs['mfa-form']=[
            function(data){
                location.href="/index/index"+loginParam;
                return 0;
            }
        ];
        $("#backToLogin").on('click',function(data){
            $("#reset-form").hide();
            $("#login-form").show();
            $("#reset-result").hide();
            $("#forgetpwd-form").hide();
            self.find('.popover-title').text(config.lang.login);
            return false;
        });
        $("#forget-pwd").on('click',function(data){
            $("#login-form").hide();
            $("#forgetpwd-form").show();
            self.find('.popover-title').text(config.lang.resetpwd);
            return false;
        });
        ajaxCbs['forgetpwd-form']=[
            function(data){
                console.log(data);

                $("#go-check-email").attr('href',"http://"+data.link);
                $("#go-check-email").on('click',function(){
                    $("#check-mail").hide();
                    $("#reset-form").show();
                });
                $("#check-mail").show();
                $("#forgetpwd-form").hide();
                return 0;

            }
        ];
        ajaxCbs['reset-form']=[
            function(data){
                $("#reset-form").hide();
                $("#reset-result-txt").text(config.lang.resetpwd_suc);
                $("#reset-result").show();
                return 0;

            },
            function(data){
                $("#captcha-img").attr('src',$("#captcha-img").attr("data")+"/?r="+Math.random());


                return 0;

            }
        ];

        $("#captcha-img").on('click',function(){

           this.src=$(this).attr("data")+"/?r="+Math.random();
        });

        if(typeof callBack!='undefined'){
            if(callBack=='reset_pwd'){

                $("#requestCode").val(requestCode);
                $("#actionToken").val(actionToken);
                self.find('.popover-title').text(config.lang.resetpwd);
                $("#login-form").hide();
                $("#reset-form").show();
            }
            if(callBack=="meeting"){
                if(meeting.user.level!='110'){
                    $("#"+popupWinId).find('.popover-title').text(config.lang.visit_meeting);
                }
              //self.find('.popover-title').text(config.lang.visit_meeting);


            }
        }

    });
    if(typeof callBack!='undefined' && callBack=='reset_pwd'){
        setTimeout(function(){$("#sign-in").popover('toggle');},50);

    }



    if(!$.isEmptyObject(meeting)){
        if(meeting.user.level!=110) {
            setTimeout(function(){
                $("#sign-in").trigger('click');
            },100);
        } else {
                meetingLogin(meeting);
        }


    }

    var qrLoginGuide = '<div><img id="pb_qrcode" /></div>';
    var faceLogin='<div class="return-error"></div><div class="return-suc"></div><div style="display: none" id="face-preview"></div> <canvas id="cam-canvas" width="240" height="240" style="display: none"></canvas> <video id="cam-video" autoplay loop style="display:none" width="240" height="240"></video>';
    var bsFist = 1;
    var isListener=false;
    $("#qr-face-in").attr({'data-content': faceLogin, 'title': config.lang.putface_frame}).popover(
        {html: true, trigger: 'click', placement: 'bottom'}
    ).on('shown.bs.popover',function () {
            var that=$(this).next();
            console.log(that);

            seajs.use(['ht','canvas2blob'],function(){

                var video=$(that).find("#cam-video");
                video.show();
                var htracker = new headtrackr.Tracker({});
                var canvasInput=$(that).find("#cam-canvas")[0];
                var videoInput=video[0];
                console.log(canvasInput,videoInput);
                htracker.init(videoInput, canvasInput);
                var errDiv = $(that).find(".return-error");
                var sucDiv = $(that).find(".return-suc");
                htracker.start();
                document.addEventListener('headtrackrStatus',
                    function (event) {
                        console.log(event.status);
                        if (event.status == "getUserMedia") {

                        }
                        else if (event.status == "found") {
                            var canvas = $("<canvas>")[0];

                            canvasInput.getContext('2d').drawImage(videoInput, 0, 0, 240, 240);
                            var data = canvasInput.toDataURL('image/png');


                            canvasInput.toBlob(function (blob) {

                                var formData = new FormData();
                                formData.append('ctoken', config.ctoken);
                                formData.append('face',1);
                                formData.append("avatar", blob);
                                $.ajax({
                                    url: "/user/faceverify",
                                    type: "POST",
                                    data: formData,
                                    processData: false,  // 告诉jQuery不要去处理发送的数据
                                    contentType: false,   // 告诉jQuery不要去设置Content-Type请求头
                                    global: false,
                                    success: function (data) {

                                        var ret = $.parseJSON(data);
                                        if (ret.code == '1') {
                                            sucDiv.text(ret.msg).show();
                                            setTimeout(function () {
                                                sucDiv.hide()
                                            }, 2500);
                                            videoInput.pause();
                                            video.hide();
                                            location.href="/index";
                                        }
                                        else {
                                            errDiv.text(ret.msg).show();
                                            setTimeout(function () {
                                                errDiv.hide()
                                            }, 2500);
                                        }


                                    }
                                });


                            }, "image/png");

                            console.log("FOV",htracker.getFOV());
                        }
                    }
                );
            });

        });







    $("#sign-up-form").on('submit',function(e){




        var formParent= $(this);
        var errNode=formParent.find(".return-error");
        var sucNode=formParent.find(".return-suc");

        var that=this;

        $.post($(this).attr("action"),$(this).serialize(),function(data){


            var ret= $.parseJSON(data);
            var showNode=errNode;
            if(ret.code==-1)
            {

                errNode.text(ret.msg).show();

            }
            else
            {
                setWizStep("2");
                showNode=sucNode;
                sucNode.text(ret.msg).show();
                $(that).hide();
                $("#regcode-get").on('click',getRegcode);
                $("#regcode-form").show();
                $(document).data('regUserName',$("#reg_username").val());
                //setTimeout(function(){location.href="index/index";},2000);
            }

        });



        e.preventDefault();
        return false;

    });

    $("#sign-in").attr({"data-content":loginHtml,'data-original-title':(config.lang.signin+'<a id="wechat-sign-in" class="sign-in"><img src="/img/apps/wechat.png" style="float:right" alt="" height="26" width="26"/></a><a id="qr-sign-in" class="sign-in"><img src="/img/qrcode.png" style="float:right" alt="" height="26" width="26"/></a>')});

return false;

});


(function($) {
    $.fn.countdown = function(options) {
        // default options
        var defaults = {
            attrName : 'data-diff',
            tmpl : '<span class="hour">%{h}</span><span class="minute">%{m}</span>分钟<span class="second">%{s}</span>秒',
            end : 'has ended',
            afterEnd : null
        };
        options = $.extend(defaults, options);


        // trim zero
        function trimZero(str) {
            return parseInt(str.replace(/^0/g, ''));
        }
        // convert string to time
        function getDiffTime(str) {
            var m;
            if ((m = /^(\d{4})[^\d]+(\d{1,2})[^\d]+(\d{1,2})\s+(\d{2})[^\d]+(\d{1,2})[^\d]+(\d{1,2})$/.exec(str))) {
                var year = trimZero(m[1]),
                    month = trimZero(m[2]) - 1,
                    day = trimZero(m[3]),
                    hour = trimZero(m[4]),
                    minute = trimZero(m[5]),
                    second = trimZero(m[6]);
                return Math.floor((new Date(year, month, day, hour, minute, second).getTime() - new Date().getTime()) / 1000);
            }
            return parseInt(str);
        }
        // format time
        function format(diff) {
            var tmpl = options.tmpl, day, hour, minute, second;
            day = /%\{d\}/.test(tmpl) ? Math.floor(diff / 86400) : 0;
            hour = Math.floor((diff - day * 86400) / 3600);
            minute = Math.floor((diff - day * 86400 - hour * 3600) / 60);
            second = diff - day * 86400 - hour * 3600 - minute * 60;
            tmpl = tmpl.replace(/%\{d\}/ig, day)
                .replace(/%\{h\}/ig, hour)
                .replace(/%\{m\}/ig, minute)
                .replace(/%\{s\}/ig, second);
            return tmpl;
        }
        // main
        return this.each(function() {
            var el = this,
                diff = getDiffTime($(el).attr(options.attrName));
            function update() {
                if (diff <= 0) {
                    $(el).html(options.end);
                    if (options.afterEnd) {
                        options.afterEnd();
                    }
                    return;
                }
                $(el).html(format(diff));
                setTimeout(function() {
                    diff--;
                    update();
                }, 1000);
            }
            update();
        });
    };
})(jQuery);


var tabsFn = (function() {

    function init() {
        setHeight();
    }

    function setHeight() {
        var $tabPane = $('.tab-pane'),
            tabsHeight = $('.nav-tabs').height();

        $tabPane.css({
            height: tabsHeight
        });
    }

    $(init);
})();