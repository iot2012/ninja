
var __topic= {};

jQuery.topic = function( id ) {
    var callbacks,
        method,
        topic = id && __topic[ id ];
    if ( !topic ) {
        callbacks = jQuery.Callbacks();
        topic = {
            publish: callbacks.fire,
            subscribe: callbacks.add,
            unsubscribe: callbacks.remove
        };
        if ( id ) {
            __topic[ id ] = topic;
        }
    }
    return topic;
};

