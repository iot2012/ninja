define(function(require, exports, module) {
    var options={
        temp:{
            file:'<div data-ext="<%=ext%>" class="form-group <%=margin%>"> <label  class="col-sm-6 col-md-3 col-lg-3 control-label _extra-cls"><%=lang[key]?lang[key]:key%></label> <div class="col-sm-6 col-md-9 col-lg-9"> <%if(num>0){%> <%for(var i=0;i<num;i++){%> <div id="<%=id%><%=i%>"  class="pull-left upload-file" type="file"  data-seq="<%=i%>" name="<%=key%>[<%=i%>]"></div> <%}%> <%}else {%> <div id="<%=id%>" type="file" class="upload-file pull-left" name="<%=key%>"></div> <%}%> </div> </div>',
            input:'<div class="form-group"> <label  class="col-sm-6 col-md-3 col-lg-3 control-label _extra-cls"><%=lang[key]?lang[key]:key%></label> <div class="col-sm-6 col-md-9 col-lg-9"><%if(num>0){%> <%for(var i=0;i<num;i++){%> <input id="<%=id%>" ref=\'{"reg":"<%=type%>"}\' type="<%=type%>"  class="form-control"  placeholder="<%=lang[key]?lang[key]:key%>" <%=value%> name="<%=key%>[]"><%}%> <%}else {%> <input ref=\'{"reg":"<%=type%>"}\' type="<%=type%>" id="<%=id%>"  class="form-control"  placeholder="<%=lang[key]?lang[key]:key%>" <%=value%> name="<%=key%>"> <%}%></div></div>',
            textarea:'<div class="form-group"> <label  class="col-sm-6 col-md-3 col-lg-3 control-label _extra-cls"><%=lang[key]?lang[key]:key%></label> <div class="col-sm-6 col-md-9 col-lg-9"><textarea  class="form-control"  placeholder="<%=lang[key]?lang[key]:key%>" rows="5" name="<%=key%>"><%=value%></textarea></div></div>',
            checkbox:'<div class="form-group"><label  class="col-sm-6 col-md-3 col-lg-3 control-label _extra-cls"><%=lang[key]?lang[key]:key%></label><div class="checkbox"> <%for(var i=0;i<fields.length;i++){%> <%for(var key1 in fields[i]){%> <label> <input type="checkbox" name="<%=key%>[]" value="<%=fields[i][key1]%>"><%=lang[key+"_"+key1]?lang[key+"_"+key1]:(lang[key1]?lang[key1]:(key+"_"+key1))%></label> <%}%> <%}%> </div></div>',
            radio:'<div class="form-group"><label  class="col-sm-6 col-md-3 col-lg-3 control-label _extra-cls"><%=lang[key]?lang[key]:key%></label><div class="checkbox"> <%for(var i=0;i<fields.length;i++){%> <%for(var key1 in fields[i]){%> <label> <input type="radio" name="<%=key%>" value="<%=fields[i][key1]%>"><%=lang[key+"_"+key1]?lang[key+"_"+key1]:(lang[key1]?lang[key1]:(key+"_"+key1))%></label> <%}%> <%}%> </div></div>'
        },
        fileKeys:['img','icon','avatar','avatars','imgs','icons','logo','logos'],
        skipKeys:['theme','id','dataApi'],
        inputTypes:['email','datetime','date','color','datetime-local','month','week','time','number','range','search','tel','url','file','image','password']

    };
     var getOptions=function(){
        return options;
    }
var combLang=function(){
    var langs=[];
    var limiter=' ';
    var lang=arguments[0];
    for(i=1;i<arguments.length;i++){
        langs.push(lang[arguments[i]]?lang[arguments[i]]:arguments[i]);
    }

    if(true || config.lang.langType!='zhcn'){
        limiter='';
     
    }
    return langs.join(limiter);

}
    var getLang=function(lang,compName,key){

        if(key=='true'){key='l_true';}
        if(key=='false'){key='l_false';}
        if(typeof lang[(compName+'_'+key)]!='undefined'){
            return lang[(compName+'_'+key)];
        } else if(typeof lang[key]!='undefined'){
            return lang[key];
        }

        return key;
    }
    var getValue=function(value){
        var data={"true":true,"false":false}
        return data[value]?data[value]:value;
    }
    var getType=function(object){
        return Object.prototype.toString.call(object);
    }
    var isImgKey=function(key){
       var fileKeys=['img','icon','avatar','avatars','imgs','icons','logo','logos']
        var result=fileKeys.map(function(item){
            var regExp=new RegExp(item+'$|\[[\w-]+'+item+'\]','i');
            return regExp.test(key);

        });
        return result.indexOf(true)>=0;

    }
    var merge = function() {
      var il = arguments.length;
      var result={};
      for (var i = il - 1; i >= 0; --i) {
          for (var key in arguments[i]) {
              if (arguments[i].hasOwnProperty(key)) {
                  result[key] = arguments[i][key];
              }
          }
      }

     return result;   
    };
    var process=function(cfg,lang,prefix,extra){
        var prefix=prefix || "";
        var extra=extra || {};
        var inputs=[];
        var options=getOptions();
        var cfgs=cfg.fields;
        for(var i=0;i<cfgs.length;i++){
            for(var key in cfgs[i]){
                var keyType=getType(cfgs[i][key]);
                 switch(keyType){
                    case '[object Object]':
                    {
                        var num=('num' in cfgs[i][key])?cfgs[i][key]['num']:0;
                        var type=('type' in cfgs[i][key])?cfgs[i][key]['type']:'text';
                         var ext=('ext' in cfgs[i][key])?cfgs[i][key]['ext']:'*';
                        if(type=='checkbox'){
                            inputs.push(template.compile(options.temp.checkbox)(merge({lang:lang},cfgs[i][key],{key:key})));
                        } else if(type=='radio'){
                            inputs.push(template.compile(options.temp.radio)(merge({lang:lang},cfgs[i][key],{key:key})));
                        } else if(type=='textarea') {
                            inputs.push(template.compile(options.temp.textarea)(merge({lang:lang},cfgs[i][key],{key:key})));
                        } else if(type=='file'){
                            var margin='';
                            if(isImgKey(key)){
                                margin='upload-gap';
                            }
                            inputs.push(template.compile(options.temp.file)(merge({ext:ext},{margin:margin},{id:(prefix+key)},{num:num},{lang:lang},cfgs[i][key],{key:key},{type:type})));
                        } else {
                            inputs.push(template.compile(options.temp.input)(merge({num:num},{lang:lang},cfgs[i][key],{key:key},{type:type})));
                        }
                        break;
                    }
                
                     case '[object String]':
                    {
                     

                        inputs.push('<div class="form-group"> <label  class="col-sm-6 col-md-3 col-lg-3 control-label">'+combLang(lang,key)+"</label><div class='col-sm-6 col-md-9 col-lg-9'>"+combLang(lang,cfgs[i][key])+"</div></div>");

                        break;
                    }

                }
           

            } 
        }
        var extraData=[];
        for(var key in extra){
            extraData.push('<input type="hidden" name="'+key+'" value="'+extra[key]+'">');
        }
        return '<form id="_cfg_'+prefix+'" class="form-horizontal ajax-form" role="form" action="'+cfg.action+'" method="post">'+extraData.join("")+'<input type="hidden" name="_cfg_section" value="'+prefix+'">'+inputs.join("")+"</form>";

    };


    module.exports={
        process:process,
        options:options,
        getOptions:getOptions
    };
});



