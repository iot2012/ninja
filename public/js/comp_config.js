define(function(require, exports, module) {
    var options={
        temp:{
            input:'<div class="form-group"> <label  class="col-sm-6 col-md-3 col-lg-3 control-label _extra-cls">#key#</label> <div class="col-sm-6 col-md-9 col-lg-9"><input ref='+'{"reg":"#reg#"}'+' type="#reg#"  class="form-control"  placeholder="#key#" name="#name#"></div></div>',
            link:'<label  class="col-sm-6 col-md-3 col-lg-3 control-label _extra-cls">#key#</label><div class="col-sm-6 col-md-9 col-lg-9"><div class="input-group"> <div class="input-group-btn"> <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">#choose-link#<span class="caret"></span></button> <ul class="dropdown-menu" role="menu"> <li><a href="#outer-link">#outerLink#</a></li> </ul> </div>  <input ref='+'{"reg":"#reg#"}'+' type="#reg#"  class="form-control"  placeholder="#key#" name="#name#"></div></div>',
            textarea:'<div class="form-group"> <label  class="col-sm-6 col-md-3 col-lg-3 control-label _extra-cls">#key#</label> <div class="col-sm-6 col-md-9 col-lg-9"><textarea ref='+'{"reg":"#reg#"}'+' class="form-control"  placeholder="#key#" rows="5" name="#name#"></textarea></div></div>'
        },
        fileKeys:['img','icon'],
        skipKeys:['theme','id','dataApi'],
        inputTypes:['email','datetime','date','color','datetime-local','month','week','time','number','range','search','tel','url','file','image','password']

    };
    var getOptions=function(){
        return options;
    }
    var getLang=function(lang,compName,key){

        if(key=='true'){key='l_true';}
        if(key=='false'){key='l_false';}
        if(typeof lang[(compName+'_'+key)]!='undefined'){
            return lang[(compName+'_'+key)];
        } else if(typeof lang[key]!='undefined'){
            return lang[key];
        }
        console.log(key);
        return key;
    }
    var getValue=function(value){
        var data={"true":true,"false":false}
        return data[value]?data[value]:value;
    }
    var getType=function(object){
        return Object.prototype.toString.call(object);
    }
    var isFileKey=function(key){

        var result=options.fileKeys.map(function(item){
            var regExp=new RegExp(item+'$|\[[\w-]+'+item+'\]','i');
            return regExp.test(key);

        });
        return result.indexOf(true)>=0;

    }
    var processContent=function(obj,keyName,lang,compName,level){

        var level=level || '';
        var orgTemp=orgTemp || '<div class="panel panel-default"> <div class="panel-heading">#header#</div> <div class="panel-body">#content#</div> </div>';
        var temp=temp || orgTemp;
        var content='';
        var options=getOptions();
        for(var key in obj){
            if(options.skipKeys.indexOf(key)!=-1){
                continue;
            }
            var keyType=getType(obj[key]);

            switch(keyType){
                case '[object Object]':
                {

                    content+=processContent(obj[key],key,lang,compName,(level==''?key:(level+'['+key+']')));

                    break;
                }
                case  '[object Array]':{
                    var cnt=0;
                    var result=obj[key].map(function(item){
                        var content=processContent(item,key,lang,compName,((level==''?key:(level+'['+key+']'))+'['+cnt+']'))+"<button class='btn btn-primary btn-lg btn-block add-block hidden'></button>";
                        cnt++;
                        return content;


                    });
                    content+=result.join("");

                    break;
                }
                default:{

                    var vals=obj[key].split(",");

                    if(vals.length>=2){

                        var select='<div class="form-group"> <label  class="col-sm-6 col-md-3 col-lg-3 control-label">#key#</label> <div class="col-sm-6 col-md-9 col-lg-9"><select class="form-control">';
                        var opts=[];
                        for(var i=0;i<vals.length;i++){
                            var ele=vals[i].split(":");
                            langTxt='';
                            if(ele.length==1){
                                langTxt=getLang(lang,compName,ele[0]);

                            } else {
                                langTxt=getLang(lang,compName,ele[1]);


                            }
                            opts.push('<option value="'+getValue(ele[0])+'">'+langTxt+'</option>');



                        }
                        content+=(select+opts.join("")).replace(/#key#/g,getLang(lang,compName,key)).replace(/#name#/,(level==''?key:(level+'['+key+']')))+"</select></div>";

                    } else {

                        var inputType="text";
                        if($.trim(obj[key])!=''){
                            inputType=(options.inputTypes.indexOf(obj[key])!=-1)?obj[key]:'text';
                        }
                        if(isFileKey(key)){
                            inputType="file";
                        }
                        if(/link$/.test(key)){
                            content+=options.temp.link.replace(/#reg#/g,inputType).replace(/#key#/g,getLang(lang,compName,key)).replace(/#name#/,(level==''?key:(level+'['+key+']'))).replace(/_extra-cls/,"site-link").replace(/#outerLink#/,config.lang.enterLink).replace(/#choose-link#/,config.lang.choose_link);

                        }
                        else if(/\w+(desc|title)$/.test(inputType)){
                                content+=options.temp.textarea.replace(/#reg#/,inputType).replace(/#key#/g,getLang(lang,compName,key)).replace(/#name#/,(level==''?key:(level+'['+key+']')));
                        } else {
                                content+=options.temp.input.replace(/#reg#/g,inputType).replace(/#key#/g,getLang(lang,compName,key)).replace(/#name#/,(level==''?key:(level+'['+key+']')));
                        }

                    }
                    break;
                }


            }

        }

        return temp.replace(/#header#/g,getLang(lang,compName,keyName)).replace(/#content#/g,content);

    };


    module.exports={
        processContent:processContent,
        options:options,
        getOptions:getOptions
    };
});



