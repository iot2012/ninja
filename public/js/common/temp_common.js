var dataReadyCb;
if(typeof seajs=='function'){
    seajs.config(
        {base: '/templates/dinning/assets/js'}
    );

}
var toATag=function(href){
    var a=document.createElement("a");
    a.href=href;
    return a;
}
var isImgKey=function(key){
    var fileKeys=['img','icon','avatar','avatars','imgs','icons','logo','logos']
    var result=fileKeys.map(function(item){
        var regExp=new RegExp(item+'$|\[[\w-]+'+item+'\]','i');
        return regExp.test(key);

    });
    return result.indexOf(true)>=0;

}
/**
 * 每个页面对应返回数据哪个section数据
 */



var config={
    t_section:{
        'index':'homepage'
    },
    imgdir:'/'
};
var openAqiModal=function(envInfo){
   // var oldTemp='<div class="am-modal am-modal-no-btn" tabindex="-1" id="air-modal"> <div class="am-modal-dialog"> <div class="am-modal-hd">当前环境信息: <a href="javascript: void(0)" class="am-close am-close-spin" data-am-modal-close>&times;</a> </div> <div class="am-modal-bd"> <div class="widget-container widget-weather boxed clearfix air-card"> <div class="air-temp"><strong>#aqi#</strong></div> <div class="water-temp"><strong>#humidity#%</strong></div> <div class="wind-speed"><strong>#temp#°C</strong></div> </div> </div></div> </div>';

    var temp='<div id="page1" class="group-tab">  <div style="text-align:center;color:white;">#time#</div><div> <div style="height: 85px;width: 90%;margin:0 auto"> <div class="data-group in-line x"> <label style="font-size:15px;">温度(℃)</label> <br> <br> <img src="/air_wechat/img/icon_tem.png" alt="" style="width: 30px;"> <label style="font-size:45px;" id="temp">#temp#</label> </div> <div class="data-group in-line "> <label style="font-size:15px;">湿度(%)</label> <br> <br> <img src="/air_wechat/img/icon_hum.png" alt="" style="width: 30px;"> <label style="font-size:45px;" id="hum">#humidity#</label> </div> <div class="data-group in-line "> <label style="font-size:15px;">浓度(ug/m3)</label> <br> <br> <img src="/air_wechat/img/icon_ug.png" alt="" style="width: 30px;"> <label style="font-size:45px;" id="pm25">#aqi#</label> </div> </div> </div> <div style="height: 40px;width: 100%;"></div> <div class="progress"> <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">    <span style="float:left">0</span> IAQI<span class="sr-only">#iaqi#</span><span>(#pollution#)</span></div> <span style="float:right">500</span> </div> </div>';
    for(var key in envInfo){
        var reg=new RegExp("#"+key+"#","i");
        temp=temp.replace(reg,envInfo[key]);
    }
    var modal=$(temp);
    if($("#air-modal").size()==0){
        $("body").append(modal);
        modal.modal({height:"300px"});
    } else {
        modal.modal('open');
    }


    return modal;
}
var setTimeoutModal=function(modal,title,msg){
    modal.find(".am-modal-hd").html(title+'<a href="#close" style="right: 35px; font-size: 26px;" class="am-close am-close-spin" data-am-modal-close>&times;</a>');
    modal.find(".am-modal-bd").html('<div classs="_msg am-alert am-alert-danger" style="color: #dd514c;background-color: rgba(221,81,76,.115);">'+msg+'</div>');
    modal.find(".am-close-spin").click(function(evt){
        modal.modal('close');
        evt.preventDefault();
        return false;
    });


}
var setAqiModal=function(modal,title,envInfo){
    var aqiStyle='#page1 .data-group>label{margin-top:-15px}  .short-pol {display: none;float:right} #page1 { background-color:#9BCD9B; padding: 10px 5px 30px 5px; } #page1 .data-group{ width: 31%; position: relative; margin: 1%; } #page1 .in-line { float: left; display: inline-block; text-align: center;} #page1 .data-group img {margin-top: -14px;} .long-pol {line-height: 35px; font-size: 25px;} #page1 label{ color: #fff; font-size: 50px; } #page1 p{ color: #fff; height: 20px; font-size: 15px; padding: 5px; } #page1 .box { width: 100%; margin: 0px auto; } #page1 .twobox div{ width: 50%; } #page1 .threebox div{ width: 33.333%; } #page1 .tab { color: white; width: 100px; } #page1 .nav_tab { list-style: none; } #page1 .nav_tab li { display: inline-block; padding: 10px; width: 80px; } #page1 .group-tab { position: absolute; width: 100%; /*background-color: rgba(66,66,66,0.5);*/ padding: 20px 0 20px 0; } #page1 .hidden { visibility: hidden; } #page1 .tab_active a { color: blue; } #div_tab { margin-top: 20px; } #move_line { margin-left: 20px; margin-top: 5px; width: 40%; position: absolute; } #page1 .weather { margin: 20px 30px 0px 30px; width: 90%; height: 130px; } #page1 .place_weather{ } #page1 .place_child{ height: 150px; float: left; width: 50%; } #page1 .place_weather a{ line-height: 50px; height: 40px; font-size: 30px; } #page1 .place_weather label{ height: 30px; font-size: 20px; } #page1 .pull_info{ height: 40px; width: 100%; } #page1 .weather_para { height: 50px; } #page1 .weather_para img { width: 30px; } #page1 .weather_para label { font-size: 20px; } #weathercontent label { font-size: 20px; } #main_aqi { background: rgba(255, 255, 255, 0.3); border: 0px white solid; border-radius: 100px; width: 150px; height: 150px; margin: 0px auto; } #main_aqi img { margin-top: 3px; height: 40px; padding-bottom: 8px; vertical-align: middle; } #main_aqi label { font-size: 35px; } .weather_sensor label { font-size: 15px; } #page1 .weather_div { width: 15%; float: left; line-height: 20px; } #page1 .weather_div_1 { width: 35%; float: left; line-height: 20px; } @-webkit-keyframes progress-bar-stripes { from { background-position: 40px 0; } to { background-position: 0 0; } } @-o-keyframes progress-bar-stripes { from { background-position: 40px 0; } to { background-position: 0 0; } } @keyframes progress-bar-stripes { from { background-position: 40px 0; } to { background-position: 0 0; } } #page1 .progress { padding: 0px 20px } .progress { width: 100%; height: 35px; overflow: hidden; border-radius: 4px; -webkit-box-shadow: inset 0 1px 10px rgba(0, 0, 0, .3); box-shadow: inset 0 1px 10px rgba(0, 0, 0, .3); } .progress-bar { float: left; width: 0; height: 100%; font-size: 25px; line-height: 35px; color: #fff; text-align: center; background-color: #337ab7; -webkit-box-shadow: inset 0 -1px 0 rgba(0, 0, 0, .15); box-shadow: inset 0 -1px 0 rgba(0, 0, 0, .15); -webkit-transition: width .6s ease; -o-transition: width .6s ease; transition: width .6s ease; } .progress-striped .progress-bar, .progress-bar-striped { background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent); background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent); background-image: linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent); -webkit-background-size: 40px 40px; background-size: 40px 40px; } .progress.active .progress-bar, .progress-bar.active { -webkit-animation: progress-bar-stripes 2s linear infinite; -o-animation: progress-bar-stripes 2s linear infinite; animation: progress-bar-stripes 2s linear infinite; } .progress-bar-success { background-color: #d9534f; } .progress-striped .progress-bar-success { background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent); background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent); background-image: linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent); } #modal-loading .am-modal-hd a.am-close.am-close-spin { right: 35px; font-size: 26px; }';
    var aqiStyleId='_aqi_style';
    if($("#"+aqiStyleId).size()==0){
        $("<style>").attr("id",aqiStyleId).html(aqiStyle).appendTo($("head"));
    }


    //var temp='<div class="widget-container widget-weather boxed clearfix air-card"> <div class="air-temp"><strong>#aqi#</strong></div> <div class="water-temp"><strong>#humidity#%</strong></div> <div class="wind-speed"><strong>#temp#°C</strong></div> </div>';
    var temp='<div id="page1" class="group-tab"> <div style="text-align:center;color:white;">#time#</div> <div> <div style="height: 85px;width: 90%;margin:0 auto"> <div class="data-group in-line x"> <label style="font-size:15px;">温度(℃)</label> <br> <br> <img src="/air_wechat/img/icon_tem.png" alt="" style="width: 30px;"> <label style="font-size:30px;" id="temp">#temp#</label> </div> <div class="data-group in-line "> <label style="font-size:15px;">湿度(%)</label> <br> <br> <img src="/air_wechat/img/icon_hum.png" alt="" style="width: 30px;"> <label style="font-size:30px;" id="hum">#humidity#</label> </div> <div class="data-group in-line "> <label style="font-size:15px;">浓度(ug/m3)</label> <br> <br> <img src="/air_wechat/img/icon_ug.png" alt="" style="width: 30px;"> <label style="font-size:30px;" id="pm25">#aqi#</label> </div> </div> </div> <div style="height: 40px;width: 100%;"></div> <div class="progress"> <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 1%">    <span style="float:left"></span>  <span class="sr-only iaqi">#iaqi#</span><span class="short-pol">IAQI:#pollution#</span></div><span class="long-pol-aqi">#iaqi#</span><span class="long-pol">IAQI:#pollution#</span> </div> <div style=" padding: 0 20px; "><span style="float:left;color:white">0</span><span style="float:right;color:white">500</span></div></div>';
    for(var key in envInfo){
        var reg=new RegExp("#"+key+"#","ig");
        temp=temp.replace(reg,envInfo[key]);
    }
    modal.find(".am-modal-hd").html(title+'<a href="#close" class="am-close am-close-spin" data-am-modal-close>&times;</a>');
    modal.find(".am-modal-bd").html(temp);
    if(envInfo['width']>=60){
        modal.find(".short-pol").show();
        modal.find(".long-pol").hide();
    }
    if(envInfo['width']<=20){
        modal.find(".iaqi").hide();
        modal.find(".long-pol-aqi").show();

    } else {
        modal.find(".iaqi").show();
        modal.find(".long-pol-aqi").hide();
    }

    modal.find(".am-close-spin").click(function(evt){
        modal.modal('close');

        evt.preventDefault();
        return false;
    });
    modal.find(".progress-bar").animate({
        width:(envInfo['width']+"%")
    });
    return modal;
}
var openLoadingModal=function(opts){

    var temp='<div class="am-modal am-modal-loading am-modal-no-btn" tabindex="-1" id="modal-loading"> <div class="am-modal-dialog"> <div class="am-modal-hd">正在载入...</div> <div class="am-modal-bd"> <span class="am-icon-spinner am-icon-spin" style="font-size: 5em"></span> </div> </div> </div>';
    var modal=$(temp);
    if($("#modal-loading").size()==0){
        $("body").append(modal);
        modal.modal(opts);
    } else {
        $("#modal-loading").modal('open');
    }

    return modal;
}
var showDisabledModal=function(opts){

    var temp='<div class="am-modal am-modal-loading am-modal-no-btn" tabindex="-1" id="modal-err"> <div class="am-modal-dialog"> <div class="am-modal-hd">获取失败<a href="#close" style="right: 35px; font-size: 26px;" class="am-close am-close-spin" data-am-modal-close>&times;</a></div> <div class="am-modal-bd"><div classs="_msg am-alert am-alert-danger" style="color: #dd514c;background-color: rgba(221,81,76,.115);">请在微信摇一摇中访问</div></div> </div> </div>';
    var modal=$(temp);
    if($("#modal-err").size()==0){
        $("body").append(modal);
        modal.modal(opts);
    } else {
        modal.modal('open');
    }
    modal.find(".am-close-spin").click(function(evt){
        modal.modal('close');
        modal.remove();
        evt.preventDefault();
        return false;
    });
    return modal;
}
var pollIdx=function(iAqi){
    var  aqiLevel = [0,50,100,150,200,250];
    var aqiName = ["优","良","轻度污染","中度污染","严重污染","重度污染"];

    if(iAqi > aqiLevel[5])
    {
       return aqiName[5];
    }else{
        for(i = 0 ; i < 5 ; i++)
        {
            if(iAqi>=aqiLevel[i] && iAqi<aqiLevel[i+1])
            {
                return aqiName[i];
            }
        }
    }

}

function calcIaqi(aqi)
{
    var len = 8;
    var IAQI = [0,50,100,150,200,300,400,500];
    var BP = [0,35,75,115,150,250,350,500];
    var CP  = aqi;
    for(i = 1; i < len; i++)
    {
        if(CP == BP[i])
            return IAQI[i];
        else if(CP < BP[i] && CP > BP[i-1])
        {
            return ((IAQI[i] - IAQI[i-1])*(CP - BP[i-1])/(BP[i] - BP[i-1]) + IAQI[i-1]);
        }
    }
    return 0;
}


Date.prototype.format = function(fmt)
{ //author: meizz
    var o = {
        "M+" : this.getMonth()+1,                 //月份
        "d+" : this.getDate(),                    //日
        "h+" : this.getHours(),                   //小时
        "m+" : this.getMinutes(),                 //分
        "s+" : this.getSeconds(),                 //秒
        "q+" : Math.floor((this.getMonth()+3)/3), //季度
        "S"  : this.getMilliseconds()             //毫秒
    };
    if(/(y+)/.test(fmt))
        fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
    for(var k in o)
        if(new RegExp("("+ k +")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
    return fmt;
}

var getMinDistBeacon=function(beaconArrs){
    var minDistBeacon={};
    var cnt=0;
    if(beaconArrs['beacons'].length>=1){
        beaconArrs['beacons'].forEach(function(beacon){
            if(cnt==0){
                minDistBeacon=beacon;
            } else {
                if(beacon.accuracy<minDistBeacon.accuracy){
                    minDistBeacon=beacon;
                }
            }
            cnt++;

        });
    } else {
        minDistBeacon=beaconArrs['beacons'][0];
    }
    return minDistBeacon;

}
var completeSearch=function(argv,loadingModal){
    //回调函数

    if(window.reqLock===false){


        window.reqLock=true;
        var minDistBeacon=getMinDistBeacon(argv);
        $.ajax({url:"http://api.gogoinfo.cn/air/sensorinfo.json",type:'GET',data:minDistBeacon,success:function(data){

            // {"code":1,"msg":"","data":{"ctime":1442633678,"temp":12,"humidity":32,"aqi":123,"actionType":0,"message":""}}
            if(!('data' in data) || !('temp' in data.data)){
                var envInfo=data.data;
                envInfo={'humidity':envInfo.humidity?envInfo.humidity:45,'aqi':(envInfo.aqi?envInfo.aqi:88),temp:(envInfo.temp?envInfo.temp:28)};
                var date=new Date();
                envInfo['time']=envInfo['ctime']?(envInfo['ctime']*1000):0;

                if(envInfo['time']!=0){

                    date.setTime(envInfo['time']);
                }
                envInfo['time']=date.format("yyyy-MM-dd hh:mm");

            } else {
                var time = new Date().format("yyyy-MM-dd hh:mm");
                var aqi=88;
                envInfo={'humidity':45,'aqi':aqi,temp:28,time:time};
            }
            envInfo['iaqi']=calcIaqi(envInfo['aqi']);
            envInfo['pollution']=pollIdx(envInfo['iaqi']);
            envInfo['width']=Math.round(envInfo['iaqi']/5);


            //envInfo={'humidity':45,'aqi':88,temp:28,pollution:111,time:"2012-12-12 11:11"};

            setAqiModal(loadingModal,'当前环境信息',envInfo);
            wx.stopSearchBeacons({
                complete:function(res){
                    // alert("stop complete");
                }
            });
        },error:function(jqXHR,textStatus,errorThrown){
            var title='请求错误';
            var msg='请求错误';
            if(textStatus=='timeout'){

                msg='10秒内没有获取到数据,稍后再试';
            }

            setTimeoutModal(loadingModal,'请求错误','10秒内没有获取到数据,稍后再试');

        }});
    }


};

var envRefReq=function(evt){
    if(!/MicroMessenger/i.test(navigator.userAgent)){
        showDisabledModal();
        evt.preventDefault();
        return false;
    }
    window.ibeacon_search_failed=false;
    var query=location.search.substr(1);
    query=$.unparam(query);
    var loadingModal=openLoadingModal({height:'300px',width:'380px'});
    setInterval(function(){

        if(!("_searched" in window) || window._searched==false){
            if(!loadingModal.hasClass("am-modal-out") && loadingModal.find(".am-icon-spin").size()>0){
                setTimeoutModal(loadingModal,'没有查找到信息','请确认摇一摇访问链接,且设备打开');
            }

        }

    },8000);

    wx.onSearchBeacons({
        success:function(argv){
            //回调函数

            if(window.reqLock===false && window.ibeacon_search_failed==false){

                window.reqLock=true;
                var minDistBeacon=getMinDistBeacon(argv);

                $.ajax({url:"http://api.gogoinfo.cn/air/sensorinfo.json",type:'GET',timeout:8000,data:minDistBeacon,success:function(data){
                    window._searched=true;
                    // {"code":1,"msg":"","data":{"ctime":1442633678,"temp":12,"humidity":32,"aqi":123,"actionType":0,"message":""}}
                    if(!('data' in data) || !('temp' in data.data)){
                        var envInfo=data.data;
                        envInfo={'humidity':envInfo.humidity?envInfo.humidity:45,'aqi':(envInfo.aqi?envInfo.aqi:88),temp:(envInfo.temp?envInfo.temp:28)};
                        var date=new Date();
                        envInfo['time']=envInfo['ctime']?(envInfo['ctime']*1000):0;

                        if(envInfo['time']!=0){

                            date.setTime(envInfo['time']);
                        }
                        envInfo['time']=date.format("yyyy-MM-dd hh:mm");

                    } else {
                        var time = new Date().format("yyyy-MM-dd hh:mm");
                        var aqi=88;
                        envInfo={'humidity':45,'aqi':aqi,temp:28,time:time};
                    }
                    envInfo['iaqi']=calcIaqi(envInfo['aqi']);
                    envInfo['pollution']=pollIdx(envInfo['iaqi']);
                    envInfo['width']=Math.round(envInfo['iaqi']/5);


                    //envInfo={'humidity':45,'aqi':88,temp:28,pollution:111,time:"2012-12-12 11:11"};

                    setAqiModal(loadingModal,'当前环境信息',envInfo);
                    wx.stopSearchBeacons({
                        complete:function(res){
                            // alert("stop complete");
                        }
                    });
                },error:function(jqXHR,textStatus,errorThrown){
                    var title='请求错误';
                    var msg='请求错误';
                    if(textStatus=='timeout'){

                        msg='10秒内没有获取到数据,稍后再试';
                    }
                    window.ibeacon_search_failed=true;
                    setTimeoutModal(loadingModal,'请求错误','10秒内没有获取到数据,稍后再试');

                }});
            }


        },fail:function(){

            //alert("failed");
        }

    });


    wx.ready(function(){
        wx.startSearchBeacons({
            ticket:(query && 'ticket' in query)?query.ticket:'',  //摇周边的业务ticket, 系统自动添加在摇出来的页面链接后面

            fail:function(){
                window.ibeacon_search_failed=true;
                setTimeoutModal(loadingModal,'没有查找到信息','请确认摇一摇访问链接,且设备打开');
            }
        });
        evt.preventDefault();
        return false;
    });


};
$(function(){

    window.reqLock=false;
    $("a[href=#envref]").click(envRefReq);
    $("a[href='#wechat-follow']").click(function(evt){

        seajs.use(['http://zb.weixin.qq.com/nearbycgi/addcontact/BeaconAddContactJsBridge.js'],function(){

            BeaconAddContactJsBridge.ready(function(){
                //判断是否关注
                BeaconAddContactJsBridge.invoke('checkAddContactStatus',{} ,function(apiResult){
                    if(apiResult.err_code == 0){
                        var status = apiResult.data;
                        if(status == 1){

                            BeaconAddContactJsBridge.invoke('jumpAddContact');
                        }else{

                            //跳转到关注页
                            BeaconAddContactJsBridge.invoke('jumpAddContact');
                        }
                    }else{
                        //alert(apiResult.err_msg);
                    }
                });
            });

        });
        evt.preventDefault();
        return false;

    });


    var query=location.search.substr(1);
    query=$.unparam(query);
    var section='index';
    if(location.href.match(/[\w-]+\.html/)){
        section=toATag(location.href).pathname.substr(location.href.lastIndexOf("/")+1).split(".")[0];
    }

    var sid=query['sid'];
    config.sid=sid;
    config.ver=query['ver'];


    if(section){
        if(section in config.t_section){
            section=config.t_section[section];
        }
    }
    /**
     * 加入版本号以便CDN使用
     */
    $("body").on('click','a',function(){
        var href=$(this).attr('href');
       if(href.indexOf("?")!=-1){
            $(this).attr("href",href+"&ver="+config.ver);
       } else {
           $(this).attr("href",href+"?ver="+config.ver);
       }

    });

    /**
     * sid 是siteId，站点id
     */
    console.log("query",query);
    if('sid' in query){
        $.post('/temp/?sid='+sid, $.extend(query,{section:section}),function(data){
            console.log("rawdata",data);
            var orgData= $.parseJSON(data);

            console.log("data",orgData);
            tmpData={};
            if(('data' in orgData) && typeof orgData.data=='object' && ('temp' in orgData.data)){
                tmpData=orgData.data.temp;
            }

            if(('data' in orgData) && typeof orgData.data=='object' && ('comps' in orgData.data) && $.isArray(orgData.data.comps)){
                data=orgData.data.comps;

                for(var key in data.fields){
                    if($("[data-edit='"+key+"']").size()>0){
                        if(isImgKey(key)){
                            if($.isArray(data.fields[key])){
                                for(var i=0;i<data.fields[key].length;i++){
                                    $("[data-edit='"+key+"["+i+"]']").attr('src',data.fields[key][i]);
                                }
                            } else {
                                $("[data-edit='"+key+"']").attr('src',data.fields[key]);
                            }

                        } else {
                            $("[data-edit='"+key+"']").html(data.fields[key]);
                        }

                    }  else if($("[data-display='"+key+"']").size()>0){
                        if($.isArray(data.fields[key])){
                            for(var i=0;i<data.fields[key].length;i++){
                                $("[data-display='"+key+"="+data.fields[key][i]+"']").show();
                            }
                        } else {
                            $("[data-display='"+key+"']").show();
                        }

                    }


                }
            }



            $("script[type='text/html']").each(function(idx,item)   {
                console.log('tmpData',tmpData);
                var id=$(item).attr("id").replace(/_t_/,'');
                var data={};
                data[id]={};
                $(item).replaceWith(template(id,$.extend(data,{_c:config},tmpData)));
            });
            if($.isArray(dataReadyCb)){
                for(var i=0;i<dataReadyCb.length;i++){
                    $.isFunction(dataReadyCb[i]) && dataReadyCb[i](orgData);
                }
            } else if($.isFunction(dataReadyCb)){
                dataReadyCb(orgData);
            }
        }).complete(function(data){


            $("#_ry_temp_css").remove();
        });
    }






});
if((typeof sendAuth=='function')){
    $.ajaxSetup({
        beforeSend:sendAuth,
        timeout:10000

    });
    $("body").on('submit',"form.ajax-form",ajaxSubmitHandler);

}

/**
 *  prevent submitting multiple time in short period
 */


$("body").on("click","a[href='#back']",function(){
    history.back();
    return false;
});