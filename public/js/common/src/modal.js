

var openModal=function(jqObj,title,body,cbDict,cssDict,attrDict,txtDict,modalOpts){

    var temp='<div class="modal fade" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> <h4 class="modal-title" id="myModalLabel"><%=title%></h4> </div> <%if(body!="" && !!body==true){%><div class="modal-body"> <%==body%> </div><%}%> <%if(show_footer){%><div class="modal-footer" <%if(body=="" || !!body==false){%>style="border-top:0px"<%}%>> <button type="button" class="btn btn-default cancel-btn" data-dismiss="modal"><%=cancelTxt%></button> <button type="button" class="btn btn-primary ok-btn"><%=okTxt%></button> </div><%}%> </div> </div> </div>';
    var nullCb=function(){};
    var cbs= $.extend({ok:nullCb,cancel:nullCb,show:nullCb,hide:nullCb,shown:nullCb,hidden:nullCb},cbDict);
    var id='m_'+uniqueId();
    var attr=$.extend({id:id},(attrDict || {}));
    var txtDict=txtDict || {};
    var css=cssDict || {};
    var modalOpts=modalOpts || {'show_footer':false};

    var oldModal=$("#"+attr['id']);
    if(oldModal.size()>0){
        oldModal.modal('toggle');
    }
    else {
        var modal=$(template.compile(temp)($.extend({title:title,body:body},{show_footer:modalOpts['show_footer'],cancelTxt:config.lang.cancel,okTxt:config.lang.ok},txtDict)));

        modal.css(css);
        modal.attr(attr);
        modal.find(".ok-btn").on('click',function(){
            cbs['ok'](id);
            modal.modal('toggle');
        });
        modal.find(".cancel-btn").on('click',function(){
            cbs['cancel'](id);
            modal.modal('toggle');
        });
        modal.on('shown.bs.modal',function(){
            cbs['shown'](this,id);

        });
        modal.on('show.bs.modal',function(){
            cbs['show'](this,id);
        });
        modal.on('hidden.bs.modal',function(){
            cbs['hidden'](this,id);
        });
        modal.modal($.extend({backdrop:true},modalOpts));
    }



}
var openPlainModal=function(jqObj,title,body,modalId,shown){

    openModal(jqObj,title,body,{'show':function(obj,id){
        console.log("hide");

    },'shown':function(obj,id){
        $("#"+id).find('.modal-footer').hide();
        shown(id);
    }},{},{'id':modalId},{},{'show_footer':false});

}
var loginTimeoutModal=function(time){
    (function(){
        var logoutInt=setInterval(function checkLogin(){
            $.post("/user/checklogin",{uid:config.user.uid},function(data){
                var data=$.parseJSON(data);
                if(data.data.status!=1){
                    clearInterval(logoutInt);
                    openModal($("body"),config.lang.login_timeout,config.lang.op_timeout,{ok:function(id){
                        location.href="/user/logout";
                    },cancel:function(id){
                        window.close();
                    },show:function(obj){
                        $(obj).find("button.close,button.cancel-btn").hide();
                    }},{},{id:"logout-modal"}, {cancelTxt:config.lang.iquit, okTxt:config.lang.relogin},{show_footer:true,backdrop:'static',keyboard:false});
                }
            });

        },time);
    }());


};
