
var addFileShare=function(msg){
    console.log("addFileShare");
    if(msg.get('subtype')=='file' && msg.get("fileurl")){
        console.log("file msg",msg);
        var  filetype=(msg.get("filename")?msg.get("filename").split(".").pop():'');

        var ext=msg.get("fileurl")?((msg.get("fileurl").indexOf("?")!=-1)?("&ext="+filetype):("?ext="+filetype)):"unknown";
        var  share='<li id="service_<%=uuid%>"><a href="#<%=fileurl%>" onclick="return viewDoc(this,\'<%=filetype%>\');return false;"><div style="display:block"><img src="/img/icons/filetype/<%=filetype%>.128_128.png" alt="" ></div><span class="icon-text"><%=filename%></span></a></li>';

        var shareHtml=template.compile(share)({fileurl:msg.get("fileurl"),filetype:filetype,filename:msg.get("filename"),uuid:msg.get("uuid")});

        $("#docs").prepend(shareHtml);
        console.log("addFileShare",msg);

    }


}




var viewDoc=function(linkObj,ext){
    var ext2='';
    if(ext){

        ext2="&ext="+ext;
    }

    var url = $(linkObj).attr("href").substr(1);
    var urls = url.split(".");

    var ext = urls[urls.length - 1];
    var name = $(linkObj).text();
    var url_hash = url.hashCode();

    var oldIframe = $("iframe[hash=" + url_hash + "]");
    if (oldIframe.size() > 0) {
        $("#cur-doc-sec>iframe").hide();

        oldIframe.show();
        $("#cur-doc-tab").trigger('click');
    }
    else {
        var googleUrl = ["doc", "docx", "ppt", "pptx", "xls", "xlsx"];
        var googleViewerUrl = "/viewer?url=";
        if (googleUrl.indexOf(ext) != -1) {
            var iframe = createIframe({name: name, hash: url_hash, src: (googleViewerUrl + encodeURIComponent(url))});

        }
        else {
            var viewUrl = "/docs/view?url=";
            var iframe = createIframe({name: name, hash: url_hash, src: (viewUrl + encodeURIComponent(url)+ext2)});


        }

        $("#cur-doc-sec>iframe").hide();
        $("#cur-doc-sec").append(iframe.show());
        $("#cur-doc-tab").trigger('click');
    }
    return false;

}

var acceptNudge=function(){

    $("body").shake({left:-15})

}

/**
 *
 * @param notify
 * @param callback
 */
var condNewGroup=function(notify,callback){
    var gids=/g_\w+/.exec(notify.gid);
    if(gids){
        var discuss=discussList.findWhere({id:gids[1]});
        if(typeof discuss=='undefined'){

            var data=$.parseJSON(data);
            discussList.add(data);
            callback(group);
        }

    }
    else {
        callback(group);
    }
}
var changeActionState=function(type,en){

    var en=en || true;
    if(en!=true)
    {
        if(type=='screen')
        {
            $("#zoom-screen").hide();
            $("#share-screen-ban").hide();
        }
        else if(type=='video'){
            $("#toggle-video").hide();
            $("#chat-video-ban").hide();

        }
    }
    else
    {
        if(type=='screen')
        {
            $("#zoom-screen").show();
            $("#share-screen-ban").show();
        }
        else if(type=='video'){
            $("#toggle-video").show();
            $("#chat-video-ban").show();

        }
    }

}