var regHideNavBarEvent=function(){

    window.addEventListener("load",function() {
        // Set a timeout...
        setTimeout(function(){
            // Hide the address bar!
            window.scrollTo(0, 1);
        }, 0);
    });
    window.onorientationchange = function() {
        setTimeout(function(){
            window.scrollTo(0, 1);
        }, 0);
    }
}