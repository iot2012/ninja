var addQuery=function(href,json){
    var arr=[];
    var query=json;

    if(typeof query!='string'){
        for(var key in json){
            arr.push((key+"="+json[key]));
        }
    }
    query=arr.join("&");
    if(href.match(/[^\?]\?[^\?]+/)){
        href+="&"+query;
    } else {

        href+=(href[[href.length-1]]=="/")?("?"+query):("/?"+query);

    }
    return href;

}
var getAbsUrl=function(reqUri,protocol){
    var protocol=protocol || location.protocol;

    return location.protocol+"//"+location.host+reqUri;
}

function parseURL(url) {
    var a =  document.createElement('a');
    a.href = url;
    return {
        source: url,
        protocol: a.protocol.replace(':',''),
        host: a.hostname,
        port: a.port,
        query: a.search,
        params: (function(){
            var ret = {},
                seg = a.search.replace(/^\?/,'').split('&'),
                len = seg.length, i = 0, s;
            for (;i<len;i++) {
                if (!seg[i]) { continue; }
                s = seg[i].split('=');
                ret[s[0]] = s[1];
            }
            return ret;
        })(),
        file: (a.pathname.match(/\/([^\/?#]+)$/i) || [,''])[1],
        hash: a.hash.replace('#',''),
        path: a.pathname.replace(/^([^\/])/,'/$1'),
        relative: (a.href.match(/tps?:\/\/[^\/]+(.+)/) || [,''])[1],
        segments: a.pathname.replace(/^\//,'').split('/')
    };
};
