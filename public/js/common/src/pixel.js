/**
 * get pixel color from an image
 * @param img image src
 * @param x   x coordinate of point
 * @param y   y coordinate of point
 * @param cb  call back when got the pixel color
 */
var getPixelColor = function (img, x, y, cb) {
    var isObj = false;
    if (img.length > 1024) {
        isObj = true;
    }
    var canvas = $("<canvas></canvas>")[0];
    var ctx = canvas.getContext("2d");
    var imgObj = $("<img>");
    imgObj[0].src = img;
    var image = imgObj[0];

    imgObj.on('load', function () {
        var dimension = 380; // Deep dimensions reasonable.
        var dw;
        var dh;
        console.log("image width", image.width, image.height);
        // set max dimension
        if ((image.width > dimension) || (image.height > dimension)) {
            if (image.width > image.height) {
                // scale width to fit, adjust height
                dw = parseInt(image.width * (dimension / image.width));
                dh = parseInt(image.height * (dimension / image.width));
            } else {
                // scale height to fit, adjust width
                dh = parseInt(image.height * (dimension / image.height))
                dw = parseInt(image.width * (dimension / image.height));
            }
            canvas.width = dw;
            canvas.height = dh;
        }

        ctx.drawImage(image, 0, 0, canvas.width, canvas.height);

        var imagedata = ctx.getImageData(x, y, 1, 1);

        //  get pixelArray from imagedata object
        var data = imagedata.data;
        cb(getHex(data));
        //  calculate offset into array for pixel at mouseX/mouseY

        //  get RGBA values

    });


};