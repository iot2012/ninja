
var showCity = function (countryNode, stateNode, cityNode, state) {

    var cityNode = cityNode || $("#pro_city");
    var cityArr = [];
    var state = stateNode.val();
    var country = countryNode.val();
    if (state == '' || state == null) {

    }
    else {
        _.each(gLocation[country][state], function (val, key) {
            if (key != 'n') {
                cityArr.push($("<option>").val(key).text(val.n));
            }

        });

    }
    cityNode.html('');
    if (cityNode.length > 0) {
        cityNode.append(cityArr);
    }

}

var showCountryState = function (stateNode, cityNode, country, state, city) {

    /**
     * contry: select contry value
     * state: select state value
     * city: select city value
     *
     */
    var stateNode = stateNode || $("#pro_state");
    var cityNode = cityNode || $("#pro_city");

    var country = country || '';
    var state = state || '';
    var city = city || '';

    var cityArr = [];
    var stateArr = [];
    var cnt = 0;

    var val1 = gLocation[country];

    if (val1[0]) {
        if (typeof val1[0][0] == 'undefined') {

            _.each(val1[0], function (v, k) {
                if (k != 'n' && k != '-1') {
                    if (k == city) {
                        cityArr.push($("<option selected>").val(k).text(v.n));
                    }
                    else {
                        cityArr.push($("<option>").val(k).text(v.n));
                    }

                }

            });
        }


    }
    else {
        _.each(val1, function (val2, key2) {
            if (key2 != 'n' && key2 != '-1') {
                if (key2 == state) {
                    stateArr.push($("<option selected>").val(key2).text(val2.n));
                }
                else {
                    stateArr.push($("<option>").val(key2).text(val2.n));
                }


                if (cnt == 0) {
                    _.each(val2, function (val3, key3) {
                        if (key3 != 'n' && key3 != '-1') {
                            if (key3 == city) {
                                cityArr.push($("<option selected>").val(key3).text(val3.n));
                            }
                            else {
                                cityArr.push($("<option>").val(key3).text(val3.n));
                            }


                        }
                    });
                    cnt++;
                }

            }


        });

    }
    stateNode.html('');
    if (stateArr.length > 0) {
        stateNode.append(stateArr);
    }
    cityNode.html('');
    if (cityArr.length > 0) {
        cityNode.append(cityArr);
    }

}
var initLocation = function (countryNode, stateNode, cityNode, selCountry, selState, selCity) {

    var countryArr = [];

    var countryNode = countryNode || $("#pro_country");
    var cityNode = cityNode || $("#pro_city");
    var stateNode = stateNode || $("#pro_state");

    var selCountry = selCountry || 1;
    var selState = selState || '';
    var selCity = selCity || '';


    _.each(gLocation, function (val, key) {
        if (key == selCountry) {
            countryArr.push($("<option selected>").val(key).text(val.n));

        }
        else {
            countryArr.push($("<option>").val(key).text(val.n));

        }

    });


    countryNode.append(countryArr);

    showCountryState(stateNode, cityNode, selCountry, selState, selCity);

    countryNode.on('change', function () {
        var country = countryNode.val();
        showCountryState(stateNode, cityNode, country);

    });
    stateNode.on('change', function () {
        var state = stateNode.val();
        showCity(countryNode, stateNode, cityNode, state);


    });


}



