

var saveCssData=function(){
    /**
     * selector is keyword
     */
    var selectors=arguments[0];
    if(typeof selectors==='string'){

        selectors=selectors.split(/,/);
    }
    if($.isArray(selectors) && arguments.length==2){
        var len=selectors.length;
        for(var i=0;i<len;i++){
            var name=selectors[i].replace(/[\[\]\s\-\_\.]/g,'');
            var cssNames=arguments[1];
            if(typeof cssNames=='string'){
                cssNames=cssNames.split(/\s/);

            }
            $(document).data(("css-"+name),$(selectors[i]).css(cssNames));

        }

    }

    else if($.isPlainObject(selectors)){

        for(var cssSelector in selectors){
            console.log(cssSelector);

            if(cssSelector.indexOf(",")!=-1){

                var allSels=cssSelector.split(/,/);
                for(var j=0;j<allSels.length;j++){

                    var name=allSels[j].replace(/[\[\]\s\.]/g,'');

                    var cssNames=selectors[cssSelector];
                    if(typeof cssNames=='string'){

                        cssNames=cssNames.split(/\s+/);
                    }

                    $(document).data(("css-"+name),$(allSels[j]).css(cssNames));
                }




            }



        }

    }


}



var restoreCssData=function(){


    var selectors=arguments[0];
    if(typeof selectors==='string'){

        selectors=selectors.split(/,/);
    }
    if($.isArray(selectors)){
        var len=selectors.length;
        for(var i=0;i<len;i++){
            var name=selectors[i].replace(/[\[\]\s\-\_\.]/g,'');
            $(selectors[i]).css($(document).data(("css-"+name)));

        }

    }

    else if($.isPlainObject(selectors)){
        for(var cssSelector in selectors){
            if(cssSelector.indexOf(",")!=-1){

                var allSels=cssSelector.split(/,/);
                for(var j=0;j<allSels.length;j++){

                    var name=allSels[j].replace(/[\[\]\s\.]/g,'');

                    $(allSels[j]).css($(document).data(("css-"+name)));

                }

            }

        }

    }
}

