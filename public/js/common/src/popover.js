var setUpRmPopover=function(jqObj,title,popOverOpts,cbDict,css){
    var defaultOpts={html: true, trigger: 'click', placement: 'left',container:"body",isDestroy:true};
    var popOverOpts=$.extend(defaultOpts,popOverOpts);


    jqObj.attr({"data-content": "<div class='alert-warning setup-popover'><button class='btn btn-default bt-cancel'>" + config.lang.cancel + "</button><button class='bt-ok btn btn-danger pull-right'>" + config.lang.ok + "</button></div>", 'data-original-title': title});
    var nullCb=function(){};
    var cbs= $.extend({ok:nullCb,cancel:nullCb,show:nullCb,hide:nullCb,shown:nullCb,hidden:nullCb},cbDict);


    jqObj.popover(popOverOpts).on("shown.bs.popover",function (e) {
        var self=this;
        var  popOver=$('#'+$(this).attr("aria-describedby"));

        popOver.find(".bt-ok").on('click', function (e) {
            e.preventDefault();
            if(popOverOpts['isDestroy']){
                $(self).popover('destroy');
            } else {
                $(self).popover('hide');
            }

            cbs['ok'](popOver,self);

            return false;


        });
        popOver.find('.popover-title').css({'color': 'black'});

        popOver.find(".bt-cancel").on('click', function (e) {
            e.preventDefault();
            cbs['cancel'](popOver,self);
            $(self).popover('hide');

            return false;



        });


    }).on('hidden.bs.popover', cbs['hidden']).on('show.bs.popover',cbs['show']).on('hide.bs.popover',cbs['hide']);

    // return jqObj;
}


var createPopover=function(jqObj,title,html,popOverOpts,cbDict,css){
    var defaultOpts={html: true, trigger: 'click', placement: 'left',closable:true};
    var popOverOpts= $.extend(defaultOpts,popOverOpts);
    jqObj.attr({"data-content": html, 'data-original-title': title});
    var nullCb=function(){};
    var css=css || {};
    var cbs= $.extend({ok:nullCb,cancel:nullCb,show:nullCb,hide:nullCb,shown:nullCb,hidden:nullCb},cbDict);
    if(defaultOpts.closable){
        var old=cbs['shown'];
        cbs['shown']=function(self){
            old(self);
            self.css(css);
            var titleEle=self.find(".popover-title");
            var closeDiv=$('<span class="fa fa-times pull-right"></span>');
            closeDiv.on('click',function(){
                jqObj.popover('hide');
            }).on('mouseover',function(e){$(this).css({'cursor':'pointer'});}).on('mouseout',function(e){$(this).css({'cursor':'pointer'})});
            if(titleEle.find(".fa-times").size()==0){
                titleEle.append(closeDiv);
            }


        }
    }



    jqObj.popover(popOverOpts).on("shown.bs.popover",function () {

        var self=$(this);
        var popObj=jqObj.next();
        if(popOverOpts.container){

            popObj=$('#'+self.attr("aria-describedby"));
        }
        cbs['shown'](popObj);

        popObj.find(".bt-ok").on('click', function () {
            cbs['ok']();
            popObj.remove();

        });
        popObj.find('.popover-title').css({'color': 'black'});

        popObj.find(".bt-cancel").on('click', function () {
            cbs['cancel']();
            jqObj.popover('hide');

        });
    }).on('hidden.bs.popover', cbs['hidden']).on('show.bs.popover',cbs['show']).on('hide.bs.popover',cbs['hide']);

    return jqObj;
}