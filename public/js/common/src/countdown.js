
var setCountDown=function(countdown,timestamp,doneCallback){

    var doneCallback=doneCallback||function(){console.log("countdown finished");};
    var target_date = timestamp;

    var days, hours, minutes, seconds;

// get tag element

    var countDownInt=setInterval(function () {

        // find the amount of "seconds" between now and target
        var current_date = new Date().getTime();
        var seconds_left = (target_date - current_date) / 1000;

        // do some time calculations
        days = parseInt(seconds_left / 86400);
        seconds_left = seconds_left % 86400;

        hours = parseInt(seconds_left / 3600);
        seconds_left = seconds_left % 3600;

        minutes = parseInt(seconds_left / 60);
        seconds = parseInt(seconds_left % 60);
        countdown.innerHTML = '<span class="days">' + days +  ' <b>'+config.lang.days+'</b></span> <span class="hours">' + hours + ' <b>'+config.lang.hours+'</b></span> <span class="minutes">'
        + minutes + ' <b>'+config.lang.minutes+'</b></span> <span class="seconds">' + seconds + ' <b>'+config.lang.seconds+'</b></span>';

        if((days==0 && hours==0 && seconds==0 && minutes==0) || seconds<0){
            clearInterval(countDownInt);
            doneCallback();
        }
    }, 1000);
}