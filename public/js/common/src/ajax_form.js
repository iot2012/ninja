var ajaxSubmitHandler=function(e){

    ajaxSubmit($(this));

    e.preventDefault();
    return false;

};
var ajaxSubmit=function(jqForm,jqSubbtn,loadingImg){
    var that=jqForm;
    var formId=jqForm[0].id;

    var subBtn=jqSubbtn || jqForm.find("[type=submit]");

    var loadingImg=loadingImg || $(".loading-icon").clone();

    $(document).ajaxStart(function(){
        var oW=subBtn.outerWidth();
        var oH=subBtn.outerHeight();
        if(!subBtn.prev().hasClass("loading-icon"))
        {
            var btnOffset=subBtn.offset();
            loadingImg.css({position:"absolute"});
            subBtn.before(loadingImg);
            subBtn.prop("disabled",true);
            loadingImg.show();

            loadingImg.offset({left:(btnOffset.left+(oW-16)/2),top:(btnOffset.top+(oH-16)/2)});

        }
        else {
            loadingImg.show();
        }

    }).ajaxComplete(function(){
        loadingImg.hide();
        setTimeout(function(){subBtn.prop("disabled",false);},1000);

    });
    if(ajaxCheck(that))
    {
        ajaxPost(that);
        return false;
    }
    return false;

}


var ajaxPost=function(jqForm){
    var postData=jqForm.serialize();
    var upload=jqForm.find(":file");
    var prefix=(location.protocol+"//"+location.host);
    var doPost=function(extra){
        var postData=jqForm.serialize();
        if($.isPlainObject(extra)){
            extra=$.param(extra);
            postData=$.extend(postData,extra);
        }
        /**
         * jqForm[0].action和jqForm.attr("action")区别是有些浏览器里定义了action的字段名会被取到导致不正确
         */
        $.post(jqForm.attr("action"),jqForm.serialize(),function(data){
            var ret={code:-1,msg:config.lang.req_failed,data:{}};
            if(ival.json.test(data)){
                ret=$.parseJSON(data);
                var errDiv = jqForm.find(".return-error");
                var sucDiv = jqForm.find(".return-suc");
                if(errDiv.size()==0){
                    jqForm.prepend('<div class="return-error"></div>');

                }
                if(sucDiv.size()==0){
                    jqForm.prepend(' <div class="return-suc"></div>');
                }
                /**
                 *  get registed callback
                 *  ajaxCbs[formId]=[sucCallback,failCallback]
                 */

            }
            var formId=jqForm[0].id;
            ajaxCbs[formId+'-data']=postData;
            ajaxCbs[formId]=[(ajaxCbs[formId] && ajaxCbs[formId][0])?ajaxCbs[formId][0]:function(){}, (ajaxCbs[formId] && ajaxCbs[formId][1])?ajaxCbs[formId][1]:function(){}];
            processRet(ret,sucDiv,errDiv,ajaxCbs[formId][0],ajaxCbs[formId][1],3000,jqForm);

        });
    };
    if(upload.size()>0){

        var uploadWorker = new Worker((prefix+'/js/uploadworker.js'));

        /**
         * get msg mid
         */
        uploadWorker.onmessage=function (e) {
            console.log(e);
            var ret = e.data;
            if (ret.code == 1) {
                doPost(ret.data);
            }
        };
        uploadWorker.postMessage({url: (prefix+'/index/media/upload/' + file.name + ".json"), data: file});
    } else {
        doPost();
    }

}


var sendAuth=function(xhr,setting){
    /**
     * 定义后台交互超时
     * @type {{}}
     */
    //if(typeof loginTimeoutInt!='undefined'){
    //    clearTimeout(loginTimeoutInt);
    //    loginTimeoutModal(parseInt(config.setting.login_timeout)*1000);
    //}

    var errArr={};
    var valdata='';
    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    if(this.type=='POST' || this.type=='PUT')
    {
        if(typeof this.contentType!='undefined' && !_.isEmpty(this.contentType) && this.contentType.indexOf('application/json')!=-1){

            if(typeof this.data=='undefined'){

                this.data={};
            }
            else
            {
                this.data=$.parseJSON(this.data);
            }
            console.log("json data",this.data);
            this.data['ctoken']=config.ctoken;
            console.log("json data",this.data);
            this.data=$.toJSON(this.data);
            return true;
        }
        if(typeof this.data==='undefined'){
            if(!_.isEmpty(this.contentType) && this.contentType.indexOf('application/json')!=-1)
            {
                this.data={};
                this.data['ctoken']=config.ctoken;
                this.data=$.toJSON(this.data);
            }
            else {
                this.data='ctoken='+encodeURIComponent(config.ctoken);
            }

            return true;
        }
        if(typeof this.data=="string")
        {

            valdata= $.unparam(this.data);
            if(this.data=='')
            {
                this.data='ctoken='+encodeURIComponent(config.ctoken);
            }
            else
            {
                this.data+='&ctoken='+encodeURIComponent(config.ctoken);
            }

        }
        if($.isPlainObject(valdata))
        {

            for(var key in valdata)
            {

                var input=$(":input[name="+key+"]");
                if(input.size()>1){
                    var input=$(":input[name="+key+"]:visible");
                }
                input.removeClass('input-err');
                input.tooltip('destroy');
                if(input.size()>0)
                {
                    var res=checkOneInput(input);
                    console.log(res);
                    if(res[0]!=true)
                    {
                        var res2={};
                        errArr[key]=res[1];

                    }
                }

            }

            if(!$.isEmptyObject(errArr))
            {
                console.log("errarr",errArr);
                _.each(errArr,function(title,name){

                    var obj=$(":input[name="+name+"]");
                    if(obj.hasClass("input-err"))
                    {
                        obj.tooltip('destroy');
                        obj.tooltip({

                            title:title,
                            trigger:'click'
                        });

                    }
                    else
                    {
                        obj.addClass("input-err");

                        obj.tooltip({

                            title:title,
                            trigger:'click'
                        });

                    }

                });
                return false;
            }
            return true;
        }
    }

    return true;

}


var processRet=function(ret,suc,fail,succb,failcb,tm,jqForm){

    var succb=succb || function(){};
    var failcb=failcb || function(){};
    var tm=tm||2500;
    if(ret.code==1)
    {
        succb(ret.data);
        suc.text(ret.msg).show('fast',function(){
            setTimeout(function(){suc.hide();},tm);
        });
    }
    else
    {
        failcb(ret.data);

        if(jqForm){
            var allInputs=jqForm.find(":input");
            allInputs.removeClass("input-err");

            for(var field in ret.data){
                var input=jqForm.find(":input[name="+field.replace(/^t_/,"")+"]");
                if(input.size()>0){
                    input.addClass("input-err");
                    showTips(input,ret.data[field],{duration:10000});
                }

            }
        }
        fail.text(ret.msg).show('fast',function(){
            setTimeout(function(){fail.hide();},tm);
        });
    }

}