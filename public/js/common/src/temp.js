var z=function(){
    var getHtmlFrag=function(orgId,cb){
        var orgUrl="/"+config.moduleName+"/js/"+config.tempdir+"/temp/"+orgId;
        if(orgId.indexOf("/")!=-1){
            orgUrl=orgId;
            orgId=orgId.match(/[^\/]*\/([^\/]+)$/)[1];
        }
        var id=orgId.replace(/[\/\.]/g,'_');
        var tempId='temp_'+id;
        var htmlData=$(document).data(tempId);
        if(htmlData){

            cb(htmlData);

        }
        else
        {

            $.get(orgUrl).done(
                function(data){
                    $(document).data(tempId,data);
                    cb(data);
                }


            );

        }
    }
    return {
        getHtmlFrag:getHtmlFrag

    }

};