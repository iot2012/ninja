var reloginQuit=function(time){

    if($("#logout-modal").size()==0){
        openModal($("body"),config.lang.offline_alert,config.lang.another_login,{ok:function(id){
            location.href="/user/logout";
        },cancel:function(id){
            window.close();
        },show:function(obj){
            $(obj).find("button.close,button.cancel-btn").hide();
        }},{},{id:"logout-modal"}, {cancelTxt:config.lang.iquit, okTxt:config.lang.relogin},{'show_footer':true,backdrop:'static',keyboard:false});
    } else {
        $("#logout-modal").modal('show');
    }
};



var doLogin=function(){


    $.post("/user/login",{username:$("input[name=username]").val(),password:$("input[name=password]").val(),remember:$("input[name=remember]").val()},function(data){

        try{

            var data= $.parseJSON(data);
            if(data.code==1)
            {
                location.href="/index/index";
                return 0;
            }
            else
            {
                showTips($("#loginBtn"),data.msg,{duration:5000});
                $("#loginBtn").prop("disabled",false);
            }

        }

        catch(ex)
        {
            $("#loginBtn").prop("disabled",false);
        }

    });
};



