
var showTips = function (self, msg,options) {
    var opt={
        isshow:true,
        trigger:'manual',
        place:'bottom',
        duration:2000,
        container:'body'
    };



    var orgTitle=self.attr("data-original-title");
    self.tooltip("destroy");
    self.attr({"data-original-title":msg});
    opt= $.extend(opt,options);
    var args= $.extend(args,arguments);
    self.tooltip({title: msg, placement: opt.place, trigger: opt.trigger}).on('shown.bs.tooltip', function () {

        _.delay(function () {
            self.tooltip('destroy');
        }, opt.duration);

    });
    if(opt.isshow) {self.tooltip('show');}
    return false;
}