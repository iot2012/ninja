
var doLockScreen=function(){
    var obj=$("#lockScreenMask").css({width:"100%","height":"100%","z-index":"9999"});


    var visibleEs=$("body").children(":visible");
    $(document).data("visible",visibleEs);
    $("body").children(":visible").hide();
    obj.load("user/lockScreen",function(){


        $("#signInForm input[name=username]").val(config.user.username);
        $("#lockScreenMask").show();


        obj.find(".login-img").css({'background-image':"url("+config.user.icon_url+")"});
        obj.find("#loginBtn").on('click',function(evt){
            $.post("user/login",{username:$("input[name=username]").val(),password:$("input[name=password]").val()},function(data){

                try{
                    console.log(data);
                    var data= $.parseJSON(data);
                    console.log(data);
                    if(data.code=='1')
                    {

                        $("#lockScreenMask").hide();
                        console.log(visibleEs);
                        $(document).data("visible").show();
                        return 0;
                    }
                    else
                    {
                        console.log(data.msg);
                    }

                }

                catch(ex)
                {


                }

            });
            console.log("clicked")
            return false;

        });

    });
    obj.fadeIn();
}


var showCity = function (countryNode, stateNode, cityNode, state) {

    var cityNode = cityNode || $("#pro_city");
    var cityArr = [];
    var state = stateNode.val();
    var country = countryNode.val();
    if (state == '' || state == null) {

    }
    else {
        _.each(gLocation[country][state], function (val, key) {
            if (key != 'n') {
                cityArr.push($("<option>").val(key).text(val.n));
            }

        });

    }
    cityNode.html('');
    if (cityNode.length > 0) {
        cityNode.append(cityArr);
    }

}

var showCountryState = function (stateNode, cityNode, country, state, city) {

    /**
     * contry: select contry value
     * state: select state value
     * city: select city value
     *
     */
    var stateNode = stateNode || $("#pro_state");
    var cityNode = cityNode || $("#pro_city");

    var country = country || '';
    var state = state || '';
    var city = city || '';

    var cityArr = [];
    var stateArr = [];
    var cnt = 0;

    var val1 = gLocation[country];

    if (val1[0]) {
        if (typeof val1[0][0] == 'undefined') {

            _.each(val1[0], function (v, k) {
                if (k != 'n' && k != '-1') {
                    if (k == city) {
                        cityArr.push($("<option selected>").val(k).text(v.n));
                    }
                    else {
                        cityArr.push($("<option>").val(k).text(v.n));
                    }

                }

            });
        }


    }
    else {
        _.each(val1, function (val2, key2) {
            if (key2 != 'n' && key2 != '-1') {
                if (key2 == state) {
                    stateArr.push($("<option selected>").val(key2).text(val2.n));
                }
                else {
                    stateArr.push($("<option>").val(key2).text(val2.n));
                }


                if (cnt == 0) {
                    _.each(val2, function (val3, key3) {
                        if (key3 != 'n' && key3 != '-1') {
                            if (key3 == city) {
                                cityArr.push($("<option selected>").val(key3).text(val3.n));
                            }
                            else {
                                cityArr.push($("<option>").val(key3).text(val3.n));
                            }


                        }
                    });
                    cnt++;
                }

            }


        });

    }
    stateNode.html('');
    if (stateArr.length > 0) {
        stateNode.append(stateArr);
    }
    cityNode.html('');
    if (cityArr.length > 0) {
        cityNode.append(cityArr);
    }

}
var initLocation = function (countryNode, stateNode, cityNode, selCountry, selState, selCity) {

    var countryArr = [];

    var countryNode = countryNode || $("#pro_country");
    var cityNode = cityNode || $("#pro_city");
    var stateNode = stateNode || $("#pro_state");

    var selCountry = selCountry || 1;
    var selState = selState || '';
    var selCity = selCity || '';


    _.each(gLocation, function (val, key) {
        if (key == selCountry) {
            countryArr.push($("<option selected>").val(key).text(val.n));

        }
        else {
            countryArr.push($("<option>").val(key).text(val.n));

        }

    });


    countryNode.append(countryArr);

    showCountryState(stateNode, cityNode, selCountry, selState, selCity);

    countryNode.on('change', function () {
        var country = countryNode.val();
        showCountryState(stateNode, cityNode, country);

    });
    stateNode.on('change', function () {
        var state = stateNode.val();
        showCity(countryNode, stateNode, cityNode, state);


    });


}




var GeneralSetting={

    topDivId:'',
    init:function(topDivId){
        this.topDivId=topDivId;
        this.inputDataInit(config.setting);
        $("#"+topDivId+' .slider-button').on('click', this.enAudVidClicked);
        $(":input[name=en_desknotify]").on('click',this.desktopNotify);
        $(":input[name=en_camlogin]").on('click',this.camLogin);
    },
    inputDataInit:function(configSetting){
        var that=this;
        if (configSetting != {}) {
            _.each(configSetting, function (val, key) {
                console.log("setting here");
                var input = $("#"+that.topDivId+" :input[name=" + key + "]");

                if (input.size() > 0) {
                    if(key != 'enablesound'){
                        if(input[0].tagName.toLowerCase()==="input" && ['checkbox','radio'].indexOf(input[0].type)!=-1){
                            $("#"+that.topDivId+" :input[name=" + key + "][value='" + val + "']").prop("checked", true);
                        }
                        else {
                            input.val(val);
                        }

                    }

                    else {
                        if (key == 'enablesound'){
                            if(val == '1') {
                                input.parent().parent().find(".slider-frame>.slider-button").addClass('on');

                            }
                            $("#"+that.topDivId+" :input[name=" + key + "]").val(val);
                        }

                    }

                }




            });

        }
    },
    enAudVidClicked:function (e) {
        var parentNode = $(this).parent().parent();
        var input = parentNode.find("input");
        if ($(this).hasClass("on")) {
            input.val('0');
            $(this).removeClass("on").html("&bull;").css({"font-size": "5em"});
        } else {
            input.val('1');
            $(this).css({"font-size": "1em"}).addClass("on").html("|");
        }

    },
    desktopNotify:function(e){

        var that=this;
        if(notify.permissionLevel()==notify.PERMISSION_DEFAULT){
            notify.requestPermission(function(data){

                if(notify.permissionLevel()==notify.PERMISSION_GRANTED){

                    $(that).prop("checked",true);
                    e.preventDefault();
                    return false;

                }
                else {
                    $(that).prop("checked",false);
                    console.log("denieyd,default");
                }

            });

        }

    },
    camLogin:function(e){
        if($(this).prop("checked"))
        {
            seajs.use(['ht.js'],function(){
                var video=$("#cam-video");
                video.show();
                var htracker = new headtrackr.Tracker({});
                var canvasInput=$("#cam-canvas")[0];
                var videoInput=video[0];
                htracker.init(videoInput, canvasInput);
                var errDiv = $("#general-form").parent().find(".return-error");
                var sucDiv = $("#general-form").parent().find(".return-suc");
                htracker.start();
                var cnt=1;
                document.addEventListener('headtrackrStatus',
                    function (event) {
                        console.log(event.status);
                        if (event.status == "getUserMedia") {

                        }
                        else if (event.status == "found" && cnt==1) {
                            cnt++;
                            var canvas = $("<canvas>")[0];

                            canvasInput.getContext('2d').drawImage(videoInput, 0, 0, 240, 240);
                            var data = canvasInput.toDataURL('image/png');


                            canvasInput.toBlob(function (blob) {

                                var formData = new FormData();
                                formData.append('ctoken', config.ctoken);
                                formData.append('face',1);
                                formData.append("avatar", blob);
                                $.ajax({
                                    url: "/user/facedetect",
                                    type: "POST",
                                    data: formData,
                                    processData: false,  // 告诉jQuery不要去处理发送的数据
                                    contentType: false,   // 告诉jQuery不要去设置Content-Type请求头
                                    global: false,
                                    success: function (data) {

                                        var ret = $.parseJSON(data);
                                        if (ret.code == '1') {
                                            sucDiv.text(ret.msg).show();
                                            setTimeout(function () {
                                                sucDiv.hide()
                                            }, 2500);
                                            videoInput.pause();
                                            video.hide();
                                        }
                                        else {
                                            errDiv.text(ret.msg).show();
                                            setTimeout(function () {
                                                errDiv.hide()
                                            }, 2500);
                                        }


                                    }
                                });


                            }, "image/png");

                            console.log("FOV",htracker.getFOV());
                        }
                    }
                );
            });
        }




    }



};
var ProfileSetting={
    avatarTabClicked:function (e) {
        Holder.run({

            themes: {
                "simple": {
                    background: "green",
                    foreground: "green",
                    size: 20
                }
            },
            images: ".img-holder"
        });
    },
    mfaClicked:function(e){
        var self=this;
        seajs.use(["jquery.qx.js"],function(data){

            if($(self).prop('checked')==true){
                $("#qr_mfa_div").slideDown();
                $.post("/userprofile/getFactorCodeUri").done(function(data){
                    data= $.parseJSON(data);
                    data=data.data;
                    $("#qr_mfa").qrcode({
                        text:data.url
                    });



                })

            }
            else {
                $("#qr_mfa_div").slideUp();
                $("#qr_mfa canvas").remove();
            }


        });

    },
    capPicTabShown:function () {

        if ($(this)[0].id == 'ava-arch-tab') {
            var errDiv = $("#ava-arch-sec").find(".return-error");
            var sucDiv = $("#ava-arch-sec").find(".return-suc");
            var lis = '';
            $.get("/user/getsysicon", function (data) {


                if ($("#ava-arch-sec").find("li").size() == 0) {
                    var icons = $.parseJSON(data);

                    for (var i = 0, len = icons.length; i < len; i++) {
                        lis += "<li><a href='#'><img src='/" + icons[i] + "' width='40' height='40'></a></li>";

                    }
                    var ul = '<button class="btn btn-primary" id="update-avatar">Update</button><ul class="nav nav-pills">' + lis + '</ul>';

                    var ulNode = $(ul);
                    var aTags = ulNode.find("a");
                    var btn = $(ulNode[0]);
                    aTags.on('click', function (e) {

                        siteUser.set('icon_url',$(this).find("img").attr("src"));


                        e.preventDefault();
                        return false;


                    });
                    btn.on('click', function () {
                        $.post("/user/updateavatar", {sys: 1, src: $("#user-icon>img").attr('src')}, function (data) {

                            var ret = $.parseJSON(data);
                            processRet(ret, sucDiv, errDiv);

                        });


                    });
                    $("#ava-arch-sec .ava-arch-content").append(ulNode);
                }


            });

        }
        else if ($(this)[0].id == 'ava-pic-tab') {
            var preview = $("#ava-pic-sec").find('.img-preview'),
                actionsNode = $('#avatar-actions'),
                currentFile,
                replaceResults = function (img) {
                    var content;
                    if (!(img.src || img instanceof HTMLCanvasElement)) {
                        content = $('<span>Loading image file failed</span>');
                    } else {
                        content = $('<a target="_blank">').append(img)
                            .attr('download', currentFile.name)
                            .attr('href', img.src || img.toDataURL());
                    }
                    preview.children().replaceWith(content);
                    if (img.getContext) {
                        actionsNode.show();
                    }
                },
                displayImage = function (file, options) {
                    currentFile = file;

                    $("#cam-actions").hide();
                    if (!loadImage(
                            file,
                            replaceResults,
                            options
                        )) {
                        preview.children().replaceWith(
                            $('<span>Your browser does not support the URL or FileReader API.</span>')
                        );
                    }
                },

                dropChangeHandler = function (e) {

                    e.preventDefault();
                    e = e.originalEvent;
                    var target = e.dataTransfer || e.target,
                        file = target && target.files && target.files[0],
                        options = {
                            maxHeight: 180,
                            canvas: true
                        };
                    if (!file) {
                        return;
                    }

                    displayImage(file, options);

                },
                coordinates;
            if (window.createObjectURL || window.URL || window.webkitURL || window.FileReader) {
                preview.children().hide();
            }
            $(document)
                .on('dragover', function (e) {
                    e.preventDefault();
                    e = e.originalEvent;
                    e.dataTransfer.dropEffect = 'copy';
                })
                .on('drop', dropChangeHandler);
            $('#fileupload').on('change', dropChangeHandler);
            $('#avatar-edit').on('click', function (event) {
                event.preventDefault();
                var imgNode = preview.find('img, canvas'),
                    img = imgNode[0];
                imgNode.Jcrop({
                    setSelect: [0, 0, 180, 180],
                    aspectRatio: 1 / 1,
                    bgColor: 'black',
                    bgOpacity: 0.3,
                    onSelect: function (coords) {
                        coordinates = coords;
                    },
                    onRelease: function () {
                        coordinates = null;
                    }
                }).parent().on('click', function (event) {
                    event.preventDefault();
                });
            });
            $('#avatar-crop').on('click', function (event) {
                event.preventDefault();
                var img = preview.find('img, canvas')[0];
                if (img && coordinates) {
                    replaceResults(loadImage.scale(img, {
                        left: coordinates.x,
                        top: coordinates.y,
                        sourceWidth: coordinates.w,
                        sourceHeight: coordinates.h,
                        maxHeight: 180
                    }));
                    coordinates = null;
                }
            });

        }
        else if ($(this)[0].id == 'ava-cam-tab') {

            var preview = $("#ava-cam-sec").find(".img-preview");
            preview.children().replaceWith($("<div>"));
            $("#cam-actions").show();
            $("#cam-upload").prop('disabled', true);
            $("#avatar-take").prop('disabled', true);


            navigator.getMedia(
                {
                    video: true,
                    audio: false
                },
                function (stream) {
                    var video = $("<video id='avatar-cam'>")[0];
                    if (navigator.mozGetUserMedia) {
                        video.mozSrcObject = stream;
                    } else {
                        var vendorURL = window.URL || window.webkitURL;

                        video.src = vendorURL ? vendorURL.createObjectURL(stream) : stream;

                        video.addEventListener('canplay', function (ev) {
                            $("#avatar-take").prop('disabled', false);
                            $("#cam-upload").prop('disabled', false);
                            if (!streaming) {

                                height = 240;
                                video.setAttribute('width', 240);
                                video.setAttribute('height', 240);
                                // canvas.setAttribute('width', 240);
                                //canvas.setAttribute('height', height);
                                streaming = true;
                            }
                        }, false);

                        $("#avatar-take").on('click', function (e) {

                            $("#avatar-cam").stop();
                            $("#avatar-cam").remove();
                            var canvas = $("<canvas>")[0];
                            preview.children().replaceWith(canvas);
                            canvas.getContext('2d').drawImage(video, 0, 0, 180, 180);
                            var data = canvas.toDataURL('image/png');


                        });
                        preview.append(video);

                    }
                    video.play();
                },
                function (err) {
                    console.log("An error occured! " + err);
                }
            );

        }

    },
    picUpload:function (e) {

        /**
         *
         * update user avatar
         *
         */

        var preview = $(this).parent().parent().find(".img-preview");
        var errDiv = $(this).parent().parent().find(".return-error");
        var sucDiv = $(this).parent().parent().find(".return-suc");
        var canvas = preview.find('canvas')[0];
        console.log("picture upload",preview);
        console.log(canvas);
        canvas.toBlob(function (blob) {

            var formData = new FormData();
            formData.append('ctoken', config.ctoken);
            formData.append("avatar", blob);
            $.ajax({
                url: "/user/updateavatar",
                type: "POST",
                data: formData,
                processData: false,  // 告诉jQuery不要去处理发送的数据
                contentType: false,   // 告诉jQuery不要去设置Content-Type请求头
                global: false,
                success: function (data) {
                    $("#cam-upload").prop('disabled', true);
                    $("#avatar-take").prop('disabled', true);
                    var ret = $.parseJSON(data);
                    if (ret.code == '1') {
                        siteUser.set('icon_url', canvas.toDataURL());
                        sucDiv.text(ret.msg).show();
                        setTimeout(function () {
                            sucDiv.hide()
                        }, 2500);
                    }
                    else {
                        errDiv.text(ret.msg).show();
                        setTimeout(function () {
                            errDiv.hide()
                        }, 2500);
                    }


                }
            });


        }, "image/png");


    },

    initInputs:function (data) {
        var topDivId=this.topDivId;
        var countryNode = $("#pro_country");
        var stateNode = $("#pro_state");
        var cityNode = $("#pro_city");
        var address = [countryNode, stateNode, cityNode];

        if (data != false) {
            _.each(data, function (val, key) {
                var input = $("#"+topDivId+" :input[name=" + key + "]");

                if (key == 'country') {
                    address.push(val);
                    return;
                }
                if (key == 'state') {
                    address.push(val);
                    return;
                }
                if (key == 'city') {
                    address.push(val);
                    return;
                }
                if (input.size() > 0) {
                    var tagName = input[0].tagName.toLowerCase();
                    var inputType = input[0].type;

                    if (inputType == "checkbox" || inputType == "radio") {

                        $("#"+topDivId+" :input[name=" + key + "][value='" + val + "']").prop("checked", true);
                        $("#"+topDivId+" :input[name=" + key + "][value='" + val + "']").prop("checked", true);

                    }
                    else {
                        $("#"+topDivId+" :input[name=" + key + "]").val(val);
                    }

                }


            });


        }
        if(countryNode.size()>0){


            initLocation.apply(this, address);
        }

    },
    basicTabInit:function () {
        $("#birthday_div").datetimepicker();
        var that=this;
        $("#apps button a").on('click',this.appDownloadClicked);
        if(profile.get('uid')>0){
            initData(profile.toJSON());
        }
        else {
            $.get("/userprofile/get",function(data){
                profile.set($.parseJSON(data));
                that.initInputs(profile.toJSON());
            });
        }



    },
    topDivId:'',
    init:function(topDivId){

        this.topDivId=topDivId;
        $("#mfa").on('click',this.mfaClicked);
        ajaxCbs['lab-form']=[function(){
            setTimeout(function(){ $("#lab-form canvas").remove();},50);


        }];
        var that=this;
        $("#"+this.topDivId+" a[href=#pro-avatar]").on('click',this.avatarTabClicked);
        seajs.use(['jquery.jcrop', 'canvas2blob', 'load_image', '/css/jquery.jcrop.css'], function () {
            var streaming = false;

            $("#"+that.topDivId+' .ava-tabs a[data-toggle="tab"]').on('shown.bs.tab',that.capPicTabShown);
            $("#avatar-upload,#cam-upload").on('click',that.picUpload);
        });
        var that=this;
        seajs.use(["/css/bs3.datepicker.css", "bs3.datepicker.js", "lib/location_"+config.langType], function(){

            that.basicTabInit();
        });

    }


};
var GlobalSetting={
    formSubmit:function (e) {
        var formParent = $(this);
        var formId=$(this)[0].id;
        var errNode = formParent.find(".return-error");
        var sucNode = formParent.find(".return-suc");
        var sendKey=$("select[name=send_key]").val();
        var formData=$(this).serialize();
        var submitBtn=formParent.find("[type=submit]");
        setuploadingEffect(submitBtn);
        $.post($(this).attr("action"), formData, function (data) {

            var ret = $.parseJSON(data);
            var showNode = errNode;
            if (ret.code == -1) {
                errNode.text(ret.msg).show();

            }
            else {
                if(formId=='general-form'){
                    var newSetting=$.unparam(formData);

                    setting.set(newSetting);

                }
                showNode = sucNode;
                sucNode.text(ret.msg).show();

            }

            setTimeout(function () {
                showNode.hide();
            }, 2000);
        }).always(function(){

        });


        //console.log($(this).serialize());

        e.preventDefault();
        return false;

    },

    init:function(topDivId){

        $("#"+topDivId+" form").on('submit', this.formSubmit);


    }


}
var PrivacySetting={
    appDownloadClicked:function(evt){

        if($(this).find("span").hasClass('fa-apple')){
            $(this).attr('href','https://itunes.apple.com/us/app/google-authenticator/id388497605?mt=8');
        }
        else if($(this).find("span").hasClass('fa-windows')){
            $(this).attr('href','http://www.windowsphone.com/en-us/store/app/authenticator/021dd79f-0598-e011-986b-78e7d1fa76f8');
        }
        else if($(this).find("span").hasClass('fa-android')){
            $(this).attr('href','https://itunes.apple.com/us/app/google-authenticator/id388497605?mt=8');
        }

    },
    topDivId:'',
    init:function(topDivId){
        this.topDivId=topDivId;
        $("#apps button a").on('click',this.appDownloadClicked);


        initData(topDivId,{"addmeflag":setting.get('addmeflag'),blockmsg:setting.get('blockmsg')});



    }

};
