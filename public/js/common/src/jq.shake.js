jQuery.fn.shake = function(config) {
    var defaults= $.extend({left:-6,ltime:10,mtime:40},config);

    this.each(function(i) {
        $(this).css({ "position": "relative" });
        for (var x = 1; x <= 3; x++) {
            $(this).animate({ left: defaults.left }, defaults.ltime).animate({ left: 0 }, defaults.mtime).animate({ left: Math.abs(defaults.left) }, defaults.ltime).animate({ left: 0 }, defaults.mtime);
        }
    });
    return this;
}