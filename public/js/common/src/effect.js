



var setuploadingEffect=function(subBtn,loadingImg){
    var height=subBtn.height();
    if(height<64){
        loadingImg=loadingImg || $(".loading-icon").clone();

    } else if(height<200 && height>64) {
        loadingImg=loadingImg || $("<img />").attr("src","/img/ajax-loading-small.gif");
    } else if(height<300 && height>200) {
        loadingImg=loadingImg || $("<img />").attr("src","/img/ajax-loading-middle.gif");
    } else if(height>300 && height>450) {
        loadingImg=loadingImg || $("<img />").attr("src","/img/ajax-loading-large.gif");
    } else if(height>450) {
        loadingImg=loadingImg || $("<img />").attr("src","/img/ajax-loading-xlarge.gif");
    }


    $(document).ajaxStart(function(){

        if(!subBtn.prev().hasClass("loading-icon"))
        {
            centerObj(subBtn,loadingImg);

        }
        else {
            loadingImg.show();
        }

    }).ajaxComplete(function(){
        loadingImg.hide();
        $(document).off('ajaxStart');
        setTimeout(function(){subBtn.prop("disabled",false);},1000);

    });

}
