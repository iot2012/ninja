/**
 * Created by chjade on 5/6/15.
 */
var isJson=function(str) {
    var result=false;
    try {
        result=JSON.parse(str);
    } catch (e) {
        result=false;
    }
    return result;
}
var isRegJson=function(text){
    if (/^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
            replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
            replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
        return true;
        //the json is ok

    }else{
        return false;
        //the json is not ok

    }
}

var isJsonRegex = function(str) {
    if (str == '') return false;
    str = str.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, '');
    return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(str);
}

var ajaxCheck=function(jqForm){

    /**
     * required input value checking
     * @type {Array}
     */
    var that=jqForm;
    var requiredErrs=[];
    $(that).find(":input.required").each(function(idx,item){
        var input=$(item);
        if($.trim(input.val())===''){
            var label= input.parent().find("label") || input.parent().parent().find("label");
            console.log(label);
            var errMsg=(label.size()>0?label.text():$(item).attr("name").replace(/_/g,' '))+" is invalid";

            requiredErrs.push([input,errMsg]);
        }
        else {

            input.removeClass("input-err");
            input.tooltip('destroy');
        }

    });
    for(var i=0;i<requiredErrs.length;i++){

        requiredErrs[i][0].addClass("input-err");
        requiredErrs[i][0].tooltip({

            title:requiredErrs[i][1],
            trigger:'click'
        });
    }
    if(requiredErrs.length>0){
        return false;
    }
    return true;
}
var checkOneInput=function(input){

    var regData=input.attr("ref");

    if(regData)
    {


        var regData=$.parseJSON(regData);

        var errMsg='';
        if(typeof regData['msg']!='undefined'){
            errMsg=regData['msg'];
        }
        else {
            var label=input.parent().parent().find("label") || input.parent().find("label");
            var labelText=label.size()>0?label.text():$(this).attr("name").replace(/_/g,' ')+config.lang.is_invalid;
            errMsg=labelText;
        }
        var inputVal= $.trim(input.val());
        if(regData['ref'])
        {
            var refNode=$(":input[name="+regData['ref']+"]");
            if(refNode.size()>0 && !($.trim(refNode.val())==inputVal))
            {
                return [false,config.lang.chk_match.replace(/#name#/,config.lang[input.attr("name")]?config.lang.regData['ref']:input.attr("name"))];
            }
            else
            {
                return [true,refNode];
            }

        }
        if(regData['r']==1)
        {

            if(inputVal==''){
                return [false,input.attr('placeholder')+' '+config.lang.cant_empty];
            }
            else
            {
                if(!ival[regData['reg']].test(inputVal))
                {
                    return [false,errMsg];

                }
            }
        }
        else
        {
            if(inputVal!=''){
                if(!ival[regData['reg']].test(inputVal))
                {

                    return [false,errMsg];

                }
            }
        }




    }
    return [true,1];

}

function checkInputs(inputRegs,inputs){

    var errInputs=[];
    var emptyInputs=[];
    for(var name in inputRegs){
        if(inputRegs[name]['r']==1){
            if(inputs[name]=='' || !inputs[name]){
                emptyInputs.push(name);
                continue;
            }

        }
        if(!inputRegs[name]['reg'].test(inputs[name])){
            errInputs.push(name);
            continue;
        }

    }
    if(errInputs.length!=0 || emptyInputs.length!=0){
        return [false,errInputs,emptyInputs];
    }
    return [true];
}
var checkInput=function(){

    $(":input").blur(function(){
        var res=checkOneInput($(this));
        if(res[0]===true)
        {
            if(res[1]!='1')
            {
                res[1].removeClass("input-err");
                $(this).tooltip('destroy');
            }

            $(this).removeClass("input-err");
            $(this).tooltip('destroy');
            return;
        }
        if($(this).hasClass("input-err"))
        {
            $(this).tooltip('destroy');
            $(this).tooltip({

                title:res[1],
                trigger:'click'
            });

        }
        else
        {

            $(this).addClass("input-err");

            $(this).tooltip({

                title:res[1],
                trigger:'click'
            });

        }


    });

}