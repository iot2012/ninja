

var attachDropzone=function(jqSel,cfg,isBg,cbs){
    if($(jqSel).size()==0){
        return false;
    }
    var isBg=isBg || true;
    var cfg=cfg || {};
    var cbs=$.extend({
        success:function(e,data){

            var input=$(this.element);
            var type=input.attr("data-prev-type");
            if(type=='bg'){
                $(input.attr('data-prev')).css({"background":"url("+('/upload'+data.data.mediaurl)+") no-repeat"});
            }
            var bgDom=$("#page-form").find("input[data-bg]");

            bgDom.val(data.data.mediaurl);

            console.log("success","fd",e,data);
        },
        error: function(e){

            console.log("error",e,this);
        },
        addedfile:function(e){

            console.log("addedfile",e,this);
        },
        complete:function(file){
            var input=$(this.element);
            var type=input.attr("data-prev-type");
            dropzone.removeFile(file);
            console.log("complete",file,this);
        }

    },cbs);
    Dropzone.autoDiscover=false;
    var options= $.extend({ url: "/media/upload.json" ,dictDefaultMessage:"点击或者拖放添加",previewsContainer:'',uploadMultiple:false,maxFiles:100},cfg);
    var dropzone=new Dropzone(jqSel,options);

    dropzone.on('sending', function(file, xhr, formData){
        if('extraData' in options){
            for(var key in options['extraData']){
                formData.append(key, options['extraData'][key]);
            }
        }

    });
    dropzone.on("complete",cbs['complete']);
    dropzone.on("success",cbs['success']);
    dropzone.on("error", cbs['error']);
    dropzone.on("addedfile", cbs['addedfile']);
}