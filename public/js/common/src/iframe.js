var createIframe = function (attr, css) {

    var localAttr = {name: '', class: 'iframe-doc', frameborder: 'no', allowtransparency: true, scrolling: 'auto', hidefocus: ""};
    var localCss = {width: "100%", height: "100%", top: 0, 'background-color': 'transparent', left: 0};
    var props = {"webkitallowfullscreen": true, "allowfullscreen": true};
    var attr = $.extend(localAttr, attr);
    var css = $.extend(localCss, css);

    return $("<iframe></iframe>").attr(attr).css(css).prop(props);
}