var langPluck=function(lang,plkArr){
    var result={};
    for(var i= 0,len=plkArr.length;i<len;i++){
        result['l_'+plkArr[i]]=lang[plkArr[i]];
    }
    return result;
}
var combLang=function(arr){
    if(config.langType=='enus'){
        return arr.join(" ");
    }
    else if(config.langType=='zhcn'){
        return arr.join("");
    }

}
var _t=function(){
    var langs=[];

    var langObj=arguments[0];
    var initVal=1;
    if(typeof langObj=='string'){
         langObj=config.lang;
         initVal=0;
    }


    for(i=initVal;i<arguments.length;i++){
        langs.push(langObj[arguments[i]]?langObj[arguments[i]]:arguments[i]);
    }

    return langs.join(langObj._langDel);

}