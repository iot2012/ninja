


var GeneralSetting={

    topDivId:'',
    init:function(topDivId){
        this.topDivId=topDivId;
        this.inputDataInit(config.setting);
        $("#"+topDivId+' .slider-button').on('click', this.enAudVidClicked);
        $(":input[name=en_desknotify]").on('click',this.desktopNotify);
        $(":input[name=en_camlogin]").on('click',this.camLogin);
    },
    inputDataInit:function(configSetting){
        var that=this;
        if (configSetting != {}) {
            _.each(configSetting, function (val, key) {
                console.log("setting here");
                var input = $("#"+that.topDivId+" :input[name=" + key + "]");

                if (input.size() > 0) {
                    if(key != 'enablesound'){
                        if(input[0].tagName.toLowerCase()==="input" && ['checkbox','radio'].indexOf(input[0].type)!=-1){
                            $("#"+that.topDivId+" :input[name=" + key + "][value='" + val + "']").prop("checked", true);
                        }
                        else {
                            input.val(val);
                        }

                    }

                    else {
                        if (key == 'enablesound'){
                            if(val == '1') {
                                input.parent().parent().find(".slider-frame>.slider-button").addClass('on');

                            }
                            $("#"+that.topDivId+" :input[name=" + key + "]").val(val);
                        }

                    }

                }




            });

        }
    },
    enAudVidClicked:function (e) {
        var parentNode = $(this).parent().parent();
        var input = parentNode.find("input");
        if ($(this).hasClass("on")) {
            input.val('0');
            $(this).removeClass("on").html("&bull;").css({"font-size": "5em"});
        } else {
            input.val('1');
            $(this).css({"font-size": "1em"}).addClass("on").html("|");
        }

    },
    desktopNotify:function(e){

        var that=this;
        if(notify.permissionLevel()==notify.PERMISSION_DEFAULT){
            notify.requestPermission(function(data){

                if(notify.permissionLevel()==notify.PERMISSION_GRANTED){

                    $(that).prop("checked",true);
                    e.preventDefault();
                    return false;

                }
                else {
                    $(that).prop("checked",false);
                    console.log("denieyd,default");
                }

            });

        }

    },
    camLogin:function(e){
        if($(this).prop("checked"))
        {
            seajs.use(['ht.js'],function(){
                var video=$("#cam-video");
                video.show();
                var htracker = new headtrackr.Tracker({});
                var canvasInput=$("#cam-canvas")[0];
                var videoInput=video[0];
                htracker.init(videoInput, canvasInput);
                var errDiv = $("#general-form").parent().find(".return-error");
                var sucDiv = $("#general-form").parent().find(".return-suc");
                htracker.start();
                var cnt=1;
                document.addEventListener('headtrackrStatus',
                    function (event) {
                        console.log(event.status);
                        if (event.status == "getUserMedia") {

                        }
                        else if (event.status == "found" && cnt==1) {
                            cnt++;
                            var canvas = $("<canvas>")[0];

                            canvasInput.getContext('2d').drawImage(videoInput, 0, 0, 240, 240);
                            var data = canvasInput.toDataURL('image/png');


                            canvasInput.toBlob(function (blob) {

                                var formData = new FormData();
                                formData.append('ctoken', config.ctoken);
                                formData.append('face',1);
                                formData.append("avatar", blob);
                                $.ajax({
                                    url: "/user/facedetect",
                                    type: "POST",
                                    data: formData,
                                    processData: false,  // 告诉jQuery不要去处理发送的数据
                                    contentType: false,   // 告诉jQuery不要去设置Content-Type请求头
                                    global: false,
                                    success: function (data) {

                                        var ret = $.parseJSON(data);
                                        if (ret.code == '1') {
                                            sucDiv.text(ret.msg).show();
                                            setTimeout(function () {
                                                sucDiv.hide()
                                            }, 2500);
                                            videoInput.pause();
                                            video.hide();
                                        }
                                        else {
                                            errDiv.text(ret.msg).show();
                                            setTimeout(function () {
                                                errDiv.hide()
                                            }, 2500);
                                        }


                                    }
                                });


                            }, "image/png");

                            console.log("FOV",htracker.getFOV());
                        }
                    }
                );
            });
        }




    }



};
