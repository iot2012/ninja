
var centerObj=function(container,div){


    var oW=container.outerWidth(true);
    var oH=container.outerHeight(true);

    div.css({position:"absolute","z-index":999});
    container.before(div);
    container.prop("disabled",true);
    div.show();

    var containerOffset=container.offset();
    div.offset({left:(containerOffset.left+(oW-div.outerWidth())/2),top:(containerOffset.top+(oH-div.outerHeight())/2)});



}
var appendCenterDiv=function(container,centerDom){

    var oW=container.outerWidth();
    var oH=container.outerHeight();

    centerDom.css({position:"absolute"});
    container.before(centerDom);
    container.prop("disabled",true);

    var btnOffset=centerDom.offset();
    centerDom.offset({left:(btnOffset.left+(oW-centerDom.width())/2),top:(btnOffset.top+(oH-centerDom.height())/2)});
    centerDom.show();
}