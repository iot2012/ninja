/**
 *
 * @param container which container displays overlay
 * @param title
 * @param timeout timeout seconds
 * @param type
 */
var showOverlay=function(container,type,title,timeout){
    //check,times
    var type=type|| "check";
    var timeout=timeout || 5;
    if(type=='spin') {
        type = "fa-spinner fa-spin";
    }
    else if(type=='spin-lg') {
        type = "fa-spinner fa-spin fa-10x";
    }
    else if(type=='spin-md') {
        type = "fa-spinner fa-spin fa-5x";
    }
    else if(type=='spin-sm'){
        type="fa-spinner fa-spin fa-3x";
    }
    else {
        type="fa-"+type;
    }
    var overlay='<div class="ui-ios-overlay"><span class="title">'+title+'</span> <span class="fa '+type+'"></span></div>';
    var obj=$(overlay);
    setTimeout(function(){obj.remove();},timeout*1000);
    centerObj(container,obj);
    return obj;

}