
var createAlert=function(title,content,alertType,time,okCallback,oktxt,forceJoinCallback,forceTxt,attrs){
    var alertTemp='<div class="alert alert-<%=alertType%> alert-dismissible" role="alert" style="text-align:center;display:none; z-index: 9999;margin-left: 0px; width: 100%;top:0px;left:0px"> ' +
        '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button> ' +
        '<%if(video==true){%><button type="button" class="btn btn-primary joinbtn" style="float: right; margin-right: 20px; "><%=oktxt%></button><button type="button" class="btn btn-primary forcejoinbtn" style="float: right; margin-right: 20px; "><%=forceTxt%></button><%}%>' +
        '<strong><%=title%></strong>&nbsp;<%=content%></div>';

    var time=time || 2000;
    var hasbtn= false;
    var oktxt=oktxt||"Try Again";
    var forceTxt=forceTxt || 'Force join';
    var okCb=function(){};
    var forceJoinCb=function(){};


    if($.isFunction(okCallback)){

        hasbtn= true;
        okCb=okCallback;
        forceJoinCb=forceJoinCallback;
    }
    var alertHtml=template(alertTemp)({alertType:(alertType || "info"),title:title,content:content,video:hasbtn,oktxt:oktxt,forceTxt:forceTxt});

    var alertObj=$(alertHtml);

    alertObj.find(".joinbtn").on('click',okCb);
    alertObj.find(".forcejoinbtn").on('click',forceJoinCb);
    alertObj.slideDown(400,function(){
        var that=this;
        setTimeout(function(){$(that).slideUp();},time);
    });
    return alertObj;

}
var showAlert=function(title,content,alertType,time,okCallback,oktxt,forceCb,forceTxt){
    var alertObj=createAlert(title,content,alertType,time,okCallback,oktxt,forceCb,forceTxt);
    $("body").append(alertObj.css({position: "fixed",top: "0px",left: "0px"}));

}