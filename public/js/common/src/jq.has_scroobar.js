jQuery.fn.hasScrollBar = function(direction)
{
    var direction=direction || 'vertical'
    if (direction == 'vertical')
    {
        return this.get(0).scrollHeight > this.innerHeight();
    }
    else if (direction == 'horizontal')
    {
        return this.get(0).scrollWidth > this.innerWidth();
    }
    return false;

}
$.fn.hasVScroll = function() {
    //innerHeight
    return this.get(0).scrollHeight > this.height();
}
$.fn.hasHScroll = function() {
   return this.get(0).scrollWidth > this.width();
}
