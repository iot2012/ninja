navigator.getMedia = ( navigator.getUserMedia ||
navigator.webkitGetUserMedia ||
navigator.mozGetUserMedia ||
navigator.msGetUserMedia);

var DetectRTC={};

var DetectRTC = {};


DetectRTC.hasMicrophone = false;
DetectRTC.hasSpeakers = false;
DetectRTC.hasWebcam = false;

DetectRTC.MediaDevices = [];

function checkDeviceSupport(callback) {
    // This method is useful only for Chrome!

    // Firefox seems having no support of enumerateDevices feature.
    // Though there seems some clues of "navigator.getMediaDevices" implementation.
    if (isFirefox) {
        DetectRTC.hasMicrophone = true;
        DetectRTC.hasSpeakers = true;
        DetectRTC.hasWebcam = true;
        callback && callback();
        return;
    }

    if(!navigator.getMediaDevices && MediaStreamTrack && MediaStreamTrack.getSources) {
        navigator.getMediaDevices = MediaStreamTrack.getSources.bind(MediaStreamTrack);
    }

    // if still no "getMediaDevices"; it MUST be Firefox!
    if (!navigator.getMediaDevices) {
        log('navigator.getMediaDevices is undefined.');
        // assuming that it is older chrome or chromium implementation
        if (isChrome) {
            DetectRTC.hasMicrophone = true;
            DetectRTC.hasSpeakers = true;
            DetectRTC.hasWebcam = true;
        }

        callback && callback();
        return;
    }

    navigator.getMediaDevices(function (devices) {
        devices.forEach(function (device) {
            // if it is MediaStreamTrack.getSources
            if(device.kind == 'audio') {
                device.kind = 'audioinput';
            }

            if(device.kind == 'video') {
                device.kind = 'videoinput';
            }

            if(!device.deviceId) {
                device.deviceId = device.id;
            }

            if(!device.id) {
                device.id = device.deviceId;
            }

            DetectRTC.MediaDevices.push(device);

            if(device.kind == 'audioinput' || device.kind == 'audio') {
                DetectRTC.hasMicrophone = true;
            }

            if(device.kind == 'audiooutput') {
                DetectRTC.hasSpeakers = true;
            }

            if(device.kind == 'videoinput' || device.kind == 'video') {
                DetectRTC.hasWebcam = true;
            }

            // there is no "videoouput" in the spec.
        });

        if (callback) callback();
    });
}
