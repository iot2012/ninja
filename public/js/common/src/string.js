var capStr = function (string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
String.prototype.padZero= function(len, c){
    var s= this, c= c || "0", len= len || 2;
    while(s.length < len) s= c + s;
    return s;
};