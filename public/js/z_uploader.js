define(function(require, exports, module) {

    var template=require("art");
    require('cropper');
    require('webuploader');
    window.Uploader = (function() {

        // -------setting-------
        // 如果使用原始大小，超大的图片可能会出现 Croper UI 卡顿，所以这里建议先缩小后再crop.
        var FRAME_WIDTH = 1600;


        var _ = WebUploader;
        var Uploader = _.Uploader;
        var uploaderContainer = $('.uploader-container');
        var uploader, file;

        if ( !Uploader.support() ) {
            alert( 'Web Uploader 不支持您的浏览器！');
            throw new Error( 'WebUploader does not support the browser you are using.' );
        }

        // hook,
        // 在文件开始上传前进行裁剪。
        Uploader.register({
            'cropImage': 'cropImage'
        }, {

            cropImage: function( file ) {

                var data = file._cropData,
                    image, deferred;

                file = this.request( 'get-file', file );
                deferred = _.Deferred();

                image = new _.Lib.Image();

                deferred.always(function() {
                    image.destroy();
                    image = null;
                });
                image.once( 'error', deferred.reject );
                image.once( 'load', function() {
                    image.crop( data.x, data.y, data.width, data.height, data.scale );
                });

                image.once( 'complete', function() {
                    var blob, size;

                    // 移动端 UC / qq 浏览器的无图模式下
                    // ctx.getImageData 处理大图的时候会报 Exception
                    // INDEX_SIZE_ERR: DOM Exception 1
                    try {
                        blob = image.getAsBlob();
                        size = file.size;
                        file.source = blob;
                        file.size = blob.size;

                        file.trigger( 'resize', blob.size, size );

                        deferred.resolve();
                    } catch ( e ) {
                        console.log( e );
                        // 出错了直接继续，让其上传原始图片
                        deferred.resolve();
                    }
                });

                file._info && image.info( file._info );
                file._meta && image.meta( file._meta );
                image.loadFromBlob( file.source );
                return deferred.promise();
            }
        });

        return {
            /**
             *
             * @param selectCb
             * @param opts {
             *  pickId:'#filePicker',
             *  cbs:{afterSendFile:''},
             *  prevSel:'预选',
             *  urls:{preview:'预览url',upload:'上传url',swf:'flash地址'}
             * }
             */
            init: function( selectCb ,opts) {
                if('cbs' in opts){
                    if('afterSendFile' in opts.cbs){

                        Uploader.register({'after-send-file':"afterSendFile"},{
                            'afterSendFile':opts.cbs.afterSendFile
                        });
                    }

                }
                uploader = new Uploader({
                    pick: {
                        id: (opts.pickId || '#filePicker'),
                        multiple: false
                    },
                    auto:false,
                    // 设置用什么方式去生成缩略图。
                    thumb: {
                        quality: 70,

                        // 不允许放大
                        allowMagnify: false,

                        // 是否采用裁剪模式。如果采用这样可以避免空白内容。
                        crop: false
                    },

                    // 禁掉分块传输，默认是开起的。
                    chunked: false,

                    // 禁掉上传前压缩功能，因为会手动裁剪。
                    compress: false,

                    // fileSingleSizeLimit: 2 * 1024 * 1024,

                    server: opts.urls.upload,
                    swf: opts.urls.swf,
                    //  fileNumLimit: 10,
                    onError: function() {
                        var args = [].slice.call(arguments, 0);
                        alert(args.join('\n'));
                    }
                });

                uploader.on('fileQueued', function( _file ) {
                    file = _file;

                    uploader.makeThumb( file, function( error, src ) {

                        if ( error ) {
                            alert('不能预览');
                            return;
                        }

                        selectCb( src );

                    }, FRAME_WIDTH, 1 );   // 注意这里的 height 值是 1，被当成了 100% 使用。
                });
            },

            crop: function( data ) {

                var scale = Croper.getImageSize().width / file._info.width;
                data.scale = scale;

                file._cropData = {
                    x: data.x1,
                    y: data.y1,
                    width: data.width,
                    height: data.height,
                    scale: data.scale
                };
            },

            upload: function() {
                uploader.upload();
            }
        }
    })();

// ---------------------------------
// ---------  Crpper ---------------
// ---------------------------------
    window.Croper = function(imgUploader,opts) {
        var container = imgUploader.find('.cropper-wraper');
        var $image = container.find('.img-container img');
        var btn = imgUploader.find('.upload-btn');
        var isBase64Supported, callback;

        $image.cropper({
            aspectRatio: 16 / 9,
            preview: opts.prevSel,
            done: function(data) {
                // console.log(data);
                btn.show();
            }
        });

        function srcWrap( src, cb ) {

            // we need to check this at the first time.
            if (typeof isBase64Supported === 'undefined') {
                (function() {
                    var data = new Image();
                    var support = true;
                    data.onload = data.onerror = function() {
                        if( this.width != 1 || this.height != 1 ) {
                            support = false;
                        }
                    }
                    data.src = src;
                    isBase64Supported = support;
                })();
            }

            if ( isBase64Supported ) {
                cb( src );
            } else {
                // otherwise we need server support.
                // convert base64 to a file.
                $.ajax(opts.urls.preview, {
                    method: 'POST',
                    data: src,
                    dataType:'json'
                }).done(function( response ) {
                    if (response.result) {
                        cb( response.result );
                    } else {
                        alert("预览出错");
                    }
                });
            }
        }

        btn.on('click', function() {

            callback && callback($image.cropper("getData"));
            return false;
        });

        return {
            setSource: function( src ) {

                // 处理 base64 不支持的情况。
                // 一般出现在 ie6-ie8
                srcWrap( src, function( src ) {
                    $image.cropper("setImgSrc", src);
                });

                container.removeClass('webuploader-element-invisible');

                return this;
            },

            getImageSize: function() {
                var img = $image.get(0);
                return {
                    width: img.naturalWidth,
                    height: img.naturalHeight
                }
            },

            setCallback: function( cb ) {
                callback = cb;
                return this;
            },

            disable: function() {
                $image.cropper("disable");
                return this;
            },

            enable: function() {
                $image.cropper("enable");
                return this;
            }
        }

    };
    var options={
        feature:{
            sys_icon:false,
            take_photo:false,
            upload:true
        },
        prevSel:".img-preview",
        /**
         * uploader:配置封装的upload
         * glyphicon:系统的图标
         */
            styleId:'_z_img_upload_style',
        urls:{
          'swf':'/js/Uploader.swf','glyphicon':'/admin/js/page/temps/sysicons/glyphicon.html','upload':'/media/upload.json','preview':'/media/preview.json'
            //'../../server/preview.php'
        },
        style:'.img-preview { width: 160px; height: auto; margin-top: 1em; border: 1px solid #ccc; } .cropper-wraper { position: relative; } .upload-btn:hover { background: #f0f0f0; }',
        cbs:{},
        temp:{
            img:'<div class="modal fade"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><%=lang.l_close%></span></button> <h4 class="modal-title"><%=lang.l_title%></h4> </div> <div class="modal-body"> <ul class="nav nav-pills nav-justified feature-tabs"><li class="<%if((feature.upload && feature.sys_icon===false && feature.take_photo==false)){%>hidden<%}%>"><a href="#upload_img" data-toggle="tab"> <%=lang.l_upload_img%></a></li> <%if(feature.take_photo){%><li><a href="#take_photo" data-toggle="tab"><%=lang.l_take_photo%></a></li><%}%> <%if(feature.sys_icon){%><li><a href="#sys_icon" data-toggle="tab"><%=lang.l_sys_icon%></a></li><%}%> </ul> <div class="tab-content"> <div class="tab-pane active" id="upload_img"> <div class="uploader-container"> <div id="filePicker" class="filePicker"><%=lang.l_upload_img%></div> <div class="upload-btn btn btn-primary" style="display:none"><%=lang.l_start_upload%></div><div class="cropper-wraper webuploader-element-invisible"> <div class="img-container"><img src="" alt="" /></div>  <div class="img-preview"></div> </div> </div> </div> <%if(feature.take_photo){%><div class="tab-pane" id="take_photo"> </div><%}%> <%if(feature.sys_icon){%><div class="tab-pane" id="sys_icon"> </div><%}%> </div>  </div> </div> </div> </div>'

        },
        lang:{
            'l_close':'关闭',
            'l_take_photo':'拍照',
            'l_upload_img':'上传图片',
            'l_start_upload':'开始上传',
            'l_title':'拍照片',
            'l_sys_icon':'系统图标'

        }
    };

    var combLang=function(){
        var langs=[];
        var limiter=' ';
        var lang=arguments[0];
        for(i=1;i<arguments.length;i++){
            langs.push(lang[arguments[i]]?lang[arguments[i]]:arguments[i]);
        }

        if(true || config.lang.langType!='zhcn'){
            limiter='';

        }
        return langs.join(limiter);

    }
    var getOptions=function(){
        return options;
    }
    var getMid=function(id){
        return "m_"+id;
    }
    var initUploadDialog=function(imgUploader,opts){
        window.Croper=window.Croper(imgUploader,opts);
        var container = imgUploader.find('.uploader-container');
        var styleId=opts.styleId || '_z_img_upload_style';
        if($("#_z_img_upload_style").size()==0){
            $("head").append($("<style>").html(opts.style).attr("id",styleId));
        }

        Uploader.init(function( src ) {
            Croper.setSource( src );

            Croper.setCallback(function( data ) {
                Uploader.crop(data);
                Uploader.upload();
            });
            // 隐藏选择按钮。

            // 当用户选择上传的时候，开始上传。

        },opts);
    };
    var showImgUploadDialog=function(id,extOpts){
        var opts=getOptions();
        opts= $.extend(opts,extOpts);
        var imgTemp=opts.temp.img;
        var mid=getMid(id);
        if($("#"+mid).size()==0){
            var imgUploader=$(template.compile(imgTemp)(opts));
            imgUploader.attr("id",mid);

            imgUploader.on('click','[data-toggle="tab"]',function(evt){
                var tab=$(this).attr("href");
                if(tab=='#sys_icon') {
                    if($(tab).find("div").size()==0){
                        $.get('/admin/js/page/temps/sysicons/glyphicon.html',function(data){
                            $(tab).html(data);
                        });
                    }
                } else if(tab="#upload_img"){
                    if($(tab).find(".webuploader-pick").size()==0) {
                        initUploadDialog(imgUploader,opts);
                    }
                }

            });

            $(imgUploader).modal({ keyboard: false,backdrop:'static'}).on('shown.bs.modal',function(){
                if($(this).find(".feature-tabs>li").size()==1){

                    $(this).find(".feature-tabs>li>a").trigger('click');
                }
            });
        } else {
            $("#"+mid).modal('show');
        }

    };
    /**
     *
     * @param id
     * @param prevJqObj 关闭后预览的节点
     */
    var hideImgUploadDialog=function(id,prevJqObj){

        var mid=getMid(id);
        var opts=getOptions();
        $("#"+mid).modal('hide');
        if(prevJqObj.size()>0){
            console.log($("#"+mid).find(opts.prevSel).find('img').attr('src'));
            prevJqObj.attr('src',$("#"+mid).find(opts.prevSel).find('img').attr('src'));
        }

    };
    module.exports={
        show:showImgUploadDialog,
        hide:hideImgUploadDialog,
        options:options
    };
});
