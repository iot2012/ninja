/**
 * Created by ryatek21 on 10/21/15.
 */

var lettersMinLen=0;
function jsMenus(menus,result){


    var tempArr=[];
    for(var key in menus){
        console.log(key);

        if(menus[key]['subs']){
            var subHtml="<li data-value='"+menus[key]['id']+"'>"+menus[key]['name']+"<ol>"+jsMenus(menus[key]['subs'],result)+"</ol></li>";
            if(menus[key]['pid']==0){
                result.push(subHtml);
            } else {
                tempArr.push(subHtml);

            }





        } else{

            var liHtml="<li data-value='"+menus[key]['id']+"'"+(menus[key]['is_checked']?" data-checked='1'":"")+">"+menus[key]['name']+"</li>";
            if(menus[key]['pid']==0){
                result.push(liHtml);
            } else {
                tempArr.push(liHtml);

            }





        }

    }
    return tempArr.join("");


}

var opCol=function(tbName,primaryId,cfg){
    var cfg=cfg || opMenus;

    cfg=cfg.map(function(item){
        return '<li> <a href="#"  data="'+tbName+'" class="opt-'+item+'" > <i class="fa fa-'+item+' fa-15x"></i> </a> </li>';
    });
    return '<td class="operation"> <ul class="nav navbar-nav" data="r_'+primaryId+'" data-table="'+tbName+'">'+cfg.join('')+"</ul> </td>";


}
var appendRow=function(formJson,opCol2){


    var newRow = buildRow($.isArray(formJson)?formJson:[formJson],colName(formJson['ztb']),opCol2);


    var tbId="tb_"+formJson['ztb'];

    delete formJson['ztb'];

    $("#"+tbId).append(newRow).trigger('footable_redraw');
}
var buildRow=function(rowData,refCols,opCol2,cols){

    var cols=cols || jsonCols;

    var rows=rowData.map(function(row){
        var primaryId=row[primaryKey];
        var tr='<tr id="r_'+primaryId+'">';
        var tbName=row['ztb'];
        // add more dropdown menu
        //<li class="dropdown pull-right"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-list fa-15x"></i> </a> <ul class="dropdown-menu" role="menu"> <li> <a href="#"> <span class="fa fa-file-code-o"></span> CSV </a> </li> <li> <a href="#"> <span class="fa fa-file-pdf-o"></span>Excel </a> </li> <li class="divider"></li> </ul> </li>
        //使用opCol2变量会导致opCol2为undefined,导致始终执行后面的函数
        var opCol3=opCol2 || opCol(tbName,primaryId);

        var tds=refCols.map(function(key){
            if(cols[key].reg=='file'){
                row[key]=ajaxMedia[row[key]];
            }
            var checked=$(".filter_cols a[data=c_"+key+"] :checkbox").prop('checked');
            if(checked){
                return '<td data="c_' + key + '">' + ((typeof row[key]!='undefined')?row[key]:'') + '</td>';
            } else {
                return '<td style="display:none" data="c_' + key + '">' + ((typeof row[key]!='undefined')?row[key]:'') + '</td>';
            }

        });
        return tr+'<th  data-ignore="true" class="edit-row"><input type="checkbox"/></th>'+tds.join('')+opCol3+'</tr>';

    });
    return rows;

}
var beforeOp=function(action,obj){
    if(jsonActions && (action in jsonActions)){
        if(!jsonActions[action].match(/^[\w-]+$/)){
            location.href=addQuery(jsonActions[action],obj);
        } else {
            (typeof jsonActions[action]=='function') && jsonActions[action]();
        }
        return true;
    }
    return false
}
var colName=function(tbName){
    var tds=$("#tb_"+tbName+">thead>tr.col-meta").eq(0).find("th[data^=c_]");
    return Array.prototype.slice.call(tds.map(function(idx,item){return $(item).attr('data').replace(/c_/,'');}),0);
}
var loadData=function(loadingObj,param,table){
    var table = table || $(loadingObj).closest('table');
    setuploadingEffect($(loadingObj));
    console.log("load more");
    var table = table || $(loadingObj).closest('table');
    var footable=table.data('footable');
    $.post("/admin/table/view?ztb="+tbModel,param).done(function(data){
        if($.trim(data)!=''){
            var tbModel=table.attr("id").replace(/tb_/,'');
            var newRow=buildRow($.parseJSON(data),colName(tbModel));
            table.find("tbody").append(newRow).trigger('footable_redraw');
        }

    });
}
var loadMore=function(e){
    var page=parseInt($(".pagination  .footable-page:last").find("a[data-page]").text(),10)+1;
    var param={p:page,t:'more'};
    loadData(this,param);
    //build up the row we are wanting to add

    //tbNode.append(newRow).trigger('footable_redraw');


}
var clearFilter=function (e) {
    e.preventDefault();
    $('.filter-status').val('');
    $('table.demo').trigger('footable_clear_filter');
};
function toUnicode(orgStr) {
    var str=[];
    for(var i= 0,len=orgStr.length;i<len;i++){
        if(ival.cn.test(orgStr[i])){
            str.push('\\u'+orgStr[i].charCodeAt(0).toString(16));
        } else {
            str.push(orgStr[i]);
        }

    }
    return str.join('');
}
var doGlobalSearch=function(evt){
    console.log("global search");
    var kw=$(this).val();
    var kws=this.id.split("_");
    var keyword="#"+kws[0]+"_keyword";
    console.log(kw,toUnicode(kw));
    var param=$.param({"_":encodeURIComponent(kw)});
    $(keyword).val(encodeURIComponent(kw));
    var tbId="#tb_"+kws[0];
    evt.preventDefault();
    $(tbId).trigger('footable_filter', {filter: param});

    return false;
}

var doGlobalSearch2= _.debounce(doGlobalSearch,600);
var doFilter=function(e){
    /* Ignore tab key */
    console.log("do filter",this);
    var code = e.keyCode || e.which;
    if (code == '9') return;
    /* Useful DOM data and selectors */
    var $input = $(this);
    var data={};
    var tb=$(this).parent().parent().parent().parent().attr("id");

    var result={};
    $("#"+tb+" .filters  input.col-filter").each(function(idex,item){
        var input=$(item);
        console.log(item);

        var val= $.trim(input.val());
        if(val.length>lettersMinLen){
            result[input.attr('data')]=val;
        }



    });
    var param=$.param(result);
    $(("#"+tb+"_keyword")).val($.param(result));
    console.log("result",$.param(result));
    $(('table#tb_'+tb)).trigger('footable_filter', {filter: $.param(result)});
};
var lazyShowGotoTop = _.debounce(showGotoTop, 400);
/**
 *
 * rowData:array
 * refcols:reference order
 */
/**
 *
 * rowData:array
 * refcols:reference order
 */

var initUploadPanel=function(node,acceptFiles,callback){
    var fileKey=$(node).attr("data");
    var maxFiles=parseInt(jsonCols[fileKey]['maxFiles']?jsonCols[fileKey]['maxFiles']:1);
    var fileSize=parseInt(jsonCols[fileKey]['fileSize']?jsonCols[fileKey]['fileSize']:100*1024*1024);
    var acceptExts=jsonCols[fileKey]['acceptExts'].join(",");
    var callback=callback || function(){};
    attachDropzone(node,{url: "/media/upload.json",acceptedFiles:acceptExts,paramName: fileKey,maxFilesize:fileSize,maxFiles:maxFiles},false,{
        success:function(e,data){
            callback(data);
            if(data.code==1){
                $("[type=hidden][name="+fileKey+"]").val(data.data.mediaurl);
                showOverlay($("#tb_{{$data.tbName}}"),'check',config.lang.upload_suc,2);
            } else {
                showOverlay($("#tb_{{$data.tbName}}"),'times',data.msg,2);
            }
            console.log("upload suc",e,"msg",data);
        },
        error:function(e,data){
            showOverlay($("#tb_{{$data.tbName}}"),'times',config.lang.upload_failed,2);
        }

    });
}

var addRow=function(evt){

    if($(this).attr('href')!='' && $(this).attr('href')!='#'){

        return true;
    }



    var modalName=tbName=$(this).attr("data");


    if(beforeOp('add',{ztb:tbName,action:'add'})){
        evt.preventDefault();
        return;
    }

    var formName=tbName+"-form-add";

    var modalId=pid=tbName+"_add_win";
    ajaxCbs[formName]=[function(data){

        var formJson=$.unparam(ajaxCbs[formName+'-data']);

        formJson['uuid']=formJson['uuid']?formJson['uuid']:defUuid;

        //build up the row we are wanting to add
        formJson[primaryKey]=data.id;
        debugger;
        for(var key in data){
            if($.isPlainObject(data[key])){
                data[key]= JSON.stringify(data[key]);
            }
        }

        formJson= $.extend(formJson,data);

        console.log("ajax append row");
        appendRow(formJson);
        if($.isFunction(tbOpCbs['beforeAdd'])){
            tbOpCbs['beforeAdd'](formJson,formName,modalName);
        }


        setTimeout(function(){$("#"+pid).modal('hide');},3000);
    }];


    $(evt).prop("disabled",true);
    var modal = $("#" + pid);
    var lTbName=config.lang['t_'+tbName.toLowerCase()]?config.lang['t_'+tbName.toLowerCase()]:tbName;
    var rowData={};
    if($.isFunction(tbOpCbs['beforeAdd'])){
        tbOpCbs['beforeAdd'](rowData,formName,modalId);
    }
    if (modal.size() < 1) {

        $.get('/admin/js/table/temp/record_add.html', function (html) {
            var profileModal = $(template.compile(html)($.extend({},{l_record:config.lang.record,l_add:config.lang.add,l_tbName:tbNameTxt,tbName:tbModel,'write_all':canWrite(),lang: config.lang,cols:jsonCols,inputTypes:inputTypes})));
            // profileModal.drags({handle: ".modal-header"});
            // profileModal.find(".modal-dialog").resizable();
            // jqObj,title,html,popOverOpts,css,cbDict

            debugger;
            profileModal.modal({backdrop:false}).on("hidden.bs.modal",function(){
                $(evt).prop("disabled",false);

            }).on('hidden.bs.modal',function(){
                var treeNode="#"+formName+" [data-tree]";
                $(treeNode).empty();
            }).on('shown.bs.modal',function(){

                var treeNode="#"+formName+" [data-tree]";

                if($(treeNode).size()>0) {
                    seajs.use(['jq.qubit', '/css/jquery.bonsai.css', 'jquery.bonsai'], function () {

                        $(treeNode).each(function (idx, node) {
                            var name = $(node).attr("name");
                            var result=[];

                            jsMenus(jsonCols[name]['data_source']['dataset'],result);
                            console.log(result,jsonCols[name]['data_source']['dataset'],JSON.stringify(jsonCols[name]['data_source']['dataset']));


                            var treeHtml = '<ol class="menu-content" data-name="'+name+'"><li class="expanded" data-value="0">默认<ol>' + result.join("") + "</ol></li></ol>";

                            $(node).append(treeHtml).find(".menu-content").bonsai({
                                expandAll: true,
                                createInputs: 'radio'
                            });


                        });


                    });
                }





                condLoad(formName);

                var input=profileModal.find("input[data-role]");

                if($.isFunction(tbOpCbs['add'])){
                    tbOpCbs['add'](rowData,formName,modalId);
                }
                if($.isFunction(tbOpCbs['always'])){
                    tbOpCbs['always'](rowData,formName,modalId);
                }


            });

        });
    }  else {
        modal.modal({backdrop:false});
    }


    evt.preventDefault();

}
var canWrite=function(){
    return config.user.level==1;
}
var editRow=function(e){

    var ulNode=$(this).parent().parent();
    var rowNodeId=ulNode.attr("data");
    var rowNode=$("#"+rowNodeId);
    var rowData=getRowData(rowNodeId);
    var tbName=ulNode.attr("data-table");

    var tbId="tb_"+tbName;
    var pid=tbName+"_edit_win";
    /**
     * @todo do some work to process one 2 many,many to many relation
     */

    var rowId=rowNodeId.replace(/r_/,'');

    if(beforeOp('edit',{ztb:tbName,pid:rowId,action:'edit'})){
        evt.preventDefault();
        return;
    }


    var modal = $("div[id=" + pid + "]");
    var clickTarget=$(this);
    clickTarget.prop('disabled',true);
    var formName=tbName+'-form-edit';
    ajaxCbs[tbName+'-form-edit']=[function(data){
        var formJson=$.unparam(ajaxCbs[tbName+'-form-edit-data']);
        if($.isFunction(tbOpCbs['afterEdit'])){
            tbOpCbs['afterEdit'](formJson,formName);
        }
        for(var key in formJson){
            var node=rowNode.find("td[data=c_"+key+"]");
            if(node.size()>0){
                node.text(formJson[key]);
            }

        }

        setTimeout(function(){$("#"+pid).modal('hide');},2000);
        // setTimeout(function(){modal.modal('hide');modal.toggle();console.log("tt",modal);},1500);


    }];

    var lTbName=config.lang['t_'+tbName.toLowerCase()]?config.lang['t_'+tbName.toLowerCase()]:tbName;
    if (modal.size() < 1) {

        $.get('/admin/js/table/temp/record_edit.html', function (html) {
            var profileModal = $(template.compile(html)($.extend({},{l_tbName:tbNameTxt,tbName:tbName,'write_all':canWrite(),l_edit:config.lang.edit,l_record:config.lang.record,rowId:rowId,row:rowData,lang: config.lang,cols:jsonCols,inputTypes:inputTypes})));

            // profileModal.drags({handle: ".modal-header"});
            // profileModal.find(".modal-dialog").resizable();

            profileModal.on('shown.bs.modal',function(){

                var input=profileModal.find("input[data-role]");
                condLoad(formName,{"datetime":function(){
                    $(".bs-datetime").datetimepicker();
                }});

                _.each(rowData, function (val, key) {

                    var  newKey="e_"+tbName+"_"+key;
                    var  input= $("#"+pid+" :input[name=" + key + "]");
                    console.log(input);
                    if (input.size() > 0) {
                        if(key=='action'){
                            console.debug("start debug");
                        }
                        if (input[0].tagName.toLowerCase() == "select") {
                            $("#"+pid+" :input[name=" + key + "] option[value='" + val + "']").prop("selected", true);
                        }
                        else if (input[0].type.toLowerCase() == "checkbox" || input[0].type.toLowerCase() == "radio") {
                            console.log(input[0]);
                            $("#"+pid+" :input[name=" + key + "][value='" + val + "']").prop("checked", true);

                        }
                        else {
                            if (key == 'enablesound' && val == '1') {
                                // input.parent().find(".slider-frame>.slider-button").addClass('on');
                            }
                            console.log(key);
                            $("#"+newKey).val(val);
                        }

                    }


                });
                if($.isFunction(tbOpCbs['edit'])){
                    tbOpCbs['edit'](rowData,formName);
                }
                if($.isFunction(tbOpCbs['always'])){
                    tbOpCbs['always'](rowData,formName);
                }

            }).on('hidden.bs.modal',function(){
                clickTarget.prop('disabled',false);
            });
            profileModal.modal()
        });
    }
    else {
        modal.modal('show');
    }


    e.preventDefault();
}
var showMoreOperation=function(e){

}
var doRm=function(rmData){
    console.log("do remove");
    $.post("/admin/table/recordsremove",rmData,function(data){
        var data= $.parseJSON(data);
        if(data.code==1){
            if(rmData['ids'].length>1){
                $("input[name=sel_all]").prop("checked",false);
            }

            rmData['ids'].map(function(item){
                $("#tb_"+tbModel).data('footable').removeRow( $("#r_"+item));

            });
            console.log('remove suc');
        } else {
            console.log('remove failure');
        }
    });
}

var setUpRmRow=function(popUpEle,opts,title){
    var opts=opts || {};
    var many=false;
    var title=title || config.lang.rm_row_prompt;

    setUpRmPopover(popUpEle,title,opts,{
        'shown':function(){
            console.log("rm shownn");
        },
        'show':function(){
            console.log("rm show");
        },
        'ok':function(){
            var rmData={};

            rmData['ztb']=popUpEle.attr("data");
            rmData["ids"]=[popUpEle.attr("data-id")];

            doRm(rmData);
            console.log("ok show");

        },
        'hidden':function(){
            console.log("ok hidden");
            console.log("hidden");
        }
    });
}

var setUpRmManyRow=function(popUpEle,opts,title){
    var opts=opts || {};
    var many=false;
    var title=title || config.lang.rm_row_prompt;

    setUpRmPopover(popUpEle,title,opts,{
        'shown':function(){
            console.log("rm shownn");
        },
        'show':function(){
            console.log("rm show");
        },
        'ok':function(){
            var rmData={};

            rmData['ztb']=popUpEle.attr("data");

            var ids=Array.prototype.slice.call($("tbody tr").has(".edit-row>input:checked:visible").map(function(a,item){return $(item).attr("id").replace(/r_/,'');}),0);

            console.log("many ids",ids);
            rmData["ids"]=ids;


            doRm(rmData);
            console.log("ok show");

        },
        'hidden':function(){
            console.log("ok hidden");

        }
    });
}


var chkWechatBind=function(obj){

    var form=$(obj).closest("form");

    var obj=$(obj);
    $.get('/user/chkwechat').done(function(data){
        var data= $.parseJSON(data);
        if(data.data.status==0){
            setQrUrl(function(data){

                if (/^http[s]?:\/\/.+/.test($.trim(data))) {

                    createPopover(obj,'检测到您未绑定微信,请先扫描二维码绑定微信','<img width="140px" height="auto" src="/img/ajax-loading.gif">',{placement:'bottom',trigger:'manual'},{'shown':function(obj){
                        console.log(obj);
                        $(obj).find("img").attr('src',data);

                    }});
                    obj.popover('show');
                }


            },'?action=adduser&type=wechat');

        }
    });
}
function showGotoTop() {
    var $gotoTop = $('.goto-top');

    if ($(document).scrollTop() > 0) {
        $gotoTop.fadeIn('slow');
    } else {
        $gotoTop.fadeOut('slow');
    }
}
var getRowData=function(rowNodeId){
    var row={};
    $("#"+rowNodeId).find("td[data^=c_]").each(function(idx,item){
        row[$(item).attr("data").replace(/c_/,'')]= $.trim($(item).text());
    });
    return row;
}
var doRecordSearch=function(evt,kw){
    console.log("resetabcdd");
    var table=$("#"+$(evt).attr("data-tb"));
    var footable=table.data('footable');
    console.log("reset",footable);
    footable.reset();
    table.find("tbody").empty();
    loadData(evt,{kw: kw},table);
    table.trigger('footable_redraw');

}


function condLoad(formName,cbs){
    var flags={'datetime':false,'file':false,'tags':false};
    var defCbs={
        'datetime':function(){},
        'file':function(){},
        'tags':function(){

        }


    };
    cbs= $.extend(defCbs,cbs);

    var fileNodeName="#"+formName+" button[data-role=upload]";
    var fileNode=$(fileNodeName);


    var dateNodeName="#"+formName+" input[type=datetime]";
    var dateNode=$(fileNodeName);

    var tagsNodeName="#"+formName+" input[data-role=tags]";
    var tagsNode=$(fileNodeName);

    console.log(fileNode,fileNodeName);
    if(tagsNode.size()>0){
        if(!tagsNode.hasClass('dz-clickable')) {
            tagsNode.on('click', function (e) {
                return false;
            });


            tagsNode.addClass('dropzone').css({"min-width": "300px"});
            initUploadPanel(tagsNodeName, fileNode.attr('data-mime'));
        }
    }

    if(dateNode.size()>0){
        if(!dateNode.hasClass('dz-clickable')) {
            seajs.use(["/css/bs3.datepicker.css","bs3.datepicker.js"],cbs['datetime']);
        }
    }


    if(tagsNode.size()>0){
        seajs.use(['/js/bs.tagsinput','/css/bs.tagsinput.css'],cbs['tags']);
    }

}
var initTableUi=function(jqTb){

    $(jqTb).footable({
        bookmarkable: { enabled: false }

    }).bind( {
        'footable_sorting' : function(e) {
            //return confirm('Do you want to sort by column: ' + e.column.name + ', direction: ' + e.direction);
        },
        'footable_filtering' : function(e) {
            var selected = $(this).find('.filter-status').find(':selected').text();
            if (selected && selected.length > 0) {
                e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
                e.clear = !e.filter;
            }
        },
        'footable_paging':function(e) {


        }
    });

    var footable =  $(jqTb).data('footable');
    $(jqTb).footable().on('click', '.row-delete', function(e) {
        e.preventDefault();
        //get the footable object
        var footable =  $(jqTb).data('footable');

        //get the row we are wanting to delete
        var row = $(this).parents('tr:first');

        //delete the row
        footable.removeRow(row);
    });

}
var initTableEvt=function(){
    $("body").on('click','.opt-choose',function(){
        var id=$(this).attr("data-id");

        window.top.$.publish("page-cfg",{id:id});

    });
    $('.nav-tabs a').click(function (e) {
        //show the tab!
        $(this).tab('show');

    }).on('shown', function (e) {

        //make sure that any footable in the visible tab is resized
        $('.tab-pane.active .footable').trigger('footable_resize');

    });

    //if there is a hash, then show the tab
    if (window.location.hash.length > 0) {
        $('.nav-tabs a[href="' + window.location.hash + '"]').tab('show');
    }



    var searchKeyword="#"+prefix+"search_keyword";
    var keyword="#"+prefix+"keyword";
    var tbId='tb_'+prefix;





    $('.clear-filter').click(clearFilter);

    $(".search_keyword").on('keyup',doGlobalSearch);





    $(window).scroll(lazyShowGotoTop);
    $(window).resize(lazyShowGotoTop);
    $('.goto-top').click(function() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
        return false;
    });

    /**
     * 表格过滤
     */
    $('.filterable .filters input').on('keyup',doFilter);
    /**
     *
     */
    $(".fixed-table-pagination").on('click','.dropdown-menu a',function(){
        console.log("dropdown",this);

        var prefix=$(this).parent().parent().attr('data');
        var pagerNode=$(".fixed-table-pagination[data="+prefix+"]");
        var pageSizeNode=pagerNode.find(".page-size");
        var cnt= parseInt($.trim($(this).text()));
        var startNode=pagerNode.find(".start");
        var endNode=pagerNode.find(".end");
        var start=parseInt($.trim(startNode.text()));
        var end=parseInt($.trim(endNode.text()));

        var size=parseInt($.trim(pageSizeNode.text()));
        console.log(cnt,size);
        if(cnt!=size){
            size=cnt;
            $(this).parent().parent().find("li").removeClass('active');

            $(this).parent().addClass('active');
            end=cnt+start-1;
            pageSizeNode.text(size);
            endNode.text(end);
            var tbId="#tb_"+prefix;

            /**
             * change page size
             */
            $(tbId).attr('data-page-size',size);
            $(tbId).data('pageSize',size);
            $(tbId).data('footable').options.pageSize=size;
            var pager=$("table").data('footablePaging');
            pager.setupPaging();

        }


    });
    //隐藏一列
    $.each(jsonCols,function(k,v){
        if('rights' in v && (v.rights== 8 || (v.rights!=8 && ((v.rights & 4) !=4)))){
            console.log(k);
            $(".filter_cols a[data=c_"+k+"] input[type=checkbox]").trigger('click');

            $("td[data=c_"+k+"]").hide();
            $("th[data=c_"+k+"]").hide();

        }
    });

    $("body").on('click','table .filter_cols li a',function(e){
        var checkBox=$(this).find("input[type=checkbox]");
        var isChecked=checkBox.prop("checked");
        var colName=$(this).attr("data");
        var tdNode=$("td[data=media_cs]");
        var colSpan=parseInt(tdNode.attr("colspan"));

        if(isChecked){
            tdNode.attr("colspan",(colSpan+1));
            $("td[data="+colName+"]").show();
            $("th[data="+colName+"]").show();
        }
        else {

            tdNode.attr("colspan",(colSpan-1));
            $("td[data="+colName+"]").hide();
            $("th[data="+colName+"]").hide();
        }


    });
    $('.operation .fa.fa-list').on('click',showMoreOperation);

    $("body").on('click','table a[data-page=more]',loadMore);

    $('.operation a.opt-times').each(function(idx,item){
        setUpRmRow($(item));
    });
    setUpRmManyRow($("a.rm-sel-row"),{placement:'bottom'},config.lang.rm_sel_row_prompt);



    $('body').on('click','.operation a.opt-edit',editRow);


    $("#add-row").on('click',addRow);


    var recordAddWin=$(".record-add-win");
    $(".col-meta .edit-row>input[type=checkbox]").on('click',function(e){
        var state=$(this).prop('checked');

        var tbObj=$(this).parent().parent().parent().parent();
        var checkObjs=tbObj.find("tbody .edit-row>input:visible");
        checkObjs.prop('checked',state);
        var self=this;
        setTimeout(function(){  $(self).prop('checked',state);},20);
        e.preventDefault();
        return false;


    });





    seajs.use(['/js/dropzone','/css/dropzone.css'],function(){
        Dropzone.autoDiscover=false;

        attachDropzone(".import.dropzone",{url: "/admin/table/import/?ztb={{$data.tbName}}&t=csv",acceptedFiles:'text/*'},false,{
            success:function(e,data){
                var data= $.parseJSON(data);
                if(data.code==1){
                    showOverlay($("#tb_"+tbModel),'check','导入成功');
                } else {
                    showOverlay($("#tb_"+tbModel),'times',data.msg);
                }

                console.log("upload suc",e,"msg",data);
            },
            error:function(e,data){
                showOverlay($("#tb_"+tbModel),'times','导入失败');
            }

        });

    });
    checkInput();

    $(".search_keyword").on('keypress',function(evt){

        if(evt.keyCode=='13'){
            evt.preventDefault();
            doRecordSearch(this,$.trim($(this).val()));

            return false;
        }
    });
    $(".search-btn").on('click',function(evt){
        doRecordSearch(this,$.trim($(this).parent().find(".search_keyword").val()));
        evt.preventDefault();
        return false;

    });
    $("body").on('click','.img-thumb',imgPrev);


}

var imgPrev=function(){

        var imgObj=$(this);

        var overlapObj=$(template.compile(capScreenTemp)({src:this.src,note:this.title,download:true})).on('hidden.bs.modal',function(){
            $(this).remove();
            console.log(this,"remove");
        });
        overlapObj.find('a.external').attr("href",this.src);
        overlapObj.modal();

}