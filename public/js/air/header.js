//'use strict';
// Source: js/common/src/position.js

var centerObj=function(container,div){


    var oW=container.outerWidth(true);
    var oH=container.outerHeight(true);

    div.css({position:"absolute","z-index":999});
    container.before(div);
    container.prop("disabled",true);
    div.show();

    var containerOffset=container.offset();
    div.offset({left:(containerOffset.left+(oW-div.outerWidth())/2),top:(containerOffset.top+(oH-div.outerHeight())/2)});



}
var appendCenterDiv=function(container,centerDom){

    var oW=container.outerWidth();
    var oH=container.outerHeight();

    centerDom.css({position:"absolute"});
    container.before(centerDom);
    container.prop("disabled",true);

    var btnOffset=centerDom.offset();
    centerDom.offset({left:(btnOffset.left+(oW-centerDom.width())/2),top:(btnOffset.top+(oH-centerDom.height())/2)});
    centerDom.show();
};// Source: js/common/src/ajax_form.js
var ajaxSubmitHandler=function(e){

    ajaxSubmit($(this));

    e.preventDefault();
    return false;

};
var ajaxSubmit=function(jqForm,jqSubbtn,loadingImg){
    var that=jqForm;
    var formId=jqForm[0].id;

    var subBtn=jqSubbtn || jqForm.find("[type=submit]");

    var loadingImg=loadingImg || $(".loading-icon").clone();

    $(document).ajaxStart(function(){
        var oW=subBtn.outerWidth();
        var oH=subBtn.outerHeight();
        if(!subBtn.prev().hasClass("loading-icon"))
        {
            var btnOffset=subBtn.offset();
            loadingImg.css({position:"absolute"});
            subBtn.before(loadingImg);
            subBtn.prop("disabled",true);
            loadingImg.show();

            loadingImg.offset({left:(btnOffset.left+(oW-16)/2),top:(btnOffset.top+(oH-16)/2)});

        }
        else {
            loadingImg.show();
        }

    }).ajaxComplete(function(){
        loadingImg.hide();
        setTimeout(function(){subBtn.prop("disabled",false);},1000);

    });
    if(ajaxCheck(that))
    {
        ajaxPost(that);
        return false;
    }
    return false;

}


var ajaxPost=function(jqForm){
    var postData=jqForm.serialize();
    var upload=jqForm.find(":file");
    var prefix=(location.protocol+"//"+location.host);
    var doPost=function(extra){
        var postData=jqForm.serialize();
        if($.isPlainObject(extra)){
            extra=$.param(extra);
            postData=$.extend(postData,extra);
        }
        /**
         * jqForm[0].action和jqForm.attr("action")区别是有些浏览器里定义了action的字段名会被取到导致不正确
         */
        $.post(jqForm.attr("action"),jqForm.serialize(),function(data){
            var ret={code:-1,msg:config.lang.req_failed,data:{}};
            if(ival.json.test(data)){
                ret=$.parseJSON(data);
                var errDiv = jqForm.find(".return-error");
                var sucDiv = jqForm.find(".return-suc");
                if(errDiv.size()==0){
                    jqForm.prepend('<div class="return-error"></div>');

                }
                if(sucDiv.size()==0){
                    jqForm.prepend(' <div class="return-suc"></div>');
                }
                /**
                 *  get registed callback
                 *  ajaxCbs[formId]=[sucCallback,failCallback]
                 */

            }
            var formId=jqForm[0].id;
            ajaxCbs[formId+'-data']=postData;
            ajaxCbs[formId]=[(ajaxCbs[formId] && ajaxCbs[formId][0])?ajaxCbs[formId][0]:function(){}, (ajaxCbs[formId] && ajaxCbs[formId][1])?ajaxCbs[formId][1]:function(){}];
            processRet(ret,sucDiv,errDiv,ajaxCbs[formId][0],ajaxCbs[formId][1],3000,jqForm);

        });
    };
    if(upload.size()>0){

        var uploadWorker = new Worker((prefix+'/js/uploadworker.js'));

        /**
         * get msg mid
         */
        uploadWorker.onmessage=function (e) {
            console.log(e);
            var ret = e.data;
            if (ret.code == 1) {
                doPost(ret.data);
            }
        };
        uploadWorker.postMessage({url: (prefix+'/index/media/upload/' + file.name + ".json"), data: file});
    } else {
        doPost();
    }

}


var sendAuth=function(xhr,setting){
    /**
     * 定义后台交互超时
     * @type {{}}
     */
    //if(typeof loginTimeoutInt!='undefined'){
    //    clearTimeout(loginTimeoutInt);
    //    loginTimeoutModal(parseInt(config.setting.login_timeout)*1000);
    //}

    var errArr={};
    var valdata='';
    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    if(this.type=='POST' || this.type=='PUT')
    {
        if(typeof this.contentType!='undefined' && !_.isEmpty(this.contentType) && this.contentType.indexOf('application/json')!=-1){

            if(typeof this.data=='undefined'){

                this.data={};
            }
            else
            {
                this.data=$.parseJSON(this.data);
            }
            console.log("json data",this.data);
            this.data['ctoken']=config.ctoken;
            console.log("json data",this.data);
            this.data=$.toJSON(this.data);
            return true;
        }
        if(typeof this.data==='undefined'){
            if(!_.isEmpty(this.contentType) && this.contentType.indexOf('application/json')!=-1)
            {
                this.data={};
                this.data['ctoken']=config.ctoken;
                this.data=$.toJSON(this.data);
            }
            else {
                this.data='ctoken='+encodeURIComponent(config.ctoken);
            }

            return true;
        }
        if(typeof this.data=="string")
        {

            valdata= $.unparam(this.data);
            if(this.data=='')
            {
                this.data='ctoken='+encodeURIComponent(config.ctoken);
            }
            else
            {
                this.data+='&ctoken='+encodeURIComponent(config.ctoken);
            }

        }
        if($.isPlainObject(valdata))
        {

            for(var key in valdata)
            {

                var input=$(":input[name="+key+"]");
                if(input.size()>1){
                    var input=$(":input[name="+key+"]:visible");
                }
                input.removeClass('input-err');
                input.tooltip('destroy');
                if(input.size()>0)
                {
                    var res=checkOneInput(input);
                    console.log(res);
                    if(res[0]!=true)
                    {
                        var res2={};
                        errArr[key]=res[1];

                    }
                }

            }

            if(!$.isEmptyObject(errArr))
            {
                console.log("errarr",errArr);
                _.each(errArr,function(title,name){

                    var obj=$(":input[name="+name+"]");
                    if(obj.hasClass("input-err"))
                    {
                        obj.tooltip('destroy');
                        obj.tooltip({

                            title:title,
                            trigger:'click'
                        });

                    }
                    else
                    {
                        obj.addClass("input-err");

                        obj.tooltip({

                            title:title,
                            trigger:'click'
                        });

                    }

                });
                return false;
            }
            return true;
        }
    }

    return true;

}


var processRet=function(ret,suc,fail,succb,failcb,tm,jqForm){

    var succb=succb || function(){};
    var failcb=failcb || function(){};
    var tm=tm||2500;
    if(ret.code==1)
    {
        succb(ret.data);
        showOverlay(jqForm,'',ret.msg);
    }
    else
    {
        failcb(ret.data);

        if(jqForm){
            var allInputs=jqForm.find(":input");
            allInputs.removeClass("input-err");

            for(var field in ret.data){
                var input=jqForm.find(":input[name="+field.replace(/^t_/,"")+"]");
                if(input.size()>0){
                    input.addClass("input-err");
                    showTips(input,ret.data[field],{duration:10000});
                }

            }
        }
        showOverlay(jqForm,'times',ret.msg);
    }

};// Source: js/common/src/validator.js
/**
 * Created by chjade on 5/6/15.
 */
var isJson=function(str) {
    var result=false;
    try {
        result=JSON.parse(str);
    } catch (e) {
        result=false;
    }
    return result;
}
var isRegJson=function(text){
    if (/^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
            replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
            replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
        return true;
        //the json is ok

    }else{
        return false;
        //the json is not ok

    }
}

var isJsonRegex = function(str) {
    if (str == '') return false;
    str = str.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, '');
    return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(str);
}

var ajaxCheck=function(jqForm){

    /**
     * required input value checking
     * @type {Array}
     */
    var that=jqForm;
    var requiredErrs=[];
    $(that).find(":input.required").each(function(idx,item){
        var input=$(item);
        if($.trim(input.val())===''){
            var label= input.parent().find("label") || input.parent().parent().find("label");
            console.log(label);
            var errMsg=(label.size()>0?label.text():$(item).attr("name").replace(/_/g,' '))+" is invalid";

            requiredErrs.push([input,errMsg]);
        }
        else {

            input.removeClass("input-err");
            input.tooltip('destroy');
        }

    });
    for(var i=0;i<requiredErrs.length;i++){

        requiredErrs[i][0].addClass("input-err");
        requiredErrs[i][0].tooltip({

            title:requiredErrs[i][1],
            trigger:'click'
        });
    }
    if(requiredErrs.length>0){
        return false;
    }
    return true;
}
var checkOneInput=function(input){

    var regData=input.attr("ref");

    if(regData)
    {


        var regData=$.parseJSON(regData);

        var errMsg='';
        if(typeof regData['msg']!='undefined'){
            errMsg=regData['msg'];
        }
        else {
            var label=input.parent().parent().find("label") || input.parent().find("label");
            var labelText=label.size()>0?label.text():$(this).attr("name").replace(/_/g,' ')+config.lang.is_invalid;
            errMsg=labelText;
        }
        var inputVal= $.trim(input.val());
        if(regData['ref'])
        {
            var refNode=$(":input[name="+regData['ref']+"]");
            if(refNode.size()>0 && !($.trim(refNode.val())==inputVal))
            {
                return [false,config.lang.chk_match.replace(/#name#/,config.lang[input.attr("name")]?config.lang.regData['ref']:input.attr("name"))];
            }
            else
            {
                return [true,refNode];
            }

        }
        if(regData['r']==1)
        {

            if(inputVal==''){
                return [false,input.attr('placeholder')+' '+config.lang.cant_empty];
            }
            else
            {
                if(!ival[regData['reg']].test(inputVal))
                {
                    return [false,errMsg];

                }
            }
        }
        else
        {
            if(inputVal!=''){
                if(!ival[regData['reg']].test(inputVal))
                {

                    return [false,errMsg];

                }
            }
        }




    }
    return [true,1];

}


var checkInput=function(){

    $(":input").blur(function(){
        var res=checkOneInput($(this));
        if(res[0]===true)
        {
            if(res[1]!='1')
            {
                res[1].removeClass("input-err");
                $(this).tooltip('destroy');
            }

            $(this).removeClass("input-err");
            $(this).tooltip('destroy');
            return;
        }
        if($(this).hasClass("input-err"))
        {
            $(this).tooltip('destroy');
            $(this).tooltip({

                title:res[1],
                trigger:'click'
            });

        }
        else
        {

            $(this).addClass("input-err");

            $(this).tooltip({

                title:res[1],
                trigger:'click'
            });

        }


    });

};// Source: js/common/src/effect.js




var setuploadingEffect=function(subBtn,loadingImg){
    var height=subBtn.height();
    if(height<64){
        loadingImg=loadingImg || $(".loading-icon").clone();

    } else if(height<200 && height>64) {
        loadingImg=loadingImg || $("<img />").attr("src","/img/ajax-loading-small.gif");
    } else if(height<300 && height>200) {
        loadingImg=loadingImg || $("<img />").attr("src","/img/ajax-loading-middle.gif");
    } else if(height>300 && height>450) {
        loadingImg=loadingImg || $("<img />").attr("src","/img/ajax-loading-large.gif");
    } else if(height>450) {
        loadingImg=loadingImg || $("<img />").attr("src","/img/ajax-loading-xlarge.gif");
    }


    $(document).ajaxStart(function(){

        if(!subBtn.prev().hasClass("loading-icon"))
        {
            centerObj(subBtn,loadingImg);

        }
        else {
            loadingImg.show();
        }

    }).ajaxComplete(function(){
        loadingImg.hide();
        $(document).off('ajaxStart');
        setTimeout(function(){subBtn.prop("disabled",false);},1000);

    });

}
;// Source: js/common/src/tooltip.js

var showTips = function (self, msg,options) {
    var opt={
        isshow:true,
        trigger:'manual',
        place:'bottom',
        duration:2000,
        container:'body'
    };



    var orgTitle=self.attr("data-original-title");
    self.tooltip("destroy");
    self.attr({"data-original-title":msg});
    opt= $.extend(opt,options);
    var args= $.extend(args,arguments);
    self.tooltip({title: msg, placement: opt.place, trigger: opt.trigger}).on('shown.bs.tooltip', function () {

        _.delay(function () {
            self.tooltip('destroy');
        }, opt.duration);

    });
    if(opt.isshow) {self.tooltip('show');}
    return false;
};// Source: js/common/src/overlay.js
/**
 *
 * @param container which container displays overlay
 * @param title
 * @param timeout timeout seconds
 * @param type
 */
var showOverlay=function(container,type,title,timeout){
    //check,times
    var type=type|| "check";
    var timeout=timeout || 5;
    if(type=='spin') {
        type = "fa-spinner fa-spin";
    }
    else if(type=='spin-lg') {
        type = "fa-spinner fa-spin fa-10x";
    }
    else if(type=='spin-md') {
        type = "fa-spinner fa-spin fa-5x";
    }
    else if(type=='spin-sm'){
        type="fa-spinner fa-spin fa-3x";
    }
    else {
        type="fa-"+type;
    }
    var overlay='<div class="ui-ios-overlay"><span class="title">'+title+'</span> <span class="fa '+type+'"></span></div>';
    var obj=$(overlay);
    setTimeout(function(){obj.remove();},timeout*1000);
    centerObj(container,obj);
    return obj;

}