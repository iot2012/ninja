


//var wxDev=openWXDeviceLib();
//
//alert(JSON.stringify(wxDev));

var openWindow=function(url){

    var defaultSettings = {
        centerBrowser: 1, // center window over browser window? {1 (YES) or 0 (NO)}. overrides top and left
        centerScreen: 0, // center window over entire screen? {1 (YES) or 0 (NO)}. overrides top and left
        height: 580, // sets the height in pixels of the window.
        left: 0, // left position when the window appears.
        location: 0, // determines whether the address bar is displayed {1 (YES) or 0 (NO)}.
        menubar: 0, // determines whether the menu bar is displayed {1 (YES) or 0 (NO)}.
        resizable: 1, // whether the window can be resized {1 (YES) or 0 (NO)}. Can also be overloaded using resizable.
        scrollbars: 1, // determines whether scrollbars appear on the window {1 (YES) or 0 (NO)}.
        status: 0, // whether a status line appears at the bottom of the window {1 (YES) or 0 (NO)}.
        width: 360, // sets the width in pixels of the window.
        windowName: "null", // name of window set from the name attribute of the element that invokes the click
        windowURL:url, // url used for the popup
        top: 0, // top position when the window appears.
        toolbar: 0 // determines whether a toolbar (includes the forward and back buttons) is displayed {1 (YES) or 0 (NO)}.
    };

    var settings = $.extend({}, defaultSettings, null || {});

    var isie=/MSIE (\d+\.\d+);/.test(navigator.userAgent);
    //Up the height
    if (isie){
        settings.height = $(window).height() *.85;
    }
    else {
        settings.height = window.outerHeight *.85;
    }

    var windowFeatures = 'height=' + settings.height   +
        ',width=' +
        settings.width +
        ',toolbar=' +
        settings.toolbar +
        ',scrollbars=' +
        settings.scrollbars +
        ',status=' +
        settings.status +
        ',resizable=' +
        settings.resizable +
        ',location=' +
        settings.location +
        ',menuBar=' +
        settings.menubar;

    //settings.windowName = this.name || settings.windowName;
    //settings.windowURL = this.href || settings.windowURL;

    var centeredY, centeredX;

    if (settings.centerBrowser) {
        if (isie) {//hacked together for IE browsers
            centeredY = (window.screenTop - 120) + ((((document.documentElement.clientHeight + 120) / 2) - (settings.height / 2)));
            centeredX = window.screenLeft + ((((document.body.offsetWidth + 20) / 2) - (settings.width / 2)));
        }
        else {
            centeredY = window.screenY + (((window.outerHeight / 2) - (settings.height / 2)));
            centeredX = window.screenX + (((window.outerWidth / 2) - (settings.width / 2)));
        }
        childWindow=window.open(settings.windowURL, settings.windowName, windowFeatures + ',left=' + centeredX + ',top=' + centeredY);
        childWindow.focus();

    }

    if (settings.centerScreen) {
        centeredY = (screen.height - settings.height) / 2;
        centeredX = (screen.width - settings.width) / 2;
        childWindow=window.open(settings.windowURL, settings.windowName, windowFeatures + ',left=' + centeredX + ',top=' + centeredY);
        childWindow.focus();

    }
    else {
        childWindow=window.open(settings.windowURL, settings.windowName, windowFeatures + ',left=' + settings.left + ',top=' + settings.top);
        childWindow.focus();

    }

    return childWindow;

}


routie({
    'sensor': function() {
        $('#arrow-down').show();
        $("#"+this.path+"-tab").trigger('click');

    },
    'buy': function() {
        console.log("ww");

        $("#"+this.path+"-tab").trigger('click');
    },

    'download': function() {
        $("#"+this.path+"-tab").trigger('click');

    },
    'airenv': function() {

        $("#"+this.path+"-tab").trigger('click');
    },
    '*':function() {
        $('#arrow-down').show();
    }
});
$(function(){



    var airUrl="http://api.gogoinfo.cn/air/envref";
    var doArrowDown=function(){

        $(".adv").hide();
        $('.air-card').hide();
        $('#arrow-down').hide();
        $('.air-detail-card').addClass('animated fadeInUp').show();
        var curAirOverLay=showOverlay($("#air-detail-card"),'spin-md','',10);
        $.getJSON(airUrl).done(function(data){
            console.log(data,data.code);
            if(data.code==1){
                var cityName='';
                if('city' in data.data){
                    cityName=data.data.city;
                } else {
                    cityName='北京';
                }
                $(".air_cond").addClass("icon-"+(data.data.current.day_cond_py==''?data.data.current.night_cond_py:data.data.current.day_cond_py));
                $("#cur_air_so2").text(data.data.current.aqi.so2_1h);
                $("#cur_air_no2").text(data.data.current.aqi.no2_1h);
                $("#cur_air_pm25").text(data.data.current.aqi.pm25_1h);
                $("#cur_air_pm10").text(data.data.current.aqi.pm10_1h);

                $("#cur_air_time").text(config.lang.today+data.data.current.time);
                $("#cur_air_city_name").text(cityName);
                $("#cur_air_temp").text(data.data.current.temp+"°");
                $("#cur_air_hum").text(data.data.current.humidity+"%");
            }

        }).always(function(){
            curAirOverLay.remove();
        });
    }
    var autoSearch= _.debounce(function () {

        var filterLis = $("#airenv-sec>.list-group:not(:hidden) .search-item");

        var rex = new RegExp($(this).val(), 'i');
        filterLis.hide();
        filterLis.filter(function () {
            return rex.test($(this).find('.search-item-txt').text());
        }).show();
    },300);

    $("#wifi-tab").on('click',function(evt){
        location.href="/air/wifi";
        evt.preventDefault();
        return false;
    });
    $("#search_place").on('keyup', autoSearch);
    $("#province-list").on('click',"li.search-item",function(){
        var code=$(this).attr("data");

        var overLay=showOverlay($(this),'spin-md','',10);

        $.post('/air/cities',{code:code}).done(function(data) {
            var city = '<li class="list-group-item  search-item"  data="<%=code%>"> <a href="#airenv-sec-detail" data-toggle="tab"><span class="fa fa-angle-right expand-item"></span> <span class="search-item-txt"><%=name%></span></a> </li>';



            var data = $.parseJSON(data);

            var cities = data.map(function (item) {
                return template.compile(city)(item)
            });
            var cityList=$("#city-list");
            cityList.html(cities.join(""));
            cityList.find('li.search-item').on('shown.bs.tab',function(e){
                // e.target // newly activated tab
                // e.relatedTarget // previous active tab
                var cityName=$(this).find(".search-item-txt").text();
                var airOverLay=showOverlay($("#airenv-sec-detail"),'spin-md','',10);
                $.getJSON(airUrl,{'city':$(this).attr('data')}).done(function(data){
                    console.log(data,data.code);
                    if(data.code==1){

                        $(".air_cond").addClass("icon-"+(data.data.current.day_cond_py==''?data.data.current.night_cond_py:data.data.current.day_cond_py));
                        $("#air_so2").text(data.data.current.aqi.so2_1h);
                        $("#air_no2").text(data.data.current.aqi.no2_1h);
                        $("#air_pm25").text(data.data.current.aqi.pm25_1h);
                        $("#air_pm10").text(data.data.current.aqi.pm10_1h);

                        $("#air_time").text(config.lang.today+data.data.current.time);
                        $("#air_city_name").text(cityName);
                        $("#air_temp").text(data.data.current.temp+"°");
                        $("#air_hum").text(data.data.current.humidity+"%");
                    }

                }).always(function(){
                    airOverLay.remove();
                });

            });

            $("#province-list").hide();
            $("#city-list").show();
        }).always(function(){
            overLay.remove();
        });

    });

    $(".z-nav-bottom>ul>li>a").on('click',function(evt){
        console.log("windows",$(this).attr('href'));
        if($(this).attr('href')!="#sensor-sec"){
            $('#arrow-down').hide();
        } else {
            $('#arrow-down').show();
        }

    });
    $("body").on('click',".product-select>li>a",function(evt){
        $(this).parent().parent().find("li>a").not(this).removeClass('active');
        $(this).addClass('active');
        evt.preventDefault();
        return false;
    });

    $("#arrow-down").click(function(evt){
        doArrowDown();
    });
    //var airCard = new Hammer.Manager($(".air-card")[0], {
    //    recognizers: [
    //        // RecognizerClass, [options], [recognizeWith, ...], [requireFailure, ...]
    //        [Hammer.Rotate],
    //        [Hammer.Pinch, { enable: false }, ['rotate']],
    //        [Hammer.Swipe,{ direction: Hammer.DIRECTION_HORIZONTAL }],
    //    ]
    //});


    var arrowDown = new Hammer($("#arrow-down")[0]);

    arrowDown.get('swipe').set({ direction: Hammer.DIRECTION_VERTICAL,threshold: 10});

    arrowDown.on('swipeup',function(evt){
        console.log("swipeup",evt)
        doArrowDown();
    });
    $("#buy-btn").on('click',function(evt){
        //openWindow('/wechat/getjsparams?pid='+$(this).attr('data'));
        //alert("click");
        //evt.preventDefault();
        //evt.stopPropagation();
        //return false;

        //openWindow('/wechat/getJsParams/?pid='+$(this).attr('data'));
        if(isRequiredWechat()) {
            var number=$("input[name=number]").val();
            var model= $.trim($(".model-sel>a.active").text());
            var color=$.trim($(".color-sel>a.active").text());
            $("input[name=number]").parent().removeClass("input-error");
            $(".model-data,.color-data").removeClass("input-error");
            if(number==''){
                $("input[name=number]").parent().addClass("input-error");
                return false;
            }
            if(model==''){
                $(".model-data").addClass("input-error");
                return false;
            }
            if(color==''){
                $(".color-data").addClass("input-error");
                return false;
            }
            location.href = '/wechat/buy?number='+number+'&model='+model+'&color='+color+'&showwxpaytitle=1&pid=' + $(this).attr('data');


            evt.preventDefault();
            evt.stopPropagation();
            return false;

        }
        evt.preventDefault();
        evt.stopPropagation();
        return false;

    });
    function isRequiredWechat(){
        var result=navigator.userAgent.match(/MicroMessenger\/(\d\.\d+)/);
        if(result){
            var version=parseFloat(result[1]);
            return version>=5;
        }
        return false;

    }
    function invokeWxPay(json){
        console.log("invoke Json api");
        WeixinJSBridge.invoke(
            'getBrandWCPayRequest', json,
            function(res){
                console.log(res);
                if(res.err_msg == "get_brand_wcpay_request:ok" ) {


                }     // 使用以上方式判断前端返回,微信团队郑重提示：res.err_msg将在用户支付成功后返回    ok，但并不保证它绝对可靠。
            }
        );
    }


});

