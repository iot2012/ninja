(function($) {
    function getAbsUrl(url){
        return url.match(/^http[s]/)!=-1?url:(location.href.protocol+"//"+url);
    }
    $("[data-wechat=login]").on('click',function(evt){
        var url=$(this).attr("data-url");
        if(!url){
            url=getAbsUrl(location.href);
        }
        location.href="http://air.gogoinfo.cn/wechat/mobilelogin?_r="+encodeURIComponent(url);
        evt.preventDefault();
        return false;
    });
})(jQuery);