var dataReadyCb;
if(typeof seajs=='function'){
    seajs.config(
        {base: '/templates/dinning/assets/js'}
    );

}

var isImgKey=function(key){
    var fileKeys=['img','icon','avatar','avatars','imgs','icons','logo','logos']
    var result=fileKeys.map(function(item){
        var regExp=new RegExp(item+'$|\[[\w-]+'+item+'\]','i');
        return regExp.test(key);

    });
    return result.indexOf(true)>=0;

}
/**
 * 每个页面对应返回数据哪个section数据
 */



var config={
    t_section:{
        'index':'homepage'
    },
    imgdir:'/'
};


Date.prototype.format = function(fmt)
{ //author: meizz
    var o = {
        "M+" : this.getMonth()+1,                 //月份
        "d+" : this.getDate(),                    //日
        "h+" : this.getHours(),                   //小时
        "m+" : this.getMinutes(),                 //分
        "s+" : this.getSeconds(),                 //秒
        "q+" : Math.floor((this.getMonth()+3)/3), //季度
        "S"  : this.getMilliseconds()             //毫秒
    };
    if(/(y+)/.test(fmt))
        fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
    for(var k in o)
        if(new RegExp("("+ k +")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
    return fmt;
}
$(function(){






    var query=location.search.substr(1);
    query=$.unparam(query);
    var section=/\/([^\/\?]+)[\?]?[^\?]+$/.exec(location.href);
    section=section[1] || section[2];

    var sid=query['sid'];
    config.sid=sid;
    config.ver=query['ver'];
    section=section.replace(/\.html?$/,'');

    if(section){
        if(section in config.t_section){
            section=config.t_section[section];
        }
    }
    /**
     * 加入版本号以便CDN使用
     */
    //$("body").on('click','a',function(){
    //    var href=$(this).attr('href');
    //    if(href.indexOf("?")!=-1){
    //        $(this).attr("href",href+"&ver="+config.ver);
    //    } else {
    //        $(this).attr("href",href+"?ver="+config.ver);
    //    }
    //
    //});

    /**
     * sid 是siteId，站点id
     */
    var tmpData={
        'brand':{'title':'昱城环境设备有限公司'},
        'menus':[
            {'name':'主页','href':'/'},
            {'name':'产品','href':'product.html',subs:[
                {
                    "pid": 2,
                    "name": "中央空调系列",
                    "href": "product.html?cid=1",
                    "id": "3"
                },
                {
                    "pid": 2,
                    "name": "中央采暖系列",
                    "href": "product.html?cid=2",
                    "id": "4"
                },
                {
                    "pid":2,
                    "name": "中央新风系列",
                    "href": "product.html?cid=3",
                    "id": "5"
                },

                {
                    "pid": 2,
                    "name": "中央净水系列",
                    "href": "product.html?cid=4",
                    "id": "6"
                },
                {
                    "pid": 2,
                    "name": "中央除尘系列",
                    "href": "product.html?cid=5",
                    "id": "8"
                },
                {
                    "pid": 2,
                    "name": "中央热水系列",
                    "href": "product.html?cid=6",
                    "id": "9"
                },
                {
                    "pid": 2,
                    "name": "其它商用系列",
                    "href": "product.html?cid=7",
                    "id": "10"
                }
            ]
            },
            {
                "pid": 3,
                "name": "案例",
                "href": "example.html",
                "id": "10"
            },
            {
                "pid": 4,
                "name": "FAQ常见问题",
                "href": "faq.html",
                "id": "10"
            },
            {
                "pid": 5,
                "name": "联系我们",
                "href": "contact-us.html",
                "id": "10"
            }
        ]


    };
    $("script[type='text/html']").each(function(idx,item)   {
        console.log('tmpData',tmpData);
        var id=$(item).attr("id").replace(/_t_/,'');
        $(item).replaceWith(template(id,$.extend({_c:config},tmpData)));
    });
    //if('sid' in query){
    //    $.post('/temp/?sid='+sid, $.extend(query,{section:section}),function(data){
    //        console.log("rawdata",data);
    //        var orgData= $.parseJSON(data);
    //        console.log("data",orgData);
    //        tmpData=('temp' in orgData.data)?orgData.data.temp:{};
    //
    //        if('comps' in orgData.data  && $.isArray(orgData.data.comps)){
    //            data=orgData.data.comps;
    //
    //            for(var key in data.fields){
    //                if($("[data-edit='"+key+"']").size()>0){
    //                    if(isImgKey(key)){
    //                        if($.isArray(data.fields[key])){
    //                            for(var i=0;i<data.fields[key].length;i++){
    //                                $("[data-edit='"+key+"["+i+"]']").attr('src',data.fields[key][i]);
    //                            }
    //                        } else {
    //                            $("[data-edit='"+key+"']").attr('src',data.fields[key]);
    //                        }
    //
    //                    } else {
    //                        $("[data-edit='"+key+"']").html(data.fields[key]);
    //                    }
    //
    //                }  else if($("[data-display='"+key+"']").size()>0){
    //                    if($.isArray(data.fields[key])){
    //                        for(var i=0;i<data.fields[key].length;i++){
    //                            $("[data-display='"+key+"="+data.fields[key][i]+"']").show();
    //                        }
    //                    } else {
    //                        $("[data-display='"+key+"']").show();
    //                    }
    //
    //                }
    //
    //
    //            }
    //        }
    //
    //
    //
    //
    //        if($.isArray(dataReadyCb)){
    //            for(var i=0;i<dataReadyCb.length;i++){
    //                $.isFunction(dataReadyCb[i]) && dataReadyCb[i](orgData);
    //            }
    //        } else if($.isFunction(dataReadyCb)){
    //            dataReadyCb(orgData);
    //        }
    //    }).complete(function(data){
    //
    //        $("#_ry_temp_css").remove();
    //    });
    //}






});
if((typeof sendAuth=='function')){
    $.ajaxSetup({
        beforeSend:sendAuth,
        timeout:10000

    });
    $("body").on('submit',"form.ajax-form",ajaxSubmitHandler);

}

