<?php


use Zend\Cache\Storage\Adapter\Memcached;
use Zend\Cache\StorageFactory;
use Zend\Session\SaveHandler\Cache;
use Zend\Session\SessionManager;


error_reporting( E_ALL & ~E_NOTICE);
ini_set('display_errors', 'on');



date_default_timezone_set("Asia/Chongqing");
define("BASE_PATH", realpath(dirname(__FILE__) ));
define("APPLICATION_PATH",  realpath(dirname(__FILE__) . '/../'));

define("APP_PATH",  realpath(dirname(__FILE__) . '/../'));
define("LIBRARY_PATH", realpath(dirname(__FILE__) . "/library/"));
define("PUBLIC_PATH", dirname(__FILE__));
define("DS", '/');
$application = new Yaf\Application( APP_PATH . "/conf/application.ini");
$application->bootstrap()->run();




?>

